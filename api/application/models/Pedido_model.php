<?php

class Pedido_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->imazon = $this->load->database('imazon', TRUE);
        $this->imazon10 = $this->load->database('imazon10', TRUE);
    }

    public function create($d = null) {
        $this->db->insert('pedido', $d);
        return  $insertId = $this->db->insert_id();
    }

    public function insert_item_pedido($d = null) {
        return $this->db->insert('item_pedido', $d);
    }

    public function insert_log_transacao($d = null) {
        return $this->db->insert('log_transacao', $d);
    }

    public function atualizalog($data) {
        $this->db->where('transacao', $data['id']);
        $this->db->set('pago', 1);
        $res = $this->db->update('log_transacao');
        if ($res) {
            return $res;
        } else {
            return false;
        }
    }

    public function get_verificapago($data) {
        $f = $this->db->get_where('log_transacao', array('pago' => 1, 'transacao' => $data['pedido']))->result();
        if (count($f) >= 1) {
            return 1;
        } else {
            return 2;
        }
    }

    public function get_transacoes_em_aberto_by_id_usuario($id_usuario = null) {
        $sql = 'SELECT * FROM pedido as p
                join
                item_pedido as ip
                on  
                ip.transacao = p.transacao
                WHERE id_aluno = "' . $id_usuario . '" AND data_compra < ( NOW() - INTERVAL 5 DAY ) AND situacao != 3
                AND ip.tipo_item >= 7 
                AND ip.tipo_item <= 30 GROUP BY p.transacao';
        return $this->db->query($sql)->result();
    }

    public function get_by_codigo($d = null) {
        return $this->db->get_where('pedido', array('id_pedido' => $d), 1);
    }

    public function get_by_transacao($transacao = null) {
        return $this->db->get_where('pedido', array('transacao' => $transacao), 1);
    }

    public function atualiza_pedido($pedido, $dt) {
        //echo $pedido; print_r($dt);	

        $this->db->where('transacao', $pedido);
        $this->db->update('pedido', $dt);
    }

    public function get_by_transacaoped($transacao = null) {
        return $this->db->get_where('item_pedido', array('transacao' => $transacao));
    }

    public function get_by_transacao_aluno($transacao = null) {
        $this->db->where('id_aluno', $this->session->userdata('imazon_id_fic'));
        return $this->db->get_where('pedido', array('transacao' => $transacao), 1);
    }

    public function get_by_aluno($d = null) {
        $this->db->from('pedido');
        $this->db->where('id_aluno', $this->session->userdata('imazon_id_fic'));
        return $this->db->get();
    }

    public function get_by_aluno_token($d) {
        $this->db->from('pedido');
        $this->db->where('cod_interme', $d);

        return $this->db->get();
    }

    public function get_by_aluno_declaracao($d = null) {
        $this->db->select('curso.nome,item_pedido.tipo_item, item_pedido.valor,pedido.data_compra');
        $this->db->from('pedido');
        $this->db->join('item_pedido', 'item_pedido.transacao = pedido.transacao');
        $this->db->join('historico', 'historico.id = item_pedido.id_historico');
        $this->db->join('curso', 'historico.id_curso = curso.id_curso');
        $this->db->where('pedido.id_aluno', $this->session->userdata('imazon_id_fic'));
        $this->db->where('pedido.situacao', 3);
        $this->db->where('item_pedido.tipo_item > ', '3');
        $this->db->like('pedido.data_compra', '2016-');
        $this->db->order_by('pedido.data_compra asc');
        return $this->db->get();
    }

    public function get_by_aluno_declaracao_imazon($d = null) {
        $this->imazon->select('*');
        $this->imazon->from('aluno_curso');
        $this->imazon->join('curso', 'aluno_curso.id_curso = curso.id_curso');
        $this->imazon->where('aluno_curso.email', $this->session->userdata('imazon_id_fic'));
        $this->imazon->where('aluno_curso.situacao', 'Transação Concluída');
        $this->imazon->like('aluno_curso.data_compra', '2016-');
        $this->imazon->order_by('aluno_curso.data_compra asc');
        return $this->imazon->get();
    }

    public function get_by_aluno_declaracao_imazon10($d = null) {
        $this->imazon10->select('*');
        $this->imazon10->from('aluno_curso');
        $this->imazon10->join('curso', 'aluno_curso.id_curso = curso.id_curso');
        $this->imazon10->where('aluno_curso.email', $this->session->userdata('imazon_id_fic'));
        $this->imazon10->where('aluno_curso.situacao', 'Transação Concluída');
        $this->imazon10->like('aluno_curso.data_compra', '2016-');
        $this->imazon10->order_by('aluno_curso.data_compra asc');
        return $this->imazon10->get();
    }

    public function notifica_fatura_aberta() {
        return $this->db->get_where('pedido', array('id_aluno' => $this->session->userdata('imazon_id_fic'), 'situacao' => 0));
    }

    public function get_item_pedido_by_transacao($d = null) {
        $c = $this->db->select('*')
                ->from('item_pedido')
                ->join('pedido', 'item_pedido.transacao = pedido.transacao')
                ->join('produto', 'item_pedido.tipo_item = produto.id_produto')
                ->join('historico', 'item_pedido.id_historico = historico.id')
                ->join('curso', 'curso.id_curso = historico.id_curso')
                ->where('item_pedido.transacao', $d)
                ->get();

        if (count($c) >= 1) {
            return $c;
        } else {
            return false;
        }
    }
    
    public function get_historico_itens_transacao($d = null) {
        $c = $this->db->select('*')
                ->from('item_pedido')
                ->join('pedido', 'item_pedido.transacao = pedido.transacao')
                ->join('produto', 'item_pedido.tipo_item = produto.id_produto')
                ->join('historico', 'item_pedido.id_historico = historico.id')
                ->join('curso', 'curso.id_curso = historico.id_curso')
                ->where(array('item_pedido.transacao' => $d, 'historico.situacao'=>0))
                ->get();

        if (count($c) >= 1) {
            return $c;
        } else {
            return false;
        }
    }

    public function update_status_pedido($d = null) {
        $this->db->where('id_pedido', $d['id_pedido']);
        return $this->db->update('transacao', $d);
    }

    public function verifica_certificado_web($d = null) {
        $this->db->where('historico', $d);
        return $this->db->get('item_pedido', $d);
    }

    public function verifica_compra_segunda_chamada_historico($historico) {
        $pedidos = $this->db->select('transacao')->where('id_aluno', $this->session->userdata('imazon_id_fic'))->get('pedido')->result();
        $fatura = 0;
        if (count($pedidos) > 1) {
            foreach ($pedidos as $p) {
                $item_pedido = $this->db->where('transacao', $p->transacao)->where('id_historico', $historico)->get('item_pedido')->result();
                if (count($item_pedido) > 0) {
                    foreach ($item_pedido as $ip) {
                        if ($ip->tipo_item == 4) {
                            $fatura++;
                        }
                    }
                }
            }
            if ($fatura > 0) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

}
