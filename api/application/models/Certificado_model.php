<?php

class Certificado_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function add($certificado = null) {
        return $this->db->insert('certificado', $certificado);
    }

    public function get_certificados_by_historico($historico = null) {
        $this->db->from('produto');
        $this->db->join('certificado', 'produto.id_produto = certificado.id_tipo');
        $this->db->where('certificado.id_historico', $historico);
        return $this->db->get();
    }

    public function get_certificados_impresos_by_aluno() {
        return $this->db->from('produto')
                        ->select("id_certificado, curso.nome, certificado.endereco, certificado.situacao, pedido.data_alteracao, pedido.data_compra, produto.valor, produto.nome as nomeProduto, historico.id_historico, certificado.data_registro")
                        ->join('certificado', 'produto.id_produto = certificado.id_tipo')
                        ->join('historico', 'historico.id_historico = certificado.id_historico')
                        ->join('curso', 'curso.id_curso = historico.id_curso')
                        ->join('item_pedido', 'item_pedido.id_historico = historico.id_historico')
                        ->join('pedido', 'item_pedido.transacao = pedido.transacao')
                        ->where('historico.id_aluno', $this->session->userdata('imazon_id_fic'))
                        ->where('certificado.situacao', 1)
                        ->where('pedido.situacao', 3)
                        ->get();
    }

}
