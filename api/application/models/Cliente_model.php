<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Cliente_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        
        $this->db = $this->load->database('default', TRUE);
    }
    
    public function get_cliente($id_fic)
    {
        $this->db->select();
        $this->db->where('id_fic', $id_fic);
        
        return $this->db->get('idq_cliente')->result_array();
    }
    
    public function set_log_saldo_compra($id_cliente, $valor, $transacao)
    {
        $log_saldo = array('id_cliente'=>$id_cliente, 'data'=>date("Y-m-d H:i:s"), 'valor'=>$valor, 'transacao'=>$transacao );
        
        $this->db->insert('idq_log_saldo_compra', $log_saldo);
        return $insertId = $this->db->insert_id();
    }
    
    public function update_saldo_cliente($id_cliente, $saldo)
    {
        $this->db->set('saldo', $saldo);
        $this->db->where('id', $id_cliente);
        $this->db->update('idq_cliente');
    }
    
    public function is_instituicao($nome, $chave, $token, $situacao)
    {
        $this->db->select();
        $this->db->where('nome', $nome);
        $this->db->where('chave', $chave);
        $this->db->where('token', $token);
        $this->db->where('situacao', $situacao);
        
        $data_instituicao = $this->db->get('idq_instituicao')->result_array();
        
        if(count($data_instituicao)>0)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
    
    public function is_transacao($transacao)
    {
        $this->db->select('transacao');
        $this->db->where('id_aluno', $transacao);
        $transacao_pedido = $this->db->get('pedido')->result_array();
        
        if ( count($transacao_pedido)>0 )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    private function is_log_bonificacao($transacao)
    {
        $this->db->select('transacao');
        $this->db->where('transacao', $transacao);
        $transacao_bonificacao =  $this->db->get('idq_log_bonificacao')->result_array();
        
        if( count($transacao_bonificacao)>0 )
        {
           return true;
        }
        else
        {
           return false;
        }
    }
    
    public function set_bonificacao($transacao)
    {
        $transacao = 'IMAC93ea679e62';
        
        $is_log_bonificacao = $this->is_log_bonificacao($transacao);
        
        if( $is_log_bonificacao==false )
        {
            $this->db->select();
            $this->db->where('transacao', $transacao);
            $this->db->where('cod_interme !=', null);
            $pedido =  $this->db->get('pedido')->result_array();
            
            echo '<pre>';var_dump($pedido);
            die;
            
            $valor_transacao = $pedido[0]['valor'];

            if (count($pedido) == 1)
            {
                $bonificacao_cliente = $this->get_percentual_bonificacao_cliente($pedido[0]['id_aluno']);
                
                $percentual_bonificacao = $bonificacao_cliente[0]['percentual_bonificacao'];
                $percentual_bonificacao = floatval($percentual_bonificacao);

                $valor_transacao = floatval($valor_transacao);

                $bonificacao = $valor_transacao*($percentual_bonificacao/100);
                
                //$bonificacao = round($bonificacao, 2);

                if($pedido[0]['id_aluno'])
                {
                    $this->db->select();
                    $this->db->where('id_fic', $pedido[0]['id_aluno']);
                    $cliente =  $this->db->get('idq_cliente')->result_array();

                    if($cliente[0]['id'])
                    {
                        $this->db->select();
                        $this->db->where('id_fic', $cliente[0]['id_fic'] );
                        $aluno =  $this->db->get('aluno')->result_array();

                        if( $aluno[0]['email'])
                        {
                            $this->db->select();
                            $this->db->where('email_indicado', $aluno[0]['email'] );
                            $this->db->where('situacao', 1 );
                            $indicacao =  $this->db->get('idq_indicacao')->result_array();
                            
                            if($indicacao[0]['id_cliente'])
                            {
                                $this->db->select();
                                $this->db->where('id', $indicacao[0]['id_cliente']);
                                $this->db->where('situacao', 1);
                                $indicacao_cliente =  $this->db->get('idq_cliente')->result_array();
                            }
                        }
                    }
                }

               $id_cliente = $indicacao_cliente[0]['id'];
               $idq_log_bonificacao = array( 'id_cliente'=>$id_cliente, 'data'=>date("Y-m-d H:i:s"), 
                                                'valor'=>$bonificacao, 'previsao_resgate'=>date('Y-m-d H:i:s', strtotime(' + 20 days') ) ,'transacao'=>$transacao, 'tipo_log_bonificacao'=>0 ); 

                $this->db->insert('idq_log_bonificacao',$idq_log_bonificacao);
            }
            else
            {
                return false;
            }
        }
    }
    
    public function get_log_saldo_compra($id_transacao)
    {
        $this->db->select('id');
        $this->db->where('transacao', $id_transacao );
        $id =  $this->db->get('idq_log_saldo_compra')->result_array();
        
        return $id[0]['id'];
    }
    
    public function get_bonificacao_cliente($id_fic)
    {
        $this->db->select('saldo');
        $this->db->where('id_fic', $id_fic);
        return $this->db->get('idq_cliente')->result_array();
    }
}