<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ClienteApi extends CI_Controller
{
    public function __construct()
    {
	parent::__construct();
        $this->load->model("Cliente_model", "cl");
        
        $this->load->model("usuarios_model", "um");
        $this->load->model("produtos_model", "pm");

        $this->load->model("Historico_model", "hm");
        $this->load->model('pedido_model', 'prm');

        $this->load->library('cart');
    }
    
    private function auth_instituicao($nome, $chave, $token, $situacao)
    {
        return $this->cl->is_instituicao($nome, $chave, $token, $situacao);
    }
    
    public function auth()
    {
        $nome = 'imazoncursos';
        $chave = 'ehfgu9wflwojf0oklfrknjfihy7e9j2r';
        $token = 'fi3fhwiekfmfk3f454154f==';
        $situacao = 0;
        
        $id_fic_codificado = $this->input->post('idq_u');
        $valor_codificado = $this->input->post('idq_p');
        $cart = $this->input->post('cart');
        
        $id_fic = decodifica($id_fic_codificado);
        $valor = decodifica($valor_codificado);
        
        //$valor_formatado = number_format($valor, 2, ',', ' ');
        
        //($valor_formatado);
        //die;
        
        $dados_cliente = array('nome'=>$nome, 'chave'=>$chave,'tolken'=>$token, 'situacao'=>$situacao, 'id_fic'=>$id_fic, 'valor'=>$valor);
        $this->consome_saldo($dados_cliente, $cart);
    }

    private function consome_saldo($parametros, $cart)
    {
        $id_fic = $parametros['id_fic'];
        $valor = $parametros['valor'];
        
        $transacao = "IMAC" . substr(md5(microtime()), 0, 10); //gera id da transacao
        $transacao_existente = true;
        
        while ($transacao_existente)
        {
            $transacao_existente = $this->cl->is_transacao($transacao);
            
            if($transacao_existente==true)
            {
                $transacao = "IMAC" . substr(md5(microtime()), 0, 10);
            }
        }
        
        $auth_instituicao = $this->auth_instituicao($parametros['nome'], $parametros['chave'], $parametros['tolken'], $parametros['situacao']);
        
        if($auth_instituicao)
        {
            if($id_fic && $transacao && $valor)
            {
                $cliente = $this->cl->get_cliente($id_fic);

                if( count($cliente)>0 )
                {
                    $cliente_saldo = floatval($cliente[0]['saldo']);
                    
                    //$cliente_saldo = number_format($cliente_saldo, 2);
                    
                    if($cliente_saldo>=$valor)
                    {
                        $saldo_atual = $cliente_saldo- $valor;
                        
                        $id_log = $this->cl->set_log_saldo_compra($cliente[0]['id'], $valor, $transacao);
                        
                        if($id_log)
                        {
                            $this->cl->update_saldo_cliente($cliente[0]['id'], $saldo_atual);
                            
                            $this->redirect_pagamento_imazon($id_fic, $valor, $transacao, $cart);
                            $this->retorno_tray_imazon($transacao);
                            echo 'sucesso';
                        }
                        else
                        {
                            echo "falha";
                        }
                    }
                    else
                    {
                        echo 'saldo_insuficiente';
                    }
                }
                else
                {
                    echo 'client_inexistente';
                }
            }
            else
            {
                echo 'Falta Parametros';
            }
        }
    }
    
    private function redirect_pagamento_imazon($id_fic, $valor, $transacao, $cart)
    {
        $url = str_replace('api/','',base_url());
        $url = $url."site/gerarPagamentoSaldo";
        
        $parametros_pagamento = array('id_fic'=>$id_fic, 'valor'=>$valor,'transacao'=>$transacao, 'cart'=>$cart);
        
                            $cr = curl_init();
                            curl_setopt($cr, CURLOPT_URL, $url);
                            curl_setopt($cr, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($cr, CURLOPT_POST, TRUE);
                            curl_setopt($cr, CURLOPT_POSTFIELDS, http_build_query($parametros_pagamento) );
                            curl_exec($cr);
                            curl_close($cr);
    }
    
    private function retorno_tray_imazon($pedido)
    {
        $url = str_replace('api/','',base_url());
        $url = $url.'Retornotray/retorno_tray_indique_ganhe';
        
        $parametro_retorno = array('pedido'=>$pedido);
                            $cr = curl_init();
                            curl_setopt($cr, CURLOPT_URL, $url);
                            curl_setopt($cr, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($cr, CURLOPT_POST, TRUE);
                            curl_setopt($cr, CURLOPT_POSTFIELDS, http_build_query($parametro_retorno) );
                            curl_exec($cr);
                            curl_close($cr);
    }
    
    public function get_bonificacao_cliente()
    {
        $id_fic = $this->input->post('id_fic');
        
        if($id_fic)
        {
            $saldo = $this->cl->get_bonificacao_cliente($id_fic);
            if( isset($saldo[0]['saldo']) )
            {
                echo $saldo[0]['saldo'];
            }
            else
            {
                echo '0,00';
            }
        }
        else
        {
            return null;
        }
    }
}