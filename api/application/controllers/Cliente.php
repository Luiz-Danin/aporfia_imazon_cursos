<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cliente extends CI_Controller
{
    public function __construct()
    {
	parent::__construct();
        $this->load->model("Cliente_model", "cl");
    }
    
    public function index()
    {
        $this->load->view('api_ganhe');
    }
    
    public function gerarPagamentoSaldo()
    {
        $id_fic = $this->input->post('idq_u');
        $valor = $this->input->post('idq_p');
        
        $id_transacao = "IMAC" . substr(md5(microtime()), 0, 10); //gera id da transacao
        
        $transacao_existente = true;
        
        while ($transacao_existente)
        {
            $transacao_existente = $this->cl->is_transacao($id_transacao);
            
            if($transacao_existente==true)
            {
                $id_transacao = "IMAC" . substr(md5(microtime()), 0, 10);
            }
        }
        
        $transacao = $this->prm->create(array('transacao' => $id_transacao,
                    'id_aluno' => $this->session->userdata('imazon_id_fic'),
                    'data_compra' => date('Y-m-d h:i:s'),
                    'tipo_pagamento' => 0,
                    'situacao' => 0,
                    'valor' => $total
                ));
                
        if (!$transacao)
        {
            redirect(base_url('finalizarPedido'));
        }
        
        $this->load->model('base_legal_model', 'blm');
        $this->load->model('historico_model', 'hm');
        $this->load->model('certificado_model', 'certfm');
        $r = $this->prm->get_by_transacao($id_transacao)->result();
        $base_legal = $this->blm->get_atual()->result();
                
          if (count($r) == 1)
          {
              foreach ($this->cart->contents() as $c)
              {
                        $prod = $this->pm->get_by_nome_produto(strtolower(trim($c['tipo'])));
                        if ($c['tipo'] == "CI")
                        {
                            //insere itens do carrinho na transação
                            $insereItemPedido = $this->prm->insert_item_pedido(array('transacao' => $id_transacao,
                                'id_historico' => $c['historico'],
                                'tipo_item' => $c['id_produto'],
                                'valor' => $c['price']
                                    )
                            );

                            if (!$insereItemPedido) {
                                redirect('finalizarPedido');
                            }

                        }
                        else if ($c['tipo'] == "SC")
                        {

                            //insere itens do carrinho na transação

                            $insereItemPedido = $this->prm->insert_item_pedido(array('transacao' => $id_transacao,
                                'id_historico' => $c['historico'],
                                'tipo_item' => $c['id_produto'],
                                'valor' => $c['price']
                                    )
                            );

                            if (!$insereItemPedido) {

                                redirect('finalizarPedido');
                            }
                        }
                        else
                        {
                            //insere itens do carrinho na transação
                            $this->prm->insert_item_pedido(array('transacao' => $id_transacao,
                                'id_historico' => $c['historico'],
                                'tipo_item' => $c['id_produto'],
                                'valor' => $c['price']
                                    )
                            );
                        }
                    }
                    
                    //Ação intermediador
                    $this->load->model('pedido_model', 'pedidom');
                    $dt = array('cod_interme' => NULL);
                    $this->pedidom->atualiza_pedido($idtransacao, $dt);
          }       
        
    }
    
    public function decrement_saldo()
    {
        $id_fic = $this->input->post('id_fic', true);
        $transacao = $this->input->post('transacao', true);
        $valor = $this->input->post('valor', true);
        
        $parametros = ['id_fic'=>$id_fic, 'transacao'=>$transacao, 'valor'=>$valor];
        
        $cr = curl_init();
        curl_setopt($cr, CURLOPT_URL, "https://aporfia.com.br/ganhe/api/ClienteApi/consome_saldo");
        curl_setopt($cr, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($cr, CURLOPT_POST, TRUE);
        curl_setopt($cr, CURLOPT_POSTFIELDS, $parametros);
        $retorno = curl_exec($cr);
        curl_close($cr);
        
        echo $retorno;
        if($retorno==='sucesso')
        {
            $this->gerarPagamentoSaldo();
        }
    
        die;
    }
}