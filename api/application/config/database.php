<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$active_group = 'default';
$query_builder = TRUE;

$db['default'] = array(

    'dsn'	=> '',
	'hostname' => 'localhost',
	'username' => 'imazoncursos_apl',
	'password' => '#IC4-20B3!',
	'database' => 'imazoncursos',
	'dbdriver' => 'mysqli',
	'dbprefix' => '',
	'pconnect' => FALSE,
	'db_debug' => TRUE,
	'cache_on' => FALSE,
	'cachedir' => '',
	'char_set' => 'utf8',
	'dbcollat' => 'utf8_general_ci',
	'swap_pre' => '',
	'encrypt' => FALSE,
	'compress' => FALSE,
	'stricton' => FALSE,
	'failover' => array(),
	'save_queries' => TRUE
);

$db['imazon'] = array(
	'dsn'	=> '',
	'hostname' => 'localhost',
	'username' => 'imazon_apl',
	'password' => '#B3-20Im4!',
	'database' => 'imazon',
	'dbdriver' => 'mysqli',
	'dbprefix' => '',
	'pconnect' => FALSE,
	'db_debug' => TRUE,
	'cache_on' => FALSE,
	'cachedir' => '',
	'char_set' => 'utf8',
	'dbcollat' => 'utf8_general_ci',
	'swap_pre' => '',
	'encrypt' => FALSE,
	'compress' => FALSE,
	'stricton' => FALSE,
	'failover' => array(),
	'save_queries' => TRUE,
        'autoinit'=>TRUE
);

$db['imazon10'] = array(
	'dsn'	=> '',
	'hostname' => 'localhost',
	'username' => 'imazon10_apl',
	'password' => '#Im4-10B3!',
	'database' => 'imazon10',
	'dbdriver' => 'mysqli',
	'dbprefix' => '',
	'pconnect' => TRUE ,
	'db_debug' => TRUE,
	'cache_on' => FALSE,
	'cachedir' => '',
	'char_set' => 'utf8',
	'dbcollat' => 'utf8_general_ci',
	'swap_pre' => '',
	'encrypt' => FALSE,
	'compress' => FALSE,
	'stricton' => FALSE,
	'failover' => array(),
	'save_queries' => TRUE,
        'autoinit'=>TRUE
);

$db['vip'] = array(
	'dsn'	=> '',
	'hostname' => 'localhost',
	'username' => 'imazonvip_apl',
	'password' => '#V1p-3BD20!',
	'database' => 'imazonvip',
	'dbdriver' => 'mysqli',
	'dbprefix' => '',
	'pconnect' => FALSE,
	'db_debug' => TRUE,
	'cache_on' => FALSE,
	'cachedir' => '',
	'char_set' => 'utf8',
	'dbcollat' => 'utf8_general_ci',
	'swap_pre' => '',
	'encrypt' => FALSE,
	'compress' => FALSE,
	'stricton' => FALSE,
	'failover' => array(),
	'save_queries' => TRUE
);

//$db['default']['hostname'] = 'localhost';
//$db['default']['username'] = 'imazon_apl';
//$db['default']['password'] = '';
//$db['default']['database'] = 'imazoncursos';
//$db['default']['dbdriver'] = 'mysqli';
//$db['default']['dbprefix'] = '';
//$db['default']['pconnect'] = FALSE;
//$db['default']['db_debug'] = TRUE;
//$db['default']['autoinit'] = FALSE;
//$db['default']['cache_on'] = FALSE;
//$db['default']['cachedir'] = '';
//$db['default']['char_set'] = 'utf8';
//$db['default']['dbcollat'] = 'utf8_general_ci';
//
//$db['imazon']['hostname'] = 'localhost';
//$db['imazon']['username'] = 'imazon_apl';
//$db['imazon']['password'] = '';
//$db['imazon']['database'] = 'imazon';
//$db['imazon']['dbdriver'] = 'mysqli';
//$db['imazon']['dbprefix'] = '';
//$db['imazon']['pconnect'] = FALSE;
//$db['imazon']['db_debug'] = TRUE;
//$db['imazon']['autoinit'] = FALSE;
//$db['imazon']['cache_on'] = FALSE;
//$db['imazon']['cachedir'] = '';
//$db['imazon']['char_set'] = 'utf8';
//$db['imazon']['dbcollat'] = 'utf8_general_ci';
//
//
//$db['imazon10']['hostname'] = 'localhost';
//$db['imazon10']['username'] = 'imazon_apl';
//$db['imazon10']['password'] = '';
//$db['imazon10']['database'] = 'imazon10';
//$db['imazon10']['dbdriver'] = 'mysqli';
//$db['imazon10']['dbprefix'] = '';
//$db['imazon10']['pconnect'] = FALSE;
//$db['imazon10']['db_debug'] = TRUE;
//$db['imazon10']['autoinit'] = FALSE;
//$db['imazon10']['cache_on'] = FALSE;
//$db['imazon10']['cachedir'] = '';
//$db['imazon10']['char_set'] = 'utf8';
//$db['imazon10']['dbcollat'] = 'utf8_general_ci';
//
//$db['vip']['hostname'] = 'localhost';
//$db['vip']['username'] = 'imazon_apl';
//$db['vip']['password'] = '';
//$db['vip']['database'] = 'imazonvip';
//$db['vip']['dbdriver'] = 'mysqli';
//$db['vip']['dbprefix'] = '';
//$db['vip']['pconnect'] = FALSE;
//$db['vip']['db_debug'] = TRUE;
//$db['vip']['autoinit'] = FALSE;
//$db['vip']['cache_on'] = FALSE;
//$db['vip']['cachedir'] = '';
//$db['vip']['char_set'] = 'utf8';
//$db['vip']['dbcollat'] = 'utf8_general_ci';


?>