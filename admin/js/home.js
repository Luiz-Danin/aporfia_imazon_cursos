function ajax_validar_certificado(url)
{
	document.getElementById('botao_verifica').value = 'Verificando...';
	document.getElementById('botao_verifica').disabled = 1;
	req = null;
	if (window.XMLHttpRequest) {
		req = new XMLHttpRequest();
		req.onreadystatechange = processReqChange2;
		req.open("GET",url,true);
		req.send(null);
	} else if (window.ActiveXObject) {
		req = new ActiveXObject("Microsoft.XMLHTTP");
		if (req) {
			req.onreadystatechange = processReqChange2;
			req.open("GET",url,true);
			req.send();
		}
	}
}
	
function processReqChange2()
{
	if (req.readyState == 4) {
		if (req.status ==200) {
			alert(req.responseText);
			document.getElementById('botao_verifica').value = 'Verificar';
			document.getElementById('botao_verifica').disabled = 0;
		} else {
			alert("Houve um problema ao obter os dados:n" + req.statusText);
		}
	}
}

function validar_certificado(valor)
{
	valor = document.getElementById('numero').value;
	url="<?php echo base_url();?>site/ajax_validar_certificado/"+valor;
	ajax_validar_certificado(url);
	
}

setFocus('filter');