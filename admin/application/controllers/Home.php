<?php

class Home extends CI_Controller{
    
    public function __construct() {
        parent::__construct();
        session_start();
        $this->load->helper('seguranca');
        
        auth();
        $this->load->view('adm/inc/header');
        
        $this->seguranca();
    }
    
    public function seguranca(){
        if(!isset($_SESSION['nome_facilitador']) && !isset($_SESSION['email_facilitador']) && !isset($_SESSION['login']) ){
            session_destroy();
            redirect('login');
        }
    }
    
    public function index(){
        redirect('assinaturas');
        //$this->load->view('adm/tela/home');
	//	$this->load->view('adm/inc/footer');
    }
    
}
