<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class imazon extends CI_Controller {

    public function __construct() {
        parent::__construct();
        session_start();
        $this->seguranca();
        $this->load->model("cursos_model", "cm");
        $this->load->model("produtos_model", "prodm");
        $this->load->model('base_legal_model', 'bm');
        if ($this->uri->segment(3) != "ajax_altera_imagem_curso" && $this->uri->segment(3) != "ajax_altera_status_publicado" && $this->uri->segment(3) != "ajax_autocomplete_busca_curso"  && $this->uri->segment(3) != "ajax_adiciona_curso_aluno") {
            $this->load->view('adm/inc/header');
        }
        $this->load->library('pagination');
        $this->load->library('form_validation');
    }

    private function seguranca() {
        if (!isset($_SESSION['nome_facilitador']) && !isset($_SESSION['email_facilitador']) && !isset($_SESSION['login'])) {
            session_start();
            session_destroy();
            redirect('login');
        }
    }

    public function index() {
        $inicio = (!$this->uri->segment("4")) ? 0 : $this->uri->segment("4");
        $config['base_url'] = base_url() . 'cursos/imazon/index';
        $config['per_page'] = 15;
        $config['uri_segment'] = 4;
        $form = $this->form_validation->set_rules('buscar', 'busca', 'required|min_length');
        if (null !== $this->input->post('buscar') && strlen(trim($this->input->post('buscar'))) >= 2 && $form == TRUE) {
            $config['total_rows'] = $this->cm->count_all_cursos_imazon_adm(trim($this->input->post('buscar')));
            $data['cursos'] = $this->cm->get_all_cursos_imazon_adm(trim($this->input->post('buscar')), $inicio, $config['per_page'])->result();
        } else {
            $config['total_rows'] = $this->cm->count_all_cursos_imazon_adm();
            $data['cursos'] = $this->cm->get_all_cursos_imazon_adm(null, $inicio, $config['per_page'])->result();
        }
        $this->pagination->initialize($config);
        $data['paginacao'] = $this->pagination->create_links();
        $this->load->view('adm/tela/cursos_imazon', $data);
        $this->load->view('adm/inc/footer');
    }

    public function editar() {
        if ($this->uri->segment(4) !== null) {
            $curso = decodifica($this->uri->segment(4));
        }
        $data['curso']    = $this->cm->get_curso_imazon_by_id($curso)->result();
        $data['questoes'] = $this->cm->get_questoes_curso_imazon_by_id($curso)->result();
        $this->load->view('adm/tela/edit/cursos_imazon', $data);
        $this->load->view('adm/inc/footer');
    }

    //CRUD
    public function add(){
        if($this->cm->add_curso_imazon($this->input->post())){
            $curso = $this->cm->get_ultimo_curso_imazon()->result();
            redirect("cursos/imazon/editar/".codifica($curso[0]->id_curso));
        }else{
            $_SESSION['msg'] = "Curso n&atilde;o cadastrado";
            redirect("cursos/imazon");
        }
        
    }
    
    public function atualizar() {
        if ($this->uri->segment(4) !== null && $this->uri->segment(5) !== null) {
            $status = decodifica($this->uri->segment(5));
            $id_curso = decodifica($this->uri->segment(4));
            $this->cm->atualiza_dados_curso_imazon($id_curso, array('status' => $status));
            redirect('cursos/imazon/index/');
        }
    }

    public function edit() {
        $dados = array("nome"=>$this->input->post("nome"), "questionario"=>$this->input->post("questionario"), "descricao"=>$this->input->post("descricao"));
        $this->cm->atualiza_dados_curso_imazon(decodifica($this->input->post('id_curso')), $dados);
        redirect('cursos/imazon/editar/'.$this->input->post("id_curso"));
    }

    public function del_questao_curso(){
        if($this->uri->segment(4)){
           $questao = decodifica($this->uri->segment(4));
           if($this->cm->remove_questao_curso_imazon($questao)){
               $_SESSION['msg'] = "Excluida com sucesso";
           }else{
               $_SESSION['msg'] = "Erro ao excluir questão";
           }
        }
        redirect("cursos/imazon/editar/".$this->uri->segment(5));
    }
    
    public function atualiza_questao_prova(){
        $dados = array("resposta"      => $this->input->post("resposta"), 
                       "id_avaliacao"  => $this->input->post("id_avaliacao"),
                       "justificativa" => $this->input->post("justificativa"),
                       "descricao"     => $this->input->post("descricao"),
                       );
        $questao = decodifica($this->input->post("id_questao"));
        if($this->cm->atualiza_dados_questao_curso_imazon($questao, $dados)){
            $_SESSION['msg'] = "Alterado com sucesso";
        }else{
            $_SESSION['msg'] = "Erro ao alterar questão";
        }
        redirect("cursos/imazon/editar/".$this->input->post("curso"));
    }
    
    public function ajax_altera_status_publicado(){
        $dados = array("status"=>$this->input->post("status"));
        if($this->cm->atualiza_dados_curso_imazon(decodifica($this->input->post('id_curso')), $dados)){
            echo 'ok';
        }else{
            echo 'erro';
        }
    }
    
    public function ajax_altera_imagem_curso(){
        
        if($_FILES['img-nova']['name']!==null){
            $diretorio =  $_SERVER['DOCUMENT_ROOT']."/imazoncursos/imgs/cursos";
            //echo $diretorio.$_FILES['img-nova']['name'];
            $ext = pathinfo($_FILES['img-nova']['name'], PATHINFO_EXTENSION);
            if($ext == "jpg" ||$ext == "jpeg" ||$ext == "png"){
                $tamanho_imagem = $_FILES['img-nova']['size'];
                $nome_arquivo   = md5($_FILES['img-nova']['name'].  microtime()).'.'.$ext;
                $tamanho = round($tamanho_imagem / 1024);
                if($tamanho < 3062){ //se imagem for até 1MB envia
                    if(move_uploaded_file($_FILES['img-nova']['tmp_name'], $diretorio.'/'.$nome_arquivo)){
                        $dados = array("imagem"=>$nome_arquivo);
                        if($this->cm->atualiza_dados_curso_imazon(decodifica($this->input->post('id_curso')), $dados)){
                            echo 'ok';
                        }else{
                            echo "Imagem não alterada por problemas no servidor.";
                        }
                    }else{
                        echo "Imagem não alterada por problemas no servidor.";
                    }
                }else{
                    echo "Tamanho de imagem não permitido.";
                }
            }else{
                echo "Formato de imagem não permitido.";
            }
        }
        
        /*$dados = array("status"=>$this->input->post("status"));
        if($this->cm->atualiza_dados_curso_imazon(decodifica($this->input->post('id_curso')), $dados)){
            echo 'ok';
        }else{
            echo 'erro';
        }*/
    }
    
    public function ajax_adiciona_curso_aluno(){
        $this->form_validation->set_rules('carga_horaria', 'carga_horaria', 'required');
        $this->form_validation->set_rules('id_fic', 'id_fic', 'required');
        $this->form_validation->set_rules('curso', 'curso', 'required');
        if($this->form_validation->run()){
            $base_legal = $this->bm->get_base_legal_atual();
            
                $hash = date('Y') . '-' . strtoupper(substr(md5($this->input->post("id_fic") . ':' . microtime() . '-' . microtime()), 0, 4) . '-' . substr(md5($this->input->post("id_fic") . ':' . microtime()), 0, 4));
                $autenticacao = strtoupper(substr(md5($hash), 0, 8));
                $autenticacao = date('Y') . '-' . substr($autenticacao, 0, 4) . '-' . substr($autenticacao, 4, 9);
                $idHistoricNovo = $hash;
                
                $carga_horaria = $this->prodm->get_carga_horaria($this->input->post("carga_horaria"));
                
                if (count($carga_horaria) == 1) {
                    $this->load->model("usuarios_model", "um");
                    $usuario = $this->um->get_by_id_fic($this->input->post("id_fic"))->result();
                    if(count($usuario)==1){
                        $historico = array('id_historico' => $idHistoricNovo, 'id_aluno' => $usuario[0]->id_fic,
                        'data_aprovacao' => '0000-00-00', 'hora_aprovacao' => '00:00:00', 'nota' => 0,
                        'email' => $usuario[0]->email, 'id_avaliacao' => 1, 'id_curso' => $this->input->post("curso"),
                        'primeiro_dia' => date('Y-m-d'), 'primeira_hora' => date('h:m:s'), 
                        'carga_horaria' => $carga_horaria[0]->tempo,
                        'id_baselegal' => $base_legal[0]->id_base_lega, 'situacao' => 1, 'codigo_autenticacao' => $autenticacao);
 
                        if($this->cm->add_curso_aluno_imazon($historico)){
                            $resp = array("resposta"=>"ok");
                        }else{
                            $resp= null;
                        }
                    }else{
                        $resp = null;
                    }                     
                }else{
                    $resp= null;
                }
              
        }else{
            $resp= null;
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($resp));
    }
    
    public function ajax_autocomplete_busca_curso(){
        $this->form_validation->set_rules('nome', 'nome', 'required|min_lenght[3]');
        if($this->form_validation->run()){
            $cursos = $this->cm->get_autocomplete_curso_imazon_by_nome($this->input->post('nome'))->result();
            if(count($cursos)==0){
                $cursos = null;
            }
        }else{
            $cursos = null;
        }
        
        $this->output->set_content_type('application/json')->set_output(json_encode($cursos));
    }
    
    public function add_questao_curso(){
        
        $this->form_validation->set_rules('descricao', 'descricao', 'required');
        if($this->form_validation->run()){
            $questao = array("descricao"=>$this->input->post("descricao"), "justificativa"=>$this->input->post("justificativa"), "resposta"=>$this->input->post("resposta"), "id_avaliacao"=>$this->input->post("id_avaliacao"), "id_curso"=>decodifica($this->input->post("id_curso")));
            if($this->cm->add_questao_curso_imazon($questao)){
                $_SESSION["msg"] = "Questao adicionada com sucesso";
            }else{
                $_SESSION["msg"] = "Erro ao adicionar questao";
            }
        }else{
            $_SESSION["msg"] = "Erro ao adicionar questao. Dados incorretos";
        }
        redirect("cursos/imazon/editar/".$this->input->post("id_curso"));
        
    }
    
}
