<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Vip extends CI_Controller {

    public function __construct() {
        parent::__construct();
        session_start();
        $this->load->helper('seguranca');
        
        auth();
        $this->seguranca();

        if ($this->uri->segment(4) != "ajax_altera_status_publicado") {
            $this->load->view('adm/inc/header');
        }
        $this->load->library('pagination');
        $this->load->library('form_validation');
        $this->load->model('cursos_model', 'cm');
        $this->load->model('aulas_model', 'am');
    }

    private function seguranca() {
        if (!isset($_SESSION['nome_facilitador']) && !isset($_SESSION['email_facilitador']) && !isset($_SESSION['login'])) {
            session_start();
            session_destroy();
            redirect('login');
        }
    }

    public function index() {
        $inicio = (!$this->uri->segment("4")) ? 0 : $this->uri->segment("4");
        $config['base_url'] = base_url() . 'cursos/vip/index';
        $config['per_page'] = 15;
        $config['uri_segment'] = 4;
        if (null !== $this->input->post('buscar') && strlen($this->input->post('buscar')) >= 2) {
            $form = $this->form_validation->set_rules('buscar', 'busca', 'required|min_length');
            if (null !== $this->input->post('buscar') && $form == TRUE) {
                $config['total_rows'] = $this->cm->count_all($this->input->post('buscar'));
            }
        } else {
            $config['total_rows'] = $this->cm->count_all();
        }
        //busca dados do curso
        if (null !== $this->input->post('buscar') && strlen($this->input->post('buscar')) >= 2 && $form == TRUE) {
            $data['dados'] = $this->cm->get_all($inicio, $config['per_page'], $this->input->post('buscar'))->result();
        } else {
            $data['dados'] = $this->cm->get_all($inicio, $config['per_page'])->result();
        }
        $this->pagination->initialize($config);
        $data['paginacao'] = $this->pagination->create_links();
        $this->load->view('adm/tela/cursos', $data);
        $this->load->view('adm/inc/footer');
    }

    public function editar() {
        if ($this->uri->segment(4)) {
            $data['aulas'] = $this->cm->get_all_aulas_curso(decodifica($this->uri->segment(4)));
            $data['proximo'] = $this->cm->get_by_proximo_curso(decodifica($this->uri->segment(4)))->result();
            $data['anterior'] = $this->cm->get_by_curso_anterior(decodifica($this->uri->segment(4)))->result();
            $data['dados']  = $this->cm->get_by_id(decodifica($this->uri->segment(4)))->result();
            $data['imagem'] = $this->cm->get_imagens(decodifica($this->uri->segment(4)))->result(); 
            $data['videos'] = $this->cm->get_videos(decodifica($this->uri->segment(4)))->result();
            $data['audios'] = $this->cm->get_audios(decodifica($this->uri->segment(4)))->result();
            $data['arquivos'] = $this->cm->get_arquivos(decodifica($this->uri->segment(4)))->result();
            if (count($data['dados']) == 1) {
                $this->load->view('adm/tela/edit/cursos', $data);
                $this->load->view('adm/inc/footer');
            } else {
                redirect('cursos/vip');
            }
        } else {
            redirect('cursos/vip');
        }
    }

    public function criar_aula() {
        $id_curso = decodifica($this->uri->segment(4));
        if ($id_curso) {
            if($this->am->add_aula_curso($id_curso)){
                $aula = $this->am->ultima_aula_criada($id_curso)->result();
                redirect('cursos/vip/ver_aula/'.codifica($aula[0]->cursos_aulas_id).'/'.$id_curso);
            } else {
                redirect('cursos/vip');
            }
        } else {
            redirect('cursos/vip');
        }
    }

    public function apagar_aula(){
        $id_aula  = decodifica($this->uri->segment(4));
        $id_curso = decodifica($this->uri->segment(5));
        
        if($this->am->delete_aula($id_aula)){
            $_SESSION['msg'] = "Aula deletada com sucesso!";
        }else{
            $_SESSION['msg'] = "Erro ao excluir aula";
        }
        
        redirect('cursos/vip/editar/'.codifica($id_curso));
    }
    
    public function ver_aula() {
        if ($this->uri->segment(4)) {
            $data['aula'] = $this->cm->get_aula_by_id(decodifica($this->uri->segment(4)))->result();
       
            $data['arquivos_aula'] = $this->am->get_arquivos_aula(decodifica($this->uri->segment(4)));
            foreach ($data['arquivos_aula'] as $aa)
                ;
            $data['imagens_aula'] = $this->am->get_imagens_aula(decodifica($this->uri->segment(4)));
            foreach ($data['imagens_aula'] as $ia)
                ;
            $data['videos_aula'] = $this->am->get_videos_aula(decodifica($this->uri->segment(4)));
            foreach ($data['videos_aula'] as $va)
                ;
            $data['exercicios_aula'] = $this->am->get_exercicios_aula(decodifica($this->uri->segment(4)));
            foreach ($data['exercicios_aula'] as $ea)
                ;
            if (count($data['aula']) == 1) {
                $this->load->view('adm/tela/edit/aula', $data);
                $this->load->view('adm/inc/footer');
            } else {
                redirect('cursos/vip');
            }
        } else {
            redirect('cursos/vip');
        }
    }

    public function mais_visitados() {
        $inicio = (!$this->uri->segment("4")) ? 0 : $this->uri->segment("4");
        $config['base_url'] = base_url() . 'cursos/vip/mais_visitados';
        $config['per_page'] = 12;
        $config['uri_segment'] = 4;
        $config['total_rows'] = count($this->cm->mais_visitados()->result());
        $data['dados'] = $this->cm->mais_visitados($inicio, $config['per_page'])->result();
        $this->pagination->initialize($config);
        $data['paginacao'] = $this->pagination->create_links();
        $this->load->view('adm/tela/cursos_mais_visitados', $data);
        $this->load->view('adm/inc/footer');
    }

    public function nao_procurados() {
        $inicio = (!$this->uri->segment("4")) ? 0 : $this->uri->segment("4");
        $config['base_url'] = base_url() . 'cursos/vip/nao_procurados';
        $config['per_page'] = 12;
        $config['uri_segment'] = 4;
        $config['total_rows'] = count($this->cm->nao_procurados()->result());
        $data['dados'] = $this->cm->nao_procurados($inicio, $config['per_page'])->result();
        $this->pagination->initialize($config);
        $data['paginacao'] = $this->pagination->create_links();
        $this->load->view('adm/tela/cursos_nao_procurados', $data);
        $this->load->view('adm/inc/footer');
    }

    public function mais_vendidos() {
        $inicio = (!$this->uri->segment("4")) ? 0 : $this->uri->segment("4");
        $config['base_url'] = base_url() . 'cursos/vip/mais_visitados';
        $config['per_page'] = 12;
        $config['uri_segment'] = 4;
        $config['total_rows'] = count($this->cm->mais_vendidos()->result());
        $data['dados'] = $this->cm->mais_vendidos($inicio, $config['per_page'])->result();
        $this->pagination->initialize($config);
        $data['paginacao'] = $this->pagination->create_links();
        $this->load->view('adm/tela/cursos_mais_vendidos', $data);
        $this->load->view('adm/inc/footer');
    }

    //CRUD
    public function atualizar() {

        if ($this->uri->segment(4) !== null && $this->uri->segment(5) !== null) {
            $categoria = $this->uri->segment(5);
            $operacao = $this->uri->segment(4);
            switch ($operacao) {
                case "publicado": $operacao = "CPublicado";
                    break;

                case "situacao": $operacao = "situacao";
                    break;

                default: redirect('cursos/vip');
                    break;
            }
            if ($operacao == "CPublicado") {
                $status = $this->cm->get_status2($categoria, $operacao)->result();
            } else {
                $status = $this->cm->get_status($categoria, $operacao)->result();
            }
            if ($operacao != "situacao") {
                if ($status[0]->$operacao === "Nao") {
                    $status = "Sim";
                } else {
                    $status = "Nao";
                }
            } else {
                if ($status[0]->$operacao === "Incompleto") {
                    $status = "Completo";
                } else {
                    $status = "Incompleto";
                }
            }
            if ($operacao == "CPublicado") {
                $dados = array($operacao => $status, "cedoc_doc_id_fk" => $categoria);
                $this->cm->edit_by_id2($dados);
            } else {
                $dados = array($operacao => $status, "cedoc_doc_id" => $categoria);
                $this->cm->edit_by_id($dados);
            }
            redirect('cursos/vip/index/' . $this->uri->segment(6));
        }
    }

    public function add() {
        $dados = elements(array('CCSubCat', 'cursos_cat_id', 'CCDescricaoCat', 'CCMostrar', 'CCDestaque', 'CCDisponivel'), $this->input->post());
        $this->cm->add($dados);
        redirect('cursos/vip');
    }

    public function edit() {
        $dados = elements(array('nome', 'objetivo3', 'descricao'), $this->input->post());
        $dados['cedoc_doc_id'] = decodifica($this->input->post('cedoc_doc_id'));
        $this->cm->edit_by_id($dados);
        redirect('cursos/vip/editar/' . codifica($dados['cedoc_doc_id']));
    }

    public function editAula() {
        $dados = elements(array('CATitulo', 'CADescricao', 'cursos_aulas_id'), $this->input->post());
        $dados['cursos_aulas_id'] = decodifica($dados['cursos_aulas_id']);
        $this->cm->edit_aula_by_id($dados);
        redirect('cursos/vip/ver_aula/' . codifica($dados['cursos_aulas_id']));
    }

    public function ajax_altera_status_publicado() {
        if ($this->input->post("status") == "ativar") {
            $dados = array("CPublicado" => "Sim", "cedoc_doc_id_fk" => decodifica($this->input->post("id")));
        } else {
            $dados = array("CPublicado" => "Nao", "cedoc_doc_id_fk" => decodifica($this->input->post("id")));
        }
        if ($this->cm->altera_status_publicado($dados)) {
            return true;
        } else {
            return false;
        }
    }
    
    public function ajax_altera_img_curso() {
        if ($this->input->post()) {
            print_r($_REQUEST);
        }
    }

    public function del() {
        if ($this->uri->segment(4)) {
            $this->cm->del(decodifica($this->uri->segment(4)));
            redirect('cursos/vip');
        } else {
            redirect('cursos/vip');
        }
    }

}
