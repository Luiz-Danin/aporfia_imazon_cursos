<?php

class categorias extends CI_Controller {

    public function __construct() {
        parent::__construct();
        session_start();
        $this->seguranca();
        $this->load->model('categorias_model', 'cm');
        $this->load->view('adm/inc/header');
    }

    private function seguranca() {
        if (!isset($_SESSION['nome_facilitador']) && !isset($_SESSION['email_facilitador']) && !isset($_SESSION['login'])) {
            redirect('login');
        }
    }

    public function index() {
        $data['dados'] = $this->cm->get_all()->result();
        $this->load->view('adm/tela/categorias', $data);
        $this->load->view('adm/inc/footer');
    }

    public function editar() {
        if ($this->uri->segment(3)) {
            $data['dados'] = $this->cm->get_by_id(decodifica($this->uri->segment(3)))->result();
            if (count($data['dados']) > 0) {
                $this->load->view('adm/tela/edit/categorias', $data);
                $this->load->view('adm/inc/footer');
            } else {
                redirect('categorias');
            }
        } else {
            redirect('categorias');
        }
    }

    public function atualizar() {

        if ($this->uri->segment(3) !== null && $this->uri->segment(4) !== null) {
            $categoria = $this->uri->segment(4);
            $operacao = $this->uri->segment(3);
            switch ($operacao) {
                case "mostrar": $operacao = "CCMostrar";
                    break;

                case "destaque": $operacao = "CCDestaque";
                    break;

                case "disponivel": $operacao = "CCDisponivel";
                    break;

                default: redirect('categorias');
                    break;
            }
            $status = $this->cm->get_status($categoria, $operacao)->result();
            if ($status[0]->$operacao === "Nao") {
                $status = "Sim";
            } else {
                $status = "Nao";
            }
            $dados = array($operacao => $status, "cursos_cat_id" => $categoria);
            $this->cm->edit_by_id($dados);
            redirect('categorias');
        }
    }

    public function add() {
        $dados = elements(array('CCSubCat', 'cursos_cat_id', 'CCDescricaoCat', 'CCMostrar', 'CCDestaque', 'CCDisponivel'), $this->input->post());
        $this->cm->add($dados);
        redirect('categorias');
    }

    public function edit() {
        $dados = elements(array('CCSubCat', 'cursos_cat_id', 'CCDescricaoCat'), $this->input->post());
        $dados['cursos_cat_id'] = decodifica($dados['cursos_cat_id']);
        $this->cm->edit_by_id($dados);
        redirect('categorias/editar/' . codifica($dados['cursos_cat_id']));
    }

    public function del() {
        if ($this->uri->segment(3)) {
            $this->cm->del(decodifica($this->uri->segment(3)));
            redirect('categorias');
        } else {
            redirect('categorias');
        }
    }

}
