<?php
class Acessos extends CI_Controller{

    public function __construct() {
        parent::__construct();
        session_start();
        $this->load->helper('seguranca');
        
        auth();
        $this->seguranca();
        $this->load->model('usuarios_model','ui');
        $this->load->view('adm/inc/header');
        $this->load->library('table');
    }
    
    public function seguranca(){
        if(!isset($_SESSION['nome_facilitador']) && !isset($_SESSION['email_facilitador']) && !isset($_SESSION['login']) ){
            redirect('admin/login');
        }
    }
    
    public function index(){
        $inicio                 = (!$this->uri->segment("4")) ? 0 : $this->uri->segment("4");
        
        //inicia library de paginação
        $this->load->library('pagination');
        $this->load->library('form_validation');
	
        $config['base_url']     = base_url().'admin/acessos/index';
        $config['uri_segment']  = 4;
		 
		
        if($this->input->post('buscar') !== null && strlen($this->input->post('buscar')) >=2){
			$config['total_rows'] = $this->ui->count_all_log_usuarios($this->input->post('buscar'));
			$config['per_page']     = $config['total_rows'];
        }else{
			$config['per_page']     = 10;
            $config['total_rows'] = $this->ui->count_all_log_usuarios();
        }
        
        //busca dados do curso
        if(null !== $this->input->post('buscar') && strlen($this->input->post('buscar')) >=2){
            $data['acessos']    = $this->ui->get_all_log_usuarios($inicio,  $config['per_page'], $this->input->post('buscar'))->result();
        }else{
            $data['acessos']    = $this->ui->get_all_log_usuarios($inicio,  $config['per_page'])->result();
        }
        
        $this->pagination->initialize($config);
        $data['paginacao'] = $this->pagination->create_links();
       
        $this->load->view('adm/tela/acessos', $data);
        $this->load->view('adm/inc/footer');
    }
}
