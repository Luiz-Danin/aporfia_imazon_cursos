<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class assinaturas extends CI_Controller {

    public function __construct() {
        parent::__construct();
        session_start();
        $this->seguranca();
        $this->load->library('table');
        $this->load->helper('form');
        $this->load->helper('criptografia');
        $this->load->model('transacao_model', 'tm');
        $this->load->model('assinaturas_model', 'a');
        $this->load->model('planos_model', 'p');

        if (!in_array($this->uri->segment(3), array("verifica_transacoes_por_aluno_imazon","verifica_ceritificado_impressso_por_aluno", "ajax_notifica_adm", "verifica_dados_aluno", "alt_dados_aluno"))) {
            $this->load->view('adm/inc/header');
        }
    }

    public function seguranca() {
        if (!isset($_SESSION['nome_facilitador']) && !isset($_SESSION['email_facilitador']) && !isset($_SESSION['login'])) {
            session_destroy();
            redirect('login');
        }
    }

    public function index() {

        $this->load->library('pagination');

        //$inicio = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
        $inicio = (!@$_REQUEST["id"]) ? 0 : @$_REQUEST["id"];

        //inicia library de paginação
        $this->load->library('pagination');
        $this->load->library('form_validation');
        
        
        $config['base_url']    = base_url() . 'assinaturas/index';
        $config['per_page']    = 15;
        $config['uri_segment'] = 3;
        $config['page_query_string'] = TRUE;
        
        if(@$_REQUEST["buscar"] && strlen(trim($_REQUEST["buscar"]))>2){
            $config['total_rows']  = $this->a->count_all_by_nome(array('nome'=>trim($_REQUEST["buscar"])));
        }
        if(@$_REQUEST["buscar"] && strlen(trim($_REQUEST["buscar"]))>2){
            $data['faturas'] = $this->a->get_by_nome(array('nome'=>trim($_REQUEST["buscar"])), $inicio, $config['per_page'])->result();
        }else{
            $data['faturas'] = null;
        }
        
        $data["por_pagina"] = $config['per_page'];
        @$data["paginas"] = round($config['total_rows']/$config['per_page']);
        $this->pagination->initialize($config);
        $data['paginacao'] = $this->pagination->create_links();
        $this->load->view('adm/tela/assinaturas', $data);
        $this->load->view('adm/inc/footer');
    }

    public function add() {

        $this->load->library('form_validation');

        //regras de validação
        $this->form_validation->set_rules('nome', 'Nome', 'min_length[3]|required');
        $this->form_validation->set_rules('valor', 'Valor', 'required');
        $this->form_validation->set_rules('validade', 'Validade', 'required');

        if ($this->form_validation->run() == TRUE) {
            $dados = array('nome' => $this->input->post('nome'),
                'valor' => $this->input->post('valor'),
                'validade' => $this->input->post('validade'),
                'dt_cadastro' => date('Y-m-d h:m:s'),
                'descricao' => $this->input->post('descricao'),
                "codigo" => rand(1, 232)
            );

            $this->load->helper('massinaturas');

            $interval = array("length" => $dados['validade'], unit => "MONTH");

            //prepara data
            $data_string = array("code" => $dados['codigo'], "name" => $dados['nome'], "description" => $dados['descricao'], "amount" => $dados['valor'], "interval" => $interval);


            $this->p->add($dados);
        }
        redirect("planos");
    }

    function edit() {
        $data['plano'] = $this->p->get_by_id(decodifica($this->uri->segment(4)))->result();
        $this->load->view('adm/tela/editar_plano', $data);
    }
    
    public function verifica_transacoes_por_aluno(){
        
        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'email', 'required');
        $ar = null;
        $i = 1;
        if ($this->form_validation->run() == TRUE) {
            $this->load->model('certificados_model', 'cm');
            $as = $this->cm->verifica_transacoes_por_aluno($this->input->post('email'))->result();
            foreach($as as $r){
                
                if($r->tipo_pagamento==null){
                    $tipo_pag = "Nada consta";
                }else{
                    $tipo_pag = $r->tipo_pagamento;
                }
                
                $ar[$i] = array('transacao'=>$r->transacao, 'data_compra'=>date('d/m/Y', strtotime($r->data_compra)), 'situacao'=>$r->situacao, 'plano'=>$r->nomeplano, 'tipo_pagamento'=>$tipo_pag);
                $i++;
            }
        }else{
            $ar = null;
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($ar));
    }
    
    public function verifica_transacoes_por_aluno_imazon(){
        
        $this->load->model('transacao_model', 'trm');
        $as = $this->trm->verifica_transacoes_por_aluno($this->input->post('email'))->result();
        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'email', 'required');
        $i  = 1;
        $ar = null;
        if ($this->form_validation->run() == TRUE) {
            foreach($as as $r){
                $a = $this->trm->get_dados_transacoes_por_aluno($r->transacao);
                if(count($a)>0){
                    foreach($a as $r){
                        if($r->tipo_pagamento==null){
                            $tipo_pag = "Nada consta";
                        }else{
                            $tipo_pag = $r->tipo_pagamento;
                        }
                        $ar[$i] = array("historico"=>$r->historico,'transacao'=>$r->transacao, 'data_compra'=>date('d/m/Y', strtotime($r->data_compra)), 'situacao'=>$r->situacao, 'tipo_pagamento'=>$tipo_pag, 'nomeProduto'=>$r->nomeProduto, 'nomeCurso'=>$r->nomeCurso, 'valor'=>$r->valor, 'data_pagamento'=>$r->data_pagamento);
                        
                        if($r->data_pagamento==null){
                            $ar[$i]['data_pagamento'] = "Pendente";
                        }        
                                
                        $i++;
                    }
                }
            }
        }else{
            $ar = null;
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($ar));
    }
    
    public function verifica_ceritificado_impressso_por_aluno(){
        
        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'email', 'required');
        $ar = null;
        $i = 1;
        if ($this->form_validation->run() == TRUE) {
            $this->load->model('certificados_model', 'cm');
            $as = $this->cm->verifica_certificado_impressso_por_aluno($this->input->post('email'))->result();
            foreach($as as $r){
                $ar[$i] = array('pedido'=>$r->pedido, 'cadastro'=>date('d/m/Y', strtotime($r->dt_cadastro)), 'tipo_pagamento'=>$r->tipo_pagamento, 'pagamento'=>$r->pagamento);
                $i++;
            }
        }else{
            $ar = null;
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($ar));
    }
    
    public function verifica_dados_aluno(){
        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'email', 'required');
        if ($this->form_validation->run() == TRUE) {
            $this->load->model("usuarios_model", "um");
            $ar = $this->um->get_by_id_fic($this->input->post('email'))->result();
        }else{
            $ar[0] = null;
        }
        
        $this->output->set_content_type('application/json')->set_output(json_encode($ar[0]));
    }
    
    public function alt_dados_aluno(){
       
        $this->load->library("form_validation");
        
        $this->form_validation->set_rules('nome', 'nome', 'required|max_length[120]');
        
        if ($this->form_validation->run() == true) {
            $this->load->helper('array');
            $dados = elements(array('dt_val_ass', 'nome',  'email', 'apelido', 'estado', 'cidade', 'cep', 'endereco', 'numero', 'complemento', 'bairro', 'ddd_telefone', 'telefone', 'ddd_celular', 'celular'), $this->input->post());
           
            $dv = explode("-", $dados['dt_val_ass']);
            $this->load->model('usuarios_model', 'um');
            $dados['dt_val_ass'] = date_format(date_create($dados['dt_val_ass']), "Y-m-d");
            $id_fic = $this->input->post('id_aluno');
            if($this->um->update_by_id_fic($dados, $id_fic)){
                echo "Ok";
            }else{
                echo "Erro";
            }
        }else{
            echo validation_errors();
        }
    }
    
    //historico vip
    public function verifica_cursos_aluno(){
      
        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'email', 'required');
        $ar = null;
        $i = 1;
        if ($this->form_validation->run() == TRUE) {
            $this->load->model('cursos_model', 'cm');
            $as = $this->cm->get_cursos_by_aluno($this->input->post('email'))->result();
            foreach($as as $r){
                $ar[$i] = array('historico'=>$r->usuarios_cursos_matriculados_id, 'nome'=>$r->nome, 'status'=>$r->status, 'cadastro'=>date('d/m/Y', strtotime($r->UCMData)), 'ch'=>$r->UCMCargaHorariaCertificado, 'termino'=>date('d/m/Y', strtotime($r->UCMData . "+" . round($r->UCMCargaHorariaCertificado / 8) . " days")));
               
                if($r->ultimo_dia!="0000-00-00 00:00:00"){
                    $ar[$i]['ultimo_dia'] = date('d/m/Y', strtotime($r->ultimo_dia));
                }else{
                    $ar[$i]['ultimo_dia'] = "Falta avaliar";
                }
                
                
                if(date('d/m/Y', strtotime($r->UCMDataConclusaoCertificado))!="00/00/0000"){
                    $ar[$i]['conclusao'] = date('d/m/Y', strtotime($r->UCMDataConclusaoCertificado));
                }else{
                    $ar[$i]['conclusao'] = "Falta avaliar";
                }
                
                $i++;
            }
        }else{
            $ar = null;
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($ar));
    }
    
    //historico imazon
    public function verifica_cursos_aluno_imazon(){
      
        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'email', 'required');
        $ar = null;
        $i = 1;
        if ($this->form_validation->run() == TRUE) {
            $this->load->model('cursos_model', 'cm');
            $as = $this->cm->get_cursos_by_aluno_imazon($this->input->post('email'))->result();
            foreach($as as $r){
                $ar[$i] = array('matricula'=>$r->id,'historico'=>$r->id_historico, 'status'=>$r->situacao,'nome'=>$r->nome, 'cadastro'=>date('d/m/Y', strtotime($r->primeiro_dia)),  'ultimo_dia'=>date('d/m/Y', strtotime($r->data_aprovacao)), 'ch'=>$r->carga_horaria, 'termino'=>date('d/m/Y', strtotime($r->primeiro_dia . "+" . round($r->carga_horaria / 8) . " days")), 'nota'=>$r->nota,'obser'=>$r->observacao,'conclusao'=>date('d/m/Y', strtotime($r->primeiro_dia . "+90 days")));
                if($r->ultimo_dia != "0000-00-00"){
                    $ar[$i]['ultimo_dia'] = date('d/m/Y', strtotime($r->ultimo_dia));
                }else{
                    $ar[$i]['ultimo_dia'] = "Falta avaliar";
                }
                $i++;
            }
        }else{
            $ar = null;
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($ar));
    }
    
    //historico imazon
    public function altera_status_matricula_imazon(){
      
        $this->load->library('form_validation');
        $this->form_validation->set_rules('matricula', 'matricula', 'required|min_length[2]');
        $this->form_validation->set_rules('situacao', 'situacao', 'required');
        $ar = null;
        $i = 1;
        if ($this->form_validation->run() == TRUE) {
            $this->load->model('cursos_model', 'cm');
            $as = $this->cm->altera_status_matricula_imazon($this->input->post('matricula'), $this->input->post('situacao'));
            if($as){
                $ar = array("resposta"=>true);
            }else{
                $ar = null;
            }
        }else{
            $ar = null;
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($ar));
    }
    
}
