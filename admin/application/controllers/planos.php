<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class planos extends CI_Controller{
    
    
    public function __construct(){
        parent::__construct();
        session_start();
        $this->seguranca();
        $this->load->library('table');
        $this->load->helper('form');
        $this->load->helper('criptografia');
        $this->load->model('planos_model', 'p');
        $this->load->view('adm/inc/header');
    }
    
    private function seguranca(){
        if(!isset($_SESSION['nome_facilitador']) && !isset($_SESSION['email_facilitador']) && !isset($_SESSION['login']) ){
            session_start();
            session_destroy();
            redirect('login');
        }
    }
    
    public function index(){
        $data['planos'] = $this->p->get_all()->result();
        $this->load->view('adm/tela/planos',$data);
        $this->load->view('adm/inc/footer');
    }
    
    
    public function add(){
        
        $this->load->library('form_validation');
        
        //regras de validação
        $this->form_validation->set_rules('nome', 'Nome', 'min_length[3]|required');
        $this->form_validation->set_rules('valor', 'Valor', 'required');
        $this->form_validation->set_rules('validade', 'Validade', 'required');
        
        if ($this->form_validation->run() == TRUE){
            $dados = array('nome'        => $this->input->post('nome'),
                           'valor'       => str_replace(array(',','.'), '', $this->input->post('valor')),
                           'validade'    => $this->input->post('validade'),
                           'dt_cadastro' => date('Y-m-d h:m:s'),
                           'descricao'   => $this->input->post('descricao'),
                           "codigo"=> rand(1, 232)
                        );
            
            $interval = array("length" =>  $dados['validade'], "unit" => "MONTH");
               
            //prepara data
            $data_string = array("code"=> $dados['codigo'], "name"=> $dados['nome'], "description"=> $dados['descricao'], "amount"=> $dados['valor'], "interval"=>$interval);
            
            //$this->load->helper('massinaturas');
            
            //$result = criar_plano($data_string);
            
            //if((int)$result['http_code']==201){
            $this->p->add($dados);             
            //}
       
            $_SESSION['msg'] = $result['mensagem'];
        }
        
        
        redirect("planos");
    }
    
    function edit(){
        $data['plano'] = $this->p->get_by_id(decodifica($this->uri->segment(3)))->result();
        $this->load->view('adm/tela/edit/plano',$data);
    }
    
    public function alt(){
        
        $this->load->library('form_validation');
        
        //regras de validação
        $this->form_validation->set_rules('nome', 'Nome', 'min_length[3]|required');
        $this->form_validation->set_rules('valor', 'Valor', 'required');
        $this->form_validation->set_rules('validade', 'Validade', 'required');
             
        if ($this->form_validation->run() == TRUE){
            if($this->input->post('ativo')=="on"){
                $status = 'active';
            }else{
                $status = 'inactive';
            }
            $dados = array('nome'        => $this->input->post('nome'),
                       'valor'        => str_replace(array(',','.'), '', $this->input->post('valor')),
                       'validade'     => $this->input->post('validade'),
                       'dt_cadastro'  => date('Y-m-d h:m:s'),
                       'codigo'       => decodifica($this->input->post('codigo')),
                       'descricao'    =>$this->input->post('descricao')
                    );
            $interval = array("length" =>  $dados['validade'], 'unit' => "MONTH");
            $this->p->update_by_id(decodifica($this->input->post('id')), $dados);           
            $_SESSION['msg'] = $result['mensagem'];
        }
        redirect("planos");
    }
    
    public function del(){
        
            $id = decodifica($this->uri->segment(4));
            //$this->load->helper('massinaturas');
            //$result = desativar_plano($id);
            //if($result['http_code']==200){
            $this->p->delete(decodifica($this->uri->segment(3)));
            //}
            $_SESSION['msg'] = $result['mensagem']; 
            redirect("planos");
            
    }
    
    public function ativar(){
        
            $id = decodifica($this->uri->segment(4));
            ////$this->load->helper('massinaturas');
            //$result = ativar_plano($id);
            //if((int)$result['http_code']==200){
            $this->p->ativar(decodifica($this->uri->segment(3)));
            //}
            $_SESSION['msg'] = $result['mensagem']; 
            redirect("planos");
	
    }
    
}
