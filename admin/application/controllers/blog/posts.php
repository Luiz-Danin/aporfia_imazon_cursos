<?php

class posts extends CI_Controller{

   
    public function __construct() {
        parent::__construct();
        session_start();
        $this->seguranca();
        $this->load->model('post_blog_model', 'p');
        $this->load->model('categoria_blog_model', 'cat');
        if($this->uri->segment(3)!=="envia_img"){
            $this->load->view('adm/inc/header');
        }
        $this->load->library('table');
    }

    public function seguranca() {
        if (!isset($_SESSION['nome_facilitador']) && !isset($_SESSION['email_facilitador']) && !isset($_SESSION['login'])) {
            redirect('admin/login');
        }
    }

    public function index() {

        $inicio = (!$this->uri->segment("4")) ? 0 : $this->uri->segment("4");

        //inicia library de paginação
        $this->load->library('pagination');
        $this->load->library('form_validation');

        $config['base_url'] = base_url() . 'admin/blog/posts/index';
        $config['uri_segment'] = 4;

        if ($this->input->post('buscar') !== null && strlen($this->input->post('buscar')) >= 2) {
            $config['total_rows'] = $this->p->count_all($this->input->post('buscar'));
            $config['per_page'] = $config['total_rows'];
        } else {
            $config['per_page'] = 10;
            $config['total_rows'] = $this->p->count_all();
        }

        $data['categorias'] = $this->cat->get_all()->result();
        
        //busca dados do curso
        if (null !== $this->input->post('buscar') && strlen($this->input->post('buscar')) >= 2) {
            $data['posts'] = $this->p->get_all($inicio, $config['per_page'], $this->input->post('buscar'))->result();
        } else {
            $data['posts'] = $this->p->get_all()->result();
        }

        $this->pagination->initialize($config);
        $data['paginacao'] = $this->pagination->create_links();
        $this->load->view('adm/tela/posts_blog', $data);
        $this->load->view('adm/inc/footer');
    }

    public function editar() {
        if ($this->uri->segment(4)) {
            $data['categorias'] = $this->cat->get_all()->result();
            $data['dados'] = $this->p->get_by_id(decodifica($this->uri->segment(4)))->result();
            if (count($data['dados']) > 0) {
                $this->load->view('adm/tela/edit/posts_blog', $data);
                $this->load->view('adm/inc/footer');
            } else {
                redirect('blog/posts');
            }
        } else {
            redirect('blog/posts');
        }
    }
    
    public function add() {
        $dados = elements(array('titulo', 'conteudo', 'categoria'), $this->input->post());
        $dados['dt_cadastro'] = date("Y-m-d h:i:s");
        $dados['page_id'] = NormalizaURL($this->input->post('titulo'));
        $dados['conteudo'] = str_replace("../../", "./", $dados['conteudo']);
        $this->p->add($dados);
        redirect('blog/posts');
    }

    public function edit() {
        $dados = elements(array('titulo', 'id', 'conteudo', 'categoria'), $this->input->post());
        $dados['id']        = decodifica($dados['id']);
        $dados['dt_alteracao'] = date('Y-m-d h:i:s');
        $dados['conteudo']  = str_replace("../../../../", "./", $dados['conteudo']);
        //print_r($dados);
        //exit;
        $this->p->edit_by_id($dados);
        redirect('blog/posts/editar/' . codifica($dados['id']));
    }
    
    public function ativar($id, $redireciona = null) {
        $dados = array('id'=>decodifica($id), 'status'=>1);
        $this->p->edit_by_id($dados);
        if($redireciona != null){
            redirect('blog/posts/editar/' . codifica($dados['id']));
        }
        redirect('blog/posts/');
    }
    
    public function desativar($id, $redireciona = null) {
        $dados = array('id'=>decodifica($id), 'status'=>0);
        $this->p->edit_by_id($dados);
        
        if($redireciona != null){
            redirect('blog/posts/editar/' . codifica($dados['id']));
        }
        redirect('blog/posts/');
    }

    public function del() {
        if ($this->uri->segment(4)) {
            $this->p->del(decodifica($this->uri->segment(4)));
        }
        redirect('blog/posts');
    }

    public function envia_img(){
        if($_FILES['img-nova']['name']!==null){
            $diretorio =  $_SERVER['DOCUMENT_ROOT']."/uploads/img";
            //echo $diretorio.$_FILES['img-nova']['name'];
            $ext = pathinfo($_FILES['img-nova']['name'], PATHINFO_EXTENSION);
            if($ext == "jpg" ||$ext == "jpeg" ||$ext == "png"){
                $tamanho_imagem = $_FILES['img-nova']['size'];
                $nome_arquivo   = md5($_FILES['img-nova']['name'].  microtime()).'.'.$ext;
                $tamanho = round($tamanho_imagem / 1024);
                if($tamanho < 3062){ //se imagem for até 1MB envia
                    if(move_uploaded_file($_FILES['img-nova']['tmp_name'], $diretorio.'/'.$nome_arquivo)){
                        echo '/'.$nome_arquivo;
                    }else{
                        echo "Imagem não alterada por problemas no servidor.";
                    }
                }else{
                    echo "Tamanho de imagem não permitido.";
                }
            }else{
                echo "Formato de imagem não permitido.";
            }
        }
    }
    
}
