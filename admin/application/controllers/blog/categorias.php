<?php

class categorias extends CI_Controller {

    public function __construct() {
        parent::__construct();

        session_start();
        $this->seguranca();
        $this->load->model('categoria_blog_model', 'cat');
        $this->load->view('adm/inc/header');
        $this->load->library('table');
    }

    public function seguranca() {
        if (!isset($_SESSION['nome_facilitador']) && !isset($_SESSION['email_facilitador']) && !isset($_SESSION['login'])) {
            redirect('admin/login');
        }
    }

    public function index() {

        $inicio = (!$this->uri->segment("4")) ? 0 : $this->uri->segment("4");

        //inicia library de paginação
        $this->load->library('pagination');
        $this->load->library('form_validation');

        $config['base_url'] = base_url() . 'admin/blog/categorias/index';
        $config['uri_segment'] = 4;

        if ($this->input->post('buscar') !== null && strlen($this->input->post('buscar')) >= 2) {
            $config['total_rows'] = $this->cat->count_all($this->input->post('buscar'));
            $config['per_page'] = $config['total_rows'];
        } else {
            $config['per_page'] = 10;
            $config['total_rows'] = $this->cat->count_all();
        }

        //busca dados do curso
        if (null !== $this->input->post('buscar') && strlen($this->input->post('buscar')) >= 2) {
            $data['categorias'] = $this->cat->get_all($inicio, $config['per_page'], $this->input->post('buscar'))->result();
        } else {
            //$data['categorias'] = $this->cat->get_all($inicio, $config['per_page'])->result();
            $data['categorias'] = $this->cat->get_all()->result();
        }

        $this->pagination->initialize($config);
        $data['paginacao'] = $this->pagination->create_links();
        $this->load->view('adm/tela/categorias_blog', $data);
        $this->load->view('adm/inc/footer');
    }

    public function editar() {
        if ($this->uri->segment(4)) {
            $data['dados'] = $this->cat->get_by_id(decodifica($this->uri->segment(4)))->result();
            if (count($data['dados']) > 0) {
                $this->load->view('adm/tela/edit/categorias_blog', $data);
                $this->load->view('adm/inc/footer');
            } else {
                redirect('blog/categorias');
            }
        } else {
            redirect('blog/categorias');
        }
    }
    
    public function add() {
        $dados = elements(array('nome'), $this->input->post());
        $dados['dt_cadastro'] = date("Y-m-d H:i:s");
        $this->cat->add($dados);
        redirect('blog/categorias');
    }

    public function edit() {
        $dados = elements(array('nome', 'id'), $this->input->post());
        $dados['id'] = decodifica($dados['id']);
        $this->cat->edit_by_id($dados);
        redirect('blog/categorias/editar/' . codifica($dados['id']));
    }

    public function del() {
        if ($this->uri->segment(4)) {
            $this->cat->del(decodifica($this->uri->segment(4)));
        }
        redirect('blog/categorias');
    }

}
