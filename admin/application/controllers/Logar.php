<?php

class Logar extends CI_Controller {

    public function __construct() {
        parent::__construct();
        session_start();
        $this->load->model('usuarios_model', 'um');
    }

    public function index() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'email', 'required|min_length[3]');
        if ($this->form_validation->run() === TRUE) {
            $dados = array('email' => $this->input->post('email'));
            $resultado = $this->um->verifica_login_adm($dados)->result();
            
            if ((int) count($resultado) === 1) {
                
                $_SESSION['email_facilitador'] = $resultado[0]->email;
                $_SESSION['nome_facilitador']  = $resultado[0]->nome;
                $_SESSION['login_2'] = true;
                $_SESSION['key_ima'] = substr(md5($_SESSION['email_facilitador'].  microtime()), 0, 16);
                require_once(APPPATH.'libraries/phpmailer/class.phpmailer.php');
                require_once(APPPATH.'libraries/phpmailer/class.smtp.php');
                   

                //Nova instância do PHPMailer  

               $mail = new PHPMailer;

                $mail->IsSMTP();
                $mail->SMTPAuth = true;
                $mail->SMTPSecure = "ssl";
                $mail->Port = 465;
                $mail->Host = "smtp.gmail.com";      // sets GMAIL as the SMTP server  
                $mail->Username = "cadastro@imazon.com.br";
                $mail->Password = "657@beta";
                $mail->Subject = 'Novo código de acesso Imazon';
                $mail->Sender = $mail->Username;
                $mail->From = $mail->Username;
                $mail->FromName = "Imazon Cursos";
                $mail->AddAddress($resultado[0]->email);
                $mail->Body = utf8_decode("Seu codigo de acesso é: " . $_SESSION['key_ima']);
                $mail->AltBody = $mail->Body;

                $enviado = $mail->Send();
                redirect('login/confirm');
            } else {
                session_destroy();
                redirect('login');
            }
        } else {
            session_destroy();
            redirect('login');
        }
    }

    public function confirm() {
        if (isset($_SESSION['login_2']) && isset($_SESSION['email_facilitador'])) {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('senha', 'senha', 'required|min_length[3]');
            $resultado = $this->um->get_dados_login_adm($_SESSION['email_facilitador'])->result();
            if ($_SESSION['key_ima'] == $this->input->post('senha')) {
                $_SESSION['login'] = $resultado[0]->login;
                unset($_SESSION['login_2']);
                unset($_SESSION['key_ima']);
                $_SESSION['logado'] = TRUE;
                redirect('assinaturas');
            } else {
                session_destroy();
                redirect('login');
            }
        } else {
            session_destroy();
            redirect('login');
        }
    }

}
