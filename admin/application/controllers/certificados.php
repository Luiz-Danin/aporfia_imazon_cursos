<?php

class certificados extends CI_Controller {

    public function __construct() {
        parent::__construct();
        session_start();
        $this->seguranca();

        $this->load->model('certificados_model', 'cm');
        $this->load->model('cursos_model', 'cursosm');
        $this->load->model('base_legal_model', 'bm');
        
        $this->load->view('adm/inc/header');
        $this->load->library('table');
    }

    public function seguranca() {
        if (!isset($_SESSION['nome_facilitador']) && !isset($_SESSION['email_facilitador']) && !isset($_SESSION['login'])) {
            redirect('login');
        }
    }

    public function index() {
        redirect('certificados/nao_enviados');
    }

    public function nao_enviados() {

        $inicio = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");

        //inicia library de pagina��o
        $this->load->library('pagination');
        $this->load->library('form_validation');

        $config['base_url']    = base_url() . 'admin/certificados/nao_enviados';
        $config['uri_segment'] = 3;
        if ($this->input->post('buscar') !== null && strlen($this->input->post('buscar')) >= 2) {
            $config['total_rows'] = $this->cm->count_all_inativo($this->input->post('buscar')) + $this->cm->count_all_inativo_imazon($this->input->post('buscar'));
            $config['per_page']   = $config['total_rows'];
        } else {
            $config['per_page']   = 10;
            $config['total_rows'] = count($this->cm->count_all_inativo()->result()) + $this->cm->count_all_inativo_imazon();
        }
        //busca dados do curso
        if (null !== $this->input->post('buscar') && strlen($this->input->post('buscar')) >= 2) {
            $data['certificados']        = $this->cm->get_all_inativo($inicio, $config['per_page'], $this->input->post('buscar'));
            $data['certificados_imazon'] = $this->cm->get_all_inativo_imazon($inicio, $config['per_page'], $this->input->post('buscar'));
        } else {
            $data['certificados']        = $this->cm->get_all_inativo($inicio, $config['per_page']);
            $data['certificados_imazon'] = $this->cm->get_all_inativo_imazon($inicio, $config['per_page']);
        }
        $this->pagination->initialize($config);
        $data['paginacao'] = $this->pagination->create_links();
        $this->load->view('adm/tela/certificado_nao_enviados', $data);
        $this->load->view('adm/inc/footer');
    }

    public function pendentes() {
        $inicio = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
        $this->load->library('pagination');
        $this->load->library('form_validation');
        $config['base_url'] = base_url() . 'admin/certificados/pendentes';
        $config['uri_segment'] = 3;
        if ($this->input->post('buscar') !== null && strlen($this->input->post('buscar')) >= 2) {
            $config['total_rows'] = $this->cm->count_all_pendentes($this->input->post('buscar'));
            $config['per_page'] = $config['total_rows'];
        } else {
            $config['total_rows'] = $this->cm->count_all_pendentes();
            $config['per_page'] = 10;
        }
        if (null !== $this->input->post('buscar') && strlen($this->input->post('buscar')) >= 2) {
            $data['certificados'] = $this->cm->get_all_pendentes($inicio, $config['per_page'], $this->input->post('buscar'));
        } else {
            $data['certificados'] = $this->cm->get_all_pendentes($inicio, $config['per_page']);
        }
        $this->pagination->initialize($config);
        $data['paginacao'] = $this->pagination->create_links();
        $this->load->view('adm/tela/certificado_pendentes', $data);
        $this->load->view('adm/inc/footer');
    }

    public function enviados() {
        $inicio = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
        $this->load->library('pagination');
        $this->load->library('form_validation');
        $config['base_url'] = base_url() . 'admin/certificados/enviados';
        $config['uri_segment'] = 3;
        if ($this->input->post('buscar') !== null && strlen(trim($this->input->post('buscar'))) >= 2) {
            $config['total_rows'] = $this->cm->count_all_ativo(trim($this->input->post('buscar'))) + $this->cm->count_all_ativo_imazon($this->input->post('buscar'));
            $config['per_page'] = $config['total_rows'];
        } else {
            $config['per_page'] = 10;
            $config['total_rows'] = $this->cm->count_all_ativo();
        }
        if (null !== $this->input->post('buscar') && strlen(trim($this->input->post('buscar'))) >= 2) {
            $data['certificados'] = $this->cm->get_all_ativo($inicio, $config['per_page'], trim($this->input->post('buscar')));
            $data['certificados_imazon'] = $this->cm->get_all_ativo_imazon($inicio, $config['per_page'], $this->input->post('buscar'));
        } else {
            $data['certificados_imazon'] = $this->cm->get_all_ativo_imazon($inicio, $config['per_page']);
            $data['certificados'] = $this->cm->get_all_ativo($inicio, $config['per_page']);
        }
        $this->pagination->initialize($config);
        $data['paginacao'] = $this->pagination->create_links();
        $this->load->view('adm/tela/certificado_enviados', $data);
        $this->load->view('adm/inc/footer');
    }

    //gera certificado web
    function gerar_certificado_impresso() {

        $this->load->model('usuarios_model', 'um');
        $cedoc_doc_id_fk = base64_decode($this->uri->segment(3));
        $email           = base64_decode($this->uri->segment(4));
        
        if ($cedoc_doc_id_fk && $email) {
            $certificados = $this->cm->gera_todos_inativos($cedoc_doc_id_fk, $email)->result();
            $aluno = $this->um->get_by_id_fic($email)->result();

            if (count($certificados) == 0) {
                redirect('admin/certificados');
            } else {
                $params = array('orientation' => 'P', 'unit' => 'mm', 'size' => 'A4');
                $this->load->library('fpdf', $params);

                //gera certificados em pdf
                foreach ($certificados as $c) {
                    
                    $aulas = $this->cursosm->get_aulas($cedoc_doc_id_fk)->result();

                    $ementa = null;

                    $i = 1; //;
                    $limite = 16;

                    if(strlen(trim($c->descricao))>2){
                        $ementa = nl2br($c->descricao);
                    }else{
                        if(count($aulas)>0){
                            foreach ($aulas as $la) {
                                if ($i < 16) {
                                    $ementa .= nl2br($i . '. ' . ltrim(strtoupper($la->CATitulo)) . "\n");
                                }
                                $i++;
                            }
                        }
                    }
                    
                    $this->fpdf->AddPage();
                    $this->fpdf->Image('imgs/logo.png', 148, 5); //logo abed
                    $this->fpdf->SetFont('Arial', 'B', 8);
                    $this->fpdf->Ln(10); //quebra de linha
                    $this->fpdf->setX(10);
                    $this->fpdf->Ln(5); //quebra de linha
                    $this->fpdf->SetFont('Arial', '', 12);
                    $this->fpdf->setY("58");
                    $this->fpdf->setX("5");


                    $this->fpdf->Cell(0, -20, 'Conferido a', 0, 1, 'C');
                    $this->fpdf->SetFont('Arial', 'B', 10);
                    $this->fpdf->Cell(0, 35, $aluno[0]->nome, 0, 1, 'C');
                    $this->fpdf->SetFont('Arial', '', 10);
                    $this->fpdf->Cell(0, -25, 'Por ter conclu�do com �xito o Curso Livre ', 0, 1, 'C');
                    $this->fpdf->MultiCell(0, 40, utf8_decode($c->nome), 0, 'C');

                    $this->fpdf->setY(80);
                    $this->fpdf->setX(40);
                    $this->fpdf->Cell(0, 5, 'Tipo: Capacita��o/Atualiza��o', 0, 1);
                    $this->fpdf->setX(40);
                    $this->fpdf->Cell(0, 5, 'Aproveitamento: 100,0', 0, 1);
                    $this->fpdf->setX(40);
                    $this->fpdf->Cell(0, 5, 'Certificado Registrado Sob. N� ' . $c->usuarios_cursos_matriculados_id, 0, 1);

                    $this->fpdf->setY(80);
                    $this->fpdf->setX(120);
                    $this->fpdf->Cell(0, 5, 'Carga Hor�ria: ' . $c->UCMCargaHorariaCertificado . ' Horas', 0, 1);


                    if ($c->UCMData != '00/00/0000') {
                        $this->fpdf->setX(120);
                        $this->fpdf->Cell(0, 5, 'Data Matr�cula: ' . date('d/m/Y', strtotime($c->UCMData)), 0, 1);
                    } else {
                        $this->fpdf->setX(120);
                        $this->fpdf->Cell(0, 5, 'Data Matr�cula: ' . date('d/m/Y', strtotime($c->UCMData)), 0, 1);
                    }

                    $this->fpdf->setX(120);
                    $this->fpdf->Cell(0, 5, 'Data de Conclus�o: ' . date('d/m/Y', strtotime($c->UCMData)), 0, 1);

                    $this->fpdf->SetFont('Arial', '', 10);
                    $this->fpdf->SetXY(10, 100);
                    $texto = "Base Legal: Lei N� 9394/96, Art 40, Resolu��o CNE/CEB N� 6/2012, Art. 25�";
                    $texto2 = "Decreto 5154/2004, Art. 3�";
                    $this->fpdf->MultiCell(0, 5, $texto, 0, 'C');
                    $this->fpdf->MultiCell(0, 5, $texto2, 0, 'C');
                    $this->fpdf->SetFont('Arial', 'B', 10);

                    $this->fpdf->Cell(340, 18, 'Bel�m, ' . date("d/m/Y"), 0, 1, 'C');

                    $this->fpdf->SetFont('Arial', '', 10);
                    $this->fpdf->SetLineWidth(0.2);
                    $this->fpdf->Line(10, 130, 70, 130);
                    $this->fpdf->SetXY(20, 135);
                    $this->fpdf->Cell(0, 1, 'Assinatura do/a Aluno/a', 0, 0);
                    $this->fpdf->SetXY(20, 140);


                    $this->fpdf->Line(90, 130, 150, 130);
                    $this->fpdf->SetXY(110, 135);
                    $this->fpdf->Cell(0, 1, 'Diretor', 0, 0);


                    // P�gina de Verso	
                    $this->fpdf->AddPage();
                    $this->fpdf->SetFont('Arial', '', 8);
                    $this->fpdf->Cell(0, 10, 'Conte�do', 0, 1, 'L');
                    $this->fpdf->SetXY(10, 34);
                    $this->fpdf->MultiCell(0, 5, utf8_decode(strip_tags($ementa)), 0, 'L');
                    $this->fpdf->SetX(120);
                    $this->fpdf->Cell(0, 1, 'Autenticar: http://www.imazon.com.br/autenticacao/', 0, 0);
                }

                $this->fpdf->Output();
            }
        
        } else {
            $certificados = $this->cm->busca_todos_certif_impressos_inativos()->result();
            
            if(count($certificados)>0){
                $params = array('orientation' => 'P', 'unit' => 'mm', 'size' => 'A4');
                $this->load->library('fpdf', $params);
                foreach($certificados as $c){
                    
                    $aluno        = $this->cm->gera_nome_certif_impresso_inativo_by_email($c->email);
                    $aulas        = $this->cursosm->get_aulas($c->curso)->result();
                    $curso        = $this->cursosm->get_by_id($c->curso)->result();
                    $certificado  = $this->cm->gera_certificado_by_historico($c->historico, $c->curso)->result();  
                    $ementa = null;
                    $i      = 1;
                    $limite = 16;
                    
                    if(strlen(trim($curso[0]->descricao))>2){
                        $ementa = nl2br($curso[0]->descricao);
                    }else{
                        foreach ($aulas as $la) {
                            if ($i < 16) {
                                $ementa .= nl2br($i . '. ' . ltrim(strtoupper($la->CATitulo)) . "\n");
                            }
                            $i++;
                        }
                    }
                   
                    $this->fpdf->AddPage();
                    $this->fpdf->Image('imgs/logo.png', 148, 5); //logo abed
                    $this->fpdf->SetFont('Arial', 'B', 8);
                    $this->fpdf->Ln(10); //quebra de linha
                    $this->fpdf->setX(10);
                    $this->fpdf->Ln(5); //quebra de linha
                    $this->fpdf->SetFont('Arial', '', 12);
                    $this->fpdf->setY("58");
                    $this->fpdf->setX("5");
                    
                    $this->fpdf->Cell(0, -20, 'Conferido a', 0, 1, 'C');
                    $this->fpdf->SetFont('Arial', 'B', 10);
                    @$this->fpdf->Cell(0, 35, $aluno[0]->nome, 0, 1, 'C');
                    $this->fpdf->SetFont('Arial', '', 10);
                    $this->fpdf->Cell(0, -25, 'Por ter conclu�do com �xito o Curso Livre ', 0, 1, 'C');
                    $this->fpdf->MultiCell(0, 40, utf8_decode($curso[0]->nome), 0, 'C');

                    $this->fpdf->setY(80);
                    $this->fpdf->setX(40);
                    $this->fpdf->Cell(0, 5, 'Tipo: Capacita��o/Atualiza��o', 0, 1);
                    $this->fpdf->setX(40);
                    $this->fpdf->Cell(0, 5, 'Aproveitamento: 100,0', 0, 1);
                    $this->fpdf->setX(40);
                    $this->fpdf->Cell(0, 5, 'Certificado Registrado Sob. N� ' . $c->id, 0, 1);

                    $this->fpdf->setY(80);
                    $this->fpdf->setX(120);
                    @$this->fpdf->Cell(0, 5, 'Carga Hor�ria: ' . $certificado[0]->UCMCargaHorariaCertificado . ' Horas', 0, 1);
                    
                    if (@$certificado[0]->UCMData != '00/00/0000') {
                        $this->fpdf->setX(120);
                        @$this->fpdf->Cell(0, 5, 'Data Matr�cula: ' . date('d/m/Y', strtotime($certificado[0]->UCMData)), 0, 1);
                    } else {
                        $this->fpdf->setX(120);
                        $this->fpdf->Cell(0, 5, 'Data Matr�cula: ' . date('d/m/Y', strtotime($certificado[0]->UCMData)), 0, 1);
                    }

                    $this->fpdf->setX(120);
                    @$this->fpdf->Cell(0, 5, 'Data Conclus�o: ' . date('d/m/Y', strtotime($certificado[0]->UCMData)), 0, 1);

                    $this->fpdf->SetFont('Arial', '', 10);
                    $this->fpdf->SetXY(10, 100);
                    $texto = "Base Legal: Lei N� 9394/96, Art 40, Resolu��o CNE/CEB N� 6/2012, Art. 25�";
                    $texto2 = "Decreto 5154/2004, Art. 3�";
                    $this->fpdf->MultiCell(0, 5, $texto, 0, 'C');
                    $this->fpdf->MultiCell(0, 5, $texto2, 0, 'C');
                    $this->fpdf->SetFont('Arial', 'B', 10);

                    $this->fpdf->Cell(340, 18, 'Bel�m, ' . date("d/m/Y"), 0, 1, 'C');

                    $this->fpdf->SetFont('Arial', '', 10);
                    $this->fpdf->SetLineWidth(0.2);
                    $this->fpdf->Line(10, 130, 70, 130);
                    $this->fpdf->SetXY(20, 135);
                    $this->fpdf->Cell(0, 1, 'Assinatura do/a Aluno/a', 0, 0);
                    $this->fpdf->SetXY(20, 140);


                    $this->fpdf->Line(90, 130, 150, 130);
                    $this->fpdf->SetXY(110, 135);
                    $this->fpdf->Cell(0, 1, 'Diretor', 0, 0);


                    // P�gina de Verso	
                    $this->fpdf->AddPage();
                    $this->fpdf->SetFont('Arial', '', 8);
                    $this->fpdf->Cell(0, 10, 'Conte�do', 0, 1, 'L');
                    $this->fpdf->SetXY(10, 34);
                    $this->fpdf->MultiCell(0, 5, utf8_decode(strip_tags($ementa)), 0, 'L');
                    $this->fpdf->SetX(120);
                    $this->fpdf->Cell(0, 1, 'Autenticar: http://www.imazon.com.br/autenticacao/', 0, 0);
                }
                @$this->fpdf->Output();
            }else{
                redirect("admin/certificados/nao_enviados");
            }
        }
    }

    //gera certificado web
    function gerar_certificado_impresso_imazon() {
        $this->load->model('usuarios_model', 'um');
        $idCertificado = base64_decode($this->uri->segment(3));
        $email = base64_decode($this->uri->segment(4));
        $base_legal = $this->bm->get_base_legal_atual();
        $texto      = utf8_decode($base_legal[0]->texto);
        if ($idCertificado && $email) {
            $certificados = $this->cm->gera_certificado_inativo_imazon($idCertificado, $email);
            $aluno = $this->um->get_by_id_fic($email)->result();
            if (count($certificados) == 0) {
                redirect('certificados');
            } else {
                $params = array('orientation' => 'P', 'unit' => 'mm', 'size' => 'A4');
                $this->load->library('fpdf', $params);
                //gera certificados em pdf
                foreach ($certificados as $c) {
                    $i = 1; //;
                    $limite = 16;
                    $this->fpdf->AddPage();
                    $this->fpdf->Image('imgs/logo.png', 148, 5); //logo abed
                    $this->fpdf->SetFont('Arial', 'B', 8);
                    $this->fpdf->Ln(10); //quebra de linha
                    $this->fpdf->setX(10);
                    $this->fpdf->Ln(5); //quebra de linha
                    $this->fpdf->SetFont('Arial', '', 12);
                    $this->fpdf->setY("58");
                    $this->fpdf->setX("5");
                    
                    $this->fpdf->Cell(0, -20, 'Conferido a', 0, 1, 'C');
                    $this->fpdf->SetFont('Arial', 'B', 10);
                    $this->fpdf->Cell(0, 35, utf8_decode($aluno[0]->nome), 0, 1, 'C');
                    $this->fpdf->SetFont('Arial', '', 10);
                    $this->fpdf->Cell(0, -25, 'Por ter concluido com �xito o Curso Livre ', 0, 1, 'C');
                    $this->fpdf->MultiCell(0, 40, utf8_decode($c->nome), 0, 'C');

                    $this->fpdf->setY(80);
                    $this->fpdf->setX(40);
                    $this->fpdf->Cell(0, 5, 'Tipo: Capacita��o/Atualiza��o', 0, 1);
                    $this->fpdf->setX(40);
                    $this->fpdf->Cell(0, 5, 'Aproveitamento: '.$c->nota.',0', 0, 1);
                    $this->fpdf->setX(40);
                    $this->fpdf->Cell(0, 5, 'Certificado Registrado Sob. N� ' . $c->id_historico, 0, 1);

                    $this->fpdf->setY(80);
                    $this->fpdf->setX(120);
                    $this->fpdf->Cell(0, 5, 'Carga Hor�ria: ' . $c->carga_horaria . ' Horas', 0, 1);


                    if ($c->primeiro_dia != '00/00/0000') {
                        $this->fpdf->setX(120);
                        $this->fpdf->Cell(0, 5, 'Data Matr�cula: ' . date('d/m/Y', strtotime($c->primeiro_dia)), 0, 1);
                    } else {
                        $this->fpdf->setX(120);
                        $this->fpdf->Cell(0, 5, 'Data Matr�cula: ' . date('d/m/Y', strtotime($c->primeiro_dia)), 0, 1);
                    }

                    $this->fpdf->setX(120);
                    $this->fpdf->Cell(0, 5, 'Data Conclus�o: ' . date('d/m/Y', strtotime($c->data_aprovacao)), 0, 1);

                    $this->fpdf->SetFont('Arial', '', 10);
                    $this->fpdf->SetXY(10, 100);
                   
                    
                    $this->fpdf->MultiCell(0, 5, $texto, 0, 'C');
                    $this->fpdf->SetFont('Arial', 'B', 10);
                    if ($c->id_tipo == 2){
                        $this->fpdf->setX(30);
                        $this->fpdf->Cell(0, 5, 'Ouro', 0, 1);
                    }
                    $this->fpdf->Cell(340, 18, 'Bel�m, ' . date("d/m/Y"), 0, 1, 'C');
                    $this->fpdf->SetFont('Arial', '', 10);
                    $this->fpdf->SetLineWidth(0.2);
                    $this->fpdf->Line(10, 130, 70, 130);
                    $this->fpdf->SetXY(20, 135);
                    $this->fpdf->Cell(0, 1, 'Assinatura do/a Aluno/a', 0, 0);
                    $this->fpdf->SetXY(20, 140);


                    $this->fpdf->Line(90, 130, 150, 130);
                    $this->fpdf->SetXY(110, 135);
                    $this->fpdf->Cell(0, 1, 'Diretor', 0, 0);


                    // P�gina de Verso	
                    $this->fpdf->AddPage();
                    $this->fpdf->SetFont('Arial', '', 8);
                    $this->fpdf->Cell(0, 10, 'Conte�do', 0, 1, 'L');
                    $this->fpdf->SetXY(10, 34);
                    $this->fpdf->MultiCell(0, 5, utf8_decode(strip_tags($c->descricao)), 0, 'L');
                    $this->fpdf->SetX(120);
                    $this->fpdf->Cell(0, 2, 'Autenticar em: https://www.imazoncursos.com.br/autenticacao/', 0, 0);
                    $this->fpdf->SetX(120);
                    $this->fpdf->Cell(0, 10, 'C�digo de autentica�ao: '.$c->codigo_autenticacao, 0, 0);
                    
                }

                $this->fpdf->Output();
            }
        
        } else {
            
            $certificados = $this->cm->gera_todos_certificados_inativo_imazon();
            //print_r($certificados);
            //exit;
            if(count($certificados)>0){
                $params = array('orientation' => 'P', 'unit' => 'mm', 'size' => 'A4');
                $this->load->library('fpdf', $params);
                
                //gera certificados em pdf
                foreach ($certificados as $c) {
                    $i = 1; //;
                    $limite = 16;
                    $this->fpdf->AddPage();
                    $this->fpdf->Image('imgs/logo.png', 148, 5); //logo abed
                    $this->fpdf->SetFont('Arial', 'B', 8);
                    $this->fpdf->Ln(10); //quebra de linha
                    $this->fpdf->setX(10);
                    $this->fpdf->Ln(5); //quebra de linha
                    $this->fpdf->SetFont('Arial', '', 12);
                    $this->fpdf->setY("58");
                    $this->fpdf->setX("5");


                    $this->fpdf->Cell(0, -20, 'Conferido a', 0, 1, 'C');
                    $this->fpdf->SetFont('Arial', 'B', 10);
                    $this->fpdf->Cell(0, 35, utf8_decode($c->nomeAluno), 0, 1, 'C');
                    $this->fpdf->SetFont('Arial', '', 10);
                    $this->fpdf->Cell(0, -25, 'Por ter concluido com �xito o Curso Livre ', 0, 1, 'C');
                    $this->fpdf->MultiCell(0, 40, utf8_decode($c->cursoNome), 0, 'C');

                    $this->fpdf->setY(80);
                    $this->fpdf->setX(40);
                    $this->fpdf->Cell(0, 5, 'Tipo: Capacita��o/Atualiza��o', 0, 1);
                    $this->fpdf->setX(40);
                    $this->fpdf->Cell(0, 5, 'Aproveitamento: '.$c->nota.',0', 0, 1);
                    $this->fpdf->setX(40);
                    $this->fpdf->Cell(0, 5, 'Certificado Registrado Sob. N� ' . $c->id_historico, 0, 1);

                    $this->fpdf->setY(80);
                    $this->fpdf->setX(120);
                    $this->fpdf->Cell(0, 5, 'Carga Hor�ria: ' . $c->carga_horaria . ' Horas', 0, 1);


                    if ($c->primeiro_dia != '00/00/0000') {
                        $this->fpdf->setX(120);
                        $this->fpdf->Cell(0, 5, 'Data Matr�cula: ' . date('d/m/Y', strtotime($c->primeiro_dia)), 0, 1);
                    } else {
                        $this->fpdf->setX(120);
                        $this->fpdf->Cell(0, 5, 'Data Matr�cula: ' . date('d/m/Y', strtotime($c->primeiro_dia)), 0, 1);
                    }

                    $this->fpdf->setX(120);
                    $this->fpdf->Cell(0, 5, 'Data Conclus�o: ' . date('d/m/Y', strtotime($c->data_aprovacao)), 0, 1);

                    $this->fpdf->SetFont('Arial', '', 10);
                    $this->fpdf->SetXY(10, 100);
                   
                    
                    $this->fpdf->MultiCell(0, 5, $texto, 0, 'C');
                    $this->fpdf->SetFont('Arial', 'B', 10);
                    if ($c->id_tipo == 2){
                        $this->fpdf->setX(30);
                        $this->fpdf->Cell(0, 5, 'Ouro', 0, 1);
                    }
                    $this->fpdf->Cell(340, 18, 'Bel�m, ' . date("d/m/Y"), 0, 1, 'C');
                    $this->fpdf->SetFont('Arial', '', 10);
                    $this->fpdf->SetLineWidth(0.2);
                    $this->fpdf->Line(10, 130, 70, 130);
                    $this->fpdf->SetXY(20, 135);
                    $this->fpdf->Cell(0, 1, 'Assinatura do/a Aluno/a', 0, 0);
                    $this->fpdf->SetXY(20, 140);


                    $this->fpdf->Line(90, 130, 150, 130);
                    $this->fpdf->SetXY(110, 135);
                    $this->fpdf->Cell(0, 1, 'Diretor', 0, 0);


                    // P�gina de Verso	
                    $this->fpdf->AddPage();
                    $this->fpdf->SetFont('Arial', '', 8);
                    $this->fpdf->Cell(0, 10, 'Conte�do', 0, 1, 'L');
                    $this->fpdf->SetXY(10, 34);
                    $this->fpdf->MultiCell(0, 5, utf8_decode(strip_tags($c->descricao)), 0, 'L');
                    $this->fpdf->SetX(120);
                    $this->fpdf->Cell(0, 2, 'Autenticar em: https://www.imazoncursos.com.br/autenticacao/', 0, 0);
                    $this->fpdf->SetX(120);
                    $this->fpdf->Cell(0, 10, 'C�digo de autentica�ao: '.$c->codigo_autenticacao, 0, 0);
                }

                $this->fpdf->Output();
                
            }else{
                //redirect("admin/certificados/nao_enviados");
            }
        }
    }

    
    //gera certificado web
    function gerar_etiqueta() {
        $this->load->model('usuarios_model', 'um');
        
        if($this->uri->segment(3)){
            $aluno = base64_decode($this->uri->segment(3));
            $data['usuario'] = $this->um->get_by_email($aluno)->result();
            $params = array('orientation' => 'P', 'unit' => 'mm', 'size' => 'A4');
            $this->load->library('fpdf', $params);
            $this->fpdf->SetFont('Arial', '', 12);
            $this->fpdf->AddPage();
            $this->fpdf->setY(50);
            $this->fpdf->setX(40);
            $this->fpdf->MultiCell(120, 4, "\n" . utf8_decode($data['usuario'][0]->nome) . "\n\n" . utf8_decode($data['usuario'][0]->endereco) . ", " . $data['usuario'][0]->numero . ", " . utf8_decode($data['usuario'][0]->complemento)  . " " . utf8_decode($data['usuario'][0]->bairro). ", \n\n" . utf8_decode($data['usuario'][0]->cidade) . ", " . utf8_decode($data['usuario'][0]->estado) . "\n\nCEP: " . $data['usuario'][0]->cep . "\n\n", 1, 'L');
            $this->fpdf->Output();
        }else{
            $certificados = $this->cm->gera_todas_etiquetas()->result();
            if(count($certificados)>0){
                $params = array('orientation' => 'P', 'unit' => 'mm', 'size' => 'A4');
                $this->load->library('fpdf', $params);
                $params = array('orientation' => 'P', 'unit' => 'mm', 'size' => 'A4');
                foreach($certificados as $c){
                    $data['usuario'] = $this->um->get_by_id_fic($c->email)->result();
                    $this->load->library('fpdf', $params);
                    $this->fpdf->SetFont('Arial', '', 12);
                    $this->fpdf->AddPage();
                    $this->fpdf->setY(50);
                    $this->fpdf->setX(40);
                    $this->fpdf->MultiCell(120, 4, "\n" . utf8_decode($data['usuario'][0]->nome) . "\n\n" . utf8_decode($data['usuario'][0]->endereco) . ", " . $data['usuario'][0]->numero . ", " . utf8_decode($data['usuario'][0]->complemento) . " " . utf8_decode($data['usuario'][0]->bairro). ", \n\n" . utf8_decode($data['usuario'][0]->cidade) . ", " . utf8_decode($data['usuario'][0]->estado) . "\n\nCEP: " . $data['usuario'][0]->cep . "\n\n", 1, 'L');
                }
                $this->fpdf->Output();
            }    
        }
    }

    //gera certificado web
    function gerar_etiqueta_imazon() {
        $this->load->model('usuarios_model', 'um');
        
        if($this->uri->segment(3)){
            $aluno = base64_decode($this->uri->segment(3));
            $data['usuario'] = $this->um->get_by_email($aluno)->result();
            $params = array('orientation' => 'P', 'unit' => 'mm', 'size' => 'A4');
            $this->load->library('fpdf', $params);
            $this->fpdf->SetFont('Arial', '', 12);
            $this->fpdf->AddPage();
            $this->fpdf->setY(50);
            $this->fpdf->setX(40);
            $this->fpdf->MultiCell(120, 2.5, "\n" . utf8_decode($data['usuario'][0]->nome) . "\n\n" . utf8_decode($data['usuario'][0]->endereco) . ", " . $data['usuario'][0]->numero . ", " . $data['usuario'][0]->complemento . " " . utf8_decode($data['usuario'][0]->bairro). ", \n\n" . utf8_decode($data['usuario'][0]->cidade) . ", " . utf8_decode($data['usuario'][0]->estado) . "\n\nCEP: " . $data['usuario'][0]->cep . "\n\n", 1, 'L');
            $this->fpdf->Output();
        }else{
            $certificados = $this->cm->gera_todas_etiquetas_imazon();
            
            if(count($certificados)>0){
                $params = array('orientation' => 'P', 'unit' => 'mm', 'size' => 'A4');
                $this->load->library('fpdf', $params);
                $params = array('orientation' => 'P', 'unit' => 'mm', 'size' => 'A4');
                foreach($certificados as $c){
                    $this->load->library('fpdf', $params);
                    $this->fpdf->SetFont('Arial', '', 12);
                    $this->fpdf->AddPage();
                    $this->fpdf->setY(50);
                    $this->fpdf->setX(40);
                    $this->fpdf->MultiCell(120, 2.5, "\n" . utf8_decode($c->nomeAluno) . "\n\n" . utf8_decode($c->endereco) . ", " . $c->numero . ", " . $c->complemento . " " . utf8_decode($c->bairro). ", \n\n" . utf8_decode($c->cidade) . ", " . utf8_decode($c->estado) . "\n\nCEP: " . $c->cep . "\n\n", 1, 'L');
                }
                $this->fpdf->Output();
            }    
        }
    }
    
    
    public function alterar_status() {

        if ($this->uri->segment(3) !== null && $this->uri->segment(4) !== null) {
            $id = decodifica($this->uri->segment(4));
            $status = $this->uri->segment(3);
            $this->cm->altera_status($id, $status);
            redirect('certificados/nao_enviados');
        }
    }
    
    public function alterar_status_imazon() {
        if ($this->uri->segment(3) !== null && $this->uri->segment(4) !== null) {
            $id = decodifica($this->uri->segment(4));
            $status = $this->uri->segment(3);
            $this->cm->altera_status_imazon($id, $status);
            redirect('certificados/nao_enviados');
        }
    }

}
