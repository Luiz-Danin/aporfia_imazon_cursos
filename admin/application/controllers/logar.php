<?php

class logar extends CI_Controller {

    public function __construct() {
        parent::__construct();
        session_start();
        $this->load->model('usuarios_model', 'um');
    }

    public function index() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'email', 'required|min_length[3]');
        if ($this->form_validation->run() === TRUE) {
            $dados = array('email' => $this->input->post('email'));
            $resultado = $this->um->verifica_login_adm($dados)->result();
            if ((int) count($resultado) === 1) {
                $_SESSION['email_facilitador'] = $resultado[0]->email;
                $_SESSION['nome_facilitador']  = $resultado[0]->nome;
                $_SESSION['login_2'] = true;
                $_SESSION['key_ima'] = substr(md5($_SESSION['email_facilitador'].date("d-m-Y")), 0, 16);
                $this->load->library("phpmailer");
				
				$mail = new PHPMailer;
        		/*$mail->IsSMTP();
        		$mail->SMTPAuth = true;
        		$mail->SMTPSecure = "ssl";
        		$mail->Port = 465;
        		$mail->Host = "177.234.149.209";      // sets GMAIL as the SMTP server  
       		    $mail->Username = "contato";
        		$mail->Password = "268b7dbcf";
        		$mail->IsHTML(true); // Define que o e-mail será enviado como HTML
        		$mail->CharSet = 'utf-8'; // Charset da mensagem (opcional)
        		$mail->Subject = 'Código';
        		$mail->From = 'contato@formacaofacil.com'; // Seu e-mail
       		    $mail->FromName = "Imazon";
                $mail->AddAddress($resultado[0]->email);
				*/
					$mail = new PHPMailer;
                    $mail->IsSMTP();
                    $mail->SMTPAuth = true;
                    $mail->SMTPSecure = "ssl";
                    $mail->Port = 465;
                    $mail->Host = "smtp.gmail.com";      // sets GMAIL as the SMTP server  
                    $mail->Username = "cadastro@imazon.com.br";
                    $mail->Password = "657@beta";
                    $mail->Subject = 'Novo código de acesso Imazon';
                    $mail->Sender = $mail->Username;
                    $mail->From = $mail->Username;
                    $mail->FromName = "Imazon Cursos";
                     $mail->AddAddress($resultado[0]->email);
				$mail->Body = "Seu codigo de acesso é: " . $_SESSION['key_ima'];
                $mail->AltBody = $mail->Body;
                //return $mail->Send();
                //Nova instância do PHPMailer  
				/*
                $mail = new PHPMailer;
                //Informa que será utilizado o SMTP para envio do e-mail  
                $mail->IsSMTP();
                //Informa que a conexão com o SMTP será autênticado  
                $mail->SMTPAuth = true;
                //Configura a segurança para SSL  
                $mail->SMTPSecure = "ssl";
                //Informa a porta de conexão do GAMIL  
                $mail->Port = 465;
                //Informa o HOST do GMAIL  
                $mail->Host = "smtp.gmail.com";      // sets GMAIL as the SMTP server  
                //Usuário para autênticação do SMTP  
                $mail->Username = "atendimento@imazon.com.br";
                //Senha para autênticação do SMTP  
                $mail->Password = "beta1234";
                //Titulo do e-mail que será enviado  
                $mail->Subject = "Novo código de acesso";
                //Preenchimento do campo FROM do e-mail  
                $mail->From = $mail->Username;
                $mail->FromName = "Imazon";
                //E-mail para a qual o e-mail será enviado  
                //$mail->AddAddress($this->input->post('email'));
                $mail->AddAddress($resultado[0]->email);
                //Conteúdo do e-mail  
                $mail->Body = "Seu codigo de acesso é: " . $_SESSION['key_ima'];
                $mail->AltBody = $mail->Body;
                //Dispara o e-mail 
				*/ 

                $enviado = $mail->Send();
                redirect('login/confirm');
            } else {
                session_destroy();
                redirect('login');
            }
        } else {
            session_destroy();
            redirect('login');
        }
    }

    public function confirm() {
        if (isset($_SESSION['login_2']) && isset($_SESSION['email_facilitador'])) {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('senha', 'senha', 'required|min_length[3]');
            $resultado = $this->um->get_dados_login_adm($_SESSION['email_facilitador'])->result();
            if ($_SESSION['key_ima'] == $this->input->post('senha')) {
                $_SESSION['login'] = $resultado[0]->login;
                unset($_SESSION['login_2']);
                unset($_SESSION['key_ima']);
                redirect('assinaturas');
            } else {
                session_destroy();
                redirect('login');
            }
        } else {
            session_destroy();
            redirect('login');
        }
    }

}
