<?php
class logout extends CI_Controller{
    
    public function index(){
        session_start();
        session_destroy();
        redirect('admin/login');
    }
}
