<?php
class Login extends CI_Controller{
    
    public function __construct() {
        parent::__construct();
    }
    
    public function index(){
       session_start();
       session_destroy();
       $this->load->view('adm/tela/login');        
    }
    
    public function confirm(){
       session_start();
       if(isset($_SESSION['login_2']) && isset($_SESSION['email_facilitador'])){
           $this->load->view('adm/tela/login_senha');
       }else{
           session_destroy();
           redirect("login");
       }
               
    }
    
    
}
