<?php

class Termo extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        session_start();
        $this->load->helper('seguranca');
        
        auth();
        $this->seguranca();
        $this->load->model('termos_model', 'tem');
        $this->load->library('form_validation');
        $this->load->view('adm/inc/header');
        $this->load->library('table');
    }

    public function seguranca() {
        if (!isset($_SESSION['nome_facilitador']) && !isset($_SESSION['email_facilitador']) && !isset($_SESSION['login'])) {
            redirect('admin/login');
        }
    }

    public function index() {
        $this->load->library('pagination');
        $inicio = (!$this->uri->segment("3")) ? 0 : $this->uri->segment("3");
        $config['base_url']   = base_url() . 'termo/index';
        $config['uri_segment'] = 3;
        if ($this->input->post('buscar') !== null && strlen($this->input->post('buscar')) >= 2) {
            $config['total_rows'] = $this->tem->count_all(trim($this->input->post('buscar')));
            $config['per_page']   = $config['total_rows'];
        } else {
            $config['per_page']   = 15;
            $config['total_rows'] = $this->tem->count_all();
        }
        if (null !== $this->input->post('buscar') && strlen($this->input->post('buscar')) >= 2) {
           $data['termos'] = $this->tem->get_all($this->input->post('buscar'), $inicio, $config['per_page'])->result(); 
        } else {
           $data['termos'] = $this->tem->get_all(null, $inicio, $config['per_page'])->result();
        }
        
        $this->pagination->initialize($config);
        $data['paginacao'] = $this->pagination->create_links();
        $data["termos"] = $this->tem->get_all()->result();

        $this->load->view('adm/tela/termos', $data);
        $this->load->view('adm/inc/footer');
    }

    public function add() {
        
        //$this->form_validation->set_rules("descricao", "descricao", "required");
        $this->form_validation->set_rules("conteudo", "conteudo", "required");
        if ($this->form_validation->run()) {
            $arquivo = null;
            if ($_FILES['endereco']['name'] !== null) {
                $diretorio = $_SERVER['DOCUMENT_ROOT'] . "/files";
                $ext = pathinfo($_FILES['endereco']['name'], PATHINFO_EXTENSION);
                if ($ext == "pdf") {
                    $nome_arquivo = md5($_FILES['endereco']['name'] . microtime()) . '.' . $ext;
                    if (move_uploaded_file($_FILES['endereco']['tmp_name'], $diretorio . '/' . $nome_arquivo)) {
                        $arquivo = $nome_arquivo;    
                    }else{
                        $_SESSION['msg'] = "Termo não inserido com sucesso! Verifique o tamanho e formato do arquivo e tente novamente.";
                        //redirect("termo");
                    }
                }else{
                    $_SESSION['msg'] = "Termo não inserido com sucesso! Verifique o formato do arquivo e tente novamente.";
                    //redirect("termo");
                }
            }
            $dados = array("descricao"=>$this->input->post("descricao"), "conteudo"=> trim(str_replace('\n', '', $this->input->post("conteudo"))), "status"=>1, "data_publicacao"=>date("Y-m-d h:m:s"));
            
            //print_r($dados);
            //exit;
            if($arquivo!==null){
                $dados["endereco"] = $arquivo; 
            }
            if ($this->tem->add($dados)) {
               $_SESSION['msg'] = "Termo inserido com sucesso!";
            } else {
                $_SESSION['msg'] = "Termo não inserido com sucesso! Verifique os dados e tente novamente.";
            }
            redirect("termo");
        } else {
            $_SESSION['msg'] = "Termo não inserido com sucesso! Verifique os dados e tente novamente.";
        }
    }

}
