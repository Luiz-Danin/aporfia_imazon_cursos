<!DOCTYPE html>
<meta charset="ISO-8859-1">
<!-- Modal -->
<div class="modal fade" id="ver_dados_assinatura" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Log</h4>
      </div>
      <div class="modal-body">
        <?php 
        
            if(count($log)>=1){
                
            $tmpl = array ('table_open' => '<table class="table table-striped">');

            $this->table->set_template($tmpl); 
            $this->table->set_heading('Status', 'Data');
            
            foreach($log as $l){
                
                switch ($l->tipo_alteracao) {
                    case 0: $status = '<span class="label label-info">Aguardando pagamento</span>'; break;
                    case 3: $status = '<span class="label label-success">Ativada</span>'; break;
                }
                
                $this->table->add_row($status, $l->data_alteraca);
            }
            echo $this->table->generate();
            }else{
                echo '<pre>Nenhum registro.</pre>';
            }
        ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>


<script>
$(function(){
    $("#ver_dados_assinatura").modal("show");
});
</script>