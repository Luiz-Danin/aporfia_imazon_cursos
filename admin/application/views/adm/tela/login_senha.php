<!DOCTYPE html>
<html lang="pt">
  
<head>
    <meta charset="ISO-8859-1">
    <title>Login</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="apple-mobile-web-app-capable" content="yes"> 
    
        <link href="<?=base_url()?>/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url()?>/assets/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />

        <link href="<?=base_url()?>/assets/css/font-awesome.css" rel="stylesheet">
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">

        <link href="<?=base_url()?>/assets/css/style.css" rel="stylesheet" type="text/css">
        <link href="<?=base_url()?>/assets/css/pages/signin.css" rel="stylesheet" type="text/css">

</head>

<body>
	
	<div class="navbar navbar-fixed-top">
	
	<div class="navbar-inner">
		
		<div class="container">
			
			<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>
			
			<a class="brand" href="index.html">
				Imazon blog				
			</a>		
			
			<div class="nav-collapse">
				<ul class="nav pull-right">
					
					<li class="">						
						<a href="" class="">
							<i class="icon-chevron-left"></i>
							Voltar ao site
						</a>
						
					</li>
				</ul>
				
			</div><!--/.nav-collapse -->	
	
		</div> <!-- /container -->
		
	</div> <!-- /navbar-inner -->
	
</div> <!-- /navbar -->



<div class="account-container">
	
	<div class="content clearfix">
		
            <form action="<?=base_url('logar/confirm')?>" method="post">
		
			<div class="login-fields">
				<p>Digite a senha enviada para o seu email</p>
				<div class="field">
					<label for="password">Password:</label>
					<input type="password" id="senha" name="senha" placeholder="Senha" class="login password-field" required/>
				</div> <!-- /password -->
                               
			</div> <!-- /login-fields -->
			
			<div class="login-actions">
				
				<button class="button btn btn-success btn-large">Entrar</button>
				
			</div> <!-- .actions -->
			
			
			
		</form>
		
	</div> <!-- /content -->
	
</div> <!-- /account-container -->




<script src="<?=base_url()?>/assets/js/jquery-1.7.2.min.js"></script>
<script src="<?=base_url()?>/assets/js/bootstrap.js"></script>

<script src="<?=base_url()?>/assets/js/signin.js"></script>

</body>

</html>
