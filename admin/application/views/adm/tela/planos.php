<script src="https://raw.githubusercontent.com/plentz/jquery-maskmoney/master/dist/jquery.maskMoney.min.js" type="text/javascript"></script>
<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-pencil"></i>
	      				<h3>Planos</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						<div class="tabbable">
<?php
if(count($planos) > 0){

$tmpl = array ('table_open' => '<table class="table table-striped">');

$this->table->set_template($tmpl); 
    $this->table->set_heading('Nome', 'Valor', 'Validade', 'Status','Opções');
    foreach($planos as $p){
        if($p->status == 0){
            $status = '<a href="'.  base_url("planos/ativar").'/'.codifica($p->id).'/'.codifica($p->codigo).'" ><span class="label label-danger">Ativar</span></a>';
        }else{
            $status = '<a href="'.  base_url("planos/del").'/'.codifica($p->id).'/'.codifica($p->codigo).'" ><span class="label label-success">Desativar</span></a>';
        }
        $this->table->add_row($p->nome, $p->valor, $p->validade.' mes(es)', $status,'<a href="'.  base_url("planos/edit").'/'.codifica($p->id).'" ><span class="label label-info">Editar</span></a> <a href="'.  base_url("planos/del").'/'.codifica($p->id).'/'.codifica($p->codigo).'" onclick="return confirm(\'Deseja realmente excluir?\')"><span class="label label-danger">Excluir</span></a>');
        
    }
    echo $this->table->generate();
}else{
    echo '<hr><pre>Nenhum plano cadastrado.</pre>';
} 

?>

</div>
						<!-- Button to trigger modal -->
<a type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal">Novo plano</a>
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Novo Plano</h4>
      </div>
      <div class="modal-body">
        
          <?php echo validation_errors(); ?>
<?=form_open('planos/add');?>

<?=form_fieldset('Nome');?>
<?php 
$nome = array(
              'name'        => 'nome',
              'maxlength'   => '12',
              'value'       => set_value('nome'),
              'required'    => 'true'
            ); 
?>
<?php echo form_error('nome'); ?>
<?=form_input($nome);?>
<?=form_fieldset_close();?> 

<?=form_fieldset('Valor');?>
<?php
$valor = array(
              'name'        => 'valor',
              'maxlength'   => '12',
              'required'    => 'true',
              'type'        => 'text',
              'class'       => 'moeda',
              'value'       => set_value('valor')
        ); 
?>
 <?php echo form_error('valor'); ?>
<?=form_input($valor);?>
<?=form_fieldset_close();?> 
 
<?php echo form_error('validade'); ?>
<?=form_fieldset('Validade');?>
<?php
$validade = array(
              'name'        => 'validade',
              'maxlength'   => '12',
              'value'       => set_value('validade'),
              'required'    => 'true',
              'type'        => 'number',
	      'placeholder' => 'Em meses',
	
            ); 
?>
<?=form_input($validade);?>
<?=form_fieldset_close();?> 
          
<?=form_fieldset('Descrição');?>
<?=form_textarea(array('name'=>'descricao'))?>          
<?=form_fieldset_close();?> 
          
<?php
echo '<hr>';
$botao = array('name'=>'alterar', 'value'=>"Cadastrar", 'class'=>"btn btn-success");

?>
<?=form_submit($botao);?>
<?=form_close();?>
     </div>
    </div>
  </div>
</div>


<script>
  $(function() {
    $('.moeda').maskMoney();
  })
</script>