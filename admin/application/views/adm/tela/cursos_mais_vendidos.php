<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-pencil"></i>
	      				<h3>Cursos Mais Vendidos</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						<div class="tabbable">
                                                    
						<br>
                                                
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th>Título</th>
                                                        </tr>
                                                    </thead>
                                                    <?php foreach($dados as $d){?>
                                                    <tbody>
                                                        <tr>
                                                            <td><?=$d->nome?></td>
                                                        </tr>
                                                    </tbody>
                                                    <?php }?>
                                                </table> 
                                                <?=$paginacao?>
                                                						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
