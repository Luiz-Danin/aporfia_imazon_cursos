<div class="main">

    <div class="main-inner">

        <div class="container">

            <div class="row">

                <div class="span12">      		

                    <div class="widget ">

                        <div class="widget-header">
                            <i class="icon-pencil"></i>
                            <h3>Posts (Blog)</h3>
                        </div> <!-- /widget-header -->

                        <div class="widget-content">
                            <div class="tabbable">
                                <form method="POST" action="">
                                    <label for="consulta">Buscar:</label>
                                    <?php if (!isset($_POST['consulta'])) { ?>
                                        <input type="text" id="consulta" name="consulta" maxlength="255" required/>
                                    <?php } else { ?>
                                        <input type="text" id="consulta" value="<?= $_POST['consulta'] ?>" name="consulta" maxlength="255" required/>
                                    <?php } ?>


                                    <input type="submit" value="OK" />
                                </form> 
                                <br>

                                <table class="table">
                                    <thead>
                                    <th>Nome</th>
                                    <th width="10%">Data de cadastro</th>
                                    <th width="4%"></th>
                                    </thead>
                                    <?php
                                    foreach ($posts as $d) {

                                        if ($d->status == 1) {
                                            $btn_link = '<a href="' . current_url() . '/desativar/' . codifica($d->id) . '"><i class="icon icon-ok"></i></a>';
                                        } else {
                                            $btn_link = '<a href="' . current_url() . '/ativar/' . codifica($d->id) . '"><i class="icon icon-remove"></i></a>';
                                        }
                                        ?>
                                        <tbody>
                                            <tr>
                                                <td><a href="<?= current_url() ?>/editar/<?= codifica($d->id) ?>"><?= $d->titulo ?></a></td>
                                                <td><?= date('d/m/Y', strtotime($d->dt_cadastro)); ?></td>
                                                <td><a href="<?= current_url() ?>/del/<?= codifica($d->id) ?>" onclick="return confirm('Deseja realmente excluir esta postagem?')"><i class="icon icon-trash"></i></a> <?= $btn_link ?></td>
                                            </tr>
                                        </tbody>
                                    <?php } ?>
                                </table> 

                                <!-- Button to trigger modal -->
                                <a type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-add-categoria">Nova categoria</a>

                            </div>

                        </div> <!-- /widget-content -->

                    </div> <!-- /widget -->

                </div> <!-- /span8 -->




            </div> <!-- /row -->

        </div> <!-- /container -->

    </div> <!-- /main-inner -->

</div> <!-- /main -->

<!-- Modal -->
<div class="modal fade" id="modal-add-categoria" tabindex="-1" role="dialog" aria-labelledby="modal-add-categoriaLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <form action="<?= base_url('blog/posts/add') ?>" method="post">      
                    <div class="form-group">
                        <label for="titulo">Título</label>
                        <input type="text" class="form-control"  id="titulo" required="" name="titulo">
                    </div>

                    <select class="form-control" name="categoria">
                        <?php
                        foreach ($categorias as $d) {
                            echo '<option value="' . $d->id . '">' . $d->nome . '</option>';
                        }
                        ?>
                    </select>
                    <div class="form-group">
                        <label for="Conteudo">Conteúdo</label>  
                        <textarea class="form-control ckeditor" name="conteudo" rows="3"></textarea>
                    </div>                     
                    <button type="submit" class="btn btn-success">Alterar</button>
                    <a class="btn btn-primary" role="button" data-toggle="collapse" href="#inserir-img-anexo" aria-expanded="false" aria-controls="inserir-img-anexo">
                        Inserir imagem
                    </a>
                </form>

                <div class="collapse" id="inserir-img-anexo">
                    <div class="well">
                        <form action="<?= base_url('blog/posts/envia_img') ?>" method="post" id="upload_img" onsubmit="return false;">   
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"></div>
                                <div>
                                    <input type="submit" class="btn btn-success fileinput-exists" id="upload_img" value="Enviar">
                                    <span class="btn btn-default btn-file">
                                        <span class="fileinput-new">Escolher imagem</span>
                                        <span class="fileinput-exists">Escolher outra</span>
                                        <input type="file" name="img-nova">
                                    </span>
                                    <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Excluir</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    $('#upload_img').submit(function () {
        $(this).ajaxSubmit({
            success: function (e) {
                img = '<img src="../../uploads/img' + e + '">';
                conteudo = CKEDITOR.instances.conteudo.getData();
                novoConteudo = $("#files").html();
                CKEDITOR.instances.conteudo.setData(conteudo + img);
                $("#add-conteudo-upload").css('display', 'none');
                $('.fileinput').fileinput('reset');
            }
        });
        return false;
    });
</script>