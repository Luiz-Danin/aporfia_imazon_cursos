<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-pencil"></i>
	      				<h3>Certificados Enviados</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						<div class="tabbable">
                                                    <form class="form-inline" action="<?=base_url("admin/certificados/enviados")?>" method="post">
                                                    <div class="form-group">
                                                      <label class="sr-only" for="buscar">Pesquisar</label>
                                                      <div class="input-group">
                                                          <div class="input-group-addon">
                                                              <i class="glyphicon glyphicon-search"></i>
                                                          </div>
                                                          <input type="text" class="form-control" id="busca"r name="buscar" placeholder="Buscar por nome ou email">
                                                      </div>
                                                    </div>
                                                  </form>
                                                <?php if(null !== $this->input->post('buscar') && strlen($this->input->post('buscar'))>=3) echo count($certificados).' resultado(s) para <b>"'.$this->input->post('buscar').'"</b>';?>    
                                                <?php

                                                if(count($certificados)>0){

                                                   $tmpl = array ('table_open'          => '<table class="table table-striped">');

                                                   $this->table->set_template($tmpl); 

                                                   $this->table->set_heading('Aluno', 'Data de emissão', 'Email', 'Curso', 'Enviado', 'Endereço');
                                                   foreach($certificados as $c){
                                                       
                                                       
                                                       $status = '  <a href="' . base_url("admin/certificados/alterar_status") . '/0/' . codifica($c['id']) . '"><span class="label label-danger" style="padding: 2px;">
                                                                                                                          <i class="glyphicon glyphicon-minus"></i> Marcar como não enviado
                                                                                                                        </span></a> ';
                                        
                                                       
                                                       
                                                       $endereco = '<span class="pop label label-info" style="padding: 2px;" data-container="body" data-toggle="popover" data-placement="bottom" data-content="'.$c['endereco'].', '.$c['numero'].', '.$c['bairro'].', '.$c['cidade'].', '.$c['estado'].'">
                                                                      <i class="glyphicon glyphicon-info-sign"></i> Ver
                                                                    </span>';
                                                       
                                                       if((int)$c['status'] === 1){
                                                           $c['status'] = '<span class="label label-success">Sim</span>';
                                                       }else{
                                                           $c['status'] = '<span class="label label-danger">Não</span>';
                                                       }
                                                       
                                                       $this->table->add_row(str_replace(strtoupper($this->input->post('buscar')), "<b>".strtoupper($this->input->post('buscar'))."</b>", $c['aluno']), date('d/m/Y', strtotime($c['cadastro'])), str_replace($this->input->post('buscar'), "<b>".$this->input->post('buscar')."</b>", $c['email']), $c['curso'], $c['status'].$status, $endereco);
                                                   }
                                                   echo $this->table->generate();  
                                                }else{
                                                    echo '<hr><pre>Nenhum item encontrado.</pre>';
                                                }

                                                ?>
                                                </div>
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	<?=$paginacao?>
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->

<script>
$(document).ready(function(){
    $('.pop').click(function(){
        $(this).popover('show');
    });
});
</script>