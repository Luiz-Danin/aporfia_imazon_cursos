<div class="main">

    <div class="main-inner">

        <div class="container">

            <div class="row">

                <div class="span12">      		

                    <div class="widget ">

                        <div class="widget-header">
                            <i class="icon-pencil"></i>
                            <h3>Termos</h3>
                        </div> <!-- /widget-header -->

                        <div class="widget-content">

                            <div class="tabbable">
                                <form method="POST" action="<?= base_url('termo/') ?>">
                                    <label for="consulta">Buscar:</label>
                                    <?php if (!isset($_POST['buscar'])) { ?>
                                        <input type="text" id="buscar" name="buscar" maxlength="255" required/>
                                    <?php } else { ?>
                                        <input type="text" id="buscar" value="<?= $_POST['buscar'] ?>" name="buscar" maxlength="255" required/>
                                    <?php } ?>
                                    <input type="submit" value="OK" />
                                </form> 
                                <br>

                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Descrição</th>
                                            <th width="24%">Endereço</th>
                                            <th width="24%">Data de publicação</th>
                                            <th width="24%">Data de atualização</th></th>
                                            <th width="4%">Status</th>
                                        </tr>
                                    </thead>
                                    <?php
                                    foreach ($termos as $a) {

                                        if ($a->status == 0) {
                                            $status = '<span class="label label-danger">Inativo</span>';
                                        } else {
                                            $status = '<span class="label label-success">Ativo</span>';
                                        }
                                        if($a->data_atualizacao!=="0000-00-00 00:00:00"){
                                            $atualizacao = $a->data_atualizacao;
                                        }else{
                                            $atualizacao = "Nenhuma";
                                        }
                                        //date("d/m/Y", strtotime($a->data_atualizacao ))
                                        ?>
                                        <tbody>
                                            <tr>
                                                <td><?= $a->descricao ?></td>
                                                <td><?= $a->endereco ?></td>
                                                <td><b><?= date("d/m/Y", strtotime($a->data_publicacao))?></b></td>
                                                <td><b><?= $atualizacao?></b></td>
                                                <td><b><?= $status ?></b></td>
                                            </tr>
                                        </tbody>
                                    <?php } ?>
                                </table> 
                                <?= $paginacao ?>

                                <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapse-novo-termo" aria-expanded="false" aria-controls="collapse-novo-termo">
                                    Adicionar novo
                                </button>
                                <div class="collapse" id="collapse-novo-termo">
                                    <br />
                                    <div class="well">
                                        <?php echo form_open_multipart('termo/add');?>
                                            <div class="form-group">
                                                <label for="Descricao">Descri&ccedil;&atilde;o</label>  
                                                <textarea class="form-control ckeditor" name="descricao" rows="3"></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label for="Conteudo">Conte&uacute;do</label>  
                                                <textarea class="form-control ckeditor" name="conteudo" rows="3"></textarea>
                                            </div> 
                                            <div class="form-group">
                                                <label for="arquivo_pdf">Arquivo PDF</label>
                                                <input type="file" id="arquivo_pdf" name="endereco">
                                                <p class="help-block">Somente arquivos em formato PDF.</p>
                                            </div>
                                            <button type="submit" class="btn btn-success">Inserir</button>
                                        </form>
                                    </div>
                                </div>

                            </div> <!-- /widget-content -->

                        </div> <!-- /widget -->

                    </div> <!-- /span8 -->




                </div> <!-- /row -->

            </div> <!-- /container -->

        </div> <!-- /main-inner -->

    </div> <!-- /main -->
