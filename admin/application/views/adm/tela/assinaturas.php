<div class="main">

    <div class="main-inner">

        <div class="container">

            <div class="row">

                <div class="span12">      		

                    <div class="widget ">

                        <div class="widget-header">
                            <i class="icon-pencil"></i>
                            <h3>Alunos</h3>
                        </div> <!-- /widget-header -->

                        <div class="widget-content">

                            <div class="tabbable">
                                <form method="GET" action="<?= base_url('assinaturas') ?>">
                                    <label for="consulta">Buscar:</label>
                                    <?php if (!isset($_GET['buscar'])) { ?>
                                        <input type="text" placeholder="Digite o nome" id="buscar" name="buscar" maxlength="255" required/>
                                    <?php } else { ?>
                                        <input type="text" placeholder="Digite o nome" id="buscar" value="<?= $_GET['buscar'] ?>" name="buscar" maxlength="255" required/>
                                    <?php } ?>
                                    <input type="submit" value="OK" />
                                </form> 

                                <?php
                                if (isset($faturas) && count($faturas) > 0) {

                                    $tmpl = array('table_open' => '<table class="table table-striped">');

                                    $this->table->set_template($tmpl);
                                    $this->table->set_heading('Usuario', 'Financeiro', 'Acad&ecirc;mico', 'Email', 'Id fic.', 'Vip at&eacute;', 'Op&ccedil;&otilde;es');
                                    foreach ($faturas as $p) {
                                        $opcoes = '<div class="btn-group" role="group" aria-label="...">
                                                        <div class="btn-group" role="group">
                                                          <button type="button" class="btn btn-default btn-adicionar-curso-imazon" email="' . $p->id_fic . '" nome="' . $p->nome . '" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            Curso
                                                            <span class="glyphicon glyphicon-plus"></span>
                                                          </button>
                                                        </div>
                                                      </div>';
                                        $this->table->add_row($p->nome . ' <a href="#" class="dados" email="' . $p->id_fic . '" nome="' . $p->nome . '">'
                                                . '<i class="glyphicon glyphicon-pencil"></i>'
                                                . '</a>', '<a href="#" class="financeiro" email="' . $p->id_fic . '" nome="' . $p->nome . '">$VIP</a> | '
                                                . '<a href="#" class="financeiro-imazon" email="' . $p->id_fic . '" nome="' . $p->nome . '">$Imazon</a>', ' <a href="#" class="academico" email="' . $p->id_fic . '" nome="' . $p->nome . '">VIP</a> | '
                                                . '<a href="#" class="academico-imazon" email="' . $p->id_fic . '" nome="' . $p->nome . '">Imazon</a>', $p->email, $p->id_fic, date('d/m/Y', strtotime($p->dt_val_ass)), $opcoes);
                                    }
                                    echo $this->table->generate();
                                } else {
                                    echo '<hr><pre>Digite o nome do aluno.</pre>';
                                }
                                ?>
                            </div>
                            <?php
                            if ($paginas > 0 && $_REQUEST['buscar']) {
                                $i = 0;
                                $indice = 0;
                                echo '<nav><ul class="pagination">';
                                while ($i < $paginas) {
                                    if (isset($_REQUEST['id']) && $_REQUEST['id'] == $indice) {
                                        echo '<li class="disabled"><a href="' . base_url('assinaturas/index?buscar=' . $_REQUEST['buscar']) . '&id=' . $indice . '"/><b>' . $i . '</b></a></li>';
                                    } else {
                                        echo '<li><a href="' . base_url('assinaturas/index?buscar=' . $_REQUEST['buscar']) . '&id=' . $indice . '"/>' . $i . '</a></li>';
                                    }
                                    $i++;
                                    $indice = $indice + $por_pagina;
                                }
                                echo '</ul></nav>';
                            }
                            ?>						
                        </div> <!-- /widget-content -->

                    </div> <!-- /widget -->

                </div> <!-- /span8 -->


            </div> <!-- /row -->

        </div> <!-- /container -->

    </div> <!-- /main-inner -->

</div> <!-- /main -->    

<!-- Modal -->
<div class="modal fade" id="modal-financeiro-imazon" tabindex="-1" role="dialog" aria-labelledby="label-financeiro-imazon">
    <div class="modal-dialog" role="document">
        <div style="width: 150%; float: left; margin-left: -25%;" class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="label-financeiro-imazon"></h4>
            </div>
            <div class="modal-body">
                <table class="table table-striped">
                    <thead>
                        <tr>
                          <!--<td>Descri&ccedil;&atilde;o</td>-->
                            <td>Matr&iacute;cula</td>  
                            <td>Pedido</td>
                            <td>Valor</td>
                            <td>Descri&ccedil;&atilde;o</td>
                            <td>Gerado em</td>
                            <td>Pago em</td>
                            <td>Situa&ccedil;&atilde;o</td>
                            <td>Tipo pagamento</td>
                        </tr>
                    </thead>
                    <tbody class="mbody-financeiro-imazon"></tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modal-financeiro" tabindex="-1" role="dialog" aria-labelledby="label-financeiro">
    <div class="modal-dialog" role="document">
        <div style="width: 116%;" class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="label-financeiro"></h4>
            </div>
            <div class="modal-body">
                <table class="table table-striped">
                    <thead>
                        <tr>
                          <!--<td>Descri&ccedil;&atilde;o</td>-->
                            <td>#</td>  
                            <td>Pedido</td>
                            <td>Gerado em</td>
                            <td>Situa&ccedil;&atilde;o</td>
                            <td>Tipo pagamento</td>
                        </tr>
                    </thead>
                    <tbody class="mbody-financeiro"></tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modal-academico" tabindex="-1" role="dialog" aria-labelledby="label-academico">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width: 900px; float: left; margin: 0% 0% 0% -25%;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="label-academico"></h4>
            </div>
            <div class="modal-body" style="max-height: 500px; overflow-y: scroll;">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <td>Hist&oacute;rico</td>
                            <td>Curso</td>
                            <td>Situa&ccedil;&atilde;o</td>
                            <td>CH</td>
                            <td>Dt. matr&iacute;cula</td>
                            <td>Dt. conclus&atilde;o</td>
                            <td>Prazo finaliza&ccedil;&atilde;o</td>
                        </tr>
                    </thead>
                    <tbody class="mbody-academico"></tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modal-academico-imazon" tabindex="-1" role="dialog" aria-labelledby="label-academico-imazon">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width: 900px; float: left; margin: 0% 0% 0% -25%;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="label-academico-imazon"></h4>
            </div>
            <div class="modal-body" style="max-height: 500px; overflow-y: scroll;">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <td>Matr&iacute;cula</td>
                            <td>Curso</td>
                            <td>Nota</td>
                            <td>Origem</td>
                            <td>Situa&ccedil;&atilde;o</td>
                            <td>CH</td>
                            <td>Dt. matr&iacute;cula</td>
                            <td>Dt. conclus&atilde;o</td>
                            <td>Prazo finaliza&ccedil;&atilde;o</td>
                        </tr>
                    </thead>
                    <tbody class="mbody-academico-imazon"></tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modal-dados" tabindex="-1" role="dialog" aria-labelledby="label-dados">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="label-dados"></h4>
            </div>
            <div class="modal-body">
                <form id="form-alt-dados-aluno" onsubmit="return false;">
                    <div class="form-group">
                        <label for="nome">Nome</label>
                        <input type="text" class="form-control" id="nome" name="nome">
                    </div> 
                    <div class="form-group">
                        <label for="apelido">Apelido</label>
                        <input type="text" class="form-control" id="apelido" name="apelido">
                    </div>   
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" id="email" name="email">
                    </div>
                    <div class="form-group">
                        <label for="cep">CEP</label>
                        <input type="text" class="form-control" id="cep" name="cep">
                    </div>  
                    <div class="form-group">
                        <label for="endereco">Endere&ccedil;o</label>
                        <input type="text" class="form-control" id="endereco" name="endereco">
                    </div>
                    <label for="telefone">N&CircleDot; e Complemento</label> 
                    <div class="row">
                        <div class="col-xs-2">
                            <input type="text" class="form-control" name="numero" id="numero">
                        </div>
                        <div class="col-xs-4">
                            <input type="text" class="form-control" name="complemento" id="complemento">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="bairro">Bairro</label>
                        <input type="text" class="form-control" id="bairro" name="bairro">
                    </div>
                    <div class="form-group">
                        <label for="cidade">Cidade</label>
                        <input type="text" class="form-control" id="cidade" name="cidade">
                    </div>      
                    <div class="form-group">
                        <label for="estado">Estado</label>
                        <input type="text" class="form-control" id="estado" name="estado">
                    </div>      
                    <label for="telefone">Telefone (DDD + Telefone)</label> 
                    <div class="row">
                        <div class="col-xs-2">
                            <input type="text" class="form-control" name="ddd_telefone" id="ddd_telefone">
                        </div>
                        <div class="col-xs-4">
                            <input type="text" class="form-control" name="telefone" id="telefone">
                        </div>
                    </div>
                    <label for="telefone">Celular (DDD + Celular)</label> 
                    <div class="row">
                        <div class="col-xs-2">
                            <input type="text" class="form-control" name="ddd_celular" id="ddd_celular">
                        </div>
                        <div class="col-xs-4">
                            <input type="text" class="form-control" name="celular" id="celular">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Data">Data de nascimento*</label>
                        <input type="text" name="data_nasc" class="form-control" data-mask="99-99-9999" id="data_nasc" placeholder="Somente n&uacute;meros">
                    </div>
                    <div class="form-group">
                        <label for="data_validade">Data de validade*</label>
                        <input type="text" name="dt_val_ass" class="form-control" data-mask="99-99-9999" required="" id="dt_val_ass" placeholder="Somente n&uacute;meros">
                    </div>
                    <hr>
                    <input type="hidden" name="id_aluno" id="id_aluno"/>
                    <button type="submit" class="btn btn-default">Salvar</button>
                </form>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="modal-adicionar-curso-imazon" tabindex="-1" role="dialog" aria-labelledby="label-adicionar-curso-imazon">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="label-adicionar-curso-imazon">Adicionar curso (Imazon) - <span id="lbl-adicionar-curso-imazon-nome"></span></h4>
            </div>
            <div class="modal-body">
                <form class="form-inline">
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text" class="form-control" id="ac-busca-imazon" placeholder="Pelo menos 3 caracteres">
                            <div class="input-group-addon limpar-busca-autocomplete">Limpar</div>
                        </div>
                    </div>
                    <hr>
                    <div class="resultado-texto"></div>
                </form>
            </div>
        </div>
    </div>
</div>


<script>
//    $(document).ready(function(){
//        alert('teste');
//    });
//    
    
    function matricularCurso(id) {
        var email = $(".btn-adicionar-curso-imazon").attr("email");
        if (email.length > 4 && id != undefined) {
            if (confirm("Deseja cadastrar este curso para este aluno?")) {
                var ch = prompt("Digite a carga horária");
                if(ch<10){
                    alert("Valor digitado incorretamente!");
                }else{
                    $.post("<?= base_url("cursos/imazon/ajax_adiciona_curso_aluno") ?>", {carga_horaria:ch, id_fic: email, curso:id}, function (e) {
//                        alert(e.toSource());
                        if(e.resposta == "ok"){
                            alert("Feito com sucesso!");
                            $("#lista-curso-" + id).remove();
                        }else{
                            alert("Não foi possível cadastrar o curso. Tente novamente mais tarde.");
                        }
                    });
                }    
            }
        }
    }

    $(function () {
        $("#ac-busca-imazon").keyup(function () {
            
            var titulo = $(this).val().trim();
           $(".resultado-texto").html('');
            if (titulo.length > 3) {
                $(".resultado-texto").html('<p>Aguarde...</p>');
                $(this).focus();
                $.post("<?= base_url("cursos/imazon/ajax_autocomplete_busca_curso") ?>",  {nome: titulo}, function (e) {
                    //alert(e.toSourge());
                    var x = "";
                    var txt = "";
                    var data = e, x, txt;
                    
                    for (x in data) {
                        txt += '<a href="#" class="list-group-item" id="lista-curso-' + data[x].id_curso + '" onclick="return matricularCurso(' + data[x].id_curso + ')">' + data[x].nome + ' <span class="badge"><i class="glyphicon glyphicon-plus"></i></span></a>';
                    }
                    txt = txt.replace("undefined", "Resultados para <b>" + titulo + "</b>.");
                    $(".resultado-texto").html('<div class="list-group" style="height: 200px; overflow:auto;">' + txt + '</div>');
                });
                
            }
        });
    });

//    $(function () {
//        $("#ac-busca-imazon").keyup(function () {
//            alert('teste');
//            var titulo = $(this).val().trim();
//            if (titulo.length == 0) {
//                $("#ac-busca-imazon").html("");
//                $(".resultado-texto").html("");
//            }
//        });
//    });

    $(function () {
        $(".limpar-busca-autocomplete").click(function () {
            $(".resultado-texto").html("");
        });
    });

    $(function () {
        $(".btn-adicionar-curso-imazon").click(function () {
            var email = $(this).attr("email"),
                    nome = $(this).attr("nome");
            $(".resultado-texto").html("");
            $("#lbl-adicionar-curso-imazon-nome").html(nome + " <b>(" + email + ")</b>");
            $("#modal-adicionar-curso-imazon").modal("show");
        });
    });


    $(function () {
        $(".financeiro").click(function () {
            var nome = $(this).attr('nome');
            var idfic = $(this).attr('email');
            $('#label-financeiro').html('Financeiro - ' + nome + ' - ' + idfic);
            $("#modal-financeiro").modal('show');
            $('.mbody-financeiro').html('');
            var email = $(this).attr("email");
            $.post("<?= base_url('assinaturas/verifica_ceritificado_impressso_por_aluno') ?>", {"email": email}, function (e) {
                var x, txtPagamento, txtTipoPagamento;
                for (x in e) {
                    switch (e[x].tipo_pagamento) {
                        case "10":
                            txtTipoPagamento = '<span class="label label-default">Boleto</span>';
                            break;
                        case "1":
                            txtTipoPagamento = '<span class="label label-info">Visa</span>';
                            break;
                        case "2":
                            txtTipoPagamento = '<span class="label label-danger">Mastercard</span>';
                            break;
                        case "37":
                            txtTipoPagamento = '<span class="label label-danger">American</span>';
                            break;
                        case "45":
                            txtTipoPagamento = '<span class="label label-danger">Aura</span>';
                            break;
                        case "55":
                            txtTipoPagamento = '<span class="label label-danger">Dinners</span>';
                            break;
                        case "56":
                            txtTipoPagamento = '<span class="label label-success">Hypercard</span>';
                            break;
                        default:
                            txtTipoPagamento = '<span class="label label-success">Boleto</span>';
                            break;
                    }

                    switch (e[x].pagamento) {
                        case "0":
                            txtPagamento = '<span class="label label-info">Pendente</span>';
                            break;
                        case "1":
                            txtPagamento = '<span class="label label-info">Em Andamento</span>';
                            break;
                        case "5":
                            txtPagamento = '<span class="label label-danger">Reclamação</span>';
                            break;
                        case "6":
                            txtPagamento = '<span class="label label-danger">Devolvida</span>';
                            break;
                        case "7":
                            txtPagamento = '<span class="label label-danger">Cancelado</span>';
                            break;
                        case "8":
                            txtPagamento = '<span class="label label-danger">Fraude</span>';
                            break;
                        case "3":
                            txtPagamento = '<span class="label label-success">Pago</span>';
                            break;
                        case "4":
                            txtPagamento = '<span class="label label-success">Pago</span>';
                            break;
                    }

                    $('.mbody-financeiro').append('<tr><td>Cert. Imp.</td><td>' + e[x].pedido + '</td><td>' + e[x].cadastro + '</td><td>' + txtPagamento + '</td><td>' + txtTipoPagamento + '</td></tr>');
                }
            });
            $.post("<?= base_url('assinaturas/verifica_transacoes_por_aluno') ?>", {"email": email}, function (e) {
                var x, txtPagamento;
                for (x in e) {
                    switch (e[x].situacao) {
                        case "0":
                            txtPagamento = '<span class="label label-info">Pendente</span>';
                            break;
                        case "1":
                            txtPagamento = '<span class="label label-info">Em Andamento</span>';
                            break;
                        case "5":
                            txtPagamento = '<span class="label label-danger">Reclamação</span>';
                            break;
                        case "6":
                            txtPagamento = '<span class="label label-danger">Devolvida</span>';
                            break;
                        case "7":
                            txtPagamento = '<span class="label label-danger">Cancelado</span>';
                            break;
                        case "8":
                            txtPagamento = '<span class="label label-danger">Fraude</span>';
                            break;
                        case "3":
                            txtPagamento = '<span class="label label-success">Pago</span>';
                            break;
                        case "4":
                            txtPagamento = '<span class="label label-success">Pago</span>';
                            break;
                    }

                    $('.mbody-financeiro').append('<tr><td>' + e[x].plano + '</td><td>IMAZONVIP' + e[x].transacao + '</td><td>' + e[x].data_compra + '</td><td>' + txtPagamento + '</td><td><span class="label label-default">' + e[x].tipo_pagamento + '</span></td></tr>');
                }
            });
        });
    });
    //financeiro

    $(function () {
        $(".financeiro-imazon").click(function () {
            var nome = $(this).attr('nome');
            var idfic = $(this).attr('email');
            $('#label-financeiro-imazon').html('Financeiro - ' + nome + ' - ' + idfic + ' <b>(Imazon)</b>');
            $("#modal-financeiro-imazon").modal('show');
            $('.mbody-financeiro-imazon').html('');
            var email = $(this).attr("email");
            $.post("<?= base_url('assinaturas/verifica_transacoes_por_aluno_imazon') ?>", {"email": email}, function (e) {
                var x, txtPagamento, txtTipoPagamento;
                for (x in e) {
                    switch (e[x].situacao) {
                        case "0":
                            txtPagamento = '<span class="label label-info">Nada consta</span>';
                            break;
                        case "1":
                            txtPagamento = '<span class="label label-info">Em andamento</span>';
                            break;
                        case "5":
                            txtPagamento = '<span class="label label-danger">Cancelado</span>';
                            break;
                        case "6":
                            txtPagamento = '<span class="label label-danger">Cancelado</span>';
                            break;
                        case "7":
                            txtPagamento = '<span class="label label-danger">Cancelado</span>';
                            break;
                        case "8":
                            txtPagamento = '<span class="label label-danger">Cancelado</span>';
                            break;
                        case "3":
                            txtPagamento = '<span class="label label-success">Aprovado</span>';
                            break;
                        case "4":
                            txtPagamento = '<span class="label label-success">Aprovado</span>';
                            break;
                    }
                    //alert(e[x].tipo_pagamento);
                    switch (parseInt(e[x].tipo_pagamento)) {
                        case 10:
                            txtTipoPagamento = '<span class="label label-default">Boleto</span>';
                            break;
                        case 1:
                            txtTipoPagamento = '<span class="label label-info">Visa</span>';
                            break;
                        case 2:
                            txtTipoPagamento = '<span class="label label-danger">Mastercard</span>';
                            break;
                        case 37:
                            txtTipoPagamento = '<span class="label label-danger">American</span>';
                            break;
                        case 45:
                            txtTipoPagamento = '<span class="label label-danger">Aura</span>';
                            break;
                        case 55:
                            txtTipoPagamento = '<span class="label label-danger">Dinners</span>';
                            break;
                        case 56:
                            txtTipoPagamento = '<span class="label label-success">Hypercard</span>';
                            break;
                        case 0:
                            txtTipoPagamento = '<span class="label label-success">N&atilde;o identificado</span>';
                            break;
                        case 66:
                            txtTipoPagamento = '<span class="label label-success">Saldo Indique e Ganhe</span>';
                            break;
                        default:
                            txtTipoPagamento = '<span class="label label-success">Boleto</span>';
                            break;
                    }

                    $('.mbody-financeiro-imazon').append('<tr><td>' + e[x].historico + '</td><td>' + e[x].transacao + '</td><td>' + e[x].valor + '</td><td><b>' + e[x].nomeProduto + '</b> do curso <b>' + e[x].nomeCurso + '</b></td><td>' + e[x].data_compra + '</td><td>' + e[x].data_pagamento + '</td><td>' + txtPagamento + '</td><td>' + txtTipoPagamento + '</td></tr>');
                }
            })
        });
    });
    //financeiro-imazon


    $(function () {
        $(".academico-imazon").click(function () {
            var nome = $(this).attr('nome');
            var idfic = $(this).attr('email');
            $('#label-academico-imazon').html('Acad&ecirc;mico - ' + nome + ' - ' + idfic + ' (Imazon)');
            $("#modal-academico-imazon").modal('show');
            $('.mbody-academico-imazon').html('');
            var email = $(this).attr("email");
            $.post("<?= base_url('assinaturas/verifica_cursos_aluno_imazon') ?>", {"email": email}, function (e) {
                var x, txtSituacao;
                for (x in e) {
                    
                    switch (e[x].status) {
                        case "0":
                            txtSituacao = '<span class="label label-info" onclick="altera_status_matricula('+e[x].matricula+')" id="matricula-'+e[x].matricula+'">Inscrito</span>';
                            break;
                        case "1":
                            txtSituacao = '<span class="label label-success" onclick="altera_status_matricula('+e[x].matricula+')" id="matricula-'+e[x].matricula+'">Matr&iacute;culado</span>';
                            break;
                        case "2":
                            txtSituacao = '<span class="label label-success ">Aprovado</span>';
                            break;
                        case "3":
                            txtSituacao = '<span class="label label-warning" onclick="altera_status_matricula('+e[x].matricula+')" id="matricula-'+e[x].matricula+'">Reprovado</span>';
                            break;
                        case "4":
                            txtSituacao = '<span class="label label-danger " onclick="altera_status_matricula('+e[x].matricula+')" id="matricula-'+e[x].matricula+'">Jubilado</span>';
                            break;
                        case "5":
                            txtSituacao = '<span class="label label-warning " onclick="altera_status_matricula('+e[x].matricula+')" id="matricula-'+e[x].matricula+'">Cancelado</span>';
                            break;
                        case "6":
                            txtSituacao = '<span class="label label-warning">Arquivado</span>';
                            break;
                        case "7":
                            txtSituacao = '<span class="label label-danger" onclick="altera_status_matricula('+e[x].matricula+')" id="matricula-'+e[x].matricula+'" >Erro</span>';
                            break;
                        case "8":
                            txtSituacao = '<span class="label label-danger" onclick="altera_status_matricula('+e[x].matricula+')" id="matricula-'+e[x].matricula+'" >Congelado</span>';
                            break;
                        case "9":
                            txtSituacao = '<span class="label label-danger" onclick="altera_status_matricula('+e[x].matricula+')" id="matricula-'+e[x].matricula+'" >Bloqueado</span>';
                            break;
                            
                    }
                    $('.mbody-academico-imazon').append('<tr><td>' + e[x].historico + '</td><td>' + e[x].nome + '</td><td>' + e[x].nota + '</td><td>' + e[x].obser + '</td><td>' + txtSituacao + '</td><td>' + e[x].ch + ' h</td><td>' + e[x].cadastro + '</td><td>' + e[x].ultimo_dia + '</td><td>' + e[x].termino + ' &aacute; ' + e[x].conclusao + '</td></tr>');
                }
            });
        });

    });

    function altera_status_matricula(matricula){
    
        if(confirm("Deseja mesmo executar esta operação?")){
            $.post("<?= base_url('assinaturas/altera_status_matricula_imazon') ?>", {"matricula": matricula, "situacao":6}, function (e) {
                if(e.resposta == true){
                    $("#matricula-"+matricula).attr("class", "label label-warning").html("Arquivado");
                }else{
                    alert("Erro ao alterar status.");
                }
            });
        }
    
        
    }

    $(function () {
        $(".academico").click(function () {
            var nome = $(this).attr('nome');
            var idfic = $(this).attr('email');
            $('#label-academico').html('Acad&ecirc;mico - ' + nome + ' - ' + idfic);
            $("#modal-academico").modal('show');
            $('.mbody-academico').html('');
            var email = $(this).attr("email");

            $.post("<?= base_url('assinaturas/verifica_cursos_aluno') ?>", {"email": email}, function (e) {
                var x, txtSituacao;
                for (x in e) {
                    switch (e[x].status) {
                        case "0":
                            txtSituacao = '<span class="label label-warning">Cursando</span>';
                            break;
                        case "1":
                            txtSituacao = '<span class="label label-success">Concluido</span>';
                            break;
                        case "2":
                            txtSituacao = '<span class="label label-danger">Cancelado</span>';
                            break;
                        case "3":
                            txtSituacao = '<span class="label label-info">Jubilado</span>';
                            break;
                        case "4":
                            txtSituacao = '<span class="label label-info">Arquivado</span>';
                            break;
                    }

                    $('.mbody-academico').append('<tr><td>' + e[x].historico + '</td><td>' + e[x].nome + '</td><td>' + txtSituacao + '</td><td>' + e[x].ch + ' h</td><td>' + e[x].cadastro + '</td><td>' + e[x].ultimo_dia + '</td><td>' + e[x].termino + ' &aacute; ' + e[x].conclusao + '</td></tr>');
                }
            });
        });
    });
    //academico
    $(function () {
        $('.dados').click(function () {
            var nome = $(this).attr('nome'), email = $(this).attr('email');
            $('#label-dados').html('Dados - ' + nome);
            $("#modal-dados").modal('show');

            $.post("<?= base_url('assinaturas/verifica_dados_aluno') ?>", {"email": email}, function (e) {
                if (e.data_nasc != null) {
                    var data_nasc = e.data_nasc.split("-");
                    $("#data_nasc").val(data_nasc[2] + '-' + data_nasc[1] + '-' + data_nasc[0]);
                }
                var dt_val_ass = e.dt_val_ass.split("-");

                $("#nome").val(e.nome);
                $("#email").val(e.email);
                $("#apelido").val(e.apelido);
                $("#ddd_telefone").val(e.ddd_telefone);
                $("#telefone").val(e.telefone);
                $("#ddd_celular").val(e.ddd_celular);
                $("#celular").val(e.celular);
                $("#cep").val(e.cep);
                $("#endereco").val(e.endereco);
                $("#numero").val(e.numero);
                $("#complemento").val(e.complemento);
                $("#bairro").val(e.bairro);
                $("#cidade").val(e.cidade);
                $("#estado").val(e.estado);
                $("#id_aluno").val(e.id_fic);
                $("#dt_val_ass").val(dt_val_ass[2] + '-' + dt_val_ass[1] + '-' + dt_val_ass[0]);

            });

        });
    });
    //dados
    $(function () {
        $("#form-alt-dados-aluno").submit(function () {
            $.post("<?= base_url('assinaturas/alt_dados_aluno') ?>", $("#form-alt-dados-aluno").serialize(), function (e) {
//                alert(e.toSource());
                if (e.op == "ok") {
                    alert("Alterado com sucesso!");
                    
                } else {
                    alert("Não foi possível alterar os dados do aluno.");
                }
            }, "json");
        });
    });
    //cep
    jQuery(function ($) {
        $("#cep").change(function () {
            var cep_code = $(this).val();
            if (cep_code.length <= 0)
                return;
            $.get("http://cep.correiocontrol.com.br/" + cep_code + ".json",
                    function (e) {
                        if (e.cep) {
                            $("input#cep").val(e.cep);
                            $("input#estado").val(e.uf);
                            $("input#cidade").val(e.localidade);
                            $("input#bairro").val(e.bairro);
                            $("input#endereco").val(e.logradouro);
                        } else {
                            $("input#cep").focus(function () {
                                alert("CEP incorreto");
                            });
                        }
                    });
        });
    });

    

</script>