<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-pencil"></i>
	      				<h3>Certificados Pendentes</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						<div class="tabbable">
                                                    
                                                    <form class="form-inline" action="<?=base_url("admin/certificados/nao_enviados")?>" method="post">
                                                    <div class="form-group">
                                                      <label class="sr-only" for="buscar">Pesquisar</label>
                                                      <div class="input-group">
                                                          <div class="input-group-addon">
                                                              <i class="glyphicon glyphicon-search"></i>
                                                          </div>
                                                          <input type="text" class="form-control" id="busca"r name="buscar" placeholder="Buscar por nome ou email">
                                                      </div>
                                                    </div>
                                                  </form>
                                                  
                                                    
                                                    <?php if(null !== $this->input->post('buscar') && strlen($this->input->post('buscar'))>=3) echo count($certificados).' resultado(s) para <b>"'.$this->input->post('buscar').'"</b>';?>
                                                    
                                                    <?php

                                                    if(count($certificados)>0){

                                                        $tmpl = array ('table_open'          => '<table class="table table-striped" >');

                                                        $this->table->set_template($tmpl); 

                                                        $this->table->set_heading('Aluno', 'Data de emissão', 'Email', 'Curso', 'Endereço', 'Status');
                                                       
                                                        foreach($certificados as $c){
                                                            
                                                            if((int)$c['status'] === 0){
                                                                $status = '<a onclick="return confirm(\'Deseja realmente enviar este certificado?\')" href="'.base_url("admin/certificados/alterar_status").'/1/'.codifica($c['id']).'"><span class="label label-danger" style="padding: 2px;" >
                                                                                                                          <i class="glyphicon glyphicon-okn"></i> Marcar como enviado
                                                                                                                        </span></a>';
                                                            }else{
                                                                $status = '<a href="'.base_url("admin/certificados/alterar_status").'/0/'.codifica($c['id']).'"><span class="label label-success" style="padding: 2px;">
                                                                                                                          <i class="glyphicon glyphicon-minus"></i> Marcar como não enviado
                                                                                                                        </span></a>';
                                                            }
                                                            
                                                            $endereco = '<span class="pop label label-info" style="padding: 2px;" data-container="body" data-toggle="popover" data-placement="bottom" data-content="'.$c['endereco'].', '.$c['numero'].', '.$c['bairro'].', '.$c['cidade'].', '.$c['estado'].'">
                                                                                                                          <i class="glyphicon glyphicon-info-sign"></i> Ver
                                                                                                                        </span>';

                                                            $btstatus = '';
                                                            //botão gera certificado
                                                            $btstatus.='&nbsp;<a href="'.base_url('certificados/gerar_certificado_impresso/').'/'.base64_encode($c['id_curso']).'/'.base64_encode($c['id_fic']).'" alt="Imprimir Certificado"     target="_blank"><span class="label label-success" style="padding: 2px;">&nbsp;<i class="glyphicon glyphicon-print"></i>&nbsp;</span></a>';

                                                            //botão etiqueta
                                                            $btstatus.='&nbsp;<a href="'.base_url('certificados/gerar_etiqueta/').'/'.base64_encode($c['email']).'" target="_blank"><span class="label label-info" style="padding: 2px;" alt="Imprimir Etiqueta">&nbsp;<i class="glyphicon glyphicon-tags"></i>&nbsp;</span></a>';
                                                            
                                                            $this->table->add_row(str_replace(strtoupper($this->input->post('buscar')), "<b>".strtoupper($this->input->post('buscar'))."</b>", $c['aluno']), date('d/m/Y', strtotime($c['cadastro'])), str_replace($this->input->post('buscar'), "<b>".$this->input->post('buscar')."</b>", $c['email']), $c['curso'], $endereco, $status.$btstatus);
                                                        }  
                                                        
                                                        echo $this->table->generate();  
                                                    }else{
                                                        echo '<hr><pre>Nenhum item encontrado.</pre>';
                                                    }

                                                    ?>
                                                    
                                             </div>
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->

<script>
$(document).ready(function(){
    $('.pop').click(function(){
        $(this).popover('show');
    });
});
</script>