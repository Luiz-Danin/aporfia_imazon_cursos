<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-pencil"></i>
	      				<h3>Cursos (<b>Imazon</b>)</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						<div class="tabbable">
                                                    <form method="POST" action="<?=base_url('cursos/imazon/')?>">
                                                    <label for="consulta">Buscar:</label>
                                                    <?php if(!isset($_POST['buscar'])){?>
                                                        <input type="text" id="buscar" name="buscar" maxlength="255" required/>
                                                    <?php }else{ ?>
                                                        <input type="text" id="buscar" value="<?=$_POST['buscar']?>" name="buscar" maxlength="255" required/>
                                                    <?php }?>
                                                    <input type="submit" value="OK" />
                                                </form> 
						<br>
                                                
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th>Título</th>
                                                            <th width="4%">Publicado</th>
                                                            <th width="4%">CH.</th>
                                                            <th width="2%">Op&ccedil;&otilde;es</th>
                                                        </tr>
                                                    </thead>
                                                    <?php foreach($cursos as $d){?>
                                                    <tbody>
                                                        <tr>
                                                            <td><?=$d->nome?></td>
                                                            <td>
                                                                <?php
                                                                   if($d->status == 1){
                                                                       echo '<a href="'.base_url('cursos/imazon/atualizar').'/'.codifica($d->id_curso).'/'.codifica(0).'"><span class="label label-success">Sim</span></a>';
                                                                   }else{
                                                                       echo '<a href="'.base_url('cursos/imazon/atualizar').'/'.codifica($d->id_curso).'/'.codifica(1).'"><span class="label label-danger">Não</span></a>';
                                                                   }  
                                                                ?>
                                                            </td>
                                                            <td><?=$d->carga_horaria?></td>
                                                            <td>
                                                                <a href="<?=base_url('cursos/imazon/editar')?>/<?=codifica($d->id_curso)?>" title="Editar dados">
                                                                    <i class="glyphicon glyphicon-pencil">                                                                        
                                                                    </i>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                    <?php }?>
                                                </table> 
                                                <button class="btn btn-success" id="btn-add-curso">Novo</button>
                                                <?=$paginacao?>
                                                						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->


<!-- Modal -->
<div class="modal fade" id="modal-add-curso" tabindex="-1" role="dialog" aria-labelledby="modal-add-cursoLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modal-add-cursoLabel">Cadastrar curso</h4>
      </div>
      <div class="modal-body">
        
          <form action="<?=base_url('cursos/imazon/add')?>" method="post">
                                                                    
            <div class="form-group">
              <label for="nome">Nome</label>
              <input type="text" class="form-control"  id="nome" name="nome">
            </div>

            <div class="form-group">
                <label for="Descricao">Descri&ccedil;&atilde;o</label>  
              <textarea class="form-control ckeditor" name="descricao" rows="3"></textarea>
            </div>
              
            <div class="form-group">
                <label for="Questionario">Question&aacute;rio</label>  
              <textarea class="form-control ckeditor" name="questionario" rows="3"></textarea>
            </div>
    
            <div class="form-group">
              <label for="Ordem">Publicar</label>
              <div class="radio">
                <label>
                  <input type="radio" name="status" value="0">
                  Não
                </label>
              </div>
              <div class="radio">
                <label>
                  <input type="radio" name="status" value="1">
                  Sim
                </label>
              </div>
            </div>  
              
            <button type="submit" class="btn btn-success">Criar</button>
            
          </form>
     </div>
    </div>
  </div>
</div>


<script>
$(function(){
    $("#btn-add-curso").click(function(){
        $("#modal-add-curso").modal("show");
    });
});
</script>