<div class="main">
    <div class="main-inner">

        <div class="container">

            <div class="row">

                <div class="span12">      		

                    <div class="widget ">

                        <div class="widget-header">
                            <i class="icon-user"></i>
                            <h3>Editar aula</h3>
                        </div> <!-- /widget-header -->

                        <div class="widget-content">

                            <div class="tabbable">
                                <br />
                                <div class="tab-content">
                                    <div class="tab-pane active" id="formcontrols">

                                        <form action="<?= base_url('cursos/vip/editAula') ?>" method="post">

                                            <div class="form-group">
                                                <label for="Titulo">T&iacute;tulo</label>  
                                                <input class="form-control" name="CATitulo" value="<?= $aula[0]->CATitulo ?>" />
                                            </div>    


                                            <div class="form-group">
                                                <label for="Descricao">Descri&ccedil;&atilde;o</label>  
                                                <textarea class="form-control ckeditor" name="CADescricao" rows="3"><?= $aula[0]->CADescricao ?></textarea>
                                            </div>  
                                            <input type="hidden" name="cursos_aulas_id" value="<?= codifica($aula[0]->cursos_aulas_id) ?>">
                                            <input type="submit" class="btn btn-success" value="Salvar">     
                                            <a href="javascript:history.back()" class="btn btn-default">Voltar</a>
                                        </form>

                                    </div>
                                    <hr>
                                    <p><b>Conte&uacute;do</b></p>
                                     <?php
                echo '<!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation"><a href="#arquivos" aria-controls="arquivos" role="tab" data-toggle="tab">Arquivos</a></li>
    <li role="presentation"><a href="#imagens" aria-controls="imagens" role="tab" data-toggle="tab">Imagens</a></li>
    <li role="presentation"><a href="#videos" aria-controls="videos" role="tab" data-toggle="tab">Videos</a></li>
  </ul>';

                echo '<!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane" id="arquivos">';
                if (count($arquivos_aula) > 0) {
                    echo '<hr><a href="' . base_url('uploads') . '/_files/' . $arquivos_aula[0]->CAACaminho . '" target="_blank" class="btn btn-danger"><i class="glyphicon glyphicon-save"></i>' . $arquivos_aula[0]->CAATitulo . '</a>';
                } else {
                    echo '<pre>Nenhum arquivo para esta aula.</pre>';
                }
                echo '
    </div>
    <div role="tabpanel" class="tab-pane" id="imagens">';
                if (count($imagens_aula) > 0) {
                    echo '<hr><img src="' . base_url('uploads/_files') . '/' . $imagens_aula[0]->CAICaminho . '">';
                } else {
                    echo '<pre>Nenhuma imagem para esta aula.</pre>';
                }
                echo '</div>
    <div role="tabpanel" class="tab-pane" id="videos">';
                if (count($videos_aula) > 0) {
                    echo '<hr><a href="' . $videos_aula[0]->CAVCaminho . '" target="_blank" class="btn btn-primary"><i class="glyphicon glyphicon-facetime-video"></i> ' . $videos_aula[0]->CAVTitulo . '</a>';
                } else {
                    echo '<pre>Nenhum video para esta aula.</pre>';
                }
                echo '
    </div>
  </div>';
                ?>
                                </div>


                            </div>

                        </div> <!-- /widget-content -->

                    </div> <!-- /widget -->

                </div> <!-- /span8 -->

            </div> <!-- /row -->

        </div> <!-- /container -->

    </div> <!-- /main-inner -->

</div> <!-- /main -->

<script>
    $(function () {
        $("#alterar").click(function () {

            var acao = $("#alterar").attr("acao"),
                    acao = acao.toString();

            if (acao == "ativar") {
                $("#alterar").attr("class", "btn btn-danger");
                $("#alterar").attr("acao", "desativar");
                $("#alterar").html("Desativar");

            } else {
                $("#alterar").attr("class", "btn btn-info");
                $("#alterar").attr("acao", "ativar");
                $("#alterar").html("Publicar");

            }

            //envia requisição post ajax
            $.post("<?= base_url('cursos/vip/ajax_altera_status_publicado') ?>", {id: "<?= codifica($dados[0]->cedoc_doc_id_fk) ?>", status: acao}).fail(function () {
                alert("Erro ao alterar status");
            });

        });

    });
</script>