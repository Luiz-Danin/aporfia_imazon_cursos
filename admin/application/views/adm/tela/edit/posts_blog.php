<div class="main">
    <div class="main-inner">

        <div class="container">

            <div class="row">

                <div class="span12">      		

                    <div class="widget ">

                        <div class="widget-header">
                            <i class="icon-user"></i>
                            <h3>Editar post (Blog)</h3>
                        </div> <!-- /widget-header -->

                        <div class="widget-content">

                            <div class="tabbable">

                                <br>

                                <div class="tab-content">
                                    <div class="tab-pane active" id="formcontrols">   
                                        <form action="<?= base_url('blog/posts/edit') ?>" method="post">      
                                            <div class="form-group">
                                                <label for="titulo">Título</label>
                                                <input type="text" class="form-control"  id="titulo" name="titulo" value="<?= $dados[0]->titulo ?>">
                                            </div>
                                            <select class="form-control" name="categoria">
                                                <?php
                                                foreach ($categorias as $d) {
                                                    if ($dados[0]->categoria == $d->id) {
                                                        $selected = 'selected=""';
                                                    } else {
                                                        $selected = '';
                                                    }
                                                    echo '<option value="' . $d->id . '" ' . $selected . '>' . $d->nome . '</option>';
                                                }
                                                ?>
                                            </select>
                                            <div class="form-group">
                                                <label for="Conteudo">Conteúdo</label>  
                                                <textarea class="form-control ckeditor" name="conteudo" rows="3"><?= str_replace("./uploads/", "../../../../uploads/", $dados[0]->conteudo) ?></textarea>
                                            </div>  
                                            <?php
                                            if ($dados[0]->status == 1) {
                                                $btn_link = '<a href="' . base_url('blog/posts') . '/desativar/' . codifica($dados[0]->id) . '/redir" class="btn btn-danger">Desativar</a>';
                                            } else {
                                                $btn_link = '<a href="' . base_url('blog/posts') . '/ativar/' . codifica($dados[0]->id) . '/redir" class="btn btn-success">Ativar</a>';
                                            }
                                            ?>
                                            <input type="hidden" name="id" value="<?= codifica($dados[0]->id) ?>">    
                                            <button type="submit" class="btn btn-success">Salvar</button>
                                            <?= $btn_link ?>
                                            <a class="btn btn-primary" role="button" data-toggle="collapse" href="#inserir-img-anexo" aria-expanded="false" aria-controls="inserir-img-anexo">
                                                Inserir imagem
                                            </a>
                                            <a href="<?= base_url('blog/posts') ?>" class="btn btn-default">Voltar</a>
                                        </form>
                                        
                                        <div class="collapse" id="inserir-img-anexo">
                                            <div class="well">
                                                <form action="<?= base_url('blog/posts/envia_img') ?>" method="post" id="upload_img" onsubmit="return false;">   
                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"></div>
                                                        <div>
                                                            <input type="submit" class="btn btn-success fileinput-exists" id="upload_img" value="Enviar">
                                                            <span class="btn btn-default btn-file">
                                                                <span class="fileinput-new">Escolher imagem</span>
                                                                <span class="fileinput-exists">Escolher outra</span>
                                                                <input type="file" name="img-nova">
                                                            </span>
                                                            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Excluir</a>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- /widget-content -->
                    </div> <!-- /widget -->
                </div> <!-- /span8 -->
            </div> <!-- /row -->
        </div> <!-- /container -->
    </div> <!-- /main-inner -->
</div> <!-- /main -->

<script>
    $('#upload_img').submit(function () {
        $(this).ajaxSubmit({
            success: function (e) {
                img = '<img src="../../../../uploads/img' + e + '">';
                conteudo = CKEDITOR.instances.conteudo.getData();
                novoConteudo = $("#files").html();
                CKEDITOR.instances.conteudo.setData(conteudo + img);
                $("#add-conteudo-upload").css('display', 'none');
                $('.fileinput').fileinput('reset');
            }
        });
        return false;
    });
</script>