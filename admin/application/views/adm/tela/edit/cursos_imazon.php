<div class="main">
    <div class="main-inner">

        <div class="container">

            <div class="row">

                <div class="span12">      		

                    <div class="widget ">

                        <div class="widget-header">
                            <i class="icon-user"></i>
                            <h3>Editar curso</h3>
                        </div> <!-- /widget-header -->

                        <div class="widget-content">

                            <div class="tabbable">

                                <br />

                                <div class="tab-content">
                                    <div class="tab-pane active" id="formcontrols">
                                        <form action="<?= base_url('cursos/imazon/ajax_altera_imagem_curso') ?>" method="post" enctype="multipart/form-data" id="form-ulpoad-img">
                                            <div class="media">
                                                <div class="media-left media-middle">
                                                    <a href="#">
                                                        <img class="media-object" id="img-curso" style="width:100px" src="<?= "http://192.168.0.30/imazoncursos/imgs/cursos/" . $curso[0]->imagem ?>" alt="<?= $curso[0]->nome ?>">
                                                    </a>
                                                </div>
                                                <div class="media-body">
                                                    <h5><b>Imagem do curso</b></h5>
                                                    <div class="form-group">
                                                        <input type="file" name="img-nova" id="img-nova">
                                                        <input type="hidden"name="id_curso" value="<?= codifica($curso[0]->id_curso)?>"/>
                                                        <p class="help-block">Clique no botão acima e selecione outra <b>imagem</b> para este curso.<br /></p>
                                                    </div>
                                                </div>
                                                <div class="progress" style="display: none;">
                                                    <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                                                      <span style="color: rgb(255, 255, 255); font-weight: bold;">Enviando...</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>    
                                        <form action="<?= base_url('cursos/imazon/edit') ?>" method="post" enctype="multipart/form-data">

                                            <div class="form-group">
                                                <label for="Nome">Nome</label>
                                                <input type="text" class="form-control"  id="Nome" name="nome" value="<?= $curso[0]->nome ?>">
                                            </div>



                                            <div class="form-group">
                                                <label for="Conteudo">Conte&uacute;do</label>  
                                                <textarea rows="10" style="width:50%;"  name="questionario" ><?= $curso[0]->questionario ?></textarea>
                                            </div> 

                                            <div class="form-group">
                                                <label for="Descricao">Descrição</label>  
                                                <textarea  name="descricao" rows="10" style="width:50%;" ><?= $curso[0]->descricao ?></textarea>
                                            </div>      

                                            <input type="hidden" name="id_curso" value="<?= codifica($curso[0]->id_curso) ?>">    
                                            <button type="submit" class="btn btn-success">Alterar</button>
                                            <a href="<?= base_url('cursos/imazon') ?>" class="btn btn-default">Voltar</a>
                                            <?php
                                            if ($curso[0]->status == 1) {
                                                echo '<span type="button" id="alterar" valor="0" class="btn btn-danger">Desativar</span>';
                                            } else {
                                                echo '<span type="button" id="alterar" valor="1" class="btn btn-info">Publicar</span>';
                                            }
                                            ?>
                                            <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#coll-questoes" aria-expanded="false" aria-controls="collapseExample">
                                                Quest&otilde;es
                                            </button>
                                        </form>

                                        <div class="collapse" id="coll-questoes">
                                            <hr>
                                            <div class="well">
                                                <?php
                                                if (isset($questoes)) {
                                                    echo '<table class="table table-striped">';
                                                    echo '<thead><td><b>Descricao</b></td><td><b>Justificativa</b></td><td><b>Resposta</b></td><td><b>Tipo de prova</b></td></thead>';
                                                    foreach ($questoes as $q) {
                                                        $resposta = null;
                                                        $tipo_prova = null;
                                                        if ($q->resposta == 0) {
                                                            $resposta = '<div class="radio">
                                                                                    <label>
                                                                                        <input type="radio" name="resposta" id="resposta" value="0" checked>
                                                                                        Não
                                                                                    </label>
                                                                                </div>
                                                                                <div class="radio">
                                                                                    <label>
                                                                                        <input type="radio" name="resposta" id="resposta" value="1">
                                                                                        Sim
                                                                                    </label>
                                                                                </div>';
                                                        } else {
                                                            $resposta = '<div class="radio">
                                                                                    <label>
                                                                                        <input type="radio" name="resposta" id="resposta" value="0">
                                                                                        Não
                                                                                    </label>
                                                                                </div>
                                                                                <div class="radio">
                                                                                    <label>
                                                                                        <input type="radio" name="resposta" id="resposta" value="1" checked>
                                                                                        Sim
                                                                                    </label>
                                                                                </div>';
                                                        }

                                                        switch ($q->id_avaliacao) {
                                                            case 1:
                                                                $tipo_prova = '<div class="radio">
                                                                                        <label>
                                                                                            <input type="radio" name="id_avaliacao" id="id_avaliacao" value="1" checked>
                                                                                            Teste
                                                                                        </label>
                                                                                    </div>
                                                                                    <div class="radio">
                                                                                        <label>
                                                                                            <input type="radio" name="id_avaliacao" id="id_avaliacao" value="2">
                                                                                            Re-teste
                                                                                        </label>
                                                                                    </div>
                                                                                    <div class="radio">
                                                                                        <label>
                                                                                            <input type="radio" name="id_avaliacao" id="id_avaliacao" value="3">
                                                                                            Segunda-chance
                                                                                        </label>
                                                                                    </div>';
                                                                break;
                                                            case 2:
                                                                $tipo_prova = '<div class="radio">
                                                                                        <label>
                                                                                            <input type="radio" name="id_avaliacao" id="id_avaliacao" value="1" >
                                                                                            Teste
                                                                                        </label>
                                                                                    </div>
                                                                                    <div class="radio">
                                                                                        <label>
                                                                                            <input type="radio" name="id_avaliacao" id="id_avaliacao" value="2" checked>
                                                                                            Re-teste
                                                                                        </label>
                                                                                    </div>
                                                                                    <div class="radio">
                                                                                        <label>
                                                                                            <input type="radio" name="id_avaliacao" id="id_avaliacao" value="3">
                                                                                            Segunda-chance
                                                                                        </label>
                                                                                    </div>';
                                                                break;
                                                            case 3:
                                                                $tipo_prova = '<div class="radio">
                                                                                        <label>
                                                                                            <input type="radio" name="id_avaliacao" id="id_avaliacao" value="1" >
                                                                                            Teste
                                                                                        </label>
                                                                                    </div>
                                                                                    <div class="radio">
                                                                                        <label>
                                                                                            <input type="radio" name="id_avaliacao" id="id_avaliacao" value="2">
                                                                                            Re-teste
                                                                                        </label>
                                                                                    </div>
                                                                                    <div class="radio">
                                                                                        <label>
                                                                                            <input type="radio" name="id_avaliacao" id="id_avaliacao" value="3" checked> 
                                                                                            Segunda-chance
                                                                                        </label>
                                                                                    </div>';
                                                                break;

                                                            default:
                                                                break;
                                                        }

                                                        echo "<tr><form action=\"" . base_url("cursos/imazon/atualiza_questao_prova") . "\" method=\"post\">"
                                                        . "<td><textarea class=\"form-control\" name=\"descricao\" rows=\"2\">$q->descricao</textarea></td>"
                                                        . "<td><textarea class=\"form-control\" name=\"justificativa\" rows=\"2\">$q->justificativa</textarea></td>"
                                                        . "<td>$resposta</td>"
                                                        . "<td>$tipo_prova</td>"
                                                        . "<td><input type=\"submit\" value=\"Salvar\" class=\"btn btn-success\"></td>"
                                                        . "<td><a href=\"" . base_url("cursos/imazon/del_questao_curso/" . codifica($q->id_questao)) . "/" . codifica($curso[0]->id_curso) . "\" onclick=\"return confirm('Deseja realmente excluir esta questão?')\"><i class=\"glyphicon glyphicon-remove\"></i></a></td>"
                                                        . "<input type=\"hidden\" name=\"id_questao\" value=\"" . codifica($q->id_questao) . "\">"
                                                        . "<input type=\"hidden\" name=\"curso\" value=\"" . codifica($curso[0]->id_curso) . "\">"
                                                        . "</form></tr>";
                                                    }
                                                    echo '</table>';
                                                }
                                                ?>
                                                <button class="btn btn-primary" type="button" id="btn-add-questao">
                                                    +Adicionar
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div> <!-- /widget-content -->

                    </div> <!-- /widget -->

                </div> <!-- /span8 -->

            </div> <!-- /row -->

        </div> <!-- /container -->

    </div> <!-- /main-inner -->

</div> <!-- /main -->


<div class="modal fade" id="modal-add-questao" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Adicionar quest&atilde;o</h4>
            </div>
            <form action="<?= base_url("cursos/imazon/add_questao_curso") ?>" method="post"> 
                <div class="modal-body">
                    <p><b>Enunciado:</b></p>
                    <textarea class="form-control" name="descricao" rows="5"></textarea>
                    <p><b>Justificativa:</b></p>
                    <textarea class="form-control" name="justificativa" rows="5"></textarea>
                    <p><b>Resposta:</b></p>
                    <div class="radio">
                        <label>
                            <input type="radio" name="resposta" id="resposta" value="0" checked>
                            Não
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="resposta" id="resposta" value="1">
                            Sim
                        </label>
                    </div>
                    <p><b>Tipo de prova:</b></p>
                    <div class="radio">
                        <label>
                            <input type="radio" name="id_avaliacao" id="id_avaliacao" value="1" checked>
                            Teste
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="id_avaliacao" id="id_avaliacao" value="2">
                            Re-teste
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="id_avaliacao" id="id_avaliacao" value="3">
                            Segunda-chance
                        </label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Salvar</button>
                </div>
                <input type="hidden" name="id_curso" value="<?= codifica($curso[0]->id_curso) ?>">
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<script>
    $(function () {
        $("#alterar").click(function () {
            var acao = $("#alterar").attr("valor"),
                    acao = acao.toString();
            //envia requisição post ajax
            $.post("<?= base_url('cursos/imazon/ajax_altera_status_publicado') ?>", {id_curso: "<?= codifica($curso[0]->id_curso) ?>", status: acao})
                    .success(function (e) {
                        if (e == "ok") {
                            if (acao == 1) {
                                $("#alterar").attr("class", "btn btn-danger");
                                $("#alterar").attr("valor", "0");
                                $("#alterar").html("Desativar");
                            } else {
                                $("#alterar").attr("class", "btn btn-info");
                                $("#alterar").attr("valor", "1");
                                $("#alterar").html("Publicar");
                            }
                        }
                    })
                    .fail(function () {
                        alert("Erro ao alterar status");
                    });
        });
    });

    $(function () {
        $("#btn-add-questao").click(function () {
            $("#modal-add-questao").modal("show");
        });
    });

    function readImage() {
        if (this.files && this.files[0]) {
            var FR = new FileReader();
            FR.onload = function (e) {
                $(".progress").css("display", "block");
                $('#form-ulpoad-img').ajaxForm({
                    success: function(data) {
                        var resposta;
                        if(data=="ok"){
                            $('#img-curso').attr("src", e.target.result);
                            $(".btn-upload-img").remove();
                            resposta = '<div class="alert alert-success alert-dismissible" style="margin-top: 7px;" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Imagem alterada com sucesso!</div>';
                        }else{
                            resposta = '<div class="alert alert-danger alert-dismissible" style="margin-top: 7px;" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>'+data+'</div>';
                        }
                        $('#form-ulpoad-img').append(resposta);
                    }
                }).submit();
                $(".progress").css("display", "none");
            };
            FR.readAsDataURL(this.files[0]);
        }
    }

    $(function () {
        $("#img-nova").change(readImage);
    });
</script>