
<script src="https://raw.githubusercontent.com/plentz/jquery-maskmoney/master/dist/jquery.maskMoney.min.js" type="text/javascript"></script>
<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-pencil"></i>
	      				<h3>Editar Plano</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						<div class="tabbable">
<?php
foreach($plano as $p);
?>
<?=form_open('planos/alt');?>


<?=form_fieldset('Nome');?>
<?php 
$nome = array(
              'name'        => 'nome',
              'maxlength'   => '12',
              'value'       => $p->nome,
              'required'    => 'true'
            ); 
?>
<?php echo form_error('nome'); ?>
<?=form_input($nome);?>
<?=form_fieldset_close();?> 

<?=form_fieldset('Valor');?>
<?php
$valor = array(
              'name'        => 'valor',
              'maxlength'   => '12',
              'required'    => 'true',
              'type'        => 'text',
			  'class'        => 'moeda',
              'value'       => $p->valor
        ); 
?>
 <?php echo form_error('valor'); ?>
<?=form_input($valor);?>
<?=form_fieldset_close();?> 
 
<?php echo form_error('validade'); ?>
<?=form_fieldset('Validade');?>
<?php
$validade = array(
              'name'        => 'validade',
              'maxlength'   => '12',
              'value'       => $p->validade,
              'required'    => 'true',
              'type'        => 'number'  
            ); 
?>
<?=form_input($validade);?>
<?=form_fieldset_close();?>
<input type="hidden" name="id" value="<?=$this->uri->segment(4)?>">
<input type="hidden" name="codigo" value="<?=codifica($p->codigo)?>">
<?=form_fieldset('Descrição');?>
<?=form_textarea(array('name'=>'descricao', 'value'=>$p->descricao))?>          
<?=form_fieldset_close();?> 

<?=form_fieldset('Descrição');

if($p->status){
    echo '<input type="checkbox" name="ativo" checked/>';
}else{
    echo '<input type="checkbox" name="ativo"/>';
}
echo ' Ativo';

?>

<?=form_fieldset_close();?> 

<?php
echo '<hr>';
$botao = array('class'=>'btn btn-success', 'name'=>'alterar', 'value'=>"Alterar");

?>
<?=form_submit($botao);?>
<?=form_close();?>


</div>

					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
                                                    
                                                    
<script>
  $(function() {
    $('.moeda').maskMoney();
  })
</script>