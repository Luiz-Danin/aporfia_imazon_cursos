<div class="main">
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Editar categoria</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						<div class="tabbable">
						
						<br>
                                                    
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
                                                                    
                                                                    <form action="<?=base_url('categorias/edit')?>" method="post">
                                                                    
                                                                    <div class="form-group">
                                                                      <label for="Nome">Nome</label>
                                                                      <input type="text" class="form-control"  id="Nome" name="CCSubCat" value="<?=$dados[0]->CCSubCat?>">
                                                                    </div>
                                                                        
                                                                    <div class="form-group">
                                                                      <label for="Ordem">Ordem</label>
                                                                      <input type="number" class="form-control" id="Ordem" name="CCOrdem" value="<?=$dados[0]->CCOrdem?>" placeholder="Ordem">
                                                                    </div>
                                                                    
                                                                    <div class="form-group">
                                                                      <label for="Descricao">Descrição</label>  
                                                                      <textarea class="form-control ckeditor" name="CCDescricaoCat" rows="3"><?=$dados[0]->CCDescricaoCat?></textarea>
                                                                    </div>    
                                                                        
                                                                    <input type="hidden" name="cursos_cat_id" value="<?=codifica($dados[0]->cursos_cat_id)?>">    
                                                                    <button type="submit" class="btn btn-success">Alterar</button>
                                                                    <a href="<?=base_url('categorias')?>" class="btn btn-default">Voltar</a>
                                                                  </form>
                                                                    
								</div>
					
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->

