<div class="main">
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Editar categoria (Blog)</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						<div class="tabbable">
						
						<br>
                                                    
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
                                                                    
                                                                    <form action="<?=base_url('blog/categorias/edit')?>" method="post">
                                                                    
                                                                    <div class="form-group">
                                                                      <label for="Nome">Nome</label>
                                                                      <input type="text" class="form-control"  id="Nome" name="nome" value="<?=$dados[0]->nome?>">
                                                                    </div>
                                                                        
                                                                    <input type="hidden" name="id" value="<?=codifica($dados[0]->id)?>">    
                                                                    <button type="submit" class="btn btn-success">Alterar</button>
                                                                    <a href="<?=base_url('blog/categorias')?>" class="btn btn-default">Voltar</a>
                                                                  </form>
                                                                    
								</div>
					
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->

