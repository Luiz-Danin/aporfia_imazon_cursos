<div class="main">
    <div class="main-inner">

        <div class="container">

            <div class="row">

                <div class="span12">      		

                    <div class="widget ">

                        <div class="widget-header">
                            <i class="icon-user"></i>
                            <h3>Editar curso</h3>
                        </div> <!-- /widget-header -->

                        <div class="widget-content">

                            <div class="tabbable">

                                <br />

                                <div class="tab-content">
                                    <div class="tab-pane active" id="formcontrols">

                                        <form action="<?= base_url('cursos/vip/edit') ?>" method="post" enctype="multipart/form-data" >

                                            <div class="form-group">
                                                <label for="Nome">Nome</label>
                                                <input type="text" class="form-control"  id="Nome" name="nome" value="<?= $dados[0]->nome ?>">
                                            </div>
                                            
                                            <h3>Aulas</h3>

                                            <span class="label label-info" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                                <i class="glyphicon glyphicon-plus"></i> Ver aulas
                                            </span>

                                            &nbsp; <a href="<?=base_url('cursos/vip/criar_aula/')?>/<?=$this->uri->segment(4)?>"><kbd>Criar aula</kbd></a>
                                            <div class="collapse" id="collapseExample" style="border: none;">
                                                <div class="well" style="background-color: #fff; border: none;">
                                                    <nav>
                                                        <table class="table table-bordered" style="float: left; margin-left:-18px;">

                                                            <?php
                                                            foreach ($aulas as $als) {
                                                                echo '<tr><td><a href="' . base_url('cursos/vip/ver_aula') . '/' . codifica($als->cursos_aulas_id) . '"> <span class="badge">' . $als->CAOrdem . '.</span> ' . $als->CATitulo . '</a><a href="' . base_url('cursos/vip/apagar_aula') . '/' . codifica($als->cursos_aulas_id) . '/'.codifica($als->cedoc_doc_id_fk).'" onclick="confirm(\'Deseja realmente excluir esta aula?\')"><span class="badge">x</span></a></td></tr>';
                                                            }
                                                            ?>
                                                            
                                                        </table>
                                                    </nav> 
                                                </div>
                                            </div>
                                            <br />
                                            <hr>


                                            <div class="form-group">
                                                <label for="Descricao">Descrição</label>  
                                                <textarea  name="descricao" rows="10" style="width:50%;"  ><?= strip_tags($dados[0]->descricao) ?></textarea>
                                            </div>      

                                            <div class="form-group">
                                                <label for="Objetivo">Objetivo</label>  
                                                <textarea  name="objetivo3" rows="10" style="width:50%;" ><?= $dados[0]->objetivo3 ?></textarea>
                                            </div>    

                                            <input type="hidden" name="cedoc_doc_id" value="<?= codifica($dados[0]->cedoc_doc_id) ?>">    
                                            <button type="submit" class="btn btn-success">Alterar</button>
                                            <a href="<?= base_url('cursos/vip') ?>" class="btn btn-default">Voltar</a>
                                            <?php
                                            if ($dados[0]->CPublicado == "Sim") {
                                                echo '<span type="button" id="alterar" acao="desativar" class="btn btn-danger">Desativar</span>';
                                            } else {
                                                echo '<span type="button" id="alterar" acao="ativar" class="btn btn-info">Publicar</span>';
                                            }
                                            ?>

                                            <?php if (count($anterior)) { ?>
                                                <a href="<?= base_url('cursos/vip/editar') . '/' . codifica($anterior[0]->cedoc_doc_id_fk) ?>" class="btn btn-default">
                                                    <i class="glyphicon glyphicon-chevron-left"><b><?= $anterior[0]->nome ?></b></i>
                                                </a>
                                            <?php } ?>
                                            <?php if (count($proximo)) { ?>
                                                <a href="<?= base_url('cursos/vip/editar') . '/' . codifica($proximo[0]->cedoc_doc_id_fk) ?>" class="btn btn-default">
                                                    <i class="glyphicon glyphicon-chevron-right"><b><?= $proximo[0]->nome ?></b></i>
                                                </a>
                                            <?php } ?>
                                        </form>

                                    </div>
                                </div>
                            </div>

                        </div> <!-- /widget-content -->

                    </div> <!-- /widget -->

                </div> <!-- /span8 -->

            </div> <!-- /row -->

        </div> <!-- /container -->

    </div> <!-- /main-inner -->

</div> <!-- /main -->

<script>
    $(function () {
        $("#alterar").click(function () {

            var acao = $("#alterar").attr("acao"),
                    acao = acao.toString();

            if (acao == "ativar") {
                $("#alterar").attr("class", "btn btn-danger");
                $("#alterar").attr("acao", "desativar");
                $("#alterar").html("Desativar");

            } else {
                $("#alterar").attr("class", "btn btn-info");
                $("#alterar").attr("acao", "ativar");
                $("#alterar").html("Publicar");

            }

            //envia requisição post ajax
            $.post("<?= base_url('cursos/vip/ajax_altera_status_publicado') ?>", {id: "<?= codifica($dados[0]->cedoc_doc_id_fk) ?>", status: acao}).fail(function () {
                alert("Erro ao alterar status");
            });

        });

    });
    
    function readImage() {
        if ( this.files && this.files[0] ) {
            var FR = new FileReader();
            FR.onload = function(e) {
                 $('#img-curso').attr( "src", e.target.result );
                 $(".btn-upload-img").remove();
            };       
            FR.readAsDataURL( this.files[0] );
        }
    }
    
    $(function(){
        $("#img-nova").change( readImage );
    });
    
    
</script>