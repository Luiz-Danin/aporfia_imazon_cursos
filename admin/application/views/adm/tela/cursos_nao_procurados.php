<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-pencil"></i>
	      				<h3>Cursos Não Procurados</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						<div class="tabbable">
                                                    
						<br>
                                                <?php if(count($dados)>0){?>
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th>Título</th>
                                                            <th width="34%">Categoria</th>
                                                            <th width="4%">Visitas</th>
                                                        </tr>
                                                    </thead>
                                                    <?php foreach($dados as $d){?>
                                                    <tbody>
                                                        <tr>
                                                            <td><?=$d->nome?></td>
                                                            <td><b>(<?=$d->CCCat?>/<?=$d->CCSubCat?>)</b></td>
                                                            <td><b><?=$d->CVisitas?></b></td>
                                                        </tr>
                                                    </tbody>
                                                    <?php }?>
                                                </table> 
                                                <?php }else{ echo '<pre>Nenhum resultado.</pre>';}?>
                                                <?=$paginacao?>
                                                						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
