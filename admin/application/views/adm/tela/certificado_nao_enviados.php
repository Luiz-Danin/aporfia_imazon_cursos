<div class="main">

    <div class="main-inner">

        <div class="container">

            <div class="row">

                <div class="span12">      		

                    <div class="widget ">

                        <div class="widget-header">
                            <i class="icon-pencil"></i>
                            <h3>Certificados não enviados</h3>
                        </div> <!-- /widget-header -->

                        <div class="widget-content">

                            <div class="tabbable">

                                <form class="form-inline" action="<?= base_url("admin/certificados/nao_enviados") ?>" method="post">
                                    <div class="form-group">
                                        <label class="sr-only" for="buscar">Pesquisar</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="glyphicon glyphicon-search"></i>
                                            </div>
                                            <input type="text" class="form-control" id="busca"r name="buscar" placeholder="Buscar por nome ou email">
                                        </div>
                                    </div>
                                </form>
                                <span style="float:right;">
                                    <?php
                                    if (count($certificados) > 0) {
                                        echo '<a href="' . base_url('certificados/gerar_certificado_impresso') . '" target="_blank"><span class="label label-success">Imprimir todos (VIP)</span></a>';
                                    }
                                    ?><?php
                                    if (count($certificados) > 0) {
                                        echo '<a href="' . base_url('certificados/gerar_etiqueta') . '" target="_blank"> <span class="label label-success">Imprimir etiquetas (VIP)</span></a>';
                                    }
                                    echo '|';
                                    if (count($certificados_imazon) > 0) {
                                        echo '<a href="' . base_url('certificados/gerar_certificado_impresso_imazon') . '" target="_blank"><span class="label label-success">Imprimir todos (Imazon)</span></a>';
                                    }
                                    ?><?php
                                    if (count($certificados_imazon) > 0) {
                                        echo '<a href="' . base_url('certificados/gerar_etiqueta_imazon') . '" target="_blank"> <span class="label label-success">Imprimir etiquetas (Imazon)</span></a>';
                                    }
                                    
                                    ?></span>


                                <?php if (null !== $this->input->post('buscar') && strlen($this->input->post('buscar')) >= 3) echo count($certificados) + count($certificados_imazon) . ' resultado(s) para <b>"' . $this->input->post('buscar') . '"</b>'; ?>
                                <div style="padding-top: 10px;">

                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#certificado-original" aria-controls="certificado-original" role="tab" data-toggle="tab">Certificado Original</a></li>
                                        <li role="presentation"><a href="#certificado-ouro" aria-controls="certificado-ouro" role="tab" data-toggle="tab">Certificado Ouro</a></li>
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="certificado-original">

                                            <?php
                                            if (count($certificados_imazon) > 0 || count($certificados) > 0) {
                                                $tmpl = array('table_open' => '<table class="table table-striped" >');
                                                $this->table->set_template($tmpl);
                                                $this->table->set_heading('Aluno', 'Data de emissão', 'Email', 'Curso', 'Endereço', '');
                                                if ($certificados) {
                                                    foreach ($certificados as $c) {
                                                       
                                                        if ((int) $c['status'] === 0) {
                                                            $status = '<a onclick="return confirm(\'Deseja realmente enviar este certificado?\')" href="' . base_url("admin/certificados/alterar_status") . '/1/' . codifica($c['id']) . '"><span class="label label-danger" style="padding: 2px;" >
                                                                                                                          <i class="glyphicon glyphicon-okn"></i> Marcar como enviado
                                                                                                                        </span></a> ';
                                                        } else {
                                                            $status = '<a href="' . base_url("admin/certificados/alterar_status") . '/0/' . codifica($c['id']) . '"><span class="label label-success" style="padding: 2px;">
                                                                                                                          <i class="glyphicon glyphicon-minus"></i> Marcar como não enviado
                                                                                                                        </span></a> ';
                                                        }

                                                        //botão gera certificado
                                                        $status.='&nbsp;<a href="' . base_url('certificados/gerar_certificado_impresso') . '/' . base64_encode($c['id_curso']) . '/' . base64_encode($c['id_fic']) . '" alt="Imprimir Certificado"     target="_blank"><span class="label label-success" style="padding: 2px;">&nbsp;<i class="glyphicon glyphicon-print"></i>&nbsp;</span></a>';

                                                        //botão etiqueta
                                                        $status.='&nbsp;<a href="' . base_url('certificados/gerar_etiqueta/') . '/' . base64_encode($c['id_fic']) . '" target="_blank"><span class="label label-info" style="padding: 2px;" alt="Imprimir Etiqueta">&nbsp;<i class="glyphicon glyphicon-tags"></i>&nbsp;</span></a>';

                                                        $endereco = '&nbsp;<span class="pop label label-info" style="padding: 2px;" data-container="body" data-toggle="popover" data-placement="bottom" data-content="' . $c['endereco'] . ', ' . $c['numero'] . ', ' . $c['bairro'] . ', ' . $c['cidade'] . ', ' . $c['estado'] . '">
                                                                                                                          <i class="glyphicon glyphicon-info-sign"></i> Ver
                                                                                                                        </span>';

                                                        $this->table->add_row(str_replace(strtoupper($this->input->post('buscar')), "<b>" . strtoupper($this->input->post('buscar')) . "</b>", $c['aluno']), date('d/m/Y', strtotime($c['cadastro'])), str_replace($this->input->post('buscar'), "<b>" . $this->input->post('buscar') . "</b>", $c['email']), $c['curso'], $endereco, $status);
                                                    }
                                                }
                                                if ($certificados_imazon) {
                                                    foreach ($certificados_imazon as $c) {
                                                        if ($c->id_tipo == 1) {
                                                            if ((int) $c->situacao === 0) {
                                                                $status = '<a onclick="return confirm(\'Deseja realmente enviar este certificado?\')" href="' . base_url("admin/certificados/alterar_status_imazon") . '/1/' . codifica($c->id_certificado) . '"><span class="label label-danger" style="padding: 2px;" >
                                                                                                                              <i class="glyphicon glyphicon-okn"></i> Marcar como enviado
                                                                                                                            </span></a> ';
                                                            } else {
                                                                $status = '<a href="' . base_url("admin/certificados/alterar_status_imazon") . '/0/' . codifica($c->id_certificado) . '"><span class="label label-success" style="padding: 2px;">
                                                                                                                              <i class="glyphicon glyphicon-minus"></i> Marcar como não enviado
                                                                                                                            </span></a> ';
                                                            }
                                                            //botão gera certificado
                                                            $status.='&nbsp;<a href="' . base_url('certificados/gerar_certificado_impresso_imazon') . '/' . base64_encode($c->id_certificado) . '/' . base64_encode($c->id_fic) . '" alt="Imprimir Certificado"     target="_blank"><span class="label label-success" style="padding: 2px;">&nbsp;<i class="glyphicon glyphicon-print"></i>&nbsp;</span></a>';
                                                            //botão etiqueta
                                                            $status.='&nbsp;<a href="' . base_url('certificados/gerar_etiqueta_imazon/') . '/' . base64_encode($c->id_fic) . '" target="_blank"><span class="label label-info" style="padding: 2px;" alt="Imprimir Etiqueta">&nbsp;<i class="glyphicon glyphicon-tags"></i>&nbsp;</span></a>';
                                                            $endereco = '&nbsp;<span class="pop label label-info" style="padding: 2px;" data-container="body" data-toggle="popover" data-placement="bottom" data-content="' . $c->endereco . '">
                                                                                                                          <i class="glyphicon glyphicon-info-sign"></i> Ver
                                                                                                                        </span>';
                                                            $this->table->add_row($c->nomeAluno, date('d/m/Y', strtotime($c->data_emissao)), $c->id_fic, $c->nomeCurso, $endereco, $status);
                                                        }
                                                    }
                                                }
                                                echo $this->table->generate();
                                            } else {
                                                echo '<hr><pre>Nenhum item encontrado.</pre>';
                                            }
                                            ?>

                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="certificado-ouro">

                                            <?php
                                            if (count($certificados_imazon) > 0 || count($certificados) > 0) {
                                                $tmpl = array('table_open' => '<table class="table table-striped" >');
                                                $this->table->set_template($tmpl);
                                                $this->table->set_heading('Aluno', 'Data de emissão', 'Email', 'Curso', 'Endereço', '');
                                                if ($certificados_imazon) {
                                                    foreach ($certificados_imazon as $c) {
                                                        if ((int) $c->id_tipo == 2) {
                                                            if ((int) $c->situacao === 0) {
                                                                $status = '<a onclick="return confirm(\'Deseja realmente enviar este certificado?\')" href="' . base_url("admin/certificados/alterar_status_imazon") . '/1/' . codifica($c->id_certificado) . '"><span class="label label-danger" style="padding: 2px;" >
                                                                                                                                  <i class="glyphicon glyphicon-okn"></i> Marcar como enviado
                                                                                                                                </span></a> ';
                                                            } else {
                                                                $status = '<a href="' . base_url("admin/certificados/alterar_status_imazon") . '/0/' . codifica($c->id_certificado) . '"><span class="label label-success" style="padding: 2px;">
                                                                                                                                  <i class="glyphicon glyphicon-minus"></i> Marcar como não enviado
                                                                                                                                </span></a> ';
                                                            }
                                                            //botão gera certificado
                                                            $status.='&nbsp;<a href="' . base_url('certificados/gerar_certificado_impresso_imazon') . '/' . base64_encode($c->id_certificado) . '/' . base64_encode($c->id_fic) . '" alt="Imprimir Certificado"     target="_blank"><span class="label label-success" style="padding: 2px;">&nbsp;<i class="glyphicon glyphicon-print"></i>&nbsp;</span></a>';
                                                            //botão etiqueta
                                                            $status.='&nbsp;<a href="' . base_url('certificados/gerar_etiqueta_imazon/') . '/' . base64_encode($c->id_fic) . '" target="_blank"><span class="label label-info" style="padding: 2px;" alt="Imprimir Etiqueta">&nbsp;<i class="glyphicon glyphicon-tags"></i>&nbsp;</span></a>';
                                                            $endereco = '&nbsp;<span class="pop label label-info" style="padding: 2px;" data-container="body" data-toggle="popover" data-placement="bottom" data-content="' . $c->endereco . '">
                                                                                                                              <i class="glyphicon glyphicon-info-sign"></i> Ver
                                                                                                                            </span>';
                                                            $this->table->add_row($c->nomeAluno, date('d/m/Y', strtotime($c->data_emissao)), $c->id_fic, $c->nomeCurso, $endereco, $status);
                                                        }
                                                    }
                                                }
                                                echo $this->table->generate();
                                            } else {
                                                echo '<hr><pre>Nenhum item encontrado.</pre>';
                                            }
                                            ?>

                                        </div>
                                    </div>

                                </div>


                            </div>

                        </div> <!-- /widget-content -->

                    </div> <!-- /widget -->

                </div> <!-- /span8 -->




            </div> <!-- /row -->

        </div> <!-- /container -->

    </div> <!-- /main-inner -->

</div> <!-- /main -->

<script>
    $(document).ready(function () {
        $('.pop').click(function () {
            $(this).popover('show');
        });
    });
</script>