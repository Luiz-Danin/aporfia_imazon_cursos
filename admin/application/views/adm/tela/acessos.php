<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-pencil"></i>
	      				<h3>Acessos</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						<div class="tabbable">
                                                    <form method="POST" action="<?=base_url('acessos/')?>">
                                                    <label for="consulta">Buscar:</label>
                                                    <?php if(!isset($_POST['buscar'])){?>
                                                        <input type="text" id="buscar" name="buscar" maxlength="255" required/>
                                                    <?php }else{ ?>
                                                        <input type="text" id="buscar" value="<?=$_POST['buscar']?>" name="buscar" maxlength="255" required/>
                                                    <?php }?>
                                                    <input type="submit" value="OK" />
                                                </form> 
						<br>
                                                
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th>Email</th>
                                                            <th width="24%">IP</th>
                                                            <th width="4%">Data</th>
                                                        </tr>
                                                    </thead>
                                                    <?php foreach($acessos as $a){?>
                                                    <tbody>
                                                        <tr>
                                                            <td><?=$a->email?></td>
                                                            <td><b><?=$a->ip?></b></td>
															<td><b><?=date('d/m/Y', strtotime($a->dt_cadastro))?></b></td>
                                                        </tr>
                                                    </tbody>
                                                    <?php }?>
                                                </table> 
                                                <?=$paginacao?>
                                                						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
