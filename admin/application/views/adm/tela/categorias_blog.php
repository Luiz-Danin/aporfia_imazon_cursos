<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-pencil"></i>
	      				<h3>Categorias (Blog)</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						<div class="tabbable">
						<form method="POST" action="">
                                                    <label for="consulta">Buscar:</label>
                                                    <?php if(!isset($_POST['consulta'])){?>
                                                        <input type="text" id="consulta" name="consulta" maxlength="255" required/>
                                                    <?php }else{ ?>
                                                        <input type="text" id="consulta" value="<?=$_POST['consulta']?>" name="consulta" maxlength="255" required/>
                                                    <?php }?>
                                                    

                                                    <input type="submit" value="OK" />
                                                </form> 
						<br>
                                                
                                                <table class="table">
                                                    <thead>
                                                        <th>Nome</th>
                                                        <th width="10%">Data de cadastro</th>
                                                        <th width="4%"></th>
                                                    </thead>
                                                    <?php 
                                                    foreach($categorias as $d){?>
                                                    <tbody>
                                                        <tr>
                                                            <td><a href="<?=current_url()?>/editar/<?=codifica($d->id)?>"><?=$d->nome?></a></td>
                                                            <td><?=date('d/m/Y', strtotime($d->dt_cadastro));?></td>
                                                            <td><a href="<?=current_url()?>/del/<?=codifica($d->id)?>" onclick="return confirm('Deseja realmente excluir esta categoria?')"><i class="icon icon-trash"></i></a></td>
                                                        </tr>
                                                    </tbody>
                                                    <?php }?>
                                                </table> 
                                                
                                                <!-- Button to trigger modal -->
                                                <a type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-add-categoria">Nova categoria</a>
                                               
						</div>
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->

<!-- Modal -->
<div class="modal fade" id="modal-add-categoria" tabindex="-1" role="dialog" aria-labelledby="modal-add-categoriaLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
          <form action="<?=base_url('blog/categorias/add')?>" method="post">                               
            <div class="form-group">
            <label for="Nome">Nome</label>
            <input type="text" class="form-control"  id="Nome" name="nome">
            <br />
            <button type="submit" class="btn btn-success">Salvar</button>
          </form>
     </div>
    </div>
  </div>
</div>