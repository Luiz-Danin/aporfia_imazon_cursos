<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-pencil"></i>
	      				<h3>Cursos</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						<div class="tabbable">
                                                    <form method="POST" action="<?=base_url('cursos/vip/')?>">
                                                    <label for="consulta">Buscar:</label>
                                                    <?php if(!isset($_POST['buscar'])){?>
                                                        <input type="text" id="buscar" name="buscar" maxlength="255" required/>
                                                    <?php }else{ ?>
                                                        <input type="text" id="buscar" value="<?=$_POST['buscar']?>" name="buscar" maxlength="255" required/>
                                                    <?php }?>
                                                    <input type="submit" value="OK" />
                                                </form> 
						<br>
                                                
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th>Título</th>
                                                            <th width="24%">Categoria</th>
                                                            <th width="4%">Completo</th>
                                                            <th width="4%">Publicado</th>
                                                            <th width="2%"></th>
                                                        </tr>
                                                    </thead>
                                                    <?php foreach($dados as $d){?>
                                                    <tbody>
                                                        <tr>
                                                            <td><?=$d->nome?></td>
                                                            <td><b>(<?=$d->CCCat?>/<?=$d->CCSubCat?>)</b></td>
                                                            <td>
                                                                <?php
                                                                   if($d->situacao === "Completo"){
                                                                       echo '<a href="'.base_url('cursos/vip/atualizar').'/situacao/'.$d->cedoc_doc_id.'/'.$this->uri->segment(4).'"><span class="label label-success">Sim</span></a>';
                                                                   }else{
                                                                       echo '<a href="'.base_url('cursos/vip/atualizar').'/situacao/'.$d->cedoc_doc_id.'/'.$this->uri->segment(4).'"><span class="label label-danger">Não</span></a>';
                                                                   }  
                                                                ?>
                                                            </td>
                                                            <td>
                                                                <?php
                                                                   if($d->CPublicado === "Sim"){
                                                                       echo '<a href="'.base_url('cursos/vip/atualizar').'/publicado/'.$d->cedoc_doc_id.'/'.$this->uri->segment(4).'"><span class="label label-success">Sim</span></a>';
                                                                   }else{
                                                                       echo '<a href="'.base_url('cursos/vip/atualizar').'/publicado/'.$d->cedoc_doc_id.'/'.$this->uri->segment(4).'"><span class="label label-danger">Não</span></a>';
                                                                   }  
                                                                ?>
                                                            </td>
                                                            <td>
                                                                <a href="<?=base_url('cursos/vip/editar')?>/<?=codifica($d->cedoc_doc_id)?>" title="Editar dados">
                                                                    <i class="glyphicon glyphicon-pencil">                                                                        
                                                                    </i>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                    <?php }?>
                                                </table> 
                                                <?=$paginacao?>
                                                						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
        
          <form action="<?=base_url('categorias/add')?>" method="post">
                                                                    
            <div class="form-group">
              <label for="Nome">Nome</label>
              <input type="text" class="form-control"  id="Nome" name="CCSubCat">
            </div>

            <div class="form-group">
              <label for="Ordem">Ordem</label>
              <input type="number" class="form-control" id="Ordem" name="CCOrdem" placeholder="Ordem">
            </div>

            <div class="form-group">
              <label for="Descricao">Descrição</label>  
              <textarea class="form-control ckeditor" name="CCDescricaoCat" rows="3"></textarea>
            </div>
              
            <div class="form-group">
              <label for="Ordem">Disponível</label>
              <div class="radio">
                <label>
                  <input type="radio" name="CCDisponivel" value="Nao">
                  Não
                </label>
              </div>
              <div class="radio">
                <label>
                  <input type="radio" name="CCDisponivel" value="Sim">
                  Sim
                </label>
              </div>
            </div>

            <div class="form-group">
              <label for="Ordem">Destaque</label>
              <div class="radio">
                <label>
                  <input type="radio" name="CCDestaque" value="Nao">
                  Não
                </label>
              </div>
              <div class="radio">
                <label>
                  <input type="radio" name="CCDestaque" value="Sim">
                  Sim
                </label>
              </div>
            </div>

            <div class="form-group">
              <label for="Ordem">Mostrar</label>
              <div class="radio">
                <label>
                  <input type="radio" name="CCMostrar" value="Nao">
                  Não
                </label>
              </div>
              <div class="radio">
                <label>
                  <input type="radio" name="CCMostrar" value="Sim">
                  Sim
                </label>
              </div>
            </div>  
              
            <button type="submit" class="btn btn-success">Salvar</button>
            
          </form>
     </div>
    </div>
  </div>
</div>