<!DOCTYPE html>
<html lang="pt">
    <head>
        <meta charset="utf-8">
        <title>::Imazon Vip::</title>

        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="apple-mobile-web-app-capable" content="yes">
        
        <link href="<?= base_url('assets') ?>/css/bootstrap.min.css" rel="stylesheet">
<!--        <link href="<?= base_url('assets') ?>/css__?family=Open+Sans:400italic,600italic,400,600"
              rel="stylesheet">-->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600"
              rel="stylesheet">
        <link href="<?= base_url('assets') ?>/css/font-awesome.css" rel="stylesheet">
        <link href="<?= base_url('assets') ?>/css/style.css" rel="stylesheet">
        <link href="<?= base_url('assets') ?>/css/pages/dashboard.css" rel="stylesheet">
        <link href="<?= base_url('lib') ?>/jasny-bootstrap/css/jasny-bootstrap.css" rel="stylesheet">

        <!-- Placed at the end of the document so the pages load faster --> 
        <script src="<?= base_url('lib') ?>/jquery/jquery-2.1.3.min.js"></script> 
        <script src="<?= base_url('lib') ?>/ckeditor_4.4.7_standard/ckeditor.js"></script> 

        <script src="<?= base_url('assets') ?>/js/excanvas.min.js"></script> 
        <script src="<?= base_url('assets') ?>/js/chart.min.js" type="text/javascript"></script> 
        <script src="<?= base_url('assets') ?>/js/bootstrap.js"></script>
        <script type="text/javascript" src="<?= base_url('assets') ?>/js/full-calendar/fullcalendar.min.js"></script>
        <script src="<?= base_url('assets') ?>/js/base.js"></script> 
        <script src="<?= base_url('assets') ?>/js/jquery.form.js"></script>
        <script src="<?= base_url('lib') ?>/jasny-bootstrap/js/jasny-bootstrap.js"></script> 
        <script src="<?= base_url('lib') ?>/jquery-maskmoney-master/dist/jquery.maskMoney.min.js"></script>
        <script src="<?= base_url('lib') ?>/jquery.form.min.js"></script>
        <!--<script src="http://malsup.github.io/min/jquery.form.min.js"></script>-->

    </head>
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container"> 
                    <img src="<?= base_url('imgs/logo.png') ?>" style="height:75px; margin-bottom:-42px;">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span> 
                    </a>
                    <div class="nav-collapse">
                        <ul class="nav pull-right">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="icon-user"></i>
                                    <b class="caret"></b>
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?= base_url('logout') ?>">Logout</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <!--/.nav-collapse --> 
                </div>
                <!-- /container --> 
            </div>
            <!-- /navbar-inner --> 
        </div>
        <!-- /navbar -->
        <div class="subnavbar">
            <div class="subnavbar-inner">
                <div class="container">
                    <ul class="mainnav">
                        <li><a href="<?= base_url('assinaturas') ?>"><i class="icon-list-alt"></i><span>Alunos</span> </a> </li>
                        <li class="dropdown subnavbar-open-right">					
                            <a data-toggle="dropdown" class="dropdown-toggle" href="javascript:;">
                                <i class="icon-list-alt"></i>
                                <span>Cursos</span>
                                <b class="caret"></b>
                            </a>	

                            <ul class="dropdown-menu">
                                <li><a href="<?= base_url('/cursos/vip') ?>"><i class="icon-list-alt"></i><span>VIP</span> </a> </li>
                                <li><a href="<?= base_url('/cursos/imazon') ?>"><i class="icon-list-alt"></i><span>Imazon</span> </a> </li>
                            </ul>    				
                        </li>     
                        <li><a href="<?= base_url('categorias') ?>"><i class="icon-list-alt"></i><span>Categorias</span> </a> </li>
                        <li><a href="<?= base_url('planos') ?>"><i class="icon-list-alt"></i><span>Planos</span> </a> </li>
                        <li><a href="<?= base_url('termo') ?>"><i class="icon-list-alt"></i><span>Termos</span> </a> </li>
                        <li class="dropdown subnavbar-open-right">					
                            <a data-toggle="dropdown" class="dropdown-toggle" href="javascript:;">
                                <i class="icon-bar-chart"></i>
                                <span>Relatórios</span>
                                <b class="caret"></b>
                            </a>	

                            <ul class="dropdown-menu">
                                <li><a href="<?= base_url('certificados/nao_enviados') ?>">Certificados (Pagos) - Não Enviados</a></li>
                                <li><a href="<?= base_url('certificados/enviados') ?>">Certificados (Pagos) - Enviados</a></li>
                                <li><a href="<?= base_url('certificados/pendentes') ?>">Certificados Pendentes</a></li>
                                <li><a href="<?= base_url('cursos/mais_visitados') ?>">Cursos Mais Visitados</a></li>
                                <li><a href="<?= base_url('cursos/mais_vendidos') ?>">Cursos Mais Vendidos</a></li>
                                <li><a href="<?= base_url('acessos') ?>">Listar acessos </a></li>
                                <li><a href="">Auditoria</a></li>
                            </ul>    				
                        </li>
                        <li class="dropdown subnavbar-open-right">					
                            <a data-toggle="dropdown" class="dropdown-toggle" href="javascript:;">
                                <i class="icon-comment-alt"></i>
                                <span>Blog</span>
                                <b class="caret"></b>
                            </a>	

                            <ul class="dropdown-menu">
                                <li><a href="<?= base_url('blog/posts/novo') ?>">Inserir Postagem</a></li>
                                <li><a href="<?= base_url('blog/categorias') ?>">Categorias</a></li>
                                <li><a href="<?= base_url('blog/posts') ?>">Posts</a></li>
                            </ul>    				
                        </li>
                    </ul>
                </div>
                <!-- /container --> 
            </div>
            <!-- /subnavbar-inner --> 

        </div>
        <!-- /subnavbar -->
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10">
                <?php
                if (isset($_SESSION['msg'])) {
                    echo ' <div role="alert" class="alert alert-warning alert-dismissible fade in">
            ' . $_SESSION['msg'] . '
          </div>';
                    unset($_SESSION['msg']);
                }
                ?>
            </div>    
        </div>