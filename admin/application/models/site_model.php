<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Site_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->imazon = $this->load->database('imazon', TRUE);
        $this->imazon10 = $this->load->database('imazon10', TRUE);
    }

    public function cursos_home() {
        
        $this->db->select('cedoc_doc_id_fk, nome, carga_horaria');
        $this->db->from('cursos');
        $this->db->join('cedoc_doc', 'cursos.cedoc_doc_id_fk = cedoc_doc.cedoc_doc_id');
        $this->db->limit(12);
        $this->db->where('CPublicado', 'Sim');
        $lista[]   =   null;
        $i         =   1;
        $cursos    =   $this->db->get()->result();
        
        foreach ($cursos as $c) {
            $img = $this->db->get_where('cedoc_doc_imagens', array('cedoc_doc_id_fk' => $c->cedoc_doc_id_fk))->result();
            $lista[$i] = array('cedoc_doc_id_fk' => $c->cedoc_doc_id_fk, 'nome' => $c->nome, 'img' => $img[0]->caminho, 'carga_horaria' => $c->carga_horaria);
            $i++;
        }
        return $lista;
    }
    
    public function categorias_cursos_sidebar() {
        return $this->db->select('CCSubCat, CCDestaque')->get_where('cursos_cat', array('CCMostrar' => 'Sim'));
    }

    public function get_categoria_by_slug($slug = null) {
        return $this->db->select('cursos_cat_id')->get_where('cursos_cat', array('CCSubCat' => $slug));
    }

    public function get_cursos_by_slug_categoria($slug = null, $inicio = null, $limite = null) {
        $categoria = $this->get_categoria_by_slug($slug)->result();
        $this->db->select('cedoc_doc_id_fk, nome, carga_horaria');
        $this->db->join('cedoc_doc', 'cursos.cedoc_doc_id_fk = cedoc_doc.cedoc_doc_id');
        ($inicio !=  null && $limite != null) ? $this->db->limit($limite, $inicio) : $this->db->limit($limite);
        $cursos    =   $this->db->get_where('cursos', array('cursos_cat_id_fk' => $categoria[0]->cursos_cat_id, 'CPublicado' => 'Sim'))->result();
        $lista[]   =   null;
        $i         =   1;
        foreach ($cursos as $c) {
            $img = $this->db->get_where('cedoc_doc_imagens', array('cedoc_doc_id_fk' => $c->cedoc_doc_id_fk))->result();
            $lista[$i] = array('cedoc_doc_id_fk' => $c->cedoc_doc_id_fk, 'nome' => $c->nome, 'img' => $img[0]->caminho, 'carga_horaria' => $c->carga_horaria);
            $i++;
        }
        return $lista;
    }

    
    
    public function count_all_cursos_by_slug_categoria($slug = null) {
        $categoria = $this->get_categoria_by_slug($slug)->result();
        $this->db->select('cedoc_doc_id_fk, nome');
        $this->db->join('cedoc_doc', 'cursos.cedoc_doc_id_fk = cedoc_doc.cedoc_doc_id');
        return $this->db->get_where('cursos', array('cursos_cat_id_fk' => $categoria[0]->cursos_cat_id, 'CPublicado' => 'Sim'))->result();
    }

    public function get_img_curso($cedoc_doc_id) {
        $this->db->select('*');
        $this->db->from('cedoc_doc_imagens');
        $this->db->where('cedoc_doc_id_fk', $cedoc_doc_id);
        $data = $this->db->get();
        return $data->result();
    }
    
    public function curso_previa($cedoc_doc_id) {
        $this->db->select('*');
        $this->db->from('cursos');
        $this->db->where('cedoc_doc_id_fk', $cedoc_doc_id);
        $this->db->join('cedoc_doc', 'cedoc_doc.cedoc_doc_id = cursos.cedoc_doc_id_fk');
        $this->db->join('cursos_cat', 'cursos.cursos_cat_id_fk = cursos_cat.cursos_cat_id');
        $data = $this->db->get();
        return $data->result();
    }
    
    function buscar_cursos($data) {

        $this->db->select("*");
        $this->db->from("cedoc_doc");
        $this->db->join("cursos", "cursos.cedoc_doc_id_fk = cedoc_doc.cedoc_doc_id");
        $this->db->like("cedoc_doc.nome", $data["nome"]);
        $this->db->or_like("cedoc_doc.descricao", $data["nome"]);
        $this->db->where("cursos.CPublicado", "Sim");
        return $this->db->get();
    }

    function get_plano($id) {
        return $this->db->get('planos', array('id', $id));
    }

    function planos_ativos() {
        return $this->db->get_where('planos', array('status' => 1));
    }
    
    function plano_ativo_usuario($email){
        return $this->db->get_where('transacoes', array('id_aluno'=>$email))->result();
    }

    function get_cat_princip_cur() {
        $this->db->select('*');
        $this->db->from('cursos_cat');
        $this->db->where('CCMostrar', 'Sim');
        $this->db->group_by('CCSubCat');
        $this->db->order_by('CCOrdem', 'esc');
        $data = $this->db->get();
        return $data->result();
    }

    function get_quant_cursos_enviados($usuarios_id) {
        $this->db->from('cursos');
        $this->db->where('usuarios_id_fk', $usuarios_id);
        $data = $this->db->get();
        return count($data->result());
    }

    function subcat_uma($CCSubCat) {
        $this->db->select('*');
        $this->db->from('cursos_cat');
        $this->db->where('CCSubCat', $CCSubCat);
        $this->db->where('CCMostrar', 'Sim');
        $data = $this->db->get();
        return $data->result();
    }

    function get_all($tabela) {
        $this->db->select('*');
        $this->db->from($tabela);
        $this->db->join('cedoc_doc', 'cedoc_doc.cedoc_doc_id = ' . $tabela . '.cedoc_doc_id_fk');
        $this->db->order_by('cedoc_doc.nome', 'esc');
        $data = $this->db->get();
        return $data->result();
    }

    function get_all_imagens($cedoc_doc_id_fk) {
        $this->db->select('*');
        $this->db->from('cedoc_doc_imagens');
        $this->db->where('cedoc_doc_id_fk', $cedoc_doc_id_fk);
        $this->db->where('cedoc_doc_imagens.caminho <>', '');
        $data = $this->db->get();
        return $data->result();
    }

    function get_all_arquivos($cedoc_doc_id_fk) {
        $this->db->select('*');
        $this->db->from('cedoc_doc_arquivos');
        $this->db->where('cedoc_doc_arquivos.cedoc_doc_id_fk', $cedoc_doc_id_fk);
        $this->db->where('cedoc_doc_arquivos.caminho <>', '');
        $data = $this->db->get();
        return $data->result();
    }

    function get_all_aulas_curso($cedoc_doc_id_fk) {
        $this->db->select('*');
        $this->db->from('cursos_aulas');
        $this->db->where('cursos_aulas.cedoc_doc_id_fk', $cedoc_doc_id_fk);
        $this->db->order_by('CAOrdem', 'esc');
        $data = $this->db->get();
        return $data->result();
    }

    function get_all_videos_externos($cedoc_doc_id_fk) {
        $this->db->select('*');
        $this->db->from('cedoc_doc_videos_externos');
        $this->db->where('cedoc_doc_videos_externos.cedoc_doc_id_fk', $cedoc_doc_id_fk);
        $this->db->where('cedoc_doc_videos_externos.link_video <>', '');
        $data = $this->db->get();
        return $data->result();
    }

    function get_all_links($cedoc_doc_id_fk) {
        $this->db->select('*');
        $this->db->from('cedoc_doc_links');
        $this->db->where('cedoc_doc_links.cedoc_doc_id_fk', $cedoc_doc_id_fk);
        $this->db->where('cedoc_doc_links.link_doc <>', '');
        $data = $this->db->get();
        return $data->result();
    }

    function get_all_um($tabela, $id) {
        if ($id) {
            $this->db->select('*');
            $this->db->from($tabela);
            $this->db->where($tabela . '_id', $id);
            $this->db->join('cedoc_doc', 'cedoc_doc.cedoc_doc_id = ' . $tabela . '.cedoc_doc_id_fk');
            $data = $this->db->get();
            return $data->result();
        } else {
            $this->db->select('*');
            $this->db->from($tabela);
            $this->db->join('cedoc_doc', 'cedoc_doc.cedoc_doc_id = ' . $tabela . '.cedoc_doc_id_fk');
            $this->db->order_by('cedoc_doc.nome', 'random');
            $this->db->limit(1);
            $data = $this->db->get();
            return $data->result();
        }
    }

    function get_arquivos_busca($data) {

        $query = 'SELECT * FROM cursos JOIN cursos_cat ON cursos_cat.cursos_cat_id = cursos.cursos_cat_id_fk JOIN 
						cedoc_doc ON cedoc_doc.cedoc_doc_id = cedoc_doc_id_fk AND CCDisponivel = "Sim" AND CPublicado = "Sim"';

        //$query = 'SELECT * FROM cursos JOIN cursos_cat ON cursos_cat.cursos_cat_id = cursos.cursos_cat_id_fk JOIN cedoc_doc ON cedoc_doc.cedoc_doc_id = cedoc_doc_id_fk';

        if ($data['CCSubCat'] == 'cedoc_doc_id_fk') {
            $query .= ' WHERE cedoc_doc_id_fk = "' . $data['filter'] . '"';
        } else {
            if ($data['CCSubCat'] == 'Tudo') {
                $query .= ' WHERE (nome LIKE ' . "'%" . $data['filter'] . "%' OR CCSubCat LIKE " . "'%" . $data['filter'] . "%')";
            } else {
                $query .= ' WHERE CCSubCat = "' . $data['CCSubCat'] . '" AND (nome LIKE ' . "'%" . $data['filter'] . "%' OR CCSubCat LIKE " . "'%" . $data['filter'] . "%')";
                $id_subcat = $this->subcat_uma($data['filter']);
                if (isset($id_subcat[0])) {
                    $query .= ' AND (nome LIKE ' . "'%" . $data['filter'] . "%' OR CCSubCat LIKE " . "'%" . $id_subcat[0]->CCSubCat . "%')";
                }
            }
        }

        if (($data['data_ini'] <> '0000-00-00') && ($data['data_fin'] <> '0000-00-00')) {
            $query .= ' AND data_criacao >= "' . $data['data_ini'] . ' 00:00:00" AND data_criacao <= "' . $data['data_fin'] . '23:59:59"';
        }


        if ($data['contar'] == 0) {
            if ($data['CCSubCat'] == 'Tudo') {
                $query .= ' LIMIT ' . intval($data['per_page']) . ' OFFSET ' . intval($data['pagina']);
            } else {
                $query .= ' ORDER BY CDestaque desc, CDestaqueCat desc, rand() LIMIT ' . intval($data['per_page']) . ' OFFSET ' . intval($data['pagina']);
            }

            return $this->db->query($query)->result();
        } else {

            return count($this->db->query($query)->result());
        }
    }

    function cat_busca() {
        $query = 'SELECT * FROM cursos_cat WHERE cursos_cat.CCMostrar = "Sim" GROUP BY CCSubCat ORDER BY CCOrdem';
        $retorno = $this->db->query($query);
        return $retorno->result();
    }

    function valida_email($email) {
        $this->db->from('usuarios');
        $this->db->where('email', $email);
        $this->db->where('valido', 'Sim');
        if ($this->db->count_all_results() >= 1) {
            return false;
        } else {
            return true;
        }
    }

    function valida_email_existe($email) {
        $this->db->select('*');
        $this->db->from('usuarios');
        $this->db->where('email', $email);
        if ($this->db->count_all_results() >= 1) {
            return true;
        } else {
            return false;
        }
    }

    function usuarios_cadastra($data) {
        $data['data_cadastro_usuario'] = date('Y-m-d');
        $data['valido'] = 'Sim';
        $data['hashing'] = $this->super_model->genRandomString();

        $this->db->insert('usuarios', $data);
    }

    function get_registro_usuario($email) {
        $this->db->select('*');
        $this->db->from('usuarios');
        $this->db->where('email', $email);
        $data = $this->db->get();
        return $data->result();
    }

    function email_valida($data) {
        $this->db->select('*');
        $this->db->from('usuarios');
        $this->db->where('email', $email);
        $data = $this->db->get();
        return $data->result();
    }

    function validar_hashing($data) {
        $this->db->select('*');
        $this->db->from('usuarios');
        $this->db->where('email', $data['email']);
        $this->db->where('hashing', $data['hashing']);
        $this->db->limit(1);
        if ($this->db->count_all_results() == 1) {
            $this->db->select('*');
            $this->db->from('usuarios');
            $this->db->where('email', $data['email']);
            $this->db->where('hashing', $data['hashing']);
            $this->db->limit(1);
            $info = $this->db->get();
            foreach ($info->result() as $usuario) {
                $info2['valido'] = 'Sim';
                $this->db->where('usuarios_id', $usuario->usuarios_id);
                $this->db->update('usuarios', $info2);
            }
            return true;
        } else {
            return false;
        }
    }

    function registra_visita_curso($cursos_id) {
        $this->db->from('cursos');
        //$this->db->where('CPublicado','Sim');
        $this->db->where('cursos_id', $cursos_id);
        $data = $this->db->get();
        foreach ($data->result() as $item) {
            if ($item->CVisitas <> 0) {
                $info['CVisitas'] = $item->CVisitas + 1;
            } else {
                $info['CVisitas'] = 1;
            }
            $this->db->where('cursos_id', $item->cursos_id);
            $this->db->update('cursos', $info);
        }
    }

    function registra_visita_usuario($usuarios_id) {
        $this->db->from('usuarios');
        $this->db->where('usuarios_id', $usuarios_id);
        $this->db->where('status_usuario', 'Ativo');
        $data = $this->db->get();
        foreach ($data->result() as $item) {
            if ($item->UVisitas <> 0) {
                $info['UVisitas'] = $item->UVisitas + 1;
            } else {
                $info['UVisitas'] = 1;
            }
            $this->db->where('usuarios_id', $item->usuarios_id);
            $this->db->update('usuarios', $info);
        }
    }

    function checa_logado_usuario() {
        if ($this->session->userdata('usuario_logado')) {
            $info['nome'] = $this->session->userdata('nome');
            $info['email'] = $this->session->userdata('email');
            $info['usuarios_id'] = $this->session->userdata('usuarios_id');
            $info['senha'] = $this->session->userdata('senha');
            $info['usuario_logado'] = $this->session->userdata('usuario_logado');
            $this->db->select('*');
            $this->db->from('usuarios');
            $this->db->where('usuarios_id', $info['usuarios_id']);
            $this->db->where('email', $info['email']);
            $this->db->where('senha', $info['senha']);
            $this->db->where('status_usuario', 'Ativo');
            $logged = $this->db->count_all_results();

            if ($logged == 1) {
                return $info;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    function usuarios_get_um($id) {
        $this->db->select('*');
        $this->db->from('usuarios');
        $this->db->where('usuarios_id', $id);
        $data = $this->db->get();
        return $data->result();
    }

    function usuarios_get_um_email($email) {
        $this->db->select('*');
        $this->db->from('usuarios');
        $this->db->where('email', $email);
        $data = $this->db->get();
        return $data->result();
    }

    function incrementa_quant_login($usuarios_id, $UQuantLogin) {
        $info['UQuantLogin'] = $UQuantLogin + 1;
        $info['UUltimoLogin'] = date('Y-m-d H:i:s');
        $this->db->where('usuarios_id', $usuarios_id);
        $this->db->update('usuarios', $info);
    }

    function usuarios_verifica_login($email, $senha) {
        $this->db->select('*');
        $this->db->from('usuarios');
        $this->db->where('email', $email);
        $this->db->where('senha', $senha);
        $this->db->where('status_usuario', 'Ativo');
        $data = $this->db->get();
        return $data->result();
    }

    function recuperar_senha_usuario($email) {
        $this->db->select('*');
        $this->db->from('usuarios');
        $this->db->where('email', $email);
        $data = $this->db->get();
        return $data->result();
    }

    function usuarios_atualiza($data) {

        unset($data['email']);
        $this->db->where('usuarios_id', $data['usuarios_id']);
        $this->db->update('usuarios', $data);
    }

    function get_usuario_arquivo($usuarios_id_fk) {
        $this->db->from('usuarios');
        $this->db->where('usuarios_id', $usuarios_id_fk);
        $data = $this->db->get();
        return $data->result();
    }

    function get_usuario($usuarios_id_fk) {
        $this->db->from('usuarios');
        $this->db->where('usuarios_id', $usuarios_id_fk);
        $this->db->where('status_usuario', 'Ativo');
        $data = $this->db->get();
        return $data->result();
    }

    function get_usuario_cursos($usuarios_id) {
        $this->db->select('*');
        $this->db->from('cursos');
        //$this->db->where('CPublicado','Sim');
        $this->db->where('usuarios_id_fk', $usuarios_id);
        $data = $this->db->get();
        return $data->result();
    }

    function get_usuario_cursos_cat($usuarios_id) {
        $this->db->select('*');
        $this->db->from('cursos');
        //$this->db->where('CPublicado','Sim');

        $this->db->where('usuarios_id_fk', $usuarios_id);
        $this->db->join('cursos_cat', 'cursos_cat.cursos_cat_id = cursos.cursos_cat_id_fk');

        $this->db->group_by('cursos.cursos_cat_id_fk');
        $this->db->order_by('cursos_cat.CCCat', 'esc');
        $this->db->order_by('cursos_cat.CCSubCat', 'esc');
        $data = $this->db->get();
        return $data->result();
    }

    function get_quant_itens_cat_cur($cursos_cat_id) {
        $this->db->from('cursos');
        $this->db->where('CPublicado', 'Sim');
        $this->db->where('cursos_cat_id_fk', $cursos_cat_id);
        return $this->db->count_all_results();
    }

    function get_quant_cursos_cat_usuario($cursos_cat_id, $usuarios_id_fk) {
        $this->db->from('cursos');
        //$this->db->where('CPublicado','Sim');
        $this->db->where('cursos_cat_id_fk', $cursos_cat_id);
        $this->db->where('usuarios_id_fk', $usuarios_id_fk);
        return $this->db->count_all_results();
    }

    function status_usuario($usuarios_id) {
        $this->db->select('usuarios_id,status_usuario');
        $this->db->from('usuarios');
        $this->db->where('usuarios_id', $usuarios_id);
        $this->db->limit("1");
        $status_usuario = $this->db->get();
        return $status_usuario->row('status_usuario');
    }

    function get_usuario_email($email) {
        $this->db->from('usuarios');
        $this->db->where('email', $email);
        $this->db->where('status_usuario', 'Ativo');
        $data = $this->db->get();
        return $data->result();
    }

    function quant_total_arquivos_usuario($data, $usuarios_id) {

        $this->db->select('*');
        $this->db->from('cedoc_doc');
        $this->db->where('data_criacao <>', '0000-00-00 00:00:00');
        $this->db->where('data_criacao >=', $this->formata_data($data['periodoDe']) . ' 00:00:00');
        $this->db->where('data_criacao <=', $this->formata_data($data['periodoAte']) . ' 23:59:59');
        $this->db->join('cursos', 'cursos.cedoc_doc_id_fk = cedoc_doc.cedoc_doc_id AND cursos.CPublicado = "Sim" AND usuarios_id_fk = ' . $usuarios_id);
        $cursos = $this->db->count_all_results();



        return $cursos;
    }

    function formata_data($dados) {
        $data_correta = '0000-00-00 00:00';
        $data_format = explode("*", $dados);
        if (isset($data_format[2])) {
            $data_hora = explode(" ", $data_format[2]);
            if (isset($data_hora[1])) {
                $data_correta = $data_hora[0] . '-' . $data_format[1] . '-' . $data_format['0'] . ' ' . $data_hora[1];
            } else {
                $data_correta = $data_hora[0] . '-' . $data_format[1] . '-' . $data_format['0'];
            }
        }
        return $data_correta;
    }

    function get_imagem_arquivo($cedoc_doc_id_fk) {
        $this->db->from('cedoc_doc_imagens');
        $this->db->where('cedoc_doc_id_fk', $cedoc_doc_id_fk);
        $this->db->limit(1);
        $data = $this->db->get();
        return $data->row('caminho');
    }

    function get_arquivo($cedoc_doc_id_fk) {
        $this->db->select('*');
        $this->db->select('CCCat');
        $this->db->select('CCSubCat');
        $this->db->from('cursos');

        $this->db->where('cedoc_doc_id_fk', $cedoc_doc_id_fk);
        $this->db->join('cursos_cat', 'cursos_cat.cursos_cat_id = cursos.cursos_cat_id_fk');
        $this->db->join('cedoc_doc', 'cedoc_doc.cedoc_doc_id = cursos.cedoc_doc_id_fk');
        $this->db->limit(1);
        $data = $this->db->get();
        return $data->result();
    }

    function cursos_cat_todas() {
        $this->db->select('*');
        $this->db->from('cursos_cat');
        $this->db->where('CCDisponivel', 'Sim');
        $this->db->order_by('CCCat', 'esc');
        $this->db->order_by('CCSubCat', 'esc');
        $data = $this->db->get();
        return $data->result();
    }

    function usuarios_cursos_cadastra($data) {
        $info2['data_criacao'] = $data['data_criacao'];
        $info2['nome'] = $data['nome'];
        $info2['login_id_fk'] = 0;
        $info2['publicoalvo'] = $data['publicoalvo'];
        $info2['objetivo'] = $data['objetivo'];
        $info2['apresentacao'] = $data['apresentacao'];
        $info2['carga_horaria'] = $data['carga_horaria'];
        $this->db->insert('cedoc_doc', $info2);

        $this->db->select('cedoc_doc_id');
        $this->db->from('cedoc_doc');
        $this->db->order_by("cedoc_doc_id", "desc");
        $this->db->limit("1");
        $cedoc_doc_id = $this->db->get();
        $info['cedoc_doc_id_fk'] = $cedoc_doc_id->row('cedoc_doc_id');

        mkdir("./uploads/_files/cedoc_doc_" . $info['cedoc_doc_id_fk'], 0777);
        mkdir("./uploads/_images/cedoc_doc_" . $info['cedoc_doc_id_fk'], 0777);
        mkdir("./uploads/_thumb/cedoc_doc_" . $info['cedoc_doc_id_fk'], 0777);

        $info['usuarios_id_fk'] = $data['usuarios_id_fk'];
        $info['cursos_cat_id_fk'] = $data['cursos_cat_id_fk'];
        $info['CPublicado'] = $data['CPublicado'];

        $this->db->insert('cursos', $info);
        $this->db->select('*');
        $this->db->from('cursos');
        $this->db->where('usuarios_id_fk', $data['usuarios_id_fk']);
        $this->db->order_by("cursos_id", "desc");
        $this->db->limit("1");
        $cedoc_doc_id_fk = $this->db->get();
        return $cedoc_doc_id_fk->row('cedoc_doc_id_fk');
    }

    function usuarios_cursos($data) {
        $this->db->select('*');
        $this->db->from('cursos');
        $this->db->where('usuarios_id_fk', $data['usuarios_id']);
        if (!isset($data['contar'])) {
            if ($data['cedoc_doc_id_fk'] > 0) {
                $this->db->where('cursos.cedoc_doc_id_fk', $data['cedoc_doc_id_fk']);
            }
            $this->db->join('cedoc_doc', 'cedoc_doc.cedoc_doc_id = cursos.cedoc_doc_id_fk');
            $this->db->join('cursos_cat', 'cursos.cursos_cat_id_fk = cursos_cat.cursos_cat_id');
            $this->db->order_by('CCCat', 'esc');
            $this->db->order_by('CCSubCat', 'esc');
            $this->db->order_by('nome', 'esc');
            $data = $this->db->get('cursos', $data['per_page'], $data['pagina']);
            return $data->result();
        } else {
            return $this->db->count_all_results();
        }
    }

    function get_curso_capa($cedoc_doc_id_fk) {
        $this->db->from('cedoc_doc_imagens');
        $this->db->where('cedoc_doc_imagens.cedoc_doc_id_fk', $cedoc_doc_id_fk);
        $this->db->limit(1);
        $data = $this->db->get();
        return $data->result();
    }

    function usuarios_cursos_todos($usuarios_id) {
        $this->db->select('*');
        $this->db->from('cursos');
        $this->db->where('usuarios_id_fk', $usuarios_id);
        $this->db->join('cedoc_doc', 'cedoc_doc.cedoc_doc_id = cursos.cedoc_doc_id_fk');
        $this->db->join('cursos_cat', 'cursos.cursos_cat_id_fk = cursos_cat.cursos_cat_id');
        $this->db->order_by('nome', 'esc');
        $data = $this->db->get();
        return $data->result();
    }

    function get_curso_editar($cedoc_doc_id_fk, $usuarios_id) {
        $this->db->select('*');
        $this->db->from('cursos');
        $this->db->where('cedoc_doc_id_fk', $cedoc_doc_id_fk);
        $this->db->where('usuarios_id_fk', $usuarios_id);
        $this->db->join('cedoc_doc', 'cedoc_doc.cedoc_doc_id = cursos.cedoc_doc_id_fk');
        $this->db->join('cursos_cat', 'cursos.cursos_cat_id_fk = cursos_cat.cursos_cat_id');
        $data = $this->db->get();
        return $data->result();
    }

    function usuarios_cursos_atualiza($data) {
        $info['cursos_cat_id_fk'] = $data['cursos_cat_id_fk'];
        $this->db->where('cedoc_doc_id_fk', $data['cedoc_doc_id_fk']);
        $this->db->where('usuarios_id_fk', $data['usuarios_id_fk']);
        $this->db->update('cursos', $info);

        $this->db->where('cedoc_doc_id_fk', $data['cedoc_doc_id_fk']);
        $this->db->where('usuarios_id_fk', $data['usuarios_id_fk']);
        $cedoc_doc_id = $this->db->get('cursos');

        $info2['nome'] = $data['nome'];
        $info2['login_id_fk'] = 0;
        $info2['publicoalvo'] = $data['publicoalvo'];
        $info2['objetivo'] = $data['objetivo'];
        $info2['apresentacao'] = $data['apresentacao'];
        $info2['carga_horaria'] = $data['carga_horaria'];


        $this->db->where('cedoc_doc_id', $cedoc_doc_id->row('cedoc_doc_id_fk'));
        $this->db->update('cedoc_doc', $info2);
    }

    function dono_curso($usuarios_id_fk, $cedoc_doc_id_fk) {
        $this->db->where('cedoc_doc_id_fk', $cedoc_doc_id_fk);
        $this->db->where('usuarios_id_fk', $usuarios_id_fk);
        $cedoc_doc_id_fk = $this->db->get('cursos');
        return $cedoc_doc_id_fk->row('cedoc_doc_id_fk');
    }

    function cursos_imagem_miniatura_upload($file_name, $cedoc_doc_id_fk) {
        $this->db->from('cedoc_doc_imagens');
        $this->db->where('cedoc_doc_id_fk', $cedoc_doc_id_fk);
        $data = $this->db->get();
        foreach ($data->result() as $item) {
            unlink("./uploads/_thumb/" . $item->caminho);
        }

        $this->db->where('cedoc_doc_id_fk', $cedoc_doc_id_fk);
        $this->db->delete('cedoc_doc_imagens');
        $data = array(
            'caminho' => $file_name,
            'cedoc_doc_id_fk' => $cedoc_doc_id_fk
        );
        $this->db->insert('cedoc_doc_imagens', $data);
    }

    function get_quant_aulas_curso($cedoc_doc_id_fk) {
        $this->db->from('cursos_aulas');
        $this->db->where('cursos_aulas.cedoc_doc_id_fk', $cedoc_doc_id_fk);
        $data = $this->db->get();
        return count($data->result());
    }

    function curso_get_aulas_dono($cedoc_doc_id_fk, $usuarios_id_fk) {
        $this->db->from('cursos');
        $this->db->where('usuarios_id_fk', $usuarios_id_fk);
        $this->db->where('cursos.cedoc_doc_id_fk', $cedoc_doc_id_fk);
        $this->db->join('cursos_aulas', 'cursos_aulas.cedoc_doc_id_fk = cursos.cedoc_doc_id_fk');
        $this->db->order_by('cursos_aulas.CAOrdem', 'esc');
        $data = $this->db->get();
        return $data->result();
    }

    function cursos_aulas_upload($file_name, $cedoc_doc_id_fk, $descricao, $ordem_aula, $descricao_aula) {
        $data = array(
            'caminho' => $file_name,
            'descricao' => $descricao,
            'cedoc_doc_id_fk' => $cedoc_doc_id_fk,
            'ordem_aula' => $ordem_aula,
            'descricao_aula' => $descricao_aula
        );
        $this->db->insert('cedoc_doc_arquivos', $data);
        $this->db->select('cedoc_doc_arquivos_id');
        $this->db->from('cedoc_doc_arquivos');
        $this->db->where('cedoc_doc_id_fk', $cedoc_doc_id_fk);
        $this->db->order_by("cedoc_doc_arquivos_id", "desc");
        $this->db->limit("1");
        $cedoc_doc_arquivos_id = $this->db->get();
        return $cedoc_doc_arquivos_id->row('cedoc_doc_arquivos_id');
    }

    function exercicios_curso($cedoc_doc_id_fk) {
        $this->db->from('cursos_aulas');
        $this->db->where('cedoc_doc_id_fk', $cedoc_doc_id_fk);
        $this->db->join('cursos_aulas_exercicios', 'cursos_aulas_exercicios.cursos_aulas_id_fk = cursos_aulas.cursos_aulas_id');
        $data = $this->db->get();
        return $data->result();
    }

    function cursos_aulas_exercicios_cadastra($data) {
        $info = array(
            'CAEPergunta' => $data['CAEPergunta'],
            'CAEAlternativa1' => $data['CAEAlternativa1'],
            'CAEAlternativa2' => $data['CAEAlternativa2'],
            'CAEAlternativa3' => $data['CAEAlternativa3'],
            'CAEAlternativa4' => $data['CAEAlternativa4'],
            'CAEAlternativaCorreta' => $data['CAEAlternativaCorreta'],
            'cursos_aulas_id_fk' => $data['cursos_aulas_id_fk'],
        );
        $this->db->insert('cursos_aulas_exercicios', $info);
        $this->db->select('*');
        $this->db->from('cursos_aulas_exercicios');
        $this->db->where('cursos_aulas_id_fk', $data['cursos_aulas_id_fk']);
        $this->db->order_by("cursos_aulas_exercicios_id", "desc");
        $this->db->limit("1");
        $cursos_aulas_exercicios_id = $this->db->get();
        return $cursos_aulas_exercicios_id->row('cursos_aulas_exercicios_id');
    }

    function get_exercicios_aula($cursos_aulas_id_fk) {
        $this->db->from('cursos_aulas_exercicios');
        $this->db->where('cursos_aulas_id_fk', $cursos_aulas_id_fk);
        $data = $this->db->get();
        return $data->result();
    }

    function despublica_curso($cedoc_doc_id_fk, $usuarios_id_fk) {
        $info['CPublicado'] = 'Nao';
        $this->db->where('cedoc_doc_id_fk', $cedoc_doc_id_fk);
        $this->db->where('usuarios_id_fk', $usuarios_id_fk);
        $this->db->update('cursos', $info);
    }

    function cursos_aulas_atualiza($data) {
        $info['CADescricao'] = $data['CADescricao'];
        $info['CAOrdem'] = $data['CAOrdem'];
        $info['CATitulo'] = $data['CATitulo'];
        $this->db->where('cursos_aulas_id', $data['cursos_aulas_id']);
        $this->db->update('cursos_aulas', $info);
    }

    function cursos_aulas_exclui($cursos_aulas_id) {
        $this->db->where('cursos_aulas_id', $cursos_aulas_id);
        $this->db->delete('cursos_aulas');
    }

    function get_arquivo_aula_um_dono($cursos_aulas_arquivos_id, $usuarios_id_fk) {
        $this->db->select('*');
        $this->db->from('cursos_aulas_arquivos');
        $this->db->where('cursos_aulas_arquivos_id', $cursos_aulas_arquivos_id);
        $this->db->join('cursos_aulas', 'cursos_aulas_arquivos.cursos_aulas_id_fk = cursos_aulas.cursos_aulas_id');
        $this->db->join('cursos', 'cursos.cedoc_doc_id_fk = cursos_aulas.cedoc_doc_id_fk');
        $this->db->join('cedoc_doc', 'cedoc_doc.cedoc_doc_id = cursos_aulas.cedoc_doc_id_fk');
        $this->db->where('cursos.usuarios_id_fk', $usuarios_id_fk);
        $data = $this->db->get();
        return $data->result();
    }

    function get_curso_um($cedoc_doc_id_fk) {
        $this->db->from('cursos');
        $this->db->where('cedoc_doc_id_fk', $cedoc_doc_id_fk);
        $this->db->join('cedoc_doc', 'cedoc_doc.cedoc_doc_id = cursos.cedoc_doc_id_fk');
        $this->db->join('cursos_cat', 'cursos.cursos_cat_id_fk = cursos_cat.cursos_cat_id');
        $data = $this->db->get();
        return $data->result();
    }

    function matricula_usuario($cedoc_doc_id_fk, $usuarios_id_fk) {
        $this->db->from('usuarios_cursos_matriculados');
        $this->db->where('cedoc_doc_id_fk', $cedoc_doc_id_fk);
        $this->db->where('usuarios_id_fk', $usuarios_id_fk);
        if ($this->db->count_all_results() == 0) {
            $info['cedoc_doc_id_fk'] = $cedoc_doc_id_fk;
            $info['usuarios_id_fk'] = $usuarios_id_fk;
            $info['UCMData'] = date('Y-m-d H:i:s');
            $this->db->insert('usuarios_cursos_matriculados', $info);
        }
    }

    function usuarios_cursos_matriculados($data) {
        $this->db->from('usuarios_cursos_matriculados');
        $this->db->where('usuarios_cursos_matriculados.email', $data);
        $this->db->join('cursos', 'cursos.cedoc_doc_id_fk = usuarios_cursos_matriculados.cedoc_doc_id_fk', 'left');
        $this->db->join('cursos_cat', 'cursos.cursos_cat_id_fk = cursos_cat.cursos_cat_id', 'left');
        $this->db->join('cedoc_doc', 'cedoc_doc.cedoc_doc_id = cursos.cedoc_doc_id_fk', 'left');

        if (!isset($data['contar'])) {
            if ($data['cedoc_doc_id_fk'] > 0) {
                $this->db->where('cursos.cedoc_doc_id_fk', $data['cedoc_doc_id_fk']);
            }
            return $this->db->get()->result();
        } else {
            return $this->db->count_all_results();
        }
    }

    function usuarios_cursos_dono($data) {
        $this->db->from('cursos');
        $this->db->where('cursos.cedoc_doc_id_fk', $data['cedoc_doc_id_fk']);
        $this->db->where('cursos.usuarios_id_fk', $data['usuarios_id']);
        $this->db->join('cursos_cat', 'cursos.cursos_cat_id_fk = cursos_cat.cursos_cat_id');
        $this->db->join('cedoc_doc', 'cedoc_doc.cedoc_doc_id = cursos.cedoc_doc_id_fk');
        $data = $this->db->get();
        return $data->result();
    }

    function usuarios_cursos_matriculados_todos($usuarios_id) {
        $this->db->from('usuarios_cursos_matriculados');
        $this->db->where('usuarios_cursos_matriculados.usuarios_id_fk', $usuarios_id);
        $this->db->join('cursos', 'cursos.cedoc_doc_id_fk = usuarios_cursos_matriculados.cedoc_doc_id_fk', 'left');
        $this->db->join('cursos_cat', 'cursos.cursos_cat_id_fk = cursos_cat.cursos_cat_id', 'left');
        $this->db->join('cedoc_doc', 'cedoc_doc.cedoc_doc_id = cursos.cedoc_doc_id_fk', 'left');
        $data = $this->db->get();
        return $data->result();
    }

    function get_aulas_curso($cedoc_doc_id_fk) {
        $this->db->from('cedoc_doc_arquivos');
        $this->db->where('cedoc_doc_arquivos.cedoc_doc_id_fk', $cedoc_doc_id_fk);
        $this->db->where('caminho <>', '');
        $data = $this->db->get();
        return $data->result();
    }

    function curso_matriculado_certif($cedoc_doc_id_fk, $usuarios_id_fk) {
        $this->db->from('usuarios_cursos_matriculados');
        $this->db->where('usuarios_cursos_matriculados.email', $usuarios_id_fk);
        $this->db->where('usuarios_cursos_matriculados.cedoc_doc_id_fk', $cedoc_doc_id_fk);
        $this->db->where('UCMCertificadoLiberado', 'Sim');
        $data = $this->db->get();
        return $data->result();
    }

    function get_arquivo_aula_um($cedoc_doc_id_fk, $cedoc_doc_arquivos_id) {
        $this->db->select('*');
        $this->db->select('cedoc_doc_arquivos.descricao as titulo_aula');
        $this->db->from('cedoc_doc_arquivos');
        $this->db->where('cedoc_doc_arquivos_id', $cedoc_doc_arquivos_id);
        $this->db->where('cedoc_doc_arquivos.cedoc_doc_id_fk', $cedoc_doc_id_fk);
        $this->db->join('cedoc_doc', 'cedoc_doc.cedoc_doc_id = cedoc_doc_arquivos.cedoc_doc_id_fk');
        $this->db->join('cursos', 'cursos.cedoc_doc_id_fk = cedoc_doc.cedoc_doc_id');
        $this->db->join('cursos_cat', 'cursos.cursos_cat_id_fk = cursos_cat.cursos_cat_id');
        $data = $this->db->get();
        return $data->result();
    }

    function get_aula_realizada($cursos_aulas_id, $usuarios_id_fk) {
        $this->db->from('cursos_aulas_realizadas');
        $this->db->where('cursos_aulas_id_fk', $cursos_aulas_id);
        $this->db->where('usuarios_id_fk', $usuarios_id_fk);
        $data = $this->db->get();
        return $data->result();
    }

    function get_quant_aulas_realizadas($aulas, $email) {
        $quant_aulas = 0;
        foreach ($aulas as $aula) {
            $this->db->from('cursos_aulas_realizadas');
            $this->db->where('cursos_aulas_id_fk', $aula->cursos_aulas_id);
            $this->db->where('email', $email);
            $data = $this->db->get();
            $quant_aulas+= count($data->result());
        }
        return $quant_aulas;
    }

    function ja_imprimiu_certif($aulas, $usuarios_id) {
        foreach ($aulas as $aula) {
            $this->db->from('cursos_aulas_realizadas');
            $this->db->where('cursos_aulas_id_fk', $aula->cursos_aulas_id);
            $this->db->where('usuarios_id_fk', $usuarios_id);
            $this->db->where('CARHashingCertificado <>', '');
            if ($this->db->count_all_results() > 0) {
                return true;
            }
        }
    }

    function get_ultima_aula_curso($cursos_id, $usuarios_id) {
        $this->db->from('cursos');
        $this->db->where('cursos_id', $cursos_id);
        $this->db->join('cursos_aulas', 'cursos.cedoc_doc_id_fk = cursos_aulas.cedoc_doc_id_fk');
        $this->db->join('cursos_aulas_realizadas', 'cursos_aulas.cursos_aulas_id = cursos_aulas_realizadas.cursos_aulas_id_fk');
        $this->db->where('cursos_aulas_realizadas.usuarios_id_fk', $usuarios_id);
        $this->db->order_by('CARDataConcluida', 'desc');
        $this->db->limit(1);
        $data = $this->db->get();
        return ($data->result());
    }

    function remove_aula_realizada($cursos_aulas_id_fk, $usuarios_id_fk) {
        $this->db->where('cursos_aulas_id_fk', $cedoc_doc_arquivos_id);
        $this->db->where('usuarios_id_fk', $usuarios_id_fk);
        $this->db->delete('cursos_aulas_realizadas');
    }

    function adiciona_aula_realizada($cursos_aulas_id_fk, $usuarios_id_fk, $campo, $id) {
        $this->db->from('cursos_aulas_realizadas');
        $this->db->where('cursos_aulas_id_fk', $cursos_aulas_id_fk);
        $this->db->where('usuarios_id_fk', $usuarios_id_fk);
        if ($campo <> '') {
            $this->db->where($campo, $id);
        } else {
            $this->db->where('cursos_aulas_arquivos_id_fk', '0');
            $this->db->where('cursos_aulas_videos_id_fk', '0');
            $this->db->where('cursos_aulas_exercicios_id_fk', '0');
        }
        $this->db->where('CARHashingCertificado <>', '');
        if ($this->db->count_all_results() == 0) {
            $this->db->from('cursos_aulas_realizadas');
            $this->db->where('cursos_aulas_id_fk', $cursos_aulas_id_fk);
            $this->db->where('usuarios_id_fk', $usuarios_id_fk);
            if ($campo <> '') {
                $this->db->where($campo, $id);
            } else {
                $this->db->where('cursos_aulas_arquivos_id_fk', '0');
                $this->db->where('cursos_aulas_videos_id_fk', '0');
                $this->db->where('cursos_aulas_exercicios_id_fk', '0');
                $this->db->where('cursos_aulas_imagens_id_fk', '0');
            }
            $this->db->delete('cursos_aulas_realizadas');
            $info['cursos_aulas_id_fk'] = $cursos_aulas_id_fk;
            $info['usuarios_id_fk'] = $usuarios_id_fk;
            $info['CARDataConcluida'] = date('Y-m-d H:i:s');
            if ($campo <> '') {
                $info[$campo] = $id;
            }
            $this->db->insert('cursos_aulas_realizadas', $info);
            return $this->db->insert_id();
        }
    }

    function get_total_itens_curso($aulas, $cedoc_doc_id_fk) {
        $total = 0;
        $total += count($aulas);


        foreach ($aulas as $aula) {
            $this->db->from('cursos_aulas_arquivos');
            $this->db->where('cursos_aulas_arquivos.cursos_aulas_id_fk', $aula->cursos_aulas_id);
            $data = $this->db->get();
            $total += count($data->result());

            $this->db->from('cursos_aulas_videos');
            $this->db->where('cursos_aulas_videos.cursos_aulas_id_fk', $aula->cursos_aulas_id);
            $data = $this->db->get();
            $total += count($data->result());

            $this->db->from('cursos_aulas_exercicios');
            $this->db->where('cursos_aulas_exercicios.cursos_aulas_id_fk', $aula->cursos_aulas_id);
            $this->db->group_by('cursos_aulas_id_fk');
            $data = $this->db->get();
            $total += count($data->result());

            $this->db->from('cursos_aulas_imagens');
            $this->db->where('cursos_aulas_imagens.cursos_aulas_id_fk', $aula->cursos_aulas_id);
            $data = $this->db->get();
            $total += count($data->result());
        }
        return $total;
    }

    function get_curso_certif($cedoc_doc_id_fk) {
        $this->db->select('*');
        $this->db->from('cursos');
        $this->db->where('cedoc_doc_id_fk', $cedoc_doc_id_fk);
        $this->db->join('cedoc_doc', 'cedoc_doc.cedoc_doc_id = cursos.cedoc_doc_id_fk');
        $this->db->join('cursos_cat', 'cursos.cursos_cat_id_fk = cursos_cat.cursos_cat_id');
        $data = $this->db->get();
        return $data->result();
    }

    function get_menor_data_aula_realizada($cedoc_doc_id_fk, $usuarios_id_fk) {
        $this->db->from('cursos_aulas');
        $this->db->where('cursos_aulas.cedoc_doc_id_fk', $cedoc_doc_id_fk);
        $this->db->join('cursos_aulas_realizadas', 'cursos_aulas_realizadas.cursos_aulas_id_fk = cursos_aulas.cursos_aulas_id');
        $this->db->where('usuarios_id_fk', $usuarios_id_fk);
        $this->db->order_by('CARDataConcluida', 'esc');
        $this->db->limit(1);
        $data = $this->db->get();
        return $data->row('CARDataConcluida');
    }

    function get_maior_data_aula_realizada($cedoc_doc_id_fk, $usuarios_id_fk) {
        $this->db->from('cursos_aulas');
        $this->db->where('cursos_aulas.cedoc_doc_id_fk', $cedoc_doc_id_fk);
        $this->db->join('cursos_aulas_realizadas', 'cursos_aulas_realizadas.cursos_aulas_id_fk = cursos_aulas.cursos_aulas_id');
        $this->db->where('usuarios_id_fk', $usuarios_id_fk);
        $this->db->order_by('CARDataConcluida', 'desc');
        $this->db->limit(1);
        $data = $this->db->get();
        return $data->row('CARDataConcluida');
    }

    function get_hashing_certificado($cedoc_doc_id_fk, $usuarios_id_fk) {
        $this->db->from('cursos_aulas');
        $this->db->where('cursos_aulas.cedoc_doc_id_fk', $cedoc_doc_id_fk);
        $this->db->join('cursos_aulas_realizadas', 'cursos_aulas_realizadas.cursos_aulas_id_fk = cursos_aulas.cursos_aulas_id');
        $this->db->where('usuarios_id_fk', $usuarios_id_fk);
        ;
        $data = $this->db->get();
        foreach ($data->result() as $item) {
            if ($item->CARHashingCertificado) {
                return $item->CARHashingCertificado;
            } else {
                $info['CARHashingCertificado'] = $cedoc_doc_id_fk . $usuarios_id_fk . $this->super_model->genRandomStringMenor();
                $this->db->where('cursos_aulas_realizadas_id', $item->cursos_aulas_realizadas_id);
                $this->db->update('cursos_aulas_realizadas', $info);
                return $info['CARHashingCertificado'];
            }
        }
    }

    function validar_certificado($numero) {
        $this->db->select('*');
        $this->db->select('cedoc_doc.nome as nome_curso');
        $this->db->select('usuarios.nome as nome_usuario');
        $this->db->from('cursos_aulas_realizadas');
        $this->db->where('CARHashingCertificado', $numero);
        $this->db->join('usuarios', 'usuarios.usuarios_id = cursos_aulas_realizadas.usuarios_id_fk');
        $this->db->join('cursos_aulas', 'cursos_aulas_realizadas.cursos_aulas_id_fk = cursos_aulas.cursos_aulas_id');
        $this->db->join('cedoc_doc', 'cursos_aulas.cedoc_doc_id_fk = cedoc_doc.cedoc_doc_id');
        $info = $this->db->get();
        return $info->result();
    }

    function get_curso_professor($cedoc_doc_id_fk) {
        $this->db->from('cursos');
        $this->db->where('cedoc_doc_id_fk', $cedoc_doc_id_fk);
        $this->db->join('usuarios', 'usuarios.usuarios_id = cursos.usuarios_id_fk');
        $this->db->limit(1);
        $data = $this->db->get();
        return $data->result();
    }

    function curso_get_aula_dono($cedoc_doc_id_fk, $usuarios_id_fk, $cursos_aulas_id) {
        $this->db->from('cursos');
        $this->db->where('usuarios_id_fk', $usuarios_id_fk);
        $this->db->where('cursos.cedoc_doc_id_fk', $cedoc_doc_id_fk);
        $this->db->join('cursos_aulas', 'cursos_aulas.cedoc_doc_id_fk = cursos.cedoc_doc_id_fk');
        $this->db->where('cursos_aulas.cursos_aulas_id', $cursos_aulas_id);
        $data = $this->db->get();
        return $data->result();
    }

    function curso_get_aula($cursos_aulas_id) {
        $this->db->select('*');
        $this->db->select('cursos_aulas_id as cursos_aulas_id_fk');
        $this->db->from('cursos_aulas');
        $this->db->where('cursos_aulas.cursos_aulas_id', $cursos_aulas_id);
        $data = $this->db->get();
        return $data->result();
    }

    function usuarios_cursos_aulas_exercicios_atualiza($data) {
        $info = array(
            'CAEPergunta' => $data['CAEPergunta'],
            'CAEAlternativa1' => $data['CAEAlternativa1'],
            'CAEAlternativa2' => $data['CAEAlternativa2'],
            'CAEAlternativa3' => $data['CAEAlternativa3'],
            'CAEAlternativa4' => $data['CAEAlternativa4'],
            'CAEAlternativaCorreta' => $data['CAEAlternativaCorreta'],
        );
        $this->db->where('cursos_aulas_exercicios_id', $data['cursos_aulas_exercicios_id']);
        $this->db->update('cursos_aulas_exercicios', $info);
    }

    function usuarios_cursos_aulas_exercicios_exclui($cursos_aulas_exercicios_id) {
        $this->db->where('cursos_aulas_exercicios_id', $cursos_aulas_exercicios_id);
        $this->db->delete('cursos_aulas_exercicios');
    }

    function cursos_aulas_cadastra($data) {
        $data = array(
            'CATitulo' => $data['CATitulo'],
            'CADescricao' => $data['CADescricao'],
            'CAOrdem' => $data['CAOrdem'],
            'cedoc_doc_id_fk' => $data['cedoc_doc_id_fk']
        );
        $this->db->insert('cursos_aulas', $data);
        $this->db->select('cursos_aulas_id');
        $this->db->from('cursos_aulas');
        $this->db->where('cedoc_doc_id_fk', $data['cedoc_doc_id_fk']);
        $this->db->order_by("cursos_aulas_id", "desc");
        $this->db->limit("1");
        $cursos_aulas_id = $this->db->get();
        return $cursos_aulas_id->row('cursos_aulas_id');
    }

    function get_curso_aula($cedoc_doc_id_fk, $cursos_aulas_id) {
        $this->db->from('cursos');
        $this->db->where('cursos.cedoc_doc_id_fk', $cedoc_doc_id_fk);
        $this->db->join('cursos_aulas', 'cursos_aulas.cedoc_doc_id_fk = cursos.cedoc_doc_id_fk');
        $this->db->where('cursos_aulas_id', $cursos_aulas_id);
        $this->db->order_by('cursos_aulas.CAOrdem', 'esc');
        $data = $this->db->get();
        return $data->result();
    }

    function get_arquivos_aula($cursos_aulas_id_fk) {
        $this->db->from('cursos_aulas_arquivos');
        $this->db->where('cursos_aulas_id_fk', $cursos_aulas_id_fk);
        $this->db->join('cursos_aulas', 'cursos_aulas.cursos_aulas_id = cursos_aulas_arquivos.cursos_aulas_id_fk');
        $data = $this->db->get();
        return $data->result();
    }

    function get_imagens_aula($cursos_aulas_id_fk) {
        $this->db->from('cursos_aulas_imagens');
        $this->db->where('cursos_aulas_id_fk', $cursos_aulas_id_fk);
        $this->db->join('cursos_aulas', 'cursos_aulas.cursos_aulas_id = cursos_aulas_imagens.cursos_aulas_id_fk');
        $data = $this->db->get();
        return $data->result();
    }

    function curso_get_aula_itens($cursos_aulas_id_fk) {
        $query = 'SELECT CAATitulo as Titulo FROM ((SELECT CAATitulo FROM cursos_aulas_arquivos WHERE cursos_aulas_id_fk = ' . $cursos_aulas_id_fk . ') UNION (SELECT CAVTitulo FROM cursos_aulas_videos WHERE cursos_aulas_id_fk = ' . $cursos_aulas_id_fk . ') UNION (SELECT CAITitulo FROM cursos_aulas_imagens WHERE cursos_aulas_id_fk = ' . $cursos_aulas_id_fk . ')) as tabela';
        $query = $this->db->query($query);
        return $query->result();
    }

    function get_arquivo_aula($cursos_aulas_id_fk, $cursos_aulas_arquivos_id) {
        $this->db->from('cursos_aulas_arquivos');
        $this->db->where('cursos_aulas_id_fk', $cursos_aulas_id_fk);
        $this->db->where('cursos_aulas_arquivos_id', $cursos_aulas_arquivos_id);
        $data = $this->db->get();
        return $data->result();
    }

    function get_imagem_aula($cursos_aulas_id_fk, $cursos_aulas_imagens_id) {
        $this->db->from('cursos_aulas_imagens');
        $this->db->where('cursos_aulas_id_fk', $cursos_aulas_id_fk);
        $this->db->where('cursos_aulas_imagens_id', $cursos_aulas_imagens_id);
        $data = $this->db->get();
        return $data->result();
    }

    function get_video_aula($cursos_aulas_id_fk, $cursos_aulas_videos_id) {
        $this->db->from('cursos_aulas_videos');
        $this->db->where('cursos_aulas_id_fk', $cursos_aulas_id_fk);
        $this->db->where('cursos_aulas_videos_id', $cursos_aulas_videos_id);
        $data = $this->db->get();
        return $data->result();
    }

    function get_exercicio_aula($cursos_aulas_id_fk) {
        $this->db->from('cursos_aulas_exercicios');
        $this->db->where('cursos_aulas_id_fk', $cursos_aulas_id_fk);
        $data = $this->db->get();
        return $data->result();
    }

    function get_arquivo_aula_download($cursos_aulas_id_fk, $cursos_aulas_arquivos_id) {
        $this->db->from('cursos_aulas_arquivos');
        $this->db->where('cursos_aulas_id_fk', $cursos_aulas_id_fk);
        $this->db->where('cursos_aulas_arquivos_id', $cursos_aulas_arquivos_id);
        $this->db->where('cursos_aulas_arquivos.CAADownload', 'Sim');
        $this->db->join('cursos_aulas', 'cursos_aulas.cursos_aulas_id = cursos_aulas_arquivos.cursos_aulas_id_fk');
        $this->db->join('cedoc_doc', 'cedoc_doc.cedoc_doc_id = cursos_aulas.cedoc_doc_id_fk');
        $data = $this->db->get();
        return $data->result();
    }

    function get_arquivo_aula_assistir($cursos_aulas_id_fk, $cursos_aulas_arquivos_id) {
        $this->db->from('cursos_aulas_arquivos');
        $this->db->where('cursos_aulas_id_fk', $cursos_aulas_id_fk);
        $this->db->where('cursos_aulas_arquivos_id', $cursos_aulas_arquivos_id);
        $this->db->join('cursos_aulas', 'cursos_aulas.cursos_aulas_id = cursos_aulas_arquivos.cursos_aulas_id_fk');
        $this->db->join('cedoc_doc', 'cedoc_doc.cedoc_doc_id = cursos_aulas.cedoc_doc_id_fk');
        $this->db->join('cursos', 'cursos.cedoc_doc_id_fk = cedoc_doc.cedoc_doc_id');
        $this->db->join('cursos_cat', 'cursos_cat.cursos_cat_id = cursos.cursos_cat_id_fk', 'left');
        $data = $this->db->get();
        return $data->result();
    }

    function cursos_aulas_arquivos_upload($data) {
        $data = array(
            'CAACaminho' => $data['file_name'],
            'CAATitulo' => $data['CAATitulo'],
            'CAADescricao' => $data['CAADescricao'],
            'CAADownload' => $data['CAADownload'],
            'cursos_aulas_id_fk' => $data['cursos_aulas_id_fk']
        );
        $this->db->insert('cursos_aulas_arquivos', $data);
        $this->db->select('cursos_aulas_arquivos_id');
        $this->db->from('cursos_aulas_arquivos');
        $this->db->where('cursos_aulas_id_fk', $data['cursos_aulas_id_fk']);
        $this->db->order_by("cursos_aulas_arquivos_id", "desc");
        $this->db->limit("1");
        $cursos_aulas_arquivos_id = $this->db->get();
        return $cursos_aulas_arquivos_id->row('cursos_aulas_arquivos_id');
    }

    function usuarios_cursos_aulas_arquivos_atualiza($data) {
        $info = array(
            'CAATitulo' => $data['CAATitulo'],
            'CAADescricao' => $data['CAADescricao'],
            'CAADownload' => $data['CAADownload']
        );
        $this->db->where('cursos_aulas_arquivos_id', $data['cursos_aulas_arquivos_id']);
        $this->db->update('cursos_aulas_arquivos', $info);
    }

    function usuarios_cursos_aulas_arquivos_exclui($cursos_aulas_arquivos_id) {
        $this->db->where('cursos_aulas_arquivos_id', $cursos_aulas_arquivos_id);
        $this->db->delete('cursos_aulas_arquivos');
    }

    function cursos_aulas_imagens_upload($data) {
        $data = array(
            'CAICaminho' => $data['file_name'],
            'CAITitulo' => $data['CAITitulo'],
            'CAIDescricao' => $data['CAIDescricao'],
            'cursos_aulas_id_fk' => $data['cursos_aulas_id_fk']
        );
        $this->db->insert('cursos_aulas_imagens', $data);
        $this->db->select('cursos_aulas_imagens_id');
        $this->db->from('cursos_aulas_imagens');
        $this->db->where('cursos_aulas_id_fk', $data['cursos_aulas_id_fk']);
        $this->db->order_by("cursos_aulas_imagens_id", "desc");
        $this->db->limit("1");
        $cursos_aulas_imagens_id = $this->db->get();
        return $cursos_aulas_imagens_id->row('cursos_aulas_imagens_id');
    }

    function usuarios_cursos_aulas_imagens_atualiza($data) {
        $info = array(
            'CAITitulo' => $data['CAITitulo'],
            'CAIDescricao' => $data['CAIDescricao']
        );
        $this->db->where('cursos_aulas_imagens_id', $data['cursos_aulas_imagens_id']);
        $this->db->update('cursos_aulas_imagens', $info);
    }

    function usuarios_cursos_aulas_imagens_exclui($cursos_aulas_imagens_id) {
        $this->db->where('cursos_aulas_imagens_id', $cursos_aulas_imagens_id);
        $this->db->delete('cursos_aulas_imagens');
    }

    function get_videos_aula($cursos_aulas_id_fk) {
        $this->db->from('cursos_aulas_videos');
        $this->db->where('cursos_aulas_id_fk', $cursos_aulas_id_fk);
        $this->db->join('cursos_aulas', 'cursos_aulas.cursos_aulas_id = cursos_aulas_videos.cursos_aulas_id_fk');
        $data = $this->db->get();
        return $data->result();
    }

    function get_audios_aula($cursos_aulas_id_fk) {
        $this->db->from('cursos_aulas_arquivos');
        $this->db->where('cursos_aulas_id_fk', $cursos_aulas_id_fk);
        $this->db->join('cursos_aulas', 'cursos_aulas.cursos_aulas_id = cursos_aulas_arquivos.cursos_aulas_id_fk');
        $this->db->like('CAACaminho', 'mp3');
        $data = $this->db->get();
        return $data->result();
    }

    function cursos_aulas_videos_cadastra($data) {
        $data = array(
            'CAVCaminho' => $data['CAVCaminho'],
            'CAVTitulo' => $data['CAVTitulo'],
            'CAVDescricao' => $data['CAVDescricao'],
            'cursos_aulas_id_fk' => $data['cursos_aulas_id_fk']
        );
        $this->db->insert('cursos_aulas_videos', $data);
        $this->db->select('cursos_aulas_videos_id');
        $this->db->from('cursos_aulas_videos');
        $this->db->where('cursos_aulas_id_fk', $data['cursos_aulas_id_fk']);
        $this->db->order_by("cursos_aulas_videos_id", "desc");
        $this->db->limit("1");
        $cursos_aulas_videos_id = $this->db->get();
        return $cursos_aulas_videos_id->row('cursos_aulas_videos_id');
    }

    function usuarios_cursos_aulas_videos_atualiza($data) {
        $info = array(
            'CAVTitulo' => $data['CAVTitulo'],
            'CAVCaminho' => $data['CAVCaminho'],
            'CAVDescricao' => $data['CAVDescricao']
        );
        $this->db->where('cursos_aulas_videos_id', $data['cursos_aulas_videos_id']);
        $this->db->update('cursos_aulas_videos', $info);
    }

    function usuarios_cursos_aulas_videos_exclui($cursos_aulas_videos_id) {
        $this->db->where('cursos_aulas_videos_id', $cursos_aulas_videos_id);
        $this->db->delete('cursos_aulas_videos');
    }

    function salva_anotacao($data) {
        $this->db->from('cursos_aulas_anotacoes');
        $this->db->where('cursos_aulas_anotacoes_id', $data['cursos_aulas_anotacoes_id']);
        $this->db->where('usuarios_id_fk', $data['usuarios_id']);
        $info['CAAAnotacoes'] = $data['CAAAnotacoes'];
        $this->db->update('cursos_aulas_anotacoes', $info);
    }

    function get_anotacoes_curso($cedoc_doc_id_fk, $usuarios_id_fk) {
        $this->db->from('cursos_aulas_anotacoes');
        $this->db->where('cedoc_doc_id_fk', $cedoc_doc_id_fk);
        $this->db->where('usuarios_id_fk', $usuarios_id_fk);
        $data = $this->db->get();
        if (count($data->result()) == 0) {
            $info['cedoc_doc_id_fk'] = $cedoc_doc_id_fk;
            $info['usuarios_id_fk'] = $usuarios_id_fk;
            $this->db->insert('cursos_aulas_anotacoes', $info);
        } else {
            return $data->result();
        }
    }

    function usuarios_recados_cadastra($data) {
        $info['usuarios_id_fk_de'] = $data['usuarios_id_fk'];
        $info['usuarios_id_fk_para'] = $data['usuarios_id'];
        $info['URRecado'] = $data['URRecado'];
        $info['URDataCadastro'] = date('Y-m-d H:i:s');
        $this->db->insert('usuarios_recados', $info);

        $this->db->select('usuarios_recados_id');
        $this->db->from('usuarios_recados');
        $this->db->order_by("usuarios_recados_id", "desc");
        $this->db->limit("1");
        $usuarios_recados_id = $this->db->get();
        return $usuarios_recados_id->row('usuarios_recados_id');
    }

    function get_ultima_aula($cedoc_doc_id_fk) {
        $this->db->from('cursos_aulas');
        $this->db->where('cursos_aulas.cedoc_doc_id_fk', $cedoc_doc_id_fk);
        $this->db->order_by('cursos_aulas.CAOrdem', 'desc');
        $this->db->limit(1);
        $data = $this->db->get();
        return $data->result();
    }

    
    function buscar_certificado($data) {
        $this->db->select('*');
        $this->db->from('usuarios_cursos_matriculados');
        $this->db->join('cedoc_doc', 'usuarios_cursos_matriculados.cedoc_doc_id_fk = cedoc_doc.cedoc_doc_id');
        $this->db->where('usuarios_cursos_matriculados.status', 1);
        $this->db->where('usuarios_cursos_matriculados.email', $data);
        return $this->db->get();
    }

    function dados_curso_matriculado($curso) {
        $this->db->where('cedoc_doc_id_fk', $curso);
        $this->db->where('status', 1);
        $q2 = $this->db->get('usuarios_cursos_matriculados');
        return $q2;
    }

}
