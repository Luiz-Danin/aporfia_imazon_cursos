<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class usuarios_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->imazon = $this->load->database('imazon', TRUE);
        $this->imazon10 = $this->load->database('imazon10', TRUE);
        $this->imazoncursos = $this->load->database('imazoncursos', TRUE);
    }

    public function insere_log_usuarios($data) {
        return $this->db->insert('log_usuarios', $data);
    }
    
     
    public function get_all_log_usuarios($inicio = null, $limite = null, $busca = null){
       $this->db->from('log_usuarios');
        if($busca !== null){ $this->db->like('email', $busca);}
        if($inicio !== null && $limite !== null){$this->db->limit($limite, $inicio);}
        $this->db->order_by("dt_cadastro desc, email desc");
        return $this->db->get(); 
    }
	
    public function count_all_log_usuarios($busca = null){
        
        $this->db->from('log_usuarios');
        if($busca !== null){$this->db->like('email', $busca);}
		return $this->db->count_all_results();

    }

    public function verifica_status_curso($id, $usuario) {
        $this->db->from('usuarios_cursos_matriculados');
        $this->db->where('usuarios_cursos_matriculados_id', $id);
        $this->db->where('email', $usuario);
        //$this->db->where('status', '0');
        $this->db->limit('1');
        return $this->db->count_all_results();
    }

    public function verifica_email_cad($email) {
        $ver = count($this->imazoncursos->get_where('aluno', array('email' => $email))->result());
        $ver2 = count($this->imazoncursos->get_where('aluno', array('id_fic' => $email))->result());
        $ver3 = count($this->imazon->get_where('aluno', array('email' => $email))->result());
        if ($ver == 0 && $ver2 == 0 && $ver3 == 0) {
            return true;
        } else {
            return false;
        }
    }

    public function verifica_matricula($id, $usuario) {
        $this->db->from('usuarios_cursos_matriculados');
        $this->db->where('usuarios_cursos_matriculados_id', $id);
        $this->db->where('email', $usuario);
        //$this->db->where('status', '0');
        $this->db->limit('1');
        return $this->db->get();
    }

    public function verifica_validade_plano($usuarios_id) {
        return $this->imazoncursos->from('planos_usuarios')->where('usuario', $usuarios_id)->where('status', 1)->get();
    }

    public function historico($email) {
        $this->imazoncursos->select("*");
        $this->imazoncursos->where('historico.nota >=', 5);
        $this->imazoncursos->where('historico.situacao ', 1);
        $this->imazoncursos->where('email', $email);
        $this->imazoncursos->join('curso', 'curso.id_curso = historico.id_curso');
        $query = $this->imazoncursos->get('historico');
        return $query;
    }

    public function historico_imazon10($email) {
        $this->imazon10->select("*");
        $this->imazon10->where('historico.nota >=', 5);
        $this->imazon10->where('email', $email);
        $this->imazon10->join('curso', 'curso.id_curso = historico.id_curso');
        $query = $this->imazon10->get('historico');
        return $query;
    }

    //lista dados por email
    public function get_nome_certificado($aluno) {
        return $this->imazoncursos->get_where('aluno', array('nome' => $aluno), 1);
    }

    //lista dados por email
    public function get_nome_recibo($email) {
        return $this->imazoncursos->get_where('aluno', array('email' => $email), 1);
    }

    //lista cursos em que o aluno est� matriculado
    public function usuarios_cursos_matriculados($data, $inicio = null, $limite = null, $filtro = null) {
        $this->db->from('usuarios_cursos_matriculados');
        $this->db->where('usuarios_cursos_matriculados.email', $data);
        $this->db->join('cursos', 'cursos.cedoc_doc_id_fk = usuarios_cursos_matriculados.cedoc_doc_id_fk');
        $this->db->join('cursos_cat', 'cursos.cursos_cat_id_fk = cursos_cat.cursos_cat_id');
        $this->db->join('cedoc_doc', 'cedoc_doc.cedoc_doc_id = cursos.cedoc_doc_id_fk');
        $this->db->where_not_in('usuarios_cursos_matriculados.status', array('2', '3', '4'));
        $this->db->order_by('usuarios_cursos_matriculados_id', 'desc');
        if ($filtro !== null)
            $this->db->like('cedoc_doc.nome', $filtro);
        if ($inicio !== null)
            $this->db->limit($limite, $inicio);
        return $this->db->get()->result();
        //exit;
    }

    //lista cursos em que o aluno est� matriculado
    public function count_cursos_matriculados($data, $inicio = null, $limite = null, $filtro = null) {
        $this->db->from('usuarios_cursos_matriculados');
        $this->db->where('usuarios_cursos_matriculados.email', $data);
        $this->db->join('cedoc_doc', 'cedoc_doc.cedoc_doc_id = usuarios_cursos_matriculados.cedoc_doc_id_fk');
        $this->db->where_not_in('usuarios_cursos_matriculados.status', array('2', '3', '4'));
        if ($filtro !== null)
            $this->db->like('cedoc_doc.nome', $filtro);
        return $this->db->get()->result();
    }

    public function get_ultimo_plano_usuario($usuario_id) {
        $this->db->select('planos_usuarios.id as id, planos.nome as nome, planos_usuarios.status as status, planos_usuarios.dt_validade as dt_validade');
        $this->db->from('planos_usuarios');
        $this->db->join('planos', 'planos.id = planos_usuarios.plano');
        $this->db->where('usuario', $usuario_id);
        $this->db->order_by('planos_usuarios.id', 'desc');
        $this->db->limit(1);
        $data = $this->db->get();
        return $data;
    }

    //verifica e valida login de usu�rio
    public function get_dados_login_adm($dados) {
        $this->imazoncursos->where('email = "' . $dados . '"')->or_where('login = "' . $dados . '"');
        $this->imazoncursos->from('facilitador'); 
        return $this->imazoncursos->get();
    }
    
    //verifica e valida login de usu�rio
    public function verifica_login_adm($dados) {
        $this->imazoncursos->where('email = "' . $dados['email'] . '" AND id_grupo = 1')->or_where('login = "' . $dados['email'] . '" AND id_grupo = 1');
        $this->imazoncursos->from('facilitador'); 
        return $this->imazoncursos->get();
    }

    //lista dados por email
    public function get_by_email($email) {
        $this->imazoncursos->where('email', $email);
        $this->imazoncursos->limit(1);
        $query = $this->imazoncursos->get('aluno');
        return $query;
    }
    
    //lista dados por email
    public function get_by_id_fic($email) {
        $this->imazoncursos->where('id_fic', $email);
        $this->imazoncursos->limit(1);
        $query = $this->imazoncursos->get('aluno');
        return $query;
    }

    //lista dados por email
    public function logar($email) {
        $this->imazoncursos->where('email', $email);
        $this->imazoncursos->limit(1);
        $query = $this->imazoncursos->get('aluno');
        return $query;
    }

    //novo
    public function validar_email($data) {

        //print_r($this->imazoncursos->where("email", $data)->or_where("id_fic", $data)->get("aluno")->result());
        //exit;
        if (count($this->imazoncursos->where("email", $data)->or_where("id_fic", $data)->get("aluno")->result()) == 1) {
            return true;
        } else if (count($this->imazon->get_where("aluno", array("email" => $data))->result()) == 1 && count($this->imazon10->get_where("aluno", array("email" => $data))->result()) == 1 && count($this->imazoncursos->get_where("aluno", array("email" => $data))->result()) == 0) {
            $dadosImazon = $this->imazon->get_where("aluno", array("email" => $data))->result();
            $datav = date('Y-m-d', strtotime("+7 days", strtotime(date("d-m-Y"))));
			
            $dados = array('id_fic' => $dadosImazon[0]->email,
                'nome' => strtoupper($dadosImazon[0]->nome),
                'apelido' => '',
                'email' => $dadosImazon[0]->email,
                'senha' => $dadosImazon[0]->senha,    
                'telefone' => $dadosImazon[0]->telefone,
                'endereco' => strtoupper($dadosImazon[0]->endereco),
                'numero' => $dadosImazon[0]->numero,
                'dt_val_ass' => $datav,
                'complemento' => strtoupper($dadosImazon[0]->complemento),
                'bairro' => strtoupper($dadosImazon[0]->bairro),
                'cidade' => strtoupper($dadosImazon[0]->cidade),
                'estado' => strtoupper($dadosImazon[0]->estado),
                'cep' => $dadosImazon[0]->cep,
                'codigo_alfa' => md5($dadosImazon[0]->email),
                'situacao_aluno' => $dadosImazon[0]->bloqueado,
                'data_cadastro' => date("Y-m-d h:m:s"),
                "situacao_aluno" => 1);
            $this->imazoncursos->insert('aluno', $dados);
            if (count($this->imazoncursos->get_where("aluno", array("email" => $data))->result()) == 1) {
                return true;
            }
        } else {
            return false;
        }
    }

    public function matricula_usuario($cedoc_doc_id_fk, $usuarios_id_fk, $carga_horaria, $hash) {

        //verifica se usu�rio est� cadastrado no curso
        $query1 = $this->db->where(array('cedoc_doc_id_fk' => $cedoc_doc_id_fk, 'email' => $usuarios_id_fk))->get('usuarios_cursos_matriculados')->result();

        //se usu�rio nunca cadastrou no curso
        if (count($query1) === 0) {
            $validade = strtotime("+ 90 days");
            $validade = date('Y-m-d h:m:s', $validade);
            $info['UCMDataConclusaoCertificado'] = $validade;
            $info['cedoc_doc_id_fk'] = $cedoc_doc_id_fk;
            $info['email'] = $usuarios_id_fk;
            $info['UCMData'] = date('Y-m-d H:i:s');
            $info['status'] = 0;
            $info['UCMCargaHorariaCertificado'] = $carga_horaria;
            $info['UCMCertificadoLiberado'] = $hash;
            if ($this->db->insert('usuarios_cursos_matriculados', $info)) {
                return true;
            } else {
                echo 'Erro de cadastro';
                return false;
            }
        } else {

            foreach ($query1 as $q) {

                $this->db->where('cedoc_doc_id_fk', $cedoc_doc_id_fk);
                $this->db->where('email', $usuarios_id_fk);
                $this->db->where_in('status', array('2'));
                $this->db->from('usuarios_cursos_matriculados');
                $query2 = $this->db->count_all_results();

                if ($query2 >= 1) {
                    //remove aulas j� feitas
                    $aulas = $this->db->get_where('cursos_aulas', array('cedoc_doc_id_fk' => $cedoc_doc_id_fk))->result();
                    foreach ($aulas as $a) {
                        $this->db->delete('cursos_aulas_realizadas', array('cursos_aulas_id_fk' => $a->cursos_aulas_id, 'email' => $usuarios_id_fk));
                    }
                }
            }

            //insere curso
            //cancela matr�culas anteriores
            $this->db->where('cedoc_doc_id_fk', $cedoc_doc_id_fk);
            $this->db->where('email', $usuarios_id_fk);
            $this->db->where_in('status', array('0'));

            $this->db->update('usuarios_cursos_matriculados', array('status' => 2));

            $validade = strtotime("+ 90 days");
            $validade = date('Y-m-d h:m:s', $validade);
            $info['UCMDataConclusaoCertificado'] = $validade;
            $info['UCMCargaHorariaCertificado'] = $carga_horaria;
            $info['cedoc_doc_id_fk'] = $cedoc_doc_id_fk;
            $info['email'] = $usuarios_id_fk;
            $info['UCMData'] = date('Y-m-d h:m:s');
            $info['status'] = 0;
            $info['UCMCertificadoLiberado'] = $hash;
            if ($this->db->insert('usuarios_cursos_matriculados', $info)) {
                return true;
            } else {
                return false;
            }
        }
    }

    //verifica e valida login de usu�rio
    public function verifica_login($dados) {

        if ($this->verifica_email_cadastrado($dados['email'])) {
            $this->imazoncursos->where('email', $dados['email']);
            $this->imazoncursos->where('senha', $dados['senha']);
            $this->imazoncursos->from('aluno');
            $this->imazoncursos->limit(1);
            $query = $this->imazoncursos->count_all_results();
            return $query;
        } else {
            if ($this->validar_email($dados['email'])) {
                $this->imazoncursos->where('email', $dados['email']);
                $this->imazoncursos->where('senha', $dados['senha']);
                $this->imazoncursos->from('aluno');
                $this->imazoncursos->limit(1);
                $query = $this->imazoncursos->count_all_results();
                return $query;
            } else {
                return false;
            }
        }
    }

    public function verifica_email_cadastrado($email) {
        if (count($this->imazoncursos->get_where("aluno", array("email" => $email))->result()) == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function rec_senha_email($email) {

        $e = $this->imazoncursos->get_where("aluno", array("email" => $email));
        $i = count($e);

        if ($i == 1) {
            return $e;
        } else {
            return false;
        }
    }

    public function add($d = null) {
        if (count($this->imazoncursos->get_where("aluno", array("email" => $d['email']))->result()) == 0) {
            if ($this->imazoncursos->insert('aluno', $d)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    //atualiza dados do usu�rio
    public function update($d = null) {
        $this->imazoncursos->where('id_fic', $_SESSION['imazon']['email']);
        if ($this->imazoncursos->update('aluno', $d)) {
            return true;
        } else {
            return false;
        }
    }

    //atualiza dados do usu�rio
    public function atualizaEmail($d = null) {
        $this->imazoncursos->where('id_fic', $_SESSION['imazon']['email']);
        if ($this->imazoncursos->update('aluno', $d)) {
            return true;
        } else {
            return false;
        }
    }

    //atualiza dados do usu�rio
    public function atualizaSenha($senha_atual, $senha_nova) {
        //echo $senha_atual .' - '. $senha_nova;

        $this->imazoncursos->where('id_fic', $_SESSION['imazon']['email']);
        $aluno = $this->imazoncursos->get('aluno')->result();
        if (count($aluno) == 1) {
            if ($aluno[0]->senha == trim($senha_atual)) {
                $dados = array("senha" => $senha_nova);
                $this->imazoncursos->where('id_fic', $_SESSION['imazon']['email']);
                if ($this->imazoncursos->update('aluno', $dados)) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    //atualiza dados do usu�rio
    public function update_by_id_aluno($d = null) {
        $this->imazoncursos->where('id_aluno', $d['id_aluno']);
        if ($this->imazoncursos->update('aluno', $d)) {
            return true;
        } else {
            return false;
        }
    }
    
     //atualiza dados do usu�rio
    public function update_by_id_fic($d = null, $id_fic) {
        $this->imazoncursos->where('id_fic', $id_fic);
        if ($this->imazoncursos->update('aluno', $d)) {
            return true;
        } else {
            return false;
        }
    }

    public function update_conta_aluno($d = null) {
        $this->imazoncursos->where('email', $d['id_fic']);
        if ($this->imazoncursos->update('aluno', $d)) {
            return true;
        } else {
            return false;
        }
    }

    public function verifica_dados_aluno() {

        $aluno = $this->imazoncursos->get_where('aluno', array('id_fic' => $_SESSION['email_aluno']), 1)->result();

        if (strlen($aluno[0]->ddd_telefone) < 2) {
            return false;
        }
        if (strlen($aluno[0]->telefone) < 8) {
            return false;
        }

        if (strlen($aluno[0]->endereco) < 6) {
            return false;
        }
        if (strlen($aluno[0]->numero) < 1) {
            return false;
        }
        if (strlen($aluno[0]->bairro) < 3) {
            return false;
        }
        if (strlen($aluno[0]->cidade) < 2) {
            return false;
        }
        if (strlen($aluno[0]->estado) < 2) {
            return false;
        }
        if (strlen($aluno[0]->cep) < 8) {
            return false;
        }
        if (strlen($aluno[0]->data_nasc) < 6) {
            return false;
        }
        return true;
    }

}
