<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
    

class produtos_model extends CI_Model{
   
    public function __construct() {
        parent::__construct();
        $this->imazoncursos = $this->load->database('imazoncursos', TRUE);
    }
   
    public function get_all_carga_horaria(){
        return $this->imazoncursos->not_like('nome', 'segunda')->get_where('produto', array('tipoprod'=>'mat'))->result();
        return $this->imazoncursos->get_where('produto', array('tipoprod'=>'mat'))->result();
    }
    
    public function get_by_id($id){
        return $this->imazoncursos->get_where('produto', array('id_produto'=>$id),1)->result();
    }
    
    public function get_by_nome_produto($tipoprod){
        return $this->imazoncursos->like('tipoprod', $tipoprod)->get('produto')->result();
    }
    
    public function get_all_tipo_certificado(){
        return $this->imazoncursos->get_where('produto', array('tipoprod'=>'doc'))->result();
    }
    
    public function get_all_tipo_certificado_ativo(){
        return $this->imazoncursos->get_where('produto', array('tipoprod'=>'doc', 'situacao'=>1))->result();
    }
    
    public function verifica_carga_horaria($id){
        return $this->imazoncursos->get_where('produto', array('id_produto'=>$id),1)->result();
    }
    
    public function get_carga_horaria($tempo){
        return $this->imazoncursos->get_where('produto', array('tempo'=>$tempo, 'tipoprod'=>'mat'),1)->result();
    }
    
    public function get_valor_minimo_carga_horaria(){
        return $this->imazoncursos->limit(1)->where('tipoprod', 'mat')->like('nome', 'carga')->order_by('valor asc')->get('produto')->result();
    }
    
}
