<?php

class Categoria_blog_model extends CI_Model {
    
    public function __construct() {
        parent::__construct();
        $this->imazoncursos = $this->load->database('imazoncursos', TRUE);
    }
    
    public function add($data){
        $this->imazoncursos->insert('categorias_blog', $data);
    }
    
    public function edit_by_id($data){
        $this->imazoncursos->where('id', $data['id']);
        return $this->imazoncursos->update('categorias_blog', $data); 
    }
    
    public function count_all($busca=null){
        if($busca!=null){
            $this->imazoncursos->like('nome', $busca);
        }
        
        $this->imazoncursos->from('categorias_blog');
        return $this->imazoncursos->count_all_results();

    }
    
    public function get_all($inicio = null, $limite = null, $busca = null){
        if($busca){
            $this->imazoncursos->like('nome', $busca);
        }
        if($inicio && $limite){
            $this->imazoncursos->limit($inicio, $limite);
        }
        return $this->imazoncursos->get('categorias_blog');
    }
    
    public function get_by_id($id){
        return $this->imazoncursos->get_where('categorias_blog', array('id'=>$id));
    }
    
    public function del($id){
        $this->imazoncursos->where('id', $id);
        return$this->imazoncursos->delete('categorias_blog'); 
    }
    
}
