<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Assinaturas_model extends CI_Model {
    
    public function __construct() {
        parent::__construct();
        $this->imazon = $this->load->database('imazon', TRUE);
        $this->imazon10 = $this->load->database('imazon10', TRUE);
        $this->imazoncursos = $this->load->database('imazoncursos', TRUE);
    }
    
    public function add($dados = null){
        $this->db->insert('planos_usuarios', $dados);
    }
    
  public function get_by_nome($dados, $inicio = null, $limite = null){
        $this->imazoncursos->from('aluno');
        $this->imazoncursos->like('nome', $dados['nome']);
        $this->imazoncursos->or_like('apelido', $dados['nome']);
        $this->imazoncursos->or_like('email', $dados['nome']);
        if($inicio !== null && $limite !== null){
            $this->imazoncursos->limit($limite, $inicio);
        }else{
            $this->imazoncursos->limit(12);
        }    
        return $this->imazoncursos->get();
  }
  
  public function count_all_by_nome($dados){
        $this->imazoncursos->from('aluno');
        $this->imazoncursos->like('nome', $dados['nome']);
        $this->imazoncursos->or_like('apelido', $dados['nome']);
        $this->imazoncursos->or_like('email', $dados['nome']);
        return $this->imazoncursos->count_all_results();   
  }
    
    public function count_all($busca = null){
        if($busca !== null){$this->db->like('usuario', $busca);}
        $this->db->from('planos_usuarios');
        $this->db->join('planos', 'planos.id = planos_usuarios.plano');
        return $this->db->count_all_results();
    }
    
    public function get_all2($inicio = null, $limite = null, $busca = null){
        $this->db->from('planos_usuarios');
        $this->db->join('planos', 'planos.id = planos_usuarios.plano');
        if($busca !== null){ $this->db->like('usuario', $busca);}
        if($inicio !== null && $limite !== null){$this->db->limit($limite, $inicio);} 
        return $this->db->get();
    }

    public function get_all($inicio =null, $limite =null, $busca = null){
        
        $query = 'SELECT a.email as email, p.nome as plano, a.nome as nome, valor, dt_validade, pu.status as statuss, pu.status_pagamento as pagamento, pu.assinatura as assinatura, pu.usuario as usuario FROM planos_usuarios '
                    . 'as pu INNER JOIN planos as p '
                    . 'ON p.id = pu.plano '
                    . 'INNER JOIN imazon.aluno as a '
                    . 'on a.email = pu.usuario';
        
        if($busca!==null){
            $query .= ' WHERE a.nome like "%'.$busca.'%" || a.email like "%'.$busca.'%"';
        }
     
        return  $this->db->query($query);
           
    }
    
    public function get_dados_assinatura($assinatura, $usuario){
        $data = $this->db->get_where("planos_usuarios", array('assinatura'=>$assinatura,'usuario'=>$usuario), 1)->result();
        
        if(count($data)==1){
            return $data;
        }else{
            return false;
        }
    }
    
    public function cancelar_assinatura($assinatura, $usuario){
        
        $data = $this->db->get_where("planos_usuarios", array('assinatura'=>$assinatura,'usuario'=>$usuario), 1)->result();
        
        if(count($data)==1){
            
            if((int)$data[0]->status == 0 || (int)$data[0]->status == 1){
                
                $this->db->where('assinatura', $assinatura);
                $this->db->where('usuario', $usuario);
                $this->db->update('planos_usuarios', array('status'=>2)); 
                
                return true;
            }else{
                return false;
            }
        
        }else{
            return false;
        }
    }
    
    public function verificar_assinatura($assinatura, $usuario){
        $data = $this->db->get_where("planos_usuarios", array('assinatura'=>$assinatura,'usuario'=>$usuario), 1)->result();
        
        if(count($data)==1){
            return true;
        }else{
            return false;
        }
    } 
    
    public function get_by_id($id = null){
        $query = $this->db->get_where('planos_usuarios', array('id' => $id));
        return $query;
    }
    
    public function delete($id = null){
        if($id !== null)
            
            $data = array(
               'status' => 0
            );

            $this->db->update('planos_usuarios', $data, array('id' => $id));
    }
    
    public function ativar($id = null){
        if($id !== null)
            
            $data = array(
               'status' => 1
            );

            $this->db->update('planos_usuarios', $data, array('id' => $id));
    }
    
    public function update_by_id($id = null, $dados = null){
       if($id !== null)
           $this->db->update('planos_usuarios', $dados, array('id' => $id));
    }
    
    
}
