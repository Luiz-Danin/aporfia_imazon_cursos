<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Aulas_model extends CI_Model{
    
    public function __construct() {
        parent::__construct();
    }

    public function get_arquivos_aula($cursos_aulas_id_fk){
        $this->db->from('cursos_aulas_arquivos');
        $this->db->where('cursos_aulas_id_fk',$cursos_aulas_id_fk);
        $this->db->join('cursos_aulas','cursos_aulas.cursos_aulas_id = cursos_aulas_arquivos.cursos_aulas_id_fk');
        $data = $this->db->get();
        return $data->result();
    }    
    
    public function get_imagens_aula($cursos_aulas_id_fk){
        $this->db->from('cursos_aulas_imagens');
        $this->db->where('cursos_aulas_id_fk',$cursos_aulas_id_fk);
        $this->db->join('cursos_aulas','cursos_aulas.cursos_aulas_id = cursos_aulas_imagens.cursos_aulas_id_fk');
        $data = $this->db->get();
        return $data->result();
    }
    
    public function verifica_aula($curso, $usuarios_id){
        $this->db->from('cursos_aulas_realizadas');
        $this->db->where('cursos_aulas_id_fk',$curso);	
        $this->db->where('email',$usuarios_id);
        $data = $this->db->get();
        return $data;
    }
    
    public function get_aulas_realizadas($aulas,$usuarios_id_fk){
        $als = '';
        $i     = 1;
        foreach($aulas as $a){
            $als[$i] = $a->cursos_aulas_id;
            $i++;
        }
        
        $this->db->from('cursos_aulas_realizadas');
        $this->db->where_in('cursos_aulas_id_fk', $als);
        $this->db->where('email',$usuarios_id_fk);
        return $this->db->get()->result();
        
    }
    
    public function adiciona_aula_realizada($cursos_aulas_id_fk,$usuarios_id_fk,$campo = null,$id = null){
		
        $this->db->from('cursos_aulas_realizadas');
        $this->db->where('cursos_aulas_id_fk',$cursos_aulas_id_fk);
        $this->db->where('email',$usuarios_id_fk);
       
        if ($this->db->count_all_results()==0){
				
                $this->db->from('cursos_aulas_realizadas');
                $this->db->where('cursos_aulas_id_fk',$cursos_aulas_id_fk);
                $this->db->where('email',$usuarios_id_fk);	
				
                if ($campo<>''){
                        $this->db->where($campo,$id);
                }else{
                        $this->db->where('cursos_aulas_arquivos_id_fk','0');
                        $this->db->where('cursos_aulas_videos_id_fk','0');
                        $this->db->where('cursos_aulas_exercicios_id_fk','0');
                        $this->db->where('cursos_aulas_imagens_id_fk','0');
                }	
				
                $this->db->delete('cursos_aulas_realizadas');
                $info['cursos_aulas_id_fk'] = $cursos_aulas_id_fk;
                $info['email'] = $usuarios_id_fk;
                $info['CARDataConcluida'] = date('Y-m-d H:i:s');
                
				if ($campo<>''){
                    $info[$campo] = $id;
                }
				
                $this->db->insert('cursos_aulas_realizadas',$info);
                return $this->db->insert_id();
        }
    }
    
    public function get_video_aula($cursos_aulas_id_fk,$cursos_aulas_videos_id){
            $this->db->from('cursos_aulas_videos');
            $this->db->where('cursos_aulas_id_fk',$cursos_aulas_id_fk);
            $this->db->where('cursos_aulas_videos_id',$cursos_aulas_videos_id);
            $data = $this->db->get();
            return $data->result();
    }
    
    public function get_exercicio_aula($cursos_aulas_id_fk){
        $this->db->from('cursos_aulas_exercicios');
        $this->db->where('cursos_aulas_id_fk',$cursos_aulas_id_fk);
        $data = $this->db->get();
        return $data->result();
    }
    
    public function get_arquivo_aula($cursos_aulas_id_fk,$cursos_aulas_arquivos_id){
            $this->db->from('cursos_aulas_arquivos');
            $this->db->where('cursos_aulas_id_fk',$cursos_aulas_id_fk);
            $this->db->where('cursos_aulas_arquivos_id',$cursos_aulas_arquivos_id);
            $data = $this->db->get();
            return $data->result();
    }
    
    public function get_imagem_aula($cursos_aulas_id_fk,$cursos_aulas_imagens_id){
        $this->db->from('cursos_aulas_imagens');
        $this->db->where('cursos_aulas_id_fk',$cursos_aulas_id_fk);
        $this->db->where('cursos_aulas_imagens_id',$cursos_aulas_imagens_id);
        $data = $this->db->get();
        return $data->result();
    }
    
    public function get_ultima_aula_curso($cursos_id,$usuarios_id){
            $this->db->from('cursos');
            $this->db->where('cursos_id',$cursos_id);
            $this->db->join('cursos_aulas','cursos.cedoc_doc_id_fk = cursos_aulas.cedoc_doc_id_fk');
            $this->db->join('cursos_aulas_realizadas','cursos_aulas.cursos_aulas_id = cursos_aulas_realizadas.cursos_aulas_id_fk');
            $this->db->where('cursos_aulas_realizadas.email',$usuarios_id);
            $this->db->order_by('CARDataConcluida','desc');
            $this->db->limit(1);
            $data = $this->db->get();
            return ($data->result());
    }
    
    public function curso_get_aula($cursos_aulas_id, $limite = null, $inicio = null){
        $this->db->select('*');
        $this->db->select('cursos_aulas_id as cursos_aulas_id_fk');
        $this->db->from('cursos_aulas');
        $this->db->where('cursos_aulas.cursos_aulas_id',$cursos_aulas_id);
        $data = $this->db->get();
        
        if($inicio !== null)
            $this->db->limit($limite, $inicio);
        
        return $data->result();
    }
    
    public function get_videos_aula($cursos_aulas_id_fk){
        $this->db->from('cursos_aulas_videos');
        $this->db->where('cursos_aulas_id_fk',$cursos_aulas_id_fk);
        $this->db->join('cursos_aulas','cursos_aulas.cursos_aulas_id = cursos_aulas_videos.cursos_aulas_id_fk');
        $data = $this->db->get();
        return $data->result();
    }
    
    public function get_exercicios_aula($cursos_aulas_id_fk){
        $this->db->from('cursos_aulas_exercicios');
        $this->db->where('cursos_aulas_id_fk',$cursos_aulas_id_fk);
        $data = $this->db->get();
        return $data->result();
    }
    
    public function add_aula_curso($data = null){
        $this->db->order_by("CAOrdem", " DESC "); 
        $this->db->limit(1); 
        $aula  = $this->db->get_where('cursos_aulas', array('cedoc_doc_id_fk'=>$data))->result();
        $ordem = $aula[0]->CAOrdem+1;
        $d     = array('cedoc_doc_id_fk'=>$data, 'CAOrdem'=>$ordem, 'CADescricao'=>'', 'CATitulo'=>'');
        
        if($this->db->insert('cursos_aulas', $d)){
            return true;
        }else{
            return false;
        }
        
        
    }
    
    public function ultima_aula_criada($data = null){
        $this->db->order_by("CAOrdem", " DESC "); 
        $this->db->limit(1); 
        return $this->db->get_where('cursos_aulas', array('cedoc_doc_id_fk'=>$data));
    }
    
    public function delete_aula($id = null){
        if($this->db->delete('cursos_aulas', array('cursos_aulas_id' => $id))){
            return true;
        }else{
            return false;
        } 
    }
    
}
