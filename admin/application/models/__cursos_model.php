<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class cursos_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->imazoncursos = $this->load->database('imazoncursos', TRUE);
    }
    
    public function curso_previa($cedoc_doc_id){
            $this->db->select('*');
            $this->db->from('cursos');
            $this->db->where('cedoc_doc_id_fk',$cedoc_doc_id);
            $this->db->join('cedoc_doc', 'cedoc_doc.cedoc_doc_id = cursos.cedoc_doc_id_fk');
            $this->db->join('cursos_cat', 'cursos.cursos_cat_id_fk = cursos_cat.cursos_cat_id');
            $data = $this->db->get(); 
            return $data->result();
    }
    
    public function curso_get_aula($cursos_aulas_id, $limite = null, $inicio = null){
        $this->db->select('*');
        $this->db->select('cursos_aulas_id as cursos_aulas_id_fk');
        $this->db->from('cursos_aulas');
        $this->db->where('cursos_aulas.cursos_aulas_id',$cursos_aulas_id);
        $data = $this->db->get();
        
        if($inicio !== null)
            $this->db->limit($limite, $inicio);
        
        return $data->result();
    }
    
    public function get_total_itens_curso($aulas,$cedoc_doc_id_fk){
          
            $total = 0;
            $total += count($aulas);

            foreach ($aulas as $aula){
                    $this->db->from('cursos_aulas_arquivos');
                    $this->db->where('cursos_aulas_arquivos.cursos_aulas_id_fk',$aula->cursos_aulas_id);
                    $data = $this->db->get();
                    $total += count($data->result());

                    $this->db->from('cursos_aulas_videos');
                    $this->db->where('cursos_aulas_videos.cursos_aulas_id_fk',$aula->cursos_aulas_id);
                    $data = $this->db->get();
                    $total += count($data->result());

                    $this->db->from('cursos_aulas_exercicios');
                    $this->db->where('cursos_aulas_exercicios.cursos_aulas_id_fk',$aula->cursos_aulas_id);
                    $this->db->group_by('cursos_aulas_id_fk');
                    $data = $this->db->get();
                    $total += count($data->result());

                    $this->db->from('cursos_aulas_imagens');
                    $this->db->where('cursos_aulas_imagens.cursos_aulas_id_fk',$aula->cursos_aulas_id);
                    $data = $this->db->get();
                    $total += count($data->result());
            }
            
            return $total;
    }
    
    public function get_curso_capa($cedoc_doc_id_fk){
        $this->db->from('cedoc_doc_imagens');
        $this->db->where('cedoc_doc_imagens.cedoc_doc_id_fk',$cedoc_doc_id_fk);
        $this->db->limit(1);
        $data = $this->db->get(); 
        return $data->result();
    }
    
    public function get_all_aulas_curso($cedoc_doc_id_fk, $inicio = null, $limite = null){
            $this->db->select('*');
            $this->db->from('cursos_aulas');
            $this->db->where('cursos_aulas.cedoc_doc_id_fk',$cedoc_doc_id_fk);
            $this->db->order_by('CAOrdem','esc');
            if($inicio !== null && $limite !== null)
                $this->db->limit($limite, $inicio);
            $data = $this->db->get();
            return $data->result();
            
    }
    
    public function get_all_aulas_curso_by_aluno($cedoc_doc_id_fk, $inicio = null, $limite = null){
            $this->db->select('*');
            $this->db->from('cursos_aulas as ca');
            $this->db->join('cursos_aulas_realizadas', 'cursos_aulas_id_fk = cursos_aulas_id');
            $this->db->where('cedoc_doc_id_fk',$cedoc_doc_id_fk);
            $this->db->order_by('CAOrdem','esc');
            if($inicio !== null && $limite !== null)
                $this->db->limit($limite, $inicio);
            $data = $this->db->get();
            return $data->result();
            
    }
    
    public function altera_status_curso_matriculado($usuarios_id, $curso, $status, $param = null){
        
	$this->db->where('email', $usuarios_id);
		
        if($param != null){
            $this->db->where('status', 0);
        }
		
        $this->db->where('usuarios_cursos_matriculados_id', $curso);
        $this->db->update('usuarios_cursos_matriculados', array('status'=>$status, 'UCMDataConclusaoCertificado'=>date('Y-m-d h:m:s')));
    }
    
    public function cancelar_curso_matriculado($usuarios_id, $usuarios_cursos_matriculados_id){
        $this->db->where('email', $usuarios_id);
	$this->db->where('usuarios_cursos_matriculados_id', $usuarios_cursos_matriculados_id);
        $this->db->where('status', 0);
        $c = $this->db->get('usuarios_cursos_matriculados')->result();
        if(count($c)==1){
            $this->db->where('email', $usuarios_id);
            $this->db->where('usuarios_cursos_matriculados_id', $usuarios_cursos_matriculados_id);
            $this->db->where('status', 0);
            return $this->db->update('usuarios_cursos_matriculados', array('status'=>2));
        }else{
            return false;
        } 
    }
    
    public function concluir_curso($usuarios_id, $curso){
	$this->db->where('email', $usuarios_id);
        $this->db->where('usuarios_cursos_matriculados_id', $curso);
        $this->db->update('usuarios_cursos_matriculados', array('status'=>1, 'ultimo_dia'=>date('Y-m-d h:m:s')));
    }
    
    public function verifica_data_finalizacao_curso($usuarios_id, $curso){
        $this->db->from("usuarios_cursos_matriculados");
        $this->db->where("email", $usuarios_id);
        $this->db->where("cedoc_doc_id_fk", $curso);
        return $this->db->get();
    }
    
    public function verifica_matricula_aluno($matricula, $usuarios_id_fk) {
        $this->db->where('email', $usuarios_id_fk);
        $this->db->where('usuarios_cursos_matriculados_id', $matricula);
        $this->db->limit(1);
        return $this->db->get('usuarios_cursos_matriculados');
    }
    
    public function curso_matriculado_aluno($cedoc_doc_id_fk, $usuarios_id_fk) {
       
        $this->db->from('usuarios_cursos_matriculados');
        $this->db->where('usuarios_cursos_matriculados.email', $usuarios_id_fk);
        $this->db->where('usuarios_cursos_matriculados.cedoc_doc_id_fk', $cedoc_doc_id_fk);
        //$this->db->or_where_in('usuarios_cursos_matriculados.status', array('0','1'));
        $this->db->join('cedoc_doc', 'usuarios_cursos_matriculados.cedoc_doc_id_fk = cedoc_doc.cedoc_doc_id');
        $this->db->where('status',0);
        $this->db->limit(1);
        return $this->db->get();
    }

    public function get_exercicios_aula($cursos_aulas_id_fk) {
        return $this->db->from('cursos_aulas_exercicios')->where('cursos_aulas_id_fk', $cursos_aulas_id_fk)->get()->result();
    }

    public function curso_matriculado_certif($usuarios_cursos_matriculados_id, $usuarios_id_fk) {
        $this->db->from('usuarios_cursos_matriculados');
        $this->db->where('usuarios_cursos_matriculados.email', $usuarios_id_fk);
        $this->db->where('usuarios_cursos_matriculados.usuarios_cursos_matriculados_id', $usuarios_cursos_matriculados_id);
        $this->db->where('usuarios_cursos_matriculados.status', '1');
        $this->db->join('cedoc_doc', 'usuarios_cursos_matriculados.cedoc_doc_id_fk = cedoc_doc.cedoc_doc_id');
        $this->db->limit(1);
        $data = $this->db->get();
        return $data;
    }

    //verifica se aluno est� matr�culado no curso
    public function verifica_matricula_aluno_curso($curso, $aluno) {
        $this->db->from('usuarios_cursos_matriculados');
        $this->db->where('usuarios_cursos_matriculados.email', $aluno);
        $this->db->where('usuarios_cursos_matriculados.usuarios_cursos_matriculados_id', $curso);
        $status = array('2', '3', '4');
        $this->db->where_not_in('status', $status);
        return $this->db->count_all_results();
    }
    
    //verifica se aluno est� matr�culado no curso
    public function gera_declaracao_matricula($curso, $aluno) {
        $this->db->from('usuarios_cursos_matriculados');
        $this->db->where('usuarios_cursos_matriculados.email', $aluno);
        $this->db->where('usuarios_cursos_matriculados.usuarios_cursos_matriculados_id', $curso);
        $this->db->join('cedoc_doc', 'usuarios_cursos_matriculados.cedoc_doc_id_fk = cedoc_doc.cedoc_doc_id');
        return $this->db->get();
    }
    
    //verifica se aluno est� matr�culado no curso
    public function verifica_ultima_matricula_aluno_curso($curso, $aluno) {
        //$this->db->from('usuarios_cursos_matriculados')->where('usuarios_cursos_matriculados.email', $aluno)->where('usuarios_cursos_matriculados.cedoc_doc_id_fk', $curso)->order_by("usuarios_cursos_matriculados_id", "desc")->limit(1);
        $this->db->from('usuarios_cursos_matriculados')
                ->where('usuarios_cursos_matriculados.email', $aluno)
                ->where('usuarios_cursos_matriculados.cedoc_doc_id_fk', $curso)
                ->where('usuarios_cursos_matriculados.status', 0)
                ->order_by("usuarios_cursos_matriculados_id", "desc")
                ->limit(1);
        return $this->db->get();
    }

    public function get_curso($curso) {
        return $this->db->get_where('cedoc_doc', array('cedoc_doc_id' => $curso));
    }

    public function count_all($busca = null) {

        if ($busca !== null) {
            $this->db->like('nome', $busca);
        }
        $this->db->from('cedoc_doc');
        $this->db->join('cursos', 'cursos.cedoc_doc_id_fk = cedoc_doc.cedoc_doc_id');
        $this->db->join('cursos_cat', 'cursos.cursos_cat_id_fk = cursos_cat.cursos_cat_id');
        return $this->db->count_all_results();
    }

    public function get_all($inicio = null, $limite = null, $busca = null) {

        $this->db->from('cedoc_doc');
        $this->db->join('cursos', 'cursos.cedoc_doc_id_fk = cedoc_doc.cedoc_doc_id');
        $this->db->join('cursos_cat', 'cursos.cursos_cat_id_fk = cursos_cat.cursos_cat_id');
        if ($busca !== null) {
            $this->db->like('nome', $busca);
        }
        if ($inicio !== null && $limite !== null) {
            $this->db->limit($limite, $inicio);
        }
        return $this->db->get();
    }

    public function mais_visitados($inicio = null, $limite = null) {

        $this->db->from('cedoc_doc');
        $this->db->join('cursos', 'cursos.cedoc_doc_id_fk = cedoc_doc.cedoc_doc_id');
        $this->db->join('cursos_cat', 'cursos.cursos_cat_id_fk = cursos_cat.cursos_cat_id');
        $this->db->order_by('CVisitas', 'DESC');
        if ($inicio !== null && $limite !== null) {
            $this->db->limit($limite, $inicio);
        }
        return $this->db->get();
    }

    public function mais_vendidos($inicio = null, $limite = null) {

        $this->db->from('cedoc_doc');
        $this->db->join('usuarios_cursos_matriculados', 'usuarios_cursos_matriculados.cedoc_doc_id_fk = cedoc_doc.cedoc_doc_id');
        $this->db->group_by('cedoc_doc_id_fk');
        if ($inicio !== null && $limite !== null) {
            $this->db->limit($limite, $inicio);
        }
        return $this->db->get();
    }

    public function nao_procurados($inicio = null, $limite = null) {

        $this->db->from('cedoc_doc');
        $this->db->join('cursos', 'cursos.cedoc_doc_id_fk = cedoc_doc.cedoc_doc_id');
        $this->db->join('usuarios_cursos_matriculados', 'usuarios_cursos_matriculados.cedoc_doc_id_fk = cedoc_doc.cedoc_doc_id');
        $this->db->where('CVisitas', 0);
        if ($inicio !== null && $limite !== null) {
            $this->db->limit($limite, $inicio);
        }
        return $this->db->get();
    }

    public function get_by_id($id) {

        $this->db->from('cedoc_doc');
        $this->db->join('cursos', 'cursos.cedoc_doc_id_fk = cedoc_doc.cedoc_doc_id');
        $this->db->join('cursos_cat', 'cursos.cursos_cat_id_fk = cursos_cat.cursos_cat_id');
        $this->db->where('cedoc_doc.cedoc_doc_id', $id);
        $this->db->limit(1);
        return $this->db->get();
    }

    public function get_cursos_by_aluno($email) {
        $this->db->select('usuarios_cursos_matriculados_id, nome, status, UCMData, UCMDataConclusaoCertificado, ultimo_dia, UCMCargaHorariaCertificado');
        $this->db->from('cedoc_doc');
        $this->db->join('cursos', 'cursos.cedoc_doc_id_fk = cedoc_doc.cedoc_doc_id');
        $this->db->join('usuarios_cursos_matriculados', 'usuarios_cursos_matriculados.cedoc_doc_id_fk = cursos.cedoc_doc_id_fk');
        $this->db->where('usuarios_cursos_matriculados.email', $email);
        $this->db->order_by('usuarios_cursos_matriculados.UCMData', 'desc');
        return $this->db->get();
    }
    
    public function get_cursos_by_aluno_imazon($email) {
        $this->imazoncursos->select("historico.nota, historico.id_historico, curso.nome, historico.primeiro_dia, historico.data_aprovacao, historico.carga_horaria, historico.situacao, historico.data_aprovacao as ultimo_dia");
        $this->imazoncursos->join("curso", "curso.id_curso = historico.id_curso");
        $this->imazoncursos->where("id_aluno", $email);
        $this->imazoncursos->order_by("historico.id_historico desc");
        return $this->imazoncursos->get("historico");
    }
    
    public function get_aula_by_id($id) {
        return $this->db->get_where('cursos_aulas', array('cursos_aulas_id' => $id));
    }

    public function get_aulas($id) {
        return $this->db->get_where('cursos_aulas', array('cedoc_doc_id_fk' => $id));
    }

    public function get_videos($id) {
        return $this->db->get_where('cedoc_doc_videos', array('cedoc_doc_id_fk' => $id));
    }

    public function get_audios($id) {
        return $this->db->get_where('cedoc_doc_audios', array('cedoc_doc_id_fk' => $id));
    }

    public function get_imagens($id) {
        return $this->db->get_where('cedoc_doc_imagens', array('cedoc_doc_id_fk' => $id));
    }

    public function get_arquivos($id) {
        return $this->db->get_where('cedoc_doc_arquivos', array('cedoc_doc_id_fk' => $id));
    }

    public function get_status($id, $operacao) {
        $this->db->select($operacao);
        $this->db->where('cedoc_doc_id', $id);
        return $this->db->get('cedoc_doc');
    }

    public function get_status2($id, $operacao) {
        $this->db->select($operacao);
        $this->db->where('cedoc_doc_id_fk', $id);
        return $this->db->get('cursos');
    }

    public function get_by_proximo_curso($id) {
        $this->db->from('cursos');
        $this->db->join('cedoc_doc', 'cedoc_doc.cedoc_doc_id = cursos.cedoc_doc_id_fk');
        $this->db->where('cedoc_doc_id_fk >', $id);
        $this->db->order_by('cursos_id', 'asc');
        $this->db->limit(1);
        return $this->db->get();
    }

    public function get_by_curso_anterior($id) {
        $this->db->from('cursos');
        $this->db->join('cedoc_doc', 'cedoc_doc.cedoc_doc_id = cursos.cedoc_doc_id_fk');
        $this->db->where('cedoc_doc_id_fk <', $id);
        $this->db->order_by('cursos_id', 'desc');
        $this->db->limit(1);
        return $this->db->get();
    }

    public function edit_by_id($data) {
        $this->db->where('cedoc_doc_id', $data['cedoc_doc_id']);
        return $this->db->update('cedoc_doc', $data);
    }

    public function edit_aula_by_id($data) {
        $this->db->where('cursos_aulas_id', $data['cursos_aulas_id']);
        return $this->db->update('cursos_aulas', $data);
    }

    public function edit_by_id2($data) {
        $this->db->where('cedoc_doc_id_fk', $data['cedoc_doc_id_fk']);
        return $this->db->update('cursos', $data);
    }

    public function altera_status_publicado($data) {
        $this->db->where('cedoc_doc_id_fk', $data['cedoc_doc_id_fk']);
        if ($this->db->update('cursos', $data)) {
            return true;
        } else {
            return false;
        }
    }

    public function del($id) {
        $this->db->where('cedoc_doc_id_fk', $id);
        return$this->db->delete('cursos_cat');
    }
    
    /*CURSO DO IMAZON*/

    public function add_curso_imazon($curso){
        return $this->imazoncursos->insert("curso", $curso);
    }
    
    public function add_curso_aluno_imazon($curso){
        return $this->imazoncursos->insert("historico", $curso);
    }
    
    public function add_questao_curso_imazon($questao){
        return $this->imazoncursos->insert("questao", $questao);
    }
    
    public function count_all_cursos_imazon_adm($nome=null){
       if($nome!==null){
            $this->imazoncursos->like("nome", $nome)->or_like("descricao", $nome)->or_like("questionario", $nome);
       }
       return $this->imazoncursos->from("curso")->count_all_results(); 
    }
    
    public function get_all_cursos_imazon_adm($nome = null, $inicio = null, $limite = null){
       if($nome!==null){
            $this->imazoncursos->like("nome", $nome)->or_like("descricao", $nome)->or_like("questionario", $nome);
       }
       return $this->imazoncursos->select("id_curso, nome, carga_horaria, status, imagem")->from("curso")->limit($limite, $inicio)->order_by("nome  asc")->get(); 
    }
    
    public function atualiza_dados_curso_imazon($curso, $dados){
        return $this->imazoncursos->where('id_curso', $curso)->update('curso', $dados);
    }
    
    public function atualiza_dados_questao_curso_imazon($questao, $dados){
        return $this->imazoncursos->where('id_questao', $questao)->update('questao', $dados);
    }
    
    public function remove_questao_curso_imazon($questao){
        return $this->imazoncursos->delete('questao', array('id_questao' => $questao));
    }
    
    public function get_curso_imazon_by_id($curso){
       return $this->imazoncursos->get_where("curso", array("id_curso"=>$curso), 1); 
    }
    
    public function get_autocomplete_curso_imazon_by_nome($nome){
       return $this->imazoncursos->select("nome, id_curso")->like("nome", $nome)->limit(15)->get("curso"); 
    }
    
    public function get_questoes_curso_imazon_by_id($curso){
        return $this->imazoncursos->get_where("questao", array("id_curso"=>$curso)); 
    }
    
    public function get_ultimo_curso_imazon(){
       return $this->imazoncursos->order_by("id_curso desc")->limit(1)->get("curso"); 
    }
    
}
