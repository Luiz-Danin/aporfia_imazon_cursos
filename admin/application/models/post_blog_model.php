<?php

class post_blog_model extends CI_Model {
    
    public function __construct() {
        parent::__construct();
        $this->imazoncursos = $this->load->database('imazoncursos', TRUE);
    }
    
    public function add($data){
        $this->imazoncursos->insert('post_blog', $data);
    }
    
    public function edit_by_id($data){
        $this->imazoncursos->where('id', $data['id']);
        return $this->imazoncursos->update('post_blog', $data); 
    }
    
    public function count_all($busca=null){
        if($busca!=null){
            $this->imazoncursos->like('titulo', $busca);
        }
        
        $this->imazoncursos->from('post_blog');
        return $this->imazoncursos->count_all_results();

    }
    
    public function get_all($inicio = null, $limite = null, $busca = null){
        if($busca){
            $this->imazoncursos->like('titulo', $busca);
        }
        if($inicio && $limite){
            $this->imazoncursos->limit($inicio, $limite);
        }
        return $this->imazoncursos->get('post_blog');
    }
    
    public function get_by_id($id){
        return $this->imazoncursos->get_where('post_blog', array('id'=>$id));
    }
    
    public function del($id){
        $this->imazoncursos->where('id', $id);
        return$this->imazoncursos->delete('post_blog'); 
    }
    
}
