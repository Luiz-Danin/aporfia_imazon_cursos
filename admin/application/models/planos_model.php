<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class planos_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->imazoncursos = $this->load->database('imazoncursos', TRUE);
    }

    public function add($dados = null) {
        $this->db->insert('planos', $dados);
    }

    public function get_plano($id) {
        return $this->db->get_where('planos', array('id' => $id, 'status' => 1),1);
    }

    public function get_all() {
        return $this->db->get('planos');
    }

    public function get_by_id($id = null) {
        $query = $this->db->get_where('planos', array('id' => $id));
        return $query;
    }

    public function insert_log_plano_usuario($data) {

        $data = $this->db->insert('log_planos_usuarios', $data);
        return $data;
    }

    public function delete($id = null) {
        if ($id !== null)
            $data = array('status' => 0);
        $this->db->update('planos', $data, array('id' => $id));
    }

    public function get_plano_usuario($usuario_id) {
        $this->db->from('planos_usuarios');
        $this->db->where('usuario', $usuario_id);
        $this->db->where('status', 1);
        $data = $this->db->get();
        return $data;
    }

    public function verifica_validade_plano($usuarios_id) {
        $data       = $this->imazoncursos->from('aluno')->where('id_fic', $usuarios_id)->limit(1)->get()->result();
        //$data       = $this->imazoncursos->from('aluno')->where('email', $usuarios_id)->limit(1)->get()->result();
        if(count($data)==1){
            $vencimento = date("Y-m-d h:m:s", strtotime($data[0]->dt_val_ass));
            $atual      = date('Y-m-d h:m:s');
            if($vencimento >= $atual){
              return true; 
            }else{
              return false;
            }
        }else{
            return false;
        }
        
    }
    
    public function get_validade_plano($usuarios_id) {
        //return $this->imazoncursos->from('aluno')->where('email', $usuarios_id)->limit(1)->get()->result();
        return $this->imazoncursos->from('aluno')->where('id_fic', $usuarios_id)->limit(1)->get()->result();
    }

    public function ativar($id = null) {
        if ($id !== null)
            $data = array(
                'status' => 1
            );

        $this->db->update('planos', $data, array('id' => $id));
    }

    public function update_by_id($id = null, $dados = null) {
        return $this->db->update('planos', $dados, array('codigo' => $dados['codigo']));
    }

}
