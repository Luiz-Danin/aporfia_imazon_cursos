<?php

class certificados_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->imazon       = $this->load->database('imazon', TRUE);
        $this->imazoncursos = $this->load->database('imazoncursos', TRUE);
    }

    public function add_certificado_impresso($certificado){
        return $this->db->insert('certificados_impressos', $certificado);
    }
    
    //verifica se usu�rio j� imprimiu certificado(�rea do aluno)
    public function verifica_imprimiu_ceritificado($aulas, $usuarios_id) {
        foreach ($aulas as $aula) {
            $this->db->from('cursos_aulas_realizadas');
            $this->db->where('cursos_aulas_id_fk', $aula->cursos_aulas_id);
            $this->db->where('email', $usuarios_id);
            $this->db->where('CARHashingCertificado <>', '');
            if ($this->db->count_all_results() > 0) {
                return true;
            }
        }
    }
    
    public function gera_certificado_by_historico($historico, $cedoc_doc_id_fk){
        return $this->db->get_where('usuarios_cursos_matriculados', array('usuarios_cursos_matriculados_id'=>$historico), 1) ;
    }
    
    public function count_all_inativo_imazon($busca = null) {
        if ($busca !== null) {
           return $this->imazoncursos
                   ->select('certificado.id_certificado as id_certificado, certificado.situacao as situacao, aluno.nome as nomeAluno, historico.id_historico as id_historico, certificado.endereco as endereco, pedido.data_compra as data_emissao, curso.nome as nomeCurso, certificado.id_tipo as id_tipo, aluno.id_fic as id_fic, historico.id_curso as id_curso')
                   ->from('certificado')
                   ->join('historico', 'historico.id_historico = certificado.id_historico')
                   ->join('aluno', 'historico.id_aluno = aluno.id_fic')
                   ->join('pedido', 'certificado.transacao = pedido.transacao')
                   ->join('curso', 'curso.id_curso = historico.id_curso')
                   ->where('certificado.situacao', 0)
                   ->like('aluno.nome', $busca)
                   ->or_like('aluno.email', $busca)
                   ->count_all_results();
        } else {
           return $this->imazoncursos
                   ->select('certificado.id_certificado as id_certificado, certificado.situacao as situacao, aluno.nome as nomeAluno, historico.id_historico as id_historico, certificado.endereco as endereco, pedido.data_compra as data_emissao, curso.nome as nomeCurso, certificado.id_tipo as id_tipo, aluno.id_fic as id_fic, historico.id_curso as id_curso')
                   ->from('certificado')
                   ->join('historico', 'historico.id_historico = certificado.id_historico')
                   ->join('aluno', 'historico.id_aluno = aluno.id_fic')
                   ->join('pedido', 'certificado.transacao = pedido.transacao')
                   ->join('curso', 'curso.id_curso = historico.id_curso')
                   ->where('certificado.situacao', 0)
                   ->count_all_results();
        }
    }
    
    
    public function count_all_inativo($busca = null) {

        if ($busca !== null) {
            $al = $this->imazoncursos->like('nome', $busca)->get('aluno')->result();
            $i = 0;
            if(count($al)>=1){
                
                foreach($al as $a){
                   $cert = $this->db->join('cedoc_doc', 'cedoc_doc.cedoc_doc_id = certificados_impressos.curso')->get('certificados_impressos', array('pagamento'=>3, 'status'=>0, 'email'=>$a->email))->result();
                   if(count($cert)>=1){
                       foreach ($cert as $c) {
                           $i++;
                       }
                   }
                }
                return  $i;
            }else{
                return false;
            }

        } else {

            $cert = $this->db->get('certificados_impressos', array('pagamento'=>3, 'status'=>0));
            return $cert;
        }
    }

    public function count_all_pendentes($busca = null) {

         if ($busca !== null) {
            $al = $this->imazoncursos->like('nome', $busca)->get('aluno')->result();
            $i = 0;
            if(count($al)>=1){
                
                foreach($al as $a){
                   $cert = $this->db->join('cedoc_doc', 'cedoc_doc.cedoc_doc_id = certificados_impressos.curso')->get('certificados_impressos', array('status'=>0, 'pagamento'=>0, 'email'=>$a->email))->result();
                   if(count($cert)>=1){
                       foreach ($cert as $c) {
                           $i++;
                       }
                   }
                }
                return  $i;
            }else{
                return false;
            }

        } else {

            $cert = $this->db->get('certificados_impressos', array('status'=>0, 'pagamento'=>0));
            return count($cert);
        }
    }

    public function count_all_ativo($busca = null) {

        if ($busca !== null) {
            $al = $this->imazoncursos->like('nome', $busca)->get('aluno')->result();
            $i = 0;
            if(count($al)>=1){
                
                foreach($al as $a){
                   $cert = $this->db->join('cedoc_doc', 'cedoc_doc.cedoc_doc_id = certificados_impressos.curso')->get('certificados_impressos', array('pagamento'=>3, 'status'=>1, 'email'=>$a->email))->result();
                   if(count($cert)>=1){
                       foreach ($cert as $c) {
                           $i++;
                       }
                   }
                }
                return  $i;
            }else{
                return false;
            }

        } else {

            $cert = $this->db->get('certificados_impressos', array('pagamento'=>3, 'status'=>1));
            return count($cert);
        }
    }

    public function get_all_ativo($inicio = null, $limite = null, $busca = null) {
         if ($busca !== null) {
            $al   = $this->imazoncursos->like('nome', $busca)->or_like('email', $busca)->like('id_fic', $busca)->get('aluno')->result();
            $i    = 1;
            $ret  = null;
            if(count($al)>=1){
                foreach($al as $a){
                   $this->db->join('cedoc_doc', 'cedoc_doc.cedoc_doc_id = certificados_impressos.curso')->where(array('certificados_impressos.pagamento'=>3, 'certificados_impressos.status'=>1, 'email'=>$a->email));
                   /*if($inicio !== null &&  $limite!=null){$this->db->limit($inicio, $limite);}*/
                   $cert = $this->db->get('certificados_impressos')->result();
                   //$cert = $this->db->join('cedoc_doc', 'cedoc_doc.cedoc_doc_id = certificados_impressos.curso')->get('certificados_impressos', array('pagamento'=>3, 'status'=>1, 'email'=>$a->email))->result();
                   if(count($cert)>=1){
                       foreach ($cert as $c) {
                           @$ret[$i] = array('endereco'=>$a->endereco,'numero'=>$a->numero, 'bairro'=>$a->bairro, 'cidade'=>$a->cidade, 'estado'=>$a->estado, 'aluno'=>$a->nome, 'email'=>$a->email,'id_curso'=>$c->id, 'curso'=>$c->nome, 'pedido'=>$c->pedido, 'cadastro'=>$c->dt_cadastro, 'status'=>$c->status, 'id'=>$c->id);
                           $i++;
                       }
                   }
                }
            }    
                return  $ret;
        } else {
           $this->db->join('cedoc_doc', 'cedoc_doc.cedoc_doc_id = certificados_impressos.curso')->where(array('certificados_impressos.pagamento'=>3, 'certificados_impressos.status'=>1));
           /*if($inicio !== null &&  $limite!=null){$this->db->limit($inicio, $limite);}*/
           $cert = $this->db->get('certificados_impressos')->result();
           if($inicio !== null &&  $limite!=null){
               $this->db->limit($inicio, $limite);
           }
           $ret  = null;
           $i    = 0;
           foreach ($cert as $c){
               $al = $this->imazoncursos->where('email', $c->email)->get('aluno')->result();
               if(count($al)==1){
                   @$ret[$i] = array('endereco'=>$al[0]->endereco,'numero'=>$al[0]->numero, 'bairro'=>$al[0]->bairro, 'cidade'=>$al[0]->cidade, 'estado'=>$al[0]->estado, $c->estado, 'aluno'=>$al[0]->nome, 'email'=>$al[0]->email,'id_curso'=>$c->id, 'curso'=>$c->nome, 'pedido'=>$c->pedido, 'cadastro'=>$c->dt_cadastro, 'status'=>$c->status, 'id'=>$c->id); 
               }
               $i++;
           }
           return $ret;
        }
    }

    public function get_all_pendentes($inicio = null, $limite = null, $busca = null) {

        if ($busca !== null) {
            
            $al   = $this->imazoncursos->like('nome', $busca)->get('aluno')->result();
            $i    = 1;
            $ret  = null;
            if(count($al)>=1){
                
                foreach($al as $a){
                   $cert = $this->db->join('cedoc_doc', 'cedoc_doc.cedoc_doc_id = certificados_impressos.curso')->get('certificados_impressos', array('pagamento'=>0, 'status'=>0, 'email'=>$a->email))->result();
                   if(count($cert)>=1){
                       foreach ($cert as $c) {
                           @$ret[$i] = array('endereco'=>$a->endereco,'numero'=>$a->numero, 'bairro'=>$a->bairro, 'cidade'=>$a->cidade, 'estado'=>$a->estado, 'aluno'=>$a->nome, 'email'=>$a->email,'id_curso'=>$c->id, 'curso'=>$c->nome, 'pedido'=>$c->pedido, 'cadastro'=>$c->dt_cadastro, 'status'=>$c->status, 'id'=>$c->id);
                           $i++;
                       }
                   }
                }
            }    
             return $ret;
        } else {
         
           if($inicio !== null &&  $limite!=null){
               $this->db->limit($inicio, $limite);
           }
           $cert = $this->db->join('cedoc_doc', 'cedoc_doc.cedoc_doc_id = certificados_impressos.curso')->get('certificados_impressos', array('pagamento'=>0, 'status'=>0))->result();
           $ret  = null;
           $i    = 0;
           foreach ($cert as $c){
               $al = $this->imazoncursos->where('email', $c->email)->get('aluno')->result();
               if(count($al)==1){
                   @$ret[$i] = array('endereco'=>$al[0]->endereco,'numero'=>$al[0]->numero, 'bairro'=>$al[0]->bairro, 'cidade'=>$al[0]->cidade, 'estado'=>$al[0]->estado, $c->estado, 'aluno'=>$al[0]->nome, 'email'=>$al[0]->email,'id_curso'=>$c->id, 'curso'=>$c->nome, 'pedido'=>$c->pedido, 'cadastro'=>$c->dt_cadastro, 'status'=>$c->status, 'id'=>$c->id); 
               }
               $i++;
           }
           return $ret;
        }
    }

    public function get_all_inativo($inicio = null, $limite = null, $busca = null) {

        if ($busca !== null) {
            
            $al   = $this->imazoncursos->like('nome', $busca)->or_like('email', $busca)->or_like('id_fic', $busca)->get('aluno')->result();
            $i    = 1;
            $ret  = null;
            if(count($al)>=1){
                foreach($al as $a){
                   $this->db->join('cedoc_doc', 'cedoc_doc.cedoc_doc_id = certificados_impressos.curso')->where(array('certificados_impressos.pagamento'=>3, 'certificados_impressos.status'=>0, 'email'=>$a->email));
                   /*if($inicio !== null &&  $limite!=null){$this->db->limit($inicio, $limite);}*/
                   $cert = $this->db->get('certificados_impressos')->result();
                   if(count($cert)>=1){
                       foreach ($cert as $c) {
                           @$ret[$i] = array('endereco'=>$a->endereco,'numero'=>$a->numero, 'bairro'=>$a->bairro, 'cidade'=>$a->cidade, 'estado'=>$a->estado, 'aluno'=>$a->nome, 'email'=>$a->email,'id_curso'=>$c->id, 'curso'=>$c->nome, 'pedido'=>$c->pedido, 'cadastro'=>$c->dt_cadastro, 'status'=>$c->status, 'id'=>$c->id);
                           $i++;
                       }
                   }
                }
            }    
                return  $ret;
        } else {
           $this->db->join('cedoc_doc', 'cedoc_doc.cedoc_doc_id = certificados_impressos.curso')->where(array('certificados_impressos.pagamento'=>3, 'certificados_impressos.status'=>0));
           /*if($inicio !== null &&  $limite!=null){$this->db->limit($inicio, $limite);}*/
           $cert = $this->db->get('certificados_impressos')->result();
           $ret  = null;
           $i    = 0;
           foreach ($cert as $c){
               $al = $this->imazoncursos->where('email', $c->email)->get('aluno')->result();
               if(count($al)==1){
                   @$ret[$i] = array('endereco'=>$al[0]->endereco,'numero'=>$al[0]->numero, 'bairro'=>$al[0]->bairro, 'cidade'=>$al[0]->cidade, 'estado'=>$al[0]->estado, 'aluno'=>$al[0]->nome, 'email'=>$al[0]->email,'id_curso'=>$c->id, 'curso'=>$c->nome, 'pedido'=>$c->pedido, 'cadastro'=>$c->dt_cadastro, 'status'=>$c->status, 'id'=>$c->id); 
               }
               $i++;
           }
           return $ret;
        }
    }
    
    public function get_all_inativo_imazon($inicio = null, $limite = null, $busca = null) {
        if ($busca !== null) {
           return $this->imazoncursos
                   ->select('certificado.id_certificado as id_certificado, certificado.situacao as situacao, aluno.nome as nomeAluno, historico.id_historico as id_historico, certificado.endereco as endereco, pedido.data_compra as data_emissao, curso.nome as nomeCurso, certificado.id_tipo as id_tipo, aluno.id_fic as id_fic, historico.id_curso as id_curso')
                   ->from('certificado')
                   ->join('historico', 'historico.id_historico = certificado.id_historico')
                   ->join('aluno', 'historico.id_aluno = aluno.id_fic')
                   ->join('pedido', 'certificado.transacao = pedido.transacao')
                   ->join('curso', 'curso.id_curso = historico.id_curso')
                   ->where('certificado.situacao', 0)
                   ->like('aluno.nome', $busca)
                   ->or_like('aluno.email', $busca)
                   ->limit($limite)
                   ->get()
                   ->result();
        } else {
            return $this->imazoncursos
                   ->select('certificado.id_certificado as id_certificado, certificado.situacao as situacao, aluno.nome as nomeAluno, historico.id_historico as id_historico, certificado.endereco as endereco, pedido.data_compra as data_emissao, curso.nome as nomeCurso, certificado.id_tipo as id_tipo, aluno.id_fic as id_fic, historico.id_curso as id_curso')
                   ->from('certificado')
                   ->join('historico', 'historico.id_historico = certificado.id_historico')
                   ->join('aluno', 'historico.id_aluno = aluno.id_fic')
                   ->join('pedido', 'certificado.transacao = pedido.transacao')
                   ->join('curso', 'curso.id_curso = historico.id_curso')
                   ->where('certificado.situacao', 0)
                   ->limit($limite)
                   ->get()
                   ->result();
        }
    }

    public function get_all_ativo_imazon($inicio = null, $limite = null, $busca = null) {
        if ($busca !== null) {
           return $this->imazoncursos
                   ->select('certificado.id_certificado as id_certificado, certificado.situacao as situacao, aluno.nome as nomeAluno, historico.id_historico as id_historico, certificado.endereco as endereco, pedido.data_compra as data_emissao, curso.nome as nomeCurso, certificado.id_tipo as id_tipo, aluno.id_fic as id_fic, historico.id_curso as id_curso')
                   ->from('certificado')
                   ->join('historico', 'historico.id_historico = certificado.id_historico')
                   ->join('aluno', 'historico.id_aluno = aluno.id_fic')
                   ->join('pedido', 'certificado.transacao = pedido.transacao')
                   ->join('curso', 'curso.id_curso = historico.id_curso')
                   ->where('certificado.situacao', 1)
                   ->like('aluno.nome', $busca)
                   ->or_like('aluno.email', $busca)
                   ->limit($limite)
                   ->get()
                   ->result();
        } else {
            return $this->imazoncursos
                   ->select('certificado.id_certificado as id_certificado, certificado.situacao as situacao, aluno.nome as nomeAluno, historico.id_historico as id_historico, certificado.endereco as endereco, pedido.data_compra as data_emissao, curso.nome as nomeCurso, certificado.id_tipo as id_tipo, aluno.id_fic as id_fic, historico.id_curso as id_curso')
                   ->from('certificado')
                   ->join('historico', 'historico.id_historico = certificado.id_historico')
                   ->join('aluno', 'historico.id_aluno = aluno.id_fic')
                   ->join('pedido', 'certificado.transacao = pedido.transacao')
                   ->join('curso', 'curso.id_curso = historico.id_curso')
                   ->where('certificado.situacao', 1)
                   ->limit($limite)
                   ->get()
                   ->result();
        }
    }
    
    public function get_all($inicio = null, $limite = null, $busca = null) {

        $this->db->from('cedoc_doc');
        $this->db->join('cursos', 'cursos.cedoc_doc_id_fk = cedoc_doc.cedoc_doc_id');
        $this->db->join('cursos_cat', 'cursos.cursos_cat_id_fk = cursos_cat.cursos_cat_id');
        if ($busca !== null) {
            $this->db->like('nome', $busca);
        }
        if ($inicio !== null && $limite !== null) {
            $this->db->limit($limite, $inicio);
        }
        return $this->db->get();
    }

    public function enviados($busca = null) {
       if ($busca !== null) {
            
            $al   = $this->imazoncursos->like('nome', $busca)->get('aluno')->result();
            $i    = 1;
            $ret  = null;
            if(count($al)>=1){
                
                foreach($al as $a){
                   $cert = $this->db->join('cedoc_doc', 'cedoc_doc.cedoc_doc_id = certificados_impressos.curso')->get('certificados_impressos', array('pagamento'=>1, 'status'=>1, 'email'=>$a->email))->result();
                   if(count($cert)>=1){
                       foreach ($cert as $c) {
                           @$ret[$i] = array('endereco'=>$a->endereco,'numero'=>$a->numero, 'bairro'=>$a->bairro, 'cidade'=>$a->cidade, 'estado'=>$a->estado, 'aluno'=>$a->nome, 'email'=>$a->email,'id_curso'=>$c->id, 'curso'=>$c->nome, 'pedido'=>$c->pedido, 'cadastro'=>$c->dt_cadastro, 'status'=>$c->status, 'id'=>$c->id);
                           $i++;
                       }
                   }
                }
            }    
                return  $ret;
        } else {
          
           $cert = $this->db->join('cedoc_doc', 'cedoc_doc.cedoc_doc_id = certificados_impressos.curso')->get('certificados_impressos', array('pagamento'=>1, 'status'=>1))->result();
           if($inicio !== null &&  $limite!=null){
               $this->db->limit($inicio, $limite);
           }
           $ret  = null;
           $i    = 0;
           foreach ($cert as $c){
               $al = $this->imazoncursos->where('email', $c->email)->get('aluno')->result();
               if(count($al)==1){
                   @$ret[$i] = array('endereco'=>$al[0]->endereco,'numero'=>$al[0]->numero, 'bairro'=>$al[0]->bairro, 'cidade'=>$al[0]->cidade, 'estado'=>$al[0]->estado, $c->estado, 'aluno'=>$al[0]->nome, 'email'=>$al[0]->email,'id_curso'=>$c->id, 'curso'=>$c->nome, 'pedido'=>$c->pedido, 'cadastro'=>$c->dt_cadastro, 'status'=>$c->status, 'id'=>$c->id); 
               }
               $i++;
           }
           return $ret;
        }
    }

    public function nao_enviados($busca = null) {
        if ($busca == null) {
            return $this->db->query('SELECT c.id as id , a.nome as aluno, a.endereco as endereco, a.bairro as bairro, a.cidade as cidade, a.estado as estado, a.numero as numero, c.dt_cadastro cadastro, c.status as status, a.email, ced.nome as curso '
                            . 'FROM certificados_impressos '
                            . 'as c INNER JOIN imazon.aluno as a '
                            . 'ON c.email = a.email '
                            . 'INNER JOiN cedoc_doc as ced '
                            . 'on ced.cedoc_doc_id = c.curso WHERE c.status = 0');
        }
    }

    public function altera_status_fatura_certificado($id, $status, $tipo_pagamento) {
	    $data = array('tipo_pagamento' =>$tipo_pagamento , 'pagamento'=>$status);
        $this->db->where('pedido', $id);
        return $this->db->update('certificados_impressos', $data);
    }
    
    public function altera_status($id, $status) {
        $data = array('status' => $status, 'dt_alteracao'=>date('Y-m-d h:i:s'));
        $this->db->where('id', $id);
        return $this->db->update('certificados_impressos', $data);
    }
    
    public function altera_status_imazon($id, $status) {
        $data = array('situacao' => $status);
        $this->imazoncursos->where('id_certificado', $id);
        return $this->imazoncursos->update('certificado', $data);
    }
    
    public function busca_todos_certif_impressos_inativos(){
        $this->db->where(array('certificados_impressos.pagamento'=>3, 'certificados_impressos.status'=>0));
        return $this->db->get('certificados_impressos');
    }
    
    
    public function gera_todas_etiquetas(){
        $this->db->where(array('certificados_impressos.pagamento'=>3, 'certificados_impressos.status'=>0));
        $this->db->group_by('email');
        return $this->db->get('certificados_impressos');
    }
    
    public function gera_todas_etiquetas_imazon(){
        $this->imazoncursos->select('certificado.endereco as endereco, curso.nome as cursoNome, aluno.id_fic as id_fic, aluno.nome as nomeAluno, curso.id_curso as id_curso, historico.nota as nota, historico.carga_horaria as carga_horaria, historico.primeiro_dia as primeiro_dia, curso.descricao as descricao, certificado.id_tipo as id_tipo');
        $this->imazoncursos->from('certificado');
        $this->imazoncursos->join('historico', 'historico.id_historico = certificado.id_historico');
        $this->imazoncursos->join('curso', 'historico.id_curso = curso.id_curso');
        $this->imazoncursos->join('aluno', 'historico.id_aluno = aluno.id_fic');
        $this->imazoncursos->where('certificado.situacao', 0);
        $this->imazoncursos->group_by('aluno.id_fic');
        return $this->imazoncursos->get()->result();
    }
    
    public function gera_nome_certif_impresso_inativo_by_email($email){
        return $this->imazoncursos->where('id_fic', $email)->get('aluno')->result();
    }
    
    public function gera_todos_inativos($cedoc_doc_id_fk, $usuarios_id_fk) {
        $this->db->from('usuarios_cursos_matriculados');
        $this->db->where('usuarios_cursos_matriculados.email', $usuarios_id_fk);
        //$this->db->where('usuarios_cursos_matriculados.cedoc_doc_id_fk', $cedoc_doc_id_fk);
        $this->db->join('cedoc_doc', 'usuarios_cursos_matriculados.cedoc_doc_id_fk = cedoc_doc.cedoc_doc_id');
        $this->db->join('certificados_impressos', 'usuarios_cursos_matriculados.cedoc_doc_id_fk = certificados_impressos.curso');
        $this->db->limit(1);
        $data = $this->db->get();
        return $data;
    }
    
    public function gera_certificado_inativo_imazon($certificado=null, $email=null){
        $this->imazoncursos->from('certificado');
        $this->imazoncursos->where('certificado.situacao', 0);
        $this->imazoncursos->where('certificado.id_certificado', $certificado);
        $this->imazoncursos->join('historico', 'historico.id_historico = certificado.id_historico');
        $this->imazoncursos->join('curso', 'historico.id_curso = curso.id_curso');
        return $this->imazoncursos->get()->result();
    }
    
    public function gera_todos_certificados_inativo_imazon($certificado=null, $email=null){
        $this->imazoncursos->select('curso.nome as cursoNome, aluno.nome as nomeAluno, curso.id_curso as id_curso, historico.nota as nota, historico.carga_horaria as carga_horaria, historico.primeiro_dia as primeiro_dia, curso.descricao as descricao, certificado.id_tipo as id_tipo');
        $this->imazoncursos->from('certificado');
        $this->imazoncursos->join('historico', 'historico.id_historico = certificado.id_historico');
        $this->imazoncursos->join('curso', 'historico.id_curso = curso.id_curso');
        $this->imazoncursos->join('aluno', 'historico.id_aluno = aluno.id_fic');
        $this->imazoncursos->where('certificado.situacao', 0);
        return $this->imazoncursos->get()->result();
    }
    
    public function gera_todos_inativos_imazon($certificado=null, $email=null) {
        
        $this->imazoncursos->from('certificado');
        $this->imazoncursos->where('certificado.situacao', 0);
        
        if($certificado!==null){
            $this->imazoncursos->where('certificado.id_certificado', $certificado);
        }
       
        $registros = $this->imazoncursos->get()->result();
        
        if(count($registros)==1){
           foreach($registros as $r){
               //print_r($r);
               //echo $email;
               if($email!==null){
                   echo $r->id_historico;
                   $this->imazoncursos->from('historico');
                   $this->imazoncursos->where("historico.id_historico", $r->id_historico);
                   $this->imazoncursos->where('historico.id_aluno', $email);
                   $historico = $this->imazoncursos->get()->result();
                   print_r($historico);
                   echo '<br>';
               }
               
           } 
        }
        exit;
        //return $this->imazoncursos->join('curso', 'curso.id_curso = historico.id_curso')->get()->result();
    }
    
    public function verifica_ceritificado_impressso($email, $curso){
        $this->db->where('email', $email);
        $this->db->where('curso', $curso);
		$this->db->order_by('id', 'desc');
        $query = $this->db->get('certificados_impressos');
        return $query;
    }
    
    public function verifica_certificado_impressso_por_aluno($email){
        return $this->db->where('email', $email)->order_by('id', 'desc')->get('certificados_impressos');
    }
    
    public function verifica_transacoes_por_aluno($email){
        $this->db->select('transacoes.id as transacao, data_compra, situacao, planos.nome as nomeplano, tipo_pagamento');
        $this->db->join('planos', 'planos.id = transacoes.cod_plano');
        $this->db->order_by('transacoes.id', 'desc');
        return $this->db->where('transacoes.id_aluno', $email)->get('transacoes');
    }
    
    

}
