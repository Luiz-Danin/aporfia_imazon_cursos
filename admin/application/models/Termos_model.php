<?php

class Termos_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->imazoncursos = $this->load->database('imazoncursos', TRUE);
    }
    
    public function get_all($titulo = null, $inicio = null, $limite = null){
        if($titulo != null){
            $this->imazoncursos->like("descricao", $titulo);
        }
        if($inicio != null || $limite != null){
            $this->imazoncursos->limit($limite, $inicio);
        }else{
            $this->imazoncursos->limit(15);
        }
        $this->imazoncursos->order_by("id_termo desc");
        return $this->imazoncursos->get("termo");
    }
    
    public function count_all($titulo=null){
        if($titulo!==null){
            $this->imazoncursos->like("descricao", $titulo);
        }    
        $this->imazoncursos->from('termo');
        return $this->imazoncursos->count_all_results();
    }
    
    public function add($termo){
        $this->imazoncursos->where("status", 1);
        $this->imazoncursos->update("termo", array("data_atualizacao"=>date("Y-m-d h:m:s"), "status"=>0));
        return $this->imazoncursos->insert("termo", $termo);
    }
    
    public function get_termo_valido(){
        $this->imazoncursos->from('termo');
        return $this->imazoncursos->where("status", 1)->limit(1);
    }
    
}
