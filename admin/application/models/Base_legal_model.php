<?php

class Base_legal_model extends CI_Model {

    public function __construct() {
       parent::__construct();
       $this->imazoncursos = $this->load->database('imazoncursos', TRUE);
    }
    
    
   public function get_base_legal_atual(){
       return $this->imazoncursos->get('base_lega', array('situacao', 1))->result();
   }
    
}
