<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuarioimazon_model extends CI_Model{

    public function __construct() {
        parent::__construct();
        $this->imazon       = $this->load->database('imazon', TRUE);
        $this->imazon10     = $this->load->database('imazon10', TRUE);
        $this->imazoncursos = $this->load->database('imazoncursos', TRUE);
    }
    
    function get_notificacoes_assinaturas(){
        $data = $this->db->get("notificacoes_assinaturas");
        //$data = $this->db->join("log_planos_usuarios", "notificacoes_assinaturas.assinatura = log_planos_usuarios.assinatura");
        print_r($data->result());
    }
    
    function insert_notifica_assinatura($dados){
        $this->db->insert(`notificacoes_assinaturas`,$dados);  
    }
    
    function insert_requisicao_moip($dados){
        $this->db->insert("requisicao_moip",$dados);
    }
    
    function insert_log_moip_webhook($dados){
        $this->db->insert("log_moip_webhook", $dados);
    }
    
    function get_plano($id){
        return $this->db->get_where('planos', array('id'=> $id, 'status'=>1));
    }
   
    function buscar_certificado_imazon($data){
			
			$this->imazon->get('historico');
			echo $this->imazon->count_all_results();
    }
        
    function cancelar_assinatura($assinatura, $usuario){
        
        $data = $this->db->get_where("planos_usuarios", array('assinatura'=>$assinatura,'usuario'=>$usuario), 1)->result();
        
        if(count($data)==1){
            
            if((int)$data[0]->status == 0 || (int)$data[0]->status == 1){
                
                $this->db->where('assinatura', $assinatura);
                $this->db->where('usuario', $usuario);
                $this->db->update('planos_usuarios', array('status'=>2)); 
                
                return true;
            }else{
                return false;
            }
        
        }else{
            return false;
        }
    }
    
    function webhook_ativar_assinatura($assinatura){
        $data = $this->db->get_where("planos_usuarios", array('assinatura'=>$assinatura))->result();
        if(count($data)==1){
            if((int)$data[0]->status != 1){
                $plano = $this->db->get_where("planos", array('id'=>$data[0]->plano), 1)->result();
                $validade_nova = date('Y-m-d H:i:s',strtotime($data[0]->dt_validade . "+".$plano[0]->validade." months"));
                $this->db->where('assinatura', $assinatura);
                $this->db->update('planos_usuarios', array('status'=>1, 'dt_validade'=>$validade_nova)); 
                
                $this->db->insert('notificacoes_assinaturas', array('status'=>0, 'dt_cadastro'=>date('Y-m-d h:m:s'), 'assinatura'=>$assinatura, 'msg'=>"Pagamento efetuado com sucesso!"));
            }
        }
    }
    
    function webhook_cancelar_assinatura($assinatura){
        $data = $this->db->get_where("planos_usuarios", array('assinatura'=>$assinatura))->result();
        if(count($data)==1){
            
                $plano = $this->db->get_where("planos", array('id'=>$data[0]->plano), 1)->result();
                $validade_nova = date('Y-m-d H:i:s',strtotime($data[0]->dt_validade . "+".$plano[0]->validade." months"));
                $this->db->where('assinatura', $assinatura);
                $this->db->update('planos_usuarios', array('status'=>0, 'dt_validade'=>$validade_nova)); 
                
                $this->db->insert('notificacoes_assinaturas', array('status'=>0, 'dt_cadastro'=>date('Y-m-d h:m:s'), 'assinatura'=>$assinatura, 'msg'=>"Pagamento efetuado com sucesso!"));
                
                $assinatura = $this->ui->cancelar_assinatura($assinatura, $_SESSION['email_aluno']);
			
                //insere no log
                $dados_assinatura = $this->ui->get_dados_assinatura($assinatura, $_SESSION['email_aluno']);
                $log = array('plano_usuario'=>$dados_assinatura[0]->plano, 
                            'dt_cadastro'=>date('Y-m-d h:s:m'), 
                            'assinatura'=>$dados_assinatura[0]->assinatura,
                            'status'=>0);
                $this->ui->insert_log_plano_usuario($log);
                
        }
    }
    
     function ativar_assinatura($assinatura, $usuario){
        
        $data = $this->db->get_where("planos_usuarios", array('assinatura'=>$assinatura,'usuario'=>$usuario), 1)->result();
        
        if(count($data)==1){
            
            if((int)$data[0]->status == 0){
                
                $plano = $this->db->get_where("planos", array('id'=>$data[0]->plano), 1)->result();
                
                $validade_nova = date('Y-m-d H:i:s',strtotime($data[0]->dt_validade . "+".$plano[0]->validade." months"));
                
                $this->db->where('assinatura', $assinatura);
                $this->db->where('usuario', $usuario);
                $this->db->update('planos_usuarios', array('status'=>1, 'dt_validade'=>$validade_nova)); 
                
                return true;
            }else{
                return false;
            }
        
        }else{
            return false;
        }
    }
    
       
    function verificar_assinatura($assinatura, $usuario){
        $data = $this->db->get_where("planos_usuarios", array('assinatura'=>$assinatura,'usuario'=>$usuario), 1)->result();
        
        if(count($data)==1){
            return true;
        }else{
            return false;
        }
    } 
    
    function verificar_assinatura_ativa($assinatura, $usuario){
        $data = $this->db->get_where("planos_usuarios", array('assinatura'=>$assinatura,'usuario'=>$usuario), 1)->result();
        
        if(count($data)==1){
            return true;
        }else{
            return false;
        }
    }

	function get_dados_assinatura($assinatura, $usuario){
        $data = $this->db->get_where("planos_usuarios", array('assinatura'=>$assinatura,'usuario'=>$usuario), 1)->result();
        
        if(count($data)==1){
            return $data;
        }else{
            return false;
        }
    }

	function get_log_assinatura($assinatura){
        $data = $this->db->get_where("log_planos_usuarios", array('assinatura'=>$assinatura));
        if(count($data)>=1){
            return $data;
        }else{
            return false;
        }
    }	
	
    function faturas_moip_assinaturas($usuario){
            //return $this->db->get_where('planos_usuarios', array('usuario'=>$usuario));
            $this->db->select("planos.nome as plano,planos.nome as plano, planos_usuarios.dt_cadastro, planos_usuarios.dt_validade, planos_usuarios.assinatura, planos_usuarios.status as status, planos_usuarios.status_pagamento as pagamento");
            $this->db->from("planos_usuarios");
            $this->db->join("planos", "planos_usuarios.plano = planos.id");
            $this->db->where('usuario', $usuario);
            return $this->db->get();
    }	
    
    function adiciona_aula_realizada($cursos_aulas_id_fk,$usuarios_id_fk,$campo = null,$id = null){
		
        $this->db->from('cursos_aulas_realizadas');
        $this->db->where('cursos_aulas_id_fk',$cursos_aulas_id_fk);
        $this->db->where('email',$usuarios_id_fk);
       
        if ($this->db->count_all_results()==0){
				
                $this->db->from('cursos_aulas_realizadas');
                $this->db->where('cursos_aulas_id_fk',$cursos_aulas_id_fk);
                $this->db->where('email',$usuarios_id_fk);	
				
                if ($campo<>''){
                        $this->db->where($campo,$id);
                }else{
                        $this->db->where('cursos_aulas_arquivos_id_fk','0');
                        $this->db->where('cursos_aulas_videos_id_fk','0');
                        $this->db->where('cursos_aulas_exercicios_id_fk','0');
                        $this->db->where('cursos_aulas_imagens_id_fk','0');
                }	
				
                $this->db->delete('cursos_aulas_realizadas');
                $info['cursos_aulas_id_fk'] = $cursos_aulas_id_fk;
                $info['email'] = $usuarios_id_fk;
                $info['CARDataConcluida'] = date('Y-m-d H:i:s');
                
				if ($campo<>''){
                    $info[$campo] = $id;
                }
				
                $this->db->insert('cursos_aulas_realizadas',$info);
                return $this->db->insert_id();
        }
    }
		
    function curso_get_aula($cursos_aulas_id, $limite = null, $inicio = null){
        $this->db->select('*');
        $this->db->select('cursos_aulas_id as cursos_aulas_id_fk');
        $this->db->from('cursos_aulas');
        $this->db->where('cursos_aulas.cursos_aulas_id',$cursos_aulas_id);
        $data = $this->db->get();
        
        if($inicio !== null)
            $this->db->limit($limite, $inicio);
        
        return $data->result();
    }
    
    function curso_matriculado_certif($cedoc_doc_id_fk,$usuarios_id_fk){
		$this->db->from('usuarios_cursos_matriculados');
        $this->db->where('usuarios_cursos_matriculados.email',$usuarios_id_fk);
        $this->db->where('usuarios_cursos_matriculados.cedoc_doc_id_fk',$cedoc_doc_id_fk);
        //$this->db->or_where_in('usuarios_cursos_matriculados.status', array('0','1'));
		$this->db->where('usuarios_cursos_matriculados.status', '1');
		$this->db->join('cedoc_doc', 'usuarios_cursos_matriculados.cedoc_doc_id_fk = cedoc_doc.cedoc_doc_id');
       // $this->db->order_by('usuarios_cursos_matriculados.usuarios_cursos_matriculados_id','desc');
        $this->db->limit(1);
        $data = $this->db->get();
        return $data;
    }
	
	function curso_matriculado_aluno($cedoc_doc_id_fk,$usuarios_id_fk){
		$this->db->from('usuarios_cursos_matriculados');
        $this->db->where('usuarios_cursos_matriculados.email',$usuarios_id_fk);
        $this->db->where('usuarios_cursos_matriculados.cedoc_doc_id_fk',$cedoc_doc_id_fk);
        //$this->db->or_where_in('usuarios_cursos_matriculados.status', array('0','1'));
		$this->db->join('cedoc_doc', 'usuarios_cursos_matriculados.cedoc_doc_id_fk = cedoc_doc.cedoc_doc_id');
		$this->db->limit(1);
        $data = $this->db->get();
        return $data;
    }
    
    function get_aula_realizada($cursos_aulas_id,$usuarios_id_fk){
            $this->db->from('cursos_aulas_realizadas');
            $this->db->where('cursos_aulas_id_fk',$cursos_aulas_id);
            $this->db->where('email',$usuarios_id_fk);
            $data = $this->db->get();
            return $data->result();
    }
    
    function get_imagem_aula($cursos_aulas_id_fk,$cursos_aulas_imagens_id){
        $this->db->from('cursos_aulas_imagens');
        $this->db->where('cursos_aulas_id_fk',$cursos_aulas_id_fk);
        $this->db->where('cursos_aulas_imagens_id',$cursos_aulas_imagens_id);
        $data = $this->db->get();
        return $data->result();
    }
    
    function get_curso_capa($cedoc_doc_id_fk){
        $this->db->from('cedoc_doc_imagens');
        $this->db->where('cedoc_doc_imagens.cedoc_doc_id_fk',$cedoc_doc_id_fk);
        $this->db->limit(1);
        $data = $this->db->get(); 
        return $data->result();
    }

    function insere_log_usuarios($data){
        return $this->db->insert('log_usuarios', $data);
    }
    
    function get_arquivos_aula($cursos_aulas_id_fk){
        $this->db->from('cursos_aulas_arquivos');
        $this->db->where('cursos_aulas_id_fk',$cursos_aulas_id_fk);
        $this->db->join('cursos_aulas','cursos_aulas.cursos_aulas_id = cursos_aulas_arquivos.cursos_aulas_id_fk');
        $data = $this->db->get();
        return $data->result();
    }

    function get_imagens_aula($cursos_aulas_id_fk){
        $this->db->from('cursos_aulas_imagens');
        $this->db->where('cursos_aulas_id_fk',$cursos_aulas_id_fk);
        $this->db->join('cursos_aulas','cursos_aulas.cursos_aulas_id = cursos_aulas_imagens.cursos_aulas_id_fk');
        $data = $this->db->get();
        return $data->result();
    }
    
    function get_videos_aula($cursos_aulas_id_fk){
        $this->db->from('cursos_aulas_videos');
        $this->db->where('cursos_aulas_id_fk',$cursos_aulas_id_fk);
        $this->db->join('cursos_aulas','cursos_aulas.cursos_aulas_id = cursos_aulas_videos.cursos_aulas_id_fk');
        $data = $this->db->get();
        return $data->result();
    }

    function get_audios_aula($cursos_aulas_id_fk){
        $this->db->from('cursos_aulas_arquivos');
        $this->db->where('cursos_aulas_id_fk',$cursos_aulas_id_fk);
        $this->db->join('cursos_aulas','cursos_aulas.cursos_aulas_id = cursos_aulas_arquivos.cursos_aulas_id_fk');
        $this->db->like('CAACaminho','mp3');
        $data = $this->db->get();
        return $data->result();
    }
    
    function get_exercicio_aula($cursos_aulas_id_fk){
        $this->db->from('cursos_aulas_exercicios');
        $this->db->where('cursos_aulas_id_fk',$cursos_aulas_id_fk);
        $data = $this->db->get();
        return $data->result();
    }
    
    function get_exercicios_aula($cursos_aulas_id_fk){
        $this->db->from('cursos_aulas_exercicios');
        $this->db->where('cursos_aulas_id_fk',$cursos_aulas_id_fk);
        $data = $this->db->get();
        return $data->result();
    }
    
    function get_video_aula($cursos_aulas_id_fk,$cursos_aulas_videos_id){
            $this->db->from('cursos_aulas_videos');
            $this->db->where('cursos_aulas_id_fk',$cursos_aulas_id_fk);
            $this->db->where('cursos_aulas_videos_id',$cursos_aulas_videos_id);
            $data = $this->db->get();
            return $data->result();
    }
    
    function get_arquivo_aula($cursos_aulas_id_fk,$cursos_aulas_arquivos_id){
            $this->db->from('cursos_aulas_arquivos');
            $this->db->where('cursos_aulas_id_fk',$cursos_aulas_id_fk);
            $this->db->where('cursos_aulas_arquivos_id',$cursos_aulas_arquivos_id);
            $data = $this->db->get();
            return $data->result();
    }
    
    function get_ultima_aula_curso($cursos_id,$usuarios_id){
            $this->db->from('cursos');
            $this->db->where('cursos_id',$cursos_id);
            $this->db->join('cursos_aulas','cursos.cedoc_doc_id_fk = cursos_aulas.cedoc_doc_id_fk');
            $this->db->join('cursos_aulas_realizadas','cursos_aulas.cursos_aulas_id = cursos_aulas_realizadas.cursos_aulas_id_fk');
            $this->db->where('cursos_aulas_realizadas.email',$usuarios_id);
            $this->db->order_by('CARDataConcluida','desc');
            $this->db->limit(1);
            $data = $this->db->get();
            return ($data->result());
    }
    
    function get_plano_usuario($usuario_id){
            $this->db->from('planos_usuarios');
            $this->db->where('usuario',$usuario_id);
            $this->db->where('status',1);
            $data = $this->db->get();
            return $data; 
    }
    
    function get_ultimo_plano_usuario($usuario_id){
            $this->db->select('planos_usuarios.id as id, planos.nome as nome, planos_usuarios.status as status, planos_usuarios.dt_validade as dt_validade');
            $this->db->from('planos_usuarios');
            $this->db->join('planos', 'planos.id = planos_usuarios.plano');
            $this->db->where('usuario',$usuario_id);
            $this->db->order_by('planos_usuarios.id','desc');
            $this->db->limit(1);
            $data = $this->db->get();
            return $data; 
    }
    
    function matricula_usuario($cedoc_doc_id_fk,$usuarios_id_fk, $carga_horaria){
        
            //verifica se usu�rio est� cadastrado no curso
            $query1 = $this->db->get_where('usuarios_cursos_matriculados', array('cedoc_doc_id_fk' => $cedoc_doc_id_fk, 'email'=>$usuarios_id_fk))->result();
            
            //se usu�rio nunca cadastrou no curso
            if(count($query1) === 0){
                
                $validade  = strtotime("+ 90 days");
                $validade  = date('Y-m-d h:m:s', $validade);
                $info['UCMDataConclusaoCertificado'] = $validade;
                $info['cedoc_doc_id_fk'] = $cedoc_doc_id_fk;
                $info['email'] = $usuarios_id_fk;
                $info['UCMData'] = date('Y-m-d H:i:s');
                $info['status'] = 0;
                $info['UCMCargaHorariaCertificado'] = $carga_horaria; 
                $this->db->insert('usuarios_cursos_matriculados',$info);
                return true;
                
            }else{
                //print_r($query1);
                
                foreach($query1 as $q){
                    
                    $this->db->where('cedoc_doc_id_fk', $cedoc_doc_id_fk);
                    $this->db->where('email', $usuarios_id_fk);
                    $this->db->where('status', 1);
                    $this->db->from('usuarios_cursos_matriculados');
                    $query2 = $this->db->count_all_results();
                    
                    
                    if($query2 === 1){
                        return false;
                    }else{
                        
                        //remove aulas j� feitas
                        $aulas = $this->db->get_where('cursos_aulas', array('cedoc_doc_id_fk' => $cedoc_doc_id_fk))->result();
                        
                        foreach($aulas as $a){
                            $this->db->delete('cursos_aulas_realizadas', array('cursos_aulas_id_fk' => $a->cursos_aulas_id)); 
                        }
                        
                       
                    }
                }
                
                //insere curso
                 //cancela matr�culas anteriores
                $this->db->where('usuarios_cursos_matriculados_id', $q->usuarios_cursos_matriculados_id);
                $this->db->where('status',0);
                $this->db->where('email',$usuarios_id_fk);
                $this->db->update('usuarios_cursos_matriculados', array('status'=>2));

                $validade  = strtotime("+ 90 days");
                $validade  = date('Y-m-d h:m:s', $validade);
                $info['UCMDataConclusaoCertificado'] = $validade;
				$info['UCMCargaHorariaCertificado'] = $carga_horaria;
                $info['cedoc_doc_id_fk'] = $cedoc_doc_id_fk;
                $info['email'] = $usuarios_id_fk;
                $info['UCMData'] = date('Y-m-d H:i:s');
                $info['status'] = 0;

                $this->db->insert('usuarios_cursos_matriculados',$info);
                
                
            }
            
    }
    
    //verifica se usu�rio j� imprimiu certificado
    function verifica_imprimiu_ceritificado($aulas,$usuarios_id){
            foreach ($aulas as $aula){
                    $this->db->from('cursos_aulas_realizadas');
                    $this->db->where('cursos_aulas_id_fk',$aula->cursos_aulas_id);	
                    $this->db->where('email',$usuarios_id);
                    $this->db->where('CARHashingCertificado <>','');
                    if ($this->db->count_all_results()>0)
                    {
                            return true;	
                    }
            }
    }
    
    public function verifica_status_curso($id, $usuario){
        $this->db->from('usuarios_cursos_matriculados');
        $this->db->where('usuarios_cursos_matriculados_id',$id);
        $this->db->where('email',$usuario);
        return $this->db->count_all_results();
    }
    
    public function altera_status($usuarios_id, $curso, $status, $param = null){
        
	$this->db->where('email', $usuarios_id);
		
        if($param != null){
            $this->db->where('status', 0);
        }
		
        $this->db->where('usuarios_cursos_matriculados_id', $curso);
        $this->db->update('usuarios_cursos_matriculados', array('status'=>$status, 'UCMDataConclusaoCertificado'=>date('Y-m-d h:m:s')));
    }
	
    public function concluir_curso($usuarios_id, $curso){
		$this->db->where('email', $usuarios_id);
        $this->db->where('usuarios_cursos_matriculados_id', $curso);
        $this->db->update('usuarios_cursos_matriculados', array('status'=>1, 'UCMDataConclusaoCertificado'=>date('Y-m-d h:m:s')));
    }
    
    public function altera_dados_moip($data){
        
        $this->db->from('dados_usuario_moip');
        $this->db->where('code', $data['code']);
        
        if($this->db->count_all_results() === 1){
            
            $this->db->where('code', $data['code']);
            $this->db->update('dados_usuario_moip', $data);
        }else{
            echo 'asd';
            $this->db->insert('dados_usuario_moip', $data);
        }
        
        
    }
    
    //verifica e valida login de usu�rio
    public function verifica_login($dados){
		
        $this->imazoncursos->where('email', $dados['email']);
        $this->imazoncursos->where('senha', $dados['senha']);
        $this->imazoncursos->from('aluno');
		$this->imazoncursos->limit(1);
        $query = $this->imazoncursos->count_all_results();
        return $query;
    }
    
    //verifica e valida login de usu�rio
    public function verifica_login_adm($dados){
		
		$this->imazon->where('email = "'.$dados['email'].'" AND senha = "'.$dados['senha'].'"');
		$this->imazon->or_where('login = "'.$dados['email'].'" AND senha = "'.$dados['senha'].'"');
		$this->imazon->from('facilitador');
        $query = $this->imazon->get();
		
		return $query;
		
    }
    
    public function confirma_solic_certif($dados){
		if($this->db->insert('certificados_impressos',$dados)){
            return true;
        }else{
            return false;
        }
    }
    
    public function verifica_fatura_paga_ceritificado_impressso($email, $curso) {
        $this->db->where('email', $email);
        $this->db->where('curso', $curso);
        $this->db->where('pagamento', 1);
        $this->db->limit(1);
        $query = $this->db->get('certificados_impressos');
        return $query;
    }

    //atualiza status de pagamento para registro de pagamentos (d�bito/cr�dito/boleto)
    public function atualiza_status_pagamento($email, $id_transacao, $pagamento){
            $data = array('pagamento' => $pagamento);
            $this->db->where('email', $email);
            $this->db->where('instrucao_moip', $instrucao_moip);
            $this->db->update('certificados_impressos', $data); 
    }
	
    public function verifica_ceritificado_impressso($email, $curso){
        $this->db->where('email', $email);
        $this->db->where('curso', $curso);
		$this->db->order_by('id', 'desc');
        $query = $this->db->get('certificados_impressos');
        return $query;
    }
    
    //lista dados por email
    public function get_by_email($email){
		
        $this->imazon->where('email', $email);
        $this->imazon->limit(1);
        $query = $this->imazon->get('aluno');
        return $query;
    }

	//lista dados por email
    public function get_by_nome($nome){
        $this->imazon->where('nome', $nome);
        $this->imazon->limit(1);
        $query = $this->imazon->get('aluno');
        return $query;
    }

	//lista dados por email
    public function get_nome_certificado($nome){
        $this->imazon->select('email, nome');
		$this->imazon->where('nome', $nome);
        //$this->imazon->limit(1);
        $query = $this->imazon->get('aluno');
        return $query;
    }	
	
	public function historico($email){
		$this->imazon->select("*");
        $this->imazon->where('historico.nota >=', 5);
		$this->imazon->where('email', $email);
		$this->imazon->join('curso', 'curso.id_curso = historico.id_curso');
		$query = $this->imazon->get('historico');
        return $query; 
    }

	public function historico_imazon10($email){
		$this->imazon10->select("*");
        $this->imazon10->where('historico.nota >=', 5);
		$this->imazon10->where('email', $email);
		$this->imazon10->join('curso', 'curso.id_curso = historico.id_curso');
		$query = $this->imazon10->get('historico');
        return $query;
    }	
	
    function get_quant_aulas_realizadas($aulas,$usuarios_id){
        $quant_aulas = 0;
        foreach ($aulas as $aula){
                $this->db->from('cursos_aulas_realizadas');
                $this->db->where('cursos_aulas_id_fk',$aula->cursos_aulas_id);	
                $this->db->where('email',$usuarios_id);
                $data = $this->db->get();
                $quant_aulas+= count($data->result());
        }
        return $quant_aulas;
    }
    
    function verifica_aula($curso, $usuarios_id){
        $this->db->from('cursos_aulas_realizadas');
        $this->db->where('cursos_aulas_id_fk',$curso);	
        $this->db->where('email',$usuarios_id);
        $data = $this->db->get();
        return $data;
    }
    
    function verifica_plano_usuario($usuarios_id, $plano){
        $this->db->from('planos_usuarios');
        $this->db->where('usuario',$usuarios_id);
        $this->db->where('plano',$plano);
        $this->db->where('status',1);
        $data = $this->db->get();
        return $data;
    }
    
    function verifica_data_finalizacao_curso($usuarios_id, $curso){
        $this->db->from("usuarios_cursos_matriculados");
        $this->db->where("email", $usuarios_id);
        $this->db->where("cedoc_doc_id_fk", $curso);
        return $this->db->get();
    }
    
    function verifica_validade_plano($usuarios_id){
        $this->db->from('planos_usuarios');
        $this->db->where('usuario',$usuarios_id);
        $this->db->where('status',1);
        $data = $this->db->get();
        return $data;
    }
    
    function renova_plano_usuario($usuarios_id){
        $d    = array('status' => 0);
        $this->db->where('usuario', $usuarios_id);
        $this->db->update('planos_usuarios', $d);    
    }
    
    //cadastrar usu�rio na tabela de planos
    function insert_plano_usuario($data){
        
        $d    = array('status' => 0);
        $this->db->where('usuario', $data['usuario']);
        $this->db->where_not_in('status', array(0,2));
        $this->db->update('planos_usuarios', $d);  
        
        $data = $this->db->insert('planos_usuarios', $data);
        return $data;
    }
	
	//cadastrar usu�rio na tabela de planos
    function insert_log_plano_usuario($data){
        
        $data = $this->db->insert('log_planos_usuarios', $data);
        return $data;
    }
    
    
    function insert_dados_moip($data){
        
        $this->db->from('dados_usuario_moip');
        $this->db->where('code', $data['code']);
        
        if($this->db->count_all_results() === 0){
            $this->db->insert('dados_usuario_moip', $data);
        }
    }
    
    function get_dados_moip($id){
        return $this->db->get_where('dados_usuario_moip', array('code'=>$id));
    }
    
    function get_hashing_certificado($cedoc_doc_id_fk,$usuarios_id_fk){
            $this->db->from('cursos_aulas');
            $this->db->where('cursos_aulas.cedoc_doc_id_fk',$cedoc_doc_id_fk);
            $this->db->join('cursos_aulas_realizadas','cursos_aulas_realizadas.cursos_aulas_id_fk = cursos_aulas.cursos_aulas_id');
            $this->db->where('email',$usuarios_id_fk);;
            $data = $this->db->get();
            foreach ($data->result() as $item){
                    if ($item->CARHashingCertificado){
                            return $item->CARHashingCertificado;	
                    }else{					
                            $info['CARHashingCertificado'] = $cedoc_doc_id_fk.$usuarios_id_fk.$this->super_model->genRandomStringMenor();
                            $this->db->where('cursos_aulas_realizadas_id',$item->cursos_aulas_realizadas_id);
                            $this->db->update('cursos_aulas_realizadas',$info);
                            return $info['CARHashingCertificado'];
                    }
            }
    }
    
    function get_total_itens_curso($aulas,$cedoc_doc_id_fk){
          
            $total = 0;
            $total += count($aulas);

            foreach ($aulas as $aula){
                    $this->db->from('cursos_aulas_arquivos');
                    $this->db->where('cursos_aulas_arquivos.cursos_aulas_id_fk',$aula->cursos_aulas_id);
                    $data = $this->db->get();
                    $total += count($data->result());

                    $this->db->from('cursos_aulas_videos');
                    $this->db->where('cursos_aulas_videos.cursos_aulas_id_fk',$aula->cursos_aulas_id);
                    $data = $this->db->get();
                    $total += count($data->result());

                    $this->db->from('cursos_aulas_exercicios');
                    $this->db->where('cursos_aulas_exercicios.cursos_aulas_id_fk',$aula->cursos_aulas_id);
                    $this->db->group_by('cursos_aulas_id_fk');
                    $data = $this->db->get();
                    $total += count($data->result());

                    $this->db->from('cursos_aulas_imagens');
                    $this->db->where('cursos_aulas_imagens.cursos_aulas_id_fk',$aula->cursos_aulas_id);
                    $data = $this->db->get();
                    $total += count($data->result());
            }
            
            return $total;
    }
    
    //atualiza dados do aluno (imazon.aluno)
    public function atualiza_dados($dados = null){
        if($dados !== null){
            
            $this->imazon->where('email', $dados['email']);
            $query = $this->imazon->update('aluno', $dados); 
            
            if($query){
                return true;
            }else{
                return false;
            }
        }
    }
    
    //lista cursos em que o aluno est� matriculado
    function usuarios_cursos_matriculados($data, $inicio = null, $limite = null, $filtro = null){
        
            $this->db->from('usuarios_cursos_matriculados');
            $this->db->where('usuarios_cursos_matriculados.email', $data);
            $this->db->join('cursos','cursos.cedoc_doc_id_fk = usuarios_cursos_matriculados.cedoc_doc_id_fk','left');
            $this->db->join('cursos_cat', 'cursos.cursos_cat_id_fk = cursos_cat.cursos_cat_id','left');
            $this->db->join('cedoc_doc', 'cedoc_doc.cedoc_doc_id = cursos.cedoc_doc_id_fk','left');
            $this->db->where_not_in('status', array('2','3','4'));
            if($filtro !== null)
                $this->db->like('cedoc_doc.nome', $filtro);
            if($inicio !== null)
                $this->db->limit($limite, $inicio);
            return $this->db->get()->result();
            
    }
    
    function curso_previa($cedoc_doc_id){
            $this->db->select('*');
            $this->db->from('cursos');
            $this->db->where('cedoc_doc_id_fk',$cedoc_doc_id);
            $this->db->join('cedoc_doc', 'cedoc_doc.cedoc_doc_id = cursos.cedoc_doc_id_fk');
            $this->db->join('cursos_cat', 'cursos.cursos_cat_id_fk = cursos_cat.cursos_cat_id');
            $data = $this->db->get(); 
            return $data->result();
    }

    function get_all_aulas_curso($cedoc_doc_id_fk, $inicio = null, $limite = null){
        
            $this->db->select('*');
            $this->db->from('cursos_aulas');
            $this->db->where('cursos_aulas.cedoc_doc_id_fk',$cedoc_doc_id_fk);
            $this->db->order_by('CAOrdem','esc');
            
            
            if($inicio !== null && $limite !== null)
                $this->db->limit($limite, $inicio);
            $data = $this->db->get();
            return $data->result();
            
    }
    
    function get_all_usuarios(){ 
        $this->imazon->from('aluno');
        $this->imazon->like('nome', 'john lennon');
        return $this->imazon->get();
    }
    
    //lista cursos em que o aluno est� matriculado
    function get_num_cursos_matriculados($data, $inicio = null, $limite = null){
        
            $this->db->from('usuarios_cursos_matriculados');
            $this->db->where('usuarios_cursos_matriculados.email', $data);
            $this->db->join('cursos','cursos.cedoc_doc_id_fk = usuarios_cursos_matriculados.cedoc_doc_id_fk','left');
            $this->db->join('cursos_cat', 'cursos.cursos_cat_id_fk = cursos_cat.cursos_cat_id','left');
            $this->db->join('cedoc_doc', 'cedoc_doc.cedoc_doc_id = cursos.cedoc_doc_id_fk','left');
            
            if($inicio !== null)
                $this->db->limit($limite, $inicio);
            return $this->db->get()->result();
            
    }
    
    
    function usuarios_cursos_matriculados_todos($data){
            $this->db->from('usuarios_cursos_matriculados');
            $this->db->where('usuarios_cursos_matriculados.email',$data);
            $this->db->join('cursos','cursos.cedoc_doc_id_fk = usuarios_cursos_matriculados.cedoc_doc_id_fk','left');
            $this->db->join('cursos_cat', 'cursos.cursos_cat_id_fk = cursos_cat.cursos_cat_id','left');
            $this->db->join('cedoc_doc', 'cedoc_doc.cedoc_doc_id = cursos.cedoc_doc_id_fk','left');
            $this ->db->limit(10);
            $data = $this->db->get();
            return $data->result();
    }
    
    function usuarios_get_um($email){
            $this->imazon->from('aluno');
            $data = $this->imazon->where('email',$email);
            return $data;			
    }
	
	function get_curso($curso){
		return $this->db->get_where('cedoc_doc', array('cedoc_doc_id'=> $curso));	
    }
	
	public function count_all_log_usuarios($busca = null){
        
        $this->db->from('log_usuarios');
        if($busca !== null){$this->db->like('email', $busca);}
		return $this->db->count_all_results();

    }
     
    public function get_all_log_usuarios($inicio = null, $limite = null, $busca = null){
       
	   $this->db->from('log_usuarios');
        if($busca !== null){ $this->db->like('email', $busca);}
        if($inicio !== null && $limite !== null){$this->db->limit($limite, $inicio);} 
        return $this->db->get();
        
    }
	
}	