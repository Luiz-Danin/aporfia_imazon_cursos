<?php

class categorias_model extends CI_Model{

    public function __construct() {
        parent::__construct();    
    }
    
    public function add($data){
        $this->db->insert('cursos_cat', $data);
    }
    
    public function get_all($inicio = null, $limite = null){
        return $this->db->get('cursos_cat', $inicio, $limite);
    }
    
    public function get_by_id($id){
        return $this->db->get_where('cursos_cat', array('cursos_cat_id'=>$id));
    }
    
    public function get_status($id, $operacao){
        $this->db->select($operacao);
        $this->db->where('cursos_cat_id', $id);
        return $this->db->get('cursos_cat');
    }
    
    public function edit_by_id($data){
        $this->db->where('cursos_cat_id', $data['cursos_cat_id']);
        return $this->db->update('cursos_cat', $data); 
    }
    
    public function del($id){
        $this->db->where('cursos_cat_id', $id);
        return$this->db->delete('cursos_cat'); 
    }
    
}
