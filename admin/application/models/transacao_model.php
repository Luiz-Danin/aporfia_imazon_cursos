<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class transacao_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->imazoncursos = $this->load->database('imazoncursos', TRUE);
    }

    public function add($data = null, $id_fic) {
        
        $transacao = substr(md5(microtime()), 0, 32);
        //$r = $this->imazoncursos->select('id_fic')->get('aluno', array('email' => $data[0]->email))->result();
	$r = $this->imazoncursos->select('id_fic')->get_where('aluno', array('id_fic' => $id_fic), 1)->result();
        $dados = array('id_aluno' => $r[0]->id_fic, 'data_compra' => date('Y-m-d h:m:s'), 'situacao' => 0, 'valor' => $data[0]->valor, 'transacao' => $transacao, 'cod_plano' => $data[0]->id, 'tipo' => 1);

        if ($this->db->insert('transacoes', $dados)) {

            $r = $this->db->order_by('id', 'DESC')->get_where('transacoes', array('transacao' => $transacao), 1)->result();

            if (count($r) == 1) {
                return $r[0]->transacao;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function verifica_fatura_paga_ceritificado_impressso($email, $curso){
        return $this->db->get_where('certificados_impressos', array('email'=>$email, 'curso'=>$curso, 'status'=>1));
    }
    
    public function get_by_id($id = null, $aluno = null) {
	    $a = $this->imazoncursos->select('id_fic')->get_where('aluno', array('id_fic' => $aluno), 1)->result();
        //$a = $this->imazoncursos->select('id_fic')->get('aluno', array('email' => $aluno))->result();
        if (count($a) == 1) {
            $r = $this->db->get_where('transacoes', array('transacao' => $id, 'id_aluno' => $a[0]->id_fic), 1)->result();
            if (count($r) == 1) {
                return $r;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
	
	
	public function get_by_id_transacao($id = null, $aluno = null) {
        $a = $this->imazoncursos->select('id_fic')->get('aluno', array('id_fic' => $aluno))->result();
        if (count($a) == 1) {
            $r = $this->db->get_where('transacoes', array('id' => $id, 'id_aluno' => $a[0]->id_fic), 1)->result();
            if (count($r) == 1) {
                return $r;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
	
	
	public function get_by_id_transacao_porId($id = null) {
    
        $f = $this->db->get_where('transacoes', array('id' => $id))->result();
        if (count($f) >= 1) {
            return $f;
        } else {
            return false;
        }
    }
	

    public function get_log_assinatura($transacao=null){
        
        if(null!=$transacao){
          return $this->db->get_where('log_transacoes', array('transacao'=>$transacao), 1);      
        }else{
            return false;
        }
        
    }
    
    public function count_all_faturas($busca = null, $inicio = null, $limite = null) {

        if ($busca != null) {
            
            $aluno = $this->imazoncursos->select('id_fic, nome, apelido, email')->like('nome', $busca)->get_where('aluno')->result();

            $retorno = array();
            $i = 0;

            foreach ($aluno as $a) {
                
                $this->db->join('planos', 'planos.id = transacoes.cod_plano');
                if ($inicio !== null && $limite !== null) {
                    $this->db->limit($limite, $inicio);
                }
                
                $faturas = $this->db->get_where('transacoes', array('id_aluno' => $a->id_fic))->result();
                if (count($faturas) > 0) {
                    
                    foreach ($faturas as $f) {
                        $retorno[$i] = array('idFatura' => $f->id,
                            'data_compra' => $f->data_compra,
                            'situacao' => $f->situacao,
                            'transacao' => $f->transacao,
                            'plano' => $f->nome,
                            'data_alteracao' => $f->data_alteracao,
                            'nomeAluno' => $a->nome,
                            'apelidoAluno' => $a->apelido,
                            'emailAluno' => $aluno[0]->email,
                            'valor' => $f->valor);
                        $i++;
                    }
                }
                
            }
            
            return $retorno;
        } else {

            $this->db->join('planos', 'planos.id = transacoes.cod_plano');
            if ($inicio !== null && $limite !== null) {
                $this->db->limit($limite, $inicio);
            }
            $faturas = $this->db->get_where('transacoes')->result();

            $retorno = array();
            $i = 0;

            foreach ($faturas as $f) {
                $aluno = $this->imazoncursos->select('nome, apelido, email')->get_where('aluno', array('id_fic' => $f->id_aluno))->result();
                $retorno[$i] = array('idFatura' => $f->id,
                                     'data_compra' => $f->data_compra, 
                                     'situacao' => $f->situacao, 
                                     'transacao' => $f->transacao, 
                                     'plano' => $f->nome, 
                                     'data_alteracao' => $f->data_alteracao, 
                                     'nomeAluno' => $aluno[0]->nome, 
                                     'apelidoAluno' => $aluno[0]->apelido, 
                                     'emailAluno' => $aluno[0]->email, 
                                     'valor' => $f->valor);
                $i++;
            }

            return $retorno;
        }
    }

    public function get_faturas_aluno($aluno = null) {
        $this->db->join('planos', 'planos.id = transacoes.cod_plano');
        $f = $this->db->get_where('transacoes', array('id_aluno' => $aluno))->result();
        if (count($f) >= 1) {
            return $f;
        } else {
            return false;
        }
    }
    
    public function gera_nf_planos_aluno($aluno = null, $transacao) {
        $this->db->select('transacoes.id as id, transacao, nome, data_compra, situacao, data_alteracao, transacoes.valor as valor');
        $this->db->join('planos', 'planos.id = transacoes.cod_plano');
        $f = $this->db->get_where('transacoes', array('id_aluno' => $aluno, 'transacao'=>$transacao))->result();
        if (count($f) >= 1) {
            return $f;
        } else {
            return false;
        }
    }
    
    public function gera_nf_certificado_aluno($aluno = null, $pedido) {
        $this->db->select('pedido, nome, dt_cadastro, dt_alteracao, pagamento');
        $this->db->join('cedoc_doc', 'cedoc_doc.cedoc_doc_id = certificados_impressos.curso');
        $f = $this->db->get_where('certificados_impressos', array('email' => $aluno, 'pedido'=>$pedido))->result();
        if (count($f) >= 1) {
            return $f;
        } else {
            return false;
        }
    }
    
    public function get_faturas_planos_aluno($aluno = null) {
        $this->db->select('transacoes.id as id, transacao, nome, data_compra, situacao, data_alteracao');
        $this->db->join('planos', 'planos.id = transacoes.cod_plano');
        $f = $this->db->get_where('transacoes', array('id_aluno' => $aluno))->result();
        if (count($f) >= 1) {
            return $f;
        } else {
            return false;
        }
    }
    
    public function get_faturas_certifica_aluno($aluno = null) {
        $this->db->select('pedido, nome, dt_cadastro, dt_alteracao, pagamento');
        $this->db->join('cedoc_doc', 'cedoc_doc.cedoc_doc_id = certificados_impressos.curso');
        $f = $this->db->get_where('certificados_impressos', array('email' => $aluno))->result();
        if (count($f) >= 1) {
            return $f;
        } else {
            return false;
        }
    }
	
	 public function update($d = null) {
        $this->db->where('id', $d['id']);
        if ($this->db->update('transacoes', $d)) {
            return true;
        } else {
            return false;
        }
    }

    
    public function verifica_transacoes_por_aluno($aluno){
        return $this->imazoncursos->get_where("pedido", array("id_aluno"=>$aluno));
    }
    
    public function get_dados_transacoes_por_aluno($transacao){
               $this->imazoncursos->select("pedido.id_aluno, item_pedido.transacao as transacao, historico.id_historico as historico, pedido.situacao as situacao, pedido.data_compra as data_compra, pedido.tipo_pagamento as tipo_pagamento, item_pedido.valor as valor, produto.nome as nomeProduto, curso.nome as nomeCurso, pedido.data_alteracao as data_pagamento"); 
               $this->imazoncursos->join("historico", "historico.id = item_pedido.id_historico");
               $this->imazoncursos->join("curso", "historico.id_curso = curso.id_curso");
               $this->imazoncursos->join("pedido", "pedido.transacao = item_pedido.transacao");
               $this->imazoncursos->join("produto", "item_pedido.tipo_item = produto.id_produto")
                    ->where(array("item_pedido.transacao"=>$transacao));
        return $this->imazoncursos->get("item_pedido")->result();
        ///$pedido = $this->imazoncursos->get_where("item_pedido", array("transacao"=>$transacao))->result();
    }
    
}
