<?php

function verificaParcelamentoInvalido($parcelas, $parcelaLimiteOperadora) {
    $parcelamentoInvalido = false;
    if ($parcelas > $parcelaLimiteOperadora) {
        $parcelamentoInvalido = true;
    }

    return $parcelamentoInvalido;
}

function teste(){
    echo "Teste";
}

function dataAccountLookup($cpf, $sellerMail, $token, $order) {
    /* -------------------------------------------------------------------------- */
    /* ------------ INICIANDO WEBSERVICE DE CONSULTAR DADOS DE CONTA ------------ */
    /* -------------------------------------------------------------------------- */
    $success = false;
    $urlPost = "https://api.bcash.com.br/service/searchAccount/json";
    $search = "{'cpf':'$cpf'}";
    $version = '1.0';
    $encode = 'UTF-8';
    $searchData = array("data" => $search, "version" => $version, "encode" => $encode);
    ob_start();
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $urlPost);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($searchData, '', '&'));
    curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Basic " . base64_encode($sellerMail . ":" . $token)));
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); /* Se seu dom�nio possuir SSL, remover esta linha de c�digo */
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE); /* Se seu dom�nio possuir SSL, remover esta linha de c�digo */
    curl_exec($ch);
    $result = ob_get_contents();
    ob_end_clean();
    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    if ($httpCode != "200") {
        echo "Data Account Lookup - There was an error! $httpCode" . "<BR>";
    } else {
        $json = json_decode($result);

        switch ($json->{'code'}) {
            case '1': /* Uma conta encontrada - Conferir se h� compatibilidade entre o email que possui do consumidor e o email que Bcash retornou */
                $mailReturned = urldecode($json->{'accounts'}[0]->mail);
                if ($order['CUSTOMER_MAIL'] != $mailReturned) {
                    $order['CUSTOMER_MAIL'] = $mailReturned;
                }
                $success = true;
                break;

            case '2': /* Mais de uma conta encontrada - Conferir se h� compatibilidade entre o email que possui do consumidor e cada email que Bcash retornou */
                $limit = preg_replace("/[^0-9]/", "", urldecode($json->{'message'}));
                $success = false;

                for ($inc = 0; $inc < $limit; $inc++) {
                    $mailReturned = urldecode($json->{'accounts'}[$inc]->mail);
                    if ($order['CUSTOMER_MAIL'] == $mailReturned) {
                        $success = true;
                        break;
                    }
                }

                if ($success == true) {
                    //echo "Consultar dados de Transa��o - Successo!"."<BR><BR>";
                } else {
                    //echo "Consultar dados de Transa��o - Incompatibilidade entre emails de sua conta Bcash e informado na loja virtual!"."<BR><BR>";
                }
                break;

            case '3': /* Nenhuma conta encontrada - Prosseguir diretamente para webservice de Cria��o da Transa��o  */
                $success = true;
                break;
        }
    }
    return $success;
}

function consultarGarantia($garantia, $email, $token) {
    $data = json_encode($garantia);

    /* Dados de utiliza��o para consumo do servi�o */
    $urlPost = "https://api.bcash.com.br/service/searchExtendedWarranty/json/";
    $version = "1.0";   /* Padr�o version = 1.0 */
    $encode = "UTF-8"; /* Padr�o encode = UTF-8    tamb�m dispon�vel em ISO-8859-1 */

    $params = array("data" => $data, "version" => $version, "encode" => $encode);
    ob_start();
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $urlPost);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params, '', '&'));
    curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Basic " . base64_encode($email . ":" . $token)));
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
    curl_exec($ch);
    /* XML ou Json de retorno */
    $resposta = ob_get_contents();
    ob_end_clean();
    /* Capturando o http code para tratamento dos erros na requisi��o */
    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    if ($httpCode != "200") {
        //Tratamento das mensagens de erro
    } else {
        //Tratamento dos dados de resposta da consulta.
        //echo "Sucesso! $httpCode <BR><BR>";
        return $resposta;
    }
}

function arrayBuilder($order, $sellerMail, $orderId) {
    /* ------------------------------------------------------ */
    /* ------------ CONSTRUINDO O ARRAY DE DADOS ------------ */
    /* ------------------------------------------------------ */
    $data = NULL; /* Setando $data como default = NULL para limpar a vari�vel */
    $x = 0;   /* Setando $x como default = 0 para foreach abaixo em vendas com multi-produtos */
    /* Preenchendo array $data com todos os produtos encontrados no array $order */
    foreach ($order as $products) {
        $data['dependentTransactions'][$x]['email'] = $products->DEPENDENT_TRANSACTIONS_EMAIL;
        $data['dependentTransactions'][$x]['value'] = $products->DEPENDENT_TRANSACTIONS_VALUE;
        $data['products'][$x]['extendedWarranty']['amount'] = $products->PRODUCT_EXTENDEDWARRANTY_AMOUNT;
        $data['products'][$x]['extendedWarranty']['monthwarranty'] = $products->PRODUCT_EXTENDEDWARRANTY_MONTHWARRANTY;
        $data['products'][$x]['extendedWarranty']['amount'] = $products->PRODUCT_EXTENDEDWARRANTY_AMOUNT;
        $data['products'][$x]['extendedWarranty']['token'] = $products->PRODUCT_EXTENDEDWARRANTY_TOKEN;
        $data['products'][$x]['code'] = $products->PRODUCT_CODE;
        $data['products'][$x]['description'] = $products->PRODUCT_DESCRIPTION;
        $data['products'][$x]['amount'] = $products->PRODUCT_AMOUNT;
        $data['products'][$x]['value'] = $products->PRODUCT_VALUE;
        $data['products'][$x++]['extraDescription'] = $products->PRODUCT_EXTRA_DESCRIPTION;
    }
    /* Definindo Tipo de Frete e valor */
    if ($order['FREIGHT'] > 0) {
        $data['freight'] = $order['FREIGHT'];
        $data['freightType'] = strtoupper($order['FREIGHT_TYPE']);
    }
    /* Dados do Consumidor e seus Dados de Entrega */
    $data['buyer']['address']['address'] = $order['CUSTOMER_ADDRESS'];
    $data['buyer']['address']['number'] = $order['CUSTOMER_ADDRESS_NUMBER'];
    $data['buyer']['address']['complement'] = $order['CUSTOMER_ADDRESS_COMPLEMENT'];
    $data['buyer']['address']['neighborhood'] = $order['CUSTOMER_ADDRESS_NEIGHBORHOOD'];
    $data['buyer']['address']['city'] = $order['CUSTOMER_ADDRESS_CITY'];
    $data['buyer']['address']['state'] = $order[CUSTOMER_ADDRESS_STATE];
    $data['buyer']['address']['zipCode'] = preg_replace('/([0-9]{5})([0-9]{3})/', '$1-$2', str_pad($order['CUSTOMER_ADDRESS_ZIPCODE'], 8, '0', STR_PAD_LEFT)); //Brazillian ZipCode must be in format: 17516000 or 17516-000
    $data['buyer']['mail'] = $order['CUSTOMER_MAIL'];
    $data['buyer']['name'] = $order['CUSTOMER_NAME'];
    $data['buyer']['phone'] = $order['CUSTOMER_PHONE'];
    $data['buyer']['cellPhone'] = $order['CUSTOMER_CELLPHONE'];
    $data['buyer']['gender'] = $order['CUSTOMER_GENDER'];
    $data['buyer']['birthDate'] = $order['CUSTOMER_BIRTHDATE'];
    $data['buyer']['cpf'] = $order['CUSTOMER_CPF'];
    $data['buyer']['rg'] = $order['CUSTOMER_RG'];


    if ($pgto_prazo) {
        $data['paymentMethod']['code'] = $order[0]->PAYMENT_METHOD;

        $creditCardData = preg_split('/\�/', $order[0]->CARD_DATA);

        $data['creditCard']['number'] = $creditCardData[0];
        $data['creditCard']['holder'] = $creditCardData[1];
        $data['creditCard']['maturityMonth'] = $creditCardData[2];
        $data['creditCard']['maturityYear'] = $creditCardData[3];
        $data['creditCard']['securityCode'] = $creditCardData[4];
        $data['installments'] = $order[0]->INSTALLMENTS;
    }

    $data['sellerMail'] = $sellerMail;
    $data['orderId'] = $orderId;
    $data['acceptedContract'] = 'S'; /* Ambos acceptedContract e viewedContract devem possuir por default 'N', porque o consumidor deve aceitar os termos de pagamento Bcash, caso consumidor aceitar os termos, alterar para 'S' */
    $data['viewedContract'] = 'S'; /* Muitos de nossos clientes utilizam de um simples checkbox para conferir se o consumidor aceitou os termos de pagamento Bcash */
    $data['urlNotification'] = 'http://www.cidadeaprendizagem.com.br/notificacao_bcash.php'; /* Esta url receber� as informa��es para atualiza��o de seus pedidos a cada mudan�a de status no Bcash, sendo os dados enviados no padr�o URL de Aviso */
    $data['urlReturn'] = 'http://www.cidadeaprendizagem.com.br/notificacao_bcash.php'; /* Caso n�o informar o input 'urlNotification' esta url receber� as informa��es para atualiza��o de seus pedidos a cada mudan�a de status no Bcash, sendo os dados enviados no padr�o URL de Retorno, por�m caso informe ambas URLs, esta ser� a p�gina ao qual encaminharemos o consumidor ao finalizar o pagamento */
    return $data;
}

function grantAccess($consumerKey) {
    /* --------------------------------------------------------------------------------- */
    /* ------------ PROVENDO ACESSO PARA WEBSERVICE DE CRIA��O DE TRANSA��O ------------ */
    /* --------------------------------------------------------------------------------- */
    $time = time() * 1000;
    $microtime = microtime();
    $rand = mt_rand();
    $urlPost = 'https://api.bcash.com.br/service/createTransaction/json/';
    $signature = array(
        'oauth_consumer_key' => $consumerKey,
        'oauth_nonce' => md5($microtime . $rand),
        'oauth_signature_method' => 'PLAINTEXT',
        'oauth_timestamp' => $time,
        'oauth_version' => '1.0'
    );
    /* Encoding dos par�metros */
    $signature = base64_encode(http_build_query($signature, '', '&'));
    $oAuth = array(
        'Authorization: OAuth realm=' . $urlPost .
        ',oauth_consumer_key=' . $consumerKey .
        ',oauth_nonce=' . md5($microtime . $rand) .
        ',oauth_signature=' . $signature .
        ',oauth_signature_method=PLAINTEXT' .
        ',oauth_timestamp=' . $time .
        ',oauth_version=1.0',
        'Content-Type:application/x-www-form-urlencoded; charset=utf-8'
    );
    return $oAuth;
}

function creatingTransaction($data, $oAuth) {
    /* ---------------------------------------------------------------------------------------------------- */
    /* ------------ PREPARANDO JSON E ENVIANDO AO BCASH VIA WEBSERVICE DE CRIA��O DE TRANSA��O ------------ */
    /* ---------------------------------------------------------------------------------------------------- */
    $data = json_encode($data);  /* Encoding $data em formato JSON */

    $version = "1.0";   /* Vers�o atual utilizada 1.0 */
    $encode = "UTF-8"; /* Encoding dispon�veis atualmente: UTF-8 ou ISO-8859-1 */
    $urlPost = 'https://api.bcash.com.br/service/createTransaction/json/';
    $params = array("data" => $data, "version" => $version, "encode" => $encode);

    ob_start();
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_URL, $urlPost);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params, '', '&'));
    curl_setopt($ch, CURLOPT_HTTPHEADER, $oAuth);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); /* Se seu dom�nio possuir SSL, remover esta linha de c�digo */
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE); /* Se seu dom�nio possuir SSL, remover esta linha de c�digo */
    curl_exec($ch);
    /* XML ou Json retornado pelo Bcash */
    $resposta = ob_get_contents();
    ob_end_clean();
    /* Obtendo httpCode para verificar erros ou successo */
    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    if ($httpCode != "200") {
        /* Se httpCode � diferente de 200, a vari�vel $resposta cont�m a descri��o e c�digo de erro para auxili�-lo a identificar a situa��o e san�-la */
        return($resposta);
    } else {
        /* Se httpCode � igual a 200, houve sucesso ao criar uma nova transa��o e a vari�vel $resposta cont�m o n�mero da transa��o (gerado pelo Bcash), n�mero do pedido (enviado pela loja virtual) e o status desta transa��o */
        $temp = json_decode($resposta);
        $order[0]->STATUS = urldecode($temp->{'status'});
        $order[0]->STATUS_DESCRIPTION = urldecode($temp->{'descriptionStatus'});

        return($resposta);
    }
}
