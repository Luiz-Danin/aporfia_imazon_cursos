<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
function pdf_create($html, $filename='', $stream=TRUE, $tipo=null){

    require_once("dompdf/dompdf_config.inc.php");

    $dompdf = new DOMPDF();
   
	$dompdf->load_html($html);
 
	// Definimos o tamanho do papel e
	// sua orienta��o (retrato ou paisagem)
	if($tipo=="certificado"){
		$dompdf->set_paper('A5','landscape');
	}else{
		$dompdf->set_paper('A4','portrait');
	}
	
	 
	// O arquivo � convertido
	$dompdf->render();
	 
	// Salvo no diret�rio tempor�rio do sistema
	// e exibido para o usu�rio
	$dompdf->stream("certificados_validados.pdf");
	
}
?>