<?php

function limita_caracteres($texto, $limite, $quebra = true) {
        $tamanho = strlen($texto);
        // Verifica se o tamanho do texto é menor ou igual ao limite
    if ($tamanho <= $limite) {
        $novo_texto = $texto;
        // Se o tamanho do texto for maior que o limite
    } else {
        // Verifica a opção de quebrar o texto
        if ($quebra == true) {
            $novo_texto = trim(substr($texto, 0, $limite)).'...';
            // Se não, corta $texto na última palavra antes do limite
        } else {
            // Localiza o útlimo espaço antes de $limite
            $ultimo_espaco = strrpos(substr($texto, 0, $limite), ' ');
            // Corta o $texto até a posição localizada
            $novo_texto = trim(substr($texto, 0, $ultimo_espaco)).'...';
        }
    }
    // Retorna o valor formatado
    return $novo_texto;
} 

function removeAcento($string){
    return preg_replace("[^a-zA-Z0-9_]", "", strtr($string, "áàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ ", "aaaaeeiooouucAAAAEEIOOOUUC_"));
}

function limpaCaracteres($string){
    return  preg_replace('/[^[:alnum:]_]/', '', preg_replace('/[^[:alpha:]_]/', '', $string));
}

function removeCaracteresInvalidos($string){
    return str_replace(array("<", ">", "\\", "/", "=", "'", "?"), "", $string);
}

function moeda($get_valor) {
    
    /*$d = explode(',', str_replace('.', ',', $get_valor));
    
    if(isset($d[1])==true && $d[1]<10){
        return $d[0].','.$d[1].'0';
    }else{
        return $d[0].',00';
    }*/
    if(strlen($get_valor)>2){
        echo substr($get_valor, 0, -2).','.substr($get_valor, -2);
    }
    
}


function NormalizaURL($str){
    $str = strtolower(utf8_decode($str)); $i=1;
    $str = strtr($str, utf8_decode('àáâãäåæçèéêëìíîïñòóôõöøùúûýýÿ'), 'aaaaaaaceeeeiiiinoooooouuuyyy');
    $str = preg_replace("/([^a-z0-9])/",'-',utf8_encode($str));
    while($i>0) $str = str_replace('--','-',$str,$i);
    if (substr($str, -1) == '-') $str = substr($str, 0, -1);
    return $str;
}