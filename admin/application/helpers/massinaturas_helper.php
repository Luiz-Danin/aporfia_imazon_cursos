<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*PLANOS*/

function listar_planos(){
    include_once 'moip-assinaturas-php-master/tests/planos/listar.php';
}

function criar_plano($dados){

        require_once(dirname(__FILE__).'/moip-assinaturas-php-master/moip.php');
    
        $codigo_plano = $dados['code'];

	$plano = $moip->plans
		->setIdentification(
			array(
				'code'        => $codigo_plano,
				'name'        => $dados['name'],
				'description' => $dados['description']
			)
		)
		->setAmount($dados['amount'])
		->setSetupFee()
		//->setValues(8000, 400) //o mesmo que setAmount e setSetupFee
		->setInterval($dados['interval']['unit'], (int)$dados['interval']['length'])
		//->setBillingCycles(10)
		//->setTrial(10, false)
		->setStatus('inactive')
		//->setMaxSignatures(100)
		//->setConfigurations('inactive', 100) //o mesmo que setStatus e setMaxSignatures
		->create();

	return array('mensagem'=>$plano->message, 'http_code'=>$plano->http_code);
}

function editar_plano($dados){

        require_once(dirname(__FILE__).'/moip-assinaturas-php-master/moip.php');
        
        $codigo_plano = $dados['code'];
	
	$plano = $moip->plans
		->setIdentification(
			array(
				'code'        => $codigo_plano,
				'name'        => $dados['name'],
				'description' => $dados['description']
			)
		)
		->setAmount($dados['amount'])
		->setSetupFee()
		//->setValues(8000, 1000) //o mesmo que setAmount e setSetupFee
		->setInterval($dados['interval']['unit'], (int)$dados['interval']['length'])
		//->setBillingCycles(10)
		//->setTrial(10, false)
		->setStatus($dados['status'])
		//->setMaxSignatures(100)
		//->setConfigurations('active', 100) //o mesmo que setStatus e setMaxSignatures
		->update();

	return array('mensagem'=>$plano->message, 'http_code'=>$plano->http_code);
}



function ativar_plano($dados){
    require_once(dirname(__FILE__).'/moip-assinaturas-php-master/moip.php');
    $codigo_plano = $dados;
    //Ativar
    $plano = $moip->plans->activate($codigo_plano);
    return array('mensagem'=>$plano->message, 'http_code'=>$plano->http_code);
}

function desativar_plano($dados){
    require_once(dirname(__FILE__).'/moip-assinaturas-php-master/moip.php');
    $codigo_plano = $dados;   
   //Inativar
    $plano = $moip->plans->inactivate($codigo_plano);
    return array('mensagem'=>$plano->message, 'http_code'=>$plano->http_code);
}
/*FIM PLANOS*/

/*ASSINATURAS*/

function suspender_assinatura($id){
    require_once(dirname(__FILE__).'/moip-assinaturas-php-master/moip.php');
    $assinatura = $moip->subscriptions->suspend($id);
    return array('mensagem'=>$assinatura->message, 'http_code'=>$assinatura->http_code);
}

function ativar_assinatura($id){
    require_once(dirname(__FILE__).'/moip-assinaturas-php-master/moip.php');
    $assinatura = $moip->subscriptions->activate($id);
    return array('mensagem'=>$assinatura->message, 'http_code'=>$assinatura->http_code);
}

function cancelar_assinatura($id){	
    require_once(dirname(__FILE__).'/moip-assinaturas-php-master/moip.php');
    $assinatura = $moip->subscriptions->cancel($id);
    return array('mensagem'=>$assinatura->message, 'http_code'=>$assinatura->http_code);
}

/*FIM ASSINATURAS*/


/*CLIENTE*/
function get_cliente_by_id($id){
    require_once(dirname(__FILE__).'/moip-assinaturas-php-master/moip.php');
    $cliente = $moip->customers->setCode($id)->get();
    return array('mensagem'=>$cliente->message, 'http_code'=>$cliente->http_code);
}
/*FIM CLIENTE*/ 

/*WEBHOOKS*/
function gatilho(){

require_once(dirname(__FILE__).'/moip-assinaturas-php-master/moip.php');

MoipLogger::open('webhooks.log');

	if(!$moip->webhooks->get()){
                echo nl2br(MoipLogger::read());
		exit;
	}else{
		
		$log  = "Property: {$moip->webhooks->property}" . "\n";
		$log .= "Date: {$moip->webhooks->date}" . "\n";
		$log .= "Env: {$moip->webhooks->env}" . "\n";
		$log .= "Resource: " . serialize($moip->webhooks->resource) . "\n";
		
		MoipLogger::write($log);
		
		switch($moip->webhooks->property){
			case 'plans':
				switch($moip->webhooks->event){
					case 'created':
						//Manipular os dados...
						break;
					case 'updated':
						//Manipular os dados...
						break;
					case 'activated':
						//Manipular os dados...
						break;						
					case 'inactivated':
						//Manipular os dados...
						break;
				}
				break;
			case 'customer':
				switch($moip->webhooks->event){
					case 'created':
						//Manipular os dados...
						break;
					case 'updated':
						//Manipular os dados...
						break;
				}
				break;
			case 'subscription':
				switch($moip->webhooks->event){
					case 'created':
						//Manipular os dados...
						break;
					case 'updated':
						//Manipular os dados...
						break;
					case 'suspended':
						//Manipular os dados...
						break;
					case 'activated':
						return array("property"=>$moip->webhooks->property,
                                                             "event"=>$moip->webhooks->event,
                                                             "assinatura"=>$moip->webhooks->resource->code,
                                                             "dt_cadastro"=>$moip->webhooks->date);
						break;
					case 'canceled':
						//Manipular os dados...
						break;
				}
				break;
			case 'invoice':
				switch($moip->webhooks->event){
					case 'created':
						//Manipular os dados...
						break;
					case 'status_updated':
						//Manipular os dados...
						break;
				}
				break;
			case 'payment':
				switch($moip->webhooks->event){
					case 'created':
						//Manipular os dados...
						break;
					case 'status_updated':
						//Manipular os dados...
						break;
				}
				break;
		}
	}
}        
/*WEBHOOKS*/        