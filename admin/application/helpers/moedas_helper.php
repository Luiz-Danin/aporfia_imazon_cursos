<?php
function moeda($get_valor) {
    
    $d = explode(',', str_replace('.', ',', $get_valor));
    
    if(isset($d[1])==true && $d[1]<10){
        return $d[0].','.$d[1].'0';
    }else{
        return $d[0].',00';
    }
}
