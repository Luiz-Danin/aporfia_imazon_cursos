<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
  | This file lets you re-map URI requests to specific controller functions.
  |
  | Typically there is a one-to-one relationship between a URL string
  | and its corresponding controller class/method. The segments in a
  | URL normally follow this pattern:
  |
  |	example.com/class/method/id/
  |
  | In some instances, however, you may want to remap this relationship
  | so that a different class/function is called than the one
  | corresponding to the URL.
  |
  | Please see the user guide for complete details:
  |
  |	http://codeigniter.com/user_guide/general/routing.html
  |
  | -------------------------------------------------------------------------
  | RESERVED ROUTES
  | -------------------------------------------------------------------------
  |
  | There area two reserved routes:
  |
  |	$route['default_controller'] = 'welcome';
  |
  | This route indicates which controller class should be loaded if the
  | URI contains no data. In the above example, the "welcome" class
  | would be loaded.
  |
  |	$route['404_override'] = 'errors/page_missing';
  |
  | This route will tell the Router what URI segments to use if those provided
  | in the URL cannot be matched to a valid route.
  |
 */

$route['default_controller'] = "site/cursos";
$route['suporte'] = "/site/suporte";
$route['cadastrar'] = "/site/cadastrar";
$route['cadastro'] = "/site/cadastrar";
$route['redir_cad_matricula'] = "/site/redir_cad_matricula";
$route['cursos'] = "/site/cursos";
$route['cursos/(:any)'] = '/site/cursos/$1';
$route['ver_curso/(:any)'] = '/site/visualizar_curso/$1';
$route['ver_curso'] = '/site/cursos';
$route['blog/(:any)'] = '/blog/index/$1';
$route['post/busca'] = '/blog/buscar_post/$1';
$route['gerarPagamento'] = 'site/gerarPagamento';
$route['post/(:any)'] = '/blog/visualizar/$1';
$route['mais_visitados'] = '/blog/mais_visitados/';
$route['mais_visitadas'] = '/blog/mais_visitados/';
$route['post'] = '/blog';
$route['cursos_por_areas/(:any)'] = '/site/cursos_areas/$1';
$route['categoria/(:any)'] = '/blog/post_por_categoria/$1';
$route['mais_vendidos']    = '/site/mais_vendidos';
$route['buscar_curso'] = '/site/buscar_curso';
$route['buscar_curso_ajax/(:any)'] = '/site/buscar_curso_ajax/$1';
$route['trocar_curso/(:any)'] = 'aluno/curso/trocar_curso';
$route['logar'] = "/site/logar";
$route['recuperar_senha'] = "/site/recuperar_senha";
$route['rec_senha'] = "/site/rec_senha";
$route['autenticacao'] = "/site/autenticador";
$route['certificado_participacao/(:any)'] = "aluno/curso/certificado_participacao/$1";
$route['meus_certificados'] = "aluno/curso/meus_certificados";
$route['autentica/(:any)'] = "/site/gera_autenticacao_certificado/$1";
$route['participacao/(:any)'] = "/site/certificado_participacao/$1";
$route['gera_autenticacao_certificado_vip/(:any)'] = "/site/gera_autenticacao_certificado_vip/$1";
$route['gera_autenticacao_certificado_vip_imp/(:any)'] = "/site/gera_autenticacao_certificado_vip_imp/$1";
$route['addItemCarrinho'] = "/cart/add";
$route['limparCarrinho'] = "/cart/clean_cart";
$route['removerItemCarrinho/(:any)'] = "/cart/del";
$route['listaCompras'] = "/site/itens_carrinho";
$route['finalizarPedido'] = "/site/finalizarPedido";
$route['endereco_cobranca'] = "aluno/usuario/endereco_cobranca";
$route['verifica_cadastro'] = "/site/verifica_cadastro";
$route['matricular_cadastrar/(:any)/(:any)'] = "/site/matricular_cadastrar/$1/$2";

$route['blog'] = "blog";
$route['home'] = "site";

$route['logar_matricular'] = "site/logar_matricular";
$route['logout'] = "site/sair";
$route['sair'] = "site/sair";
$route['404_override'] = 'site';
$route['termo'] = 'site/termo';
$route['termo_uso'] = 'site/termo_novo';
$route['matricular_curso'] = 'site/matricular_curso';
$route['ultimo_curso_matriculado'] = 'aluno/curso/ultimo_curso_matriculado';
$route['cursos_por_areas/ultimo_curso_matriculado'] = 'aluno/curso/ultimo_curso_matriculado';
$route['solicitar_segunda_chance/(:any)'] = 'cart/solicitar_segunda_chance';
$route['solicitar_certificado_impresso'] = 'cart/add_certificado';
$route['faturas'] = 'aluno/financeiro/faturas';
$route['corrigir_avaliacao'] = "aluno/curso/corrigir_avaliacao/";
$route['fazer_avaliacao/(:any)'] = "aluno/curso/fazer_avaliacao/$1";
$route['cursos_matriculados/(:any)'] = 'aluno/curso/index/$1';
$route['cursos_matriculados'] = 'aluno/curso/index';
$route['estudar/(:any)'] = 'aluno/curso/estudar';
$route['solicitar_certificado_impresso/(:any)'] = 'aluno/curso/solicitar_certificado_impresso/$1';
$route['dados_cadastrais'] = 'aluno/usuario/dados_cadastrais/';
$route['gerar_declaracao/(:any)'] = 'aluno/curso/gerar_declaracao';
$route['gerar_certificado_web/(:any)'] = 'aluno/curso/gerar_certificado_web';
$route['gerar_certificado_webnovo/(:any)'] = 'aluno/curso/gerar_certificado_webnovo';
$route['cancelar_curso/(:any)'] = 'aluno/curso/cancelar_curso';
$route['liberar_curso/(:any)'] = 'aluno/curso/liberar_curso';
$route['liberamanual/(:any)'] = 'liberamanual';


/*Testes e ajustes*/
$route['home_teste'] = "site/home2";
$route['verifica_cadastro2'] = "/site/verifica_cadastro2";
/* End of file routes.php */
/* Location: ./application/config/routes.php */