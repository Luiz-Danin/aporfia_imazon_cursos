<?php

class historico_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->vip = $this->load->database('vip', TRUE);
    }

    public function add($historico) {
        return $this->db->insert('historico', $historico);
    }

    public function verifica_situacao_segunda_chance($d = null) {
        $this->db->select('historico.id, historico.id_historico as id_historico, aluno.email as email, aluno.rg as rg, nota, curso.nome as nomeCurso, aluno.nome as nomeAluno, historico.carga_horaria, primeiro_dia, data_aprovacao, descricao, situacao, aluno.id_fic, historico.codigo_autenticacao as codigo_autenticacao');
        $this->db->from('historico');
        $this->db->join('curso', 'curso.id_curso = historico.id_curso');
        $this->db->join('aluno', 'aluno.email = historico.email');
        $this->db->where(array('historico.id_aluno' => $this->session->userdata('imazon_id_fic'), 'historico.id_historico' => $d));
        $this->db->order_by("historico.id asc");
        return $this->db->get();
    }

    public function atualiza_historico_mat($historico, $dados) {
        $data = array('situacao' => 1);
        $this->db->where('id', $historico);
        $this->db->where('situacao', '0');
        $this->db->update('historico', $data);
    }

    public function trocacurso_historico($dados, $cr) {
        $hti = $dados[0]->id;
        $data = array('id_curso' => $cr);
        $this->db->where('id', $hti);
        $this->db->where('id_curso', '2861');
        $this->db->update('historico', $data);
    }

    public function inserelogtrocacurso($dados) {
        return $this->db->insert('log_promocao', $dados);
    }

    public function atualiza_historico_mat_cancelado($historico, $dados) {
        $data = array('situacao' => 1);
        $this->db->where('id', $historico);
        $this->db->where('situacao', '5');
        $this->db->update('historico', $data);
    }

    public function atualiza_historico_by_id($id, $dados) {
        return $this->db->limit(1)->order_by('historico.id desc')->update('historico', $dados, array("id" => $id));
    }

    public function atualiza_historico_mat_devolvido($historico, $dados) {
        $data = array('situacao' => 6);
        $this->db->where('id', $historico);
        $this->db->update('historico', $data);
    }

    public function verifica_segunda_chance($d) {
        $this->db->from('historico');
        $this->db->where(array('historico.id_aluno' => $this->session->userdata('imazon_id_fic'), 'id_historico' => $d));
        $this->db->where('id_avaliacao', 2);
        return $this->db->count_all_results();
    }

    public function get_historico_by_id_aluno_curso($d = null) {
        $this->db->from('historico');
        $this->db->where(array('historico.id_aluno' => $this->session->userdata('imazon_id_fic'), 'id_historico' => $d));
        $this->db->order_by("id desc");
        $this->db->limit(1);
        return $this->db->get();
    }

    public function get_historicos_by_id_aluno_curso($d = null) {
        $this->db->from('historico');
        $this->db->where(array('historico.id_aluno' => $this->session->userdata('imazon_id_fic'), 'id_historico' => $d));
        return $this->db->get();
    }

    public function _get_curso_historico_by_id_curso($d = null) {
        $this->db->from('aluno_curso');
        $this->db->where(array('email' => $this->session->userdata('imazon_email_aluno'), 'id_curso' => $d));
        return $this->db->get();
    }

    public function _get_dados_certificado_by_id_aluno_curso($d = null) {
        $this->db->from('aluno_curso');
        $this->db->join('curso', 'curso.id_curso = aluno_curso.id_curso');
        $this->db->where(array('email' => $this->session->userdata('imazon_email_aluno'), 'aluno_curso.id_aluno_curso' => $d));
        return $this->db->get();
    }

    public function get_dados_historico_by_id_historico($d = null) {
        $this->db->from('historico');
        $this->db->join('curso', 'curso.id_curso = historico.id_curso');
        $this->db->where(array('historico.id_aluno' => $this->session->userdata('imazon_id_fic'), 'historico.id_historico' => $d));
        return $this->db->get();
    }

    public function get_dados_historico_cabecalho_curso_by_id_historico($d = null) {
        $this->db->from('historico');
        $this->db->join('aluno', 'aluno.id_fic = historico.id_aluno');
        $this->db->where(array('historico.id_aluno' => $this->session->userdata('imazon_id_fic'), 'historico.id' => $d));
        return $this->db->get();
    }

    public function gera_historico_by_id_historico($d = null) {
        $this->db->select('historico.situacao as situacao, id_historico, aluno.email as email,aluno.rg as rg, nota, curso.nome as nomeCurso, aluno.nome as nomeAluno, historico.carga_horaria,codigo_autenticacao,  primeiro_dia, data_aprovacao, descricao');
        $this->db->from('historico');
        $this->db->join('curso', 'curso.id_curso = historico.id_curso');
        //$this->db->join('aluno', 'aluno.email = historico.email');
        $this->db->join('aluno', 'aluno.id_fic= historico.id_aluno');
        $this->db->where(array('historico.id_aluno' => $this->session->userdata('imazon_id_fic'), 'historico.id_historico' => $d));
        $this->db->order_by('historico.id desc');
        $this->db->limit(1);
        return $this->db->get();
    }

    public function gera_autenticacao_certificado($d = null) {
        $this->db->select('historico.situacao as situacao, id_historico, aluno.email as email,aluno.rg as rg, nota, curso.nome as nomeCurso, aluno.nome as nomeAluno, historico.carga_horaria, primeiro_dia,codigo_autenticacao ,data_aprovacao, descricao');
        $this->db->from('historico');
        $this->db->join('curso', 'curso.id_curso = historico.id_curso');
        $this->db->join('aluno', 'aluno.email = historico.email');
        $this->db->where('historico.id_historico', $d);
        $this->db->order_by('historico.id desc');
        $this->db->limit(1);
        return $this->db->get();
    }

    public function gera_autenticacao_certificado_vip($matricula) {
        $this->vip->select('*');
        $this->vip->from('usuarios_cursos_matriculados');
        $this->vip->join('cedoc_doc', 'usuarios_cursos_matriculados.cedoc_doc_id_fk = cedoc_doc.cedoc_doc_id');
        $this->vip->where('usuarios_cursos_matriculados.usuarios_cursos_matriculados_id', $matricula);
        $this->vip->where('usuarios_cursos_matriculados.status', 1);
        return $this->vip->get();
    }

    public function gera_autenticacao_certificado_imp_vip($id_certificado) {
        $this->vip->select('*');
        $this->vip->from('usuarios_cursos_matriculados');
        $this->vip->join('cedoc_doc', 'usuarios_cursos_matriculados.cedoc_doc_id_fk = cedoc_doc.cedoc_doc_id');
        $this->vip->join('certificados_impressos', 'usuarios_cursos_matriculados.usuarios_cursos_matriculados_id = certificados_impressos.historico');
        $this->vip->where('usuarios_cursos_matriculados.status', 1);
        $this->vip->where('certificados_impressos.id', $id_certificado);
        $this->vip->where('certificados_impressos.status', 1);
        $this->vip->where('certificados_impressos.pagamento', 3);
        return $this->vip->get();
    }

    public function gera_autenticacao_certificado_vip_by_cod_autenticacao($matricula) {
        $this->vip->select('*');
        $this->vip->from('usuarios_cursos_matriculados');
        $this->vip->join('cedoc_doc', 'usuarios_cursos_matriculados.cedoc_doc_id_fk = cedoc_doc.cedoc_doc_id');
        $this->vip->where('usuarios_cursos_matriculados.UCMCertificadoLiberado', $matricula);
        $this->vip->where('usuarios_cursos_matriculados.status', 1);
        return $this->vip->get();
    }

    public function gera_autenticacao_certificado_impresso_vip_by_cod_autenticacao($matricula) {
        $this->vip->select('*');
        $this->vip->from('usuarios_cursos_matriculados');
        $this->vip->join('cedoc_doc', 'usuarios_cursos_matriculados.cedoc_doc_id_fk = cedoc_doc.cedoc_doc_id');
        $this->vip->join('certificados_impressos', 'usuarios_cursos_matriculados.usuarios_cursos_matriculados_id = certificados_impressos.historico');
        $this->vip->where('usuarios_cursos_matriculados.status', 1);
        $this->vip->where('usuarios_cursos_matriculados.UCMCertificadoLiberado', $matricula);
        $this->vip->where('certificados_impressos.status', 1);
        $this->vip->where('certificados_impressos.pagamento', 3);
        return $this->vip->get();
    }

    public function _get_historico($d = null) {
        return $this->db->get_where('aluno_curso', $d, 1);
    }

    public function get_historico_by_id($d = null) {
        return $this->db->get_where('historico', array('id' => $d), 1);
    }

    public function get_historico_by_id_historico($d = null) {
        return $this->db->get_where('historico', array('id_historico' => $d), 1);
    }

    public function get_declaracao($d = null) {
        $this->db->select('historico.id, id_historico, aluno.email as email, aluno.rg as rg, nota, curso.nome as nomeCurso, aluno.nome as nomeAluno, historico.carga_horaria, primeiro_dia, data_aprovacao, descricao, situacao, aluno.id_fic, historico.codigo_autenticacao as codigo_autenticacao');
        $this->db->from('historico');
        $this->db->join('curso', 'curso.id_curso = historico.id_curso');
        $this->db->join('aluno', 'aluno.email = historico.email');
        $this->db->where(array('historico.id_aluno' => $this->session->userdata('imazon_id_fic'), 'historico.id_historico' => $d, 'historico.situacao' => 1));
        $this->db->order_by("historico.id asc");
        $this->db->limit(1);
        return $this->db->get();
    }

    public function get_autenticacao_declaracao($d = null) {
        $this->db->select('historico.id, id_historico, aluno.email as email, aluno.rg as rg, nota, curso.nome as nomeCurso, aluno.nome as nomeAluno, historico.carga_horaria, primeiro_dia, data_aprovacao, descricao, situacao, aluno.id_fic, historico.codigo_autenticacao as codigo_autenticacao');
        $this->db->from('historico');
        $this->db->join('curso', 'curso.id_curso = historico.id_curso');
        $this->db->join('aluno', 'aluno.email = historico.email');
        $this->db->where('historico.id_historico', $d);
        $this->db->order_by("historico.id asc");
        $this->db->limit(1);
        return $this->db->get();
    }

    public function _update_historico_by_id_aluno_curso($d = null) {

        $data = array(
            'ultimo_dia' => date('Y-m-d'),
            'ultima_hora' => date('h:m:s'),
            'concluido' => 1
        );

        $this->db->where(array('email' => $this->session->userdata('imazon_email_aluno'), 'id_aluno_curso' => $d['id_aluno_curso'], 'nota' => $d['nota']));
        return $this->db->update('aluno_curso', $data);
    }

    public function atualiza_historico($historico, $dados) {
        return $this->db->limit(1)->order_by('historico.id desc')->update('historico', $dados, array("id_historico" => $historico));
    }

    public function update_num_cliques_by_id_aluno_curso($d = null) {

        $this->db->where('id_aluno_curso', $d);
        $r = $this->db->get('historico')->result();
        $cliques = $r[0]->num_cliques;
        $cliques++;

        $data = array('num_cliques' => $cliques);

        $this->db->where(array('historico.id_aluno' => $this->session->userdata('imazon_id_fic'), 'id_aluno_curso' => $d));
        $this->db->update('historico', $data);
    }

    public function remove_historico($historico) {
        $this->db->where(array('historico.id_aluno' => $this->session->userdata('imazon_id_fic'), 'historico.id_historico' => $historico));
        return $this->db->update('historico', array('situacao' => 5));
    }

    public function congela_historico($historico) {
        $this->db->where(array('historico.id_aluno' => $this->session->userdata('imazon_id_fic'), 'historico.id_historico' => $historico));
        return $this->db->update('historico', array('situacao' => 8));
    }

    public function get_dados_carrinho_by_id_historico($d = null) {
        $this->db->from('historico');
        $this->db->join('curso', 'curso.id_curso = historico.id_curso');
        $this->db->where(array('historico.id_aluno' => $this->session->userdata('imazon_id_fic'), 'historico.id_historico' => $d));
        $this->db->order_by("id desc");
        $this->db->limit(1);
        return $this->db->get();
    }

}
