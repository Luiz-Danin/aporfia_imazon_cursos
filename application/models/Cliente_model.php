<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Cliente_model extends CI_Model
{
    public function __construct() {
        parent::__construct();
        $this->imazon = $this->load->database('imazon', TRUE);
        $this->imazon10 = $this->load->database('imazon10', TRUE);
        $this->vip = $this->load->database('vip', TRUE);
    }
    
    private function get_percentual_bonificacao_cliente($id_fic)
    {
        $this->db->select('percentual_bonificacao');
        $this->db->where('id_fic', $id_fic);
        
        return  $this->db->get('idq_cliente')->result_array();
    }
    
    private function is_log_bonificacao($transacao)
    {
        $this->db->select('transacao');
        $this->db->where('transacao', $transacao);
        $transacao_bonificacao =  $this->db->get('idq_log_bonificacao')->result_array();
        
        if( count($transacao_bonificacao)>0 )
        {
           return true;
        }
        else
        {
           return false;
        }
    }

    public function set_bonificacao($transacao)
    {
        $is_log_bonificacao = $this->is_log_bonificacao($transacao);
        
        if( $is_log_bonificacao==false )
        {
            $this->db->select();
            $this->db->where('transacao', $transacao);
            $this->db->where('tipo_pagamento !=', '66');
            $pedido =  $this->db->get('pedido')->result_array();

            if (count($pedido) == 1)
            {
                $valor_transacao = $pedido[0]['valor'];
                
                $bonificacao_cliente = $this->get_percentual_bonificacao_cliente($pedido[0]['id_aluno']);
                
                if(!empty($bonificacao_cliente) ){
                
                $percentual_bonificacao = $bonificacao_cliente[0]['percentual_bonificacao'];
                $percentual_bonificacao = floatval($percentual_bonificacao);

                $valor_transacao = floatval($valor_transacao);

                $bonificacao = $valor_transacao*($percentual_bonificacao/100);
                
                $saldo = number_format($bonificacao, 2);
                
                if($pedido[0]['id_aluno'])
                {
                    $this->db->select();
                    $this->db->where('id_fic', $pedido[0]['id_aluno']);
                    $cliente =  $this->db->get('idq_cliente')->result_array();

                    if($cliente[0]['id'])
                    {
                        $this->db->select();
                        $this->db->where('id_fic', $cliente[0]['id_fic'] );
                        $aluno =  $this->db->get('aluno')->result_array();

                        if( $aluno[0]['email'])
                        {
                            $this->db->select();
                            $this->db->where('email_indicado', $aluno[0]['email'] );
                            $this->db->where('situacao', 1 );
                            $indicacao =  $this->db->get('idq_indicacao')->result_array();
                            
                            if($indicacao[0]['id_cliente'])
                            {
                                $this->db->select();
                                $this->db->where('id', $indicacao[0]['id_cliente']);
                                $this->db->where('situacao', 1);
                                $indicacao_cliente =  $this->db->get('idq_cliente')->result_array();
                            }
                        }
                    }
                }

               $id_cliente = $indicacao_cliente[0]['id'];
               $idq_log_bonificacao = array( 'id_cliente'=>$id_cliente, 'data'=>date("Y-m-d H:i:s"), 
                                                'valor'=>$saldo, 'previsao_resgate'=>date('Y-m-d H:i:s', strtotime(' + 20 days') ) ,'transacao'=>$transacao, 'tipo_log_bonificacao'=>0 ); 

               $this->db->insert('idq_log_bonificacao',$idq_log_bonificacao);
               
            } else{
                return false;
            }  
               
            }
            else
            {
                return false;
            }
        }
    }
    
    public function get_bonificacao_cliente($id_fic)
    {
        $this->db->select('saldo');
        $this->db->where('id_fic', $id_fic);
        
        return $this->db->get('idq_cliente')->result_array();
    }
    
    public function is_transacao($transacao)
    {
        $this->db->select('transacao');
        $this->db->where('id_fic', $transacao);
        $transacao_pedido = $this->db->get('pedido')->result_array();
        
        if ( count($transacao_pedido)>0 )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    public function get_log_saldo_compra($id_transacao)
    {
        $this->db->select('id');
        $this->db->where('transacao', $id_transacao );
        $id =  $this->db->get('idq_log_saldo_compra')->result_array();
        
        return $id[0]['id'];
    }
}