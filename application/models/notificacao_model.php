<?php

class notificacao_model extends CI_Model{
    
    public function __construct() {
        parent::__construct();
    }
    
    public function verifica_dados_aluno() {
        if ($this->session->userdata('imazon_logado')) {
            
            $aluno = $this->db->get_where('aluno', array('id_aluno' => $this->session->userdata('imazon_id_aluno')), 1)->result();
            
            if (strlen($aluno[0]->ddd_telefone)<2)return false;
            if (strlen($aluno[0]->telefone)<8) return false;
            if (strlen($aluno[0]->telefone)>9) return false;
            if (strlen($aluno[0]->endereco)<2) return false;
            if (strlen($aluno[0]->numero)<1) return false;
            if (strlen($aluno[0]->bairro)<3) return false;
            if (strlen($aluno[0]->cidade)<2) return false;
            if (strlen($aluno[0]->estado)<2) return false;
            if (strlen($aluno[0]->cep)<8) return false;
            return true;
        }
        
    }
    
    public function verifica_dados_contato() {
        if ($this->session->userdata('imazon_logado')) {
            $aluno = $this->db->get_where('aluno', array('id_aluno' => $this->session->userdata('imazon_id_aluno')), 1)->result();
            if (strlen($aluno[0]->ddd_telefone)<2)return false;
            if (strlen($aluno[0]->telefone)<8) return false;
            if (strlen($aluno[0]->telefone)>9) return false;
            //if (strlen($aluno[0]->endereco)<6) return false;
            //if (strlen($aluno[0]->numero)<1) return false;
            //if (strlen($aluno[0]->bairro)<3) return false;
            //if (strlen($aluno[0]->cidade)<2) return false;
            //if (strlen($aluno[0]->estado)<2) return false;
            //if (strlen($aluno[0]->cep)<8) return false;
            return true;
        }
        
    }
    
    public function notifica_faturas_abertas(){
        if ($this->session->userdata('imazon_logado')) {
            $pedidos = $this->db->get_where('pedido', array('id_aluno'=>$this->session->userdata('imazon_id_aluno'), 'situacao'=>0))->result();
            if(count($pedidos)>=1){
                return false;
            }else{
                return true;
            }
        }
    }
    
}
