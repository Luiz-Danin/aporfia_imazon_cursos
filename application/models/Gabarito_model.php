<?php

class Gabarito_model extends CI_Model{
    
    public function __construct() {
        parent::__construct();
    }

    public function add($gabarito){
        return $this->db->insert('gabarito', $gabarito);
    }
    
    
}
