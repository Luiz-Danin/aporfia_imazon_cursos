<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class cursos_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->vip = $this->load->database('vip', TRUE);
    }

    public function mais_vendidos_home(){
        return $this->db->from("curso")->join("historico", "historico.id_curso = curso.id_curso")->where("historico.situacao != 0")->group_by("curso.id_curso")->limit(15)->get()->result();
    }
    
    public function todos_cursos_home($params = null) {
        $params['status'] = 1;

        if ($params['inicio'] !== null && $params['limite'] !== null) {
            return $this->db->select('id_curso, nome, descricao, carga_horaria, imagem')->where('status', 1)->limit($params['limite'], $params['inicio'])->get('curso')->result();
        } else {
            return $this->db->select('id_curso, nome, descricao, carga_horaria, imagem')->limit(5, rand(1, 99))->get_where('curso', array('status' => $params['status']))->result();
        }
    }

    public function count_todos_cursos_home($params = null) {
        $params['status'] = 1;
        return $this->db->select('id_curso')->where($params)->get('curso')->result();
    }

    public function todos_cursos_busca($params = null) {
        if ($params['nome'] !== null) {
            $var = $params['nome'];
            return $this->db->query("SELECT * FROM  curso WHERE status=1 and (nome  like  '%".$params['nome']."%' or descricao like '%".$params['nome']."%') LIMIT 30")->result();
        }
    }
	
	
	 public function busca_cursos_ajax($params = null) {
        if ($params['nome'] !== null) {
            $var = $params['nome'];
            return $this->db->query("SELECT id_curso, nome FROM  curso WHERE status=1 and (nome  like  '%".$params['nome']."%') LIMIT 30")->result();
        }
    }

    public function count_todos_cursos_busca($params = null) {
        return $this->db->select('id_curso')->where('status', 1)->like('nome', $params['nome'])->or_like('descricao', $params['nome'])->get('curso')->result();
    }

    public function todos_cursos_areas($params = null) {
        $params['status'] = 1;
        if ($params['nome'] !== null) {
            return $this->db->select('id_curso, nome, descricao, carga_horaria, imagem')->where('status', 1)->like('nome', $params['nome'])->limit(14)->get('curso')->result();
        }
    }

    public function count_todos_cursos_areas($params = null) {
        $params['status'] = 1;
        return $this->db->select('id_curso')->where($params)->get('curso')->result();
    }

    public function get_curso_by_id($id = null) {
        return $this->db->get_where('curso', array('id_curso' => $id), 1)->result();
    }

    public function get_descricao_cursos_by_nome($nome = null) {
        return $this->db->select('id_curso, nome, descricao, imagem')->get_where('curso', array('nome' => $nome, 'status' => 1), 1)->result();
    }

    public function ultimo_curso_matriculado(){
        $this->db->from('historico');
        $this->db->where('id_aluno', $this->session->userdata('imazon_id_fic'));
        return $this->db->order_by('historico.id desc')->limit(1)->get();
    }
    
    public function count_todos_cursos_aluno_home($params = null) {
        $this->db->where_not_in('historico.situacao', array(5,6,7,8));
        $this->db->from('historico');
        $this->db->join('curso', 'curso.id_curso = historico.id_curso');
        $this->db->where('historico.id_aluno', $this->session->userdata('imazon_id_fic'));
        //$this->db->group_by('historico.id_historico');
        return count($this->db->get()->result());
    }

    //cursos do aluno
    public function todos_cursos_aluno_home($params = null) {
        $this->db->from('curso')->select('curso.id_curso as id_curso, id_historico, nome, historico.carga_horaria, imagem, situacao, historico.primeiro_dia as data_matricula, historico.id as matricula')
                     ->join('historico', 'curso.id_curso = historico.id_curso');
        $this->db->where_not_in('historico.situacao', array(5,6,7,8));
        $this->db->where('historico.id_aluno', $this->session->userdata('imazon_id_fic'));
        //$this->db->group_by('historico.id_historico');
        $this->db->order_by('historico.id desc');
        if ($params['inicio'] !== null && $params['limite'] !== null) {
            $this->db->limit($params['limite'], $params['inicio']);
        } else {
            $this->db->limit($params['limite']);
        }
        return $this->db->get()->result();
    }
   
    public function get_aulas_vip($curso = null){
        return $this->vip->get_where("cursos_aulas", array("cedoc_doc_id_fk"=>$curso),12)->result();
    }

}
