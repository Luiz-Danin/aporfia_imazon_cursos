<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class usuarios_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->imazon = $this->load->database('imazon', TRUE);
        $this->imazon10 = $this->load->database('imazon10', TRUE);
        $this->vip = $this->load->database('vip', TRUE);
    }

    public function add_auditoria($aluno) {
        return $this->db->insert("auditoria", $aluno);
    }

    public function finaliza_auditoria() {
        $this->db->where('session_id', $this->session->userdata('session_id'));
        return $this->db->update('auditoria', array("hora_saida" => date("h:i:s")));
    }

    public function historico($email) {
        $this->imazon->select("*");
        $this->imazon->where('historico.nota >=', 5);
        $this->imazon->where('email', $email);
        $this->imazon->join('curso', 'curso.id_curso = historico.id_curso');
        $query = $this->imazon->get('historico');
        return $query;
    }
 
    public function historico_imazon10($email) {
        $this->imazon10->select("*");
        $this->imazon10->where('historico.nota >=', 5);
        $this->imazon10->where('email', $email);
        $this->imazon10->join('curso', 'curso.id_curso = historico.id_curso');
        $query = $this->imazon10->get('historico');
        return $query; 
    }

    //lista dados por email
    public function get_by_email($email) {
        $this->db->where('email', $email);
        $this->db->limit(1);
        $query = $this->db->get('aluno');
        return $query;
    }

    //aqui
    //lista dados por id_fic
    public function get_by_id_fic($id_fic) {
        return $this->db->where('id_fic', $id_fic)->limit(1)->get('aluno');
    }

    //lista dados por email
    public function get_by_nome($nome) {
        $this->db->where('nome', $nome);
        $query = $this->db->get('aluno');
        return $query;
    }
    
    public function get_aluno_by_nome($nome) {
        $this->db->select('nome, id_fic')
                ->order_by('id_aluno desc')
                ->where('nome', $nome);
        $query = $this->db->get('aluno');
        return $query;
    }

    public function get_aluno() {
        $this->db->where('id_fic', $this->session->userdata('imazon_id_fic'));
        $this->db->limit(1);
        $query = $this->db->get('aluno');
        return $query;
    }

    public function get_endereco_aluno() {
        $this->db->select('email, endereco, complemento, bairro, cidade, estado, cep, numero');
        $this->db->where('id_fic', $this->session->userdata('imazon_id_fic'));
        $this->db->limit(1);
        $query = $this->db->get('aluno');
        return $query;
    }

    //anterior
    public function __validar_email($data) {

        if (count($this->db->where("email", $data)->or_where("id_fic", $data)->get("aluno")->result()) == 1) {
            return true;
        } else if (count($this->imazon->get_where("aluno", array("email" => $data), 1)->result()) == 1 
                && count($this->imazon10->get_where("aluno", array("email" => $data), 1)->result()) == 1 
                && count($this->db->get_where("aluno", array("email" => $data), 1)->result()) == 0) {

            $dadosImazon   = $this->imazon->get_where("aluno", array("email" => $data), 1)->result();
            $dadosImazon10 = $this->imazon10->get_where("aluno", array("email" => $data), 1)->result();

            if($dadosImazon[0]->bloqueado == 1 || $dadosImazon10[0]->bloqueado == 1){
                echo "Erro login bloqueado";
                exit;
            }
            
            $dados = array('id_fic' => $dadosImazon[0]->email,
                'nome' => $dadosImazon[0]->nome,
                'apelido' => '',
                'email' => $dadosImazon[0]->email,
                'senha' => $dadosImazon[0]->senha,
                //'telefone' => $dadosImazon[0]->telefone,
                'endereco' => $dadosImazon[0]->endereco,
                'numero' => $dadosImazon[0]->numero,
                'complemento' => $dadosImazon[0]->complemento,
                'bairro' => $dadosImazon[0]->bairro,
                'cidade' => $dadosImazon[0]->cidade,
                'estado' => $dadosImazon[0]->estado,
                'cep' => $dadosImazon[0]->cep,
                'codigo_alfa' => md5($dadosImazon[0]->email),
                'situacao_aluno' => 1,
                'data_cadastro' => date("Y-m-d h:m:s"),
                'origem' => 'Imazon');

            $this->db->insert('aluno', $dados);

            if (count($this->db->get_where("aluno", array("email" => $data), 1)->result()) == 1) {
                return true;
            }
        } else {
            return false;
        }
    }
    
    //novo
    public function validar_email($data) {
        
        $check_imazon = count($this->db->where("email", $data)->or_where("id_fic", $data)->get("aluno")->result());
        
        if ($check_imazon>=1) {
            return true;
        } else if (count($this->imazon->get_where("aluno", array("email" => $data), 1)->result()) == 1){ 
         

            $dadosImazon   = $this->imazon->get_where("aluno", array("email" => $data), 1)->result();
         
            if($dadosImazon[0]->bloqueado == 1){
                echo "Erro login bloqueado";
                exit;
            }
            
            $dados = array('id_fic' => $dadosImazon[0]->email,
                'nome' => $dadosImazon[0]->nome,
                'apelido' => '',
                'email' => $dadosImazon[0]->email,
                'senha' => $dadosImazon[0]->senha,
                //'telefone' => $dadosImazon[0]->telefone,
                'endereco' => $dadosImazon[0]->endereco,
                'numero' => $dadosImazon[0]->numero,
                'complemento' => $dadosImazon[0]->complemento,
                'bairro' => $dadosImazon[0]->bairro,
                'cidade' => $dadosImazon[0]->cidade,
                'estado' => $dadosImazon[0]->estado,
                'cep' => $dadosImazon[0]->cep,
                'codigo_alfa' => md5($dadosImazon[0]->email),
                'situacao_aluno' => 1,
                'data_cadastro' => date("Y-m-d h:m:s"),
                'origem' => 'Imazon');

            $this->db->insert('aluno', $dados);

            if (count($this->db->get_where("aluno", array("email" => $data), 1)->result()) == 1) {
                return true;
            }
        } else {
            return false;
        }
    }

    public function buscar_certificado_imazon_vip($data) {
        $this->vip->select('*');
        $this->vip->from('usuarios_cursos_matriculados');
        $this->vip->join('cedoc_doc', 'usuarios_cursos_matriculados.cedoc_doc_id_fk = cedoc_doc.cedoc_doc_id');
        $this->vip->where('usuarios_cursos_matriculados.status', 1);
        $this->vip->where('usuarios_cursos_matriculados.email', $data);
        return $this->vip->get();
    }
    
    public function buscar_certificado_impresso_imazon_vip($data) {
        $this->vip->select('*');
        $this->vip->from('usuarios_cursos_matriculados');
        $this->vip->join('cedoc_doc', 'usuarios_cursos_matriculados.cedoc_doc_id_fk = cedoc_doc.cedoc_doc_id');
        $this->vip->join('certificados_impressos', 'usuarios_cursos_matriculados.usuarios_cursos_matriculados_id = certificados_impressos.historico');
        $this->vip->where('usuarios_cursos_matriculados.status', 1);
        $this->vip->where('certificados_impressos.status', 1);
        $this->vip->where('certificados_impressos.pagamento', 3);
        $this->vip->where('certificados_impressos.email', $data);
        $this->vip->where('certificados_impressos.dt_envio <=', '2016/04/26 00:00:00');
        return $this->vip->get();
    }

    //verifica e valida login de usu�rio
    public function verifica_login($dados) {

        if ($this->verifica_email_cadastrado($dados['email'])) {
            $this->db->where('email', $dados['email']);
            $this->db->where('senha', $dados['senha']);
            $this->db->from('aluno');
            $this->db->limit(1);
            $query = $this->db->count_all_results();
            return $query;
        } else {
            if ($this->validar_email($dados['email'])) {
                $this->db->where('email', $dados['email']);
                $this->db->where('senha', $dados['senha']);
                $this->db->from('aluno');
                $this->db->limit(1);
                $query = $this->db->count_all_results();
                return $query;
            } else {
                return false;
            }
        }
    }

    public function atualizaEmail($d = null) {
        $this->db->where('id_fic', $this->session->userdata('imazon_id_fic'));
        if ($this->db->update('aluno', $d)) {
            return true;
        } else {
            return false;
        }
    }

    public function atualizaSenha($senha_atual, $senha_nova) {
        $this->db->where('id_fic', $this->session->userdata('imazon_id_fic'));
        $aluno = $this->db->get('aluno')->result();
        if (count($aluno) == 1) {
            if ($aluno[0]->senha == trim($senha_atual)) {
                $dados = array("senha" => $senha_nova);
                $this->db->where('id_fic', $this->session->userdata('imazon_id_fic'));
                if ($this->db->update('aluno', $dados)) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
        exit;
    }

    public function autentica_certificado($id_fic = null, $tipo_consulta = null) {
        $this->db->select("historico.situacao as situacao, historico.id as codigo, historico.id_historico, curso.nome, codigo_autenticacao, aluno.nome as nomeAluno")
                 ->from("historico")
                 ->join("aluno", 'aluno.id_fic = historico.id_aluno')
                 ->join("curso", 'historico.id_curso = curso.id_curso');
        if($tipo_consulta=="nome"){
            $this->db->where("historico.id_aluno", $id_fic);
        }else{
            $this->db->where("historico.codigo_autenticacao", $id_fic);
        }
        return $this->db->where_in("historico.situacao", array(1, 2, 3, 4, 8))->get()->result();  
    }

    public function verifica_email_cadastrado($email) {
        if (count($this->db->get_where("aluno", array("email" => $email))->result()) == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function verifica_email_cadastrado2($email) {
        $r = count($this->db->where("email", $email)->or_where("id_fic", $email)->get("aluno")->result());
        if ($r == 0) {
            return true;
        } else {
            return false;
        }
    }

    public function rec_senha_email($email) {

        $e = $this->db->get_where("aluno", array("email" => $email));
        $i = count($e);

        if ($i == 1) {
            return $e;
        } else {
            return false;
        }
    }

    public function add($usuario = null) {
        if ($usuario !== null && count($this->db->get_where("aluno", array("id_fic" => $usuario['email']), 1)->result()) == 0) {
            if ($this->db->insert('aluno', $usuario)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    //atualiza dados do usu�rio
    public function update($d = null) {
        $this->db->where('id_fic', $this->session->userdata('imazon_id_fic'));
        if ($this->db->update('aluno', $d)) {
            return true;
        } else {
            return false;
        }
    }

}
