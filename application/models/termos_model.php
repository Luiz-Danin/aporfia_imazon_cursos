<?php

class termos_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
    
    public function get_termo_valido(){
        return $this->db->where("status", 1)->limit(1)->get('termo');
    }
    
    public function get_ultimo_termo_usuario($aluno){
        return $this->db->order_by("id_aluno_termo desc")->limit(1)->get_where('aluno_termo', array("id_aluno"=>$aluno));
    }
    
    public function add_termo_aluno($termo){
        return $this->db->insert("aluno_termo", $termo);
    }
    
}
