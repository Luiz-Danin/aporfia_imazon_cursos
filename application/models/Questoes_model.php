<?php

class Questoes_model extends CI_Model{
    
    public function __construct() {
        parent::__construct();
    }

    public function get_all_by_curso($curso, $id_avaliacao){
        return $this->db->get_where('questao', array('id_curso'=>$curso, 'id_avaliacao'=>$id_avaliacao))->result();
    }
    
    
}
