<?php
class base_legal_model extends CI_Model{
    
     public function __construct() {
        parent::__construct();
    }
    
    public function get_atual(){
        return $this->db->get_where('base_lega', array('situacao'=>1), 1);
    } 
    
    public function get_by_id($id=null){
        return $this->db->get_where('base_lega', array('id_base_lega'=>$id), 1);
    } 
    
}
