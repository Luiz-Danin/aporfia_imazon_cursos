<?php

class idiomas_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
    
     function get_fatura_id($id_aluno)
    {
	     $this->db->where('pedido.situacao', '3');           
             $this->db->where('pedido.transacao', $id_aluno);
	     return $this->db->get('pedido')->result();
    }
    
    function get_all_ingles($id_aluno)
    {
	     $this->db->where('pedido.situacao', '3'); 
             $this->db->join("item_pedido","item_pedido.transacao =pedido.transacao"); 
             $this->db->where('pedido.id_aluno', $id_aluno);
	     $this->db->where('item_pedido.tipo_item', '31'); 
             return $this->db->get('pedido')->result();
    }
    
    function get_all_espanhol($id_aluno)
    {
	     $this->db->where('pedido.situacao', '3'); 
             $this->db->join("item_pedido","item_pedido.transacao =pedido.transacao"); 
             $this->db->where('pedido.id_aluno', $id_aluno);
	     $this->db->where('item_pedido.tipo_item', '32'); 
             return $this->db->get('pedido')->result();
    }
    
    
    function get_ingles_n_usada($id_aluno)
    {
             $this->db->where('pedido.situacao', '3'); 
             $this->db->join("item_pedido","item_pedido.transacao =pedido.transacao"); 
             $this->db->join("curso_idioma_historico","curso_idioma_historico.id_transacao=pedido.transacao");             
             $this->db->where('pedido.transacao', $id_aluno);
	     $this->db->where('item_pedido.tipo_item', '31'); 
             return $this->db->get('pedido')->result();
    }
    
    function get_espanhol_n_usada($id_aluno)
    {
             $this->db->where('pedido.situacao', '3'); 
             $this->db->join("item_pedido","item_pedido.transacao =pedido.transacao"); 
             $this->db->join("curso_idioma_historico","curso_idioma_historico.id_transacao=pedido.transacao");             
             $this->db->where('pedido.transacao', $id_aluno);
	     $this->db->where('item_pedido.tipo_item', '32'); 
             return $this->db->get('pedido')->result();
    }
    
    function matricula_ingles($id_aluno)
    {
             $this->db->where('pedido.situacao', '3'); 
             $this->db->join("item_pedido","item_pedido.transacao =pedido.transacao"); 
             $this->db->join("curso_idioma_historico","curso_idioma_historico.id_transacao=pedido.transacao");             
             $this->db->where('pedido.transacao', $id_aluno);
	     $this->db->where('item_pedido.tipo_item', '31'); 
             return $this->db->get('pedido')->result();
    }
    
     function matricula_espanhol($id_aluno)
    {
             $this->db->where('pedido.situacao', '3'); 
             $this->db->join("item_pedido","item_pedido.transacao =pedido.transacao"); 
             $this->db->join("curso_idioma_historico","curso_idioma_historico.id_transacao=pedido.transacao");             
             $this->db->where('pedido.transacao', $id_aluno);
	     $this->db->where('item_pedido.tipo_item', '32'); 
             return $this->db->get('pedido')->result();
    }
    
    function verifica_permissao($id_curso,$id_aluno)
    {        
             $this->db->join("pedido","curso_idioma_historico.id_transacao =pedido.transacao"); 
             $this->db->where('pedido.situacao','3');	
             $this->db->where('curso_idioma_historico.id_curso',$id_curso);	   
             $this->db->where('curso_idioma_historico.id_aluno',$id_aluno);	          
             $this->db->where('curso_idioma_historico.situacao',1);	     
             return $this->db->get('curso_idioma_historico')->result();
    }
    
    function insere_aluno_turma($data)
    {
    $this->db->insert('curso_idioma_historico', $data);           
    }
    
}
