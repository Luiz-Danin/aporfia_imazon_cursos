<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
    

class posts_model extends CI_Model{
   
    public function __construct() {
        parent::__construct();
    }
    
    
    public function get_by_page_id($id=null){
        return $this->db->get_where('post_blog', array('page_id'=>$id),1)->result();
    }
    
    public function todos_posts_publicados($param = null){
        $param['status'] = 1;
        
        $this->db->order_by('id desc');
        if($param['inicio'] !== null && $param['limite'] !== null ){
            return $this->db->select('id, titulo, conteudo, autor, page_id')->where('status', 1)->limit($param['limite'], $param['inicio'])->get('post_blog')->result();
        }else{
            return $this->db->select('id, titulo, conteudo, autor, page_id')->where('status', 1)->limit(6)->get('post_blog')->result();
        }
    }
    
    public function count_todos_posts_publicados($param=null){
        $param['status'] = 1;
        return $this->db->select('id')->where($param)->get('post_blog')->result();
    }
    
    ///////
    public function todos_posts_por_categoria($param = null){
        $param['status'] = 1;
        $this->db->order_by('id desc');
        if($param['inicio'] !== null && $param['limite'] !== null ){
            return $this->db->select('id, titulo, conteudo, autor, page_id')->where('categoria', $param['categoria'])->limit($param['limite'], $param['inicio'])->get('post_blog')->result();
        }else{
            return $this->db->select('id, titulo, conteudo, autor, page_id')->where('categoria',$param['categoria'])->limit(6)->get('post_blog')->result();
        }
    }
    
    public function count_todos_posts_por_categoria($param=null){
        $param['status'] = 1;
        return $this->db->from('post_blog')->where($param)->count_all_results();
    }
  
     public function todos_posts_busca($param=null){
        $param['status'] = 1;
        $this->db->order_by('id desc');
        if($param['titulo'] !== null){
            return $this->db->select('id, titulo, conteudo, dt_cadastro, page_id')->where('status', 1)->like('titulo', $param['titulo'])->get('post_blog')->result();
        }
        
    }
    
    public function count_todos_posts_mais_visitados($param=null){
        $param['status'] = 1;
        $this->db->select('count(*), post, page_id, titulo, conteudo')->from('post_blog')->join('log_visitas_post','log_visitas_post.post = post_blog.id');
        $this->db->where('status', 1)->group_by('post');
        return $this->db->count_all_results();
    }
    
  
     public function todos_posts_mais_visitados($param=null){
        $param['status'] = 1;
        $this->db->select('count(*) as qtde, post, page_id, titulo, conteudo')->from('post_blog')->join('log_visitas_post','log_visitas_post.post = post_blog.id');
        $this->db->where('status', 1)->group_by('post')->order_by('qtde', 'desc');
        return $this->db->get()->result();
    }
    
    public function count_todos_posts_busca($param=null){
        $param['status'] = 1;
        return $this->db->select('id')->where('titulo',$param['titulo'])->get('post_blog')->result();
    }
    
    public function insert_log_visita($param=null){
        $this->db->insert('log_visitas_post', $param);
    }
    
    public function get_all_categorias_ativas_blog(){
        return $this->db->select('nome')->from('categorias_blog')->order_by('nome')->get()->result();
    }
    
    public function get_id_categoria_by_nome($nome){
        return $this->db->select('id')->from('categorias_blog')->where('nome', urldecode($nome))->limit(1)->get()->result();
    }
    
}
