<style>
    .dropdown{
        background-color: #fff;
        border: medium none;
        float: left;
        margin: 0 0 0 -20px;
        padding: 0;
        width: 222px;
    }
</style>
<div class="row">
    <div class="col-md-12 col-xs-12">
                    <form id="frm_busca_curso" action="<?= base_url('buscar_curso') ?>" method="post">
                        <div class="input-group">
                            <input type="text" class="form-control input-lg" placeholder="Digite uma Palavra" name="busca">
                            <span class="input-group-addon" id="sizing-addon2" onClick="$('#frm_busca_curso').submit();">
                                <i class="glyphicon glyphicon-search" style="cursor:auto"></i>
                            </span>
                        </div>
                    </form>
                </div>
<!--    <div class="col-md-12 col-xs-12">
        <form id="frm_busca_curso"  action="<?= base_url('buscar_curso') ?>" method="post">
            <div class="input-group">
                <input type="text" class="form-control input-lg" name="post" placeholder="Digite uma Palavra" aria-describedby="sizing-addon2">
                <span class="input-group-addon"  onClick="$('#frm_busca_curso').submit();" id="sizing-addon2">
                    <i class="glyphicon glyphicon-search"></i>
                </span>
            </div>
        </form>
    </div>-->
</div>    
<div class="row">
    <div class="col-md-3">
        <div class="conteudo-esquerdo">
            <div class="panel-heading">
                <div class="navbar-header" id="btn-lateral">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#lateral">
                        <span class="glyphicon glyphicon-chevron-right"></span>                   
                    </button> 
                </div>
            </div>
            <div class="collapse navbar-collapse" id="lateral">
                <ul class="nav navbar-nav list-group">
                    <?php
                        foreach ($this->pm->get_all_categorias_ativas_blog() as $cat) {
                            echo '<li class="list-group-item sidebar-list"><a href="' . base_url('categoria') . '/' . urlencode($cat->nome) . '" title="'.$cat->nome.'">' . $cat->nome . '</a></li>';
                        }
                    ?>
                    <li class="list-group-item sidebar-list">
                        <a href="<?= base_url('post') ?>" title="Últimas postagens">Últimas postagens</a>
                    </li>
                    <li class="list-group-item sidebar-list">
                        <a href="<?= base_url('mais_visitadas') ?>" title="Mais visitadas">Mais visitadas</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>