<?php
if ($this->session->flashdata('success')) {
    $tipo = 'success';
}
if ($this->session->flashdata('danger')) {
    $tipo = 'danger';
}
if ($this->session->flashdata('warning')) {
    $tipo = 'warning';
}
if (isset($tipo)) {
    echo '<div class="alert alert-' . $tipo . '">' . $this->session->flashdata($tipo) . '</div>';
}
if ($this->session->flashdata('alerta-carrinho')) {
    echo '<div class="alert alert-info" id="alerta"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>' . $this->session->flashdata('alerta-carrinho') . '</div>';
}
?>

<div class="row">
    <div class="col-md-12 col-xs-12">
        <form action="<?= base_url('buscar_curso') ?>" method="post" id="form-busca">
            <div class="input-group">
                <input type="text" class="form-control input-lg" placeholder="Digite uma Palavra" name="busca" required="" minlength="2">
                <span class="input-group-addon" id="sizing-addon2" onclick="validaFormPesquisa();">
                    <i class="glyphicon glyphicon-search"></i>
                </span>
            </div>
        </form>
    </div>
</div>

<div class="row">
    <div class="col-md-3">
        <div class="conteudo-esquerdo">
            <div class="panel-heading">
                <div class="navbar-header" id="btn-lateral">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#lateral">
                        <span class="glyphicon glyphicon-chevron-right"></span>                   
                    </button> 
                </div>
            </div>
            <div class="collapse navbar-collapse" id="lateral">
                <ul class="nav navbar-nav list-group">
                
                   <!-- <li class="list-group-item sidebar-list">
                        <a target="_blank" href="https://www.idmcursos.com.br/" title="Matricular">Idiomas <i>(Novo)</i></a>
                    </li>-->
                    <li class="list-group-item sidebar-list">
                        <a href="<?= base_url('cursos_por_areas') . '/' . urlencode('Educacao') ?>" title="Educa&ccedil;&atilde;o">Educa&ccedil;&atilde;o </a>
                    </li>
                    <li class="list-group-item sidebar-list">
                        <a href="<?= base_url('cursos_por_areas') . '/' . urlencode('Administracao') ?>" title="Administra&ccedil;&atilde;o">Administra&ccedil;&atilde;o</a>
                    </li>
                    <li class="list-group-item sidebar-list">
                        <a href="<?= base_url('cursos_por_areas') . '/' . urlencode('Gestao') ?>" title="Gestão">Gest&atilde;o</a>
                    </li>
                    <li class="list-group-item sidebar-list">
                        <a href="<?= base_url('cursos_por_areas') . '/' . urlencode('Direito') ?>" title="Direito">Direito </a>
                    </li>
                    <li class="list-group-item sidebar-list">
                        <a href="<?= base_url('cursos_por_areas') . '/' . urlencode('Saude') ?>" title="Saúde">Saúde</a>
                    </li>
                    <li class="list-group-item sidebar-list">
                        <a href="<?= base_url('cursos_por_areas') . '/' . urlencode('AGRÍ') ?>" title="Agricultura">Agricultura</a>
                    </li>
                    <li class="list-group-item sidebar-list">
                        <a href="<?= base_url('cursos_por_areas') . '/' . urlencode('Arte') ?>" title="Arte">Arte</a>
                    </li>
                    <li class="list-group-item sidebar-list">
                        <a href="<?= base_url('cursos_por_areas') . '/' . urlencode('Comercio') ?>" title="Comércio">Comércio </a>
                    </li>
                    <li class="list-group-item sidebar-list">
                        <a href="<?= base_url('cursos_por_areas') . '/' . urlencode('Economia') ?>" title="Economia">Economia</a>
                    </li>
                    <li class="list-group-item sidebar-list">
                        <a href="<?= base_url('cursos_por_areas') . '/' . urlencode('Industria') ?>" title="Indústria">Indústria</a>
                    </li>
                    <li class="list-group-item sidebar-list">
                        <a href="<?= base_url('cursos_por_areas') . '/' . urlencode('Marketing') ?>" title="Marketing">Marketing</a>
                    </li>
                    <li class="list-group-item sidebar-list">
                        <a href="<?= base_url('cursos_por_areas') . '/' . urlencode('Matematica') ?>" title="Matemática">Matem&aacute;tica</a>
                    </li>
                    <li class="list-group-item sidebar-list">
                        <a href="<?= base_url('cursos_por_areas') . '/' . urlencode('Meio Ambiente') ?>" title="Meio Ambiente">Meio Ambiente</a>
                    </li>
                    <li class="list-group-item sidebar-list">
                        <a href="<?= base_url('cursos_por_areas') . '/' . urlencode('Negocios') ?>" title="Negócios">Negócios</a>
                    </li>
                    <li class="list-group-item sidebar-list">
                        <a href="<?= base_url('cursos_por_areas') . '/' . urlencode('Logistica') ?>" title="Logística">Log&iacute;stica</a>
                    </li>
                    <li class="list-group-item sidebar-list">
                        <a href="<?= base_url('cursos_por_areas') . '/' . urlencode('Psicologia') ?>" title="Psicologia">Psicologia</a>
                    </li>
                    <li class="list-group-item sidebar-list">
                        <a href="<?= base_url('cursos_por_areas') . '/' . urlencode('Recursos Humanos') ?>" title="Recursos Humanos">Recursos Humanos</a>
                    </li>
                    <li class="list-group-item sidebar-list">
                        <a href="<?= base_url('cursos_por_areas') . '/' . urlencode('Religiao') ?>" title="Religião">Religião</a>
                    </li>
                    <li class="list-group-item sidebar-list">
                        <a href="<?= base_url('mais_vendidos') ?>" title="Mais vendidos">Mais vendidos</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>