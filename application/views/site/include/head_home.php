<?php echo doctype('html5'); ?>
<html>
    <head>
        <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url("favicon") ?>/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url("favicon") ?>/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url("favicon") ?>/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url("favicon") ?>/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url("favicon") ?>/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url("favicon") ?>/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url("favicon") ?>/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url("favicon") ?>/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url("favicon") ?>/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo base_url("favicon") ?>/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url("favicon") ?>/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url("favicon") ?>/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url("favicon") ?>/favicon-16x16.png">
        <link rel="manifest" href="<?php echo base_url("favicon") ?>/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="<?php echo base_url("favicon") ?>/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
        <meta name="description" content="{titulo}">
        <meta name="keywords" content="{titulo}">
        <meta name="author" content="{palavra_chave} com Certificado">
        <?php echo header("Content-Type: text/html;  charset=utf-8", true); ?>
        <title>{titulo} | {palavra_chave} com Certificado</title>         
        <!-- Latest compiled and minified CSS -->
        <?php echo link_tag('' . base_url() . 'vendor/twbs/bootstrap/dist/css/bootstrap.min.css'); ?>
        <!-- Google fonts -->
        <?php echo link_tag('https://fonts.googleapis.com/css?family=Montserrat'); ?>
        <!--Bootstrap Social Icons-->
        <?php echo link_tag('node_modules/bootstrap-social/bootstrap-social-min.css'); ?>
        <?php echo link_tag('node_modules/bootstrap-social/assets/css/font-awesome.css'); ?>
        <!--Animate Bootstrap Icons-->
        <?php echo link_tag('vendor/components/animate.css/animate.min.css'); ?>
        <!--jQuery-->
        <!--Estilo-->
        <?php echo link_tag('template/css/estilo-min.css'); ?>
        <!--Reset-->
        <?php echo link_tag('template/css/reset-min.css'); ?>
        <?php echo '<script src="' . base_url() . 'vendor/components/jquery/jquery.min.js"></script>'; ?>
        <!-- Latest compiled and minified JavaScript -->
        <?php echo '<script src="' . base_url() . 'vendor/twbs/bootstrap/dist/js/bootstrap.min.js"></script>'; ?>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
        <script src="//oss.maxcdn.com/jquery.form/3.50/jquery.form.min.js"></script>
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-62601651-1', 'auto');
            ga('send', 'pageview');

        </script>
        <?php
        if ($this->session->userdata("rescad")) {
            echo '<!-- Google Code for Cadastros Imazon Conversion Page -->
        <script type="text/javascript">
        /* <![CDATA[ */
        var google_conversion_id = 1036809317;
        var google_conversion_language = "pt";
        var google_conversion_format = "3";
        var google_conversion_color = "ffffff";
        var google_conversion_label = "L5l_CMvn2AEQ5eix7gM";
        var google_conversion_value = 1.00;
        var google_conversion_currency = "BRL";
        var google_remarketing_only = false;
        /* ]]> */
        </script>
        <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
        </script>
        <noscript>
        <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1036809317/?value=1.00&amp;currency_code=BRL&amp;label=L5l_CMvn2AEQ5eix7gM&amp;guid=ON&amp;script=0"/>
        </div>
        </noscript>';
            $this->session->unset_userdata("rescad");
        }
        ?>
    </head>
    <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-N68ZNJP');</script>
        <!-- End Google Tag Manager -->
    <body>
        <header>
            <div class="fundo-home"></div>
            <div class="topo-involucro">
                <?php
                if ($this->agent->is_browser() && $this->agent->browser() == "Internet Explorer") {
                    echo '<script>alert("Nosso site não possui suporte para o seu navegador atual. Por favor, baixe e instale um dos seguintes navegadores: Google Chrome ou Mozila Firefox.")</script>';
                }
                ?>
                <div class="row">
                    <div id="custom-bootstrap-menu" class="navbar navbar-default navbar-fixed-top" role="navigation">
                        <div class="container-fluid">
                            <div class="navbar-header">
                                <a class="navbar-brand" href="#" >
                                    <?php echo img(base_url() . '/imgs/logo.png', TRUE); ?>
                                </a>
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-menubuilder"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                                </button>
                            </div>
                            <div class="collapse navbar-collapse navbar-menubuilder">
                                <ul class="nav navbar-nav navbar-right menu-principal">
                                    <li><a class="page-scroll" href="<?php echo base_url('site/home') ?>">Home</a>
                                    </li>
                                    <li><a class="page-scroll" href="<?php echo base_url('cursos') ?>">Cursos</a>
                                    </li>
                                    <li><a class="page-scroll" href="<?php echo base_url('autenticacao') ?>">Certificado</a>
                                    </li>
                                    <?php
                                    if ($this->session->userdata('imazon_logado')) {
                                        echo '<li><a href="' . base_url('cursos_matriculados') . '">Estudar</a>';
                                    } else {
                                        echo '<li>'
                                        . '<a class="page-scroll" href="#" local="' . base_url() . '"  id="btn-modal-cadastrar" data-toggle="modal" data-target="#modal-cadastro">Cadastro</a>' .
                                        '</li>' .
                                        '<li><a class="page-scroll" href="#" local="' . base_url() . '"  id="btn-modal-cadastrar" data-toggle="modal" data-target="#modal-cadastro">Login</a>';
                                    }
                                    ?>
                                    
                                    <!--<li><a class="page-scroll" href="<?php echo base_url('blog') ?>">Blog</a>-->
                                    <li><a class="page-scroll" href="<?php echo base_url('suporte') ?>">Suporte</a>
                                    </li>
                                    <li>      
                                        <a class="page-scroll" id="busca-topo" href="#busca" data-container="body" data-toggle="popover" data-placement="bottom">
                                            <span>
                                                <i class="glyphicon glyphicon-search"></i>
                                            </span>
                                        </a>
                                    </li>
                                    <?php
                                    if ($this->session->userdata('imazon_logado')) {
                                        echo '<li class="dropdown sidebar-list">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <span class="glyphicon glyphicon-user"></span></a>
                                                <ul class="dropdown-menu">
                                                    <li><a href="' . base_url('dados_cadastrais') . '">Meus dados</a></li>
                                                    <li><a href="' . base_url('logout') . '">Sair</a></li>
                                                </ul>
                                            </li>';
                                    }

                                    if ($this->cart->total_items() > 0) {
                                        echo '<li class="nav-menu-carrinho"><a href="' . base_url("listaCompras") . '"><i class="glyphicon glyphicon-shopping-cart icone-carrinho"></i></a></li>';
                                    }

                                    if ($this->session->userdata('imazon_logado')) {
                                        if (!$this->notificacao_model->verifica_dados_aluno()) {
                                            echo '<li class=""><a href="' . base_url("dados_cadastrais") . '" style="color: orange;" class="notifica-dados-aluno" data-container="body" data-toggle="popover" data-placement="left" data-content="Você ainda não preencheu os seus dados cadastrais!"><i class="glyphicon glyphicon-info-sign"></i></a></li>';
                                        }
                                        if (!$this->notificacao_model->notifica_faturas_abertas()) {
                                            echo '<li class=""><a href="' . base_url('faturas') . '" style="color: red;" class="notifica-dados-aluno" data-container="body" data-toggle="popover" data-placement="left" data-content="Você possui faturas em aberto"><i class="glyphicon glyphicon-usd"></i></a></li>';
                                        }
                                    }
                                    ?>
                                </ul>
                            </div>
                            <?php
                            if ($this->session->userdata('imazon_logado')) {
                                echo '<p style="float: right; font-size: 12px; margin-top: -3px;">Logado como: ' . $this->session->userdata('imazon_nome') . '</p>';
                            }
                            ?>

                        </div>
                    </div>
                </div>       

            </div><!--topo-involucro-->
        </header>