<section>
    <!-- Modal -->
    <div class="modal fade" id="modal-cadastro" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-body modal-body-cadastro">
                </div>
            </div>
        </div>
    </div>
</section>
<footer>
    <div class="rodape-involucro">
        <div class="row">
            <div class="col-md-2 col-lg-3 col-xs-3 rodape-esquerdo">
                <ul>
                    <li><a href="<?= base_url("termo") ?>">Termo de uso</a></li>
                    <li><a href="<?= base_url("autenticacao") ?>">Autenticador</a></li>
                    <li><a id="btn-modal-cadastrar" data-toggle="modal" data-target="#modal-cadastro">Cadastro</a></li>
                </ul>
            </div>
            <div class="col-md-8 col-lg-6 col-xs-6 rodape-centro">
                <ul>
                    <li><a href="<?= base_url("termo") ?>">Copyright &COPY; Imazon Cursos 2007/<?= date("Y") ?></a></li>
                    <li><a href="<?= base_url("termo") ?>">E-mail: atendimento@imazoncursos.com.br</a></li>
                    <li><a href="<?= base_url("termo") ?>">Telefone: 4004-0435 Ramal:8229</a></li>
                </ul>
            </div>
            <div class="col-md-2  col-lg-3 col-xs-2 rodape-direito">
                <ul>
                    <li><a href="<?= base_url("suporte") ?>">Suporte</a></li>
                    <li><a href="<?= base_url("blog") ?>">Blog</a></li>
                    <li><a href="<?= base_url("cursos") ?>">Cursos</a></li>
                </ul>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12" style="padding: 14px 0px 9px 14px;">
                <a href="http://bcash.com.br/" target="_blank">
                    <img src="https://a248.e.akamai.net/f/248/96284/168h/www.bcash.com.br/webroot/banners/site/bc90x95px.png" style="width:65px; margin-right: 1%;">
                </a>    
                <a href="http://lattes.cnpq.br/" target="_blank">
                    <img src="<?= base_url("imgs/cnpq-lattes.jpeg") ?>" style="width:89px; margin-right:1%;"/>
                </a>
                <a href="http://di.cnpq.br/di/cadi/infoInstituicao.do?acao=buscaDadosInst&nroIdInst=744645" target="_blank">
                    <img src="<?= base_url("imgs/cadi.jpg") ?>" style="width:226px; margin-right:1%;"/>
                </a>
                <a href="http://www.abed.org.br/site/pt/associados/consulta_associados_abed/?busca_rapida=cidade+aprendizagem" target="_blank">
                    <img src="<?= base_url("imgs/abed.png") ?>" style="width:102px; margin-right:1%;"/>
                </a>
                <a href="https://cidadeaprendizagem.com.br" target="_blank">
                    <img src="<?= base_url("imgs/logo-cidade.jpg") ?>" style="width:158px; margin-right:1%;"/>
                </a>  
                <a href="https://www.google.com/transparencyreport/safebrowsing/diagnostic/?hl=pt-BR#url=imazoncursos.com.br" target="_blank">
                    <img src="<?= base_url("imgs/icone-google.png") ?>" style="width:140px; margin-right:1%;"/>
                </a>
                <span id="ss_img_wrapper_115-55_image_en"><a href="http://www.alphassl.com/ssl-certificates/wildcard-ssl.html" target="_blank" title="SSL Certificates"><img alt="Wildcard SSL Certificates" border=0 id="ss_img" src="//seal.alphassl.com/SiteSeal/images/alpha_noscript_115-55_en.gif" title="SSL Certificate"></a></span><script type="text/javascript" src="//seal.alphassl.com/SiteSeal/alpha_image_115-55_en.js"></script>
            </div>    
        </div>
    </div>
</div>    
</footer>
</body>
<script src="<?= base_url('template/js/script.js') ?>"></script>
<script>
    $(function () {
        $("#busca-topo").popover({content: '<form class="form-inline" action="<?= base_url('buscar_curso') ?>" method="post">' +
                    '<div class="form-group">' +
                    '<div class="input-group">' +
                    '<input type="text" class="form-control" name="busca" required placeholder="Digite uma Palavra?">' +
                    '</div>' +
                    '</div>' +
                    '<button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-search"></i></button>' +
                    '</form>',
            html: true});
    });
    $(function () {
        $('.btn-topo-busca').on('click', function () {
            $(this).css('display', 'none');
            $('.busca').css('display', 'block');
        });
    });
    $(function () {
        $('.busca').on('mouseleave', function () {
            $(this).css('display', 'none').val('');
            $('.btn-topo-busca').css('display', 'block');
        });
    });
    $(function () {
        $('.busca').on('blur', function () {
            $(this).css('display', 'block');
        })
    });
    function logarExterno(local) {
        var email1 = $("#email").val();
        if (email1.length >= 6) {
            $.post('<?= base_url('verifica_cadastro') ?>', {email: email1}, function (e) {
                if (e == "true") {
                    $('.modal-body-cadastro').html('');
                    $('.modal-body-cadastro').append('<div class="alert alert-success">Por favor, digite a sua senha de acesso</div>' +
                            '<form class="frm-inline frm-login" onsubmit="return false">' +
                            '<div class="form-group">' +
                            '<input type="email" class="form-control email-aluno" id="email-login" name="email" value="' + email1 + '">' +
                            '</div>' +
                            '<input type="hidden" id="fonte" value="' + fonte + '"/>' +
                            '<div class="form-group"><input type="password" class="form-control" id="senha-login" name="senha" placeholder="Digite a sua Senha"></div><button type="submit" class="btn btn-logar btn-logar-home" onclick="logar()">Entrar</button>' +
                            ' <button type="button" class="btn btn-logar btn-recuperar-senha" onclick="recuperar_senha()">Esqueci minha senha</button>' +
                            '</form>');
                } else {
                    $('.frm-inline').removeClass('form-login-chamada').addClass('form-cadastro-chamada');
                    $('.modal-body-cadastro').html('');
                    $('.modal-body-cadastro').append('<div class="alert alert-info">Email válido! Agora digite os seus dados.</div>' +
                            '<form action="<?= base_url('cadastrar') ?>" class="frm-inline frm-cadastro" method="post" onsubmit="validaFormCadastro(); return false;">' +
                            '<div class="form-group form-group-nome">' +
                            '<input type="text" class="form-control" id="nome" required name="nome" placeholder="Digite o seu nome completo">' +
                            '</div>' +
                            '<div class="form-group form-group-email">' +
                            '<input type="email" class="form-control" id="email" min="3" required  name="email" value="' + email1 + '">' +
                            '</div>' +
                            '<div class="form-group form-group-senha1">' +
                            '<input type="password" class="form-control" required min="6" max="12" id="senha" name="senha" placeholder="Crie uma Senha">' +
                            '</div>' +
                            '<div class="form-group form-group-senha2">' +
                            '<input type="password" class="form-control" required min="6" max="12" id="senha2" name="senha2" placeholder="Repita a sua Senha">' +
                            '</div>' +
                            '<input type="hidden" id="fonte" value="' + fonte + '"/>' +
                            '<button type="submit" class="btn btn-success">Cadastrar</button>' +
                            '</form>' +
                            '<p style="font-size:12px; font-weight:bold;">*Ao clicar em cadastrar você estará concordando com nosso <a href="../termo" target="_blank">termo de uso</a></p>'
                            );
                }
            });
        }
    }
</script>
</html>