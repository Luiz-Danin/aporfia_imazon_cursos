
<div class="col-md-9">
    <div class="blank_30"></div>
    <div class="conteudo-direito" id="cursos">
        <?php
        $carga_horarias = $this->pm->get_all_carga_horaria();
        if (!$this->seguranca_model->valida_login_aluno()) {
            $url_matricula = 'redir_cad_matricula';
        } else {
            $url_matricula = 'matricular_curso';
        }

        $btn_matricular = '';

        if (isset($paginacao)) {
            echo $paginacao;
        }
        if ($cursos == null)
            if ($termo !== null) {
                echo '<div class="jumbotron">
                                <h1>Nenhum resultado</h1>
                                <p>No momento nós nao temos nenhum curso de <b>' . $termo . '</b>.</p>
                                <p><a class="btn btn-primary btn-lg" href="' . base_url('cursos') . '" role="button">Voltar</a></p>
                              </div>';
            } else {
                echo '<div class="jumbotron">
                                <h1>Nenhum resultado</h1>
                                <p>No momento nós nao temos nenhum curso nesta área</p>
                                <p><a class="btn btn-primary btn-lg" href="' . base_url('cursos') . '" role="button">Voltar</a></p>
                              </div>';
            } else
            foreach ($cursos as $c) {
                if ($c->imagem !== "") {
                    $img = base_url("imgs/cursos/$c->imagem");
                } else {
                    $img = base_url("imgs/sem-foto.png");
                }

                echo '<div class="media lista-cursos-home">
                                    <div class="media-left media-middle">
                                        <a href="' . base_url('ver_curso/' . urlencode($c->nome)) . '">
                                            <img class="media-object img-curso" src="' . $img . '" alt="' . $c->nome . '">
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <a href="' . base_url('ver_curso/' . urlencode($c->nome)) . '">'
                . '<h4 class="media-heading titulo-curso-home" style="width: 80%;">CURSO ' . $c->nome . '</h4>
                                        </a>										
									 <form action="' . base_url($url_matricula) . '" method="post" onsubmit="return validaFormMatricular(\'' . $c->id_curso . '\')">							 
									 <input type="hidden" name="curso" value="' . codifica($c->id_curso) . '" id="curso-id">
                                                                             <p style="width: 50%; font-size: 14px;" align="left">
                                       
                                       
                                        Tipo: Capacitação/Curso livre
                                       
                                        
                                        <br />
                                        Matrícula: <span class="moeda' . $c->id_curso . '" style="display: none;">R$</span><span class="valor-curso-' . $c->id_curso . '"> A partir de R$9,99.</span>
										<br>
                                                                                Duração: <span class="duracao-dias-' . $c->id_curso . '">de 2 à 90 </span> dias
										<br>
										Certificado GRÁTIS
										<br/>
										Carga horária:  
										<select name="carga_horaria" class="carga_horaria select-carga-horaria-' . $c->id_curso . '" style="width:120px;" required="">
                			';
                echo '<option>Selecione</option>';
                foreach ($carga_horarias as $ch) {

                    if ($ch->tempo == 10) {
                        echo '<option selected curso="' . $c->id_curso . '" duracao="' . $ch->tempo . ' " valor="' . $ch->valor . ' " value="' . codifica($ch->tempo) . '">' . $ch->tempo . ' horas </option>';
                    } else {
                        echo '<option curso="' . $c->id_curso . '" duracao="' . $ch->tempo . ' " valor="' . $ch->valor . ' " value="' . codifica($ch->tempo) . '">' . $ch->tempo . ' horas </option>';
                    }
                }


                echo' </p>
							    </select>
								<br><br> 
										<input type="submit" value="Matrícular" class="btn btn-success ' . $btn_matricular . '" style="float: left; margin-left: 10px;" data-id="' . $c->id_curso . '"> 
								                                        
                                        <span style="float:rigth; margin-left:10px; clear: both; font-size: 12px;" role="button" data-toggle="collapse" href="#' . md5($c->nome) . '" aria-expanded="false" aria-controls="' . md5($c->nome) . '">+ Detalhes</span>                           
                                    </div>
                                    <br />
									
                                     <div class="collapse" id="' . md5($c->nome) . '">
                                        <div class="panel-group" id="accordion-' . md5($c->nome) . '" role="tablist" aria-multiselectable="true">
				

                     <div class="panel panel-default panel-visualiza-curso">
                        <div class="panel-heading" role="tab" id="headOne-' . md5($c->nome) . '">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion-' . md5($c->nome) . '" href="#collapseOne-' . md5($c->nome) . '" aria-expanded="true" aria-controls="collapseOne-' . md5($c->nome) . '">
                                    Promoção Cursos em Dobro
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headOne-' . md5($c->nome) . '">
                            <div class="panel-body">
                               A cada matrícula você ganha uma nova totalmente grátis.
                               <br />
                               <a href="' . base_url("promocao") . '" target="_blank">+Detalhes</a>
                            </div>
                        </div>
                    </div>                    

                    <div class="panel panel-default panel-visualiza-curso">
                        <div class="panel-heading" role="tab" id="headOne-' . md5($c->nome) . '">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion-' . md5($c->nome) . '" href="#collapseOne-' . md5($c->nome) . '" aria-expanded="true" aria-controls="collapseOne-' . md5($c->nome) . '">
                                    Conteúdo
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headOne-' . md5($c->nome) . '">
                            <div class="panel-body">
                                ' . nl2br($c->descricao) . '
                            </div>
                        </div>
                    </div>
					
					 
					
                    <div class="panel panel-default panel-visualiza-curso">
                        <div class="panel-heading" role="tab" id="headTwo-' . md5($c->nome) . '">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-' . md5($c->nome) . '" href="#collapseTwo-' . md5($c->nome) . '" aria-expanded="false" aria-controls="collapseTwo-' . md5($c->nome) . '">
                                    Duração 
                                </a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headTwo-' . md5($c->nome) . '">
                            <div class="panel-body">
                                <span class="duracao-dias-' . $c->id_curso . '"></span> dias <br />
                                <span style="font-size: 10px;">Você pode prorrogar a conclusão para até 90 dias.</span>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default panel-visualiza-curso">
                        <div class="panel-heading" role="tab" id="headThree-' . md5($c->nome) . '">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-' . md5($c->nome) . '" href="#collapseThree-' . md5($c->nome) . '" aria-expanded="false" aria-controls="collapseThree-' . md5($c->nome) . '">
                                    Tipo
                                </a>
                            </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headThree-' . md5($c->nome) . '">
                            <div class="panel-body">
                                Capacitação - Curso livre 
                            </div>
                        </div>

                    </div>
                    <div class="panel panel-default panel-visualiza-curso">
                        <div class="panel-heading" role="tab" id="headFour-' . md5($c->nome) . '">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-' . md5($c->nome) . '" href="#collapseFour-' . md5($c->nome) . '" aria-expanded="false" aria-controls="collapseFour-' . md5($c->nome) . '">
                                    Serviços
                                </a>
                            </h4>
                        </div>
                        <div id="collapseFour" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headFour-' . md5($c->nome) . '">
                            <div class="panel-body">
                                <p>
                                -Inscrição
                                <br />
                                -Matrícula
                                <br />
                                -Conteúdo
                                <br />
                                -Vídeos, Lívros, Biblioteca Digital
                                <br />
                                -Certificado grátis
                                <br />
                                -Declaração de matrícula
                                <br />
                                -Validação de Certificado
                                <br />
                                -Notificações por email
                                <br />
                                -Prova online
                                <br />
                                -Reteste
                                <br />
                                -2ª chance                                
                                <br />
                                -Pagamento online
                                <br />
                                -Envio de documentos pelos correios
                                <br />
                                -Atendimento via Telefone, email ou help desk
                                </p>
                            </div>
                        </div>

                    </div>
                    <div class="panel panel-default panel-visualiza-curso">
                        <div class="panel-heading" role="tab" id="headFive">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    Impostos
                                </a>
                            </h4>
                        </div>
                        <div id="collapseFive" class="panel-collapse  collapse in" role="tabpanel" aria-labelledby="headFive">
                            <div class="panel-body">
                                R$<span class="valor-imposto-' . $c->id_curso . '"></span>
								
                            </div>
                        </div>

                    </div>
                   <div class="panel panel-default panel-visualiza-curso">
                    <div class="panel-heading" role="tab" id="headFour">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#Certificado" aria-expanded="false" aria-controls="Certificado">
                                    <i class="glyphicon glyphicon-plus"></i> Modelo de Certificado
                                </a>
                            </h4>
                        </div>
                        <div id="Certificado" class="panel-collapse collapse collapse in" role="tabpanel" aria-labelledby="Certificado">
                            <div class="panel-body">
                                <img src="' . base_url("template/img/thumb-certificado_web.png") . '" class="img-thumbnail">
                                <img src="' . base_url("template/img/thumb_certificado_web_verso.png") . '" class="img-thumbnail">
								  <button class="btn btn-success ' . $btn_matricular . '" style="float: right; margin-right: 10px;" data-id="' . $c->id_curso . '">Matrícular</button>
                            </div>
                        </div>
                    </div>  
                </div>
                </form>
                                      </div>
                                </div>';
            }
        ?>

    </div>

    <?php
    if (isset($paginacao)) {
        echo $paginacao;
    }
    ?>

</div>
</div>
</div>

</div>

<script>
    $(document).on('change', '.carga_horaria', function () {
        var curso = $(this).find("option:selected").attr('curso');
        var valor_curso = $(this).find("option:selected").attr('valor');
        var duracao = $(this).find("option:selected").attr('duracao');
        $(".valor-imposto-" + curso).html((parseFloat(valor_curso) * 0.128));
        var imposto = $(".valor-imposto-" + curso + "").html();
        imposto = imposto.replace(".", ",");
        imposto = imposto.split(",");
        $(".valor-imposto-" + curso + "").html(imposto[0] + ',' + imposto[1].substr(0, 2));
        $(".valor-curso-" + curso + "").html(valor_curso.replace(".", ","));
        $(".duracao-dias-" + curso).html(Math.ceil(parseInt(duracao) / 8));
        $(".tempo-" + curso + "").html(duracao);
        $(".moeda" + curso).css('display', 'inline');
    });

    $(document).ready(function () {
        $(".carga_horaria").each(function () {
            var curso = $(this).find("option:selected").attr('curso');
            var valor_curso = $(this).find("option:selected").attr('valor');
            var duracao = $(this).find("option:selected").attr('duracao');
            $(".valor-imposto-" + curso).html((parseFloat(valor_curso) * 0.128));
            var imposto = $(".valor-imposto-" + curso + "").html();
            imposto = imposto.replace(".", ",");
            imposto = imposto.split(",");
            $(".valor-imposto-" + curso + "").html(imposto[0] + ',' + imposto[1].substr(0, 2));
            $(".valor-curso-" + curso + "").html(valor_curso.replace(".", ","));
            $(".duracao-dias-" + curso).html(Math.ceil(parseInt(duracao) / 8));
            $(".tempo-" + curso + "").html(duracao);
            $(".moeda" + curso).css('display', 'inline');
        });

    });

    function validaFormMatricular(id) {
        if ($(".select-carga-horaria-" + id + " :selected").attr("value") == undefined) {
            alert("Selecione a carga horária!");
            return false;
        } else {
            return true;
        }

    }
</script>