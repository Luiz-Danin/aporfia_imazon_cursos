<div class="row">
    <div class="col-md-12">
        <div class="conteudo-direito">
            <div class="blank_25"></div>
            <form method="post" class="form-inline" onsubmit="loader_auth()"> 
                <div class="form-group form-group-lg" style="width: 63%;">
                    <input type="text" class="form-control input-lg" name="codigo-autenticacao" id="codigo-autenticacao" placeholder="Nome* ou C&oacute;digo de autentica&ccedil;&atilde;o**" value="<?= $this->input->post('codigo-autenticacao') ?>" style="width: 78%;" required="">
                    <button type="submit" class="btn btn-success btn-lg">Procurar</button>
                </div>
                <p class="help-block">*Digite o nome do aluno igual como est&aacute; escrito no certificado.<br />
                **Digite o c&oacute;digo de autentica&ccedil;&atilde;o igual como est&aacute; escrito no verso do certificado.</p>
            </form>   

            <div class="blank_25"></div>

            <?php
            if ((isset($resultado) and $resultado != null)  || (isset($resultado_vip) and $resultado_vip != null)  || (isset($resultado_vip_imp) and $resultado_vip_imp != null)  || (isset($resultado_imazon) and $resultado_imazon != null)  || (isset($resultado_vip2) and $resultado_vip2 != null)  || (isset($resultado_vip2_imp) and $resultado_vip2_imp !== NULL)) {

                echo '<hr>';
                echo '<table class="table table-hover"> 
                                     <thead> 
                                         <tr>
                                             <th>Nº</th>
                                             <th>Aluno</th>
                                             <th>Curso</th>
                                             <th>C&oacute;d. Autenti&ccedil;&atilde;o</th>
                                             <th width="14%">Op&ccedil;&otilde;es</th>
                                         </tr> 
                                     </thead> 
                                     <tbody>';
                if(isset($resultado)){
                if (count($resultado) > 0 && $resultado !== false) {
                    foreach ($resultado as $c) {
                        if (count($c) > 0) {
                            foreach ($c as $ac) {
                                echo '<tr><td>' . $ac->id_historico . '</td><td>' . $ac->nomeAluno . '</td><td>' . $ac->nome . '</td><td>' . $ac->codigo_autenticacao . '</td><td><a href="' . base_url("autentica/" . codifica($ac->id_historico)) . '" target="_blank">Autenticar</a></td>';
                            }
                        }
                    }
                }
                
                            }
                if(isset($resultado_imazon)){
                if (count($resultado_imazon) > 0 && $resultado_imazon !== false) {
                    foreach ($resultado_imazon as $ac) {
                        echo '<tr><td>' . $ac->id_historico . '</td><td>' . $ac->nomeAluno . '</td><td>' . $ac->nome . '</td><td>' . $ac->codigo_autenticacao . '</td><td><a href="' . base_url("autentica/" . codifica($ac->id_historico)) . '" target="_blank">Autenticar</a></td>';
                    }
                }
                }
                $certificados_errados = array(4091, 5769, 5770, 5644, 5384, 5758, 5771, 4655, 5680, 826, 5380, 5102, 5220, 4845, 5106, 5105, 5120, 5222, 4772, 4185);
                if(isset($resultado_vip)){
                if (count($resultado_vip) > 0 && $resultado_vip !== false) {
                    foreach ($resultado_vip as $vip) {
                        if (count($vip) > 0) {
                            foreach ($vip as $ad) {
                                echo '<tr>'
                                . '<td>' . $ad->usuarios_cursos_matriculados_id . '</td>'
                                . '<td>' . $nome[0]->nome . '</td>'
                                . '<td>' . $ad->nome . '</td>'
                                . '<td>' . str_replace("NÃ£o", "<p align=\"center\">-</p>", $ad->UCMCertificadoLiberado) . '</td>'
                                . '<td><a href="' . base_url("gera_autenticacao_certificado_vip/" . codifica($ad->usuarios_cursos_matriculados_id)) . '" target="_blank">Autenticar</a></td>';
                            }
                        }
                    }
                }
                
                            }
                if(isset($resultado_vip_imp)){            
                if (count($resultado_vip_imp) > 0 && $resultado_vip_imp !== false) {
                    foreach ($resultado_vip_imp as $vip) {
                        if (count($vip) > 0) {
                            foreach ($vip as $ad) {
                                if (in_array($ad->usuarios_cursos_matriculados_id, $certificados_errados)) {
                                    $status = $ad->id;
                                    $link = '<td><a href="' . base_url("gera_autenticacao_certificado_vip_imp/" . codifica($status)) . '" target="_blank">Autenticar</a></td>';
                                } else {
                                    $status = $ad->usuarios_cursos_matriculados_id . ' - ' . $ad->id;
                                }
                                echo '<tr>'
                                . '<td>' . $status . '</td>'
                                . '<td>' . $nome[0]->nome . '</td>'
                                . '<td>' . $ad->nome . '</td>'
                                . '<td>' . $ad->UCMCertificadoLiberado . '</td>'
                                . '<td><a href="' . base_url("gera_autenticacao_certificado_vip_imp/" . codifica($ad->id)) . '" target="_blank">Autenticar</a></td>';
                            }
                        }
                    }
            }}
            if(isset($resultado_vip2)){ 
                if (count($resultado_vip2) > 0 && $resultado_vip2 !== false) {
                    foreach ($resultado_vip2 as $ad) {
                        echo '<tr>'
                        . '<td>' . $ad->usuarios_cursos_matriculados_id . '</td>'
                        . '<td>' . $nome[0]->nome . '</td>'
                        . '<td>' . $ad->nome . '</td>'
                        . '<td>' . $ad->UCMCertificadoLiberado . '</td>'
                        . '<td><a href="' . base_url("gera_autenticacao_certificado_vip/" . codifica($ad->usuarios_cursos_matriculados_id)) . '" target="_blank">Autenticar</a></td>';
                    }
            }
            
                    }
                echo '</tr></tbody></table>';
            } else {
                echo "<pre>Nenhum registro encontrado</pre>";
            }
            ?>       
            <div class="blank_75"></div>
        </div>
    </div>
</div>
</div>
</div>



<!-- Modal -->
<div class="modal fade" id="loader-autenticacao" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="progress">
            <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
            </div>
            
          </div>
          <p align="center"><b>Aguarde...</b></p>
      </div>
    </div>
  </div>
</div>

<script>
function loader_auth(){
    $("#loader-autenticacao").modal("show");
}
</script>