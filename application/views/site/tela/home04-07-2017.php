 
            <div class="topo-chamada">
                <div class="img-chamada">
                    <div class="blank_50"></div>
                    <h1 class="titulo-home">Sua forma&ccedil;&atilde;o em <span style="color: #E2E200;">3 passos:</span></h1>
                    <div class="row">
                        <div class="col-md-4 col-xs-12 involucro-chamada">
                            <a data-toggle="modal" data-target="#modal-cadastro"  class="icone-chamada">
                                <p>
                                    <i class="glyphicon glyphicon-plus-sign"></i>
                                </p>    
                                <p>1. Cadastre-se</p>
                            </a>
                        </div>
                        <div class="col-md-4 col-xs-12 involucro-chamada">
                            <a href="<?=base_url("cursos")?>" class="icone-chamada">
                                <p>
                                    <i class="glyphicon glyphicon-pencil"></i>
                                </p>
                                <p>2. Estude</p>
                            </a>
                        </div>
                        <div class="col-md-4 col-xs-12 involucro-chamada">
                            <a href="<?=base_url("cursos")?>" class="icone-chamada">
                                <p>
                                    <i class="glyphicon glyphicon-education"></i>
                                <p>3. Obtenha seu Certificado</p>
                                </p>    
                            </a>
                        </div>
                    </div>
                    <div class="blank_20"></div>
                    <hr>
                    <div class="blank_30"></div>
                    <div class="row">
                        <div class="col-md-4"></div>
                        <div class="col-md-4  col-xs-12">
                            <p class="titulo-form-login-home">Entre ou <a data-toggle="modal" data-target="#modal-cadastro" class="link-home-cadastro">cadastre-se</a></p>
                            <div class="formulario-home">
                             
                                <form class="frm-inline form-login-chamada" id="login" onsubmit="return false">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input type="email" class="form-control" id="email-login" name="email" placeholder="Digite seu Email">
                                            <input type="submit" value="Continuar" class="btn btn-success btn-logar"style="float: right;margin: 2% 36% 0% 36%;"/>
                                        </div> 
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                </div>
            </div><!--topo-chamada-->
