<div class="conteudo-pagina">
    <ol class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li class="active"><a href="#">Recuperar senha</a></li>
    </ol>
    <div class="row">
        <div class="col-md-12">
            <div class="conteudo-direito">
                <?php
                if ($this->session->flashdata('success')) {
                    $tipo = 'success';
                }
                if ($this->session->flashdata('danger')) {
                    $tipo = 'danger';
                }
                if ($this->session->flashdata('warning')) {
                    $tipo = 'warning';
                }
                if (isset($tipo)) {
                    echo '<div class="alert alert-' . $tipo . '">' . $this->session->flashdata($tipo) . '</div>';
                }
                if ($this->session->flashdata('alerta-carrinho')) {
                    echo '<div class="alert alert-info" id="alerta"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>' . $this->session->flashdata('alerta-carrinho') . '</div>';
                }
                ?>
                <p>Digite seu email de acesso</p>
                <form class="form-signin" action="<?= current_url() ?>" method="post">
                    <label for="inputEmail" class="sr-only">Email</label>
                    <input type="email" id="inputEmail" class="form-control" name="email" placeholder="Digite um email válido" required autofocus>
                    <br />
                    <button class="btn btn-success" type="submit">Continuar</button>
                    <hr>
                    <p>Ainda não possui cadastro? <a href="<?= base_url("matricular_cadastrar/$curso/$carga_horaria") ?>" style="color:#0033FF">Clique aqui</a> e cadastre-se!</p>
                </form>
            </div>
        </div>
    </div>
</div>
</div>