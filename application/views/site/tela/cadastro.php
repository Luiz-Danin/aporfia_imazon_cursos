
<div class="involucro-conteudo-pagina">

    <div class="titulo-pagina">
        <div class="conteudo-titulo">
            <h1>Cadastro</h1>
        </div>
    </div>

    <div class="conteudo-pagina">
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active"><a href="#">Cadastro</a></li>
        </ol>
        <div class="row">
            <div class="col-md-12">
                <div class="conteudo-direito">
                    <form action="cadastrar" method="post">
                        <div class="form-group">
                            <label for="nome">Nome</label>
                            <input type="text" class="form-control" name="nome" id="nome" placeholder="Digite seu nome completo"  minlength="6" required value="<?= set_value('nome'); ?>">
                            <?= form_error('nome', '<div class="label label-warning">', '</div>'); ?>
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" name="email" id="email" placeholder="Informe um email válido" minlength="6" required value="<?= set_value('email'); ?>">
                            <?= form_error('email', '<div class="label label-danger">', '</div>'); ?>
                        </div>
                        <div class="form-group">
                            <label for="Numero">Celular*</label>
                            <div class="row">
                                <div class="col-md-2">
                                    <input type="text" class="form-control" value="<?= set_value('ddd_celular'); ?>" name="ddd_celular" id="ddd" maxlength="2" minlength="2" required="" placeholder="DDD">
                                </div>
                                <div class="col-xs-4">
                                    <input type="text" class="form-control" value="<?= set_value('celular'); ?>" name="celular" id="celular" maxlength="12" minlength="8" required="" id="telefone" placeholder="Telefone">
                                </div>
                            </div>
                            <?= form_error('ddd_celular', '<div class="label label-warning">', '</div>'); ?>
                            <?= form_error('celular', '<div class="label label-warning">', '</div>'); ?>
                        </div>
                        <div class="form-group">  
                            <label for="senha">Senha</label>
                            <input type="password" class="form-control" name="senha" id="senha" placeholder="Digite uma senha" minlength="6" required value="<?= set_value('senha'); ?>">
                            <?= form_error('senha', '<div class="label label-danger">', '</div>'); ?>
                        </div>
                        <div class="form-group">
                            <label for="senha2">Repita a sua senha</label>
                            <input type="password" class="form-control" name="senha2" id="senha2" placeholder="Repita a senha digitada" minlength="6" required value="<?= set_value('senha2'); ?>">
                            <?= form_error('senha2', '<div class="label label-danger">', '</div>'); ?>
                        </div>
                        <button type="submit" class="btn btn-primary">Pronto</button>
                    </form>
                    <p style="font-size:12px; font-weight:bold;">*Ao clicar em cadastrar você estará concordando com nosso <a href="<?= base_url("site/termo") ?>" target="_blank">termo de uso</a>
                        <br>
                        <i class="glyphicon glyphicon-lock"></i> Página Segura. A transmissão dos dados é criptografada.</p>
                </div>
            </div>
        </div>
    </div>

</div>