
<div class="col-md-9">
    <div class="blank_30"></div>
    <div class="conteudo-direito" id="cursos">
        <?php
              $cursos=array("INGLÊS BÁSICO");
              $data_turma=$turma[0]->data_inicio_aula;
             
               foreach ($cursos as $c) {              
               $img ="https://imazoncursos.com.br/imgs/cursos/curso_ingles.jpg";
               echo '<div class="media lista-cursos-home">
                                    <div class="media-left media-middle">
                                        <a href="#">
                                            <img class="media-object img-curso" src="' . $img . '" alt="' . $c . '">
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <a href="#">'
                . '<h4 class="media-heading titulo-curso-home" style="width: 80%;">CURSO ' . $c . '</h4>
                                        </a>										
									 <form action="' . base_url($url_matricula) . '" method="post" onsubmit="return validaFormMatricular(\'' . $c. '\')">							 
									 <input type="hidden" name="curso" value="" id="curso-id">
                                                                             <p style="width: 50%; font-size: 14px;" align="left">
                                       
                                       
                                        Tipo: Curso de Idiomas
                                       
                                        
                                        <br />
                                        Matrícula: <span class="moeda" style="display: none;">R$</span><span class="valor-curso-' . $c. '"> Grátis.</span>
										<br>
                                                                                Duração: <span class="duracao-dias-">24 Semanas  </span>
										<br>
										Certificado GRÁTIS
										<br/>
										Carga horária: 120 Horas
                                                                                <br/>   <br/>
                                                                                
                                                                          <a class="page-scroll" href="#" local="" id="btn-modal-cadastrar" data-toggle="modal" data-target="#modal-cadastro"><input type="submit" value="Matrícular" class="btn btn-success" style="float: left; margin-left: 10px;""> </a>
								
										
								                                        
                                        <span style="float:rigth; margin-left:10px; clear: both; font-size: 12px;" role="button" data-toggle="collapse" href="#' . md5($c) . '" aria-expanded="false" aria-controls="">+ Detalhes</span>                           
                                    </div>
                                    <br />
									
                                     <div class="collapse" id="' . md5($c) . '">
                                        <div class="panel-group" id="accordion-' . md5($c) . '" role="tablist" aria-multiselectable="true">
				

                     <div class="panel panel-default panel-visualiza-curso">
                        <div class="panel-heading" role="tab" id="headOne-">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion-" href="" aria-expanded="true" aria-controls="collapseOne-' . md5($c) . '">
                                    Promoção Aulas em Dobro
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headOne-">
                            <div class="panel-body">
                             O dobro de aulas pela metade do preço. 5 Aulas por semana com acompanhamento do professor, e você não paga nada a mais por isso.
                            </div>
                        </div>
                    </div>                    

                    <div class="panel panel-default panel-visualiza-curso">
                        <div class="panel-heading" role="tab" id="headOne-">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion-" href="#collapseOne-" aria-expanded="true" aria-controls="collapseOne-">
                                    Conteúdo
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headOne-">
                            <div class="panel-body">
                            Conhecendo um novo amigo no ambiente escolar   <br/>

                          
                            Começando em um novo trabalho  <br/>

                          
                            Iniciando o primeiro dia na escola   <br/>

                          
                            Visitando parentes distantes   <br/>

                           
                            Escrevendo um e-mail para um amigo distante.  <br/>

                         
                            Informando um novo endereço de residência   <br/>

                          
                            Reservando um quarto de hotel  <br/>

                       
                            Tarefas de casa   <br/>

                        
                            Almoçando em um restaurante   <br/>

                       
                            Preparando uma receita para o almoço  <br/>

                        
                            Preparando uma sobremesa  <br/>

                       
                            Encontro amoroso  <br/> 

                    
                            Indo ao cinema com um amigo.  <br/>

                        
                            Indo ao zoológico com um amigo  <br/> 

                        
                            Fazendo compras em um mercado   <br/>

                           
                            Comprando um  carro novo   <br/>

                         
                            Comprando uma casa nova  <br/>

                            
                            Como proceder em uma entrevista de emprego .  <br/>

                            
                            Como escrever um e-mail corporativo.  <br/> 

                           
                            Conhecendo um estádio de futebol.  <br/>

                           
                            Fim de semana em família   <br/>

                          
                            Visitando uma galeria de artes.  <br/>

                          
                            Fazendo transações bancárias.  <br/> 

                            
                            Passeio na praia   <br/>     
                            </div>
                        </div>
                    </div>
					
					 
					
                    <div class="panel panel-default panel-visualiza-curso">
                        <div class="panel-heading" role="tab" id="headTwo-">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-
                                " href="#collapseTwo-" aria-expanded="false" aria-controls="collapseTwo-">
                                    Duração 
                                </a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headTwo-">
                            <div class="panel-body">
                                <span class="duracao-dias-"></span> 24 Semanas (Cerca de 6 Meses)</span><br />
                                <span style="font-size: 10px;">5 aulas por semana. 40 Minutos cada aula.</span>
                            </div>
                        </div>
                    </div>
                    
                    <div class="panel panel-default panel-visualiza-curso">
                        <div class="panel-heading" role="tab" id="headTwo-">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-
                                " href="#collapseTwo-" aria-expanded="false" aria-controls="collapseTwo-">
                                    Início das Aulas
                                </a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headTwo-">
                            <div class="panel-body">
                                <span class="duracao-dias-"></span>'.date('d/m/Y', strtotime($data_turma)).' </span><br/>                                
                            </div>
                        </div>
                    </div>
           
                    <div class="panel panel-default panel-visualiza-curso">
                        <div class="panel-heading" role="tab" id="headThree-">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-" href="#collapseThree-' . md5($c->nome) . '" aria-expanded="false" aria-controls="collapseThree-' . md5($c->nome) . '">
                                    Tipo
                                </a>
                            </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headThree-">
                            <div class="panel-body">
                                Capacitação - Curso de Idiomas 
                            </div>
                        </div>

                    </div>
                    <div class="panel panel-default panel-visualiza-curso">
                        <div class="panel-heading" role="tab" id="headFour-">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-" href="#collapseFour-" aria-expanded="false" aria-controls="collapseFour-">
                                    Serviços
                                </a>
                            </h4>
                        </div>
                        <div id="collapseFour" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headFour-">
                            <div class="panel-body">
                                <p>
                                - Tudo pela internet<br/>
                                - Você acessa do computador, tablet ou celular<br/>
                                - Acesso 24 horas por dia ao conteúdo<br/>
                                - Novas aulas toda semana<br/>
                                - Temas voltados para o dia a dia<br/>
                                - Professores brasileiros que entendem você<br/>
                                - Exercícios de acompanhamento de sua evolução<br/>
                                - Tira dúvidas por áudio ou texto<br/>
                                - Novas turmas toda semana<br/>
                                - Professores acompanhando seu desenvolvimento e sua aprendizagem<br/>
                                - Certificado Grátis disponível no site<br/>
                                - Certificado Original no final do curso<br/>
                                - Equipe empenhada para o seu sucesso<br/>
                                - Pagamento facilitado por cartão de crédito, débito em conta ou boleto bancário<br/>
                                </p>
                            </div>
                        </div>

                    </div>
                    
                       <div class="panel panel-default panel-visualiza-curso">
                        <div class="panel-heading" role="tab" id="headFive">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                   Mensalidade
                                </a>
                            </h4>
                        </div>
                        <div id="collapseFive" class="panel-collapse  collapse in" role="tabpanel" aria-labelledby="headFive">
                            <div class="panel-body">
                                R$<span class="valor-imposto-"> 99,00</span><br/>
                                <span style="font-size: 10px;">Você se matricula a cada mês e tem acesso a todas as aulas durante o período..</span>
								
                            </div>
                        </div>

                    </div>

                    <div class="panel panel-default panel-visualiza-curso">
                        <div class="panel-heading" role="tab" id="headFive">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    Impostos
                                </a>
                            </h4>
                        </div>
                        <div id="collapseFive" class="panel-collapse  collapse in" role="tabpanel" aria-labelledby="headFive">
                            <div class="panel-body">
                                R$<span class="valor-imposto-"> 12,67</span><br/>
				  <span style="font-size: 10px;">Já incluído no valor da mensalidade.</span>				
                            </div>
                        </div>

                    </div>
                   <div class="panel panel-default panel-visualiza-curso">
                    <div class="panel-heading" role="tab" id="headFour">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#Certificado" aria-expanded="false" aria-controls="Certificado">
                                    <i class="glyphicon glyphicon-plus"></i> Modelo de Certificado
                                </a>
                            </h4>
                        </div>
                        <div id="Certificado" class="panel-collapse collapse collapse in" role="tabpanel" aria-labelledby="Certificado">
                            <div class="panel-body">
                                <img src="' . base_url("template/img/thumb-certificado_web.png") . '" class="img-thumbnail">
                                <img src="' . base_url("template/img/thumb_certificado_web_verso.png") . '" class="img-thumbnail">
								  
                            </div>
                        </div>
                    </div>  
                </div>
                </form>
                                      </div>
                                </div>';
            }
         
                ?>
   
   </div>
        <div class="conteudo-direito" id="cursos">
        <?php
              $cursos=array("ESPANHOL BÁSICO");
              $data_turma=$turma[0]->data_inicio_aula;
             
               foreach ($cursos as $c) {              
               $img ="https://imazoncursos.com.br/imgs/cursos/curso_espanhol";
               echo '<div class="media lista-cursos-home">
                                    <div class="media-left media-middle">
                                        <a href="#">
                                            <img class="media-object img-curso" src="' . $img . '" alt="' . $c . '">
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <a href="#">'
                . '<h4 class="media-heading titulo-curso-home" style="width: 80%;">CURSO ' . $c . '</h4>
                                        </a>										
									 <form action="' . base_url($url_matricula) . '" method="post" onsubmit="return validaFormMatricular(\'' . $c . '\')">							 
									 <input type="hidden" name="curso" value="" id="curso-id">
                                                                             <p style="width: 50%; font-size: 14px;" align="left">
                                       
                                       
                                        Tipo: Curso de Idiomas
                                       
                                        
                                        <br />
                                        Matrícula: <span class="moeda" style="display: none;">R$</span><span class="valor-curso-' . $c . '"> Grátis.</span>
										<br>
                                                                                Duração: <span class="duracao-dias-">24 Semanas  </span>
										<br>
										Certificado GRÁTIS
										<br/>
										Carga horária: 120 Horas
                                                                                <br/>   <br/>
                                                                                
                                                                          <a class="page-scroll" href="#" local="" id="btn-modal-cadastrar" data-toggle="modal" data-target="#modal-cadastro"><input type="submit" value="Matrícular" class="btn btn-success" style="float: left; margin-left: 10px;""> </a>
								
										
								                                        
                                        <span style="float:rigth; margin-left:10px; clear: both; font-size: 12px;" role="button" data-toggle="collapse" href="#' . md5($c) . '" aria-expanded="false" aria-controls="">+ Detalhes</span>                           
                                    </div>
                                    <br />
									
                                     <div class="collapse" id="' . md5($c) . '">
                                        <div class="panel-group" id="accordion-' . md5($c) . '" role="tablist" aria-multiselectable="true">
				

                     <div class="panel panel-default panel-visualiza-curso">
                        <div class="panel-heading" role="tab" id="headOne-">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion-" href="" aria-expanded="true" aria-controls="collapseOne-' . md5($c) . '">
                                    Promoção Aulas em Dobro
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headOne-">
                            <div class="panel-body">
                             O dobro de aulas pela metade do preço. 5 Aulas por semana com acompanhamento do professor, e você não paga nada a mais por isso.
                            </div>
                        </div>
                    </div>                    

                    <div class="panel panel-default panel-visualiza-curso">
                        <div class="panel-heading" role="tab" id="headOne-">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion-" href="#collapseOne-" aria-expanded="true" aria-controls="collapseOne-">
                                    Conteúdo
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headOne-">
                            <div class="panel-body">
                            ¡Hola! ¿Qué tal?<br/>
                            La familia<br/> 
                            La casa<br/>
                            El trabajo<br/>
                            La comida<br/>
                            Mi rutina diaria <br/>
                            ¿Cuál es tu número de teléfono?<br/>
                            El tiempo<br/> 
                            Ir de Compras<br/>
                            El supermercado<br/>
                            Un paseo por mi ciudad<br/>
                            Vacaciones<br/>
                            El tiempo ocio<br/>
                            ¡Describe!<br/> 
                            Mi receta favorita<br/>
                            El barrio que vivo<br/> 
                            El centro comercial<br/>
                            Salud y enfermedad<br/>
                            Antes y ahora<br/>
                            Yo no gano tanto como tú<br/>
                            ¿Qué hiciste en el fin de semana?<br/>
                            Moverse por La ciudad<br/>
                            Segunda mano <br/>
                            Consejos<br/>    
                            </div>
                        </div>
                    </div>
					
					 
					
                    <div class="panel panel-default panel-visualiza-curso">
                        <div class="panel-heading" role="tab" id="headTwo-">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-
                                " href="#collapseTwo-" aria-expanded="false" aria-controls="collapseTwo-">
                                    Duração 
                                </a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headTwo-">
                            <div class="panel-body">
                                <span class="duracao-dias-"></span> 24 Semanas (Cerca de 6 Meses)</span><br />
                                <span style="font-size: 10px;">5 aulas por semana. 40 Minutos cada aula.</span>
                            </div>
                        </div>
                    </div>
                    
                    <div class="panel panel-default panel-visualiza-curso">
                        <div class="panel-heading" role="tab" id="headTwo-">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-
                                " href="#collapseTwo-" aria-expanded="false" aria-controls="collapseTwo-">
                                    Início das Aulas
                                </a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headTwo-">
                            <div class="panel-body">
                                <span class="duracao-dias-"></span>'.date('d/m/Y', strtotime($data_turma)).' </span><br/>                                
                            </div>
                        </div>
                    </div>
           
                    <div class="panel panel-default panel-visualiza-curso">
                        <div class="panel-heading" role="tab" id="headThree-">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-" href="#collapseThree-' . md5($c->nome) . '" aria-expanded="false" aria-controls="collapseThree-' . md5($c->nome) . '">
                                    Tipo
                                </a>
                            </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headThree-">
                            <div class="panel-body">
                                Capacitação - Curso de Idiomas 
                            </div>
                        </div>

                    </div>
                    <div class="panel panel-default panel-visualiza-curso">
                        <div class="panel-heading" role="tab" id="headFour-">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-" href="#collapseFour-" aria-expanded="false" aria-controls="collapseFour-">
                                    Serviços
                                </a>
                            </h4>
                        </div>
                        <div id="collapseFour" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headFour-">
                            <div class="panel-body">
                                <p>
                                - Tudo pela internet<br/>
                                - Você acessa do computador, tablet ou celular<br/>
                                - Acesso 24 horas por dia ao conteúdo<br/>
                                - Novas aulas toda semana<br/>
                                - Temas voltados para o dia a dia<br/>
                                - Professores brasileiros que entendem você<br/>
                                - Exercícios de acompanhamento de sua evolução<br/>
                                - Tira dúvidas por áudio ou texto<br/>
                                - Novas turmas toda semana<br/>
                                - Professores acompanhando seu desenvolvimento e sua aprendizagem<br/>
                                - Certificado Grátis disponível no site<br/>
                                - Certificado Original no final do curso<br/>
                                - Equipe empenhada para o seu sucesso<br/>
                                - Pagamento facilitado por cartão de crédito, débito em conta ou boleto bancário<br/>
                                </p>
                            </div>
                        </div>

                    </div>
                    
                       <div class="panel panel-default panel-visualiza-curso">
                        <div class="panel-heading" role="tab" id="headFive">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                   Mensalidade
                                </a>
                            </h4>
                        </div>
                        <div id="collapseFive" class="panel-collapse  collapse in" role="tabpanel" aria-labelledby="headFive">
                            <div class="panel-body">
                                R$<span class="valor-imposto-"> 99,00</span><br/>
                                <span style="font-size: 10px;">Você se matricula a cada mês e tem acesso a todas as aulas durante o período..</span>
								
                            </div>
                        </div>

                    </div>

                    <div class="panel panel-default panel-visualiza-curso">
                        <div class="panel-heading" role="tab" id="headFive">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    Impostos
                                </a>
                            </h4>
                        </div>
                        <div id="collapseFive" class="panel-collapse  collapse in" role="tabpanel" aria-labelledby="headFive">
                            <div class="panel-body">
                                R$<span class="valor-imposto-"> 12,67</span><br/>
				  <span style="font-size: 10px;">Já incluído no valor da mensalidade.</span>				
                            </div>
                        </div>

                    </div>
                   <div class="panel panel-default panel-visualiza-curso">
                    <div class="panel-heading" role="tab" id="headFour">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#Certificado" aria-expanded="false" aria-controls="Certificado">
                                    <i class="glyphicon glyphicon-plus"></i> Modelo de Certificado
                                </a>
                            </h4>
                        </div>
                        <div id="Certificado" class="panel-collapse collapse collapse in" role="tabpanel" aria-labelledby="Certificado">
                            <div class="panel-body">
                                <img src="' . base_url("template/img/thumb-certificado_web.png") . '" class="img-thumbnail">
                                <img src="' . base_url("template/img/thumb_certificado_web_verso.png") . '" class="img-thumbnail">
								  
                            </div>
                        </div>
                    </div>  
                </div>
                </form>
                                      </div>
                                </div>';
            }
         
                ?>
   
   </div>
       </div>

</div>


</div>
</div>