
<div class="involucro-conteudo-pagina">

    <div class="conteudo-pagina">
        <div class="row">
            <div class="col-md-12">
                <div class="conteudo-direito">

                    <?php
                    if ($msg) {
                        echo '<div class="alert alert-warning">Dados incorretos</div>';
                    }
                    ?>

<?php if ($novo_cadastro) { ?>
                        <form action="<?= current_url() ?>" method="post">
                            <div class="form-group">
                                <label for="nome">Nome</label>
                                <?php echo form_error('nome'); ?>
                                <input type="text" class="form-control" name="nome" id="nome" placeholder="Digite seu nome completo" min="6" required value="<?= set_value('nome'); ?>">
    <?= form_error('nome', '<div class="label label-warning">', '</div>'); ?>
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <?php echo form_error('email'); ?>
                                <input type="email" class="form-control" name="email" id="email" placeholder="Informe um email válido" min="6" required value="<?= set_value('email'); ?>">
    <?= form_error('email', '<div class="label label-danger">', '</div>'); ?>
                            </div>
                            <div class="form-group">
                                <label for="Numero">Celular</label>
                                <div class="row">
                                    <div class="col-md-2">
                                        <input type="text" class="form-control" value="<?= set_value('ddd_celular'); ?>" name="ddd_celular" id="ddd" maxlength="2" minlength="2" required="" placeholder="DDD">
                                    </div>
                                    <div class="col-xs-4">
                                        <input type="text" class="form-control" value="<?= set_value('celular'); ?>" name="celular" id="celular" maxlength="12" minlength="8" required="" id="telefone" placeholder="Celular">
                                    </div>
                                </div>
    <?= form_error('ddd_celular', '<div class="label label-warning">', '</div>'); ?>
    <?= form_error('celular', '<div class="label label-warning">', '</div>'); ?>
                            </div>
                            <div class="form-group">  
                                <label for="senha">Senha</label>
                                <?php echo form_error('senha'); ?>
                                <input type="password" class="form-control" name="senha" id="senha" placeholder="Digite uma senha" min="6" required value="<?= set_value('senha'); ?>">
    <?= form_error('senha', '<div class="label label-danger">', '</div>'); ?>
                            </div>
                            <div class="form-group">
                                <label for="senha2">Repita a sua senha</label>
                                <?php echo form_error('senha2'); ?>
                                <input type="password" class="form-control" name="senha2" id="senha2" placeholder="Repita a senha digitada" min="6" required value="<?= set_value('senha2'); ?>">
                            <?= form_error('senha2', '<div class="label label-danger">', '</div>'); ?>
                            </div>
                            <?php
                            if ($curso && $carga_horaria) {
                                echo '<input type="hidden" name="curso" value="' . $curso . '"/>';
                                echo '<input type="hidden" name="carga_horaria" value="' . $carga_horaria . '"/>';
                            }
                            ?>
                            <button type="submit" class="btn btn-primary">Cadastrar</button>
                        </form>

                        <p style="font-size:12px; font-weight:bold;">*Ao clicar em cadastrar você estará concordando com nosso <a href="<?= base_url("site/termo") ?>" target="_blank">termo de uso</a>
                            <br>

<?php } else if ($usuario_existente) { ?>
                        <form action="<?= current_url() ?>" method="post">
                            <div class="form-group">
                                <label for="email">Email</label>
                                <?php echo form_error('email'); ?>
                                <input type="email" class="form-control" name="email" id="email" placeholder="Informe um email válido" min="6" required value="<?= set_value('email'); ?>">
    <?= form_error('email', '<div class="label label-danger">', '</div>'); ?>
                            </div>
                            <div class="form-group">  
                                <label for="senha">Senha</label>
                                <?php echo form_error('senha'); ?>
                                <input type="password" class="form-control" name="senha" id="senha" placeholder="Digite uma senha" min="6" required value="<?= set_value('senha'); ?>">
                            <?= form_error('senha', '<div class="label label-danger">', '</div>'); ?>
                            </div>
                            <?php
                            if ($curso && $carga_horaria) {
                                echo '<input type="hidden" name="curso" value="' . $curso . '"/>';
                                echo '<input type="hidden" name="carga_horaria" value="' . $carga_horaria . '"/>';
                            }
                            ?>
                            <button type="submit" class="btn btn-primary">Matricular</button>
                            <a href="<?= base_url("rec_senha") ?>" class="btn btn-default">Recuperar senha</a>
                        </form>
                        <br>
<?php } else { ?>

                        <form action="<?= current_url() ?>" method="post">
                            <div class="form-group">
                                <label for="email">Email</label>
                                <?php echo form_error('email'); ?>
                                <input type="email" class="form-control" name="email" id="email" placeholder="Informe um email válido" min="6" required value="<?= set_value('email'); ?>">
                            <?= form_error('email', '<div class="label label-danger">', '</div>'); ?>
                            </div>
                            <?php
                            if ($curso && $carga_horaria) {
                                echo '<input type="hidden" name="curso" value="' . $curso . '"/>';
                                echo '<input type="hidden" name="carga_horaria" value="' . $carga_horaria . '"/>';
                            }
                            ?>
                            <button type="submit" class="btn btn-primary">Continuar</button>
                        </form>
                        <br>

<?php } ?>
                    <i class="glyphicon glyphicon-lock"></i> Página Segura. A transmissão dos dados é criptografada.</p>
                    <div style="height: 10px;"></div>
                </div>
            </div>
        </div>
    </div>

</div>
</div>