
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <title>Cadastro</title>

        <style type="text/css">

            <!--

            body{

                margin:0;

                padding:0;

                background-color:#E3F1C9;

            }

            -->

        </style>

        <link href="http://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">

    </head>

    <body>

        <table cellpadding="0" cellspacing="0" border="0" width="100%" style="width:100%;background-color:#ffffff;width:100%" bgcolor="#ffffff">

            <tr>

                <td>

                    <table cellpadding="0" cellspacing="0" border="0" width="600px" align="center" style="width:600px;font-family:Verdana, Helvetica, sans-serif;color:#333333;font-size:13px;line-height:150%">

                        <tr>

                            <td style="padding:10px;">

                                <a href="https://imazoncursos.com.br/site/home"><img src="http://imgur.com/wGl2QmJ.jpg" border="0" /></a>

                            </td>

                            <td>

                                <h1 style="white-space:nowrap;Margin-left:-330px;Margin-bottom: 1px;font-family: Montserrat,Verdana,serif;font-weight: normal;color: #8FCC16;font-size: 30px;line-height: 38px;">Imazon Cursos</h1>

                            </td>

                            <td>

                            </td>



                        </tr>

                        <tr>

                            <td colspan="2" style="border-top:5px #1f381a solid;border-bottom:5px #1f381a solid">&nbsp;



                            </td>

                        </tr>

                        <tr>

                            <td colspan="2" bgcolor="#8FCC16" style="padding-top:10px;color:#1f381a;font-weight:bold" align="center">

                                <p style="padding-top: 20px;font-family: Montserrat,Verdana,serif;font-weight: normal;font-size: 20px;line-height: 1px;"><b>CADASTRO REALIZADO COM SUCESSO !</b></p>

                            </td>

                        </tr>

                        <tr>

                            <td colspan="2" bgcolor="#8FCC16" style="padding:5px;padding-bottom:0px;color:#FFFFFF" align="center">

                                <p class="size-17" style="Margin: 15px 10px 0px 25px;font-size: 17px;line-height: 26px;text-align: left;"><b>Parab&eacute;ns!</b></p>

                                <p class="size-17" style="font-family: Montserrat,Verdana,serif;Margin: 5px 0px 0px 25px;font-size: 17px;line-height: 26px;text-align: left;"><span>Voc&ecirc; realizou seu cadastro em nosso site. Fa&ccedil;a login e desfrute do conte&uacute;do disponibilizado.</span></p>

                                <p class="size-17" style="font-family: Montserrat,Verdana,serif;Margin: 5px 0px 0px 25px;font-size: 17px;line-height: 26px;text-align: left;"><span>Bons Estudos !</span></p>

                            </td>

                        </tr>

                        <tr>

                            <td colspan="2" bgcolor="#8FCC16" valign="bottom" height="35px" style="height:35px">

                                <img src="http://www.emailtemplates.com.br/templates/evolution/2.png" style="display:block" border="0" />

                            </td>

                        </tr>

                        <tr>

                            <td bgcolor="#FFFFFF" style="padding:1px;padding-bottom:0px;font-weight:bold">

                                <p class="size-17" style="font-family: Montserrat,Verdana,serif;Margin-top: 5px;Margin: 30px 0px 0px 25px;font-size: 17px;line-height: 26px;text-align: left;"><b>Acesse:</b>

                                    <a style="text-decoration: none;color:#638c13;font-size:14px;" target="_blank" href="https://imazoncursos.com.br/site/home">www.imazoncursos.com.br</a></p>

                                <p class="size-17" style="font-family: Montserrat,Verdana,serif;Margin-top: 5px;Margin: 30px 0px 0px 25px;font-size: 17px;line-height: 26px;text-align: left;"><b>Login:</b>

                                    <span style="text-decoration: none;color:#638c13;font-size:14px;">{email}</a></span>



                                <p class="size-17" style="font-family: Montserrat,Verdana,serif;Margin: 10px 0px 0px 25px;font-size: 17px;line-height: 26px;text-align: left;"><b>Senha:</b>

                                    <span style="text-decoration: none;color:#638c13;font-size:14px;">{senha}</a></span>

                            </td>

                            <td bgcolor="#FFFFFF" style="padding:15px;padding-bottom:0px;font-weight:bold">&nbsp;



                            </td>

                        </tr>

                        <tr>

                            <td align="center" colspan="2" bgcolor="#fff">

                                <br><br><br><br><a style="text-decoration: underline;transition: opacity 0.1s ease-in;color: #a3a4ad;" target="_blank" href="http://www.facebook.com/imazoncursos">

                                    <img style="border: 0;" src="https://i10.createsend1.com/static/eb/customise/13-the-blueprint-3/images/facebook.png" width="26" height="26"></a>



                                <a style="text-decoration: underline;transition: opacity 0.1s ease-in;color: #a3a4ad;" target="_blank" href="http://www.twitter.com/imazoncursos">

                                    <img style="border: 0;" src="https://i1.createsend1.com/static/eb/customise/13-the-blueprint-3/images/twitter.png" width="26" height="26"></a>

                                <br><br><span style="font-family: Montserrat,Verdana,serif;font-weight: normal;font-size: 14px;line-height: 1px;">Atenciosamente,<br>Equipe Imazon Cursos</span><br><br>

                            </td>

                        </tr>

                        <tr>

                            <td colspan="2" bgcolor="#8FCC16">

                                <img src="http://www.emailtemplates.com.br/templates/evolution/7.png" style="display:block" />

                            </td>

                        </tr>

                        <tr>

                            <td colspan="2" bgcolor="#8FCC16" align="center" style="padding-left:20px;color:#FFFFFF">

                                <p style="font-family: Montserrat,Verdana,serif;margin-top:-10px;">Copyright &#169; - Imazon Cursos 2007 / 2016<br />Contato: 4004-0435 (Ramal 8229) / atendimento@imazoncursos.com.br</p>

                            </td>

                            <td bgcolor="#8FCC16" style="color:#FFFFFF">



                            </td>

                        </tr>



                    </table>

                </td>

            </tr>

        </table>

    </body>

</html>