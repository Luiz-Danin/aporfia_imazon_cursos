
<div class="row">
    <div class="col-md-12">
        <div class="conteudo-direito">
            <div class="alert alert-info" role="alert"  id="termo">Desde o dia <?= date("d/i/Y", strtotime($termo_uso[0]->data_publicacao)) ?> o nosso termo de uso sofreu alterações. Para continuar usufruindo dos nossos serviços, você precisará concordar com o nosso novo <a href="#termo-uso"><b>termo de uso</b></a>.</div>
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default panel-suporte">
                    <div class="panel-heading" role="tab" id="headingContato">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse">
                                <i class="glyphicon glyphicon-info-sign"></i> Termo de uso
                            </a>
                        </h4>
                    </div>
                    <div id="collapseContatos" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingContato">
                        <div class="panel-body" id="termo-uso">
                            <?=  trim($termo_uso[0]->conteudo);?> 
                            <a href="<?= base_url("site/concordar_termo") ?>" class="btn btn-success">Concordo</a> <a href="" class="btn btn-warning">Não concordo</a>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>
</div>

</div>