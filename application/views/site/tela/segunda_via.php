<div class="row">
    <div class="col-md-12">
        <div class="conteudo-direito">
            <div class="blank_25"></div>
            <form method="post" class="form-inline" onsubmit="loader_auth()"> 
                <div class="form-group form-group-lg" style="width: 63%;">
                    <input type="email" class="form-control input-lg" name="email" id="email" placeholder="Email de acesso" value="<?= $this->input->post('email') ?>" style="width: 78%;" required="">
                    <button type="submit" class="btn btn-success btn-lg">Procurar</button>
                </div>
                <p class="help-block">*Digite o seu email de acesso.<br />
            </form>   
 
            <div class="blank_10"></div>

            <?php
                  
            if($this->input->post())
            if ($resultado != null) {
               
                
                if (count($resultado) > 0 && $resultado !== false) {
                    echo "<pre>Localizamos <b>".count($resultado). " fatura(s) pendente(s)</b>.</pre>";
                } 
                
                echo '<br />';
                echo '<table class="table table-hover"> 
                                     <thead> 
                                         <tr>
                                             <th>Transa&ccedil;&atilde;o</th>
                                             <th>Descri&ccedil;&atilde;o</th>
                                             <th>Data da compra</th>
                                             <th>Valor</th>
                                             <th width="14%">Op&ccedil;&otilde;es</th>
                                         </tr> 
                                     </thead> 
                                     <tbody>';
                if (count($resultado) > 0 && $resultado !== false) {
                    
                    foreach ($resultado as $c) {
                        if (count($c) > 0) {
                            $descricao = "Matrícula no(s) curso(s): ";
                                $i = 1;
                                foreach($itens_pedido[0][$c->transacao] as $ip){
                                    if($i == count($itens_pedido[0][$c->transacao])){
                                        $descricao .= "<b>".$ip->nome . "</b>.";
                                    }else  if($i == count($itens_pedido[0][$c->transacao])-1){
                                        $descricao .= "<b>".$ip->nome . "</b> e ";
                                    }else{
                                        $descricao .= "<b>" . $ip->nome . "</b>, ";
                                    }
                                    $i++;
                                }
                                echo '<tr><td>' . $c->transacao . '</td><td>'.$descricao.'</td><td>R$' . date("d/m/Y", strtotime($c->data_compra)) . '</td><td>R$' . format_number($c->valor) . '</td><td><a href="' . base_url("gerar_segunda_via/" . $c->transacao) . '" target="_blank">Imprimir</a></td>';
                        }
                    }
                }

                echo '</tr></tbody></table>';
            } else {
                echo "<pre>Nenhum registro encontrado</pre>";
            }
            ?>       
            <div class="blank_75"></div>
        </div>
    </div>
</div>
</div>
</div>



<!-- Modal -->
<div class="modal fade" id="loader-autenticacao" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="progress">
            <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
            </div>
            
          </div>
          <p align="center"><b>Aguarde...</b></p>
      </div>
    </div>
  </div>
</div>

<script>
function loader_auth(){
    $("#loader-autenticacao").modal("show");
}
</script>