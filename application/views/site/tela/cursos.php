
<div class="col-md-9">
    <div class="blank_30"></div>
    <div class="conteudo-direito" id="cursos">
        <?php
        echo '<div class="media lista-cursos-home">
                    <div class="media-left media-middle">
                        <a href="'.base_url("cursos_livres").'">
                            <img class="media-object img-curso" src="'.base_url("imgs/cursos/IMA225.jpg").'" alt="">
                        </a>
                    </div>  
                    <div class="media-body">
                        <a href="' . base_url('cursos_livres') . '">'
                            . '<h4 class="media-heading titulo-curso-home" style="width: 80%;">CURSOS LIVRES</h4>
                        </a>	
                         <br /><br /><br /><br /><br />
                               <a href="' . base_url("cursos_livres") . '"class="label label-info">+Ver mais</a>
                    </div>
                </div>';
        $url_idm = 'https://www.idmcursos.com.br/';
        echo '<div class="media lista-cursos-home">
                    <div class="media-left media-middle">
                        <a target="_blank" href="'.$url_idm.'">
                            <img class="media-object img-curso" src="'.base_url("imgs/cursos/curso_ingles.jpg").'" alt="">
                        </a>
                    </div>  
                    <div class="media-body">
                        <a target="_blank" href="' . $url_idm . '">'
                            . '<h4 class="media-heading titulo-curso-home" style="width: 80%;">CURSOS DE IDIOMAS</h4>
                        </a>		
                        <br /><br /><br /><br /><br />
                               <a target="_blank" href="' . $url_idm . '"class="label label-info">+Ver mais</a>
                    </div>
                </div>';
        ?>


    </div>


</div>
</div>
</div>

</div>

<script>
    $(document).on('change', '.carga_horaria', function () {
        var curso = $(this).find("option:selected").attr('curso');
        var valor_curso = $(this).find("option:selected").attr('valor');
        var duracao = $(this).find("option:selected").attr('duracao');
        $(".valor-imposto-" + curso).html((parseFloat(valor_curso) * 0.128));
        var imposto = $(".valor-imposto-" + curso + "").html();
        imposto = imposto.replace(".", ",");
        imposto = imposto.split(",");
        $(".valor-imposto-" + curso + "").html(imposto[0] + ',' + imposto[1].substr(0, 2));
        $(".valor-curso-" + curso + "").html(valor_curso.replace(".", ","));
        $(".duracao-dias-" + curso).html(Math.ceil(parseInt(duracao) / 8));
        $(".tempo-" + curso + "").html(duracao);
        $(".moeda" + curso).css('display', 'inline');
    });

    $(document).ready(function () {
        $(".carga_horaria").each(function () {
            var curso = $(this).find("option:selected").attr('curso');
            var valor_curso = $(this).find("option:selected").attr('valor');
            var duracao = $(this).find("option:selected").attr('duracao');
            $(".valor-imposto-" + curso).html((parseFloat(valor_curso) * 0.128));
            var imposto = $(".valor-imposto-" + curso + "").html();
            imposto = imposto.replace(".", ",");
            imposto = imposto.split(",");
            $(".valor-imposto-" + curso + "").html(imposto[0] + ',' + imposto[1].substr(0, 2));
            $(".valor-curso-" + curso + "").html(valor_curso.replace(".", ","));
            $(".duracao-dias-" + curso).html(Math.ceil(parseInt(duracao) / 8));
            $(".tempo-" + curso + "").html(duracao);
            $(".moeda" + curso).css('display', 'inline');
        });

    });

    function validaFormMatricular(id) {
        if ($(".select-carga-horaria-" + id + " :selected").attr("value") == undefined) {
            alert("Selecione a carga horária!");
            return false;
        } else {
            return true;
        }

    }
</script>