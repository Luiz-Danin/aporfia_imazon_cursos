<?php
if ($this->session->userdata('imazon_logado')) {
    $autenticacao = base64_encode(strrev(base64_encode(base64_encode('28c8edde3d61a0411511d3b1866f0636:SU1BWk9OIENVUlNPUw==').':'.$this->session->userdata('imazon_id_fic').':'.$this->session->userdata('imazon_email_aluno').':'.$this->session->userdata('imazon_nome_aluno'))));
}
if ($this->session->userdata('imazon_logado')) {
    $suporte = '(<a href="http://canalformacao.com.br/auth/'.$autenticacao.'" target="_blank">Clique aqui para Iniciar</a>)';
}else{
    $suporte = '(<a href="#" id="btn-modal-cadastrar" data-toggle="modal" data-target="#modal-cadastro">Clique aqui para Iniciar)</a>';
}

?> 


        <div class="row">
            <div class="col-md-12">
                <div class="conteudo-direito">

                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default panel-suporte">
                            <div class="panel-heading" role="tab" id="headingContato">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseContatos" aria-expanded="true" aria-controls="collapseContatos">
                                        <i class="glyphicon glyphicon-plus"></i> Contatos
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseContatos" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingContato">
                                <div class="panel-body">
                                    <table class="table">
                                        <tr>
                                            <td>
                                                Helpdesk: <?=$suporte?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                E-mail: atendimento@imazoncursos.com.br
                                            </td>
                                        </tr>
                                        <!--<tr>
                                            <td>
                                                Por Telefone: 4004-0435 Ramal 8229
                                            </td>
                                        </tr>
                                        -->
                                        <tr>
                                            <td>
                                                Horário: De Segunda à Sexta de 09:00 às 17:00 horas
                                            </td>
                                        </tr>
                                    </table>

                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default panel-suporte">
                            <div class="panel-heading" role="tab" id="headingCertificado">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" href="<?=base_url("autenticacao")?>" aria-expanded="false" aria-controls="collapseCertificado">
                                        <i class="glyphicon glyphicon-plus"></i> Autenticar Certificado
                                    </a>
                                </h4>
                            </div>
                        </div>
                        <div class="panel panel-default panel-suporte">
                            <div class="panel-heading" role="tab" id="headingFAQ">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFAQ" aria-expanded="false" aria-controls="collapseFAQ">
                                        <i class="glyphicon glyphicon-plus"></i> Perguntas Frequentes
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseFAQ" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFAQ">
                                <div class="panel-body">

                                    <div class="panel-group" id="faq" role="tablist" aria-multiselectable="true">
                                       <div class="panel panel-default panel-faq">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#faq" href="#faq-41" aria-expanded="true" aria-controls="collapseOne">
                                                      Como utilizar o curso bônus?
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="faq-41" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="faq-41">
                                                <div class="panel-body">
                                                Para utilizar um curso bônus você deverá estar logado no sistema, acessar a área de estudos onde contém seus cursos matriculados e clicar no botão usar bônus.  Em seguida abrirá uma janela de busca para digitação do curso de sua preferência. Para finalizar a troca você deverá clicar no botão usar bônus e a partir desse momento o curso estará disponível na sua área de estudos.
                                                </div>
                                            </div>
                                        </div> 
                                        
                                        <div class="panel panel-default panel-faq">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#faq" href="#faq-40" aria-expanded="true" aria-controls="collapseOne">
                                                      O que é curso bônus?
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="faq-40" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="faq-40">
                                                <div class="panel-body">
                                                Curso bônus é aquele curso que o aluno ganha a cada curso comprado com carga horária igual ou superior a 100 (cem) horas. Ressaltamos que a carga horária e a data de matrícula do curso bônus é igual à carga horário do curso comprado.
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="panel panel-default panel-faq">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#faq" href="#faq-1" aria-expanded="true" aria-controls="collapseOne">
                                                        O curso é reconhecido pelo MEC?
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="faq-1" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="faq-1">
                                                <div class="panel-body">
                                                    O Mec reconhece apenas cursos Técnicos, de Graduação e Pós-Graduação. No nosso caso ofertamos cursos Livres Profissionalizantes. Que são válidos no mercado de trabalho, mas não necessitam de reconhecimento pelo MEC.
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="panel panel-default panel-faq">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#faq" href="#faq-2" aria-expanded="true" aria-controls="collapseOne">
                                                        Os cursos tem validade?
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="faq-2" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="faq-2">
                                                <div class="panel-body">
                                                    Como qualquer outra organização, temos que estar legalmente amparados para a execução de nossas atividades. Como trabalhamos com cursos de formação continuada de trabalhadores nas várias áreas, nossa base legal encontra-se na Lei N° 9394/96, Art 40, § 2º, Inciso I e Resolução CNE/CEB Nº 6/2012, Art. 25º, Decreto 5154/2004, Art. 3°. que tratam da educação profissional. Este base legal agora está sendo impressa nos certificados para que possa ser consultada por aqueles que analisarem os certificados quando da admissão para emprego.
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="panel panel-default panel-faq">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#faq" href="#faq-3" aria-expanded="true" aria-controls="collapseOne">
                                                        Após o pagamento com quando tempo posso acessar os cursos?
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="faq-3" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="faq-3">
                                                <div class="panel-body">
                                                    Após o pagamento sua transação é processada em até 72 horas úteis. Mesmo que não receba o e-mail de confirmação você deve fazer o login no site e verificar se o curso foi liberado.
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="panel panel-default panel-faq">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#faq" href="#faq-4" aria-expanded="true" aria-controls="collapseOne">
                                                        Vocês oferecem cursos de Ensino Fudamental e Médio?
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="faq-4" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="faq-4">
                                                <div class="panel-body">
                                                    Não. Os cursos ofertados são profissionalizantes livres e não são de supletivo ou equivalente.
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="panel panel-default panel-faq">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#faq" href="#faq-5" aria-expanded="true" aria-controls="collapseOne">
                                                        Esqueci a senha que cadastrei o que faço?
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="faq-5" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="faq-5">
                                                <div class="panel-body">
                                                    Para recuperar sua senha basta ir ao link Login. Em seguida digite seu e-mail cadastrado e Clique Esqueci minha senha. O sistema lhe enviará sua senha.
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="panel panel-default panel-faq">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#faq" href="#faq-6" aria-expanded="true" aria-controls="collapseOne">
                                                       Recebo certificados por e-mail?
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="faq-6" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="faq-6">
                                                <div class="panel-body">
                                                    Não. Os certificados são enviados exclusivamente através do site, em sua página pessoal ou pelo correio.
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="panel panel-default panel-faq">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#faq" href="#faq-7" aria-expanded="true" aria-controls="collapseOne">
                                                       Os cursos são a distância?
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="faq-7" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="faq-7">
                                                <div class="panel-body">
                                                    Sim. Os cursos são totalmente on line, não existem momentos presenciais. As avaliações são realizadas através do site. Você não precisa se ausentar de sua residência, caso tenha acesso a internet.
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="panel panel-default panel-faq">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#faq" href="#faq-8" aria-expanded="true" aria-controls="collapseOne">
                                                       Quais as formas de pagamento oferecidas?
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="faq-8" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="faq-8">
                                                <div class="panel-body">
                                                    Você pode pagar através de Boleto Bancário, Debito em Conta e Cartão de Crédito. Os pagamentos com cartão de crédito não podem ser parcelados.
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="panel panel-default panel-faq">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#faq" href="#faq-9" aria-expanded="true" aria-controls="collapseOne">
                                                         Quanto tempo leva para concluir o curso?
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="faq-9" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="faq-9">
                                                <div class="panel-body">
                                            Cursos de 10 Horas --> 2 Dias<br />										
											Cursos de 20 Horas --> 3 Dias<br />
											Cursos de 30 Horas --> 4 Dias<br />
                                            Cursos de 40 Horas --> 5 Dias<br />
											Cursos de 50 Horas --> 7 Dias<br />
                                            Cursos de 60 Horas --> 8 Dias<br />
                                            Cursos de 70 Horas --> 9 Dias<br />
                                            Cursos de 80 Horas --> 10 Dias<br />
                                            Cursos de 90 Horas --> 12 Dias<br />
                                            Cursos de 100 Horas --> 13 Dias<br />
                                            Cursos de 110 Horas  --> 14 Dias<br />
                                            Cursos de 120 Horas --> 15 Dias<br />
                                            Cursos de 130 Horas --> 17 Dias<br />
                                            Cursos de 140 Horas --> 18 Dias<br />
                                            Cursos de 150 Horas --> 19 Dias<br />
                                            Cursos de 160 Horas --> 20 Dias<br />
                                            Cursos de 170 Horas --> 22 Dias<br />
                                            Cursos de 180 Horas --> 23 Dias<br />
                                            Cursos de 190 Horas --> 24 Dias<br />
                                            Cursos de 200 Horas --> 25 Dias<br />
                                            Cursos de 210 Horas --> 27 Dias<br />
                                            Cursos de 220 Horas --> 28 Dias<br />
                                            Cursos de 230 Horas --> 29 Dias<br />
                                            Cursos de 240 Horas --> 30 Dias <br />
                                            Cursos de 120 Horas --> 15 Dias<br />
                                            Cursos de 130 Horas --> 17 Dias<br />
                                            Cursos de 140 Horas --> 18 Dias<br />
                                            Cursos de 150 Horas --> 19 Dias<br />
                                            Cursos de 160 Horas --> 20 Dias<br />
                                            Cursos de 170 Horas --> 22 Dias<br />
                                            Cursos de 180 Horas --> 23 Dias<br />
                                            Cursos de 190 Horas --> 24 Dias<br />
                                            Cursos de 200 Horas --> 25 Dias<br />
                                            Cursos de 210 Horas --> 27 Dias<br />
                                            Cursos de 220 Horas --> 28 Dias<br />
                                            Cursos de 230 Horas --> 29 Dias<br />
                                            Cursos de 240 Horas --> 30 Dias <br />
                                                      
                                            
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="panel panel-default panel-faq">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#faq" href="#faq-10" aria-expanded="true" aria-controls="collapseOne">
                                                         Como faço para acessar os cursos?
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="faq-10" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="faq-10">
                                                <div class="panel-body">
                                                    Basta ir ao site e digitar seus dados de login e senha. Seu login é o e-mail cadastrado e a senha foi a que você criou no momento do cadastro. Na área interna leia inicialmente o Manual Instrutivo, antes de iniciar os cursos.
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="panel panel-default panel-faq">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#faq" href="#faq-11" aria-expanded="true" aria-controls="collapseOne">
                                                        Posso fazer cursos de áreas diferentes?
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="faq-11" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="faq-11">
                                                <div class="panel-body">
                                                    Sim. Você tem acesso a todos os cursos de todas as áreas, sem restrições.
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="panel panel-default panel-faq">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#faq" href="#faq-12" aria-expanded="true" aria-controls="collapseOne">
                                                        Qual o valor de cada curso
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="faq-12" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="faq-12">
                                                <div class="panel-body">
                                                    Os cursos variam de R$14,99 até R$129,99, alguns podem estar em promoção.
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <!--<div class="panel panel-default panel-faq">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#faq" href="#faq-13" aria-expanded="true" aria-controls="collapseOne">
                                                        Após o pagamento do certificado, quanto tempo leva para chegar em minha residência?
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="faq-13" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="faq-13">
                                                <div class="panel-body">
                                                   A empresa, após receber seu pagamento, leva até 7 dias para imprimir e postar seu certificado. O correio pode chegar a 15 dias para efetivar a entrega. No total, seu certificado pode levar até 22 dias para ser entregue. Sendo que são prazos máximos, na maioria das vezes leva de 10 a 15 dias.
                                                </div>
                                            </div>
                                        </div>-->
                                        
                                        <div class="panel panel-default panel-faq">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#faq" href="#faq-14" aria-expanded="true" aria-controls="collapseOne">
                                                        Quais as informações que vem impressas no certificado?   
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="faq-14" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="faq-14">
                                                <div class="panel-body">
                                                   Nome do curso, seu nome completo, Tipo de curso, Aproveitamento, número de registro do certificado, carga horária, data de matrícula no site, data de conslusão do curso, local, legislação em que se apoia o curso, data da impressão, assinatura digitalizada do diretor, assinatura do aluno.
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <!--<div class="panel panel-default panel-faq">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#faq" href="#faq-15" aria-expanded="true" aria-controls="collapseOne">
                                                        O site emite Histórico Escolar?   
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="faq-15" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="faq-15">
                                                <div class="panel-body">
                                                   Sim. Todos os cursos realizados ficam gravados e compõe seu histórico escolar.
                                                </div>
                                            </div>
                                        </div>-->
                                        
                                        <div class="panel panel-default panel-faq">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#faq" href="#faq-16" aria-expanded="true" aria-controls="collapseOne">
                                                        Quando terá inicio o curso em que estou matriculado(a)?
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="faq-16" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="faq-16">
                                                <div class="panel-body">
                                                   Você pode iniciar o curso logo que recebe o e-mail de confirmação do pagamento, ou 48 horas após o pagamento, caso não receba o e-mail, digitando seu e-mail e senha cadastrado no site .
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="panel panel-default panel-faq">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#faq" href="#faq-17" aria-expanded="true" aria-controls="collapseOne">
                                                       Além da taxa de inscrição tenho que pagar mensalidades?
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="faq-17" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="faq-17">
                                                <div class="panel-body">
                                                   Não. O valor é pago apenas a cada ano e durante os meses de acesso não são cobradas mensalidades.
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="panel panel-default panel-faq">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#faq" href="#faq-18" aria-expanded="true" aria-controls="collapseOne">
                                                       Os certificados dos cursos são aceitos em concursos públicos?
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="faq-18" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="faq-18">
                                                <div class="panel-body">
                                                    Sim. Todos os certificados são aceitos em concursos públicos e outras seleções. Verifique no edital as exigências dos títulos.
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="panel panel-default panel-faq">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#faq" href="#faq-19" aria-expanded="true" aria-controls="collapseOne">
                                                       Existe alguma outra forma de avaliação que não seja o Teste Objetivo?
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="faq-19" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="faq-19">
                                                <div class="panel-body">
                                                    Não. A única forma de avaliação é o Teste Objetivo. Que deve ser realizado exclusivamente pelo formulário no site.
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="panel panel-default panel-faq">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#faq" href="#faq-20" aria-expanded="true" aria-controls="collapseOne">
                                                       Se eu escolher fazer a cada mês um curso, vou receber um certificado anual contendo todos ou por cada curso?
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="faq-20" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="faq-20">
                                                <div class="panel-body">
                                                    Não. Você recebe um certificado por cada curso em que for aprovado.
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="panel panel-default panel-faq">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#faq" href="#faq-21" aria-expanded="true" aria-controls="collapseOne">
                                                       Existe algum prazo para fazer a inscrição?
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="faq-21" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="faq-21">
                                                <div class="panel-body">
                                                   Não. Você pode fazer sua matrícula a qualquer tempo. Caso tenha se cadastrado e seu boleto tenha passado da validade, basta comprar novamente o curso.
                                                </div>
                                            </div>
                                        </div>
                                        
                                         <div class="panel panel-default panel-faq">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#faq" href="#faq-22" aria-expanded="true" aria-controls="collapseOne">
                                                       Quais os requisitos e os horários dos cursos?
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="faq-22" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="faq-22">
                                                <div class="panel-body">
                                                   Para se matricular basta ter conhecimentos de uso do computador e de internet. Não precisa ter curso superior ou médio.
                                            Não existem horários, você pode acessar a qualquer hora, pois os cursos são pela internet e não existe nenhum momento presencial. 
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="panel panel-default panel-faq">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#faq" href="#faq-23" aria-expanded="true" aria-controls="collapseOne">
                                                       Paguei minha matrícula já faz mais de 48 horas, como tenho acesso aos cursos?
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="faq-23" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="faq-23">
                                                <div class="panel-body">
                                                    Vá direto no site, não espere o e-mail de conformação. Digite o e-mail e senha cadastrada no site. Dessa forma você irá ter acesso aos serviços.
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="panel panel-default panel-faq">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#faq" href="#faq-24" aria-expanded="true" aria-controls="collapseOne">
                                                       Quantos cursos posso fazer?
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="faq-24" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="faq-24">
                                                <div class="panel-body">
                                                    Não existe nenhum limite, pode fazer quantos desejar.
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="panel panel-default panel-faq">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#faq" href="#faq-25" aria-expanded="true" aria-controls="collapseOne">
                                                       Quanto tempo dura cada curso?
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="faq-25" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="faq-25">
                                                <div class="panel-body">
                                                    Estabelecemos apenas o número mínimo de dias , mas você poderá fazer em mais dias.
                                           <br /> Cursos de 10 Horas --> 2 Dias<br />
											Cursos de 20 Horas --> 3 Dias<br />
											Cursos de 30 Horas --> 4 Dias<br />
                                            Cursos de 40 Horas --> 5 Dias<br />
											Cursos de 50 Horas --> 7 Dias<br />
                                            Cursos de 60 Horas --> 8 Dias<br />
                                            Cursos de 70 Horas --> 9 Dias<br />
                                            Cursos de 80 Horas --> 10 Dias<br />
                                            Cursos de 90 Horas --> 12 Dias<br />
                                            Cursos de 100 HOras --> 13 Dias<br />
                                            Cursos de 110 Horas  --> 14 Dias<br />
                                            Cursos de 120 Horas --> 15 Dias<br />
                                            Cursos de 130 Horas --> 17 Dias<br />
                                            Cursos de 140 Horas --> 18 Dias<br />
                                            Cursos de 150 Horas --> 19 Dias<br />
                                            Cursos de 160 Horas --> 20 Dias<br />
                                            Cursos de 170 Horas --> 22 Dias<br />
                                            Cursos de 180 Horas --> 23 Dias<br />
                                            Cursos de 190 Horas --> 24 Dias<br />
                                            Cursos de 200 Horas --> 25 Dias<br />
                                            Cursos de 210 Horas --> 27 Dias<br />
                                            Cursos de 220 Horas --> 28 Dias<br />
                                            Cursos de 230 Horas --> 29 Dias<br />
                                            Cursos de 240 Horas --> 30 Dias <br />
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="panel panel-default panel-faq">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#faq" href="#faq-26" aria-expanded="true" aria-controls="collapseOne">
                                                       A carga horária vem impressa no certificado?
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="faq-26" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="faq-26">
                                                <div class="panel-body">
                                                   Sim. Além da carga horária ainda no certificado vem impresso o nome do curso, seu nome, a titulação e a base legal em que se ampara. Devidamente assinado digitalmente e registrado sob numero de protocolo no site.
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="panel panel-default panel-faq">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#faq" href="#faq-27" aria-expanded="true" aria-controls="collapseOne">
                                                       Os certificados são válidos em todo o Brasil?
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="faq-27" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="faq-27">
                                                <div class="panel-body">
                                                  Sim. E todos eles vão com a assinatura digitalizada do diretor e com carimbo de autenticação.
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="panel panel-default panel-faq">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#faq" href="#faq-28" aria-expanded="true" aria-controls="collapseOne">
                                                       Como são emitidos os certificados?
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="faq-28" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="faq-28">
                                                <div class="panel-body">
                                                  Eles são impressos, assinados digitalmente, registrados e enviados pelo site ou pelo correio e tem a mesma validade de qualquer outro certificado de curso profissionalizante livre.
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="panel panel-default panel-faq">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#faq" href="#faq-29" aria-expanded="true" aria-controls="collapseOne">
                                                       Os certificados tem validade de curso técnico, graduação ou pós-graduação?
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="faq-29" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="faq-29">
                                                <div class="panel-body">
                                                  Não. Para ter validade de curso técnico, o curso tem de ser aprovado pelo Conselho de Educação de seu Estado. Se você faz um curso técnico presencial ou a distancia procure saber se ele é aprovado pelo Conselho estadual, ou não terá validade alguma. Os cursos de graduação ou de Pós-grduação tem que ser reconhecidos pelo MEC. No nosso caso, ofertamos apenas cursos Livres, que não necessitam de reconhecimento.
                                                </div>
                                            </div>
                                        </div>
                                        
                                         <div class="panel panel-default panel-faq">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#faq" href="#faq-30" aria-expanded="true" aria-controls="collapseOne">
                                                       Onde posso utilizar os certificados emitidos?
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="faq-30" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="faq-30">
                                                <div class="panel-body">
                                                  Em provas de títulos de concursos públicos, no pedido de gratificação por aperfeiçoamento profissional na empresa publica em que trabalha, dependendo das normas de seu plano de carreira, na elaboração de seu currículo profissional para apresentação em empresas privados ou publicas, dentre outros.
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="panel panel-default panel-faq">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#faq" href="#faq-31" aria-expanded="true" aria-controls="collapseOne">
                                                       O certificado é emitido por uma empresa com CNPJ (CGC)?
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="faq-31" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="faq-31">
                                                <div class="panel-body">
                                                  Sim. O mantenedor do Programa de Formação é a Cidade Aprendizagem, cujo CNPJ vai impresso no certificado para que tenha sua validade não contestada.
                                                </div>
                                            </div>
                                        </div>
                                        
                                         <div class="panel panel-default panel-faq">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#faq" href="#faq-32" aria-expanded="true" aria-controls="collapseOne">
                                                       Em quanto tempo recebo meu certificado?
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="faq-32" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="faq-32">
                                                <div class="panel-body">
                                                  Após a conclusão do curso o certificado já estará disponível para impressão na sua área de estudos.
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="panel panel-default panel-faq">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#faq" href="#faq-33" aria-expanded="true" aria-controls="collapseOne">
                                                       E se eu não receber o certificado no tempo indicado?
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="faq-33" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="faq-33">
                                                <div class="panel-body">
                                                  Você deve enviar e-mail para atendimento@imazoncursos.com.br, que enviaremos um novo certificado sem custos adicionais.
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="panel panel-default panel-faq">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#faq" href="#faq-34" aria-expanded="true" aria-controls="collapseOne">
                                                       Existe uma carga horária nos certificados?
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="faq-34" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="faq-34">
                                                <div class="panel-body">
                                                  Sim. Todos os títulos emitidos especificamente pelo programa levam a carga horária impressa no certificado, alem dos outros itens especificados na ficha de avaliação.
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="panel panel-default panel-faq">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#faq" href="#faq-35" aria-expanded="true" aria-controls="collapseOne">
                                                        Tenho 15 anos, posso enviar uma avaliação?
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="faq-35" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="faq-35">
                                                <div class="panel-body">
                                                  É aconselhável que somente pessoas com idade igual ou superior a 16 anos enviem avaliação.
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="panel panel-default panel-faq">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#faq" href="#faq-36" aria-expanded="true" aria-controls="collapseOne">
                                                        E quanto ao certificado, posso pedir uma segunda via?
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="faq-36" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="faq-36">
                                                <div class="panel-body">
                                                  Sim. A qualquer tempo será disponibilizada uma segunda via de seu certificado. Mas para que possa ser liberada terá que ser pago o mesmo valor que pagou pela primeira via.
                                                </div>
                                            </div>
                                        </div>
                                        
                                         <div class="panel panel-default panel-faq">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#faq" href="#faq-37" aria-expanded="true" aria-controls="collapseOne">
                                                        E se informei meu endereço errado no formulário que gera o boleto, posso mudar?
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="faq-37" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="faq-37">
                                                <div class="panel-body">
                                                  Sim, desde que seja parcialmente e no máximo 24 horas apos o pagamento do boleto. Para mudanças totais de endereço é necessário outras providências que o responsável lhe orientará. Envie e-mail urgente.
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="panel panel-default panel-faq">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#faq" href="#faq-38" aria-expanded="true" aria-controls="collapseOne">
                                                       O CRECI vai aceitar o certificado?
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="faq-38" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="faq-38">
                                                <div class="panel-body">
                                                  Voce deve entrar em contato com o CRECI de seu estado antes de realizar o curso, visto que cada órgão tem suas regras especificas.
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="panel panel-default panel-faq">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#faq" href="#faq-39" aria-expanded="true" aria-controls="collapseOne">
                                                       Junto com o certificado já vem a carteira do CRECI?
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="faq-39" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="faq-39">
                                                <div class="panel-body">
                                                  Não.Voce deverá procurar o creci de sua cidade a fim de obter as informaçoes necessárias para o processo de formalização da profissão de Corretor de Imóveis.
                                                </div>
                                            </div>
                                        </div>
                                        
                                        
                                         
                                        
                                        
                                        
                                        
                                        
                                        
                                    </div>


                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default panel-suporte">
                            <div class="panel-heading" role="tab" id="headingTermo">
                                <h4 class="panel-title">
                                    <a role="button" href="<?=base_url("termo_uso")?>" aria-expanded="true" aria-controls="collapseContatos">
                                        <i class="glyphicon glyphicon-plus"></i> Termo de uso
                                    </a>
                                </h4>
                            </div>
                        </div>
                        
                    </div>

                </div>
            </div>
        </div>
    </div>

</div>