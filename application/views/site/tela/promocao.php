<div class="row">
    <div class="col-md-12">
        <div class="conteudo-direito">

            <div class="panel panel-default panel-visualiza-curso">
                <div class="panel-heading" role="tab" id="headOne-promo . '">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion-promo" href="#collapseOne-promo" aria-expanded="true" aria-controls="collapseOne-' . md5($c->nome) . '">
                            Promoção Cursos em Dobro
                        </a>
                    </h4>
                </div>
                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headOne-promo">
                    <div class="panel-body">
                        A promoção cursos em dobro voltou.<br />
                        Agora, a cada matrícula em cursos com carga horária acima de 100 (cem) horas, o aluno ganha uma nova totalmente grátis.<br />
                        Regras:<br />
                       <?php
                        function somar_data($data, $dias, $meses, $ano){
                            $data = explode("/", $data);
                            $resData = date("d/m/Y", mktime(0, 0, 0, $data[1] + $meses, $data[0] + $dias, $data[2] + $ano));
                            return $resData;
                        }
                    
                        function subtrair_data($data, $dias, $meses, $ano){
                            $data = explode("/", $data);
                            $resData = date("d/m/Y", mktime(0, 0, 0, $data[1] - $meses, $data[0] - $dias, $data[2] - $ano));
                            return $resData;
                        }
                       
                       $hoje = date('28/09/2017');
                       
                       $mes_anteirior = subtrair_data($hoje, 0, 1, 0);
                       $mes_posterior = somar_data($hoje, 1, 1, 0);
                       
                       
                       ?>
                        A promoção é válida para as matrículas com pagamento entre o dia <?= $mes_anteirior ?> e  <?=date('d/m/Y', strtotime("+15 days",strtotime(date("d-m-Y")))); ?>.<br/>
                        
                        
                        O curso bônus fica disponível após a identificação do pagamento da fatura dos cursos comprados.<br />
                        Quando liberado o bônus, o aluno poderá escolher por qualquer curso disponível, desde que a carga horária seja igual a do curso pago.

<br />
                        Caso o aluno cancele o pagamento do curso comprado, o bônus também será cancelado.<br />
                        Uma vez usado o curso bônus, não poderá ser trocado.<br />
                        O aluno tem até 90 dias para usar o curso bônus.<br />
                        A data da matrícula é a mesma do curso comprado.<br />
                        O certificado web é gratuito para os cursos bônus desta promoção.<br />
                        As regras para a realização dos cursos bônus são iguais as dos demais cursos.<br />
                        Em caso de dúvidas entre em contato pelo e-mail atendimento@imazoncursos.com.br<br />
                        <br />
                        <a href="<?=base_url("cursos")?>" class="btn btn-success">Buscar Cursos</a>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
</div>

</div>