<?= doctype('html5'); ?>
<html>
    
<head>
<style>
    .panel-default {
        border-color: #DDD;
    }
    .panel {
        margin-bottom: 20px;
        background-color: #FFF;
        border: 1px solid transparent;
        border-radius: 4px;
        box-shadow: 0px 1px 1px rgba(0, 0, 0, 0.05);
    }
</style> 

<p>Para adicionar mais itens, basta navegar pelo site.</p>

<?php //echo'<script> //gtag("event", "conversion", {"send_to": "AW-1036809317/YPe0CP7nhWgQ5eix7gM",}); </script>'; ?>

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '939857572773480'); 
fbq('track', 'PageView');
</script>
<noscript>
 <img height="1" width="1" 
src="https://www.facebook.com/tr?id=939857572773480&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->  

<!-- Facebook Pixel Code idiomas  -->

<script>

!function(f,b,e,v,n,t,s)

{if(f.fbq)return;n=f.fbq=function(){n.callMethod?

n.callMethod.apply(n,arguments):n.queue.push(arguments)};

if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';

n.queue=[];t=b.createElement(e);t.async=!0;

t.src=v;s=b.getElementsByTagName(e)[0];

s.parentNode.insertBefore(t,s)}(window,document,'script',

'https://connect.facebook.net/en_US/fbevents.js');


fbq('init', '939857572773480'); 

fbq('track', 'PageView');

</script>

<noscript>

<!--<img height="1" width="1" 

src="https://www.facebook.com/tr?id=939857572773480&ev=PageView

&noscript=1"/>-->

</noscript>

<!-- End Facebook Pixel Code -->
</head>
<body>
    
<div class="row">
    <div class="col-md-12">

        <?php
        $total = 0;
        $this->table->set_template(array('table_open' => '<table class="table table-condensed">'));
        $this->table->set_heading('Itens', 'Valor');
        foreach ($this->cart->contents() as $c) {
            $bt = '<a href="' . base_url('site/delCarrinho') . '/' . base64_encode($c['rowid']) . '"  onclick="return confirm(\'Deseja realmente excluir o curso ' . $c['name'] . '?\')" type="button" class="btn btn-danger">Remover</button>';
            $this->table->add_row($c['name'], 'R$' . format_number($c['price']));
            $total += $c['price'];
        }

        echo $this->table->generate();
        //echo '<pre>Total: R$' . format_number($this->cart->total()) . '</pre>';
        ?>
    </div>
</div>
<p>Seu Pagamento será Processado por<a href="https://www.traycheckout.com.br/" target="_blank"> Traycheckout <img src="<?= base_url("imgs"); ?>/tray.png" width="40"> </a></p>

<div class="col-md-12" id="pedido">
    
<br /><br />
    <div class="form-group">
        <label for="Valor">Escolha a Forma de pagamento</label>   

        <div class="radio">
            <span data-toggle="collapse" data-parent="#accordion" href="#boleto" aria-expanded="true" aria-controls="collapseOne" class="btn btn-default">
                <i class="glyphicon glyphicon-barcode"></i>Boleto
            </span>
            <span class="collapsed btn btn-warning" role="button" data-toggle="collapse" data-parent="#accordion" href="#cartao" aria-expanded="false" aria-controls="collapseTwo">
                <i class="glyphicon glyphicon-credit-card"></i>Cartão
            </span>
            <span class="btn btn-info" role="button" data-toggle="collapse" data-parent="#accordion" href="#debito" aria-expanded="false" aria-controls="collapseTwo">
                <i class="glyphicon glyphicon-credit-card"></i>Débito
            </span>
            <span class="btn btn-success" role="button" data-toggle="collapse" data-parent="#accordion" href="#saldo" aria-expanded="false" aria-controls="collapseTwo">
                <i class="glyphicon glyphicon-usd"></i>Saldo
            </span>
        </div>
    </div>
</div>   

<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
        <div id="boleto" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
                <form action="<?= base_url('gerarPagamento') ?>" target="_blank" method="post" id="formulario">
                    <div class="form-group">
                        <label for="Identidade">CPF:</label>
                        <input name="Identidade" id="Identidade" onKeyPress="formataCampo(Identidade, '000.000.000-00', event);" autocomplete="off" maxlength="14" minlength="14" type="text" class="form-control" required>
                    </div>

                   

                    <input name="Valor" id="Valor" value="<?= $total ?>" type="hidden">
                    <input type="hidden" name="Forma" value="6"/>
                    <?php
                    if ($this->session->userdata('aluno_logado_imazon')) {
                        echo '<input type="submit" class="btn btn-success" value="Finalizar">';
                    } else {
                        echo '<input type="submit" class="btn btn-success" value="Finalizar">';
                    }
                    ?>
                </form>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div id="debito" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
                <form action="<?= base_url('gerarPagamento') ?>" target="_blank" method="post" id="formulario">
                    <div class="form-group">
                        <label for="fdebito">Escolha o Banco: </label><br>                         
                            <input name="Forma" id="Forma" value="22" required="" type="radio">
                            <img src="<?= base_url("imgs"); ?>/ico-bradesco.jpg">

                        <label class="radio-inline">
                            <input name="Forma" id="Forma" value="23" required="" type="radio">
                            <img src="<?= base_url("imgs"); ?>/ico-bb.jpg">

                        </label>  

                        <label class="radio-inline">
                            <input name="Forma" id="Forma" value="7" required="" type="radio">
                            <img src="<?= base_url("imgs"); ?>/ico-itau.jpg">

                        </label>
                        
                        <label class="radio-inline">
                            <input name="Forma" id="Forma" value="21" required="" type="radio">
                            <img src="<?= base_url("imgs"); ?>/ico-hsbc.jpg">
                        </label>
                        
                        <label class="radio-inline">
                            <input name="Forma" id="Forma" value="14" required="" type="radio">
                            <img src="<?= base_url("imgs"); ?>/ico-peela.jpg">
                        </label>                        
                         
                    </div>

                    <div class="form-group">
                        <label for="Identidade">CPF:</label>
                        <input name="Identidade" id="Identidade" onKeyPress="formataCampo(Identidade, '000.000.000-00', event);" autocomplete="off" maxlength="14" minlength="14" type="text" class="form-control" required>           </div>                 

                    <input name="Valor" id="Valor" value="<?= $total ?>" type="hidden">

                    <?php
                    if ($this->session->userdata('aluno_logado_imazon')) {
                        echo '<input type="submit" class="btn btn-success" value="Finalizar">';
                    } else {
                        echo '<input type="submit" class="btn btn-success" value="Finalizar">';
                    }
                    ?>
                </form>
            </div>
        </div>
    </div>  


    <div class="panel panel-default">
        <div id="cartao" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
            <div class="panel-body">
                
                <form action="<?= base_url('gerarPagamento') ?>" target="_blank" method="post" id="formulario">

                    <div class="form-group">
                        <label for="cartao">Escolha a Bandeira do Cart&atilde;o: <br> <br> </label>
                        <br>
                        <label class="radio-inline">
                            <input name="Forma" id="Forma" value="3" required="" type="radio">
                            <img src="<?= base_url("imgs"); ?>/ico-visa.jpg">
                        </label>    

                        <label class="radio-inline">
                            <input name="Forma" id="Forma" value="4" required="" type="radio">
                            <img src="<?= base_url("imgs"); ?>/ico-m.jpg">
                        </label>
                        <label class="radio-inline">    
                            <input name="Forma" id="Forma" value="5" required="" type="radio">
                            <img src="<?= base_url("imgs"); ?>/ico-a.jpg">
                        </label>    
                        <label class="radio-inline">    
                            <input name="Forma" id="Forma" value="2" required="" type="radio">
                            <img src="<?= base_url("imgs"); ?>/ico-dinners.jpg">
                        </label>    
                        <label class="radio-inline">    
                            <input name="Forma" id="Forma" value="20" required="" type="radio">
                            <img src="<?= base_url("imgs"); ?>/ico-hipercard.jpg">
                        </label>    
                        <label class="radio-inline">
                            <input name="Forma" id="Forma" value="16" required="" type="radio">
                            <img src="<?= base_url("imgs"); ?>/ico-elo.jpg">
                        </label>    
                        <label class="radio-inline">    
                            <input name="Forma" id="Forma" value="18" required="" type="radio">
                            <img src="<?= base_url("imgs"); ?>/ico-aura.jpg">
                        </label>
                        
                         <label class="radio-inline">    
                            <input name="Forma" id="Forma" value="19" required="" type="radio">
                            <img src="<?= base_url("imgs"); ?>/ico-jcb.jpg">
                        </label>  
                        
                         <label class="radio-inline">    
                            <input name="Forma" id="Forma" value="15" required="" type="radio">
                            <img src="<?= base_url("imgs"); ?>/ico_discovery.jpg">
                        </label>
                              
                    </div>

            </div>

            <div class="form-group">
                <label for="CartaoNumero">Número do Cartão:</label>
                <input name="CartaoNumero" autocomplete="off"  id="CartaoNumero" minlength="14" maxlength="22" type="text" class="form-control" required>
            </div>  

            <div class="form-group">
                <label for="NomePortador">Nome do Portador (Igual ao do Cartão):</label>
                <input name="NomePortador" id="NomePortador" autocomplete="off" type="text" class="form-control" required> 
            </div>


            <div class="form-group">
                <label for="Expiracao">Validade (Ex: 08/<?= date("y") + 1 ?>):</label>
                <input name="Expiracao" id="Expiracao"  onKeyPress="formataCampo(Expiracao, '99/99', event);"  autocomplete="off" maxlength="5" type="text" class="form-control" required> 
            </div>

            <div class="form-group">
                <label for="CodigoSeguranca">Código de Segurança (Geralmente impresso no verso do Cartão. Ex:495):</label>
                <input name="CodigoSeguranca" id="CodigoSeguranca" autocomplete="off" maxlength="4" type="text" class="form-control" required>
            </div>
                  <div class="form-group">
                <label for="Identidade">CPF:</label>
                <input name="Identidade" id="Identidade" onKeyPress="formataCampo(Identidade, '000.000.000-00', event);" autocomplete="off" minlength="14" maxlength="14" type="text" class="form-control" required>
            </div>
            
            <input name="TipoCartao" id="TipoCartao" type="hidden" value="Parcelado">
            <input name="Valor" id="Valor" value="<?= $total ?>" type="hidden">
            <?php
            if ($this->session->userdata('aluno_logado_imazon')) {
                echo '<input type="submit" class="btn btn-success" value="Finalizar">';
            } else {
                echo '<input type="submit" class="btn btn-success" value="Finalizar">';
            }
            ?>
            </form>

        </div>
    </div>
    
    <div class="panel panel-default">
        <div id="saldo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
                <label>Seu Saldo é de: <strong>R$ <?php echo number_format($saldo, 2, ',', ' ') ?> </strong></label>
                <div id="container_saldo" style="display: none;">
                <div id="form_saldo" id="formulario">
                    
                    <div class="alert alert-success" role="alert"><strong>Muito bom! </strong>Saldo suficiente para a realização desta compra. Ao concluir esta compra Seu saldo será de <strong>R$ <?php $novo_saldo = $saldo-$preco; echo $saldo_formatado = number_format($novo_saldo, 2, ',', ' ')?></strong></div>
                    
                    <div class="form-group">
                        <?php
                        
                        if($saldo >=$preco)
                        {
                            echo '<input id="idq_u" type="hidden" value="'.$id_fic.'">';
                            echo '<input id="idq_p" type="hidden" value="'. $preco_codificado.'">';
                        }
                        ?>
                    </div>

                    <input name="Valor" id="Valor" value="<?= $total ?>" type="hidden">
                    <input type="hidden" name="Forma" value="6"/>
                    <?php
                    if ($this->session->userdata('aluno_logado_imazon')) {
                        echo '<input id="finalizar_saldo" type="submit" class="btn btn-success" value="Finalizar">';
                        echo '<div id="loading"></div>';
                    } else {
                        echo '<input id="finalizar_saldo" type="submit" class="btn btn-success" value="Finalizar">';
                        echo '<div id="loading"></div>';
                    }
                    ?>
                </div>
                </div>
                <div id="warning_saldo" style="display: none">
                    <div class="alert alert-warning" role="alert"><strong>Infelizmente seu Saldo é Insuficiente para a realização desta compra.</strong></div>
                </div>
            </div>
        </div>
    </div>
    
</div>
<br /><br /><br /><br /><br />
</div>

<div class="modal fade preloader-modal" tabindex="-1" role="dialog">

  <div class="modal-dialog">

    <div class="modal-content">

      <div class="modal-body">

      </div>

    </div><!-- /.modal-content -->

  </div><!-- /.modal-dialog -->

</div><!-- /.modal -->

</body>

</html>
<script>
//  $("#button_saldo").click( function(){
    
    var saldo = <?php echo $saldo;?>;
    var preco = <?php echo $preco;?>;
    var cart = <?php echo json_encode($cart); ?>;
            
    var base_url = "<?= base_url('') ?>";
    
//    /<div class="alert alert-warning" role="alert">...</div>
    if(saldo >=preco)
    {
        $("#container_saldo").show("slow");
        
        $("input#finalizar_saldo").click( function(){
            $("#finalizar_saldo").remove();
             $("#loading").html("<button  class='btn btn-success loading'>Aguarde <i class='fa fa-spinner fa-spin'></i></button>");
            $('.loading').prop('disabled', true);
            var idq_u = $("#idq_u").val();
            var idq_p = $("#idq_p").val();
            
            $.ajax({
                    method: "POST",
                    url: base_url+"api/auth",
                    data: { idq_u: idq_u, idq_p: idq_p, cart: cart },
                    success: function(response) {
                        
                        if(response==='sucesso')
                        {
                            window.location.href = base_url+'site/finalizar_compra_saldo';
                        }
                        else if(response==='saldo_insuficiente')
                        {
                             $(".loading").remove();
                             $("#loading").html("<button  class='btn btn-warning'>Saldo Insuficiente <i class='fa fa-frown-o'></i></button>");
                             setTimeout(function() { window.location=window.location;},5000);
                        }
                        else if(response==='falha')
                        {
                            $(".loading").remove();
                            $("#loading").html("<button  class='btn btn-danger'>Falha na Operação <i class='fa fa-thumbs-down'></i></button>");
                            
                            setTimeout(function() { window.location=window.location;},3000);
                        }
                        else if(response==='client_inexistente')
                        {
                            $(".loading").remove();
                            $("#loading").html("<button  class='btn btn-danger'>Cliente não existe <i class='fa fa-exclamation-triangle'></i></button>");
                            location.reload();
                        }
                            
                    }     
                  });
            
            
        } );
    }
    else
    {
        $("#warning_saldo").show("slow");
    }
    
//} );
</script>
<script>
     $("form").submit(function(){
        
        $(".modal-footer").remove();

        $(".btn-fechar-modal").remove();

        $(".preloader-modal").modal('show');

        $(".modal-body").html('<div class="progress"><div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%; color: #fff; font-weight: bold;">Aguarde, sua transa&ccedil;&atilde;o est&aacute; sendo processada...</div></div>');

        setTimeout(function(){
            window.location.href = "cursos";
        }, 2000);

    });
</script>