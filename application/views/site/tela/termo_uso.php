
<div class="row">
    <div class="col-md-12">
        <div class="conteudo-direito">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default panel-suporte">
                    <div class="panel-heading" role="tab" id="headingContato">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse">
                                <i class="glyphicon glyphicon-info-sign"></i> Termo de uso
                            </a>
                        </h4>
                    </div>
                    <div id="collapseContatos" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingContato">
                        <div class="panel-body" id="termo-uso">
                            <?= nl2br(strip_tags($termo_uso[0]->conteudo)) ?>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
</div>

</div>