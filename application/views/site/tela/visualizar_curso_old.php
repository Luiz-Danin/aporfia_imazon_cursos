<div class="col-md-9">
    <div style="height: 20px;"></div>
    <div class="conteudo-direito">
        <div class="row">
            <div class="col-md-4 col-xs-4 col-lg-4">
                <?php
                if (!$this->seguranca_model->valida_login_aluno()) {
                    $url_matricula = 'redir_cad_matricula';
                } else {
                    $url_matricula = 'matricular_curso';
                }

                if ($imagem !== "") {
                    $url = base_url("imgs/cursos/$imagem");
                } else {
                    $url = base_url("imgs/sem-foto.png");
                }

                echo '<img src="' . $url . '" class="img-visualiza-curso"/>';
                ?>
            </div>
            <div class="col-md-8 col-xs-8 col-lg-8">
                <i class="glyphicon glyphicon-time"></i>
                Carga Horária: <span class="tempo"></span> horas  
                <div style="height: 13px;"></div>
                <div class="btn-group" role="group" aria-label="...">
                    <a style="margin: auto auto;" class="btn btn-success matricular-curso btn-group-sm"  data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample" valor="<?= base64_encode($id) ?>"><i class="glyphicon glyphicon-plus-sign"></i> Mais carga horária</a>
                </div>
                <div style="height: 16px;"></div>
                <div class="collapse" id="collapseExample">
                    <div class="well">

                        <p>Selecione a carga horária desejada:
                        </p>
                        <form action="<?= base_url($url_matricula) ?>" method="post">
                            <select id="carga_horaria" name="carga_horaria" class="form-control">
                                <?php
                                if ($this->uri->segment(4) !== "blog") {
                                    $i = 1;
                                    $carga_horarias = $this->pm->get_all_carga_horaria();
                                    foreach ($carga_horarias as $ch) {
                                        echo '<option class="carga_horaria" duracao="' . $ch->tempo . ' " valor="' . $ch->valor . ' " value="' . codifica($ch->tempo) . '">' . $ch->tempo . ' horas </option>';
                                    }
                                }
                                ?>
                            </select> 
                            <br />     
                            <input type="hidden" name="curso" value="<?= codifica($id) ?>">
                            <input type="submit" value="Matrícular" class="btn btn-success">      
                            </div>
                            </div>
                            <div class="panel panel-default panel-visualiza-curso">
                                <div class="panel-heading" role="tab" id="headOne-promo . '">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion-promo" href="#collapseOne-promo" aria-expanded="true" aria-controls="collapseOne-' . md5($c->nome) . '">
                                            Promoção Cursos em Dobro
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headOne-promo">
                                    <div class="panel-body">
                                        A cada matrícula você ganha uma nova totalmente grátis.<br />
                                        <a href="<?= base_url("promocao") ?>" target="_blank">+Detalhes</a>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default panel-visualiza-curso">
                                <div class="panel panel-default panel-visualiza-curso">
                                    <div class="panel-heading" role="tab" id="headTwo">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                Investimento
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headTwo">
                                        <div class="panel-body">
                                            R$<span class="valor-curso"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-heading" role="tab" id="headFive">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                            Impostos
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseFive" class="panel-collapse  collapse in" role="tabpanel" aria-labelledby="headFive">
                                    <div class="panel-body">
                                        R$<span class="valor-imposto"></span>
                                    </div>
                                </div>

                            </div>
                            <div class="panel panel-default panel-visualiza-curso">
                                <div class="panel-heading" role="tab" id="headTwo">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            Duração
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headTwo">
                                    <div class="panel-body">
                                        <span class="duracao-dias"></span> dias <br />
                                        <span style="font-size: 10px;">Você pode prorrogar a conclusão para até 90 dias.</span>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default panel-visualiza-curso">
                                    <div class="panel-heading" role="tab" id="headOne">
                                        <h4 class="panel-title">
                                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#detalhe-curso" aria-expanded="true" aria-controls="detalhe-curso">
                                                Conteúdo
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="detalhe-curso" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headOne">
                                        <div class="panel-body">
<?= nl2br($descricao) ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-default panel-visualiza-curso">
                                    <div class="panel-heading" role="tab" id="headThree">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                Tipo
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseThree" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headThree">
                                        <div class="panel-body">
                                            Capacitação - Curso livre 
                                        </div>
                                    </div>

                                </div>
                                <div class="panel panel-default panel-visualiza-curso">

                                    <div class="panel-heading" role="tab" id="headFour">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                                Serviços
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseFour" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headFour">
                                        <div class="panel-body">
                                            <p>
                                                -Inscrição
                                                <br />
                                                -Matrícula
                                                <br />
                                                -Conteúdo
                                                <br />
                                                -Vídeos, Lívros, Biblioteca Digital
                                                <br />
                                                -Certificado grátis
                                                <br />
                                                -Declaração de matrícula
                                                <br />
                                                -Validação de Certificado
                                                <br />
                                                -Notificações por email
                                                <br />
                                                -Prova online
                                                <br />
                                                -Reteste
                                                <br />
                                                -2ª chance                                
                                                <br />
                                                -Pagamento online
                                                <br />
                                                -Envio de documentos pelos correios
                                                <br />
                                                -Atendimento via Telefone, email ou help desk
                                            </p>
                                        </div>
                                    </div>

                                </div>

                                <div class="panel panel-default panel-visualiza-curso">
                                    <div class="panel-heading" role="tab" id="headFour">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#Certificado" aria-expanded="false" aria-controls="Certificado">
                                                <i class="glyphicon glyphicon-plus"></i> Modelo de Certificado
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="Certificado" class="panel-collapse collapse collapse in" role="tabpanel" aria-labelledby="Certificado">
                                        <div class="panel-body">
                                            <img src="<?= base_url("template/img/thumb-certificado_web.png") ?>" class="img-thumbnail">
                                            <img src="<?= base_url("template/img/thumb_certificado_web_verso.png") ?>" class="img-thumbnail">
                                        </div>
                                    </div>
                                </div> 
                                <br />
                                <input type="submit" value="Matrícular" class="btn btn-success">
                                </form>
                                <a href="<?= base_url('cursos') ?>" onclick="history.go(-1)" class="btn btn-primary btn-group-sm" style="margin-left:5px;">Voltar</a>
                            </div>

                    </div>
                </div>

            </div>
            <div style="display: none;" class="carga-horaria-curso">
<?php
if ($this->uri->segment(4) !== "blog") {
    $i = 1;
    $carga_horarias = $this->pm->get_all_carga_horaria();
    foreach ($carga_horarias as $ch) {
        if ($i == 1) {
            echo '<div class="radio">
                                                        <label>
                                                            <input type="radio" name="carga_horaria" value="' . $ch->id_produto . '" checked>
                                                            ' . ucfirst($ch->nome) . ' (R$' . $ch->valor . ')
                                                        </label> 
                                                    </div>';
        } else {
            echo '<div class="radio">
                                                        <label>
                                                            <input type="radio" name="carga_horaria" value="' . $ch->id_produto . '">
                                                            ' . ucfirst($ch->nome) . ' (R$' . $ch->valor . ')
                                                        </label> 
                                                    </div>';
        }
        $i++;
    }
}
?>
            </div>
            <div class="blank_30"></div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="fb-share-button" data-href="<?= current_url() ?>" data-layout="button_count"></div>
            </div>
        </div>
    </div>

</div>

<script>
    $(document).on('change', '#carga_horaria', function () {
        var valor_curso = $(this).find("option:selected").attr('valor');
        var duracao = $(this).find("option:selected").attr('duracao');
        $(".valor-imposto").html((parseFloat(valor_curso) * 0.128));
        var imposto = $(".valor-imposto").html();
        imposto = imposto.replace(".", ",");
        imposto = imposto.split(",");
        $(".valor-imposto").html(imposto[0] + ',' + imposto[1].substr(0, 2));
        $(".valor-curso").html(valor_curso.replace(".", ","));
        $(".duracao-dias").html(Math.ceil(parseInt(duracao) / 8));
        $(".tempo").html(duracao);
    });

    $(document).ready(function () {
        var valor_curso = $(this).find("option:selected").attr('valor');
        var duracao = $(this).find("option:selected").attr('duracao');
        $(".valor-imposto").html((parseFloat(valor_curso) * 0.128));
        var imposto = $(".valor-imposto").html();
        imposto = imposto.replace(".", ",");
        imposto = imposto.split(",");
        $(".valor-imposto").html(imposto[0] + ',' + imposto[1].substr(0, 2));
        $(".valor-curso").html(valor_curso.replace(".", ","));
        $(".duracao-dias").html(Math.ceil(parseInt(duracao) / 8));
        $(".tempo").html(duracao);
    });


</script>