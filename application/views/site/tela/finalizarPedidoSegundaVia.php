<style>
    .panel-default {
        border-color: #DDD;
    }
    .panel {
        margin-bottom: 20px;
        background-color: #FFF;
        border: 1px solid transparent;
        border-radius: 4px;
        box-shadow: 0px 1px 1px rgba(0, 0, 0, 0.05);
    }
</style> 

<div class="row">
    <div class="col-md-12">

        <?php
        
        $total = 0;
        $this->table->set_template(array('table_open' => '<table class="table table-condensed">'));
        $this->table->set_heading('Itens');
        
        foreach ($itens_pedido as $c) {
            $this->table->add_row("Matrícula no curso " . $c->nome);
        }

        echo $this->table->generate();
        
        echo '<pre>Total: R$' . format_number($total_compra) . '</pre>';
        ?>
    </div>
</div>
<p>Seu Pagamento será Processado por<a href="https://www.traycheckout.com.br/" target="_blank"> Traycheckout <img src="<?= base_url("imgs"); ?>/tray.png" width="40"> </a></p>

<div class="col-md-12" id="pedido">
    
<br /><br />
    <div class="form-group">
        <label for="Valor">Escolha a Forma de pagamento</label>   

        <div class="radio">
            <span data-toggle="collapse" data-parent="#accordion" href="#boleto" aria-expanded="true" aria-controls="collapseOne" class="btn btn-default">
                <i class="glyphicon glyphicon-barcode"></i>Boleto
            </span>
            <span class="collapsed btn btn-warning" role="button" data-toggle="collapse" data-parent="#accordion" href="#cartao" aria-expanded="false" aria-controls="collapseTwo">
                <i class="glyphicon glyphicon-credit-card"></i>Cartão
            </span>
            <span class="btn btn-info" role="button" data-toggle="collapse" data-parent="#accordion" href="#debito" aria-expanded="false" aria-controls="collapseTwo">
                <i class="glyphicon glyphicon-credit-card"></i>Débito
            </span>
        </div>
    </div>
</div>   

<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
        <div id="boleto" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
                <form action="<?= current_url() ?>" target="_blank" method="post" id="formulario">
                    <div class="form-group">
                        <label for="Identidade">CPF:</label>
                        <input name="Identidade" id="Identidade" onKeyPress="formataCampo(Identidade, '000.000.000-00', event);" autocomplete="off" maxlength="14" minlength="14" type="text" class="form-control" required>
                    </div>

                   

                    <input name="Valor" id="Valor" value="<?= $total ?>" type="hidden">
                    <input type="hidden" name="Forma" value="6"/>
                    <input type="submit" class="btn btn-success" value="Finalizar">
                   
                </form>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div id="debito" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
                <form action="<?= current_url() ?>" target="_blank" method="post" id="formulario">
                    <div class="form-group">
                        <label for="fdebito">Escolha o Banco: </label><br>                         
                            <input name="Forma" id="Forma" value="22" required="" type="radio">
                            <img src="<?= base_url("imgs"); ?>/ico-bradesco.jpg">

                        <label class="radio-inline">
                            <input name="Forma" id="Forma" value="23" required="" type="radio">
                            <img src="<?= base_url("imgs"); ?>/ico-bb.jpg">

                        </label>  

                        <label class="radio-inline">
                            <input name="Forma" id="Forma" value="7" required="" type="radio">
                            <img src="<?= base_url("imgs"); ?>/ico-itau.jpg">

                        </label>
                        
                        <label class="radio-inline">
                            <input name="Forma" id="Forma" value="21" required="" type="radio">
                            <img src="<?= base_url("imgs"); ?>/ico-hsbc.jpg">
                        </label>
                        
                        <label class="radio-inline">
                            <input name="Forma" id="Forma" value="14" required="" type="radio">
                            <img src="<?= base_url("imgs"); ?>/ico-peela.jpg">
                        </label>                        
                         
                    </div>

                    <div class="form-group">
                        <label for="Identidade">CPF:</label>
                        <input name="Identidade" id="Identidade" onKeyPress="formataCampo(Identidade, '000.000.000-00', event);" autocomplete="off" maxlength="14" minlength="14" type="text" class="form-control" required>           </div>                 

                    <input name="Valor" id="Valor" value="<?= $total ?>" type="hidden">
                    <input type="submit" class="btn btn-success" value="Finalizar">
                </form>
            </div>
        </div>
    </div>  


    <div class="panel panel-default">
        <div id="cartao" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
            <div class="panel-body">

                <form action="<?= current_url() ?>" target="_blank" method="post" id="formulario">

                    <div class="form-group">
                        <label for="cartao">Escolha a Bandeira do Cart&atilde;o: <br> <br> </label>
                        <br>
                        <label class="radio-inline">
                            <input name="Forma" id="Forma" value="3" required="" type="radio">
                            <img src="<?= base_url("imgs"); ?>/ico-visa.jpg">
                        </label>    

                        <label class="radio-inline">
                            <input name="Forma" id="Forma" value="4" required="" type="radio">
                            <img src="<?= base_url("imgs"); ?>/ico-m.jpg">
                        </label>
                        <label class="radio-inline">    
                            <input name="Forma" id="Forma" value="5" required="" type="radio">
                            <img src="<?= base_url("imgs"); ?>/ico-a.jpg">
                        </label>    
                        <label class="radio-inline">    
                            <input name="Forma" id="Forma" value="2" required="" type="radio">
                            <img src="<?= base_url("imgs"); ?>/ico-dinners.jpg">
                        </label>    
                        <label class="radio-inline">    
                            <input name="Forma" id="Forma" value="20" required="" type="radio">
                            <img src="<?= base_url("imgs"); ?>/ico-hipercard.jpg">
                        </label>    
                        <label class="radio-inline">
                            <input name="Forma" id="Forma" value="16" required="" type="radio">
                            <img src="<?= base_url("imgs"); ?>/ico-elo.jpg">
                        </label>    
                        <label class="radio-inline">    
                            <input name="Forma" id="Forma" value="18" required="" type="radio">
                            <img src="<?= base_url("imgs"); ?>/ico-aura.jpg">
                        </label>
                        
                         <label class="radio-inline">    
                            <input name="Forma" id="Forma" value="19" required="" type="radio">
                            <img src="<?= base_url("imgs"); ?>/ico-jcb.jpg">
                        </label>  
                        
                         <label class="radio-inline">    
                            <input name="Forma" id="Forma" value="15" required="" type="radio">
                            <img src="<?= base_url("imgs"); ?>/ico_discovery.jpg">
                        </label>
                              
                    </div>


            </div>

           



            <div class="form-group">
                <label for="CartaoNumero">Número do Cartão:</label>
                <input name="CartaoNumero" autocomplete="off"  id="CartaoNumero" minlength="14" maxlength="22" type="text" class="form-control" required>
            </div>  

            <div class="form-group">
                <label for="NomePortador">Nome do Portador (Igual ao do Cartão):</label>
                <input name="NomePortador" id="NomePortador" autocomplete="off" type="text" class="form-control" required> 
            </div>


            <div class="form-group">
                <label for="Expiracao">Validade (Ex: 08/<?= date("y") + 1 ?>):</label>
                <input name="Expiracao" id="Expiracao"  onKeyPress="formataCampo(Expiracao, '99/99', event);"  autocomplete="off" maxlength="5" type="text" class="form-control" required> 
            </div>

            <div class="form-group">
                <label for="CodigoSeguranca">Código de Segurança (Geralmente impresso no verso do Cartão. Ex:495):</label>
                <input name="CodigoSeguranca" id="CodigoSeguranca" autocomplete="off" maxlength="4" type="text" class="form-control" required>
            </div>
                  <div class="form-group">
                <label for="Identidade">CPF:</label>
                <input name="Identidade" id="Identidade" onKeyPress="formataCampo(Identidade, '000.000.000-00', event);" autocomplete="off" minlength="14" maxlength="14" type="text" class="form-control" required>
            </div>
            
            <input name="TipoCartao" id="TipoCartao" type="hidden" value="Parcelado">
            <input name="Valor" id="Valor" value="<?= $total ?>" type="hidden">
            <input type="submit" class="btn btn-success" value="Finalizar">
            </form>

        </div>
    </div>
</div>
<br /><br /><br /><br /><br />
</div>

<div class="modal fade preloader-modal" tabindex="-1" role="dialog">

  <div class="modal-dialog">

    <div class="modal-content">

      <div class="modal-body">

      </div>

    </div><!-- /.modal-content -->

  </div><!-- /.modal-dialog -->

</div><!-- /.modal -->


<script>

     $("form").submit(function(){
        
        $(".modal-footer").remove();

        $(".btn-fechar-modal").remove();

        $(".preloader-modal").modal('show');

        $(".modal-body").html('<div class="progress"><div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%; color: #fff; font-weight: bold;">Aguarde, sua transa&ccedil;&atilde;o est&aacute; sendo processada...</div></div>');

        setTimeout(function(){
            window.location.href = "cursos";
        }, 2000);

    });

</script>