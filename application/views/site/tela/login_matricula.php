    <div class="conteudo-pagina">
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active"><a href="#">Login</a></li>
        </ol>
        <div class="row">
            <div class="col-md-12">
                <div class="conteudo-direito">
                    <p>Digite os seus dados de acesso</p>
                    <form class="form-signin" action="<?= base_url("logar_matricular") ?>" method="post">
                        <label for="inputEmail" class="sr-only">Email</label>
                        <input type="email" id="inputEmail" class="form-control" name="email" placeholder="Email" required autofocus>
                        <br />
                        <label for="inputPassword" class="sr-only">Senha</label>
                        <input type="password" id="inputPassword" name="senha" class="form-control" placeholder="Senha" required>
                        <br />
                        <?php
                        if ($curso && $carga_horaria) {
                            echo '<input type="hidden" name="curso" value="' . $curso . '"/>';
                            echo '<input type="hidden" name="carga_horaria" value="' . $carga_horaria . '"/>';
                        }
                        ?>

                        <button class="btn btn-success" type="submit">Entrar</button>
                        <a href="<?=base_url("rec_senha")?>" class="btn btn-default">Recuperar senha</a>
                        <hr>

                        <p>Ainda não possui cadastro? <a href="<?= base_url("matricular_cadastrar/$curso/$carga_horaria") ?>" style="color:#0033FF">Clique aqui</a> e cadastre-se!</p>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>