<?php
echo doctype('html5');
if (!isset($termo)) {
    $meta = array('Content-type' => 'text/html; charset=iso-8859-1', 'description' => 'My Great site', 'viewport' => 'width=device-width, initial-scale=1', 'name' => 'robots', 'content' => 'no-cache');
} else {
    $meta = array('Content-type' => 'text/html; charset=iso-8859-1', 'description' => $termo, 'viewport' => 'width=device-width, initial-scale=1', 'name' => 'robots', 'content' => 'no-cache');
}
?>
<html>
    <head>
        <?= meta($meta); ?>
        <title>{titulo} | {palavra_chave}</title>         
        <!-- Latest compiled and minified CSS -->
        <?= link_tag('' . base_url() . 'vendor/twbs/bootstrap/dist/css/bootstrap.min.css'); ?>
        <!-- Google fonts -->
        <?= link_tag('https://fonts.googleapis.com/css?family=Montserrat'); ?>
        <!--Bootstrap Social Icons-->
        <?= link_tag('node_modules/bootstrap-social/bootstrap-social.css'); ?>
        <?= link_tag('node_modules/bootstrap-social/assets/css/font-awesome.css'); ?>
        
        <!--Animate Bootstrap Icons-->
        <?= link_tag('vendor/components/animate.css/animate.min.css'); ?>
        <!--Estilo-->
        <?= link_tag('area_aluno/css/estilo.css'); ?>
        <!--Reset-->
        <?= link_tag('area_aluno/css/reset.css'); ?>
        <!--jQuery-->
        <?= '<script src="' . base_url() . 'vendor/components/jquery/jquery.min.js"></script>'; ?>
        <!-- Latest compiled and minified JavaScript -->
        <?= '<script src="' . base_url() . 'vendor/twbs/bootstrap/dist/js/bootstrap.min.js"></script>'; ?>

        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="<?= base_url() ?>area_aluno/css/jasny-bootstrap.min.css">

        <!-- Latest compiled and minified JavaScript -->
        <script src="<?= base_url() ?>area_aluno/js/jasny-bootstrap.min.js"></script>

        <script src="<?= base_url() ?>area_aluno/js/mascara.js"></script>

        <script src="http://ironsummitmedia.github.io/startbootstrap-grayscale/js/jquery.easing.min.js"></script>
        <script src="//oss.maxcdn.com/jquery.form/3.50/jquery.form.min.js"></script>
        
        
    <div id="fb-root"></div>
    <script>(function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id))
                return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.5";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
</head>
<body>

    <header>
        <div class="topo-involucro">
            <div class="row">
                <div id="custom-bootstrap-menu" class="navbar navbar-default navbar-fixed-top" role="navigation">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <a class="navbar-brand" href="#" >
                               <?= img(base_url().'/imgs/logo.png', TRUE); ?>
                            </a>
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-menubuilder"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                            </button>
                        </div>
                        <div class="collapse navbar-collapse navbar-menubuilder">
                            <ul class="nav navbar-nav navbar-right menu-principal">
                                <li><a class="page-scroll" href="<?= base_url() ?>">Home</a>
                                </li>
                                <li><a class="page-scroll" href="<?= base_url('cursos') ?>">Cursos</a>
                                </li>
                                <?php
                                if ($this->session->userdata('imazon_logado')) {
                                    echo '<li><a href="' . base_url('cursos_matriculados') . '" class="btn-nav-estudar">Estudar</a>';
                                } else {
                                    echo '<li>'
                                    . '<a class="page-scroll" href="#" data-toggle="modal" data-target="#modal-cadastro">Cadastro</a>' .
                                    '</li>' .
                                    '<li><a class="page-scroll" href="home#login">Login</a>';
                                }
                                ?>

                                </li>
                                <li><a class="page-scroll" href="<?= base_url('blog') ?>">Blog</a>
                                <li><a class="page-scroll" href="<?= base_url('suporte') ?>">Suporte</a>
                                </li>
                                <li>                                        
                                    <form class="navbar-form form-busca-topo" role="search" action="<?= base_url('buscar_curso') ?>" method="post">
                                        <div class="input-group">
                                            <input type="text" class="form-control busca" placeholder="O que deseja?" name="busca" name="busca" style="display:none;">
                                            <span class="btn-topo-busca">
                                                <i class="glyphicon glyphicon-search"></i>
                                            </span>
                                        </div>
                                    </form>
                                </li>
                                <?php
                                if ($this->session->userdata('imazon_logado')) {
                                    echo '<li class="dropdown sidebar-list">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <span class="glyphicon glyphicon-user"></span></a>
                                                <ul class="dropdown-menu">
                                                    <li><a href="' . base_url('logout') . '">Sair</a></li>
                                                </ul>
                                            </li>';
                                }

                                if ($this->cart->total_items() > 0) {
                                    echo '<li class="nav-menu-carrinho"><a href="' . base_url("listaCompras") . '#itens-carrinho"><i class="glyphicon glyphicon-shopping-cart icone-carrinho"></i></a></li>';
                                }

                                if ($this->session->userdata('imazon_logado')) {
                                    if (!$this->notificacao_model->verifica_dados_aluno()) {
                                        echo '<li class=""><a href="#" style="color: orange;" class="notifica-dados-aluno" data-container="body" data-toggle="popover" data-placement="left" data-content="Você ainda não preencheu os seus dados cadastrais!"><i class="glyphicon glyphicon-info-sign"></i></a></li>';
                                    }
                                    if (!$this->notificacao_model->notifica_faturas_abertas()) {
                                            echo '<li class=""><a href="'.base_url('faturas').'" style="color: red;" class="notifica-dados-aluno" data-container="body" data-toggle="popover" data-placement="left" data-content="Você possui faturas em aberto"><i class="glyphicon glyphicon-usd"></i></a></li>';
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>            
        </div><!--topo-involucro-->

    </header>


    <div class="involucro-conteudo-pagina">

        <div class="titulo-pagina">
            <div class="conteudo-titulo">
                <h1>{titulo}</h1>
            </div>
        </div>

        <div class="conteudo-pagina">
            <!--<ol class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">{titulo}</a></li>
            </ol>
            <!--Gerar bredcrum dinamicamente-->