<footer>
    <div class="rodape-involucro">
        <div class="row">
            <div class="col-md-2 col-lg-3 col-xs-3 rodape-esquerdo">
                <ul>
                    <li><a href="<?= base_url("termo") ?>">Termo de uso</a></li>
                    <li><a href="<?= base_url("autenticacao") ?>">Autenticador</a></li>
                    <li><a id="btn-modal-cadastrar" data-toggle="modal" data-target="#modal-cadastro">Cadastro</a></li>
                </ul>
            </div>
            <div class="col-md-8 col-lg-6 col-xs-6 rodape-centro">
                <ul>
                    <li><a href="<?= base_url("termo") ?>">Copyright &COPY; Imazon Cursos 2007/<?= date("Y") ?></a></li>
                    <li><a href="<?= base_url("termo") ?>">E-mail: atendimento@imazoncursos.com.br</a></li>
                    <!--<li><a href="<?= base_url("termo") ?>">Telefone: 4004-0435 Ramal:8229</a></li>-->
                </ul>
            </div>
            <div class="col-md-2  col-lg-3 col-xs-2 rodape-direito">
                <ul>
                    <li><a href="<?= base_url("suporte") ?>">Suporte</a></li>
                    <li><a href="<?= base_url("blog") ?>">Blog</a></li>
                    <li><a href="<?= base_url("cursos") ?>">Cursos</a></li>
                </ul>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12" style="padding: 14px 0px 9px 14px;">
                <a href="https://www.tray.com.br/" target="_blank">
                    <img src="<?= base_url("imgs/TrayCheckout.png") ?>" style="width:65px; margin-right: 1%;">
                </a>     
                <a href="http://lattes.cnpq.br/" target="_blank">
                    <img src="<?= base_url("imgs/cnpq-lattes.jpeg") ?>" style="width:89px; margin-right:1%;"/>
                </a>
                <a href="http://di.cnpq.br/di/cadi/infoInstituicao.do?acao=buscaDadosInst&nroIdInst=744645" target="_blank">
                    <img src="<?= base_url("imgs/cadi.jpg") ?>" style="width:226px; margin-right:1%;"/>
                </a>
                <a href="http://www.abed.org.br/site/pt/associados/consulta_associados_abed/?busca_rapida=cidade+aprendizagem" target="_blank">
                    <img src="<?= base_url("imgs/abed.png") ?>" style="width:102px; margin-right:1%;"/>
                </a>
                <a href="https://cidadeaprendizagem.com.br/entidade/imazon+cursos" target="_blank">
                    <img src="<?= base_url("imgs/logo-cidade.jpg") ?>" style="width:158px; margin-right:1%;"/>
                </a>  
                <a href="https://www.google.com/transparencyreport/safebrowsing/diagnostic/?hl=pt-BR#url=imazoncursos.com.br" target="_blank">
                    <img src="<?= base_url("imgs/icone-google.png") ?>" style="width:140px; margin-right:1%;"/>
                </a>
                <span id="ss_img_wrapper_115-55_image_en"><a href="http://www.alphassl.com/ssl-certificates/wildcard-ssl.html" target="_blank" title="SSL Certificates"><img alt="Wildcard SSL Certificates" border=0 id="ss_img" src="//seal.alphassl.com/SiteSeal/images/alpha_noscript_115-55_en.gif" title="SSL Certificate"></a></span><script type="text/javascript" src="//seal.alphassl.com/SiteSeal/alpha_image_115-55_en.js"></script>
            </div>    
        </div>
    </div>
</div>    
</footer>
</body>
<script src="template/js/script-min.js"></script>

<script type="text/javascript">


    $(function () {
        $(".alterar-data-matricula").on("click", function () {
            var tdi = $(this).attr("tdi");
            if (tdi) {
                $("#modalAlteraDataMatricula").modal("show");
                $('[name="tdi"]').val(tdi);
            }
            return false;
        });
    });


    $(function () {
        $('.datepicker').keypress(function (e) {
            var keyCode = (e.keyCode ? e.keyCode : e.which);
            if (keyCode > 7 && keyCode < 58) {
                e.preventDefault();
            }
        });
    });

    $(function () {
        $(".btn-salvar-data-matricula").on("click", function () {
            var data = $('.datepicker').val();
            if (!data) {
                return false;
            }
        });
    });

    $(function () {
        $('.datepicker').datepicker({
            dateFormat: 'dd-mm-yy',
            dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado', 'Domingo'],
            dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D'],
            dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom'],
            monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
            monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
            minDate: 0,maxDate:new Date(<?=date("Y")?>,12,31)
        });
    });

    function queryObj() {
        var result = {}, keyValuePairs = location.search.slice(1).split("&");
        keyValuePairs.forEach(function (keyValuePair) {
            keyValuePair = keyValuePair.split('=');
            result[decodeURIComponent(keyValuePair[0])] = decodeURIComponent(keyValuePair[1]) || '';
        });
        return result;
    }
    var myParam = queryObj();


    $(function () {
        $("#busca-topo").popover({content: '<form class="form-inline" action="<?= base_url('buscar_curso') ?>" method="post">' +
                    '<div class="form-group">' +
                    '<div class="input-group">' +
                    '<input type="text" class="form-control" name="busca" required placeholder="O que deseja estudar?">' +
                    '</div>' +
                    '</div>' +
                    '<button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-search"></i></button>' +
                    '</form>',
            html: true});
    });

    $("#cep").blur(function () {
        var cep_code = $(this).val();
        if (cep_code.length <= 0)
            return;
        $.ajax({
            type: 'GET',
            url: 'https://viacep.com.br/ws/' + cep_code + '/json/',
            crossDomain: true,
            success: function (result) {
                if (result) {
                    if (result.erro != true) {
                        $(".estado").each(function () {
                            if ($(this).val() == result.uf) {
                                $(this).prop("selected", "true");
                            }
                        });
                        //$("input#cep").val(result.cep);
                        $("input#cidade").val(result.localidade);
                        $("input#bairro").val(result.bairro);
                        $("input#endereco").val(result.logradouro);
                        $("input#estado").val(result.uf);
                    }
                }

            }
        });
    });

    $("#cep").on("keydown", function () {
        $(this).inputmask({
            mask: '99999999'
        });
    });
</script>
<?php $this->session->unset_userdata('rescad'); ?>
</html>