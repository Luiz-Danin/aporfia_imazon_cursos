<?= doctype('html5'); ?>
<html>
    <head>

        <link rel="apple-touch-icon" sizes="57x57" href="<?= base_url("favicon") ?>/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="<?= base_url("favicon") ?>/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?= base_url("favicon") ?>/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?= base_url("favicon") ?>/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?= base_url("favicon") ?>/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?= base_url("favicon") ?>/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="<?= base_url("favicon") ?>/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?= base_url("favicon") ?>/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?= base_url("favicon") ?>/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="<?= base_url("favicon") ?>/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?= base_url("favicon") ?>/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="<?= base_url("favicon") ?>/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url("favicon") ?>/favicon-16x16.png">
        <link rel="manifest" href="<?= base_url("favicon") ?>/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="<?= base_url("favicon") ?>/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
        <meta name="description" content="{titulo}">
        <meta name="keywords" content="{titulo}">
        <meta name="author" content="{palavra_chave} com Certificado">
        <?= header("Content-Type: text/html;  charset=utf-8", true); ?>
        <title>{titulo} | {palavra_chave} com Certificado</title>         
        <!-- Latest compiled and minified CSS -->
        <?= link_tag('' . base_url() . 'vendor/twbs/bootstrap/dist/css/bootstrap.min.css'); ?>
        <!-- Google fonts -->
        <?= link_tag('https://fonts.googleapis.com/css?family=Montserrat'); ?>
        <!--Bootstrap Social Icons-->
        <?= link_tag('node_modules/bootstrap-social/bootstrap-social.css'); ?>
        <?= link_tag('node_modules/bootstrap-social/assets/css/font-awesome.css'); ?>
        <!--Animate Bootstrap Icons-->
        <?= link_tag('vendor/components/animate.css/animate.min.css'); ?>
        <!--Estilo-->
        <?= link_tag('template/css/estilo.css'); ?>
        <!--Reset-->
        <?= link_tag('template/css/reset.css'); ?>
        <!--jQuery-->
        <?= '<script src="' . base_url() . 'vendor/components/jquery/jquery.min.js"></script>'; ?>
        <!-- Latest compiled and minified JavaScript -->
        <?= '<script src="' . base_url() . 'vendor/twbs/bootstrap/dist/js/bootstrap.min.js"></script>'; ?>

        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="<?= base_url() ?>template/css/jasny-bootstrap.min.css">

        <!-- Latest compiled and minified JavaScript -->
        <script src="<?= base_url() ?>template/js/jasny-bootstrap.min.js"></script>

        <script src="<?= base_url() ?>template/js/mascara.js"></script>
        <script src="<?= base_url() ?>jquery-input-mask/jquery.inputmask.bundle.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
        <script src="//oss.maxcdn.com/jquery.form/3.50/jquery.form.min.js"></script>
    <div id="fb-root"></div>
    <script>(function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id))
                return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.5";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-62601651-1', 'auto');
        ga('send', 'pageview');
    </script>

</head>

<!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-N68ZNJP');</script>
<!-- End Google Tag Manager -->

<body>
    <header>
        <div class="fundo-blog"></div>
        <div class="topo-involucro">
            <div class="row">
                <div id="custom-bootstrap-menu" class="navbar navbar-default navbar-fixed-top" role="navigation">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <a class="navbar-brand" href="#" >
                                <?= img(base_url() . '/imgs/logo.png', TRUE); ?>
                            </a>
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-menubuilder"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                            </button>
                        </div>
                        <div class="collapse navbar-collapse navbar-menubuilder">
                            <ul class="nav navbar-nav navbar-right menu-principal">
                                <?php if (!$this->session->userdata('imazon_logado')) { ?>
                                    <li><a class="page-scroll" href="<?= base_url('site/home') ?>">Home</a>
                                    </li>
                                <?php } ?>
                                <li><a class="page-scroll" href="<?= base_url('cursos') ?>">Cursos</a>
                                </li>
                                <li><a class="page-scroll" href="<?= base_url('autenticacao') ?>">Certificado</a>
                                </li>
                                <?php
                                if ($this->session->userdata('imazon_logado')) {
                                    $id_aluno = $this->session->userdata('imazon_id_aluno');
                                    $id_aluno = codifica($id_aluno);
                                    
                                    echo '<li><a href="' . base_url('cursos_matriculados') . '" class="btn-nav-estudar">Estudar</a>';
                                    echo '<li><a href="' . base_url('ganhe/cliente/index/').$id_aluno . '" class="btn-nav-estudar">Indique e Ganhe</a>'; 
                                } else {
                                    echo '<li>'
                                    . '<a class="page-scroll" href="#" local="' . base_url() . '" id="btn-modal-cadastrar" data-toggle="modal" data-target="#modal-cadastro">Cadastro</a>' .
                                    '</li>' .
                                    '<li><a class="page-scroll" href="#" local="' . base_url() . '"  id="btn-modal-cadastrar" data-toggle="modal" data-target="#modal-cadastro">Login</a>';
                                }
                                ?>

                                </li>
                               
                                <!--<li><a class="page-scroll" href="<?= base_url('blog') ?>">Blog</a>-->
                                <li><a class="page-scroll" href="<?= base_url('suporte') ?>">Suporte</a>
                                </li>

                                <?php
                                if ($this->session->userdata('imazon_logado')) {
                                    echo '<li class="dropdown sidebar-list">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <span class="glyphicon glyphicon-user"></span></a>
                                                <ul class="dropdown-menu">
                                                    <li><a href="' . base_url('dados_cadastrais') . '">Meus dados</a></li>
                                                    <li><a href="' . base_url('logout') . '">Sair</a></li>
                                                </ul>
                                            </li>';
                                }

                                if ($this->cart->total_items() > 0) {
                                    echo '<li class="nav-menu-carrinho"><a href="' . base_url("listaCompras") . '"><i class="glyphicon glyphicon-shopping-cart icone-carrinho"></i></a></li>';
                                }

                                if ($this->session->userdata('imazon_logado')) {
                                    if (!$this->notificacao_model->verifica_dados_aluno()) {
                                        echo '<li class=""><a href="' . base_url("dados_cadastrais") . '" style="color: orange;" class="notifica-dados-aluno" data-container="body" data-toggle="popover" data-placement="left" data-content="Você ainda não preencheu os seus dados cadastrais!"><i class="glyphicon glyphicon-info-sign"></i></a></li>';
                                    }
                                    if (!$this->notificacao_model->notifica_faturas_abertas()) {
                                        echo '<li class=""><a href="' . base_url('faturas') . '" style="color: red;" class="notifica-dados-aluno" data-container="body" data-toggle="popover" data-placement="left" data-content="Você possui faturas em aberto"><i class="glyphicon glyphicon-usd"></i></a></li>';
                                    }
                                }
                                ?>
                                <li>      
                                    <a class="page-scroll" id="busca-topo" href="#busca" data-container="body" data-toggle="popover" data-placement="bottom">
                                        <span>
                                            <i class="glyphicon glyphicon-search"></i>
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <?php
                        if ($this->session->userdata('imazon_logado')) {
                            echo '<p style="float: right; font-size: 12px; margin-top: -3px;">Logado como: ' . $this->session->userdata('imazon_nome') . '</p>';
                        }
                        ?>
                    </div>
                </div>
            </div>            
        </div><!--topo-involucro-->
    </header>
    <div class="involucro-conteudo-pagina">

        <div class="titulo-pagina">
            <div class="conteudo-titulo">
                <h1>{titulo}</h1>
            </div>
        </div>

        <div class="conteudo-pagina">

            <?php
            if ($this->session->flashdata('success')) {
                $tipo = 'success';
            }
            if ($this->session->flashdata('danger')) {
                $tipo = 'danger';
            }
            if ($this->session->flashdata('warning')) {
                $tipo = 'warning';
            }
            if (isset($tipo)) {
                echo '<div class="alert alert-' . $tipo . '">' . $this->session->flashdata($tipo) . '</div>';
            }
            if ($this->session->flashdata('alerta-carrinho')) {
                echo '<div class="blank_20"></div><div class="alert alert-info" id="alerta"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>' . $this->session->flashdata('alerta-carrinho') . '</div>';
            }
            ?>

            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <form id="frm_busca_curso" action="<?= base_url('buscar_curso') ?>" method="post">
                        <div class="input-group">
                            <input type="text" class="form-control input-lg" placeholder="Digite uma Palavra" name="busca">
                            <span class="input-group-addon" id="sizing-addon2" onClick="$('#frm_busca_curso').submit();">
                                <i class="glyphicon glyphicon-search" style="cursor:auto"></i>
                            </span>
                        </div>
                    </form>
                </div>
            </div>
            <div class="blank_20"></div>