<noscript>
</noscript>
<?php
$situacao = "TESTE";
switch ($historico[0]->id_avaliacao) {
    case 1:
        $situacao = "Teste";
        break;
    case 2:
        $situacao = "Segunda-chance";
        break;

    case 3:
        $situacao = "Reteste";
        break;
    
    default:
        $situacao = "Teste";
        break;
}

?>
<div class="row">
    <div class="col-md-12 col-xs-12">
        <div class="blank_35"></div>
        <div class="conteudo-direito" id="prova">
             <pre>
Confira os dados que aparecerão em seu Certificado. Caso não concorde não realize o teste:
Aluno:<?=$historico[0]->nome?>

Curso: <?=$nomeCurso?>

Avaliação: <?=$situacao?>

Carga Horária: <?=$historico[0]->carga_horaria?> Horas
				  
Data Matrícula: <?=date("d/m/Y", strtotime($historico[0]->primeiro_dia))?>

Data Conclusão: <?=date("d/m/Y")?>

Leia Atentamente as questões e marque a opção correta. Quando terminar, clique no botão "Corrigir Avaliação".
Boa Prova!</pre>
            <form action="<?=base_url('corrigir_avaliacao')?>" method="post">
                <?php
                $i = 1;
                foreach ($questoes as $q) {
                    echo '<div class="media">
                            <div class="media-body">
                              <h4 class="media-heading">' . $i . 'ª Questão</h4>
                              ' . nl2br($q->descricao) . '
                              <br /><br />
                              <label class="radio-inline">
                                <input type="radio" name="' . $q->id_questao . '" id="questao-' . $q->id_questao . '" value="1" required> Verdadeiro
                              </label>
                              <label class="radio-inline">
                                 <input type="radio" name="' . $q->id_questao . '" id="questao-' . $q->id_questao . '" value="0" required> Falso
                              </label>
                            </div>
                          </div><hr>';
                    $i++;
                }
                ?>
                  <div class="form-group">
                    <div class="col-md-12">
                      <button type="submit" class="btn btn-success">Corrigir Avaliação</button>
                    </div>
                  </div>
            </form>
        </div>

    </div>
</div>
</div>

</div>