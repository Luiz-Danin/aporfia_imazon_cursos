<style>
.bs-glyphicons-list {
    list-style: outside none none;
    padding-left: 0;
}
ol, ul {
    margin-bottom: 10px;
    margin-top: 0;
}
.bs-glyphicons li {
    font-size: 12px;
    width: 12.5%;
}
.bs-glyphicons li {
    background-color: #f9f9f9;
    border: 1px solid #fff;
    float: left;
    font-size: 10px;
    line-height: 1.4;
    padding: 10px;
    text-align: center;
}

.lista-opcoes {
    height: 120px;
    width: 120px; 
    float: left;
    padding-left: 8px;
    text-align: center;
}

.bs-glyphicons .glyphicon {
    font-size: 24px;
    margin-bottom: 10px;
    margin-top: 5px;
    
}
.glyphicon-lista{
    font-size: 30px;
    margin: auto auto;
}
span{cursor: pointer;}
</style>
<?php 
foreach($curso as $c);
?>  
    
    <div class="col-md-9 col-xs-12" style="padding:1px 14px;">
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="row">
                    <div class="col-md-10 col-xs-7">
                        <h3><?=$c->nome?> </h3>
                    </div>
                </div>
                
                <h4><b>Apresenta&ccedil;&atilde;o</b></h4>
                
                <p><?=$c->apresentacao3?></p>
                <hr>
                
                <h4><b>Objetivo</b></h4>
                <p><?=$c->objetivo3?></p>
                
                 <hr>
                 <div class="row">
                     <div class="col-md-12 col-xs-12">
                         <h4><b>Status</b></h4> 
                    <?php
                        if(isset($certificado[0]->status))
						
                        switch ((int)$certificado[0]->status) {
                            case 0:
                                echo '<span class="btn btn-info">Curso em andamento</span>';
                                break;
                            case 1:
                                echo '<span class="btn btn-success">Curso conclu&iacute;do</span>';
                                break;
                            case 2:
                                echo '<span class="btn btn-danger">Curso cancelado</span>';
                                break;
                            case 3:
                                echo '<span class="btn btn-info">Curso jubilado</span>';
                                break;
                            case 4:
                                echo '<span class="btn btn-info">Curso arquivado</span>';
                                break;
                            
                            default:
                                break;
                        }
                    ?>
                    <hr>
                 </div>
                </div> 
                
                <div class="row">
                     <div class="col-md-12 col-xs-12">
                         <h4><b>Conte&uacute;do do curso</b></h4>   
                <br />
                <div id="aguarde" class="well well-lg" style="width:350px; margin: auto; text-align: center; display: none; clear:both;">
                   <div class="progress">
                    <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                      <span class="sr-only"></span>
                    </div>
                  </div>
                    <h5>Aguarde..</h5>
                </div>
                
                <br />
               
                     </div>
                </div>
                
                <div class="progress">
                   <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                       <span style="color: #fff; font-weight: bold; font-size: 15px;">Carregando...</span>
                   </div>
                </div>
                
                <div role="tabpanel">

                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                      <li role="presentation" class="active">
                          <a href="#home" aria-controls="home" role="tab" data-toggle="tab">
                              <span aria-hidden="true" style="color: #3895F2;" class="glyphicon glyphicon-file glyphicon-lista"></span><br /><b>Aulas</b>
                          </a>
                      </li>
                      <li role="presentation">
                          <a href="#profile" id="mais-livros" data="<?=$c->nome?>" aria-controls="profile" role="tab" data-toggle="tab">
                              <span aria-hidden="true" style="color: #DFF238;" class="glyphicon glyphicon-book glyphicon-lista"  id="livros"></span><br /><b>L&iacute;vros</b>
                          </a>
                      </li>
                      <li role="presentation">
                          <a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">
                              <span aria-hidden="true" style="color: #38F238;" class="glyphicon glyphicon-bold glyphicon-lista" data="<?=$c->nome?>" id="biblioteca"></span><br /><b>Biblioteca</b>
                          </a>
                      </li>
                      <li role="presentation">
                          <a href="#videos" id="mais-videos" aria-controls="videos" role="tab" data-toggle="tab" data="<?=$c->nome?>">
                              <span aria-hidden="true" style="color: #F23838;" class="glyphicon glyphicon-facetime-video glyphicon-lista" ></span><br /><b>V&iacute;deos</b>     
                          </a>
                      </li>
                    </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                  <div role="tabpanel" class="tab-pane active" id="home">
                      <hr>
                     <table class="table table-bordered">
                            <?php
                                foreach($aulas_curso as $ac){
                                    echo '<tr><td><a href="'.base_url('aluno/curso/curso_aula').'/'.$this->uri->segment(4).'/'.$ac->cursos_aulas_id.'"> <span class="badge">'.$ac->CAOrdem.'.</span> '.$ac->CATitulo.'</a></td></tr>';
                                }
                            ?>

                        </table>
                  </div>
                  <div role="tabpanel" class="tab-pane" id="profile">
                      <hr>
                      <div id="lista-livros">                    
                      </div>
                  </div>
                  <div role="tabpanel" class="tab-pane" id="messages">
                      <hr>
                      <ul class="media-list">
                          <li>
                              <a href="http://www.dominiopublico.gov.br/pesquisa/PesquisaObraForm.jsp" target="_blank">
                                  <i class="glyphicon glyphicon-book"></i>Dom&iacute;nio P&uacute;blico
                              </a>
                          </li>
                          <li>
                              <a href="http://www.culturabrasil.org/download.htm" target="_blank">
                                  <i class="glyphicon glyphicon-book"></i>Cultura Brasil
                              </a>
                          </li>
                          <li>
                              <a href="http://www.bbm.usp.br/" target="_blank">
                                  <i class="glyphicon glyphicon-book"></i>Projeto Brasiliana
                              </a>
                          </li>
                          <li>
                              <a href="http://www.futura.org.br/" target="_blank">
                                  <i class="glyphicon glyphicon-book"></i>Canal Futura
                              </a>
                          </li>
                          <li>
                              <a href="http://www.scielo.org/php/index.php" target="_blank">
                                  <i class="glyphicon glyphicon-book"></i>Scielo
                              </a>
                          </li>
                          <li>
                              <a href="http://www.bn.br/" target="_blank">
                                  <i class="glyphicon glyphicon-book"></i>Biblioteca Nacional
                              </a>
                          </li>
                          <li>
                              <a href="https://www.gutenberg.org/wiki/PT_Principal" target="_blank">
                                  <i class="glyphicon glyphicon-book"></i>Projeto Gutenberg
                              </a>
                          </li>
                          <li>
                              <a href="http://scholar.google.com.br/" target="_blank">
                                  <i class="glyphicon glyphicon-book"></i>Google Acad&ecirc;mico
                              </a>
                          </li>
                          <li>
                              <a href="http://tvescola.mec.gov.br/tve/home" target="_blank">
                                  <i class="glyphicon glyphicon-book"></i>Tv Escola
                              </a>
                          </li>
                          
                             <li>
                              <a href="http://mecflix.mec.gov.br/" target="_blank">
                                  <i class="glyphicon glyphicon-book"></i>MACFLIX
                              </a>
                          </li>
                      </ul>
                  </div>
                  <div role="tabpanel" class="tab-pane" id="videos">
                      <hr>
                      <div id="lista-videos">                    
                      </div>
                  </div>
                </div>

</div>
                
                
                
                
               
            </div>
        </div>
    </div>   
</div>



<script type="text/javascript">
    
    $(document).ready(function(){
        $(".progress").css("display","none");
    });
    
    $("#mais-videos").click(function(){
        var topico = $(this).attr("data"),
        texto = $("#lista-videos").text();
        texto = texto.trim();
        if(texto.length === 0){
            $(".progress").css("display","block");
            $.post("../../videos", {titulo:topico}, function(data){
                $(".progress").css("display","none");
                $("#lista-videos").html(data);
				$("#lista-videos").focus();
            });
        }
    });
	
    $("#mais-livros").click(function(){
        var topico = $(this).attr("data"),
        textov = $("#lista-livros").text();
        textov = textov.trim();
        if(textov.length === 0){
            $(".progress").css("display","block");
            $.post("../../livros", {titulo:topico}, function(data){
                $(".progress").css("display","none");
                $("#lista-livros").html(data);
				$("#lista-livros").focus();
            });
        }
    });
</script>

