<div class="row">
    <div class="col-md-12">
        
        <div id="cursos">
            
            <?php
            
            if (count($cursos) > 0) {
                
                echo '<pre>' . count($cursos) . ' resultado(s) econtrado(s).</pre>';
                echo '<div class="blank_10"></div>';
                foreach ($cursos as $c) {
                    $url_imagem = null;
                    $url = base_url("seleciona_curso_troca/" . codifica($c->id_curso."|".$this->input->post("hid")));
                    if ($c->imagem !== "") {
                        $url_imagem = base_url("imgs/cursos/$c->imagem");
                    } else {
                        $url_imagem = base_url("imgs/sem-foto.png");
                    }
                    
                    echo '<div class="media lista-cursos-home escolhe-curso-troca">
                                <div class="media-left media-middle">
                                    <a href="'.$url.'" onclick="return confirm(\'Confirmar ação?\');">
                                        <img class="media-object" src="' . $url_imagem . '" alt="' . $c->nome . '">
                                    </a>
                                </div>
                                <div class="media-body">
                                    <a href="'.$url.'" onclick="return confirm(\'Confirmar ação?\');">
                                         '. '<h4 class="media-heading">CURSO ' . $c->nome . '</h4>
                                    </a>
                                    <br /><br />
                                    <a href="'.$url.'" onclick="return confirm(\'Confirmar ação?\');" class="btn btn-success">
                                        Selecionar este curso
                                    </a>   
                                </div>
                                
                            </div>';
                    echo '<div class="blank_20"></div>';
                }
            } else {
                echo "<pre>Nenhum curso foi encontrado.</pre>";
            }
            ?>

        </div>

    </div>
</div>
</div>

</div>

