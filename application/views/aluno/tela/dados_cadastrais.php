<div class="row">
    <div class="col-md-8">

        <h3>Alterar dados</h3>

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#Dados" aria-controls="Dados" role="tab" data-toggle="tab">Dados</a></li>
            <li role="presentation"><a href="#senha" aria-controls="senha" role="tab" data-toggle="tab">Alterar senha</a></li>
            <li role="presentation"><a href="#Alterar" aria-controls="Alterar" role="tab" data-toggle="tab">Alterar email</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="Dados">

                <form action="<?= base_url('aluno/usuario/alterar_dados') ?>" method="post">
                    <div id="dados-aluno">
                        <div class="form-group">
                            <label for="nome">Nome</label>
                            <input type="text" class="form-control dados-aluno" <?php if ($usuario[0]->nome != "") echo "disabled"; ?> name="nome" id="Nome" minlength="6" maxlength="120" value="<?php if ($usuario[0]->nome != "") echo $usuario[0]->nome; ?>">
                        </div>
                        <?php echo form_error('nome'); ?>

                        <div class="form-group">
                            <label for="rg">RG</label>
                            <input type="text"  class="form-control dados-aluno"  value="<?= $usuario[0]->rg ?>" name="rg" id="rg">
                        </div>
                        <?php echo form_error('rg'); ?>

                        <div class="form-group">
                            <label for="cep">Cep*</label>
                            <input type="text"  class="form-control dados-aluno" value="<?= str_replace("-", "", $usuario[0]->cep) ?>" name="cep" id="cep" minlength="8" maxlength="8" required="true">
                        </div>  
                        <?php echo form_error('cep'); ?>

                        <div class="form-group">
                            <label for="endereco">Endere&ccedil;o*</label>
                            <input type="text"  class="form-control dados-aluno" value="<?= $usuario[0]->endereco ?>" name="endereco" id="endereco" required>
                        </div>
                        <?php echo form_error('endereco'); ?>

                        <div class="form-group">
                            <label for="numero">N&uacute;mero*</label>
                            <input type="text"  class="form-control dados-aluno" value="<?= $usuario[0]->numero ?>" name="numero" id="numero" required>
                        </div>  
                        <?php echo form_error('numero'); ?>


                        <div class="form-group">
                            <label for="complement">Complemento</label>
                            <input type="text"  class="form-control dados-aluno" value="<?= $usuario[0]->complemento ?>" name="complemento" id="complemento">
                        </div>
                        <?php echo form_error('complemento'); ?>

                        <div class="form-group">
                            <label for="bairro">Bairro*</label>
                            <input type="text"  class="form-control dados-aluno" value="<?= $usuario[0]->bairro ?>" name="bairro" id="bairro" required>
                        </div>
                        <?php echo form_error('bairro'); ?>


                        <div class="form-group">
                            <label for="cidade">Cidade*</label>
                            <input type="text"  class="form-control dados-aluno" value="<?= $usuario[0]->cidade ?>" name="cidade" id="cidade" required>
                        </div>  
                        <?php echo form_error('cidade'); ?>

                        <label for="txtEstado">Estado:</label>
                        <?php echo form_error('estado'); ?>

                        <select name='estado' class="form-control" required>
                            <?php geraEstados($usuario[0]->estado) ?>
                        </select>

                        <div class="form-group">
                            <label for="Numero">Celular*</label>
                            <div class="row">
                                <div class="col-md-2">
                                    <input type="number" min="0" class="form-control" value="<?= $usuario[0]->ddd_telefone ?>" name="ddd_telefone" id="ddd" maxlength="2" minlength="2" required="" placeholder="DDD">
                                </div>
                                <div class="col-xs-4">
                                    <input type="number" min="0" class="form-control" value="<?= $usuario[0]->telefone ?>" name="telefone" id="telefone" maxlength="12" minlength="8" required="" id="telefone" placeholder="Telefone">
                                </div>
                            </div>
                        </div>
                        <?php echo form_error('ddd_telefone'); ?>
                        <?php echo form_error('telefone'); ?>

                    </div><!--fim dados aluno-->
                    <div class="form-group">
                        <!--<label for="Data">Data de nascimento</label>-->
                        <?php
                        if ($usuario[0]->data_nasc != "0000-00-00" || $usuario[0]->data_nasc != null) {
                            $nascimento = date_format(date_create($usuario[0]->data_nasc), "d-m-Y");
                        } else {
                            $nascimento = "";
                        }
                        ?>
                        <?php
                        if ($usuario[0]->data_nasc != "0000-00-00" || strlen(trim($usuario[0]->data_nasc)) > 6) {
                            ?>
                                                <!--<input type="text" name="data_nasc" class="form-control" data-mask="99-99-9999" value="<?= $nascimento ?>" id="Data" placeholder="Somente n&uacute;meros">-->
                        <?php } else { ?>
                            <input type="text" name="data_nasc" class="form-control" data-mask="99-99-9999" id="Data" placeholder="Somente n&uacute;meros">
                        <?php }
                        ?>
                    </div>
                    <?php echo form_error('data_nasc'); ?>

                    <pre>*Campos obrigatórios</pre>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success">Alterar</button>
                    </div>
                </form>    
            </div>

            <div role="tabpanel" class="tab-pane" id="Alterar">
                <br>
                <form action="<?= base_url('aluno/usuario/altera_email') ?>" method="post">
                    <div class="form-group">
                        <label for="emailAtual">Email atual</label>
                        <input type="email" class="form-control" id="emailAtual" disabled value="<?= $usuario[0]->email ?>">
                    </div>

                    <div class="form-group">
                        <label for="Novoemail">Novo email</label>
                        <input type="email" class="form-control" required name="novoEmail" minlength=6 id="Novoemail">
                    </div>
                    <?php echo form_error('novoEmail'); ?>

                    <button type="submit" class="btn btn-success">Alterar</button>
                </form>
            </div>
            <div role="tabpanel" class="tab-pane" id="senha">
                <br>
                <div id="dados-aluno">

                    <form action="<?= base_url('aluno/usuario/altera_senha') ?>" method="post">

                        <div class="form-group">
                            <label for="senha_atual">Senha atual</label>
                            <input type="password"  class="form-control dados-aluno" name="senha_atual" id="senha_atual" required="">
                        </div>
                        <?php echo form_error('senha_atual'); ?>

                        <div class="form-group">
                            <label for="senha_nova">Senha nova</label>
                            <input type="password"  class="form-control dados-aluno" name="senha_nova" id="senha_nova" minlength="6" required="">
                        </div>
                        <?php echo form_error('senha_nova'); ?>


                </div><!--fim dados aluno-->

                <div class="form-group">
                    <button type="submit" class="btn btn-success">Alterar</button>
                </div>
                </form>

            </div>

        </div>    

    </div>
</div>
</div>
</div>
