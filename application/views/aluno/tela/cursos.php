<link rel="stylesheet" href="<?= base_url() ?>jquery-ui/jquery-ui.css">
<script src="<?= base_url() ?>jquery-ui/jquery-ui.js"></script>  
<div class="row">
    <div class="col-md-8">
        <div class="blank_35"></div>
        <div class="conteudo-direito" id="cursos">
            <?= $paginacao ?>
            <?php
            if ($cursos == null)
                if ($this->session->userdata("rescad")) {
                    echo '<div class="jumbotron">
                                    <h1>Seja bem vindo, ' . $this->session->userdata("imazon_nome") . '!</h1>
                                    <p>Seu cadastro foi efetuado com sucesso!</p>
                                    <p><a class="btn btn-primary btn-lg" href="' . base_url('cursos') . '" role="button">Matricular</a></p>
                                  </div>';
                } else {
                    if (isset($termo)) {
                        echo '<div class="jumbotron">
                                    <h1>Nenhum resultado</h1>
                                    <p>No momento nós nao temos nenhum curso de <b>' . $termo . '</b>.</p>
                                    <p><a class="btn btn-primary btn-lg" href="' . base_url('cursos') . '" role="button">Matricular</a></p>
                                  </div>';
                    } else {
                        echo '<div class="jumbotron">
                                    <h1>Nenhum resultado</h1>
                                    <p>No momento você ainda não está matriculado em nenhum curso.</p>
                                    <p><a class="btn btn-primary btn-lg" href="' . base_url('cursos') . '" role="button">Matricular</a></p>
                                  </div>';
                    }
                } else
                foreach ($cursos as $c) {

                    $url = null;
                    if ($c->imagem !== "") {
                        $url = base_url("imgs/cursos/$c->imagem");
                    } else {
                        $url = base_url("imgs/sem-foto.png");
                    }

                    $matricula = '';

                    if ((int) $c->situacao == 0 || (int) $c->situacao == 9) {
                        $matricula = 'Data da matrícula: <span id="dt-' . codifica($c->matricula) . '">' . date("d/m/Y", strtotime($c->data_matricula)) . '</span> <a href="#' . codifica($c->matricula) . '" class="label label-danger alterar-data-matricula" tdi="' . codifica($c->matricula) . '"><i class="glyphicon glyphicon-pencil"></i> Alterar</a><br />';
                    } else {
                        $matricula = 'Data da matrícula: ' . date("d/m/Y", strtotime($c->data_matricula)) . '<br />';
                    }

                    switch ((int) $c->situacao) {

                        case 0: 
                            //$situacao = 'Status: <span class="label label-warning">Aguardando a matrícula</span></p><a href="'.base_url('cancelar_curso/'.codifica($c->id_historico)).'" onclick="return confirm(\'Deseja cancelar esta inscrição?\')" alt="Cancelar inscrição" title="Cancelar inscrição" style="float: right; margin-top: -140px;"><i class="glyphicon glyphicon-trash"></i></a><form action="'.base_url("addItemCarrinho").'" method="post"><input type="hidden" name="curso" value="'.codifica($c->id_curso).'"><input type="hidden" name="historico" value="'.codifica($c->matricula).'"><input type="hidden" name="carga_horaria" value="'.codifica($c->carga_horaria).'"><p><input type="submit" class="btn btn-success" value="Gerar Fatura" style="float: right; margin-top: -65px;"></p></form>';									
                            //$situacao = 'Status: <span class="label label-warning">Aguardando a matrícula</span><p><a href="'.base_url('cancelar_curso/'.codifica($c->id_historico)).'" onclick="return confirm(\'Deseja cancelar esta inscrição?\')" alt="Cancelar inscrição" title="Cancelar inscrição" style="float: right; margin-top: -140px;"><i class="glyphicon glyphicon-trash"></i></a><a href="'.base_url('estudar/'.codifica($c->id_historico)).'" class="btn btn-success" style="float: right;">Acessar</a></p>';									
                            $situacao = 'Status: <span class="label label-warning">Aguardando Pagamento</span></p><a href="' . base_url('cancelar_curso/' . codifica($c->id_historico)) . '" onclick="return confirm(\'Deseja cancelar esta inscrição?\')" alt="Cancelar inscrição" title="Cancelar inscrição" style="float: right; margin-top: -140px;"><i class="glyphicon glyphicon-trash"></i></a><p><a href="' . base_url('estudar/' . codifica($c->id_historico)) . '" class="btn btn-success" style="float: right; margin-top: -65px;">Acessar</a></p>';
                            break;

                        case 1:
                            
                            if ($c->id_curso != 2861) {
                                $situacao = 'Status: <span class="label label-info">Matriculado</span><p><a href="' . base_url('estudar/' . codifica($c->id_historico)) . '" class="btn btn-info" style="float: right; margin-top: -65px;">Estudar</a></p>';
                            }
                            if ($c->id_curso == 2861) {
                                $situacao = 'Status: <span class="label label-info">Matriculado</span><p><a href="' . base_url('trocar_curso/' . codifica($c->id_historico)) . '" class="btn btn-success" style="float: right; margin-top: -65px;">Usar Bonus</a></p>';
                            }


                            break; 
                        case 2:
                            $situacao = 'Status: <span class="label label-success">Aprovado</span><p><a href="' . base_url('estudar/' . codifica($c->id_historico)) . '" class="btn btn-success" style="float: right; margin-top: -65px;">Certificado</a></p>';
                            break;
                        case 3:
                            $situacao = 'Status: <span class="label label-danger">Reprovado</span><a href="' . base_url("solicitar_segunda_chance") . '/' . codifica($c->id_historico) . '" class="btn btn-danger" style="float: right; margin-top: -65px;">2ª Chance</a></p>';
                            break;
                        case 4:
                            $situacao = 'Status: <span class="label label-danger">Jubilado</span> <span class="label label-success"><a href="' . base_url("liberar_curso") . '/' . codifica($c->id_historico) . '" title="Liberar curso"><i class="glyphicon glyphicon-ok"></i></a></span></p>';
                            break;
                        case 9:
                            $situacao = 'Status: <span class="label label-info">Inscrito</span> <a href="' . base_url("troca_curso") . '/' . codifica($c->id_historico) . '" title="Trocar curso"><span class="label label-danger">Trocar Curso</span></a><a href="' . base_url('desbloquear_curso/' . codifica($c->id_historico)) . '" class="btn btn-success" style="float: right; margin-top: -65px;">Desbloquear</a> <br/><br/><b>Após desbloquear você não poderá trocar de curso ou  alterar a data de matrícula através da área do aluno.</b></p>';
                            break;
                        default:
                            $situacao = 0;
                            break;
                    }

                    echo '<div class="media lista-cursos-home" id="' . codifica($c->matricula) . '">
                                    <div class="media-left media-middle">
                                        <a href="">
                                            <img class="media-object" src="' . $url . '" alt="' . $c->nome . '">
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <a href="">'
                    . '<h4 class="media-heading">CURSO ' . $c->nome . '</h4>
                                        </a>
                                        <p>Carga horária: ' . $c->carga_horaria . ' horas<br />
                                           ' . $matricula . '
                                           Matrícula: ' . $c->id_historico . '<br />    
                                        ' . $situacao . '   
                                    </div>
                                    
                                </div>';
                }
            ?>

        </div>

        <?= $paginacao ?>
    </div>
</div>
</div>

</div>

<!-- Modal -->
<div class="modal fade" id="modalAlteraDataMatricula" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Alterar data de matrícula</h4>
            </div>
            <form id="formAlterarDataMatricula" method="post" action="<?= base_url("alteraDataMatricula") ?>">
                <div class="modal-body">
                    <p>Selecione a data: 
                        <input type="text" name="dataNova" class="datepicker">
                    </p>
                    <input type="hidden" name="tdi">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    <button type="submit" class="btn btn-primary btn-salvar-data-matricula">Salvar</button>
                </div>
            </form>  
        </div>
    </div>
</div>

