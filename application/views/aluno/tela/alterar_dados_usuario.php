<div class="row">
    <div class="col-md-8">
        <form action="<?= base_url('aluno/aluno/alterar_dados') ?>" method="post">

    </div>

    <div class="col-md-8">   
        <h3>Dados do Aluno</h3>
        <div id="dados-aluno">
            <div class="form-group">
                <label for="nome">Nome</label>
                <input type="text"  class="form-control dados-aluno"  value="<?= $aluno[0]->nome ?>" name="nome" id="nome">
            </div>
            <div class="form-group">
                <label for="rg">RG</label>
                <input type="text"  class="form-control dados-aluno"  value="<?= $aluno[0]->rg ?>" name="rg" id="rg">
            </div>
            <div class="form-group">
                <label for="apelido">Apelido</label>
                <input type="text"  class="form-control dados-aluno"  value="<?= $aluno[0]->apelido ?>" name="apelido" id="apelido">
            </div>

            <div class="form-group">
                <label for="email">Email</label>
                <input type="text"  class="form-control dados-aluno" disabled="" value="<?= $aluno[0]->email ?>" id="email">
            </div>    
            <div class="form-group">
                <label for="cep">Cep*</label>
                <input type="text"  class="form-control dados-aluno" value="<?= str_replace("-", "", $aluno[0]->cep) ?>" name="cep" id="cep" minlength="8" maxlength="8">
            </div>  
            <div class="form-group">
                <label for="estado">Estado*</label>
                <input type="text"  class="form-control dados-aluno"  value="<?= $aluno[0]->estado ?>" name="estado" id="estado">
            </div>
            <div class="form-group">
                <label for="cidade">Cidade*</label>
                <input type="text"  class="form-control dados-aluno" value="<?= $aluno[0]->cidade ?>" name="cidade" id="cidade">
            </div>  

            <div class="form-group">
                <label for="endereco">Endere&ccedil;o*</label>
                <input type="text"  class="form-control dados-aluno" value="<?= $aluno[0]->endereco ?>" name="endereco" id="endereco">
            </div>
            <div class="form-group">
                <label for="complement">Complemento</label>
                <input type="text"  class="form-control dados-aluno" value="<?= $aluno[0]->complemento ?>" name="complemento" id="complemento">
            </div>
            <div class="form-group">
                <label for="numero">N&uacute;mero*</label>
                <input type="text"  class="form-control dados-aluno" value="<?= $aluno[0]->numero ?>" name="numero" id="numero">
            </div>  
            <div class="form-group">
                <label for="bairro">Bairro*</label>
                <input type="text"  class="form-control dados-aluno" value="<?= $aluno[0]->bairro ?>" name="bairro" id="bairro">
            </div>

            <div class="form-group">
                <label for="Numero">Telefone*</label>
                <div class="row">
                    <div class="col-md-2">
                        <input type="text" class="form-control" value="<?= $aluno[0]->ddd_telefone ?>" name="ddd_telefone" id="ddd" maxlength="2" minlength="2" required="" placeholder="DDD">
                    </div>
                    <div class="col-xs-4">
                        <input type="text" class="form-control" value="<?= $aluno[0]->telefone ?>" name="telefone" id="telefone" maxlength="12" minlength="8" required="" id="telefone" placeholder="Telefone">
                    </div>
                </div>
            </div>

        </div><!--fim dados aluno-->
        <div class="form-group">
            <label for="Data">Data de nascimento*</label>
            <input type="text" name="data_nasc" class="form-control" data-mask="99-99-9999" required="" value="<?= @date_format(date_create($aluno[0]->data_nasc), "d-m-Y") ?>" id="Data" placeholder="Somente n&uacute;meros">
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-success">Confirmar</button>
        </div>
        </form>
    </div>
</div>