<!--Lightbox -->
<?= link_tag('node_modules/lightbox2-master/dist/css/lightbox.min.css'); ?>
<script type="text/javascript" src="<?= base_url('ckeditor/ckeditor.js') ?>"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('[name="editor-prova"]').ckeditor();
    });
</script>
<style>
    #conteudo{
        overflow:auto;
        min-height: 200px;
        max-height: 400px;
    }
</style>

<div class="col-md-9">
    <div class="blank_30"></div>
    <div class="conteudo-direito" id="postagem">
                            <?php
                            switch ($situacao) {
                                case 0:
                                    $btn_gerar_fatura = '<form action="' . base_url("addItemCarrinho") . '" method="post"><input type="hidden" name="curso" value="' . codifica($historico[0]->id_curso) . '"><input type="hidden" name="carga_horaria" value="' . codifica($historico[0]->carga_horaria) . '"><input type="hidden" name="historico" value="'.codifica($historico[0]->id).'"><input type="submit" class="btn btn-success" value="Gerar Fatura"></form>';
                                    echo '<div class="alert alert-info"><p><b>Promoção!</b> Pague sua matrícula hoje e receba o Certificado totalmente Grátis!<br> '.$btn_gerar_fatura.'</p></div>';
                                    break;
                                case 1:
                                    echo '<a href="'.base_url("cursos_matriculados#cursos").'" class="label label-primary">Voltar</a>';
                                    break;
                                default:
                                    break;
                            }
                            ?>
        
        <h3>CURSO <?= $nome ?></h3>
        <div class="col-md-12">
            <br />
            <?php
            
            if ($situacao == 2 && !$certificado_impresso) {
                echo '<div class="alert alert-success alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                            Parabéns, você foi aprovado neste curso com nota ' . $nota . '. <a data-toggle="modal" data-target="#modal-certificado"><b>Clique aqui</b></a> e solicite o seu certificado impresso!
                      </div>';
                $txt = null;
                foreach ($tiposCertificado as $tC) {
                    $txt .= '<div class="checkbox">
                                    <label>
                                      <input type="checkbox" name="id_produto[]" value="' . codifica($tC->id_produto) . '">
                                          ' . $tC->nome . ' (R$' . format_number($tC->valor) . ')
                                    </label>
                                  </div>';
                }
            }

            if ($situacao == 3) {
                if ($libera_compra_segunda_chamada) {//verdadeiro se não deve gerar fatura de 2ª chamada
                    echo '<div class="alert alert-warning alert-dismissible fade in" role="alert">
                                   <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                   Infelizmente você não obteve a nota mínima (5 pontos) para aprovação neste curso. <br /> Sua nota: ' . $nota . '.
                              </div>';
                } else {
                    echo '<div class="alert alert-warning alert-dismissible fade in" role="alert">
                               <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                               Infelizmente você não obteve a nota mínima (5 pontos) para aprovação neste curso. <br /> Sua nota: ' . $nota . '. <br /> Solicite a prova de segunda chamada <a href="' . base_url("solicitar_segunda_chance") . '/' . codifica($historico[0]->id_historico) . '">clicando aqui</a>.
                          </div>';
                }
            }

            if ($situacao == 4) {
                echo '<div class="alert alert-warning alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                            Infelizmente você foi jubilado por não ter concluído este curso a tempo. <br /> Seu prazo final foi: ' . $ultimo_dia . '.
                      </div>';
            }

            if ($situacao == 5) {
                echo '<div class="alert alert-warning alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                            Este curso foi cancelado.</div>';
            }

            if ($situacao == 6) {
                echo '<div class="alert alert-warning alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                            Este curso foi arquivado.</div>';
            }

            if (null !== $prova_liberada && $prova_liberada == true) {
                echo '<div class="alert alert-success alert-dismissible fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                Sua avaliação já está liberada. <strong><u><a href="' . base_url('fazer_avaliacao') . '/' . codifica($historico[0]->id_curso . ':' . $historico[0]->id) . '">Clique aqui</a></u></strong> para efetuar sua prova.
                          </div>';
            }

            
            if ($prova_liberada == false && $situacao == 1){
                 echo '<div class="alert alert-info alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                            Sua avaliação será liberada a partir do dia '.date("d/m/Y", $data_prova_liberada).'</div>';
            }
            
            /* if (null !== $prova_liberada && $prova_liberada == true) {
              echo '<div class="alert alert-warning alert-dismissible fade in" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
              Sua avaliação já está liberada. <a data-toggle="collapse" data-target="#seleciona-tipo-prova" aria-expanded="false" aria-controls="seleciona-tipo-prova">Clique aqui</a> para efetuar sua prova.
              </div>';

              echo '<div class="collapse" id="editor-prova">
              <h4>Digite o conteúdo da prova:</h4>
              <div class="well">
              <form class="form-editor-prova" action="' . base_url('fazer_avaliacao') . '/' . codifica($historico[0]->id_curso . ':' . $historico[0]->id_historico) . '" method="post">
              <textarea class="ckeditor" name="editor-prova"></textarea>
              <hr>
              <button type="submit" class="btn btn-primary">Submeter para avaliação</button>
              </form>
              </div>
              </div>';
              echo '<div class="collapse" id="seleciona-tipo-prova">
              <div class="well">
              <h3>Selecione o tipo de prova:</h3>
              <table class="table">
              <tr>
              <td><a id="exp-editor" data-toggle="collapse" data-target="#editor-prova" aria-expanded="false" aria-controls="editor-prova">Prova escrita</a></td>
              </tr>
              <tr>
              <td><a data-toggle="collapse" data-target="#seleciona-arquivo" aria-expanded="false" aria-controls="seleciona-arquivo">Trabalho escolar em formato de arquivo (<span style="color:red;">.pdf, .docx</span>)</a></td>
              <td><div class="collapse" id="seleciona-arquivo">
              <div class="well">
              <form class="form-arquivo-prova" action="' . base_url('fazer_avaliacao') . '/' . codifica($historico[0]->id_curso . ':' . $historico[0]->id_historico) . '" method="post" enctype="multipart/form-data">
              <input type="file" name="arquivo_prova">
              </form>
              </div>
              </div>
              </div>
              </td>
              </tr>
              <tr>
              <td><a href="' . base_url('fazer_avaliacao') . '/' . codifica($historico[0]->id_curso . ':' . $historico[0]->id_historico) . '">Prova de multipla-escolha</a></td>
              </tr>
              </table>
              </div>
              </div>';
              } */
            ?>
            <div class="progress" style="display: none;">
                <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                    <span style="color: #fff; font-weight: bold; font-size: 15px;">Carregando...</span>
                </div>
            </div>
            <div>

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <?php
                    if ($situacao == 0) {
                        $btn_doc = "#fatura";
                    } else {
                        $btn_doc = "#documentos";
                    }
                    
                    if ($situacao == 0 || $situacao == 1) {
                        if ($situacao == 0) {
                            echo '<li role="presentation" class="active">
                                        <a href="#conteudo" aria-controls="conteudo" role="tab" data-toggle="tab">
                                            <span aria-hidden="true" style="color: #3895F2;" class="glyphicon glyphicon-file glyphicon-lista"></span><br /><b>Conteúdo</b>
                                        </a>
                                    </li>';
                         echo '<li role="presentation">
                          <a href="#conteudo" data="' . $nome . '" aria-controls="livros" role="tab" data-toggle="tab">
                              <span aria-hidden="true" style="color: #DFF238;" class="glyphicon glyphicon-book glyphicon-lista"></span><br />Livros
                          </a>
                      </li>';
                           echo '<li role="presentation">
                                <a href="#conteudo" aria-controls="messages" role="tab" data-toggle="tab">
                                    <span aria-hidden="true" style="color: #38F238;" class="glyphicon glyphicon-bold glyphicon-lista" data="' . $nome . '" id="biblioteca"></span><br /><b>Biblioteca</b>
                                </a>
                            </li>';
                        echo '<li role="presentation">
                                <a href="#conteudo" aria-controls="videos" role="tab" data-toggle="tab" onclick="busca_video(\'' . $nome . '\')">
                                    <span aria-hidden="true" style="color: #F23838;" class="glyphicon glyphicon-facetime-video glyphicon-lista" ></span><br /><b>V&iacute;deos</b>     
                                </a>
                            </li>';
                        } else {
                            echo '<li role="presentation" class="active">
                                        <a href="#conteudo" aria-controls="conteudo" role="tab" data-toggle="tab">
                                            <span aria-hidden="true" style="color: #3895F2;" class="glyphicon glyphicon-file glyphicon-lista"></span><br /><b>Conteúdo</b>
                                        </a>
                                    </li>';
                        echo '<li role="presentation">
                            
                          <a href="#livros" id="mais-livros" data="' . $nome . '" aria-controls="livros" role="tab" data-toggle="tab">
                              <span aria-hidden="true" style="color: #DFF238;" class="glyphicon glyphicon-book glyphicon-lista"></span><br />Livros
                          </a>
                      </li>';
                        echo '<li role="presentation">
                                <a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">
                                    <span aria-hidden="true" style="color: #38F238;" class="glyphicon glyphicon-bold glyphicon-lista" data="' . $nome . '" id="biblioteca"></span><br /><b>Biblioteca</b>
                                </a>
                            </li>';
                        echo '<li role="presentation">
                                <a href="#videos" id="mais-videos" aria-controls="videos" role="tab" data-toggle="tab" onclick="busca_video(\'' . $nome . '\')">
                                    <span aria-hidden="true" style="color: #F23838;" class="glyphicon glyphicon-facetime-video glyphicon-lista" ></span><br /><b>V&iacute;deos</b>     
                                </a>
                            </li>';
                        echo '<li role="presentation">
                                <a href="#prova" aria-controls="videos" role="tab" data-toggle="tab">
                                    <span aria-hidden="true" style="color: red;" class="glyphicon glyphicon-pencil glyphicon-lista" ></span><br /><b>Prova</b>     
                                </a>
                            </li>';
                        }
                        
                        
                    }
                    ?>

                    
                    <li role="presentation">
                        <a href="<?= $btn_doc ?>" aria-controls="documentos" role="tab" data-toggle="tab">
                            <span aria-hidden="true" style="color: #ffac00" class="glyphicon glyphicon-folder-open glyphicon-lista" ></span><br /><b>Certificados</b>     
                        </a>
                    </li>



                    <?php
                    if ($situacao == 3 && $libera_compra_segunda_chamada == false) {
                        echo '<li role="presentation">
                                    <a id="prova-segunda-chamada" href="' . base_url("solicitar_segunda_chance") . '/' . codifica($historico[0]->id_historico) . '" role="tab">
                                        <span aria-hidden="true" style="color: red" class="glyphicon glyphicon-repeat glyphicon-lista" ></span><br /><b>2ª Chance</b>     
                                    </a>
                                </li>';
                    }
                    ?>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <?php
                    if ($situacao == 0 || $situacao == 1) {
                        ?>
                        <div role="tabpanel" class="tab-pane active" id="conteudo">
                            <hr>
                            <?php
                            switch ($situacao) {
                                case 0:
                                    echo '<br>';
                                    echo '<br /><div class="alert alert-warning"><img style="width: 8%;" src="' . base_url() . '/imgs/icones/pagamento.png"> Aguardando pagamento da matrícula.</div>';
                                    echo '<form action="' . base_url("addItemCarrinho") . '" method="post"><input type="hidden" name="curso" value="' . codifica($historico[0]->id_curso) . '"><input type="hidden" name="carga_horaria" value="' . codifica($historico[0]->carga_horaria) . '"><p><input type="hidden" name="historico" value="'.codifica($historico[0]->id).'"><input type="submit" class="btn btn-success" value="Gerar Fatura"></p></form>';
                                    break;
                                case 1:
                                    echo '<br>';
                                    echo nl2br($questionario);
                                    break;
                                default:
                                    break;
                            }
                            ?>
                        </div> 
                    <?php } ?>
                    <div role="tabpanel" class="tab-pane" id="livros">
                        <hr>
                        <div id="lista-livros">                    
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="messages">
                        <hr>
                        <ul class="media-list">
                            <li>
                                <a href="http://www.dominiopublico.gov.br/pesquisa/PesquisaObraForm.jsp" target="_blank">
                                    <i class="glyphicon glyphicon-book"></i>Dom&iacute;nio P&uacute;blico
                                </a>
                            </li>
                            <li>
                                <a href="http://www.culturabrasil.org/download.htm" target="_blank">
                                    <i class="glyphicon glyphicon-book"></i>Cultura Brasil
                                </a>
                            </li>
                            <li>
                                <a href="http://www.bbm.usp.br/" target="_blank">
                                    <i class="glyphicon glyphicon-book"></i>Projeto Brasiliana
                                </a>
                            </li>
                            <li>
                                <a href="http://www.futura.org.br/" target="_blank">
                                    <i class="glyphicon glyphicon-book"></i>Canal Futura
                                </a>
                            </li>
                            <li>
                                <a href="http://www.scielo.org/php/index.php" target="_blank">
                                    <i class="glyphicon glyphicon-book"></i>Scielo
                                </a>
                            </li>
                            <li>
                                <a href="http://www.bn.br/" target="_blank">
                                    <i class="glyphicon glyphicon-book"></i>Biblioteca Nacional
                                </a>
                            </li>
                            <li>
                                <a href="https://www.gutenberg.org/wiki/PT_Principal" target="_blank">
                                    <i class="glyphicon glyphicon-book"></i>Projeto Gutenberg
                                </a>
                            </li>
                            <li>
                                <a href="http://scholar.google.com.br/" target="_blank">
                                    <i class="glyphicon glyphicon-book"></i>Google Acad&ecirc;mico
                                </a>
                            </li>
                            <li>
                                <a href="http://tvescola.mec.gov.br/tve/home" target="_blank">
                                    <i class="glyphicon glyphicon-book"></i>Tv Escola
                                </a>
                            </li>
                            
<!--                            <li>
                              <a href="http://mecflix.mec.gov.br/" target="_blank">
                                  <i class="glyphicon glyphicon-book"></i>Macflix
                              </a>
                          </li>-->
                        </ul>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="videos">
                        <hr>
                        <ul id="lista-videos">                    
                        </ul>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="fatura">
                        <hr>
                        <div class="alert alert-info">O Certificado é liberado após a confirmação da matrícula.</div>
                         <?php
                            switch ($situacao) {
                            case 0:
                                echo '<form action="' . base_url("addItemCarrinho") . '" method="post"><input type="hidden" name="curso" value="' . codifica($historico[0]->id_curso) . '"><input type="hidden" name="carga_horaria" value="' . codifica($historico[0]->carga_horaria) . '"><p><input type="hidden" name="historico" value="'.codifica($historico[0]->id).'"><input type="submit" class="btn btn-success" value="Gerar Fatura"></p></form>';
                                break;
                        }
                        ?>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="prova">
                        <hr>
                        <?php
                         if (null !== $prova_liberada && $prova_liberada == true) {
                                echo '<div class="alert alert-success alert-dismissible fade in" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                Sua avaliação já está liberada. <strong><u><a href="' . base_url('fazer_avaliacao') . '/' . codifica($historico[0]->id_curso . ':' . $historico[0]->id) . '">Clique aqui</a> </u></strong>para efetuar sua prova.
                                          </div>';
                            }


                            if ($prova_liberada == false && $situacao == 1) {
                                echo '<div class="alert alert-info alert-dismissible fade in" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                            Sua avaliação será liberada a partir do dia ' . date("d/m/Y", $data_prova_liberada) . '</div>';
                            }
                        ?>
                    </div>    
                    <?php
                    if ($situacao == 2 || $situacao == 3) {
                        $active = 'active';
                    } else {
                        $active = '';
                    }
                    ?>

                    <div role="tabpanel" class="tab-pane <?= $active ?>" id="documentos">
                        <hr>
                        <?php
                        if ( isset($certificado_participacao) ) {
                            echo '<div style="border: 2px solid rgb(119, 119, 119); padding: 3px; height: 100px; width: 92px; border-radius: 4px; float: left;"><a href="' . base_url('autentica') . '/' . codifica($historico[0]->id_historico) . '" target="_blank"><i style="font-size: 60px; color: #7CBC0A; margin: auto; float: left; left: 10%; padding-bottom: 2px; padding-top: 3px;" class="glyphicon glyphicon-certificate"></i><p>Certificado</p></a></div>';
                        }
                        if ($declaracao) {
                            echo '<div style="border: 2px solid rgb(119, 119, 119); height: 100px; width: 92px; border-radius: 4px; float: left; margin-right: 10px; padding: 7px 2px 0px;"><a href="' . base_url('gerar_declaracao') . '/' . codifica($historico[0]->id_historico) . '" target="_blank"><i style="font-size: 60px; color: #7CBC0A; margin: auto; float: left; left: 17%;" class="glyphicon glyphicon-file"></i><p>Declaração</p></a></div>';
                            echo '<div style="border: 2px solid rgb(119, 119, 119); padding: 3px; height: 100px; width: 92px; border-radius: 4px; float: left;"><a href="' . base_url('autentica') . '/' . codifica($historico[0]->id_historico) . '" target="_blank"><i style="font-size: 60px; color: #7CBC0A; margin: auto; float: left; left: 10%; padding-bottom: 2px; padding-top: 3px;" class="glyphicon glyphicon-certificate"></i><p>Certificado</p></a></div>';
                        }
                        if ( isset($certificado_web) ) {
                            echo '<div style="border: 2px solid rgb(119, 119, 119); padding: 3px; height: 100px; width: 92px; border-radius: 4px; float: left; clear: both;"><a href="' . base_url('gerar_certificado_web') . '/' . codifica($historico[0]->id_historico) . '" target="_blank"><i style="font-size: 60px; color: #7CBC0A; margin: auto; float: left; left: 10%; padding-bottom: 2px; padding-top: 3px;" class="glyphicon glyphicon-certificate"></i><p>Certificado</p></a></div><div class="blank_30" style="clear:both;"></div>';
                        }
                        if ( isset($certificado_impresso) ) {
                            foreach ($certificado_impresso as $cert) {
                                switch ($cert->situacao) {
                                    case 0:
                                        $icone = '<img style="width: 4%;" src="' . base_url() . '/imgs/icones/hourglass.png">';
                                        $alerta = 'info';
                                        $msg = '';
                                        $cert_nome = 'Aguardando Envio';
                                        break;
                                    case 1:
                                        $icone = '<img style="width: 4%;" src="' . base_url() . '/imgs/icones/task-complete.png">';
                                        $alerta = 'success';
                                        $msg = ' Foi <b>enviado dia '.date("d/m/Y", strtotime($cert->data_registro)).'</b> com previsão de chegada para '.date('d/m/Y', strtotime($cert->data_registro . "+20 days")).'.';
                                        $cert_nome = $cert->nome;
                                        break;
                                }

                                echo '<div class="alert alert-' . $alerta . '" role="alert" style="clear: both;">' . $icone . '' . $cert_nome . $msg . '</div>';
                            }
                        }
                        if ($situacao == 4) {
                            $data_final = date("d/m/Y", strtotime($historico[0]->primeiro_dia . "+" . round($historico[0]->carga_horaria / 8) . " days")); //date('d/m/Y', strtotime());
                            echo '<div class="alert alert-warning alert-dismissible fade in" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                        Infelizmente você foi jubilado por não ter concluído este curso a tempo. <br /> Seu prazo final foi: ' . $ultimo_dia . '.
                                  </div>';
                        }
                        if ($situacao == 6) {
                            echo '<div class="alert alert-warning alert-dismissible fade in" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                        Este curso foi arquivado.</div>';
                        }
                        ?>
                    </div>

                </div>


            </div>
        </div>

    </div>
    <div class="blank_30"></div>
</div>
</div>

</div>


<?php
if ($situacao == 2) {
    $txt = null;
    foreach ($tiposCertificado as $tC) {
        $txt .= '<div class="checkbox">
                        <label>
                          <input type="checkbox" name="id_produto[]" value="' . codifica($tC->id_produto) . '">
                              ' . $tC->nome . ' (R$' . format_number($tC->valor) . ')
                        </label>
                      </div>';
    }
    ?>        
    <!-- Modal -->
    <div class="modal fade" id="modal-certificado" tabindex="-1" role="dialog" aria-labelledby="modal-certificado-label">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modal-certificado-label"><i class="glyphicon glyphicon-certificate"></i> Selecione o tipo de certificado:</h4>
                </div>
                <div class="modal-body">
                    <form action="<?= base_url('solicitar_certificado_impresso') ?>" method="post">
                        <?= $txt ?>
                        <input type="hidden" name="iad" value="<?= codifica($historico[0]->id_historico . ':' . $historico[0]->id_avaliacao) ?>"/>  
                        <p><b>Prévia dos modelos de certificados: </b></p>
                        <div class="row">
                            <div class="col-xs-6 col-md-3">
                                <a href="http://imazoncursos.com.br/imgs/frentecertoriginal.jpg"  class="thumbnail" data-lightbox="modelo" data-title="Modelo de Certificado Original">
                                    <img src="http://imazoncursos.com.br/imgs/frentecertoriginal.jpg" class="example-image" alt="Modelo de Certificado Original">
                                </a>
                            </div>
                            <div class="col-xs-6 col-md-3">
                                <a href="http://imazon.com.br/novo_imazon/imagens/modeloouro.jpg" class="thumbnail" data-lightbox="modelo" data-title="Modelo de Certificado Ouro">
                                    <img src="http://imazon.com.br/novo_imazon/imagens/modeloouro.jpg" alt="Modelo de Certificado Ouro">
                                </a>
                            </div>  
                            <div class="col-xs-6 col-md-3">
                                <a href="http://imazon.com.br/novo_imazon/imagens/imazon_ouro.jpg" class="thumbnail" data-lightbox="modelo" data-title="Verso dos certificados">
                                    <img src="http://imazon.com.br/novo_imazon/imagens/imazon_ouro.jpg" alt="Verso do certificado">
                                </a>
                            </div>  

                        </div>
                        <input type="submit" value="Continuar" class="btn btn-success"> 

                    </form>    
                </div>
            </div>
        </div>
    </div>
    <?php
}
?>

<!--Lightbox-->
<?= '<script src="' . base_url() . 'node_modules/lightbox2-master/dist/js/lightbox-plus-jquery.min.js"></script>'; ?>
<script>

    $(function () {
        $(".matricular-curso").click(function () {
            $(this).hide();
            $('.btn-voltar-estudo').css('display', 'block');
        })
    });

    $(function () {
        $('.btn-voltar-estudo').click(function () {
            $(this).hide();
            $(".matricular-curso").css('display', 'block');
            $("#conteudo-aula").removeClass('collapse in').addClass('collapse');
        });
    });

    $(function () {
        $('[name="arquivo_prova"]').change(function () {
            $(".form-arquivo-prova").submit();
        });
    });

    $(document).ready(function () {
        $(".progress").css("display", "none");
    });

    function busca_video(termo) {
        $.ajax({
            url: "https://www.googleapis.com/youtube/v3/search",
            type: "GET",
            crossDomain: true,
            data: {part: "snippet", q: termo, maxResults: 12, key: "AIzaSyCZ-KzK4j2utqcWFd9o-GD7zwj5jeryXiI", regionCode: "BR", order: "relevance", safeSearch:"moderate"},
            success: function (responde) {
                $(".progress").css("display", "block");
                //$("#lista-videos").html('<hr>');
                for (x in responde.items) {
                    $("#lista-videos").append('<div class="col-xs-12 col-md-3" style="height:230px;"><a href="https://www.youtube.com/watch?v=' + responde.items[x].id.videoId + '" class="thumbnail" target="_blank"><img src="https://i.ytimg.com/vi/' + responde.items[x].id.videoId + '/hqdefault.jpg"><p>' + responde.items[x].snippet.title + '</p></a></div>');
                }
                console.log(responde);
                 $(".progress").css("display", "none");
            }
        });
    }

    $("#mais-livros").click(function () {
        var topico = $(this).attr("data"),
                textov = $("#lista-livros").text();
        textov = textov.trim();
        if (textov.length === 0) {
            $(".progress").css("display", "block");
            $.post("<?= base_url("aluno/livros") ?>", {titulo: topico}, function (data) {
                $(".progress").css("display", "none");
                $("#lista-livros").html(data);
                $("#lista-livros").focus();
            });
            
        }
    });
</script>



