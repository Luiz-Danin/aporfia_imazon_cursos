<style>
    .thumbnail {
        max-height: 133px;
        height: 133px;
    }
    .thumbnail a > img, .thumbnail > img{
        width: 120px;
        height: 120px;
    }
    
</style>
<div class="col-md-12 col-lg-12 col-xs-12">
    <div class="row">    
        <?php
        $i = 0;
        foreach ($results as $it) {
            foreach ($it as $item) {
                ?>
                <div class="col-xs-12 col-md-3 col-lg-3" style="height: 270px;">
                    <a href="<?=$item->volumeInfo->previewLink?>" target='_blank' class="thumbnail">
                        <img src="https://books.google.com.br/books/content?id=<?=$item->id?>&printsec=frontcover&img=1&zoom=5&edge=curl&imgtk=AFLRE71LJ7r73UXGpXFxH7abY1Rs41gW4jebMirfCt1Z5-uAsnquYoJN9vMGlvlm1oX4_z4Hgqp0B7LOXMguByPeHZlpn08gz4Oxc079oidzF__MCAmBVWmG0fDHOEvHzsE1AAGT5vrB" alt="<?= html_entity_decode($item->volumeInfo->title); ?>" style="max-width: 100%;">
                    </a>
                    <div class="caption">
                        <a href="<?=$item->volumeInfo->previewLink?>" target='_blank'>
                            <p><?= html_entity_decode($item->volumeInfo->title); ?></p>
                        </a>    
                    </div>   
                </div>

                <?php
            }
        }
        ?>
    </div>
</div>

<ul>
<?php
exit;
$i = 0;
foreach ($results as $it) {
    foreach ($it as $item) {
        echo '<li class="media">
                 <div class="media-left media-middle">
                    <a href="' . $item->volumeInfo->previewLink . '" target="_blank"><img align="left" class="media-object" src="https://books.google.com.br/books/content?id=' . $item->id . '&printsec=frontcover&img=1&zoom=5&edge=curl&imgtk=AFLRE71LJ7r73UXGpXFxH7abY1Rs41gW4jebMirfCt1Z5-uAsnquYoJN9vMGlvlm1oX4_z4Hgqp0B7LOXMguByPeHZlpn08gz4Oxc079oidzF__MCAmBVWmG0fDHOEvHzsE1AAGT5vrB"/></a>
                    
                 </div>
                 <div class="media-body">
                    <p align="left">
                        <b>Título: ' . $item->volumeInfo->title . '</b><br />
                        <b>Publicado por: ' . $item->volumeInfo->publisher . '</b><br />
                    </p>
                     <a class="label label-success" href="' . $item->volumeInfo->previewLink . '" target="_blank"><i class="glyphicon glyphicon-book"></i>
                        Abrir o Livro
                    </a>
                 </div>
              </li>    
              <hr>';
    }
}
?> 
</ul>