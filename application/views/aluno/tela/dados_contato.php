<div class="row">
    <div class="col-md-8">
        <h3>Confirme seus dados</h3>
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#Dados" aria-controls="Dados" role="tab" data-toggle="tab">Dados</a></li>
        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="Dados">
                <form action="<?= current_url() ?>" method="post">
                    <div id="dados-aluno">                     
                        <div class="form-group">
                            <label for="Numero">Telefone*</label>
                            <div class="row">
                                <div class="col-md-2">
                                    <input type="text" class="form-control" value="<?= $usuario[0]->ddd_telefone ?>" name="ddd_telefone" id="ddd" maxlength="2" minlength="2" required="" placeholder="DDD">
                                </div>
                                <div class="col-xs-4">
                                    <input type="text" class="form-control" value="<?= $usuario[0]->telefone ?>" name="telefone" id="telefone" maxlength="9" minlength="8" required="" placeholder="Celular">
                                </div>
                            </div>
                        </div>                   
                    </div><!--fim dados aluno-->  
                    <pre>*Campos obrigatórios</pre>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success">Continuar</button>
                    </div>
                </form>    
            </div>
        </div>    
    </div>
</div>
</div>
</div>