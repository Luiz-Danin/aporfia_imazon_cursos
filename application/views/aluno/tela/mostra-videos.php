<div class="col-md-12 col-lg-12 col-xs-12">
    <div class="row">    
        <?php
        foreach ($searchResponse['items'] as $searchResult) {
            switch ($searchResult['id']['kind']) {
                case 'youtube#video':
                    ?>
                    <div class="col-xs-12 col-md-4 col-lg-4" style="height: 270px;">
                        <a href='https://www.youtube.com/v/<?= $searchResult['id']['videoId'] ?>?version=3&f=videos&app=youtube_gdata' target='_blank' class="thumbnail">
                            <img src="<?= $searchResult['snippet']['thumbnails']['default']['url'] ?>" alt="<?= html_entity_decode($searchResult['snippet']['title']); ?>" style="max-width: 100%;">
                        </a>
                        <div class="caption">
                            <a href='https://www.youtube.com/v/<?= $searchResult['id']['videoId'] ?>?version=3&f=videos&app=youtube_gdata' target='_blank'>
                                <p><?= html_entity_decode($searchResult['snippet']['title']); ?></p>
                            </a>    
                        </div>   
                    </div>

                    <?php
                    break;
            }
        }
        ?>
    </div>
</div>
