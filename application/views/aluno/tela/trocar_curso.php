<div class="row">
    <div class="col-md-12">
        <div class="conteudo-direito">
            <div class="blank_25"></div>
            <form method="post" class="form-inline" onsubmit="return false;"> 
                <div class="form-group form-group-lg" style="width: 63%;">
                    <input type="text" class="form-control input-lg" name="nome" id="nome" placeholder="Digite o nome do curso" value="<?= $this->input->post('email') ?>" style="width: 78%;" required="">
                    <input type="hidden" id="hid" value="<?php echo $this->uri->segment(2) ?>">
                </div>
            </form>   
            <div class="blank_50"></div>
            <div class="resultado-busca-cursos"></div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="loader-autenticacao" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="progress">
            <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
            </div>
          </div>
          <p align="center"><b>Aguarde...</b></p>
      </div>
    </div>
  </div>
</div>

 </div>
</div>

<script>
    
$(function(){
   $("#nome").on("keydown", function(){
       var nome_curso = $(this).val(),
       nome_curso     = nome_curso.trim(nome_curso),
       idn             = $("#hid").val();
       
       if(nome_curso.length % 2 == 0){
            $.post("<?php echo base_url("busca_curso_troca_ajax")?>", {nome : nome_curso, hid: idn}, function(e){
                $(".resultado-busca-cursos").html('').html(e).focus();
            });
       }

   });
});    
    
</script>