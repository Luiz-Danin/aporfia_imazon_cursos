<div class="row">
    <div class="col-md-8">
        <div class="blank_35"></div>
        <div class="conteudo-direito" id="faturas">



            <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#faturas-pagas" aria-controls="faturas-pagas" role="tab" data-toggle="tab">Pagas</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="faturas-pagas">
                        <h3>Faturas Pagas</h3>
                        <?php
                        if (count($pedidos) > 0) {
                            ?>
                            <table class="table table-striped">
                                <thead><tr><td>Transação</td><td>Valor</td><td>Data da compra</td><td>Data do pagamento</td><td>Recibo</td></tr></thead>
                                <?php
                                foreach ($pedidos as $pe) {
                                    if ((int) $pe->situacao == 3) {
                                        if ($pe->data_alteracao != "") {
                                            $data_pagamento = formata_data_dia($pe->data_alteracao);
                                        } else {
                                            $data_pagamento = "";
                                        }
                                        $bt_recibo = '<a href="' . base_url('aluno/financeiro/emitir_nfp') . '/' . codifica($pe->transacao) . '" target="_blank" alt="Emitir recibo"><span class="glyphicon glyphicon-list-alt"></span></a>';
                                        echo '<tr><td>' . $pe->transacao . '</td><td>R$' . format_number($pe->valor) . '</td><td>' . formata_data_dia($pe->data_compra) . '</td><td>' . $data_pagamento . '</td><td>' . $bt_recibo . '</td></tr>';
                                    }
                                }
                                ?>
                                <?php
                            } else {
                                echo "<pre>Nenhum registro encontrado.</pre>";
                            }
                            ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>