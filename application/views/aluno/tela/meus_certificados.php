<div class="row">
    <div class="col-md-8">
        <div class="blank_35"></div>
        <div class="conteudo-direito" id="faturas">



            <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#certificados-enviados" aria-controls="certificados-enviados" role="tab" data-toggle="tab">Enviados</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="certificados-enviados">
                        <br />
                        <?php
                        if (count($certificados) > 0) {
                            ?>
                            <table class="table table-striped">
                                <thead><tr><td>Histórico</td><td>Curso</td><td>Valor</td><td>Dt.Compra</td><td>Dt. Envio</td><td>Previsão de entrega</td></tr></thead>
                                <?php
                                foreach ($certificados as $ce) {
                                    if ($ce->data_alteracao !== "") {
                                        $data_pagamento = $ce->data_alteracao;
                                    } else {
                                        $data_pagamento = "";
                                    }
                                    $prev_entrega = date('d/m/Y', strtotime($ce->data_registro . "+20 days")); // 23/05/2009 06:34
                                    if ($ce->situacao == 1) {
                                        echo '<tr><td>' . $ce->id_historico . '</td><td>' . $ce->nomeProduto . ' ' . $ce->nome . '</td><td>R$' . format_number($ce->valor) . '</td><td>' . formata_data_dia($ce->data_compra) . '</td><td>' . formata_data_dia($ce->data_registro) . '</td><td>'.$prev_entrega.'</td></tr>';
                                    }
                                }
                                ?>
                                <?php
                            } else {
                                echo "<pre>Nenhum registro encontrado.</pre>";
                            }
                            ?>
                        </table>
                    </div>
                </div>

            </div>

        </div>

    </div>
</div>
</div>

</div>