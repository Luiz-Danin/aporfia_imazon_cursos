<div class="row">
    <div class="col-md-8">
        <h3>Confirme seus dados</h3>
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#Dados" aria-controls="Dados" role="tab" data-toggle="tab">Dados</a></li>
        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="Dados">
                
                <form action="<?= current_url() ?>" method="post">
                   
                   
                    <div class="form-group">
                        <br>
                        <button type="submit" class="btn btn-success">Continuar</button>
                        <?php
                    if(isset($_SESSION['somente_num'])){
                    ?>
                    
                    <span class="alert alert-info"><?= $_SESSION['somente_num'] ?></span>
                    
                    <?php
                    unset($_SESSION['somente_num']);
                        
                    }
                    ?>
                    </div>
                    <div id="dados-aluno">
                        <div class="form-group">
                            <label for="Nome">Nome</label>
                            <input type="text" class="form-control dados-aluno" <?php if ($usuario[0]->nome != "") echo "disabled"; ?> name="nome" id="Nome" minlength="6" maxlength="120" value="<?php if ($usuario[0]->nome != "") echo $usuario[0]->nome; ?>" required>
                        </div>  
                        <?php echo form_error('nome'); ?>

                        <div class="form-group">
                            <input  type="hidden"  class="form-control dados-aluno"  value="<?= $usuario[0]->rg ?>" name="rg" id="rg">
                        </div>
                        <?php echo form_error('rg'); ?>

                        <div class="form-group">
                            <label for="cep">Cep*</label>
                            <input type="text"  class="form-control dados-aluno" value="<?= str_replace("-", "", $usuario[0]->cep) ?>" name="cep" id="cep" minlength="8" maxlength="8"  required>
                        </div>  
                        <?php echo form_error('cep'); ?>

                        <div class="form-group">
                            <label for="endereco">Endere&ccedil;o*</label>
                            <input type="text"  class="form-control dados-aluno" value="<?= $usuario[0]->endereco ?>" name="endereco" id="endereco"  required>
                        </div>
                        <?php echo form_error('endereco'); ?>


                        <div class="form-group">
                            <label for="numero">N&uacute;mero*</label>
                            <input type="text"  class="form-control dados-aluno" value="<?= $usuario[0]->numero ?>" name="numero" id="numero"  required>
                        </div>  
                        <?php echo form_error('numero'); ?>


                        <div class="form-group">
                            <label for="complement">Complemento</label>
                            <input type="text"  class="form-control dados-aluno" value="<?= $usuario[0]->complemento ?>" name="complemento" id="complemento">
                        </div>
                        <?php echo form_error('complemento'); ?>

                        <div class="form-group">
                            <label for="bairro">Bairro*</label>
                            <input type="text"  class="form-control dados-aluno" value="<?= $usuario[0]->bairro ?>" name="bairro" id="bairro"  required>
                        </div>
                        <?php echo form_error('bairro'); ?>

                        <div class="form-group">
                            <label for="cidade">Cidade*</label>
                            <input type="text"  class="form-control dados-aluno" value="<?= $usuario[0]->cidade ?>" name="cidade" id="cidade" required>
                        </div>  
                        <?php echo form_error('cidade'); ?>


                        <label for="txtEstado">Estado:</label>
                        <select name='estado' class="form-control" required="true">
                            <?php geraEstados($usuario[0]->estado) ?>
                        </select>
                        <?php echo form_error('estado'); ?>


                        <div class="form-group">
                            <label for="Numero">Celular*</label>
                            <div class="row">
                                <div class="col-md-2">
                                    <input type="text" class="form-control" value="<?= $usuario[0]->ddd_telefone ?>" name="ddd_telefone" id="ddd" maxlength="2" minlength="2" required="" placeholder="DDD">
                                </div>
                                <div class="col-xs-4">
                                    <input type="text" class="form-control" value="<?= $usuario[0]->telefone ?>" name="telefone" id="telefone" maxlength="9" minlength="8" required="" id="telefone" placeholder="Celular"  required>
                                </div>
                            </div>
                        </div>
                        <?php echo form_error('ddd_telefone'); ?>
                        <?php echo form_error('telefone'); ?>


                    </div><!--fim dados aluno-->

                    <pre>*Campos obrigatórios</pre>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success">Continuar</button>
                    </div>
                </form>    
            </div>
        </div>    
    </div>
</div>
</div>
</div>
