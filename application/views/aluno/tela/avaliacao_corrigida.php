<!-- Google Code for ResAvalia&ccedil;&atilde;o Imazon Cursos Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1036809317;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "6EURCPLgimYQ5eix7gM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1036809317/?label=6EURCPLgimYQ5eix7gM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

<div class="row">
    <div class="col-md-12 col-xs-12">
        <div class="blank_35"></div>
        <div class="conteudo-direito" id="prova">
            <?php
            $i = 1;
            
            foreach ($resultado as $r) {

                $checked1 = null;
                $checked2 = null;

                echo '  <div class="media">
                                <div class="media-body">
                                  <h4 class="media-heading">' . $i . 'ª Questão</h4>';

                if (isset($r['resposta'])) {
                    if ($r['alternativaCorreta'] == $r['resposta']) {
                        echo '<div class="alert alert-success" role="alert" data-toggle="popover" title="Justificativa" data-placement="left" trigger="hover" data-content="' . $r['justificativa'] . '"><i class="glyphicon glyphicon-ok"></i> Você <b>acertou</b> esta questão</div>';
                    } else {
                        echo '<div class="alert alert-danger" role="alert" data-toggle="popover" title="Justificativa" data-placement="left" trigger="hover" data-content="' . $r['justificativa'] . '"><i class="glyphicon glyphicon-remove"></i> Você <b>errou</b> esta questão</div>';
                    }
                } else {
                    echo '<br /><br />';
                    echo '<div class="alert alert-info" role="alert" data-toggle="popover" title="Justificativa" data-placement="left" trigger="click" data-content="' . $r['justificativa'] . '"><i class="glyphicon glyphicon-remove"></i> Você não <b>marcou</b> esta questão</div>';
                }

                echo '                  
                                </div>
                            </div>';



                $i++;
            }
            ?>



            <div class="col-md-12">
                <?php 
                $txt = "";
                    if($nota>=5){
                        $txt = "Você foi aprovado!";
                    }else{
                        $txt = "Você não foi aprovado neste curso.";
                    }
                ?>
                
                <pre><?=$txt?> Sua nota final é: <?=$nota?></pre>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <a type="submit" class="btn btn-success" href="cursos_matriculados">Continuar</a>
                    <?php
                        
                        if($situacao==1){
                            echo '<a type="submit" class="btn btn-success" href="' . base_url('gerar_certificado_web') . '/' . codifica($id_historico) . '" target="_blank">Certificado web</a>';
                            echo '<a type="submit" class="btn btn-success" href="cursos_matriculados">Certificado original</a>';
                        }
                        if($situacao==2){
                            if($nota >=1 && $nota<5){
                                echo '<a type="submit" class="btn btn-success" href="' . base_url("solicitar_segunda_chance") . '/' . codifica($id_historico) . '">Segunda-chance</a>';
                            }
                        }
                        
                    ?>
                </div>
            </div>
            
        </div>

    </div>
</div>
</div>

</div>



<script>
    $(function () {
        $('[data-toggle="popover"]').popover()
    });
</script>
