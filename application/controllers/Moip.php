<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Moip extends CI_Controller {

    public function __construct() {
        parent::__construct();
		
		
       
		 $this->load->model('pedido_model', 'pedidom');
		 $this->load->model('certificado_model','certfm');
		 $this->load->model('historico_model','hm');
		 
    }
    
	
    public function index() {
	
	  	$emailLoja   = 'suporte@curso.me';
		$chaveAcesso = 'DEACF5082CD37FA57F9B483E20B30D02';
		$pedido =$_POST['pedido'];
		$transacao_id=$_POST['transacao_id'];
		$status = $_POST['status'];
		$urlPost 	 =	"https://www.bcash.com.br/transacao/consulta/";
		$tipoRetorno =	"2";  // 1 = XML      e    2 = JSON
		$codificacao =	"1"; //  1 = UTF-8    e    2 = ISO-8859-1
		ob_start();
		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL, $urlPost);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, array('id_transacao'=>$transacao_id,'tipo_retorno'=>$tipoRetorno,'codificacao'=>		$codificacao));
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Basic '.base64_encode($emailLoja. ':'.$chaveAcesso)));
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_exec ($ch); 
		$resposta = ob_get_contents(); 
		ob_end_clean(); 
		$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	curl_close($ch);
       	

		if($httpCode == "200"){	
		$dados 		    = json_decode($resposta, true);
		$cod_status     = $dados['transacao']['cod_status'];
		$tipo_pagamento = $dados['transacao']['cod_meio_pagamento'];
		$email= $dados['transacao']['cliente_email'];
		$transacao_id=$_POST['transacao_id'];
	    
		    /*
			codigos de pamentos proprios do sistema
			pago=3
			cancelado=7
			em andamento=1
			*/
			
			if($cod_status==1){
			$cdbanco=1;
			}
			
			if($cod_status==3){
			$cdbanco=3;
			}
			
			if($cod_status==4){
			$cdbanco=3;
			}
			
			if($cod_status==5){
			$cdbanco=5;
			}
			
			if($cod_status==6){
			$cdbanco=6;
			}
			
			if($cod_status==7){
			$cdbanco=7;
			}
			
			if($cod_status==8){
			$cdbanco=8;
			}
		    $p = $this->pedidom->get_by_transacao_aluno($pedido)->result(); //pego pedido
			
			$log = array('pago' => '0', 'email' => $email, 'data' => date('Y-m-d h:i:s'), 'situacao' => $cod_status, 'transacao' => $pedido);
	 		$this->pedidom->insert_log_transacao($log);
			
			
			$vett = array('pago' => '1', 'pedido' => $pedido);
           	$dt['res']= $this->pedidom->get_verificapago($vett);       
           	$var=$dt['res'];
			
			
			if($cod_status!=4){
			$dt=array('situacao' => $cdbanco, 'tipo_pagamento' => $tipo_pagamento, 'data_alteracao' => date('Y-m-d'),'cod_interme'=>$transacao_id,'reponsavel'=>1);
	   		$this->pedidom->atualiza_pedido($pedido, $dt);}
			
			
			if($cod_status==6){//se transação igual paga
	   			
				$ped = $this->pedidom->get_by_transacaoped($pedido)->result(); //pego item_pedido
	   			foreach($ped as $pe){
	   			$tipo = $pe->tipo_item;
	   			$historico=$pe->id_historico;
				if($tipo>=7 && $tipo<=30){// item igual a matrícula	   
	   			$dados = array('situacao' =>'6');
       			$this->hm->atualiza_historico_mat_devolvido($historico, $dados);
	                            		}// fim if($tipo>=7 || $tipo>=14){
				
									}	
			                  }       
			
			
  	       
	   		if($cod_status==3 && $var==2){//se transação igual paga
	   			$lg = array('id' =>$pedido);           
           		$this->pedidom->atualizalog($lg);
				
				$ped = $this->pedidom->get_by_transacaoped($pedido)->result(); //pego item_pedido
	   			foreach($ped as $pe){
	   			$tipo = $pe->tipo_item;
	   			$historico=$pe->id_historico;
	  
	  			if($tipo==1){// item igual a certificado original
	   			$certificado = array('id_historico' => $historico, 'codigo_programa' => '006', 'data_registro' => date('Y-m-d h:i:s'), 'situacao' => 0, 'transacao' => $pedido, 'id_tipo' => '1', 'endereco' => '');
				$this->certfm->add($certificado);		
	   			   			}// fim if($tipo==1){
				   
	    		if($tipo==2){// item igual a certificado ouro
	   			$certificado = array('id_historico' => $historico, 'codigo_programa' => '006', 'data_registro' => date('Y-m-d h:i:s'), 'situacao' => 0, 'transacao' => $pedido, 'id_tipo' => '2', 'endereco' => '');
				$this->certfm->add($certificado);
	   			   			}// fim if($tipo==2){	
				   
				if($tipo==4){// item igual a segunda chance
	       		$histo = $this->hm->get_historico_by_id($historico)->result();
				$hash = date('Y').'-'.strtoupper(substr(md5($histo[0]->id_aluno . ':' . microtime().'-'. microtime()), 0, 4).'-'.substr(md5($histo[0]->id_aluno . ':' . microtime()), 0, 4));
				$historico_novo=array('id'=> 'NULL', 'id_historico' => $histo[0]->id_historico, 'id_aluno' =>$histo[0]->id_aluno, 'email' => $histo[0]->email, 'data_aprovacao' => '0000-00-00', 'hora_aprovacao' => '00:00:00', 'nota' =>'0', 'id_avaliacao' => '2', 'id_curso' => $histo[0]->id_curso, 'primeiro_dia' => $histo[0]->primeiro_dia, 'primeira_hora' => $histo[0]->primeira_hora, 'carga_horaria' => $histo[0]->carga_horaria, 'id_baselegal' =>$histo[0]->id_baselegal, 'ip_aprovacao' =>'', 'situacao' => '1', 'observacao' =>'', 'trabalho' =>'', 'arquivo' =>'','codigo_autenticacao' =>$hash);
		
				$this->hm->add($historico_novo);//inseri historico de segunda chance
	   			  			 }// fim if($tipo==4){		   		   
	   
	   
	   			if($tipo>=7 && $tipo<=30){// item igual a matrícula	   
	   			$dados = array('situacao' =>'1');
       			$this->hm->atualiza_historico_mat($historico, $dados);
				$this->hm->atualiza_historico_mat_cancelado($historico, $dados);
				
				
				
				$histo = $this->hm->get_historico_by_id($historico)->result();				
				$hash = date('Y').'-'.strtoupper(substr(md5($histo[0]->id_aluno . ':' . microtime().'-'. microtime()), 0, 4).'-'.substr(md5($histo[0]->id_aluno . ':' . microtime()), 0, 4));				
				$hash2 = date('Y').'-'.strtoupper(substr(md5($histo[0]->id_aluno . ':' . microtime().'-'. microtime()), 0, 4).'-'.substr(md5($histo[0]->id_aluno . ':'.$hash . microtime()), 0, 4));				
				
				if($tipo>=16 && $tipo<=30){
				$historico_novo_bonus=array('id'=> 'NULL', 'id_historico' => $hash2, 'id_aluno' =>$histo[0]->id_aluno, 'email' => $histo[0]->email, 'data_aprovacao' => '0000-00-00', 'hora_aprovacao' => '00:00:00', 'nota' =>'0', 'id_avaliacao' => '1', 'id_curso' => '2861', 'primeiro_dia' => $histo[0]->primeiro_dia, 'primeira_hora' => $histo[0]->primeira_hora, 'carga_horaria' => $histo[0]->carga_horaria, 'id_baselegal' =>$histo[0]->id_baselegal, 'ip_aprovacao' =>'', 'situacao' => '1', 'observacao' =>'curso bonus', 'trabalho' =>'', 'arquivo' =>'','codigo_autenticacao' =>$hash);
				$this->hm->add($historico_novo_bonus);
										  }
				
	                            		}// fim if($tipo>=7 || $tipo>=14){
	 
	   
     
							}//  foreach($ped as $pe) varre item pedido
							
	   			}//fim if($cod_status==3) 	
				
				
				
				
	   }//if($httpCode == "200")
	     
	   
    }//public function __construct()

}//class moip extends CI_Controller
