<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Liberamanual extends CI_Controller {

    public function __construct() {
        parent::__construct();
		 $this->load->model('pedido_model', 'pedidom');
		 $this->load->model('certificado_model','certfm');
		 $this->load->model('historico_model','hm');
		 
    }
    
	
    public function index() {
         
		 $td=decodifica($this->uri->segment(2));
		 $j = $this->pedidom->get_by_transacao_aluno($td)->result();
		
		 //echo $j[0]->cod_interme;
		 //print_r($j);
			  	
		$pedido =$td;
		$transacao_id=$j[0]->cod_interme;
		
		
		
		//$params['token_account'] = 'a44677dac7cba94';   
        $params['token_account'] = '32e9c1b008b07a0';
        $params['token_transaction'] = $transacao_id;
        //$urlPost = "https://api.sandbox.traycheckout.com.br/v2/transactions/get_by_token";
        $urlPost = "https://api.traycheckout.com.br/v2/transactions/get_by_token";
		
        ob_start();
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $urlPost);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_exec($ch);

        /* XML de retorno */
        $resposta = simplexml_load_string(ob_get_contents());
        ob_end_clean();
        curl_close($ch);

        if ($resposta->message_response->message == "success") {
            $codigo_pagamento =(string)$resposta->data_response->transaction->payment->payment_method_id;
            $pedido = (string)$resposta->data_response->transaction->order_number;
            $cod_status =(string)$resposta->data_response->transaction->status_id;		
			$email=(string)$resposta->data_response->transaction->customer->email;			
		
		/*$dados 		    = json_decode($resposta, true);
		$cod_status     = $dados['transacao']['cod_status'];
		$tipo_pagamento = $dados['transacao']['cod_meio_pagamento'];
		$email= $dados['transacao']['cliente_email'];
		$transacao_id=$dados['transacao']['id_transacao'];	
	    
		    /*
			codigos de pamentos proprios do sistema
			pago=3
			cancelado=7
			em andamento=1
			*/
			
			if($cod_status==4){	$cdbanco=1;	}
			
			if($cod_status==5){	$cdbanco=1;	}
			
			if($cod_status==6){	$cdbanco=3;	}
			
			if($cod_status==7){	$cdbanco=7;	}
			
			if($cod_status==24){ $cdbanco=5;}
			
			if($cod_status==87){ $cdbanco=1;}
			
			if($cod_status==88){ $cdbanco=1;}
			
			if($cod_status==89){ $cdbanco=7;}			
			
			if($codigo_pagamento==2){ $tipo_pagamento=55;}//diners
			if($codigo_pagamento==3){ $tipo_pagamento=1;}//visa
			if($codigo_pagamento==4){ $tipo_pagamento=2;}//master
			if($codigo_pagamento==5){ $tipo_pagamento=37;}//america
			if($codigo_pagamento==6){ $tipo_pagamento=10;}//boleto
			if($codigo_pagamento==7){ $tipo_pagamento=60;}//débito itau		
			if($codigo_pagamento==14){ $tipo_pagamento=14;}//débito peela
			if($codigo_pagamento==15){ $tipo_pagamento=15;}//cartão discovery
			if($codigo_pagamento==16){ $tipo_pagamento=63;}//cartão elo
			if($codigo_pagamento==18){ $tipo_pagamento=45;}//cartão aura
			if($codigo_pagamento==19){ $tipo_pagamento=19;}//cartão jbc
			if($codigo_pagamento==20){ $tipo_pagamento=56;}//cartão hipercard
			if($codigo_pagamento==25){ $tipo_pagamento=56;}//cartão elo itaú
				
			if($codigo_pagamento==21){ $tipo_pagamento=62;}//débito hsbc
			if($codigo_pagamento==22){ $tipo_pagamento=59;}//débito bradesco
			if($codigo_pagamento==23){ $tipo_pagamento=58;}//débito banco do brasil		
			
			/*echo $cdbanco."<br/>";
			echo $pedido."<br/>";
			echo $tipo_pagamento."<br/>";			
			exit();*/
			
		    $p = $this->pedidom->get_by_transacao_aluno($pedido)->result(); //pego pedido			
			$log = array('pago' => '0', 'email' => $email, 'data' => date('Y-m-d h:i:s'), 'situacao' => $cod_status, 'transacao' => $pedido);
	 		$this->pedidom->insert_log_transacao($log);
			
			$vett = array('pago' => '1', 'pedido' => $pedido);
           	$dt['res']= $this->pedidom->get_verificapago($vett);       
           	$var=$dt['res'];
			
			
			$dt=array('situacao' => $cdbanco, 'tipo_pagamento' => $tipo_pagamento, 'data_alteracao' => date('Y-m-d'),'cod_interme'=>$transacao_id,'reponsavel'=>2);
	   		$this->pedidom->atualiza_pedido($pedido, $dt);
			
			
			/*if($cod_status==6){//se transação igual paga
	   			
				$ped = $this->pedidom->get_by_transacaoped($pedido)->result(); //pego item_pedido
	   			foreach($ped as $pe){
	   			$tipo = $pe->tipo_item;
	   			$historico=$pe->id_historico;
				if($tipo>=7 && $tipo<=30){// item igual a matrícula	   
	   			$dados = array('situacao' =>'6');
       			$this->hm->atualiza_historico_mat_devolvido($historico, $dados);
	                            		}// fim if($tipo>=7 || $tipo>=14){
				
									}	
			                  } */      
			
			
  	       
	   		if($cod_status==6 && $var==2){//se transação igual paga
	   			$lg = array('id' =>$pedido);           
           		$this->pedidom->atualizalog($lg);
	   			
				$ped = $this->pedidom->get_by_transacaoped($pedido)->result(); //pego item_pedido
	   			foreach($ped as $pe){
	   			$tipo = $pe->tipo_item;
	   			$historico=$pe->id_historico;
	  
	  			if($tipo==1){// item igual a certificado original
	   			$certificado = array('id_historico' => $historico, 'codigo_programa' => '006', 'data_registro' => date('Y-m-d h:i:s'), 'situacao' => 0, 'transacao' => $pedido, 'id_tipo' => '1', 'endereco' => '');
				$this->certfm->add($certificado);		
	   			   			}// fim if($tipo==1){
				   
	    		if($tipo==2){// item igual a certificado ouro
	   			$certificado = array('id_historico' => $historico, 'codigo_programa' => '006', 'data_registro' => date('Y-m-d h:i:s'), 'situacao' => 0, 'transacao' => $pedido, 'id_tipo' => '2', 'endereco' => '');
				$this->certfm->add($certificado);
	   			   			}// fim if($tipo==2){	
				   
				if($tipo==4){// item igual a segunda chance
	       		$histo = $this->hm->get_historico_by_id($historico)->result();
				$hash = date('Y').'-'.strtoupper(substr(md5($histo[0]->id_aluno . ':' . microtime().'-'. microtime()), 0, 4).'-'.substr(md5($histo[0]->id_aluno . ':' . microtime()), 0, 4));
				$historico_novo=array('id'=> 'NULL', 'id_historico' => $histo[0]->id_historico, 'id_aluno' =>$histo[0]->id_aluno, 'email' => $histo[0]->email, 'data_aprovacao' => '0000-00-00', 'hora_aprovacao' => '00:00:00', 'nota' =>'0', 'id_avaliacao' => '2', 'id_curso' => $histo[0]->id_curso, 'primeiro_dia' => $histo[0]->primeiro_dia, 'primeira_hora' => $histo[0]->primeira_hora, 'carga_horaria' => $histo[0]->carga_horaria, 'id_baselegal' =>$histo[0]->id_baselegal, 'ip_aprovacao' =>'', 'situacao' => '1', 'observacao' =>'', 'trabalho' =>'', 'arquivo' =>'','codigo_autenticacao' =>$hash);
		
				$this->hm->add($historico_novo);//inseri historico de segunda chance
	   			  			 }// fim if($tipo==4){		   		   
	   
	   
	   			if($tipo>=7 && $tipo<=30){// item igual a matrícula	   
	   			$dados = array('situacao' =>'9');
       			$this->hm->atualiza_historico_mat($historico, $dados);
				$this->hm->atualiza_historico_mat_cancelado($historico, $dados);
				
				
				
				$histo = $this->hm->get_historico_by_id($historico)->result();				
				$hash = date('Y').'-'.strtoupper(substr(md5($histo[0]->id_aluno . ':' . microtime().'-'. microtime()), 0, 4).'-'.substr(md5($histo[0]->id_aluno . ':' . microtime()), 0, 4));				
				$hash2 = date('Y').'-'.strtoupper(substr(md5($histo[0]->id_aluno . ':' . microtime().'-'. microtime()), 0, 4).'-'.substr(md5($histo[0]->id_aluno . ':'.$hash . microtime()), 0, 4));				
				
				if($tipo>=16 && $tipo<=30){//cursos de 100 promoção
				//if($tipo>=7 && $tipo<=30){
				$historico_novo_bonus=array('id'=> 'NULL', 'id_historico' => $hash2, 'id_aluno' =>$histo[0]->id_aluno, 'email' => $histo[0]->email, 'data_aprovacao' => '0000-00-00', 'hora_aprovacao' => '00:00:00', 'nota' =>'0', 'id_avaliacao' => '1', 'id_curso' => '2861', 'primeiro_dia' => $histo[0]->primeiro_dia, 'primeira_hora' => $histo[0]->primeira_hora, 'carga_horaria' => $histo[0]->carga_horaria, 'id_baselegal' =>$histo[0]->id_baselegal, 'ip_aprovacao' =>'', 'situacao' => '9', 'observacao' =>'curso bonus', 'trabalho' =>'', 'arquivo' =>'','codigo_autenticacao' =>$hash);
				$this->hm->add($historico_novo_bonus);
										  }
				
	                            		}// fim if($tipo>=7 || $tipo>=14){
	 
	   
     
							}//  foreach($ped as $pe) varre item pedido
							
	   			}//fim if($cod_status==3) 	
				
				
				
				
	   }//if($httpCode == "200")
	     
	 redirect("faturas");
    }//public function __construct()

}//class moip extends CI_Controller
