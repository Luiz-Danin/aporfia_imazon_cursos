<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cart extends CI_Controller {

    public function __construct() {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
        parent::__construct();
        $this->load->model("produtos_model", "pm");
        $this->load->model("cursos_model", "cm");
        $this->load->library('cart');
		$this->cart->product_name_rules = '\d\D';
    }

public function add_curso_idioma(){
                    $this->load->model('produtos_model', 'produtom');
                    $produto = $this->produtom->get_by_id($this->input->post('curso'));
                    $data    = array(
                        'id'     => 'CI' . $historico[0]->id . date('s').  uniqid(),
                        'qty'    => 1,
                        'price'  =>$produto[0]->valor,
                        'name'   => $produto[0]->nome,
                        'descricao'  => $produto[0]->nome ,
                        'tipo'       => 'IDI',
                        'historico'  => '',
                        'id_produto' => $this->input->post('curso')
                    );

                    if ($this->cart->insert($data)) {
                        $erro++;
                        $this->session->set_flashdata('success', 'Itens adicionados com succeso!');
                        redirect('finalizarPedido');
                    }else{
                         $this->session->set_flashdata('danger', 'Não foi possível adicionar o item selecionado');
                    }
    }



    public function add_certificado() {
        $dados = explode(":",decodifica($this->input->post('iad')));//0-id_historico, 1-id_avaliacao
        if ($dados[0]){
            $this->load->model("historico_model", "hm");
            $historico = $this->hm->get_dados_carrinho_by_id_historico($dados[0])->result();
            $erro = 0;
            if (count($historico)>0 && count($this->input->post('id_produto'))>=1) {
                $this->load->model('produtos_model', 'produtom');
                foreach ($this->input->post('id_produto') as $pr){
                    $produto = $this->produtom->get_by_id(decodifica($pr));
                    $data    = array(
                        'id'     => 'CI' . $historico[0]->id . date('s').  uniqid(),
                        'qty'    => 1,
                        'price'  => $produto[0]->valor,
                        'name'   => $produto[0]->nome . ' ' . ucfirst(strtolower($historico[0]->nome)),
                        'descricao'  => $produto[0]->nome . ' do curso de ' . ucfirst(strtolower($historico[0]->nome)),
                        'tipo'       => 'CI',
                        'historico'  => $historico[0]->id,
                        'id_produto' => decodifica($pr)
                    );
                    if ($this->cart->insert($data)) {
                        $erro++;
                        $this->session->set_flashdata('success', 'Itens adicionados com succeso!');
                    }else{
                         $this->session->set_flashdata('danger', 'Não foi possível adicionar o item selecionado');
                    }
                }
                if($erro==count($this->input->post('id_produto'))){
                    if(count($this->input->post('id_produto')>1)){
                        $this->session->set_flashdata('success', 'Itens adicionados com succeso!');
                    }else{
                        $this->session->set_flashdata('success', 'Item adicionado com succeso!');
                    }
                }else{
                    $this->session->set_flashdata('danger', 'Não foi possível adicionar o item selecionado');
                }
            }else{
                $this->session->set_flashdata('danger', 'Não foi possível adicionar o item selecionado');
            }
        }else{
            $this->session->set_flashdata('danger', 'Não foi possível adicionar o item selecionado');
        }
        redirect('finalizarPedido');
    }

    public function solicitar_segunda_chance() {
        if ($this->uri->segment(2)) {
            $h = decodifica($this->uri->segment(2));
            $this->load->model("historico_model", "hm");
            $historico = $this->hm->verifica_situacao_segunda_chance($h)->result();
            
             if (count($this->cart->contents()) > 0) {
                 foreach($this->cart->contents() as $ca){
                    if($ca['historico'] == $h && $ca['tipo'] == "SC"){
                        $this->session->set_flashdata('danger', 'Você já solicitou uma prova de segunda chamada.');
                        redirect('cursos_matriculados');
                    }
                 }
             }
            
            if (count($historico) >= 1 && $this->hm->verifica_segunda_chance($h) == 0) {
                if ($historico[0]->id_fic == $this->session->userdata("imazon_id_fic")) {
                    //adiciona fatura p segunda-chance
                    $this->load->model('produtos_model', 'produtom');
                    $produto = $this->produtom->get_by_id(4);
                    
                    $data = array(
                        'id' => 'SC' . $historico[0]->id . date('m'),
                        'qty' => 1,
                        'price' => $produto[0]->valor,
                        'name' => $produto[0]->nome . ' ' . ucfirst(strtolower($historico[0]->nomeCurso)),
                        'descricao' => $produto[0]->nome . ' do curso de ' . ucfirst(strtolower($historico[0]->nomeCurso)),
                        'tipo' => 'SC',
                        'historico' => $historico[0]->id,
                        'id_produto' => 4
                    );
                    
                    if ($this->cart->insert($data) !== false) {
                        $this->session->set_flashdata('success', 'Item adicionado com succeso!');
                    } else {
                        $this->session->set_flashdata('danger', 'Não foi possível adicionar o item selecionado');
                    }
                }
                
            }else {
                $this->session->set_flashdata('danger', 'Você já solicitou uma prova de segunda chamada.');
                redirect('cursos_matriculados');
            }
            
            redirect('finalizarPedido');
        } else {
            $this->session->set_flashdata('danger', 'Não foi possível adicionar o item selecionado');
            redirect('finalizarPedido');
        }
    }

    public function add() {
       
        $this->load->library("form_validation");
        $this->form_validation->set_rules('carga_horaria', 'carga_horaria', 'required');
        $this->form_validation->set_rules('curso', 'curso', 'required');
        
        if ($this->form_validation->run()) {
            
            $carga_horaria = $this->pm->get_carga_horaria(decodifica($this->input->post('carga_horaria')));
            $curso = $this->cm->get_curso_by_id(decodifica($this->input->post('curso')));
            var_dump($carga_horaria);
           
            if (count($carga_horaria) === 1) {
                $data = array(
                    'id' => decodifica($this->input->post('curso')),
                    'qty' => 1,
                    'price' => $carga_horaria[0]->valor,
                    'name' => "Matrícula no CURSO ".str_replace(',', '', $curso[0]->nome),
                    'descricao' => "Matrícula no CURSO ". str_replace(',', '', $curso[0]->nome),
                    'tipo' => 'MA',
                    'ch' => $carga_horaria[0]->tempo,
                    'id_produto' => $carga_horaria[0]->id_produto,
                    'historico' => decodifica($this->input->post('historico')),
                );
                
                if ($this->cart->insert($data)) {
//                    die('sucesso');
                    $this->session->set_flashdata('success', 'Item adicionado com succeso!');
                } else {
//                    die('nao sucesso');
                    $this->session->set_flashdata('danger', 'Não foi possível adicionar o item selecionado');
                }
                
            } else {
//                die('ch');
                $this->session->set_flashdata('warning', 'Você precisa selecionar a carga horária!');
            }
        } else {
//            die('ch2');
            $this->session->set_flashdata('warning', 'Você precisa selecionar a carga horária!');
        }
//        var_dump($this->session);
       
        redirect('finalizarPedido');
    }
    
    public function add_item_matricula($idcurso = null, $ch = null, $historico = null) {
 
            if($idcurso == null || $ch == null || $historico == null) redirect("cursos_matriculados");
        
            $carga_horaria = $this->pm->get_carga_horaria(decodifica($ch));
            $curso = $this->cm->get_curso_by_id(decodifica($idcurso));
            if (count($carga_horaria) === 1) {
                $data = array(
                    'id' => decodifica($idcurso),
                    'qty' => 1,
                    'price' => $carga_horaria[0]->valor,
                    'name' => "Matrícula no CURSO ".str_replace(',', '', $curso[0]->nome),
                    'descricao' => "Matrícula no CURSO ". str_replace(',', '', $curso[0]->nome),
                    'tipo' => 'MA',
                    'ch' => $carga_horaria[0]->tempo,
                    'id_produto' => $carga_horaria[0]->id_produto,
                    'historico' => decodifica($historico),
                );
                if ($this->cart->insert($data)) {
                    $this->session->set_flashdata('success', 'Item adicionado com succeso!');
                } else {
                    $this->session->set_flashdata('danger', 'Não foi possível adicionar o item selecionado');
                }
            } else {
                $this->session->set_flashdata('warning', 'Você precisa selecionar a carga horária!');
            }
       
            redirect('finalizarPedido');
    }

    public function del() {
        $id = $this->uri->segment(2);
        if ($id) {
            $data = array(
                'rowid' => $id,
                'qty' => 0
            );
            if ($this->cart->update($data)) {
                $this->session->set_flashdata('success', 'Item removido com succeso!');
            } else {
                $this->session->set_flashdata('danger', 'Não foi possível remover o item selecionado. Tente novamente.');
            }
            redirect('finalizarPedido');
        }
    }

    public function clean_cart() {
        $this->cart->destroy();
        $this->session->set_flashdata('success', 'Itens removidos com succeso!');
        redirect('cursos');
    }

}
