<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Blog extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("usuarios_model", "um");
        $this->load->model("posts_model", "pm");
        $this->load->library('cart');
    }

    public function index($inicio = null) {
        $this->load->library('pagination');

        $config['base_url'] = base_url() . 'blog';
        $config['total_rows'] = count($this->pm->count_todos_posts_publicados());
        $config['per_page'] = 12;
        $config['uri_segment'] = 2;
        $this->pagination->initialize($config);
        if ($inicio == null) $inicio = 0;
        $res = $this->pm->todos_posts_publicados(array('inicio' => $inicio, 'limite' => $config['per_page']));
        $count = count($res);
        $data = array('titulo' => 'Blog', 'palavra_chave' => 'Últimas postagens do Imazon Cursos', 'paginacao' => $this->pagination->create_links());

        ($count >= 1) ? $data['postagens'] = $res : $data['postagens'] = null;

        $this->parser->parse('site/include/head', $data);
        $this->load->view('site/include/sidebar_blog');
        $this->load->view('site/tela/ultimas_blog');
        $this->load->view('site/include/foot_blog');
    }
    
    public function mais_visitados($inicio = null) {
        $this->load->library('pagination');
        
        $config['base_url'] = base_url() . 'mais_visitados';
        $config['total_rows'] = $this->pm->count_todos_posts_mais_visitados();
        $config['per_page'] = 12;
        $config['uri_segment'] = 2;
        $this->pagination->initialize($config);
        if ($inicio == null) $inicio = 0;
        $res = $this->pm->todos_posts_mais_visitados(array('inicio' => $inicio, 'limite' => $config['per_page']));
        $count = count($res);
        $data = array('titulo' => 'Mais visitadas', 'palavra_chave' => 'Postagens mais visitadas do Blog Imazon Cursos', 'paginacao' => $this->pagination->create_links());

        ($count >= 1) ? $data['postagens'] = $res : $data['postagens'] = null;

        $this->parser->parse('site/include/head', $data);
        $this->load->view('site/include/sidebar_blog');
        $this->load->view('site/tela/mais_visitadas');
        $this->load->view('site/include/foot_blog');
    }

    public function post_por_categoria($categoriaNome = null ,$inicio = null) {
        $this->load->library('pagination');
        
        $categoria = $this->pm->get_id_categoria_by_nome($categoriaNome);
        if(count($categoria)===1){
            
        }else{
            redirect('blog');
        }
        
        $config['base_url'] = base_url() . 'mais_visitados';
        $config['total_rows'] = $this->pm->count_todos_posts_por_categoria(array('categoria'=>$categoria[0]->id));
        $config['per_page'] = 12;
        $config['uri_segment'] = 2;
        $this->pagination->initialize($config);
        if ($inicio == null) $inicio = 0;
        $res = $this->pm->todos_posts_por_categoria(array('inicio' => $inicio, 'limite' => $config['per_page'], 'categoria'=>$categoria[0]->id));
        $count = count($res);
        $data = array('titulo' => 'Categoria: '.urldecode($categoriaNome), 'palavra_chave' => "Postagens na categoria $categoriaNome  do Blog Imazon Cursos", 'paginacao' => $this->pagination->create_links());

        ($count >= 1) ? $data['postagens'] = $res : $data['postagens'] = null;

        $this->parser->parse('site/include/head', $data);
        $this->load->view('site/include/sidebar_blog');
        $this->load->view('site/tela/post_por_categoria');
        $this->load->view('site/include/foot_blog');
    }
    
    public function buscar_post($inicio = null) {

        if (!$this->input->post("post")) {
            redirect('blog');
        } else {
            $termo = $this->input->post("post");
        }

        $this->load->model("cursos_model", "cm");
        $this->load->library('pagination');

        $config['base_url'] = base_url() . 'post/busca';
        $config['total_rows'] = count($this->pm->count_todos_posts_busca(array('titulo' => $termo)));
        $config['per_page'] = 8;
        $config['uri_segment'] = 2;
        $this->pagination->initialize($config);
        if ($inicio == null) $inicio = 0;
        $res = $this->pm->todos_posts_busca(array('inicio' => $inicio, 'limite' => $config['per_page'], 'titulo' => $termo));
        if (count($res) > 0) {
            $count = count($res);
            
            $data = array('titulo' => 'Blog', 'palavra_chave' => 'Buscar por: ' . $termo, 'termo' => $termo, 'paginacao' => $this->pagination->create_links());
            ($count >= 1) ? $data['postagens'] = $res : $data['postagens'] = null;
            
            $this->parser->parse('site/include/head', $data);
            $this->load->view('site/include/sidebar_blog');
            $this->load->view('site/tela/busca_blog');
            $this->load->view('site/include/foot_blog');
            
        }else {
            redirect('blog');
        }
    }

    public function visualizar($id = null) {

        if ($id !== null) {
            $postagem = $this->pm->get_by_page_id($id);
            
            if (count($postagem)) {
                $this->load->library('user_agent');
                //registra log de visita
                $this->pm->insert_log_visita(array('ip'=>getenv("REMOTE_ADDR"), 'navegador'=>$this->agent->browser(), 'post'=>$postagem[0]->id, 'dt_cadastro'=>date('Y-m-d h:m:s')));
                $data = array('titulo' => $postagem[0]->titulo, 'conteudo' => $postagem[0]->conteudo, 'palavra_chave' => 'Blog do Imazon Cursos');
                $this->parser->parse('site/include/head', $data);
                $this->load->view('site/include/sidebar_blog');
                $this->load->view('site/tela/visualizar_postagem');
                $this->load->view('site/include/foot_blog');
            } else {
                redirect('blog');
            }
        } else {
            redirect('blog');
        }
    }

}
