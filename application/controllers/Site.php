<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Site extends CI_Controller {

    public function __construct() {
        
        parent::__construct();
                    
        $this->load->model("usuarios_model", "um");
        

        $this->load->model("produtos_model", "pm");

        $this->load->model("historico_model", "hm");

        $this->load->model('termos_model', 'tem');

        $this->load->model('base_legal_model', 'bm');
        
        $this->load->library('cart');
        
        //Instancia a classe fpdf
        $params = array('orientation' => 'L', 'unit' => 'mm', 'size' => 'A4');
        require_once(APPPATH.'libraries/fpdf/Fpdf.php');
        $this->fpdf = new FPDF($params);

        if ($this->cart->total_items() > 1) {

            $this->session->set_flashdata('alerta-carrinho', 'Você possui ' . $this->cart->total_items() . ' itens no seu carrinho. <a href="' . base_url('finalizarPedido') . '"><b>Clique aqui</b></a> para efetuar o pagamento!');
        } else if ($this->cart->total_items() == 1) {

            $this->session->set_flashdata('alerta-carrinho', 'Você possui 1 item no seu carrinho. <a href="' . base_url('finalizarPedido') . '"><b>Clique aqui</b></a> para efetuar o pagamento!');
        }
    }

    //telas

    public function index() {

        redirect('site/home');
    }

    public function segunda_via() {

        $data = array(
            'titulo' => '2ª Via de fatura',
            'palavra_chave' => 'Imazon Cursos'
        );

        $this->load->library("form_validation");
        $this->form_validation->set_rules("email", "email", "valid_email|required|min_length[6]|max_length[160]");

        if ($this->input->post() !== null && $this->form_validation->run() == true) {
            $email = $this->input->post("email");
            $usuario = $this->um->get_by_email($email)->result();
            
            if (count($usuario) == 1) {
                $this->load->model("pedido_model", "pedidom");

                $itens_pedido = array();
                $data["resultado"] = $this->pedidom->get_transacoes_em_aberto_by_id_usuario($usuario[0]->id_fic);
                
                foreach ($data["resultado"] as $transacoes) {
                    $itens_pedido[0][$transacoes->transacao] = $this->pedidom->get_historico_itens_transacao($transacoes->transacao)->result();
                }

                $data["itens_pedido"] = $itens_pedido;
            }
        }

        $this->parser->parse('site/include/head', $data);
        $this->load->view('site/tela/segunda_via');
        $this->load->view('site/include/foot');
    }

    public function termo() {

        $data = array(
            'titulo' => 'Termo de uso',
            'palavra_chave' => 'Imazon Cursos'
        );

        $data["termo_uso"] = $this->tem->get_termo_valido()->result();

        $this->parser->parse('site/include/head', $data);

        $this->load->view('site/tela/termo_uso');

        $this->load->view('site/include/foot');
    }

    public function termo_novo() {

        $data = array(
            'titulo' => 'Termo de uso',
            'palavra_chave' => 'Imazon Cursos'
        );

        $data["termo_uso"] = $this->tem->get_termo_valido()->result();

        $this->parser->parse('site/include/head', $data);

        $this->load->view('site/tela/novo_termo_uso');

        $this->load->view('site/include/foot');
    }

    public function concordar_termo() {

        if ($this->session->userdata("imazon_id_fic") && $this->session->userdata("termo_pendente")) {

            $termo = $this->tem->get_termo_valido()->result();

            $log_termo = array("id_aluno" => $this->session->userdata("imazon_id_fic"), "id_termo" => $termo[0]->id_termo, "ip" => getenv("REMOTE_ADDR"), "data_aceite" => date("Y-m-d h:i:s"));

            if ($this->tem->add_termo_aluno($log_termo)) {

                $aluno = array('extra' => $termo[0]->id_termo);

                if ($this->um->update($aluno)) {

                    $this->session->set_flashdata('success', 'Agora você pode continuar utilizando os nossos serviços!');

                    $this->session->unset_userdata("termo_pendente");

                    redirect("cursos_matriculados");
                } else {

                    $this->session->set_flashdata('warning', 'Vocẽ não pode fazer isso');
                }
            } else {

                $this->session->set_flashdata('warning', 'Vocẽ não pode fazer isso');
            }
        } else {

            $this->session->set_flashdata('warning', 'Vocẽ não pode fazer isso');
        }

        redirect("site");
    }

    public function home() {
        
        $this->notificacao_model->notifica_faturas_abertas();

        $data = array(
            'titulo' => 'Cursos com Certificado',
            'palavra_chave' => 'Imazon Cursos'
        );
        
        //Criação de captcha em 08/09/2017
        $captcha = array();
        $this->parser->parse('site/include/head_home', $data);
        $this->load->view('site/tela/home');
        $this->load->view('site/include/foot', $captcha);
    }

    public function home2() {

        $this->notificacao_model->notifica_faturas_abertas();

        $data = array(
            'titulo' => 'Cursos com Certificado',
            'palavra_chave' => 'Imazon Cursos'
        );

        $this->parser->parse('site/include/head_home', $data);

        $this->load->view('site/tela/home_1');

        $this->load->view('site/include/foot');
    }

    public function suporte() {

        $data = array(
            'titulo' => 'Suporte',
            'palavra_chave' => 'Suporte Imazon Cursos'
        );

        $this->parser->parse('site/include/head', $data);

        $this->load->view('site/tela/suporte');

        $this->load->view('site/include/foot');
    }

    public function promocao() {

        $data = array(
            'titulo' => 'Promoção',
            'palavra_chave' => 'Imazon Cursos'
        );

        $this->parser->parse('site/include/head', $data);

        $this->load->view('site/tela/promocao');

        $this->load->view('site/include/foot');
    }

    public function cursos() {

        $data = array(
            'titulo' => 'Mais de 1000 Cursos Disponíveis',
            'palavra_chave' => 'Imazon Cursos'
        );
        $this->parser->parse('site/include/head', $data);
        $this->load->view('site/include/sidebar_categorias_cursos');
        $this->load->view('site/tela/cursos');
        $this->load->view('site/include/foot');
    }

    public function cursos_livres($inicio = null) {

        $this->load->model("cursos_model", "cm");

        $this->load->library('pagination');

        $config['base_url'] = base_url() . 'cursos_livres';

        $config['total_rows'] = count($this->cm->count_todos_cursos_home());

        $config['per_page'] = 5;

        $config['uri_segment'] = 2;

        $this->pagination->initialize($config);

        $res = $this->cm->todos_cursos_home(array('inicio' => $inicio, 'limite' => $config['per_page']));

        $count = count($res);

        $data = array('titulo' => 'Mais de 1000 Cursos Disponíveis', 'palavra_chave' => 'Imazon Cursos', 'paginacao' => $this->pagination->create_links());

        ($count >= 1) ? $data['cursos'] = $res : $data['cursos'] = null;

        $data['valor_minimo_carga_horaria'] = $this->pm->get_valor_minimo_carga_horaria();

        $this->parser->parse('site/include/head', $data);

        $this->load->view('site/include/sidebar_categorias_cursos');

        $this->load->view('site/tela/cursos_home');

        $this->load->view('site/include/foot');
    }

    public function cursos_matriculados() {

        redirect('aluno/curso/index');
    }

    public function buscar_curso($inicio = null) {



        if ($this->input->post("busca") && strlen($this->input->post("busca")) >= 2) {

            $termo = $this->input->post("busca");
        } else {

            redirect('cursos');
        }



        $this->load->model("cursos_model", "cm");



        $res = $this->cm->todos_cursos_busca(array('inicio' => $inicio, 'limite' => 15, 'nome' => $termo));

        $count = count($res);

        $data = array('titulo' => 'Buscar curso ' . $termo, 'palavra_chave' => 'Imazon Cursos', 'termo' => $termo);

        ($count >= 1) ? $data['cursos'] = $res : $data['cursos'] = null;

        $this->parser->parse('site/include/head', $data);

        $this->load->view('site/include/sidebar_categorias_cursos');

        $this->load->view('site/tela/busca_cursos_home');

        $this->load->view('site/include/foot');
    }

    public function redir_cad_matricula() {

        if ($this->input->post("curso") && $this->input->post("carga_horaria")) {

            redirect("matricular_cadastrar/" . $this->input->post("curso") . '/' . $this->input->post("carga_horaria"));
        } else {

            redirect("cursos");
        }
    }

    public function cursos_areas($termo = null) {



        if (!$termo) {

            redirect('cursos');
        }



        $this->load->model("cursos_model", "cm");

        $this->load->library('pagination');



        $config['base_url'] = base_url() . 'cursos';

        $config['total_rows'] = count($this->cm->count_todos_cursos_areas(array('nome' => urldecode($termo))));

        $config['per_page'] = 8;

        $config['uri_segment'] = 2;

        $this->pagination->initialize($config);



        $res = $this->cm->todos_cursos_areas(array('nome' => urldecode($termo)));

        $count = count($res);

        $data = array('titulo' => 'Cursos de ' . urldecode($termo), 'palavra_chave' => 'Imazon Cursos', 'termo' => urldecode($termo), 'paginacao' => $this->pagination->create_links());

        $data['valor_minimo_carga_horaria'] = $this->pm->get_valor_minimo_carga_horaria();

        ($count >= 1) ? $data['cursos'] = $res : $data['cursos'] = null;



        $this->parser->parse('site/include/head', $data);

        $this->load->view('site/include/sidebar_categorias_cursos');

        $this->load->view('site/tela/cursos_home');

        $this->load->view('site/include/foot');
    }

    public function mais_vendidos() {





        $this->load->model("cursos_model", "cm");



        $res = $this->cm->mais_vendidos_home();

        $count = count($res);

        $data = array('titulo' => 'Cursos mais vendidos', 'palavra_chave' => 'Imazon Cursos');

        $data['valor_minimo_carga_horaria'] = $this->pm->get_valor_minimo_carga_horaria();

        ($count >= 1) ? $data['cursos'] = $res : $data['cursos'] = null;



        $this->parser->parse('site/include/head', $data);

        $this->load->view('site/include/sidebar_categorias_cursos');

        $this->load->view('site/tela/cursos_home');

        $this->load->view('site/include/foot');
    }

    public function visualizar_curso($curso = null) {



        if ($curso !== null) {

            $this->load->model("cursos_model", "cm");



            //$curso = $this->cm->get_descricao_cursos_by_nome(urldecode($curso));
            $curso = $this->cm->get_descricao_cursos_by_nome(html_entity_decode(urldecode($curso)));



            $data = array(
                'id' => $curso[0]->id_curso,
                'titulo' => "CURSO " . $curso[0]->nome,
                'descricao' => $curso[0]->descricao,
                'imagem' => $curso[0]->imagem,
                'palavra_chave' => 'Imazon Cursos'
            );



            $this->parser->parse('site/include/head', $data);

            $this->load->view('site/include/sidebar_categorias_cursos');

            $this->load->view('site/tela/visualizar_curso');

            $this->load->view('site/include/foot');
        } else {

            redirect('cursos');
        }
    }

    public function itens_carrinho() {
        if ($this->cart->total_items() > 0) {
            $data = array(
                'titulo' => 'Seu pedido',
                'palavra_chave' => 'Imazon Cursos'
            );
            $this->parser->parse('site/include/head', $data);
            $this->load->view('site/include/sidebar_categorias_cursos');
            $this->load->view('site/tela/itens_carrinho');
            $this->load->view('site/include/foot');
        } else {
            redirect('cursos');
        }
    }
    
    private function get_saldo_indiqueGanhe($id_fic)
    {
        
    }

    public function finalizarPedido() {

        if (count($this->cart->contents()) == 0)
            redirect("cursos_matriculados");

        if ($this->session->userdata("preencher_endereco") != true)
        {
            $this->session->set_flashdata('info', 'Por favor, informe seu endereço de cobrança.');
            redirect('endereco_cobranca');
        }

        $confirma_endereco = 0;
        $confirma_contato = 0;
        $this->load->model("notificacao_model", "nm");

        $preco='';
        foreach ($this->cart->contents() as $c) {
            $preco += $c['price'];
            if ($c['tipo'] == "CI") {
                $confirma_endereco++;
            }
        }
        

        if ($this->nm->verifica_dados_aluno())
        {
            if ($this->cart->total_items() > 0)
            {
                //
                $id_fic = $this->session->userdata('imazon_id_fic');
                $this->load->model('Cliente_model','cl');
                $saldo = $this->get_saldo_cliente($id_fic);
                //
                
                $data = array(
                    'titulo' => 'Finalizar pedido',
                    'palavra_chave' => 'Imazon Cursos',
                    'id_fic'=> codifica( $id_fic),
                    'saldo' =>$saldo,
                    'preco'=>$preco,
                    'preco_codificado'=> codifica($preco),
                    'cart'=>$this->cart->contents()
                );
                
                $this->load->library('table');
                $this->parser->parse('site/include/head', $data);
                $this->load->view('site/tela/finalizarPedido');
                $this->load->view('site/include/foot');
                $this->session->unset_userdata("preencher_endereco");
            } else {
                redirect('cursos');
            }
        }
        else
        {
            $this->session->unset_userdata("preencher_endereco");
            $this->session->set_flashdata('info', 'Por favor, informe seu endereço de cobrança.');
            redirect('endereco_cobranca');
        }
    }
    
    private function get_saldo_cliente($id_fic)
    {
        $id_fic = array('id_fic'=>$id_fic);
        $url = base_url().'api/ClienteApi/get_bonificacao_cliente';
        
        $cr = curl_init();
        curl_setopt($cr, CURLOPT_URL, $url);
        curl_setopt($cr, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($cr, CURLOPT_POST, TRUE);
        curl_setopt($cr, CURLOPT_POSTFIELDS, http_build_query($id_fic) );
        $saldo = curl_exec($cr);
        curl_close($cr);
        return $saldo;
    }

    public function sair() {
        //atualiza sessão
        if ($this->session->userdata('session_id')) {
            $this->load->model('usuarios_model', 'um');
            $this->um->finaliza_auditoria();
        }
        $this->session->sess_destroy();
        $this->session->set_flashdata('success', 'Você efetuou logoff. Volte sempre!');
        redirect('http://cursosapp.com.br/');
    }

    public function logar() {

        $this->load->library("form_validation");
        $this->form_validation->set_rules('email', 'email', 'valid_email|required|min_length[6]|trim');
        $this->form_validation->set_rules('senha', 'senha', 'required|matches[senha]|min_length[6]|trim');
        if ($this->form_validation->run()) {
        
            $usuario = array('email' => $this->input->post('email'), 'senha' => $this->input->post('senha'));
            //var_dump($usuario);
            //die;
            if ($this->um->verifica_login($usuario)) {
                $aluno = $this->um->get_by_email($this->input->post("email"))->result();
                
                if (count($aluno) === 1) {
                    if ($aluno[0]->situacao_aluno == 0) {

                        echo "Erro login bloqueado";

                        exit;
                    }
                    //atribuição para uma variaval do "session_id"
                    $session_id = md5(rand(date('m'), date('s')));
                    $session = array(
                        'imazon_id_aluno' => $aluno[0]->id_aluno,
                        'imazon_id_fic' => $aluno[0]->id_fic,
                        'imazon_session_id ' => $session_id,
                        'imazon_email_aluno' => $this->input->post("email"),
                        'imazon_logado' => TRUE,
                        'imazon_nome' => $aluno[0]->nome
                    );
                    
                    $this->session->set_userdata($session);
                   
                    $this->load->library('user_agent');
                    //Criação do array audit
                    $audit = array(
                        'session_id' => $session_id, 
                        'id_aluno' => $aluno[0]->id_aluno,  
                        'data_auditoria' => date("Y-m-d h:i:s"), 
                        'ip' => getenv("REMOTE_ADDR"), 
                        'navegador' => $this->agent->browser() . ' - ' . $this->agent->version(), 
                        'sistema' => $this->agent->platform());

                    $this->um->add_auditoria($audit);
                    
                    $ultimo_termo_aluno = $this->tem->get_ultimo_termo_usuario($aluno[0]->id_fic)->result();
                    $ultimo_termo = $this->tem->get_termo_valido($aluno[0]->id_fic)->result();
                    if (count($ultimo_termo_aluno) == 1) {
                        if ($ultimo_termo[0]->id_termo != $ultimo_termo_aluno[0]->id_termo) {
                            $this->session->set_userdata(array("termo_pendente" => true));
                        }
                    } else {
                        $this->session->set_userdata(array("termo_pendente" => true));
                    }
                    echo "ok";
                } else {
                    echo "Erro login";
                }
            } else {
                echo "Erro login";
            }
        } else {
            echo "false";
        }
    }

    public function autenticador() {

        $data = array(
            //'titulo' => 'Autenticador de certificado',
            'titulo' => 'Certificado',
            'palavra_chave' => 'Imazon Cursos'
        );

        if ($this->input->post("codigo-autenticacao") && strlen(trim($this->input->post("codigo-autenticacao"))) >= 5) {

            //$tipo = (strpos($this->input->post("codigo-autenticacao"), "-", 0) != 0) ? "codigo" : "nome";
            $tipo = (strpos($this->input->post("codigo-autenticacao"), " ", 0) == 0) ? "codigo" : "nome";

            $this->load->library("form_validation");

            $this->form_validation->set_rules("codigo-autenticacao", "Nome", "required|min_length[5]|max_length[60]|trim");

            if ($this->form_validation->run()) {

                $termo = $this->input->post("codigo-autenticacao");

                if ($tipo == "nome") {

                    $data['nome'] = $this->um->get_aluno_by_nome($termo)->result();

                    if (count($data['nome']) > 0) {

                        foreach ($data['nome'] as $aluno) {

                            $data['resultado'][$aluno->id_fic] = $this->um->autentica_certificado($aluno->id_fic, $tipo);

                            $data['resultado_vip'][$aluno->id_fic] = $this->um->buscar_certificado_imazon_vip($aluno->id_fic)->result();

                            $data['resultado_vip_imp'][$aluno->id_fic] = $this->um->buscar_certificado_impresso_imazon_vip($aluno->id_fic)->result();
                        }
                    } else {

                        $data["resultado"] = null;

                        $data["resultado_vip"] = null;

                        $data["resultado_vip_imp"] = null;
                    }
                } else {

                    $data['resultado_imazon'] = $this->um->autentica_certificado($termo, $tipo);

                    $data['resultado_vip2'] = $this->hm->gera_autenticacao_certificado_vip_by_cod_autenticacao($termo)->result();

                    $data['resultado_vip2_imp'] = $this->hm->gera_autenticacao_certificado_impresso_vip_by_cod_autenticacao($termo)->result();

                    if (count($data['resultado_vip2']) > 0) {

                        $data['nome'] = $this->um->get_by_id_fic($data['resultado_vip2'][0]->email)->result();
                    } else if (count($data['resultado_imazon']) > 0) {

                        $data['nome'] = $data['resultado_imazon'];
                    } else if (count($data['resultado_vip2_imp']) > 0) {

                        $data['nome'] = $this->um->get_by_id_fic($data['resultado_vip2_imp'][0]->email)->result();
                    } else {
                        $data["resultado"] = null;

                        $data["resultado_vip2"] = null;

                        $data["resultado_vip2_imp"] = null;
                    }
                }
            }
        }

        $this->parser->parse('site/include/head', $data);
        
        $this->load->view('site/tela/autenticador');
        
        $this->load->view('site/include/foot');
    }

    public function gera_autenticacao_certificado() {

        if ($this->uri->segment(2)) {
            
            $this->load->model('historico_model', 'hm');

            $id_historico = decodifica($this->uri->segment(2));
            $historico    = $this->hm->gera_autenticacao_certificado($id_historico)->result();
            
//            $n_c = strlen('OS DIREITOS DAS PESSOAS PORTADORAS DE DEFICIÊNCIAS: PREVISÃO    ');
//            echo $n_c;
//            die;
            
            $array_dados_certficado['texto']            = '';
            $array_dados_certficado['aproveitamento']   = '';
            $array_dados_certficado['data_termino']     = '';
            $array_dados_certficado['data_inicio']      = '';
            $array_dados_certficado['situacao']         = '';
            
            if (count($historico) >= 1) {
                
                $this->load->model('base_legal_model', 'blm');
                $base_legal = $this->blm->get_atual()->result();
                
                switch ($historico[0]->situacao) {
                    case 1:
                        $array_dados_certficado['texto']            = 'Por estar participando do Curso Livre';
                        $array_dados_certficado['aproveitamento']   = '';
                        //$array_dados_certficado['data_inicio']      = 'Data de Início: '.date('d/m/Y', strtotime($historico[0]->primeiro_dia));
                        $array_dados_certficado['periodo']      = 'Período: '.date('d/m/Y', strtotime($historico[0]->primeiro_dia));
                        //$array_dados_certficado['data_termino']     = '';
                        $array_dados_certficado['situacao']         = 'Situação: Cursando';
                        break;
                    case 2:
                        $array_dados_certficado['texto']            = 'Por ter Concluído o Curso Livre';
                        $array_dados_certficado['aproveitamento']   = 'Aproveitamento: '.$historico[0]->nota.',0';
                        //$array_dados_certficado['data_inicio']      = 'Data de Início: '.date('d/m/Y', strtotime($historico[0]->primeiro_dia));
                        $array_dados_certficado['periodo']      = 'Período: '.date('d/m/Y', strtotime($historico[0]->primeiro_dia)).' à '.date('d/m/Y', strtotime($historico[0]->data_aprovacao));
                        //$array_dados_certficado['data_termino']     = 'Data de término: '.date('d/m/Y', strtotime($historico[0]->data_aprovacao));
                        $array_dados_certficado['situacao']         = 'Situação: Aprovado.';
                        break;
                    case 3:
                        $array_dados_certficado['texto']            = 'Por ter participado do Curso Livre';
                        $array_dados_certficado['aproveitamento']   = '';
                        //$array_dados_certficado['data_inicio']      = 'Data de Início: '.date('d/m/Y', strtotime($historico[0]->primeiro_dia));
                        $array_dados_certficado['periodo']      = 'Período: '.date('d/m/Y', strtotime($historico[0]->primeiro_dia)).' à '.date('d/m/Y', strtotime($historico[0]->data_aprovacao));
                        //$array_dados_certficado['data_termino']     = 'Data de término: '.date('d/m/Y', strtotime($historico[0]->data_aprovacao));
                        $array_dados_certficado['situacao']         = '';
                        break;
                    case 4:
                        $array_dados_certficado['texto']            = 'Por ter participado do Curso Livre';
                        $array_dados_certficado['aproveitamento']   = '';
                        //$array_dados_certficado['data_inicio']      = 'Data de Início: '.date('d/m/Y', strtotime($historico[0]->primeiro_dia));
                        $array_dados_certficado['periodo']      = 'Período: '.date('d/m/Y', strtotime($historico[0]->primeiro_dia));
                        //$array_dados_certficado['data_termino']     = '';
                        $array_dados_certficado['situacao']         = '';
                        break;
                }
                
                
                //instancia fpdf  
                
                
                //$this->load->library('fpdf', $params);
              
                // Página de Verso	
                $this->fpdf->AliasNbPages();                
                $this->fpdf->AddPage();
                $this->fpdf->Image('imgs/fundo_certificado_imazon_frente.jpg', 0, 0, 300, 'JPG'); //logo
                $this->fpdf->setY("58");
                $this->fpdf->setX("5");
                $this->fpdf->SetFont('Arial', '', 16);
                $this->fpdf->Cell(0, 10, 'conferido a', 0, 1, 'C');
                $this->fpdf->SetFont('Arial', 'B', 16);
                $this->fpdf->Cell(0, 6, utf8_decode($historico[0]->nomeAluno), 0, 1, 'C');
                    
                if($historico[0]->rg != ""){
                    $this->fpdf->SetFont('Arial', '', 16);
                    $this->fpdf->Cell(0, 7, 'RG: ' . utf8_decode($historico[0]->rg), 0, 1, 'C');
                    $this->fpdf->SetFont('Arial', '', 16);
                    $this->fpdf->Cell(0, 5, utf8_decode($array_dados_certficado['texto']), 0, 1, 'C');
                }else{
                    $this->fpdf->SetFont('Arial', '', 16);
                    $this->fpdf->Cell(0, 7, utf8_decode($array_dados_certficado['texto']), 0, 1, 'C');
                }
                
                
                $this->fpdf->setY(115);
                $this->fpdf->Cell(0, 10, utf8_decode($array_dados_certficado['situacao']), 0, 1, 'C');
                
                $nome_curso = wordwrap($historico[0]->nomeCurso, 60, "\n", true);
                $nome_curso = utf8_decode($nome_curso);
                //$nome_curso_original = utf8_decode($historico[0]->nomeCurso);
                
                
                if($historico[0]->rg != ""){
                    $this->fpdf->setY(88);
                    $this->fpdf->MultiCell(0, 6, $nome_curso, 0, 'C');
                }else{
                    $this->fpdf->setY(77);
                    $this->fpdf->MultiCell(0, 16, $nome_curso, 0, 'C');
                }
                //Inicio de ajuste para nome de curso que excede o limite da linha
                $this->fpdf->setY(100);
                $this->fpdf->setX(50);
                $this->fpdf->Cell(0, 5, utf8_decode('Tipo: Capacitação/Atualização'), 0, 1);
                $this->fpdf->setX(50);
                $this->fpdf->Cell(0, 5, $array_dados_certficado['aproveitamento'], 0, 1);
                $this->fpdf->setX(50);
                $this->fpdf->Cell(0, 5, utf8_decode('Cert. Nº ') . $historico[0]->id_historico, 0, 1);
                $this->fpdf->setY(100);
                $this->fpdf->setX(180);
                $this->fpdf->Cell(0, 5, utf8_decode('Carga Horária: ') . $historico[0]->carga_horaria . ' Horas', 0, 1);
                $this->fpdf->setX(180);
//                var_dump(utf8_decode($array_dados_certficado['data_inicio']));
//                die;
                $this->fpdf->Cell(0, 5, utf8_decode($array_dados_certficado['periodo']), 0, 1);
                $this->fpdf->setX(180);
                //$this->fpdf->Cell(0, 5, utf8_decode($array_dados_certficado['data_termino']), 0, 1);
                $this->fpdf->SetFont('Arial', '', 16);
                $this->fpdf->SetFont('Arial', '', 16);
                $this->fpdf->SetXY(10, 125);
                $texto = utf8_decode($base_legal[0]->texto);
                $this->fpdf->MultiCell(0, 5, $texto, 0, 'C');
                $this->fpdf->SetFont('Arial', 'B', 16);
                $this->fpdf->Cell(430, 18, utf8_decode('Belém, ') . date("d/m/Y"), 0, 1, 'C');
                $this->fpdf->SetFont('Arial', '', 16);
                
                // Página de Verso	

                $this->fpdf->AddPage();
                $this->fpdf->Image('imgs/fundo_certificado_imazon_costa.jpg', 0, 0, 300, 'JPG'); //logo
                $this->fpdf->SetFont('Arial', 'B', 8);
                $this->fpdf->SetXY(20, 40);
                $this->fpdf->Cell(0, 20, utf8_decode('Conteúdo'), 0, 1, 'L');
                $this->fpdf->SetXY(20, 60);

                $this->fpdf->MultiCell(0, 5, utf8_decode(strip_tags($historico[0]->descricao)));
                $this->fpdf->SetXY(20, 90);
                //$this->fpdf->Image('imgs/abed.jpg', 236, 36, 30);
                $this->fpdf->SetXY(200, 183);
                $this->fpdf->Cell(0, 2, 'Autenticar: ' . base_url('autenticacao'), 0, 0);
                $this->fpdf->SetXY(200, 187);
               
                $this->fpdf->Cell(0, 2, utf8_decode('Códigos de Autenticação: ') . $historico[0]->codigo_autenticacao, 0, 0);

                $this->fpdf->Output();
            } else {
                redirect('autenticacao');
            }
        }



        exit;
    }

    public function certificado_participacao() {

        $h = decodifica($this->uri->segment(2));

        $this->load->model("historico_model", "hm");

        $historico = $this->hm->get_autenticacao_declaracao($h)->result();

        if (count($historico) === 1) {

            $texto = utf8_decode(str_replace("<br />", "", nl2br(strip_tags($historico[0]->descricao))));

//            $params = array('orientation' => 'L', 'unit' => 'mm', 'size' => 'A4');
//
//            $this->load->library('fpdf', $params);

            $this->fpdf->AliasNbPages();



            $this->fpdf->AddPage();

            $this->fpdf->Image('imgs/fundo_certificado_imazon.jpg', 1, 1, 270); //logo

            $this->fpdf->SetFont('Arial', 'B', 9);

            $this->fpdf->setY("40");

            $this->fpdf->setX("10");

            $this->fpdf->SetFont('Arial', 'B', 40);

            $this->fpdf->Cell(0, 13, utf8_decode('Certificado de Participação'), 0, 1, 'C');

            $this->fpdf->SetFont('Arial', '', 16);

            $this->fpdf->Cell(0, 10, 'conferido a', 0, 1, 'C');

            $this->fpdf->SetFont('Arial', 'B', 16);

            $this->fpdf->Cell(0, 10, utf8_decode($historico[0]->nomeAluno), 0, 1, 'C');

            $this->fpdf->SetFont('Arial', 'B', 16);

            $this->fpdf->Cell(0, 10, 'RG: ' . utf8_decode($historico[0]->rg), 0, 1, 'C');



            $this->fpdf->SetFont('Arial', '', 16);



            $this->fpdf->Cell(0, 5, 'Por participar do Curso Livre ', 0, 1, 'C');

            $this->fpdf->SetFont('Arial', 'B', 16);

            $this->fpdf->MultiCell(0, 10, utf8_decode($historico[0]->nomeCurso), 0, 'C');

            $this->fpdf->SetFont('Arial', '', 16);

            $this->fpdf->Cell(0, 10, utf8_decode('Tipo: Capacitação/Atualização'), 0, 1, 'C');

            $this->fpdf->Cell(0, 10, utf8_decode('Carga Horária: ') . $historico[0]->carga_horaria . ' Horas', 0, 1, 'C');

            $this->fpdf->Cell(0, 10, utf8_decode('Data de início: ') . date('d/m/Y', strtotime($historico[0]->primeiro_dia)), 0, 1, 'C');

            $this->fpdf->Cell(0, 10, utf8_decode('Matrícula: ') . $historico[0]->id_historico, 0, 1, 'C');

            $this->fpdf->SetFont('Arial', '', 16);

            $this->fpdf->SetXY(10, 125);

            $this->fpdf->SetFont('Arial', 'B', 16);

            $this->fpdf->SetXY(10, 138);

            $this->fpdf->Cell(430, 18, utf8_decode('Belém, ') . date("d/m/Y"), 0, 1, 'C');

            $this->fpdf->SetXY(10, 125);

            $this->fpdf->SetFont('Arial', '', 16);

            $this->fpdf->SetLineWidth(0.2);

            $this->fpdf->Line(30, 170, 125, 170);

            $this->fpdf->SetXY(50, 175);

            $this->fpdf->Cell(0, 1, 'Assinatura do/a Aluno/a', 0, 0);

            $this->fpdf->SetXY(20, 187);



            $this->fpdf->Image('imgs/rubrica.jpg', 200, 150, 60);

            $this->fpdf->Line(180, 170, 265, 170);

            $this->fpdf->SetXY(200, 175);

            $this->fpdf->Cell(0, 1, 'Ezelildo G Dornelas', 0, 0);

            $this->fpdf->SetXY(210, 180);

            $this->fpdf->Cell(0, 1, 'Diretor', 0, 0);



            // Página de Verso	



            $this->fpdf->AddPage();

            $this->fpdf->Image('imgs/fundo_certificado_imazon.jpg', 1, 1, 270, 'JPG'); //logo

            $this->fpdf->SetFont('Arial', 'B', 8);

            $this->fpdf->SetXY(20, 40);

            $this->fpdf->Cell(0, 20, utf8_decode('Conteúdo'), 0, 1, 'L');

            $this->fpdf->SetXY(20, 60);

            $this->fpdf->MultiCell(0, 5, strip_tags($texto));

            $this->fpdf->SetXY(20, 90);

            $this->fpdf->Image('imgs/abed.jpg', 235, 45, 30);

            $this->fpdf->SetXY(200, 183);

            $this->fpdf->SetFont('Arial', 'B', 10);

            $this->fpdf->Cell(0, 2, 'Autenticar: www.imazoncursos.com.br/autenticacao/', 0, 0);

            $this->fpdf->SetXY(200, 188);

            $this->fpdf->Cell(0, 1, utf8_decode('Código de Autenticação: ') . $historico[0]->codigo_autenticacao, 0, 0);



            $this->fpdf->Output();
        }
    }

    public function gera_autenticacao_certificado_vip_imp($certificado = null) {

        if ($certificado !== null) {

            $aut = $this->hm->gera_autenticacao_certificado_imp_vip(decodifica($certificado))->result();

            $usr = $this->um->get_by_id_fic($aut[0]->email)->result();

            if (count($aut) == 1) {

                $base_legal = $this->bm->get_atual()->result();



//                $params = array('orientation' => 'P', 'unit' => 'mm', 'size' => 'A4');
//
//                $this->load->library('fpdf', $params);



                // Página de Verso	

                $this->fpdf->AliasNbPages();

                //Instanciation of inherited class

                $this->fpdf->AddPage();

                $this->fpdf->SetMargins(19, 18);

                $this->fpdf->Image('imgs/logo.png', 19, 21); //logo

                $this->fpdf->SetFont('Arial', 'B', 9);

                $this->fpdf->Ln(1); //quebra de linha

                $this->fpdf->setX(220);

                $this->fpdf->setY("20");

                $this->fpdf->setX("10");

                $this->fpdf->SetFont('Arial', 'B', 14);

                $this->fpdf->Cell(0, 13, 'CIDADE APRENDIZAGEM', 0, 1, 'R');

                $this->fpdf->setY("28");

                $this->fpdf->setX("10");

                $this->fpdf->SetFont('Arial', '', 8);

                $this->fpdf->Cell(0, 13, utf8_decode('Travessa Quatorze de Março, 221'), 0, 1, 'R');

                $this->fpdf->setY("31");

                $this->fpdf->setX("10");

                $this->fpdf->SetFont('Arial', '', 8);

                $this->fpdf->Cell(0, 13, utf8_decode('Umarizal - Belém/PA - CEP: 66055-490'), 0, 1, 'R');

                $this->fpdf->setY("34");

                $this->fpdf->setX("10");

                $this->fpdf->SetFont('Arial', '', 8);

                $this->fpdf->Cell(0, 13, 'CNPJ: 10.910.194/0001-16', 0, 1, 'R');

                $this->fpdf->setY("70");

                $this->fpdf->setX("10");

                $this->fpdf->setX(220);

                $this->fpdf->setY("50");

                $this->fpdf->setX("10");

                $this->fpdf->SetFont('Arial', 'B', 11);

                $this->fpdf->Cell(0, 3, utf8_decode('Autenticação de Certificado'), 0, 1, 'C');



                $this->fpdf->setY(60);

                $this->fpdf->SetFont('Arial', '', 10);



                $this->fpdf->setX(20);

                $this->fpdf->Cell(0, 5, 'Nome do Aluno: ' . utf8_decode($usr[0]->nome), 0, 1);

                $this->fpdf->setX(20);

                $this->fpdf->Cell(0, 5, 'Nome do Curso: ' . utf8_decode($aut[0]->nome), 0, 1);

                $this->fpdf->setX(20);

                $this->fpdf->Cell(0, 5, utf8_decode('Tipo: Capacitação/Atualização'), 0, 1);



                $this->fpdf->setX(20);

                $this->fpdf->Cell(0, 5, 'Aproveitamento: 10,0', 0, 1);

                $this->fpdf->setX(20);

                $this->fpdf->Cell(0, 5, utf8_decode('Cert. Nº ' . $aut[0]->id), 0, 1);



                $this->fpdf->setX(20);

                $this->fpdf->Cell(0, 5, utf8_decode('Carga Horária: ' . $aut[0]->UCMCargaHorariaCertificado . ' Horas'), 0, 1);



                if ($aut[0]->UCMData != '0000-00-00') {

                    $this->fpdf->setX(20);

                    $this->fpdf->Cell(0, 5, 'Data Matricula: ' . date("d/m/Y", strtotime($aut[0]->UCMData)), 0, 1);
                } else {

                    $this->fpdf->setX(20);

                    $this->fpdf->Cell(0, 5, 'Data Matricula: ' . date("d/m/Y", strtotime($aut[0]->UCMData)), 0, 1);
                }



                $this->fpdf->setX(20);

                $this->fpdf->Cell(0, 5, utf8_decode('Data Conclusão: ' . date("d/m/Y", strtotime($aut[0]->ultimo_dia))), 0, 1);

                $this->fpdf->setX(20);

                $this->fpdf->Cell(0, 5, utf8_decode('Código da Autenticação: ' . $aut[0]->UCMCertificadoLiberado), 0, 1);

                $this->fpdf->setX(20);

                $this->fpdf->Cell(0, 5, 'Autenticado em: www.imazoncursos.com.br/autenticacao/ ', 0, 1);

                $this->fpdf->setX(20);

                $this->fpdf->Cell(0, 5, utf8_decode('Data da Autenticação: ' . date("d/m/Y")), 0, 1);





                $this->fpdf->SetFont('Arial', 'B', 10);

                $this->fpdf->setX(20);

                $this->fpdf->Cell(0, 10, utf8_decode('Programa de Formação'), 0, 1);

                $this->fpdf->SetFont('Arial', '', 10);

                $this->fpdf->setX(20);

                $this->fpdf->Cell(0, 5, 'Imazon', 0, 1);

                $this->fpdf->setX(20);

                $this->fpdf->Cell(0, 5, utf8_decode('Tipo: Formação Continuada de Trabalhadores.'), 0, 1);

                $this->fpdf->setX(20);

                $this->fpdf->Cell(0, 5, utf8_decode($base_legal[0]->texto), 0, 1);

                $this->fpdf->SetFont('Arial', 'B', 10);

                $this->fpdf->setX(20);

                $this->fpdf->Cell(0, 10, utf8_decode('Instituição de Ensino'), 0, 1);



                $this->fpdf->SetFont('Arial', '', 10);

                $this->fpdf->setX(20);

                $this->fpdf->Cell(0, 5, 'Cidade Aprendizagem', 0, 1);

                $this->fpdf->setX(20);

                $this->fpdf->Cell(0, 5, 'CNPJ: 10.910.194/0001-16', 0, 1);

                $this->fpdf->setX(20);

                $this->fpdf->Cell(0, 5, utf8_decode('Travessa Quatorze de Março, 221, Umarizal, Belém, Pará, Cep 66055-490'), 0, 1);

                $this->fpdf->setX(20);

                $this->fpdf->Cell(0, 5, utf8_decode('Diretor: Ezelildo Gonçalves Dornelas'), 0, 0, 1);





                // Página de Verso	

                $this->fpdf->AddPage();

                $this->fpdf->SetFont('Arial', 'B', 8);

                $this->fpdf->SetXY(20, 10);

                $this->fpdf->Cell(0, 15, utf8_decode('Conteúdo:'), 0, 1, 'L');

                $this->fpdf->SetXY(20, 20);



                if (strlen(trim($aut[0]->descricao)) == 0) {

                    $this->load->model("cursos_model", "cm");

                    $i = 1;

                    foreach ($this->cm->get_aulas_vip($aut[0]->cedoc_doc_id_fk) as $aulas) {

                        $this->fpdf->MultiCell(0, 3, utf8_decode(strip_tags($i . '. ' . $aulas->CATitulo)));

                        $i++;
                    }
                } else {

                    $this->fpdf->MultiCell(0, 5, utf8_decode(strip_tags($aut[0]->descricao)));
                }



                $this->fpdf->SetXY(20, 60);

                $this->fpdf->Output();
            }
        } else {

            redirect("autenticacao");
        }
    }

    public function gera_autenticacao_certificado_vip($historico = null) {

        if ($historico !== null) {

            $aut = $this->hm->gera_autenticacao_certificado_vip(decodifica($historico))->result();

            $usr = $this->um->get_by_id_fic($aut[0]->email)->result();



            if (count($aut) == 1) {

                $base_legal = $this->bm->get_atual()->result();



//                $params = array('orientation' => 'P', 'unit' => 'mm', 'size' => 'A4');

                //$this->load->library('fpdf', $params);



                // Página de Verso	

                $this->fpdf->AliasNbPages();

                //Instanciation of inherited class

                $this->fpdf->AddPage();

                $this->fpdf->SetMargins(19, 18);

                $this->fpdf->Image('imgs/logo.png', 19, 21); //logo

                $this->fpdf->SetFont('Arial', 'B', 9);

                $this->fpdf->Ln(1); //quebra de linha

                $this->fpdf->setX(220);

                $this->fpdf->setY("20");

                $this->fpdf->setX("10");

                $this->fpdf->SetFont('Arial', 'B', 14);

                $this->fpdf->Cell(0, 13, 'CIDADE APRENDIZAGEM', 0, 1, 'R');

                $this->fpdf->setY("28");

                $this->fpdf->setX("10");

                $this->fpdf->SetFont('Arial', '', 8);

                $this->fpdf->Cell(0, 13, utf8_decode('Travessa Quatorze de Março, 221'), 0, 1, 'R');

                $this->fpdf->setY("31");

                $this->fpdf->setX("10");

                $this->fpdf->SetFont('Arial', '', 8);

                $this->fpdf->Cell(0, 13, utf8_decode('Umarizal - Belém/PA - CEP: 66055-490'), 0, 1, 'R');

                $this->fpdf->setY("34");

                $this->fpdf->setX("10");

                $this->fpdf->SetFont('Arial', '', 8);

                $this->fpdf->Cell(0, 13, 'CNPJ: 10.910.194/0001-16', 0, 1, 'R');

                $this->fpdf->setY("70");

                $this->fpdf->setX("10");

                $this->fpdf->setX(220);

                $this->fpdf->setY("50");

                $this->fpdf->setX("10");

                $this->fpdf->SetFont('Arial', 'B', 11);

                $this->fpdf->Cell(0, 3, utf8_decode('Autenticação de Certificado'), 0, 1, 'C');



                $this->fpdf->setY(60);

                $this->fpdf->SetFont('Arial', '', 10);



                $this->fpdf->setX(20);

                $this->fpdf->Cell(0, 5, 'Nome do Aluno: ' . utf8_decode($usr[0]->nome), 0, 1);

                $this->fpdf->setX(20);

                $this->fpdf->Cell(0, 5, 'Nome do Curso: ' . utf8_decode($aut[0]->nome), 0, 1);

                $this->fpdf->setX(20);

                $this->fpdf->Cell(0, 5, utf8_decode('Tipo: Capacitação/Atualização'), 0, 1);



                $this->fpdf->setX(20);

                $this->fpdf->Cell(0, 5, 'Aproveitamento: 10,0', 0, 1);

                $this->fpdf->setX(20);

                $this->fpdf->Cell(0, 5, utf8_decode('Cert. Nº ' . $aut[0]->usuarios_cursos_matriculados_id), 0, 1);



                $this->fpdf->setX(20);

                $this->fpdf->Cell(0, 5, utf8_decode('Carga Horária: ' . $aut[0]->UCMCargaHorariaCertificado . ' Horas'), 0, 1);



                //$certificados_errados = array(4091,5769,5770,5644,5384,5758,5771,4655,5680,826,5380,5102,5220,4845,5106,5105,5120,5222,4772,4185);



                if ($aut[0]->UCMData != '0000-00-00') {

                    $this->fpdf->setX(20);

                    $this->fpdf->Cell(0, 5, 'Data Matricula: ' . date("d/m/Y", strtotime($aut[0]->UCMData)), 0, 1);
                } else {

                    $this->fpdf->setX(20);

                    $this->fpdf->Cell(0, 5, 'Data Matricula: ' . date("d/m/Y", strtotime($aut[0]->UCMData)), 0, 1);
                }



                $this->fpdf->setX(20);

                $this->fpdf->Cell(0, 5, utf8_decode('Data Conclusão: ' . date("d/m/Y", strtotime($aut[0]->ultimo_dia))), 0, 1);

                $this->fpdf->setX(20);

                $this->fpdf->Cell(0, 5, utf8_decode('Código da Autenticação: ' . $aut[0]->UCMCertificadoLiberado), 0, 1);

                $this->fpdf->setX(20);

                $this->fpdf->Cell(0, 5, 'Autenticado em: www.imazoncursos.com.br/autenticacao/ ', 0, 1);

                $this->fpdf->setX(20);

                $this->fpdf->Cell(0, 5, utf8_decode('Data da Autenticação: ' . date("d/m/Y")), 0, 1);





                $this->fpdf->SetFont('Arial', 'B', 10);

                $this->fpdf->setX(20);

                $this->fpdf->Cell(0, 10, utf8_decode('Programa de Formação'), 0, 1);

                $this->fpdf->SetFont('Arial', '', 10);

                $this->fpdf->setX(20);

                $this->fpdf->Cell(0, 5, 'Imazon', 0, 1);

                $this->fpdf->setX(20);

                $this->fpdf->Cell(0, 5, utf8_decode('Tipo: Formação Continuada de Trabalhadores.'), 0, 1);

                $this->fpdf->setX(20);

                $this->fpdf->Cell(0, 5, utf8_decode($base_legal[0]->texto), 0, 1);

                $this->fpdf->SetFont('Arial', 'B', 10);

                $this->fpdf->setX(20);

                $this->fpdf->Cell(0, 10, utf8_decode('Instituição de Ensino'), 0, 1);



                $this->fpdf->SetFont('Arial', '', 10);

                $this->fpdf->setX(20);

                $this->fpdf->Cell(0, 5, 'Cidade Aprendizagem', 0, 1);

                $this->fpdf->setX(20);

                $this->fpdf->Cell(0, 5, 'CNPJ: 10.910.194/0001-16', 0, 1);

                $this->fpdf->setX(20);

                $this->fpdf->Cell(0, 5, utf8_decode('Travessa Quatorze de Março, 221, Umarizal, Belém, Pará, Cep 66055-490'), 0, 1);

                $this->fpdf->setX(20);

                $this->fpdf->Cell(0, 5, utf8_decode('Diretor: Ezelildo Gonçalves Dornelas'), 0, 0, 1);





                // Página de Verso	

                $this->fpdf->AddPage();

                $this->fpdf->SetFont('Arial', 'B', 8);

                $this->fpdf->SetXY(20, 10);

                $this->fpdf->Cell(0, 15, utf8_decode('Conteúdo:'), 0, 1, 'L');

                $this->fpdf->SetXY(20, 20);



                if (strlen(trim($aut[0]->descricao)) == 0) {

                    $this->load->model("cursos_model", "cm");

                    $i = 1;

                    foreach ($this->cm->get_aulas_vip($aut[0]->cedoc_doc_id_fk) as $aulas) {

                        $this->fpdf->MultiCell(0, 3, utf8_decode(strip_tags($i . '. ' . $aulas->CATitulo)));

                        $i++;
                    }
                } else {

                    $this->fpdf->MultiCell(0, 5, utf8_decode(strip_tags($aut[0]->descricao)));
                }



                $this->fpdf->SetXY(20, 60);

                $this->fpdf->Output();
            }
        } else {

            redirect("autenticacao");
        }
    }

    public function cadastrar() {

        $this->load->library("form_validation");
        $this->form_validation->set_rules('email', 'email', 'valid_email|required|min_length[6]|trim|is_unique[aluno.id_fic]|is_unique[aluno.email]');

        if ($this->form_validation->run()) {

            $termo = $this->tem->get_termo_valido()->result();

            $usuario = array('email' => $this->input->post('email', true),
                'senha' => substr(md5(uniqid()), 0, 6),
                'data_cadastro' => date("Y-m-d h:i:s"),
                'id_fic' => md5(uniqid(rand(), true)),
                'codigo_alfa' => md5($this->input->post("email", true)),
                'situacao_aluno' => 1,
                'extra' => $termo[0]->id_termo,
                'origem' => 'Imazon Cursos');

            if ($this->um->add($usuario)) {
                $log_termo = array("id_aluno" => $usuario['id_fic'], "id_termo" => $termo[0]->id_termo, "ip" => getenv("REMOTE_ADDR"), "data_aceite" => date("Y-m-d h:i:s"));
                $this->tem->add_termo_aluno($log_termo);
                $d = $this->um->rec_senha_email($this->input->post('email', true))->result();
                if (count($d) === 1) {
                    $session = array(
                        'imazon_id_aluno' => $d[0]->id_aluno,
                        'imazon_id_fic' => $d[0]->id_fic,
                        'imazon_session_id' => md5(rand(date('m'), date('s'))),
                        'imazon_email_aluno' => $this->input->post("email", true),
                        'imazon_logado' => TRUE,
                        'rescad' => TRUE,
                        'imazon_nome' => strtoupper($this->input->post("nome", true))
                    );
                    $this->session->set_userdata($session);
                    $this->load->library('user_agent');
//                    echo '<pre>';
//                    var_dump($this->session->userdata['imazon_session_id']);
//                    die;
                    $this->um->add_auditoria(array('session_id' => $this->session->userdata['imazon_session_id'], 'id_aluno' => $d[0]->id_aluno, 'data_auditoria' => date("Y-m-d h:i:s"), 'ip' => getenv("REMOTE_ADDR"), 'navegador' => $this->agent->browser() . ' - ' . $this->agent->version(), 'sistema' => $this->agent->platform()));
                    //matriculado em algum curso caso o usuário o tenha solicitiado antes do cadastro
                    if ($this->input->post("matricular_curso")) {
                        $curso = explode(":", $this->input->post("matricular_curso"));
                        if (count($curso) == 2) {
                            $idcurso = decodifica($curso[0]);
                            $ch = decodifica($curso[1]);
                            $base_legal = $this->bm->get_atual()->result();
                            $hash = date('Y') . '-' . strtoupper(substr(md5($this->session->userdata('imazon_id_fic') . ':' . microtime() . '-' . microtime()), 0, 4) . '-' . substr(md5($this->session->userdata('imazon_id_fic') . ':' . microtime()), 0, 4));
                            $autenticacao = strtoupper(substr(md5($hash), 0, 8));
                            $autenticacao = date('Y') . '-' . substr($autenticacao, 0, 4) . '-' . substr($autenticacao, 4, 9);
                            $idHistoricNovo = $hash;
                            $historico = array('id_historico' => $idHistoricNovo, 'id_aluno' => $this->session->userdata('imazon_id_fic'),
                                'data_aprovacao' => '0000-00-00', 'hora_aprovacao' => '00:00:00', 'nota' => 0,
                                'email' => $this->session->userdata('imazon_email_aluno'), 'id_avaliacao' => 1, 'id_curso' => $idcurso,
                                'primeiro_dia' => date('Y-m-d'), 'primeira_hora' => date('h:i:s'), //'carga_horaria'=>$c['descricao'], 
                                'carga_horaria' => $ch,
                                'id_baselegal' => $base_legal[0]->id_base_lega, 'situacao' => 0, 'codigo_autenticacao' => md5(microtime()));
                        }
                    }

                    $this->load->helper("emailphpmailer");
                    $this->load->library('parser');

                    $dados = array("email" => $usuario['email'], "senha" => $usuario['senha']);
                    $msg = $this->parser->parse("site/tela/cadastro_sucesso", $dados);
                    $assunto = "Cadastro efetuado com sucesso";
                    $destinatario = $this->input->post("email", true);

                    envia_email($destinatario, $assunto, $msg);

                    if ($historico) {
                        if ($this->hm->add($historico)) {
                            redirect("ultimo_curso_matriculado");
                        }
                    }

                    if (strlen($this->input->post("fonte")) > 0) {
                        redirect($this->input->post("fonte"));
                    } else {
                        redirect("cursos");
                    }
                } else {

                    $data = array(
                        'titulo' => 'Cursos com Certificado',
                        'palavra_chave' => 'Cadastro Imazon Cursos'
                    );

                    $this->parser->parse('site/include/head_home', $data);
                    $this->load->view('site/tela/home');
                    $this->load->view('site/include/foot');
                }
            } else {

                $data = array(
                    'titulo' => 'Cursos com Certificado',
                    'palavra_chave' => 'Cadastro Imazon Cursos'
                );

                $this->parser->parse('site/include/head_home', $data);
                $this->load->view('site/tela/home');
                $this->load->view('site/include/foot');
            }
        } else {

            $data = array(
                'titulo' => 'Cursos com Certificado',
                'palavra_chave' => 'Cadastro Imazon Cursos'
            );
            $this->parser->parse('site/include/head_home', $data);
            $this->load->view('site/tela/home');
            $this->load->view('site/include/foot');
        }
    }

    public function recibo() {

        if (!$this->sm->valida_login_aluno()) {

            $d = array('id_transacao' => "098d9sas0ashd");

            $this->load->view('site/inc/head', $d);

            $this->load->view('site/tela/recibo');

            $this->load->view('site/inc/foot');
        }
    }

    public function recibo2($recibo) {
        $data = array(
            'titulo' => 'Recibo',
            'palavra_chave' => 'Imazon Cursos'
        );

        $token = $this->uri->segment(3);

        //$params['token_account'] = 'a44677dac7cba94'; 
		 $params['token_transaction'] = $token;  
        $params['token_account'] = '32e9c1b008b07a0';
       
        //$urlPost = "https://api.sandbox.traycheckout.com.br/v2/transactions/get_by_token";
        $urlPost = "https://api.traycheckout.com.br/v2/transactions/get_by_token";

        ob_start();
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $urlPost);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSLVERSION, 6);
        //curl_setopt ($ch, CURLOPT_SSLVERSION,'CURL_SSLVERSION_TLSv1_2');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_exec($ch);

        /* XML de retorno */
        $resposta = simplexml_load_string(ob_get_contents());
        ob_end_clean();
        curl_close($ch);



        if ($resposta->message_response->message == "success") {
            $codigo_pagamento = $resposta->data_response->transaction->payment->payment_method_id;
            $pedido = $resposta->data_response->transaction->order_number;
            $cod_status = $resposta->data_response->transaction->status_id;

            if ($cod_status == 4) {
                $status_banco = "Em An&aacute;lise";
            }
            if ($cod_status == 5) {
                $status_banco = "Iniciada";
            }
            if ($cod_status == 6) {
                $status_banco = "Aprovada";
            }
            if ($cod_status == 7) {
                $status_banco = "Cancelada";
            }
            if ($cod_status == 24) {
                $status_banco = "Em An&aacute;lise";
            }
            if ($cod_status == 87) {
                $status_banco = "Em An&aacute;lise";
            }
            if ($cod_status == 88) {
                $status_banco = "Em An&aacute;lise";
            }
            if ($cod_status == 89) {
                $status_banco = "Cancelada";
            }
        }

        $this->load->model('pedido_model', 'prm');
        //echo $token;

        $r = $this->prm->get_by_aluno_token($token)->result();
        $transacao = $r[0]->transacao;
        $d = array('status_banco' => $status_banco, 'transacao' => $transacao);
        $this->parser->parse('site/include/head', $data);

        $this->load->view('site/tela/transacaocartao_sucesso.php', $d);

        $this->load->view('site/include/foot');
    }

    public function recuperar_senha() {

 
        if ($this->input->post("email")) {
            
            $this->load->library("form_validation");

            $this->form_validation->set_rules('email', 'email', 'valid_email');

            if ($this->form_validation->run()) {



                $d = $this->um->rec_senha_email($this->input->post('email'))->result();



                if (count($d) >= 1) {

                    $assunto = "Recuperacao de Senha";

                    $msg = 
                            '<table cellpadding="0" cellspacing="0" border="0" width="100%" style="width:100%;background-color:#ffffff;width:100%" bgcolor="#ffffff">

	<tr>

		<td>

			<table cellpadding="0" cellspacing="0" border="0" width="600px" align="center" style="width:600px;font-family:Verdana, Helvetica, sans-serif;color:#333333;font-size:13px;line-height:150%">

				<tr>

					<td style="padding:10px;">

						<a href="https://imazoncursos.com.br/site/home"><img src="http://imgur.com/wGl2QmJ.jpg" border="0" /></a>

					</td>

					<td>

					<h1 style="white-space:nowrap;Margin-left:-330px;Margin-bottom: 1px;font-family: Montserrat,Verdana,serif;font-weight: normal;color: #8FCC16;font-size: 30px;line-height: 38px;">Imazon Cursos</h1>

					</td>

					<td>

					</td>

					

				</tr>

				<tr>

					<td colspan="2" style="border-top:5px #1f381a solid;border-bottom:5px #1f381a solid">&nbsp;

						

					</td>

				</tr>

				<tr>

					<td colspan="2" bgcolor="#8FCC16" style="padding-top:10px;color:#1f381a;font-weight:bold" align="center">

						<p style="padding-top: 20px;font-family: Montserrat,Verdana,serif;font-weight: normal;font-size: 20px;line-height: 1px;"><b>RECUPERA&Ccedil;&Atilde;O DE SENHA</b></p>

					</td>

				</tr>

				<tr>

					<td colspan="2" bgcolor="#8FCC16" style="padding:5px;padding-bottom:0px;color:#FFFFFF" align="center">

						<p class="size-17" style="Margin: 15px 10px 0px 25px;font-size: 17px;line-height: 26px;text-align: left;"><b>Olá, ' . $d[0]->nome . '</b></p>

						<p class="size-17" style="font-family: Montserrat,Verdana,serif;Margin: 5px 0px 0px 25px;font-size: 17px;line-height: 26px;text-align: left;"><span>Verificamos em nosso sistema que você solicitou a recupera&ccedil;&atilde;o de sua senha.</span></p>

					</td>

				</tr>

				<tr>

					<td colspan="2" bgcolor="#8FCC16" valign="bottom" height="35px" style="height:35px">

						<img src="http://www.emailtemplates.com.br/templates/evolution/2.png" style="display:block" border="0" />

					</td>

				</tr>

				<tr>

					<td bgcolor="#FFFFFF" style="padding:1px;padding-bottom:0px;font-weight:bold">

						

						<p class="size-17" style="font-family: Montserrat,Verdana,serif;Margin: 10px 0px 0px 25px;font-size: 17px;line-height: 26px;text-align: left;"><b>Senha:</b>' . $d[0]->senha . '</p>

						<p class="size-17" style="font-family: Montserrat,Verdana,serif;Margin: 10px 0px 0px 25px;font-size: 15px;line-height: 26px;text-align: left;">Acesse:<a href="www.imazoncursos.com.br" style="text-decoration:none;">www.imazoncursos.com.br</a></p>

						<span style="text-decoration: none;color:#638c13;font-size:14px;"></a></span></p>

						

					</td>

					<td bgcolor="#FFFFFF" style="padding:15px;padding-bottom:0px;font-weight:bold">&nbsp;

						

					</td>

				</tr>

				<tr>

					<td align="center" colspan="2" bgcolor="#fff">

					<br><br><br><br><a style="text-decoration: underline;transition: opacity 0.1s ease-in;color: #a3a4ad;" target="_blank" href="http://www.facebook.com/imazoncursos">

			<img style="border: 0;" src="https://i10.createsend1.com/static/eb/customise/13-the-blueprint-3/images/facebook.png" width="26" height="26"></a>

				

		<a style="text-decoration: underline;transition: opacity 0.1s ease-in;color: #a3a4ad;" target="_blank" href="http://www.twitter.com/imazoncursos">

			<img style="border: 0;" src="https://i1.createsend1.com/static/eb/customise/13-the-blueprint-3/images/twitter.png" width="26" height="26"></a>

						<br><br><span style="font-family: Montserrat,Verdana,serif;font-weight: normal;font-size: 14px;line-height: 1px;">Atenciosamente,<br>Equipe Imazon Cursos</span><br><br>

					</td>

				</tr>

				<tr>

					<td colspan="2" bgcolor="#8FCC16">

						<img src="http://www.emailtemplates.com.br/templates/evolution/7.png" style="display:block" />

					</td>

				</tr>

				<tr>

					<td colspan="2" bgcolor="#8FCC16" align="center" style="padding-left:20px;color:#FFFFFF">

						<p style="font-family: Montserrat,Verdana,serif;margin-top:-10px;">Copyright &#169; - Imazon Cursos 2007 / '.date('Y').'<br />Contato: atendimento@imazoncursos.com.br</p>

					</td>

					<td bgcolor="#8FCC16" style="color:#FFFFFF">

						

					</td>

				</tr>

				

			</table>

		</td>

	</tr>

</table>';



//                    $this->load->library("phpmailer");
                    require_once(APPPATH.'libraries/phpmailer/class.phpmailer.php');
                    require_once(APPPATH.'libraries/phpmailer/class.smtp.php');
                   

                    //Nova instância do PHPMailer  

                    $mail = new PHPMailer;

                    //Informa que será utilizado o SMTP para envio do e-mail  

                    $mail->IsSMTP();

                    //Informa que a conexão com o SMTP será autênticado  

                    $mail->SMTPAuth = true;

                    //Configura a segurança para SSL  

                    $mail->SMTPSecure = "ssl";

                    //Informa a porta de conexão do GAMIL  

                    $mail->Port = 465;

                    //Informa o HOST do GMAIL  

                    $mail->Host = "smtp.gmail.com";      // sets GMAIL as the SMTP server  
                    //Usuário para autênticação do SMTP  

                    $mail->Username = "recuperarsenha@imazon.com.br";

                    //Senha para autênticação do SMTP  

                    $mail->Password = "657@beta";

                    //Titulo do e-mail que será enviado  

                    $mail->Subject = $assunto;

                    //Preenchimento do campo FROM do e-mail  

                    $mail->Sender = $mail->Username;

                    $mail->From = $mail->Username;

                    $mail->FromName = "Imazon Cursos";

                    //E-mail para a qual o e-mail será enviado  

                    $mail->AddAddress($this->input->post('email'));

                    //Conteúdo do e-mail  

                    $mail->Body = $msg;

                    $mail->CharSet = 'utf8';

                    $mail->AltBody = $mail->Body;

                    //Dispara o e-mail  

                    $enviado = $mail->Send();

                    if ($enviado) {

                        echo "ok";
                    } else {

                        echo "email_invalido1";
                    }
                } else {

                    echo "email_invalido2";
                }
            } else {

                echo "email_invalido3";
            }
        } else {

            echo "email_invalido4";
        }
    }

    public function rec_senha() {



        if ($this->input->post("email")) {

            $this->load->library("form_validation");

            $this->form_validation->set_rules('email', 'email', 'valid_email');

            if ($this->form_validation->run()) {



                $d = $this->um->rec_senha_email($this->input->post('email'))->result();



                if (count($d) >= 1) {

                    $assunto = "Recuperacao de Senha";

                    $msg = '<table cellpadding="0" cellspacing="0" border="0" width="100%" style="width:100%;background-color:#ffffff;width:100%" bgcolor="#ffffff">

	<tr>

		<td>

			<table cellpadding="0" cellspacing="0" border="0" width="600px" align="center" style="width:600px;font-family:Verdana, Helvetica, sans-serif;color:#333333;font-size:13px;line-height:150%">

				<tr>

					<td style="padding:10px;">

						<a href="https://imazoncursos.com.br/site/home"><img src="http://imgur.com/wGl2QmJ.jpg" border="0" /></a>

					</td>

					<td>

					<h1 style="white-space:nowrap;Margin-left:-330px;Margin-bottom: 1px;font-family: Montserrat,Verdana,serif;font-weight: normal;color: #8FCC16;font-size: 30px;line-height: 38px;">Imazon Cursos</h1>

					</td>

					<td>

					</td>

					

				</tr>

				<tr>

					<td colspan="2" style="border-top:5px #1f381a solid;border-bottom:5px #1f381a solid">&nbsp;

						

					</td>

				</tr>

				<tr>

					<td colspan="2" bgcolor="#8FCC16" style="padding-top:10px;color:#1f381a;font-weight:bold" align="center">

						<p style="padding-top: 20px;font-family: Montserrat,Verdana,serif;font-weight: normal;font-size: 20px;line-height: 1px;"><b>RECUPERA&Ccedil;&Atilde;O DE SENHA</b></p>

					</td>

				</tr>

				<tr>

					<td colspan="2" bgcolor="#8FCC16" style="padding:5px;padding-bottom:0px;color:#FFFFFF" align="center">

						<p class="size-17" style="Margin: 15px 10px 0px 25px;font-size: 17px;line-height: 26px;text-align: left;"><b>Olá, ' . $d[0]->nome . '</b></p>

						<p class="size-17" style="font-family: Montserrat,Verdana,serif;Margin: 5px 0px 0px 25px;font-size: 17px;line-height: 26px;text-align: left;"><span>Verificamos em nosso sistema que você solicitou a recupera&ccedil;&atilde;o de sua senha.</span></p>

					</td>

				</tr>

				<tr>

					<td colspan="2" bgcolor="#8FCC16" valign="bottom" height="35px" style="height:35px">

						<img src="http://www.emailtemplates.com.br/templates/evolution/2.png" style="display:block" border="0" />

					</td>

				</tr>

				<tr>

					<td bgcolor="#FFFFFF" style="padding:1px;padding-bottom:0px;font-weight:bold">

						

						<p class="size-17" style="font-family: Montserrat,Verdana,serif;Margin: 10px 0px 0px 25px;font-size: 17px;line-height: 26px;text-align: left;"><b>Senha:</b>' . $d[0]->senha . '</p>

						<p class="size-17" style="font-family: Montserrat,Verdana,serif;Margin: 10px 0px 0px 25px;font-size: 15px;line-height: 26px;text-align: left;">Acesse:<a href="www.imazoncursos.com.br" style="text-decoration:none;">www.imazoncursos.com.br</a></p>

						<span style="text-decoration: none;color:#638c13;font-size:14px;"></a></span></p>

						

					</td>

					<td bgcolor="#FFFFFF" style="padding:15px;padding-bottom:0px;font-weight:bold">&nbsp;

						

					</td>

				</tr>

				<tr>

					<td align="center" colspan="2" bgcolor="#fff">

					<br><br><br><br><a style="text-decoration: underline;transition: opacity 0.1s ease-in;color: #a3a4ad;" target="_blank" href="http://www.facebook.com/imazoncursos">

			<img style="border: 0;" src="https://i10.createsend1.com/static/eb/customise/13-the-blueprint-3/images/facebook.png" width="26" height="26"></a>

				

		<a style="text-decoration: underline;transition: opacity 0.1s ease-in;color: #a3a4ad;" target="_blank" href="http://www.twitter.com/imazoncursos">

			<img style="border: 0;" src="https://i1.createsend1.com/static/eb/customise/13-the-blueprint-3/images/twitter.png" width="26" height="26"></a>

						<br><br><span style="font-family: Montserrat,Verdana,serif;font-weight: normal;font-size: 14px;line-height: 1px;">Atenciosamente,<br>Equipe Imazon Cursos</span><br><br>

					</td>

				</tr>

				<tr>

					<td colspan="2" bgcolor="#8FCC16">

						<img src="http://www.emailtemplates.com.br/templates/evolution/7.png" style="display:block" />

					</td>

				</tr>

				<tr>

					<td colspan="2" bgcolor="#8FCC16" align="center" style="padding-left:20px;color:#FFFFFF">

						<p style="font-family: Montserrat,Verdana,serif;margin-top:-10px;">Copyright &#169; - Imazon Cursos 2007 / '.date('Y').'<br />Contato: atendimento@imazoncursos.com.br</p>

					</td>

					<td bgcolor="#8FCC16" style="color:#FFFFFF">

						

					</td>

				</tr>

				

			</table>

		</td>

	</tr>

</table>';



                    /*$this->load->library("phpmailer");



                    $this->load->library("phpmailer");*/
					
					require_once(APPPATH.'libraries/phpmailer/class.phpmailer.php');
                    require_once(APPPATH.'libraries/phpmailer/class.smtp.php');

                    //Nova instância do PHPMailer  

                    $mail = new PHPMailer;

                    //Informa que será utilizado o SMTP para envio do e-mail  

                    $mail->IsSMTP();

                    //Informa que a conexão com o SMTP será autênticado  

                    $mail->SMTPAuth = true;

                    //Configura a segurança para SSL  

                    $mail->SMTPSecure = "ssl";

                    //Informa a porta de conexão do GAMIL  

                    $mail->Port = 465;

                    //Informa o HOST do GMAIL  

                    $mail->Host = "smtp.gmail.com";      // sets GMAIL as the SMTP server  
                    //Usuário para autênticação do SMTP  

                    $mail->Username = "recuperarsenha@imazon.com.br";

                    //Senha para autênticação do SMTP  

                    $mail->Password = "657@beta";

                    //Titulo do e-mail que será enviado  

                    $mail->Subject = $assunto;

                    //Preenchimento do campo FROM do e-mail  

                    $mail->Sender = $mail->Username;

                    $mail->From = $mail->Username;

                    $mail->FromName = "Imazon Cursos";

                    //E-mail para a qual o e-mail será enviado  

                    $mail->AddAddress($this->input->post('email'));

                    //Conteúdo do e-mail  

                    $mail->Body = $msg;

                    $mail->CharSet = 'utf8';

                    $mail->AltBody = $mail->Body;

                    //Dispara o e-mail  

                    $enviado = $mail->Send();

                    if ($enviado) {

                        $this->session->set_flashdata('success', 'Um email foi enviado para: ' . $this->input->post("email"));
                    } else {

                        $this->session->set_flashdata('warning', 'Não foi possível recuperar a sua senha.');
                    }
                } else {

                    $this->session->set_flashdata('warning', 'Email não encontrado.');
                }

                redirect("rec_senha");
            }
        } else {



            $data = array('titulo' => 'Recuperar Senha', 'palavra_chave' => 'Imazon Cursos');



            $this->parser->parse("site/include/head", $data);

            $this->load->view("site/tela/recuperar_senha");

            $this->load->view("site/include/foot");
        }
    }

    public function verifica_cadastro2() {

        if ($this->input->post("email")) {

            $this->load->library("form_validation");

            $this->form_validation->set_rules('email', 'email', 'valid_email');

            if ($this->form_validation->run()) {

                $email = $this->input->post("email");

                if ($this->um->_validar_email($email)) {//email já cadastrado na base de dados
                    echo "true";
                } else {

                    echo "false";
                }
            } else {

                echo "email inválido";
            }
        } else {

            echo "email inválido";
        }
    }

    public function verifica_cadastro() {
        
        if ($this->input->post("email", TRUE)) {
            $this->load->library("form_validation");
            $this->form_validation->set_rules('email', 'email', 'valid_email');

            if ($this->form_validation->run()) {
                $email = $this->input->post("email", TRUE);

                if ($this->um->validar_email($email)) {//email já cadastrado na base de dados
                    echo "true";
                } else {
                    echo "false";
                }
            } else {
                echo "email inválido";
            }
        } else {
            echo "email inválido";
        }
    }
    
    public function gerarPagamentoSaldo()
    {
        $id_fic = $this->input->post('id_fic');
        $valor = $this->input->post('valor');
        $id_transacao = $this->input->post('transacao');
        $cart = $this->input->post('cart');
        
        
        $this->load->model('pedido_model', 'prm');
        $transacao = $this->prm->create(array('transacao' => $id_transacao,
                    'id_aluno' => $id_fic,
                    'data_compra' => date('Y-m-d h:i:s'),
                    'tipo_pagamento' => 66,
                    'situacao' => 3,
                    'valor' => $valor
                ));
                
        if (!$transacao)
        {
            redirect(base_url('finalizarPedido'));
        }
        
        $this->load->model('base_legal_model', 'blm');
        $this->load->model('historico_model', 'hm');
        $this->load->model('certificado_model', 'certfm');
        
        $r = $this->prm->get_by_transacao($id_transacao)->result();
        $base_legal = $this->blm->get_atual()->result();
                
          if (count($r) == 1)
          {
              foreach ($cart as $c)
              {
                        $prod = $this->pm->get_by_nome_produto(strtolower(trim($c['tipo'])));
                        
                        if ($c['tipo'] == "CI")
                        {
                            //insere itens do carrinho na transação
                            $insereItemPedido = $this->prm->insert_item_pedido(array('transacao' => $id_transacao,
                                'id_historico' => $c['historico'],
                                'tipo_item' => $c['id_produto'],
                                'valor' => $c['price']
                                    )
                            );

                            if (!$insereItemPedido)
                            {
                                redirect('finalizarPedido');
                            }
                        }
                        else if ($c['tipo'] == "SC")
                        {
                            //insere itens do carrinho na transação
                            $insereItemPedido = $this->prm->insert_item_pedido(array('transacao' => $id_transacao,
                                'id_historico' => $c['historico'],
                                'tipo_item' => $c['id_produto'],
                                'valor' => $c['price']
                                    )
                            );

                            if (!$insereItemPedido) {

                                redirect('finalizarPedido');
                            }
                        }
                        else
                        {
                            //insere itens do carrinho na transação
                            $this->prm->insert_item_pedido(array('transacao' => $id_transacao,
                                'id_historico' => $c['historico'],
                                'tipo_item' => $c['id_produto'],
                                'valor' => $c['price']
                                    )
                            );
                        }
                    }
                    //Ação intermediador
                    $this->load->model('pedido_model', 'pedidom');
                    $cod_interme = $this->gera_cod_interme($id_transacao);
                    $dt = array('cod_interme' => $cod_interme);
                    $this->pedidom->atualiza_pedido($id_transacao, $dt);
          }       
    }
    private function gera_cod_interme($id_transacao)
    {
        $this->load->model("Cliente_model", "cl");
        
        $id_log_saldo_compra = $this->cl->get_log_saldo_compra($id_transacao);
        return 'saldo'.$id_log_saldo_compra;
    }
    
    public function gerarPagamento() {
        if (count($this->cart->contents()) > 0) {
            if ($this->seguranca_model->valida_login_aluno()) {
                $this->load->library("form_validation");
                if ($this->session->userdata("termo_pendente")) {
                    redirect("site/termo_novo");
                }

                //validação de dados
                if ((int) $this->input->post("Forma") == 6 || $this->input->post("Forma") == 7 || $this->input->post("Forma") == 14 || $this->input->post("Forma") == 21 || $this->input->post("Forma") == 22 || $this->input->post("Forma") == 23) {
                    $this->form_validation->set_rules("Identidade", "Identidade", "required|min_length[14]|max_length[14]");
                    //$this->form_validation->set_rules("sexo", "Sexo", "required|min_length[1]|max_length[1]");					
                    if (!$this->form_validation->run()) {
                        if (count($this->cart->contents()) > 0) {
                            $data['carrinho'] = $this->cart->contents();
                            $data = array(
                                'titulo' => 'Finalizar pedido',
                                'palavra_chave' => 'Imazon Cursos'
                            );

                            $this->load->library('table');
                            $this->parser->parse('site/include/head', $data);
                            $this->load->view('site/tela/finalizarPedido');
                            $this->load->view('site/include/foot');

                            exit;
                        }
                    }
                } else {
                    $this->form_validation->set_rules("Identidade", "Identidade", "required|min_length[14]|max_length[14]");
                    //$this->form_validation->set_rules("TipoCartao", "Tipo de cartao", "required");
                    $this->form_validation->set_rules("Forma", "Forma de pagamento", "required");
                    //$this->form_validation->set_rules("Parcelas", "Parcelas", "required");
                    $this->form_validation->set_rules("CartaoNumero", "Número do Cartao", "required|min_length[14]|max_length[22]");
                    $this->form_validation->set_rules("NomePortador", "Nome do Portador", "required");
                    $this->form_validation->set_rules("Expiracao", "Expiracao", "required");
                    $this->form_validation->set_rules("CodigoSeguranca", "Codigo de Seguranca", "required");
                    //$this->form_validation->set_rules("DataNascimento", "Data de Nascimento", "required");

                    if (!$this->form_validation->run()) {
                        if (count($this->cart->contents()) > 0) {
                            $data['carrinho'] = $this->cart->contents();
                            $data = array(
                                'titulo' => 'Finalizar pedido',
                                'palavra_chave' => 'Imazon Cursos'
                            );
                            $this->load->library('table');
                            $this->parser->parse('site/include/head', $data);
                            $this->load->view('site/tela/finalizarPedido');
                            $this->load->view('site/include/foot');
                            exit;
                        }
                    }
                }

                $endereco = '';
                $id_aluno = '';
                foreach ($this->um->get_endereco_aluno()->result() as $aluno) {
                    $endereco = $aluno->endereco . ', ' . $aluno->numero . ', ' . $aluno->complemento . ' ,' . $aluno->bairro . ' ,' . $aluno->cidade . ' ,' . $aluno->estado . ' ,' . $aluno->cep;
                    $id_aluno = $aluno->email;
                }
                $total = null; //valor da transação
                $id_transacao = "IMAC" . substr(md5(microtime()), 0, 10); //gera id da transacao
                
                $descricao = '';
                
                //calcula valor da transacao itens do carrinho
                foreach ($this->cart->contents() as $c) {
                    $total += $c['price']; //calcula valor total da transacao
                    $descricao .= $c['name'] . ', ';
                }

                $this->load->model('pedido_model', 'prm');
                //insere transacao no bd
                $transacao = $this->prm->create(array('transacao' => $id_transacao,
                    'id_aluno' => $this->session->userdata('imazon_id_fic'),
                    'data_compra' => date('Y-m-d h:i:s'),
                    'tipo_pagamento' => 0,
                    'situacao' => 0,
                    'valor' => $total
                ));
                
                if (!$transacao)
                {
                    redirect(base_url('finalizarPedido'));
                }

                $this->load->model('base_legal_model', 'blm');
                $this->load->model('historico_model', 'hm');
                $this->load->model('certificado_model', 'certfm');
                $r = $this->prm->get_by_transacao($id_transacao)->result();
                $base_legal = $this->blm->get_atual()->result();

                //verifica se transacao foi inserida com sucesso
                if (count($r) == 1) {
                    foreach ($this->cart->contents() as $c) {
                        //print_r($c);
                        //exit;
                        $prod = $this->pm->get_by_nome_produto(strtolower(trim($c['tipo'])));
                        if ($c['tipo'] == "CI") {
                            //insere itens do carrinho na transação
                            $insereItemPedido = $this->prm->insert_item_pedido(array('transacao' => $id_transacao,
                                'id_historico' => $c['historico'],
                                'tipo_item' => $c['id_produto'],
                                'valor' => $c['price']
                                    )
                            );

                            if (!$insereItemPedido) {
                                redirect('finalizarPedido');
                            }

                            //Insere Certificado
                            //******************************************
                            //******************************************
                            //$certificado = array('id_historico' => $c['historico'], 'codigo_programa' => '006', 'data_registro' => date('Y-m-d h:m:s'), 'situacao' => 0, 'transacao' => $id_transacao, 'id_tipo' => $c['id_produto'], 'endereco' => $endereco);
                            //$this->certfm->add($certificado);

                            /* if (!$this->certfm->add($certificado)) {

                              redirect('finalizarPedido');

                              } */
                        } else if ($c['tipo'] == "SC") {

                            //insere itens do carrinho na transação

                            $insereItemPedido = $this->prm->insert_item_pedido(array('transacao' => $id_transacao,
                                'id_historico' => $c['historico'],
                                'tipo_item' => $c['id_produto'],
                                'valor' => $c['price']
                                    )
                            );

                            if (!$insereItemPedido) {

                                redirect('finalizarPedido');
                            }
                        } else {
                            //insere itens do carrinho na transação
                            $this->prm->insert_item_pedido(array('transacao' => $id_transacao,
                                'id_historico' => $c['historico'],
                                'tipo_item' => $c['id_produto'],
                                'valor' => $c['price']
                                    )
                            );
                        }

                        //$total += $c['price']; //calcula valor total da transacao
                    }

                    //echo 'Ok transação: ' . $id_transacao;
                    //redirect('cursos_matriculados');
                    $aluno = $this->um->get_aluno()->result();
                    //print_r($aluno);
                    //echo"<br><br><br><br>";
                    //print_r($r);
                    //echo"<br><br><br><br>";					
                    //echo "aqui";
                    //exit();	

                    $Nome = utf8_decode($aluno[0]->nome);
                    $Email = utf8_decode($aluno[0]->email);
                    $valor = $total;
                    $idtransacao = $id_transacao;
                    $IdPagador = $aluno[0]->id_fic;
                    //$sexo = $this->input->post('sexo');
                    $cpf = $this->input->post('Identidade');
                    if(!is_numeric(str_replace(".", "", str_replace("-", "", $cpf)))){
                        $_SESSION['somente_num'] = 'Somente números são permitidos';
                        redirect('finalizarPedido');
                    }
                    //$nascimento = $this->input->post('DataNascimento');					
                    //echo $valor . "<br>" . $descricao . "<br>" . $idtransacao . "<br>" . $Email . "<br>" . $IdPagador . "<br>" . $Nome . "<br>" . $sexo . "<br>" . $cpf . "<br>" . $nascimento . "<br>";
                    // Pega informações de endereço
                    $Logradouro = utf8_decode($aluno[0]->endereco);
                    $NumeroCasa = $aluno[0]->numero;
                    $Complemento = utf8_decode($aluno[0]->complemento);
                    $Bairro = utf8_decode($aluno[0]->bairro);
                    $Cidade = utf8_decode($aluno[0]->cidade);
                    $Estado = utf8_decode($aluno[0]->estado);
                    $Cep = $aluno[0]->cep;
                    $TelefoneFixo = $aluno[0]->ddd_telefone . $aluno[0]->telefone;

                    //echo $Logradouro."<br>".$NumeroCasa."<br>".$Complemento."<br>".$Bairro."<br>".$Cidade."<br>".$Estado."<br>".$Cep."<br>".$TelefoneFixo."<br>";
                    $Forma = $this->input->post('Forma');
                    $Parcelas = $this->input->post('Parcelas');
                    $numerocartao = $this->input->post('CartaoNumero');
                    $Expiracao = $this->input->post('Expiracao');
                    $CodigoSeguranca = $this->input->post('CodigoSeguranca');
                    $NomePortador = $this->input->post('NomePortador');
                    //$DataNascimento = $this->input->post('DataNascimento');

                    if ($Expiracao == '') {
                        $Expiracao = "00/00";
                    }

                    $ext = explode("/", $Expiracao);
                    $mesvalidade = $ext[0];
                    $anovalidade = $ext[1];
                    echo "Aguarde, sua transa&ccedil;&atilde;o est&aacute; sendo processada...";

                    //echo $Forma."<br>".$Parcelas."<br>".$Expiracao."<br>".$CodigoSeguranca."<br>".$NomePortador."<br>".$DataNascimento."<br>".$mesvalidade."<br>".$anovalidade;					

                    /* $emailPagamentoLoja = "suporte@curso.me";
                      $consumerKey = "a51cd2e8a6a1fbc2886ea88d3381f5fdd4b7aead";
                      $time = time() * 1000;
                      $oauth_nonce = md5(microtime() . mt_rand());
                      $urlPost = "https://api.bcash.com.br/service/createTransaction/xml/";
                      $signature = array("oauth_consumer_key" => urlencode($consumerKey), "oauth_nonce" => urlencode($oauth_nonce),
                      "oauth_signature_method" => urlencode("PLAINTEXT"), "oauth_timestamp" => urlencode($time), "oauth_version" => urlencode("1.0"),);
                      $signature = base64_encode(http_build_query($signature, '', '&'));
                      $oAuth = array("Authorization: OAuth realm=" . $urlPost . ",oauth_consumer_key=" . $consumerKey . ",oauth_nonce=" . $oauth_nonce . ",oauth_signature=" . $signature . ",oauth_signature_method=PLAINTEXT" . ",oauth_timestamp=" . $time . ",oauth_version=1.0"
                      );

                      $data = '{"products":[{"code":"' . $idtransacao . '","description":"' . utf8_decode($descricao) . '","amount":"1","value":"' . $valor . '","extraDescription":""}],"dependentTransactions":[{"email":"","value":""}],"buyer":{"address":{"address":"' . $Logradouro . '","number":"' . $NumeroCasa . '","complement":"' . $Complemento . '","neighborhood":"' . $Bairro . '","city":"' . $Cidade . '","state":"' . $Estado . '","zipCode":"' . $Cep . '"},"mail":"' . $Email . '","name":"' . $Nome . '","cpf":"' . $cpf . '","phone":"' . $TelefoneFixo . '","cellPhone":"' . $TelefoneFixo . '","gender":"' . $sexo . '","birthDate":"' . $DataNascimento . '","searchToken":""},"paymentMethod":{"code":"' . $Forma . '"},

                      "creditCard":{"holder":"' . $NomePortador . '","number":"' . $numerocartao . '","securityCode":"' . $CodigoSeguranca . '","maturityMonth":"' . $mesvalidade . '","maturityYear":"' . $anovalidade . '"},

                      "sellerMail":"' . $emailPagamentoLoja . '","orderId":"' . $idtransacao . '","free":"","freight":"","freightType":"","discount":"","addition":"","urlReturn":"https://www.imazoncursos.com.br/moip","urlNotification":"https://www.imazoncursos.com.br/moip","installments":"' . $Parcelas . '","campaignId":"","currency":"BRL","acceptedContract":"S","viewedContract":"S"}';
                      $version = "1.0";
                      $encode = "UTF-8";
                      $params = array("data" => $data, "version" => $version, "encode" => $encode);
                      ob_start();
                      $ch = curl_init();
                      curl_setopt($ch, CURLOPT_URL, $urlPost);
                      curl_setopt($ch, CURLOPT_POST, 1);
                      curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params, '', '&'));
                      curl_setopt($ch, CURLOPT_HTTPHEADER, $oAuth);
                      curl_exec($ch);

                      /* XML ou Json de retorno */
                    /* $json_str = ob_get_contents();
                      $resposta = ob_get_contents();
                      ob_end_clean();
                      /* Capturando o httpcode para tratamento dos erros na requisição */
                    /* $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                      curl_close($ch);
                      if ($httpCode != "200") {
                      $result = @simplexml_load_string($resposta);
                      $merror = $result->error->description;
                      print utf8_decode("<script>alert('Transação Recusada. $merror!')</script>");
                      print utf8_decode("<script>window.location.href = '../site/finalizarPedido';</script>");
                      } else {
                      $this->cart->destroy();
                      $result = @simplexml_load_string($resposta);
                      $transa = $result->{'transactionId'};
                      $link = $result->{'paymentLink'};
                      $mensagem = $result->{'message'};
                      $status = $result->{'status'};
                      $descstatus = $result->{'descriptionStatus'};

                      if ($link != '') {
                      $this->cart->destroy();
                      if ($Forma == 10 || $Forma == 58 || $Forma == 59 || $Forma == 60) {
                      print utf8_decode("<script> window.location.href = '$link' ;</script>");
                      }
                      }

                      if (($Forma != 10) && ($Forma != 58) && ($Forma != 59) && ($Forma != 60) && ($descstatus != 'Em andamento')) {
                      print utf8_decode("<script>alert('Transação Recusada. $mensagem !')</script>");

                      print utf8_decode("<script>window.location.href = '../site/finalizarPedido';</script>");
                      }

                      if (($Forma != 10) && ($Forma != 58) && ($Forma != 59) && ($Forma != 60) && ($descstatus == 'Em andamento')) {

                      redirect('site/recibo2/' . $idtransacao);
                      }
                      exit;
                      } */

                    
                    $params['token_account'] = 'a44677dac7cba94';        /* Dados do Comprador sandbox */
                    //$params['token_account'] = '32e9c1b008b07a0';        /* Dados do Comprador  produacao */

                    //$urlPost = "https://api.sandbox.traycheckout.com.br/v2/transactions/pay_complete";//teste
                    $urlPost = 'https://www.aporfia.com.br/ovumpay/bcash';
                    //prdução = $urlPost = "https://api.traycheckout.com.br/v2/transactions/pay_complete";

                    $params['customer[name]'] = $Nome;
                    $params['customer[cpf]'] = $cpf;
                    $params['customer[email]'] = $Email;
                    $params['customer[contacts][][type_contact]'] = 'M';
                    $params['customer[contacts][][number_contact]'] = $TelefoneFixo;

                    $params['customer[addresses][][type_address]'] = 'B';
                    $params['customer[addresses][][postal_code]'] = $Cep;
                    $params['customer[addresses][][street]'] = $Logradouro;
                    $params['customer[addresses][][number]'] = $NumeroCasa;
                    $params['customer[addresses][][completion]'] = 'A';
                    $params['customer[addresses][][neighborhood]'] = $Bairro;
                    $params['customer[addresses][][city]'] = $Cidade;
                    $params['customer[addresses][][state]'] = $Estado;
                    /* Dados da Transação */
                    $params['transaction[available_payment_methods]'] = '2,3,4,5,6,7,14,15,16,18,19,21,22,23';
                    $params['transaction[order_number]'] = $idtransacao;
                    $params['transaction[customer_ip]'] = $_SERVER["REMOTE_ADDR"];
                    $params['transaction[url_notification]'] = base_url().'/retornotray';

                    /* Dados do Produto 1 - Notebook Preto */
                    $params['transaction_product[][description]'] = $descricao;
                    $params['transaction_product[][quantity]'] = '1';
                    $params['transaction_product[][price_unit]'] = $valor;
                    $params['transaction_product[][code]'] = '1';
                    // $params['transaction_product[][sku_code]'] = '0001';
                    $params['transaction_product[][extra]'] = '';

                    /* Dados para Pagamento */
                    $params['payment[payment_method_id]'] = $Forma;
                    $params['payment[split]'] = '01'; //quantidade de parcelas
                    $params['payment[card_name]'] = $NomePortador; //Nome Impresso no Cartão
                    $params['payment[card_number]'] = $numerocartao; //Numero Impresso no Cartão
                    $params['payment[card_expdate_month]'] = $mesvalidade; //Mês validade
                    $params['payment[card_expdate_year]'] = $anovalidade + 2000; //Ano Validade
                    $params['payment[card_cvv]'] = $CodigoSeguranca; //Código de Segurança Impresso no Cartão      

                    ob_start();
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $urlPost);
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_SSLVERSION, 6);
                    //curl_setopt ($ch, CURLOPT_SSLVERSION,'CURL_SSLVERSION_TLSv1_2');
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
                    curl_exec($ch);

                    /* XML de retorno */
                    //$resposta = simplexml_load_string(ob_get_contents());
                    $out = ob_get_contents();
                    ob_end_clean();
                    curl_close($ch);
                    
                    redirect($out);
                    
                    //Header("Location:".$out);
                    //echo $out;
                    die;
                    
                    if ($resposta->message_response->message == "success") {
                        $link = $resposta->data_response->transaction->payment->url_payment;
                        $token = $resposta->data_response->transaction->token_transaction;
                        $lk = base_url() . "recibo/" . $token;
                        $this->load->model('pedido_model', 'pedidom');
                        $dt = array('cod_interme' => (string) $token);
                        $this->pedidom->atualiza_pedido($idtransacao, $dt);
                        $this->cart->destroy();

                        if ($link != '') {
                            if (($Forma == 6) || ($Forma == 7) || ($Forma == 14) || ($Forma == 21) || ($Forma == 22) || ($Forma == 23)) {
                                redirect($link);
                            }
                        }
                        if (($Forma != 6) && ($Forma != 7) && ($Forma != 14) && ($Forma != 21) && ($Forma != 22) && ($Forma != 23)) {
                            redirect('site/recibo2/' . $token);
                        }
                    } else {
                        $erro = $resposta->error_response->validation_errors->validation_error->message_complete;
                        print "<script>alert('Transação Recusada! ($erro) ')</script>";
                        //print utf8_decode("<script>window.location.href = 'finalizarPedido';</script>");
                    }
                }
            } else {
                redirect(base_url('finalizarPedido'));
            }
        } else {
            redirect(base_url('finalizarPedido#logar'));
        }
    }

    public function gerar_segunda_via() {

        $data = array(
                        'titulo' => '2ª Via',
                        'palavra_chave' => 'Imazon Cursos'
                    );
        
        if ($this->uri->segment(2) !== null && $this->uri->segment(2) !== "") {

            $this->load->model('pedido_model', 'prm');

            $id_transacao = $this->uri->segment(2); //gera id da transacao
            $r = $this->prm->get_by_transacao($id_transacao)->result();
            
            $descricao = "";  
            
            if (count($r) == 0)
                redirect("segunda_via");
            
            if((int)$r[0]->situacao == 3) redirect("segunda_via");
            
            //if($r[0]->situacao != "3") redirect("segunda_via");
            
            $data["itens_pedido"] = $this->prm->get_historico_itens_transacao($r[0]->transacao)->result();
                    
            if ($this->input->post()) {

                $aluno = $this->um->get_by_id_fic($r[0]->id_aluno)->result();

                $Nome = utf8_decode($aluno[0]->nome);
                $Email = utf8_decode($aluno[0]->email);
                $idtransacao = $id_transacao;
                $IdPagador = $aluno[0]->id_fic;
                $cpf = $this->input->post('Identidade');

                $Logradouro = utf8_decode($aluno[0]->endereco);
                $NumeroCasa = $aluno[0]->numero;
                $Complemento = utf8_decode($aluno[0]->complemento);
                $Bairro = utf8_decode($aluno[0]->bairro);
                $Cidade = utf8_decode($aluno[0]->cidade);
                $Estado = utf8_decode($aluno[0]->estado);
                $Cep = $aluno[0]->cep;
                $TelefoneFixo = $aluno[0]->ddd_telefone . $aluno[0]->telefone;

                $Forma = $this->input->post('Forma');
                $Parcelas = $this->input->post('Parcelas');
                $numerocartao = $this->input->post('CartaoNumero');
                $Expiracao = $this->input->post('Expiracao');
                $CodigoSeguranca = $this->input->post('CodigoSeguranca');
                $NomePortador = $this->input->post('NomePortador');
             
                if ($Expiracao == '') {
                    $Expiracao = "00/00";
                }
             
                foreach($data["itens_pedido"] as $s){
                    $descricao .= "Matrícula no curso " . $s->nome . "; ";
                }

                $ext = explode("/", $Expiracao);
                $mesvalidade = $ext[0];
                $anovalidade = $ext[1];
                echo "Aguarde, sua transa&ccedil;&atilde;o est&aacute; sendo processada...";

                $params['token_account'] = '32e9c1b008b07a0';        /* Dados do Comprador  produacao */

                //$params['token_account'] = 'a44677dac7cba94';        /* Dados do Comprador sandbox */
                //$params['token_account'] = '32e9c1b008b07a0';        /* Dados do Comprador  produacao */

                //$urlPost = "https://api.sandbox.traycheckout.com.br/v2/transactions/pay_complete";
                $urlPost = "https://api.traycheckout.com.br/v2/transactions/pay_complete";

                $params['customer[name]'] = $Nome;
                $params['customer[cpf]'] = $cpf;
                $params['customer[email]'] = $Email;
                $params['customer[contacts][][type_contact]'] = 'M';
                $params['customer[contacts][][number_contact]'] = $TelefoneFixo;

                $params['customer[addresses][][type_address]'] = 'B';
                $params['customer[addresses][][postal_code]'] = $Cep;
                $params['customer[addresses][][street]'] = $Logradouro;
                $params['customer[addresses][][number]'] = $NumeroCasa;
                $params['customer[addresses][][completion]'] = 'A';
                $params['customer[addresses][][neighborhood]'] = $Bairro;
                $params['customer[addresses][][city]'] = $Cidade;
                $params['customer[addresses][][state]'] = $Estado;
                /* Dados da Transação */
                $params['transaction[available_payment_methods]'] = '2,3,4,5,6,7,14,15,16,18,19,21,22,23';
                $params['transaction[order_number]'] = $idtransacao;
                $params['transaction[customer_ip]'] = $_SERVER["REMOTE_ADDR"];
                $params['transaction[url_notification]'] = 'https://www.imazoncursos.com.br/retornotray';

                /* Dados do Produto 1 - Notebook Preto */
                $params['transaction_product[][description]'] = $descricao;
                $params['transaction_product[][quantity]'] = '1';
                $params['transaction_product[][price_unit]'] = $r[0]->valor;
                $params['transaction_product[][code]'] = '1';
                // $params['transaction_product[][sku_code]'] = '0001';
                $params['transaction_product[][extra]'] = '';

                /* Dados para Pagamento */
                $params['payment[payment_method_id]'] = $Forma;
                $params['payment[split]'] = '01'; //quantidade de parcelas
                $params['payment[card_name]'] = $NomePortador; //Nome Impresso no Cartão
                $params['payment[card_number]'] = $numerocartao; //Numero Impresso no Cartão
                $params['payment[card_expdate_month]'] = $mesvalidade; //Mês validade
                $params['payment[card_expdate_year]'] = $anovalidade + 2000; //Ano Validade
                $params['payment[card_cvv]'] = $CodigoSeguranca; //Código de Segurança Impresso no Cartão      

                ob_start();
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $urlPost);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_SSLVERSION, 6);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
                curl_exec($ch);

                /* XML de retorno */
                $resposta = simplexml_load_string(ob_get_contents());
                ob_end_clean();
                curl_close($ch);
                
                if ($resposta->message_response->message == "success") {
                    $link = $resposta->data_response->transaction->payment->url_payment;
                    $token = $resposta->data_response->transaction->token_transaction;
                    $lk = base_url() . "recibo/" . $token;
                    $this->load->model('pedido_model', 'pedidom');
                    $dt = array('cod_interme' => (string) $token);
                    $this->pedidom->atualiza_pedido($idtransacao, $dt);
                    $this->cart->destroy();

                    if ($link != '') {
                        if (($Forma == 6) || ($Forma == 7) || ($Forma == 14) || ($Forma == 21) || ($Forma == 22) || ($Forma == 23)) {
                            redirect($link);
                        }
                    }
                    if (($Forma != 6) && ($Forma != 7) && ($Forma != 14) && ($Forma != 21) && ($Forma != 22) && ($Forma != 23)) {
                        redirect('site/recibo2/' . $token);
                    }
                } else {
                    $erro = $resposta->error_response->validation_errors->validation_error->message_complete;
                    print "<script>alert('Transação Recusada! ($erro) ')</script>";
                    print utf8_decode("<script>window.location.href = 'finalizarPedido';</script>");
                }
            }
            
            $data['total_compra'] = $r[0]->valor;
            
            $this->load->library('table');
            $this->parser->parse('site/include/head', $data);
            $this->load->view("site/tela/finalizarPedidoSegundaVia");
            $this->load->view('site/include/foot');
            
        }
    }

    public function matricular_cadastrar($curso, $ch) {

        $this->load->library("form_validation");

        if (isset($curso) == true && isset($ch) == true) {

            if ($this->input->post()) {
                if ($this->input->post("email") && $this->input->post("senha") == "") {
                    $this->form_validation->set_rules('email', 'email', 'valid_email|required|min_length[6]|trim|is_unique[aluno.id_fic]|is_unique[aluno.email]');
                    $this->form_validation->set_rules('curso', 'curso', 'required|min_length[6]|trim');
                    $this->form_validation->set_rules('carga_horaria', 'carga_horaria', 'required|min_length[6]|trim');

                    if ($this->form_validation->run()) {

                        $termo = $this->tem->get_termo_valido()->result();
                        $usuario = array('email' => $this->input->post('email', true),
                            'senha' => substr(md5(uniqid()), 0, 7),
                            'data_cadastro' => date("Y-m-d h:i:s"),
                            'id_fic' => md5(uniqid(rand(), true)),
                            'codigo_alfa' => md5($this->input->post("email"), true),
                            'situacao_aluno' => 1,
                            'extra' => $termo[0]->id_termo,
                            'origem' => 'Imazon Cursos');
                        if ($this->um->add($usuario)) {
                            $log_termo = array("id_aluno" => $usuario['id_fic'], "id_termo" => $termo[0]->id_termo, "ip" => getenv("REMOTE_ADDR"), "data_aceite" => date("Y-m-d h:i:s"));
                            $this->tem->add_termo_aluno($log_termo);
                            $d = $this->um->rec_senha_email($this->input->post('email'))->result();
                            if (count($d) === 1) {
                                $session = array(
                                    'imazon_id_aluno' => $d[0]->id_aluno,
                                    'imazon_id_fic' => $d[0]->id_fic,
                                    'imazon_session_id' => md5(rand(date('m'), date('s'))),
                                    'imazon_email_aluno' => $this->input->post("email", true),
                                    'imazon_logado' => TRUE,
                                    'rescad' => TRUE,
                                    'imazon_nome' => strtoupper($this->input->post("nome"))
                                );
                                $this->session->set_userdata($session);
                                $this->load->library('user_agent');
                                $this->um->add_auditoria(array('session_id' => $this->session->userdata['imazon_session_id'], 'id_aluno' => $d[0]->id_aluno, 'data_auditoria' => date("Y-m-d h:i:s"), 'ip' => getenv("REMOTE_ADDR"), 'navegador' => $this->agent->browser() . ' - ' . $this->agent->version(), 'sistema' => $this->agent->platform(), 'versao' => 'MOBILE'));

                                if ($this->matricular_curso_externo($curso, $ch, "Cadastrar-Matricular")) {

                                    $this->load->helper("emailphpmailer");
                                    $this->load->library('parser');

                                    $dados = array("email" => $usuario['email'], "senha" => $usuario['senha']);
                                    $msg = $this->parser->parse("site/tela/cadastro_sucesso", $dados);
                                    $assunto = "Cadastro efetuado com sucesso";
                                    $destinatario = $this->input->post("email", true);

                                    envia_email($destinatario, $assunto, $msg);

                                    redirect("ultimo_curso_matriculado");
                                } else {

                                    redirect("ver_curso/$curso");
                                }
                            }
                        }
                    } else {
                        $data = array('titulo' => 'Matricular', 'palavra_chave' => 'Imazon Cursos');
                        if ($curso && $ch) {
                            $data['curso'] = $curso;
                            $data['carga_horaria'] = $ch;
                        }
                        $aluno = $this->um->get_by_email($this->input->post("email"))->result();
                        if (count($aluno) == 1) {
                            $data['usuario_existente'] = true;
                        } else {
                            $data['novo_cadastro'] = true;
                        }
                        $this->parser->parse('site/include/head', $data);
                        $this->load->view('site/tela/cadastrar_matricular');
                        $this->load->view('site/include/foot');
                    }
                } else if ($this->input->post("email") && $this->input->post("senha")) {

                    $this->form_validation->set_rules('senha', 'senha', 'required|matches[senha]|min_length[6]|trim');
                    $this->form_validation->set_rules('curso', 'curso', 'required|min_length[6]|trim');
                    $this->form_validation->set_rules('carga_horaria', 'carga_horaria', 'required|min_length[6]|trim');
                    $this->form_validation->set_rules('email', 'email', 'valid_email|required|min_length[6]|trim');

                    if ($this->form_validation->run()) {
                        //verifica se usuário já existe
                        $usuario = array('email' => $this->input->post('email'), 'senha' => $this->input->post('senha'));
                        if ($this->um->verifica_login($usuario)) {

                            $aluno = $this->um->get_by_email($this->input->post("email"))->result();
                            if (count($aluno) === 1) {
                                if ($aluno[0]->situacao_aluno == 0) {
                                    redirect("home");
                                }
                                $session = array(
                                    'imazon_id_aluno' => $aluno[0]->id_aluno,
                                    'imazon_id_fic' => $aluno[0]->id_fic,
                                    'imazon_session_id' => md5(rand(date('m'), date('s'))),
                                    'imazon_email_aluno' => $this->input->post("email"),
                                    'imazon_logado' => TRUE,
                                    'imazon_nome' => $aluno[0]->nome
                                );
                                $this->session->set_userdata($session);
                                $this->load->library('user_agent');
                                $this->um->add_auditoria(array('session_id' => $this->session->userdata['imazon_session_id'], 'id_aluno' => $aluno[0]->id_aluno, 'data_auditoria' => date("Y-m-d h:i:s"), 'ip' => getenv("REMOTE_ADDR"), 'navegador' => $this->agent->browser() . ' - ' . $this->agent->version(), 'sistema' => $this->agent->platform() . ' - ' . $this->agent->mobile(), 'versao' => 'MOBILE'));
                                $ultimo_termo_aluno = $this->tem->get_ultimo_termo_usuario($aluno[0]->id_fic)->result();
                                $ultimo_termo = $this->tem->get_termo_valido($aluno[0]->id_fic)->result();
                                if (count($ultimo_termo_aluno) == 1) {
                                    if ($ultimo_termo[0]->id_termo != $ultimo_termo_aluno[0]->id_termo) {
                                        $this->session->set_userdata(array("termo_pendente" => true));
                                    }
                                } else {
                                    $this->session->set_userdata(array("termo_pendente" => true));
                                }
                                if ($this->matricular_curso_externo($curso, $ch, "Matricular-Logar ")) {
                                    redirect("ultimo_curso_matriculado");
                                } else {
                                    redirect("ver_curso/$curso");
                                }
                            }
                        } else {

                            $data = array('titulo' => 'Matricular', 'palavra_chave' => 'Imazon Cursos');
                            if ($curso && $ch) {
                                $data['curso'] = $curso;
                                $data['carga_horaria'] = $ch;
                            }
                            $aluno = $this->um->get_by_email($this->input->post("email"))->result();
                            if (count($aluno) == 1) {
                                $data['msg'] = 'Dados incorretos!';
                                $data['usuario_existente'] = true;
                            } else {
                                $data['novo_cadastro'] = true;
                            }
                            $this->parser->parse('site/include/head', $data);
                            $this->load->view('site/tela/cadastrar_matricular');
                            $this->load->view('site/include/foot');
                        }
                    } else {
                        $data = array('titulo' => 'Matricular', 'palavra_chave' => 'Imazon Cursos');
                        if ($curso && $ch) {
                            $data['curso'] = $curso;
                            $data['carga_horaria'] = $ch;
                        }
                        $aluno = $this->um->get_by_email($this->input->post("email"))->result();
                        if (count($aluno) == 1) {
                            $data['usuario_existente'] = true;
                        } else {
                            $data['novo_cadastro'] = true;
                        }
                        $this->parser->parse('site/include/head', $data);
                        $this->load->view('site/tela/cadastrar_matricular');
                        $this->load->view('site/include/foot');
                    }
                } else {
                    $data = array('titulo' => 'Matricular', 'palavra_chave' => 'Imazon Cursos');
                    if ($curso && $ch) {
                        $data['curso'] = $curso;
                        $data['carga_horaria'] = $ch;
                    }
                    $this->parser->parse('site/include/head', $data);
                    $this->load->view('site/tela/cadastrar_matricular');
                    $this->load->view('site/include/foot');
                }
            } else {
                $data = array('titulo' => 'Matricular', 'palavra_chave' => 'Imazon Cursos');
                if ($curso && $ch) {
                    $data['curso'] = $curso;
                    $data['carga_horaria'] = $ch;
                }
                $this->parser->parse('site/include/head', $data);
                $this->load->view('site/tela/cadastrar_matricular');
                $this->load->view('site/include/foot');
            }
        } else {

            redirect("cursos");
        }
    }

    public function logar_matricular() {

        $this->load->library("form_validation");

        $this->form_validation->set_rules('email', 'email', 'valid_email|required|min_length[6]|trim');

        $this->form_validation->set_rules('senha', 'senha', 'required|matches[senha]|min_length[6]|trim');

        $this->form_validation->set_rules("curso", "curso", "required");

        $this->form_validation->set_rules("carga_horaria", "carga_horaria", "required");



        if ($this->form_validation->run()) {

            $usuario = array('email' => $this->input->post('email'), 'senha' => $this->input->post('senha'));

            if ($this->um->verifica_login($usuario)) {

                $aluno = $this->um->get_by_email($this->input->post("email"))->result();

                if (count($aluno) === 1) {



                    //se aluno bloqueado

                    if ($aluno[0]->situacao_aluno == 0) {

                        redirect("home");
                    }



                    $session = array(
                        'imazon_id_aluno' => $aluno[0]->id_aluno,
                        'imazon_id_fic' => $aluno[0]->id_fic,
                        'imazon_session_id ' => md5(rand(date('m'), date('s'))),
                        'imazon_email_aluno' => $this->input->post("email"),
                        'imazon_logado' => TRUE,
                        'imazon_nome' => $aluno[0]->nome
                    );

                    $this->session->set_userdata($session);

                    $this->load->library('user_agent');

                    $this->um->add_auditoria(array('session_id' => $this->session->userdata('session_id'), 'id_aluno' => $aluno[0]->id_aluno, 'data_auditoria' => date("Y-m-d h:i:s"), 'ip' => getenv("REMOTE_ADDR"), 'navegador' => $this->agent->browser() . ' - ' . $this->agent->version(), 'sistema' => $this->agent->platform() . ' - ' . $this->agent->mobile(), 'versao' => 'MOBILE'));



                    $ultimo_termo_aluno = $this->tem->get_ultimo_termo_usuario($aluno[0]->id_fic)->result();

                    $ultimo_termo = $this->tem->get_termo_valido($aluno[0]->id_fic)->result();

                    if (count($ultimo_termo_aluno) == 1) {

                        if ($ultimo_termo[0]->id_termo != $ultimo_termo_aluno[0]->id_termo) {

                            $this->session->set_userdata(array("termo_pendente" => true));
                        }
                    } else {

                        $this->session->set_userdata(array("termo_pendente" => true));
                    }

                    if ($this->matricular_curso_externo($this->input->post('curso'), $this->input->post('carga_horaria'), "Matricular-Logar")) {

                        redirect("ultimo_curso_matriculado");
                    } else {

                        redirect("ver_curso/" . $this->input->post("curso"));
                    }
                } else {

                    if (!$this->input->post()) {

                        redirect("cursos");
                    }



                    if (!$this->form_validation->run()) {

                        redirect("cursos");
                    }



                    $data = array('titulo' => 'Matricular', 'palavra_chave' => 'Imazon Cursos');



                    if ($this->input->post('curso') && $this->input->post('carga_horaria')) {

                        $data['curso'] = $this->input->post('curso');

                        $data['carga_horaria'] = $this->input->post('carga_horaria');
                    }



                    $this->parser->parse('site/include/head', $data);

                    $this->load->view('site/tela/login_matricula');

                    $this->load->view('site/include/foot');
                }
            } else {

                if (!$this->input->post()) {

                    redirect("cursos");
                }



                if (!$this->form_validation->run()) {

                    redirect("cursos");
                }



                $data = array('titulo' => 'Matricular', 'palavra_chave' => 'Imazon Cursos');



                if ($this->input->post('curso') && $this->input->post('carga_horaria')) {

                    $data['curso'] = $this->input->post('curso');

                    $data['carga_horaria'] = $this->input->post('carga_horaria');
                }



                $this->parser->parse('site/include/head', $data);

                $this->load->view('site/tela/login_matricula');

                $this->load->view('site/include/foot');
            }
        } else {

            redirect("cursos");
        }
    }

    public function matricular_curso() {
        $this->load->library("form_validation");
        $this->form_validation->set_rules("curso", "curso", "required");
        $this->form_validation->set_rules("carga_horaria", "carga_horaria", "required");
        if (!$this->seguranca_model->valida_login_aluno()) {
            if (!$this->input->post()) {
                redirect("cursos");
            }
            if (!$this->form_validation->run()) {
                redirect("cursos");
            }
            $data = array('titulo' => 'Matricular', 'palavra_chave' => 'Imazon Cursos');
            if ($this->input->post('curso') && $this->input->post('carga_horaria')) {
                $data['curso'] = $this->input->post('curso');
                $data['carga_horaria'] = $this->input->post('carga_horaria');
            }
            $this->parser->parse('site/include/head', $data);
            $this->load->view('site/tela/login_matricula');
            $this->load->view('site/include/foot');
        } else {
            if ($this->form_validation->run()) {
                $base_legal = $this->bm->get_atual()->result();
                $hash = date('Y') . '-' . strtoupper(substr(md5($this->session->userdata('imazon_id_fic') . ':' . microtime() . '-' . microtime()), 0, 4) . '-' . substr(md5($this->session->userdata('imazon_id_fic') . ':' . microtime()), 0, 4));
                $autenticacao = strtoupper(substr(md5($hash), 0, 8));
                $autenticacao = date('Y') . '-' . substr($autenticacao, 0, 4) . '-' . substr($autenticacao, 4, 9);
                $idHistoricNovo = $hash;
                $carga_horaria = decodifica($this->input->post("carga_horaria"));
                if ((int) $carga_horaria !== 0) {
                    $historico = array('id_historico' => $idHistoricNovo, 'id_aluno' => $this->session->userdata('imazon_id_fic'),
                        'data_aprovacao' => '0000-00-00', 'hora_aprovacao' => '00:00:00', 'nota' => 0,
                        'email' => $this->session->userdata('imazon_email_aluno'), 'id_avaliacao' => 1, 'id_curso' => decodifica($this->input->post("curso")),
                        'primeiro_dia' => date('Y-m-d'), 'primeira_hora' => date('h:i:s'), //'carga_horaria'=>$c['descricao'], 
                        'carga_horaria' => $carga_horaria,
                        'id_baselegal' => $base_legal[0]->id_base_lega, 'situacao' => 0, 'codigo_autenticacao' => $autenticacao, 'observacao' => "Imazon Cursos - Matrícula");
                    if ($this->hm->add($historico)) {
                        redirect("ultimo_curso_matriculado");
                    } else {

                        echo "Não foi possível efetuar a sua matrícula neste curso. Tente novamente em outro momento.";
                    }
                } else {

                    echo "Não foi possível efetuar a sua matrícula neste curso. Tente novamente em outro momento.";
                }
            } else {

                echo "Não foi possível efetuar a sua matrícula neste curso. Dados incorretos.";
            }
        }
    }

    public function buscar_curso_ajax($termo = null) {

        if ($termo != null && strlen($termo) >= 3) {

            $this->load->model("cursos_model", "cm");
        }

        $termo = urldecode($termo);
        $t1 = $termo;
        $termo = str_replace("CURSO", "", $termo);
        $termo = str_replace("curso", "", $termo);
        if (($t1 == 'A FORMAÇÃO DE RECURSOS HUMANOS NA EDUCAÇÃO INCLUSIVA') || ($t1 == 'CAPTAÇÃO DE RECURSOS PARA O TERCEIRO SETOR') || ($t1 == 'RECURSOS HUMANOS, AVALIAÇÃO E DESEMPENHO') || ($t1 == 'GESTÃO DE RECURSOS HUMANOS') || ($t1 == 'MEIO AMBIENTE E RECURSOS SUSTENTAVEIS') || ($t1 == 'CINEMA COMO RECURSO PEDAGÓGICO') || ($t1 == 'O PROCEDIMENTO ARBITRAL E O RECURSO')) {
            $r = $this->cm->busca_cursos_ajax(array(null, null, 'nome' => $t1));
        } else {
            $r = $this->cm->busca_cursos_ajax(array(null, null, 'nome' => $termo));
        }

        if (count($r) == 0) {

            $res = null;
        } else {

            foreach ($r as $rs) {

                $res[] = $rs;
            }
        }

        $this->output->set_content_type('application/json')->set_output(json_encode($res));
    }

    private function matricular_curso_externo($curso, $ch, $tipo_matricula) {

        $base_legal = $this->bm->get_atual()->result();

        $hash = date('Y') . '-' . strtoupper(substr(md5($this->session->userdata('imazon_id_fic') . ':' . microtime() . '-' . microtime()), 0, 4) . '-' . substr(md5($this->session->userdata('imazon_id_fic') . ':' . microtime()), 0, 4));

        $autenticacao = strtoupper(substr(md5($hash), 0, 8));
        $autenticacao = date('Y') . '-' . substr($autenticacao, 0, 4) . '-' . substr($autenticacao, 4, 9);
        $idHistoricNovo = $hash;
        $carga_horaria = $this->pm->get_carga_horaria(decodifica($ch));

        if (count($carga_horaria) == 1) {

            $historico = array('id_historico' => $idHistoricNovo, 'id_aluno' => $this->session->userdata('imazon_id_fic'),
                'data_aprovacao' => '0000-00-00', 'hora_aprovacao' => '00:00:00', 'nota' => null,
                'email' => $this->session->userdata('imazon_email_aluno'), 'id_avaliacao' => 1, 'id_curso' => decodifica($curso),
                'primeiro_dia' => date('Y-m-d'), 'primeira_hora' => date('h:m:s'),
                'carga_horaria' => $carga_horaria[0]->tempo,
                'id_baselegal' => $base_legal[0]->id_base_lega, 'situacao' => 0, 'codigo_autenticacao' => $autenticacao, 'observacao' => "Imazon Cursos $tipo_matricula");

            if ($this->hm->add($historico)) {

                return true;
            } else {

                return false;
            }
        } else {

            return false;
        }
    }
    
    public function finalizar_compra_saldo()
    {
        $this->cart->destroy();
        redirect('cursos_matriculados');
    }

}
