<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class wiki extends CI_Controller{
	
	
	public function index(){
            
            $this->load->helper('url');	
            $id = $this->uri->segment(3);

            //verifica se cliente j� est� cadastrado no moip assinaturas
            $ch = curl_init();

            //cabe�alho
            $header = array(
                                    "Content-Type: application/json"
                            ); 

            // mudar para url de produ��o remvoendo o "desenvolvedor"
            $options = array(CURLOPT_URL => 'https://en.wikipedia.org/w/api.php?action=query&titles='.$id.'&prop=revisions&rvprop=content&format=json',//url da requisi��o
                       CURLOPT_HTTPHEADER => $header,//cabe�alho da requisi��o
                       CURLOPT_SSL_VERIFYPEER => false,
                       CURLOPT_CUSTOMREQUEST => "GET",
                       CURLOPT_RETURNTRANSFER => true//retorno
            );

            //transforma requisi��o em array
            curl_setopt_array($ch, $options);

            //executa requisi��o
            $result = curl_exec($ch); 

            //fecha requisi��o
            curl_close($ch);

            echo $result;
			
	}
	
}

