<?php

class usuario extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library("form_validation");
        $this->load->helper("combox");
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
        if (!$this->seguranca_model->valida_login_aluno()) {
            $this->session->sess_destroy();
            redirect('home#login');
        }
        if ($this->session->userdata("termo_pendente")) {
            redirect("site/termo_novo#termo");
        }
        $this->load->model('usuarios_model', 'um');
    }

    public function index() {
        redirect('cursos_matriculados');
    }

    public function pesquisa() {
        $data = array('titulo' => 'Pesquisa de Satisfação', 'palavra_chave' => 'Imazon Cursos', 'usuario' => $usuario);
        $this->parser->parse('aluno/include/head', $data);
        $this->load->view("aluno/tela/pesquisa");
        $this->load->view("aluno/include/foot");
    }

    public function dados_cadastrais() {
        $usuario = $this->um->get_aluno()->result();
        $data = array('titulo' => 'Seus dados', 'palavra_chave' => 'Imazon Cursos', 'usuario' => $usuario);
        $this->parser->parse('aluno/include/head', $data);
        $this->load->view('aluno/include/sidebar');
        $this->load->view('aluno/tela/dados_cadastrais');
        $this->load->view('aluno/include/foot');
    }

    public function dados_contato() {

        $this->load->library("form_validation");
        $this->form_validation->set_rules('ddd_telefone', 'ddd', 'required|max_length[2]|trim|numeric');
        $this->form_validation->set_rules('telefone', 'celular', 'required|max_length[9]|trim|numeric');

        if ($this->input->post()) {
            if ($this->form_validation->run()) {
                $this->load->helper('array');
                $dados = elements(array('ddd_telefone', 'telefone'), $this->input->post());
                if ($this->um->update($dados)) {
                    $this->session->set_userdata(array("preencher_endereco" => true));
                    redirect("finalizarPedido");
                } else {
                    $data = array(
                        'titulo' => 'Dados de cobrança',
                        'palavra_chave' => 'Imazon Cursos'
                    );
                    $data['usuario'] = $this->um->get_aluno()->result();
                    $this->load->library('table');
                    $this->parser->parse('aluno/include/head', $data);
                    $this->load->view('aluno/include/sidebar');
                    $this->load->view('aluno/tela/dados_contato');
                    $this->load->view('aluno/include/foot');
                }
            }
        }

        $data = array(
            'titulo' => 'Dados de cobrança',
            'palavra_chave' => 'Imazon Cursos'
        );
        $data['usuario'] = $this->um->get_aluno()->result();
        $this->load->library('table');
        $this->parser->parse('aluno/include/head', $data);
        $this->load->view('aluno/include/sidebar');
        $this->load->view('aluno/tela/dados_contato');
        $this->load->view('aluno/include/foot');
    }

    public function endereco_cobranca() {
        $data = array(
            'titulo' => 'Dados de cobrança',
            'palavra_chave' => 'Imazon Cursos'
        );
        $data['usuario'] = $this->um->get_aluno()->result();

        if ($this->input->post()) {

            $this->load->library("form_validation");

            $this->form_validation->set_rules('estado', 'estado', 'required|callback_valida_estado');
            $this->form_validation->set_rules('cidade', 'cidade', 'required');
            $this->form_validation->set_rules('cep', 'cep', 'required');
            $this->form_validation->set_rules('endereco', 'endereco', 'required|min_length[2]|max_length[150]');
            $this->form_validation->set_rules('numero', 'numero', 'required');
            $this->form_validation->set_rules('bairro', 'bairro', 'required');
            $this->form_validation->set_rules('ddd_telefone', 'ddd', 'required|max_length[2]');
            $this->form_validation->set_rules('telefone', 'celular', 'required|max_length[9]');

            if ($data['usuario'][0]->nome == "") {
                $this->form_validation->set_rules('nome', 'nome', 'min_length[6]|max_length[120]|required|callback_valida_nome');
            }

            if ($this->form_validation->run() == true) {

                $this->load->helper('array');
                $dados = elements(array('estado', 'cidade', 'cep', 'endereco', 'numero', 'complemento', 'bairro', 'rg', 'ddd_telefone', 'telefone'), $this->input->post());
                $dados['estado'] = strtoupper($dados['estado']);
                $dados['cidade'] = strtoupper($dados['cidade']);
                $dados['endereco'] = strtoupper($dados['endereco']);
                $dados['complemento'] = strtoupper($dados['complemento']);
                $dados['bairro'] = strtoupper($dados['bairro']);

                if ($data['usuario'][0]->nome == "") {
                    $dados['nome'] = $this->input->post("nome");
                }

                if ($this->um->update($dados) == true) {
                    $this->session->set_userdata(array("preencher_endereco" => true));
                    redirect("finalizarPedido");
                } else {
                    $this->session->set_flashdata('danger', 'N&atilde;o foi poss&iacute;vel alterar seus dados de cobrança. Tente novamente!');
                    redirect('endereco_cobranca');
                }
            }
        }

        $this->load->library('table');
        $this->parser->parse('aluno/include/head', $data);
        $this->load->view('aluno/include/sidebar');
        $this->load->view('aluno/tela/endereco_cobranca');
        $this->load->view('aluno/include/foot');
    }

    public function alterar_dados() {

        $usuario = $this->um->get_aluno()->result();

        $this->form_validation->set_rules('estado', 'estado', 'required|callback_valida_estado');
        $this->form_validation->set_rules('cidade', 'cidade', 'required');
        $this->form_validation->set_rules('cep', 'cep', 'required');
        $this->form_validation->set_rules('endereco', 'endereco', 'required');
        $this->form_validation->set_rules('numero', 'numero', 'required');
        $this->form_validation->set_rules('bairro', 'bairro', 'required');
        $this->form_validation->set_rules('ddd_telefone', 'ddd', 'required|max_length[2]');
        $this->form_validation->set_rules('telefone', 'celular', 'required|max_length[9]');

        if ($usuario[0]->nome == ""){
            $this->form_validation->set_rules('nome', 'nome', 'min_length[6]|max_length[120]|required|callback_valida_nome');
        }
        
        if ($this->form_validation->run() == true) {

            $this->load->helper('array');
            $dados = elements(array('apelido', 'estado', 'cidade', 'cep', 'endereco', 'numero', 'complemento', 'bairro', 'ddd_telefone', 'telefone', 'ddd_celular', 'celular', 'data_nasc', 'rg'), $this->input->post());

            $dados['estado'] = strtoupper($dados['estado']);
            $dados['cidade'] = strtoupper($dados['cidade']);
            $dados['endereco'] = strtoupper($dados['endereco']);
            $dados['complemento'] = strtoupper($dados['complemento']);
            $dados['bairro'] = strtoupper($dados['bairro']);
            if ($usuario[0]->nome == "") {
                $dados['nome'] = $this->input->post("nome");
            }

            $this->um->update($dados);
            $this->session->set_flashdata('success', 'Dados alterados com sucesso!');
            if ($this->input->post('redir')) {
                redirect($this->input->post('redir'));
            }
            redirect('dados_cadastrais');
        } else {

            $data = array('titulo' => 'Seus dados', 'palavra_chave' => 'Imazon Cursos', 'usuario' => $usuario);
            $this->parser->parse('aluno/include/head', $data);
            $this->load->view('aluno/include/sidebar');
            $this->load->view('aluno/tela/dados_cadastrais');
            $this->load->view('aluno/include/foot');
        }
    }

    public function valida_nome($nome) {
        if (count(explode(" ", $nome)) >= 2) {
            return true;
        } else {
            $this->form_validation->set_message('valida_nome', 'O nome precisa conter pelo menos duas palavras.');
            return false;
        }
    }

    public function altera_email() {
        $this->load->library("form_validation");
        $this->form_validation->set_rules('novoEmail', 'email', 'required|valid_email');
        
        if ($this->form_validation->run() == true) {
            if ($this->um->verifica_email_cadastrado2($this->input->post('novoEmail'))) {
                if ($this->um->atualizaEmail(array('email' => $this->input->post("novoEmail")))) {
                    $this->session->set_flashdata('success', 'Email alterado com sucesso!');
                    redirect("dados_cadastrais#Email");
                } else {
                    $this->session->set_flashdata('danger', 'N&atilde;o foi poss&iacute;vel alterar o seu email!');
                    redirect("dados_cadastrais#Email");
                }
            } else {
                $this->session->set_flashdata('danger', 'N&atilde;o foi poss&iacute;vel alterar o seu email!');
                redirect("dados_cadastrais#Email");
            }
        } else {
            $this->session->set_flashdata('danger', 'N&atilde;o foi poss&iacute;vel alterar o seu email!');
            redirect("dados_cadastrais#Email");
        }
    }

    public function altera_senha() {
        $this->load->library("form_validation");
        $this->form_validation->set_rules('senha_atual', 'senha_atual', 'required|min_length[6]');
        $this->form_validation->set_rules('senha_nova', 'senha_nova', 'required|min_length[6]');
        
        if ($this->form_validation->run() == true) {
            if ($this->um->atualizaSenha($this->input->post("senha_atual"), $this->input->post("senha_nova"))) {
                $this->session->set_flashdata('success', 'Senha alterada com sucesso!');
                redirect("dados_cadastrais#senha");
            } else {
                
                $this->session->set_flashdata('danger', 'N&atilde;o foi poss&iacute;vel alterar a sua senha. Digite corretamente a sua senha atual.');
                redirect("dados_cadastrais#senha");
            }
        } else {
            
            if (strlen($this->input->post("senha_nova")) < 6) {
                $this->session->set_flashdata('danger', 'N&atilde;o foi poss&iacute;vel alterar a sua senha. A sua senha nova deve conter pelo menos 6 caracteres.');
            } else {
                
                $this->session->set_flashdata('danger', 'N&atilde;o foi poss&iacute;vel alterar a sua senha. Digite corretamente a sua senha atual.');
            }

            redirect("dados_cadastrais#senha");
        }
    }

    public function valida_estado($estado) {
        if ($estado !== "Selecione") {
            return true;
        }
        $this->form_validation->set_message('valida_estado', 'Você precisa informar o estado.');
        return false;
    }

}
