<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class curso_idiomas extends CI_Controller {

    public function __construct() {
        parent::__construct();
   
         if (!$this->seguranca_model->valida_login_aluno()) {
            $this->session->sess_destroy();
            redirect('home#login');
        }        
        $this->load->model("cursos_model", "cm");
        $this->load->model("usuarios_model", "um");
        $this->load->model("termos_model", "tem");
        $this->load->model("idiomas_model", "id");        
    }
    
    

    public function index($inicio = null){ 
        $paga_ingles=$this->faturas_pagas_aluno_ingles($this->session->userdata("imazon_id_fic")); 
        $livre_ingles=$this->fatura_n_usadas_ingles(); 
        $acesso_ingles=$this->verifica_permissao_estudar('1'); 
        
        $paga_espanhol=$this->faturas_pagas_aluno_espanhol($this->session->userdata("imazon_id_fic")); 
        $livre_espanhol=$this->fatura_n_usadas_espanhol(); 
        $acesso_espanhol=$this->verifica_permissao_estudar('2');
         
        if($acesso_ingles['situacao']==1){
        $dados = array('turma'=>$acesso_ingles['turma'],'email'=>$this->session->userdata("imazon_email_aluno"),'nome'=>$this->session->userdata("imazon_nome"), 'aluno'=>$this->session->userdata("imazon_id_fic"), 'curso'=>'1', 'tipo' => '4');
              
        $IDIOMAS = array(
                'nome' => 'IMAZON CURSOS',
                'chave' => 'SU1BWk9OIENVUlNPUw==',
                'token' => '28c8edde3d61a0411511d3b1866f0636'
            );
              
        $send = base64_encode(json_encode(array(
            'instituicao' => $IDIOMAS,'dados'=> $dados)));        
        }
        
      if($acesso_espanhol['situacao']==1){
       $dados_espanhol = array('turma'=>$acesso_espanhol['turma'],'email'=>$this->session->userdata("imazon_email_aluno"),'nome'=>$this->session->userdata("imazon_nome"), 'aluno'=>$this->session->userdata("imazon_id_fic"), 'curso'=>'2', 'tipo' => '4');
             
      $IDIOMAS_espanhol = array(
               'nome' => 'IMAZON CURSOS',
               'chave' => 'SU1BWk9OIENVUlNPUw==',
               'token' => '28c8edde3d61a0411511d3b1866f0636'
           );
             
       $send_espanhol = base64_encode(json_encode(array(
            'instituicao' => $IDIOMAS_espanhol,'dados'=> $dados_espanhol)));        
        }
        
        
        
        $data = array('titulo' => 'Cursos de Idiomas', 'palavra_chave' => 'Imazon Cursos','turma'=>$livre_ingles,'acesso'=>$acesso_ingles,'link'=>$send,
            'turma_espanhol'=>$livre_espanhol,'acesso_espanhol'=>$acesso_espanhol,'link_espanhol'=>$send_espanhol);    
        
       
      
        $this->parser->parse('site/include/head', $data);
        $this->load->view('aluno/include/sidebar');        
        $this->load->view('aluno/tela/cursos_idiomas');       
        $this->load->view('site/include/foot');  
    }
  
    private function faturas_pagas_aluno_ingles($id_aluno=NULL){	              
	    $faturas1=$this->id->get_all_ingles($id_aluno);                    
            return $faturas1;	            
    }
    
    private function faturas_pagas_aluno_espanhol($id_aluno=NULL){	              
	    $faturas1=$this->id->get_all_espanhol($id_aluno);                    
            return $faturas1;	            
    }
   
   private function verifica_permissao_estudar($id_curso=NULL){	              
   $id_aluno=$this->session->userdata("imazon_id_fic"); 
   $retorno=$this->id->verifica_permissao($id_curso,$id_aluno);
   
   if(count($retorno)<1){
   $a=array('resposta'=>'acesso negado','situacao'=>0,'data_liberacao'=>'0000-00-00'); 
   return $a;   
   }
   
   if(count($retorno)>=1){
    $data = explode("-", $retorno[0]->data_inicio);
        $ano = $data[0];
        $mes = $data[1];
	$dia = $data[2];	

	$d_garan = mktime(0, 0, 0, $mes, $dia, $ano);
	$data_atual = mktime();	
	
	if( $data_atual < $d_garan ){
        $a=array('resposta'=>'acesso liberado parcial','situacao'=>2,'data_liberacao'=>date('d/m/Y', strtotime($retorno[0]->data_inicio)),'turma'=>$retorno[0]->turma_ava,'curso'=>$retorno[0]->id_curso);     
        return $a;
        }
        else{
        $a=array('resposta'=>'acesso liberado','situacao'=>1,'data_liberacao'=>date('d/m/Y', strtotime($retorno[0]->data_inicio)),'turma'=>$retorno[0]->turma_ava,'curso'=>$retorno[0]->id_curso);     
        return $a;    
        }
         
   }
   
   }
        
    private function fatura_n_usadas_ingles($id_aluno=NULL){ 
            $faturas=$this->id->get_all_ingles($this->session->userdata("imazon_id_fic"));            
            foreach($faturas as $fatura){
            $cont=count($this->id->get_ingles_n_usada($fatura->transacao));            
            if($cont==0){
               $fat=$fatura->transacao;            
               break; 
                        }
                                        }
      return $fat;
   }
   
    private function fatura_n_usadas_espanhol($id_aluno=NULL){ 
            $faturas=$this->id->get_all_espanhol($this->session->userdata("imazon_id_fic"));            
            foreach($faturas as $fatura){
            $cont=count($this->id->get_espanhol_n_usada($fatura->transacao));            
            if($cont==0){
               $fat=$fatura->transacao;            
               break; 
                        }
                                        }
      return $fat;
   }
   
      private function calcula_fim_turma($dt=NULL){ 
     
      $i=-1;
      while ($i<40){
      $i=$i+1;
      //echo "<br/>".$i;
      $data=date('d/m/Y', strtotime($i."days",strtotime($dt)));
      $dia=date('l', strtotime($i."days",strtotime($dt))); 
      if($dia=='Monday'){
      $j=$j+1; 
      }  
      if($j==4){
      $datas=date('Y-m-d', strtotime($i."days",strtotime($dt)));
      } 
      }
     
      return $datas;
      }    
     
   
   
    public function matricula_ingles($id_aluno=NULL){         
    if($_POST){
    $fatura=$this->id->get_fatura_id($this->input->post('fatura'));   
 
        if($fatura[0]->situacao==3){
        $url = 'https://www.cidadeaprendizagem.com.br/interfaces/idiomas/idiomas/proxima_turma_ingles';
        $ch = curl_init(); 
		 curl_setopt($ch, CURLOPT_URL, $url);       
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FILETIME, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, array('id_aluno' =>$fatura));
               
        $proxima_matricula_ingles = json_decode(curl_exec($ch));  
        
        $datainicio=$proxima_matricula_ingles[0]->data_inicio_aula;
        $turma=$proxima_matricula_ingles[0]->id_curso_idioma_turma;
        $turma_ava=$proxima_matricula_ingles[0]->id_turma_ava;
        $data_fim=$this->calcula_fim_turma($datainicio);  

        $dados=array('id_aluno'=>$this->session->userdata("imazon_id_fic"),'id_turma'=>$turma,'turma_ava'=>$turma_ava,'id_curso'=>'1', 'data_matricula'=>date('Y-m-d H:i:s'), 
            'data_inicio'=>$datainicio,'data_termino'=>$data_fim,'frequencia'=>'0','nota'=>'0',
            'id_transacao'=>$this->input->post('fatura'),'situacao'=>'1');
        $this->id->insere_aluno_turma($dados);
        redirect('idiomas');
       
                                     }   
             }    
                                                        }
   
   
        public function matricula_espanhol($id_aluno=NULL){         
    if($_POST){
    $fatura=$this->id->get_fatura_id($this->input->post('fatura'));   
    
        if($fatura[0]->situacao==3){
        $url = 'https://www.cidadeaprendizagem.com.br/interfaces/idiomas/idiomas/proxima_turma_espanhol';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FILETIME, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, array('id_aluno' => $fatura));

        $proxima_matricula_espanhol = json_decode(curl_exec($ch));     
        
        $datainicio=$proxima_matricula_espanhol[0]->data_inicio_aula;
        $turma=$proxima_matricula_espanhol[0]->id_curso_idioma_turma;
        $turma_ava=$proxima_matricula_espanhol[0]->id_turma_ava;
        $data_fim=$this->calcula_fim_turma($datainicio);  

        $dados=array('id_aluno'=>$this->session->userdata("imazon_id_fic"),'id_turma'=>$turma,'turma_ava'=>$turma_ava,'id_curso'=>'2', 'data_matricula'=>date('Y-m-d H:i:s'), 
            'data_inicio'=>$datainicio,'data_termino'=>$data_fim,'frequencia'=>'0','nota'=>'0',
            'id_transacao'=>$this->input->post('fatura'),'situacao'=>'1');
        $this->id->insere_aluno_turma($dados);
        redirect('idiomas');
       
                                     }   
             }    
                                                        }
   
    
}
