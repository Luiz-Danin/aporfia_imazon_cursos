<?php
if (!defined('BASEPATH'))    exit('No direct script access allowed');

class Curso extends CI_Controller {

    public function __construct() {
        parent::__construct();

        if (!$this->seguranca_model->valida_login_aluno()) {
            $this->session->sess_destroy();
            redirect('home#login');
        }

        if ($this->session->userdata("termo_pendente")) {
            redirect("site/termo_novo#termo");
        }

        $this->load->model("cursos_model", "cm");
        $this->load->model("usuarios_model", "um");
        $this->load->model("termos_model", "tem");
        $this->load->model('certificado_model','certfm');
        $this->load->library('cart');

        //Instancia a classe fpdf
        $params = array('orientation' => 'L', 'unit' => 'mm', 'size' => 'A4');
        require_once(APPPATH.'libraries/fpdf/Fpdf.php');
        $this->fpdf = new FPDF($params);
        
        if ($this->cart->total_items() > 1) {
            $this->session->set_flashdata('alerta-carrinho', 'Você possui ' . $this->cart->total_items() . ' itens no seu carrinho. <a href="' . base_url('finalizarPedido') . '"><b>Clique aqui</b></a> para efetuar o pagamento!');
        } else if ($this->cart->total_items() == 1) {
            $this->session->set_flashdata('alerta-carrinho', 'Você possui 1 item no seu carrinho. <a href="' . base_url('finalizarPedido') . '"><b>Clique aqui</b></a> para efetuar o pagamento!');
        } 
    }

    public function index($inicio = null) {
        $this->load->library('pagination');
        $config['base_url'] = base_url() . 'cursos_matriculados';
        $config['total_rows'] = $this->cm->count_todos_cursos_aluno_home();
        $config['per_page'] = 15;
        $config['uri_segment'] = 2;
        $this->pagination->initialize($config);
        $res = $this->cm->todos_cursos_aluno_home(array('inicio' => $inicio, 'limite' => $config['per_page']));
        $count = count($res);
        $data = array('titulo' => 'Cursos Matriculados', 'palavra_chave' => 'Imazon Cursos', 'paginacao' => $this->pagination->create_links());
        ($count >= 1) ? $data['cursos'] = $res : $data['cursos'] = null;
        
        $this->parser->parse('aluno/include/head', $data);
        $this->load->view('aluno/include/sidebar');
        
        $this->load->view('aluno/tela/cursos');
        
        $this->load->view('aluno/include/foot');
    }

    public function trocar_curso() {
        if (!$this->uri->segment(2))
            redirect("cursos_matriculados");
        $this->load->model("historico_model", "hm");
        $id_historico = decodifica($this->uri->segment(2));
        $historico = $this->hm->get_historico_by_id_aluno_curso($id_historico)->result();
        if (count($historico) == 0)
            redirect("cursos_matriculados");
        if ((int) $historico[0]->id_curso !== 2861)
            redirect("cursos_matriculados");

        if ($this->input->post()) {
            $this->load->library("form_validation");
            $this->form_validation->set_rules("curso", "curso", "required|integer");
            $this->form_validation->set_rules("ht", "histórico", "required|integer");
            if ($this->form_validation->run()) {
                $cr = $this->input->post("curso");
                $hisr = $this->input->post("ht");
                $historico = $this->hm->trocacurso_historico($historico, $cr);
                $dt = array('historico' => $hisr, 'data' => date('Y-m-d H:i:s'));
                $st = $this->hm->inserelogtrocacurso($dt);
                redirect("estudar/" . codifica($id_historico));
            }
        }
        $ht = $historico[0]->id;
        $data = array(
            'id' => $ht,
            'titulo' => 'Usar o Bonus',
            'palavra_chave' => 'Imazon Cursos',
            'historico' => $ht);
        $this->parser->parse('site/include/head', $data);
        $this->load->view('aluno/tela/trocacurso');
        $this->load->view('aluno/include/foot');
    }

    public function desbloquear_curso(){
        if ($this->uri->segment(2) !== null) {
            $this->load->model("historico_model", "hm");
            $id_historico = decodifica($this->uri->segment(2));
            $historico = $this->hm->get_historico_by_id_aluno_curso($id_historico)->result();
            
            if(count($historico)==0) redirect("cursos_matriculados");
            
            if($historico[0]->situacao!=9)redirect("cursos_matriculados");
            
            $historico_atualizado = array('situacao'=>1);
            
            $this->hm->atualiza_historico_by_id($historico[0]->id, $historico_atualizado);
            
        }
        
        redirect("cursos_matriculados");
    }
    
    public function estudar() {
        if ($this->uri->segment(2) !== null) {
            $this->load->model("historico_model", "hm");
            $id_historico = decodifica($this->uri->segment(2));
            $historico = $this->hm->get_historico_by_id_aluno_curso($id_historico)->result();
            if (count($historico) > 0) {
                $curso = $this->cm->get_curso_by_id($historico[0]->id_curso);
                $data = array(
                    'id' => $curso[0]->id_curso,
                    'titulo' => 'Estudar',
                    'nome' => $curso[0]->nome,
                    'descricao' => $curso[0]->descricao,
                    'questionario' => $curso[0]->questionario,
                    'imagem' => $curso[0]->imagem,
                    'palavra_chave' => 'Imazon Cursos',
                    'historico' => $historico,
                    'declaracao' => null,
                    'prova_liberada' => null,
                    'certificado_impresso' => null,
                    'certificado_web' => null,
                    'nota' => null,
                    'tiposCertificado' => null,
                    'libera_compra_segunda_chamada' => null,
                    'ultimo_dia' => date('d/m/Y', strtotime($historico[0]->primeiro_dia . "+90 days"))
                );

                //recebe status/situação do histórico atual do aluno
                switch ($historico[0]->situacao) {
                    case 0://inscrito
                        break;
                    case 1://matrículado
                        $data['declaracao'] = true; //permite ao aluno solicitar uma declaração de participação no curso
                        $prova_liberada = strtotime($historico[0]->primeiro_dia . "+" . round($historico[0]->carga_horaria / 8) . " days");
                        $prova_liberada = date('d-m-Y', $prova_liberada);

                        $ultimo = strtotime($historico[0]->primeiro_dia . "+90 days");
                        $ultimo = date('d-m-Y', $ultimo);
                        $dtAtual = date('d-m-Y');

                        $pLiberada = new DateTime($historico[0]->primeiro_dia);
                        $ultimo = new DateTime($ultimo);
                        $atual = new DateTime($dtAtual);

                        $diferenca = $atual->diff($ultimo);
                        $diferenca2 = $atual->diff($pLiberada);

                        $ch = round($historico[0]->carga_horaria / 8);
                        $data['data_prova_liberada'] = strtotime($historico[0]->primeiro_dia . "+" . round($historico[0]->carga_horaria / 8) . " days");
                                
                        if ($diferenca->format('%a') <= 90) {
                            if ($diferenca2->format('%a') >= $ch) {
                                $data['prova_liberada'] = true;
                            } else {
                                $data['prova_liberada'] = false;
                            }
                        } else {
                            $data['prova_liberada'] = false;
                        }
                        break;
                    case 2://aprovado

                        $this->load->model("produtos_model", "produtom");
                        $this->load->model('certificado_model', 'cem');
                        $data['certificado_impresso'] = $this->cem->get_certificados_by_historico($historico[0]->id)->result();

                        $data['certificado_web'] = true;
                        $data['nota'] = $historico[0]->nota;
                        $data['tiposCertificado'] = $this->produtom->get_all_tipo_certificado_ativo();
                        break;
                    case 3://reprovado
                        $data['certificado_participacao'] = true; //permite ao aluno solicitar uma declaração de participação no curso
                        $this->load->model("pedido_model", "pedm");
                        $data['libera_compra_segunda_chamada'] = $this->pedm->verifica_compra_segunda_chamada_historico($historico[0]->id_historico);
                        $data['nota'] = $historico[0]->nota;
                        break;
                    default:
                        break;
                }

                $data['situacao'] = $historico[0]->situacao; //situação do histórico

                $this->parser->parse('aluno/include/head', $data);
                $this->load->view('aluno/include/sidebar');
                $this->load->view('aluno/tela/estudar');
                $this->load->view('aluno/include/foot');
            } else {
                redirect('cursos_matriculados');
            }
        } else {
            redirect('cursos_matriculados');
        }
    }

    public function cancelar_curso() {
        if ($this->uri->segment(2)) {
            $this->load->model('historico_model', 'hm');
            $id_historico = decodifica($this->uri->segment(2));
            $historico = $this->hm->get_historico_by_id_aluno_curso($id_historico)->result();
            if (count($historico) == 1 && $historico[0]->situacao == 0) {
                if ($this->hm->remove_historico($id_historico)) {
                    $this->session->set_flashdata('success', 'Inscrição cancelada com sucesso!');
                } else {
                    $this->session->set_flashdata('warning', 'Não foi possível excluir esta inscrição!');
                }
            } else {
                $this->session->set_flashdata('warning', 'Não foi possível excluir esta inscrição!');
            }
        }
        redirect('cursos_matriculados');
    }

    public function liberar_curso() {
        if ($this->uri->segment(2)) {
            $this->load->model('historico_model', 'hm');
            $id_historico = decodifica($this->uri->segment(2));
            $historico = $this->hm->get_historico_by_id_aluno_curso($id_historico)->result();

            if (count($historico) == 1 && $historico[0]->situacao == 4) {
                if ($this->hm->congela_historico($id_historico)) {
                    $hash = date('Y') . '-' . strtoupper(substr(md5($this->session->userdata('imazon_id_fic') . ':' . microtime() . '-' . microtime()), 0, 4) . '-' . substr(md5($this->session->userdata('imazon_id_fic') . ':' . microtime()), 0, 4));
                    $autenticacao = strtoupper(substr(md5($hash), 0, 8));
                    $autenticacao = date('Y') . '-' . substr($autenticacao, 0, 4) . '-' . substr($autenticacao, 4, 9);
                    $novohistorico = array('id_historico' => $historico[0]->id_historico, 'id_aluno' => $this->session->userdata('imazon_id_fic'),
                        'data_aprovacao' => '0000-00-00', 'hora_aprovacao' => '00:00:00', 'nota' => 0,
                        'email' => $this->session->userdata('imazon_email_aluno'), 'id_avaliacao' => $historico[0]->id_avaliacao,
                        'id_curso' => $historico[0]->id_curso,
                        'primeiro_dia' => date('Y-m-d'), 'primeira_hora' => date('h:i:s'),
                        'carga_horaria' => $historico[0]->carga_horaria,
                        'id_baselegal' => $historico[0]->id_baselegal, 'situacao' => 1, 'codigo_autenticacao' => $autenticacao);
                    if ($this->hm->add($novohistorico)) {
                        $this->session->set_flashdata('success', 'Inscrição cancelada com sucesso!');
                    } else {
                        $this->session->set_flashdata('warning', 'Não foi possível executar esta operação!');
                    }
                } else {
                    $this->session->set_flashdata('warning', 'Não foi possível executar esta operação!');
                }
            } else {
                $this->session->set_flashdata('warning', 'Não foi possível executar esta operação!');
            }
        }
        redirect('cursos_matriculados');
    }

    public function troca_curso(){
        $data = array('titulo' => 'Trocar Curso', 'palavra_chave' => 'Imazon Cursos');
        $this->parser->parse('site/include/head', $data);
        $this->load->view('aluno/tela/trocar_curso');
        $this->load->view('aluno/include/foot');
    }
    
    public function seleciona_curso_troca(){
        
        if($this->uri->segment(2)){
            $p = explode("|", decodifica($this->uri->segment(2)));
            if(count($p) == 2){
                $this->load->model("historico_model", "hm");
                $id_historico = decodifica($p[1]);
                $historico = $this->hm->get_historico_by_id_aluno_curso($id_historico)->result();
                
                if(count($historico)!=1) redirect("cursos_matriculados");
                if($historico[0]->situacao != 9) redirect("cursos_matriculados");
                
                $dados = array("id_curso" => $p[0]);
                $this->hm->atualiza_historico_by_id($historico[0]->id, $dados);
                
            }
        }
        redirect("cursos_matriculados");
        
    }
    
    public function ajaxBuscaCursoTroca() {
        
        if($this->input->post()){
            $busca = trim($this->input->post("nome"));
            if(strlen($busca) % 2 == 0 && strlen($busca) > 3){
                $data["cursos"] = $this->cm->todos_cursos_busca(array("nome" => $busca));
                $this->load->view("aluno/tela/cursos_disponiveis_troca", $data);   
            }
        }
        
    }
    
    public function meus_certificados() {
        $this->load->model('certificado_model', 'cem');
        $data = array('titulo' => 'Seus Certificados', 'palavra_chave' => 'Imazon Cursos');
        $data['certificados'] = $this->cem->get_certificados_impresos_by_aluno()->result();
        $this->parser->parse('aluno/include/head', $data);
        $this->load->view('aluno/include/sidebar');
        $this->load->view('aluno/tela/meus_certificados');
        $this->load->view('aluno/include/foot');
    }

    public function gerar_certificado_web() {

        if ($this->uri->segment(2)) {
            $this->load->model('historico_model', 'hm');
            $id_historico = decodifica($this->uri->segment(2));
            $historico = $this->hm->gera_historico_by_id_historico($id_historico)->result();
            if (count($historico) >= 1) {
                $this->load->model('base_legal_model', 'blm');
                $base_legal = $this->blm->get_atual()->result();
                switch ($historico[0]->situacao) {
                    case 2:
                        $situacao = 'Aprovado';
                        break;
                    case 3:
                        $situacao = 'Reprovado';
                        break;
                    default:
                        break;
                }

                //instancia fpdf
//                $params = array('orientation' => 'L', 'unit' => 'mm', 'size' => 'A4');
//                $this->load->library('fpdf', $params);

                // Página de Verso	
                $this->fpdf->AliasNbPages();

                $this->fpdf->AddPage();
                $this->fpdf->Image('imgs/fundo_certificado_imazon.jpg', 1, 1, 270, 'JPG'); //logo
                $this->fpdf->setY("40");
                $this->fpdf->setX("10");
                $this->fpdf->SetFont('Arial', 'B', 40);
                $this->fpdf->Cell(0, 13, utf8_decode('Certificado de Conclusão'), 0, 1, 'C');
                $this->fpdf->SetFont('Arial', '', 16);
                $this->fpdf->Cell(0, 10, 'conferido a', 0, 1, 'C');
                $this->fpdf->SetFont('Arial', 'B', 16);
                $this->fpdf->Cell(0, 10, utf8_decode($historico[0]->nomeAluno), 0, 1, 'C');
                $this->fpdf->SetFont('Arial', '', 16);
                $this->fpdf->Cell(0, 8, 'RG: ' . utf8_decode($historico[0]->rg), 0, 1, 'C');
                $this->fpdf->SetFont('Arial', '', 16);
                $this->fpdf->Cell(0, 5, utf8_decode('Por ter concluído o Curso Livre '), 0, 1, 'C');
                $this->fpdf->setY(120);

                $this->fpdf->setY(90);
                $this->fpdf->MultiCell(0, 5, utf8_decode($historico[0]->nomeCurso), 0, 'C');

                $this->fpdf->setY(105);
                $this->fpdf->setX(50);
                $this->fpdf->Cell(0, 5, utf8_decode('Tipo: Capacitação/Atualização'), 0, 1);
                $this->fpdf->setX(50);
                $this->fpdf->Cell(0, 5, 'Aproveitamento: ' . $historico[0]->nota . ',0', 0, 1);
                $this->fpdf->setX(50);
                $this->fpdf->Cell(0, 5, utf8_decode('Cert. Nº ') . $historico[0]->id_historico, 0, 1);
                $this->fpdf->setY(105);
                $this->fpdf->setX(180);
                $this->fpdf->Cell(0, 5, utf8_decode('Carga Horária: ') . $historico[0]->carga_horaria . ' Horas', 0, 1);
                $this->fpdf->setX(180);
                $this->fpdf->Cell(0, 5, utf8_decode('Data de Início: ') . date('d/m/Y', strtotime($historico[0]->primeiro_dia)), 0, 1);
                $this->fpdf->setX(180);
                $this->fpdf->Cell(0, 5, utf8_decode('Data de Término: ') . date('d/m/Y', strtotime($historico[0]->data_aprovacao)), 0, 1);
                $this->fpdf->setY(120);
                $this->fpdf->Cell(0, 10, utf8_decode('Situação: ') . $situacao, 0, 1, 'C');
                $this->fpdf->SetFont('Arial', '', 16);
                $this->fpdf->SetFont('Arial', '', 16);
                $this->fpdf->SetXY(10, 128);
                $texto = utf8_decode($base_legal[0]->texto);
                $this->fpdf->MultiCell(0, 5, $texto, 0, 'C');
                $this->fpdf->SetFont('Arial', 'B', 16);
                $this->fpdf->Cell(430, 18, utf8_decode('Belém, ') . date("d/m/Y"), 0, 1, 'C');
                $this->fpdf->SetFont('Arial', '', 16);
                $this->fpdf->SetLineWidth(0.2);
                $this->fpdf->Line(30, 170, 125, 170);
                $this->fpdf->SetXY(50, 175);
                $this->fpdf->Cell(0, 1, 'Assinatura do(a) Aluno(a)', 0, 0);
                $this->fpdf->SetXY(20, 187);
                $this->fpdf->Image('imgs/rubrica.jpg', 200, 150, 50);
                $this->fpdf->Line(180, 170, 265, 170);
                $this->fpdf->SetXY(200, 175);
                $this->fpdf->Cell(0, 1, 'Ezelildo G Dornelas', 0, 0);
                $this->fpdf->SetXY(210, 180);
                $this->fpdf->Cell(0, 1, 'Diretor', 0, 0);


                // Página de Verso	
                $this->fpdf->AddPage();
                $this->fpdf->Image('imgs/fundo_certificado_imazon.jpg', 1, 1, 270, 'JPG'); //logo
                $this->fpdf->SetFont('Arial', 'B', 8);
                $this->fpdf->SetXY(20, 40);
                $this->fpdf->Cell(0, 20, utf8_decode('Conteúdo'), 0, 1, 'L');
                $this->fpdf->SetXY(20, 60);
                $conteudo = str_replace("â€“", "-", $historico[0]->descricao);

                $this->fpdf->MultiCell(0, 5, strip_tags(utf8_decode($conteudo)));
                $this->fpdf->SetXY(20, 90);
                $this->fpdf->Image('imgs/abed.jpg', 236, 36, 30);
                $this->fpdf->SetXY(200, 180);
                $this->fpdf->Cell(0, 2, 'Autenticar em: www.imazoncursos.com.br/autenticacao ', 0, 0);
                $this->fpdf->SetXY(200, 187);
                $this->fpdf->Cell(0, 2, utf8_decode('Código de Autenticação: ') . $historico[0]->codigo_autenticacao, 0, 0);


                $this->fpdf->Output();
            } else {
                redirect('cursos_matriculados');
            }
        }

        exit;
    }

    public function gerar_certificado_webnovo() {



        if ($this->uri->segment(2)) {
            $this->load->model('historico_model', 'hm');
            $id_historico = decodifica($this->uri->segment(2));
            $historico = $this->hm->gera_historico_by_id_historico($id_historico)->result();
            if (count($historico) >= 1) {
                $this->load->model('base_legal_model', 'blm');
                $base_legal = $this->blm->get_atual()->result();
                switch ($historico[0]->situacao) {
                    case 2:
                        $situacao = 'Aprovado';
                        break;
                    case 3:
                        $situacao = 'Reprovado';
                        break;
                    default:
                        break;
                }
                $conteudo1 = str_replace('', "-", $historico[0]->nomeCurso);
                echo $conteudo1;
                exit();

                //instancia fpdf
//                $params = array('orientation' => 'L', 'unit' => 'mm', 'size' => 'A4');
//                $this->load->library('fpdf', $params);

                // Página de Verso	
                $this->fpdf->AliasNbPages();

                $this->fpdf->AddPage();
                //$this->fpdf->Image('imgs/fundo_certificado_imazon.jpg', 1, 1, 270, 'JPG'); //logo
                $this->fpdf->setY("40");
                $this->fpdf->setX("10");
                $this->fpdf->SetFont('Arial', 'B', 40);
                $this->fpdf->Cell(0, 13, utf8_decode('Certificado de Conclusão'), 0, 1, 'C');
                $this->fpdf->SetFont('Arial', '', 16);
                $this->fpdf->Cell(0, 10, 'conferido a', 0, 1, 'C');
                $this->fpdf->SetFont('Arial', 'B', 16);
                $this->fpdf->Cell(0, 10, utf8_decode($historico[0]->nomeAluno), 0, 1, 'C');
                $this->fpdf->SetFont('Arial', '', 16);
                $this->fpdf->Cell(0, 8, 'RG: ' . utf8_decode($historico[0]->rg), 0, 1, 'C');
                $this->fpdf->SetFont('Arial', '', 16);
                $this->fpdf->Cell(0, 5, utf8_decode('Por ter concluído o Curso Livre '), 0, 1, 'C');
                $this->fpdf->setY(120);

                $this->fpdf->setY(90);
                $conteudo1 = str_replace(' â€“ ', "-", $historico[0]->nomeCurso);

                $this->fpdf->MultiCell(0, 5, $conteudo1 . '2', 0, 'C');

                $this->fpdf->setY(105);
                $this->fpdf->setX(50);
                $this->fpdf->Cell(0, 5, utf8_decode('Tipo: Capacitação/Atualização'), 0, 1);
                $this->fpdf->setX(50);
                $this->fpdf->Cell(0, 5, 'Aproveitamento: ' . $historico[0]->nota . ',0', 0, 1);
                $this->fpdf->setX(50);
                $this->fpdf->Cell(0, 5, utf8_decode('Cert. Nº ') . $historico[0]->id_historico, 0, 1);
                $this->fpdf->setY(105);
                $this->fpdf->setX(180);
                $this->fpdf->Cell(0, 5, utf8_decode('Carga Horária: ') . $historico[0]->carga_horaria . ' Horas', 0, 1);
                $this->fpdf->setX(180);
                $this->fpdf->Cell(0, 5, utf8_decode('Data de Início: ') . date('d/m/Y', strtotime($historico[0]->primeiro_dia)), 0, 1);
                $this->fpdf->setX(180);
                $this->fpdf->Cell(0, 5, utf8_decode('Data de Término: ') . date('d/m/Y', strtotime($historico[0]->data_aprovacao)), 0, 1);
                $this->fpdf->setY(120);
                $this->fpdf->Cell(0, 10, utf8_decode('Situação: ') . $situacao, 0, 1, 'C');
                $this->fpdf->SetFont('Arial', '', 16);
                $this->fpdf->SetFont('Arial', '', 16);
                $this->fpdf->SetXY(10, 128);
                $texto = utf8_decode($base_legal[0]->texto);
                $this->fpdf->MultiCell(0, 5, $texto, 0, 'C');
                $this->fpdf->SetFont('Arial', 'B', 16);
                $this->fpdf->Cell(430, 18, utf8_decode('Belém, ') . date("d/m/Y"), 0, 1, 'C');
                $this->fpdf->SetFont('Arial', '', 16);
                $this->fpdf->SetLineWidth(0.2);
                $this->fpdf->Line(30, 170, 125, 170);
                $this->fpdf->SetXY(50, 175);
                $this->fpdf->Cell(0, 1, 'Assinatura do(a) Aluno(a)', 0, 0);
                $this->fpdf->SetXY(20, 187);
                $this->fpdf->Image('imgs/rubrica.jpg', 200, 150, 50);
                $this->fpdf->Line(180, 170, 265, 170);
                $this->fpdf->SetXY(200, 175);
                $this->fpdf->Cell(0, 1, 'Ezelildo G Dornelas', 0, 0);
                $this->fpdf->SetXY(210, 180);
                $this->fpdf->Cell(0, 1, 'Diretor', 0, 0);


                // Página de Verso	
                $this->fpdf->AddPage();
                $this->fpdf->Image('imgs/fundo_certificado_imazon.jpg', 1, 1, 270, 'JPG'); //logo
                $this->fpdf->SetFont('Arial', 'B', 8);
                $this->fpdf->SetXY(20, 40);
                $this->fpdf->Cell(0, 20, utf8_decode('Conteúdo'), 0, 1, 'L');
                $this->fpdf->SetXY(20, 60);
                $conteudo = str_replace("â€“", "-", $historico[0]->descricao);

                $this->fpdf->MultiCell(0, 5, strip_tags(utf8_decode($conteudo)));
                $this->fpdf->SetXY(20, 90);
                $this->fpdf->Image('imgs/abed.jpg', 236, 36, 30);
                $this->fpdf->SetXY(200, 180);
                $this->fpdf->Cell(0, 2, 'Autenticar em: www.imazoncursos.com.br/autenticacao ', 0, 0);
                $this->fpdf->SetXY(200, 187);
                $this->fpdf->Cell(0, 2, utf8_decode('Código de Autenticação: ') . $historico[0]->codigo_autenticacao, 0, 0);


                $this->fpdf->Output();
            } else {
                redirect('cursos_matriculados');
            }
        }

        exit;
    }

    public function fazer_avaliacao() {

        if ($this->uri->segment(2)) {
            $this->load->model("historico_model", "hm");
            $item = explode(":", decodifica($this->uri->segment(2)));
            $historico = $item[1];
            $curso = $item[0];
            $historico = $this->hm->get_dados_historico_cabecalho_curso_by_id_historico($historico)->result();


            if ($historico[0]->data_aprovacao == "0000-00-00") {
                $curso = $this->cm->get_curso_by_id($curso);
                $this->load->model("questoes_model", "qm");
                $questoes = $this->qm->get_all_by_curso($curso[0]->id_curso, $historico[0]->id_avaliacao);

                if (!in_array((int) $historico[0]->id_avaliacao, array(1, 2, 3))) {
                    $this->session->set_flashdata('danger', 'Erro ao efetuar avaliação. Entre em contato com o suporte técnico.');
                    redirect("estudar/" . codifica($historico[0]->id_historico));
                }

                if (!in_array((int) $historico[0]->situacao, array(1))) {
                    $this->session->set_flashdata('danger', 'Erro ao efetuar avaliação. Entre em contato com o suporte técnico.');
                    redirect("estudar/" . codifica($historico[0]->id_historico));
                }

                //trabalho escrito
                if (isset($_FILES['arquivo_prova'])) {
                    $target_file = $_FILES['arquivo_prova']['name'];
                    $tipo = pathinfo($target_file, PATHINFO_EXTENSION);

                    $diretorio = "provas/";
                    $nomearquivo = md5($_FILES['arquivo_prova']['tmp_name'] . rand(1, 999)) . '.' . $tipo;
                    $arquivo = $diretorio . '/' . $nomearquivo;
                    if ($_FILES['arquivo_prova']["size"] >= 500000) {
                        echo "arquivo excede o tamanho permitido";
                        redirect("estudar");
                    }

                    if (file_exists($arquivo)) {
                        echo "arquivo já existe";
                        redirect("estudar");
                    }

                    if ($tipo === "pdf" || $tipo === "doc" || $tipo === "docx" || $tipo === "odt") {
                        if (move_uploaded_file($_FILES["arquivo_prova"]["tmp_name"], $arquivo)) {

                            $dados = array('arquivo' => $nomearquivo, 'nota' => 0, 'data_aprovacao' => date('Y-m-d'), 'hora_aprovacao' => date('h:i:s'), 'id_avaliacao' => $historico[0]->id_avaliacao + 1, 'ip_aprovacao' => getenv("REMOTE_ADDR"));
                            $dadosHist = $this->hm->_atualiza_historico($historico[0]->id, $dados);
                            if ($dadosHist) {
                                $this->session->set_flashdata('success', 'Seu arquivo foi enviado com sucesso! Aguarde a correção de sua prova.');
                                redirect("estudar/" . codifica($historico[0]->id_historico));
                            } else {
                                $this->session->set_flashdata('danger', 'Erro ao atualizar histórico.');
                                redirect("estudar/" . codifica($historico[0]->id_historico));
                            }
                        } else {
                            $this->session->set_flashdata('danger', 'Erro ao enviar arquivo. Tente novamente.');
                            redirect("estudar/" . codifica($historico[0]->id_historico));
                        }
                    } else {
                        $this->session->set_flashdata('danger', 'formato não permitido');
                        redirect("estudar/" . codifica($historico[0]->id_historico));
                    }
                }

                //prova escrita
                if ($this->input->post("editor-prova")) {
                    $trabalho = $this->input->post("editor-prova");

                    $dados = array('trabalho' => $trabalho, 'nota' => 0, 'data_aprovacao' => date('Y-m-d'), 'hora_aprovacao' => date('h:i:s'), 'id_avaliacao' => $historico[0]->id_avaliacao + 1, 'ip_aprovacao' => getenv("REMOTE_ADDR"));
                    $dadosHist = $this->hm->_atualiza_historico($historico[0]->id_historico, $dados);
                    if ($dadosHist) {
                        $this->session->unset_userdata(array('avaliacao' => ''));
                        $this->session->unset_userdata(array('id_avaliacao' => ''));
                        $this->session->set_userdata(array('avaliacao' => $curso[0]->id_curso));
                        $this->session->set_userdata(array('id_avaliacao' => $historico[0]->id_avaliacao));
                        $this->session->set_userdata(array('id_historico' => $historico[0]->id));
                        $this->session->set_flashdata('success', 'Seu trabalho de conclusão foi enviado com sucesso! Aguarde a correção de sua prova.');
                        redirect("estudar/" . codifica($historico[0]->id_historico));
                    } else {
                        $this->session->set_flashdata('danger', 'Erro ao atualizar histórico.');
                        redirect("estudar/" . codifica($historico[0]->id_historico));
                    }
                }

                //prova de multipla escolha
                if (count($historico) == 1 && count($curso) == 1 && count($questoes) >= 1) {
                    $this->session->unset_userdata(array('avaliacao' => ''));
                    $this->session->unset_userdata(array('id_avaliacao' => ''));
                    $this->session->set_userdata(array('avaliacao' => $curso[0]->id_curso));
                    $this->session->set_userdata(array('id_avaliacao' => $historico[0]->id_avaliacao));
                    $this->session->set_userdata(array('id_historico' => $historico[0]->id));
                    $data['titulo'] = "Avaliação de " . ucfirst(strtolower($curso[0]->nome));
                    $data['palavra_chave'] = "Prova";
                    $data['questoes'] = $questoes;
                    $data['historico'] = $historico;
                    $data['nomeCurso'] = ucfirst(strtolower($curso[0]->nome));
                    $this->parser->parse('aluno/include/head', $data);
                    $this->load->view('aluno/tela/avaliacao');
                    $this->load->view('aluno/include/foot');
                } else {
                    $this->session->set_flashdata('danger', 'Erro ao efetuar avaliação. Entre em contato com o suporte técnico.');
                    redirect("estudar/" . codifica($historico[0]->id_historico));
                }
            } else {
                $this->session->set_flashdata('danger', 'Erro ao efetuar avaliação. Você já fez esta avaliação antes.');
                redirect("estudar/" . codifica($historico[0]->id_historico));
            }
        } else {
            $this->session->set_flashdata('danger', 'Erro ao efetuar avaliação. Entre em contato com o suporte técnico.');
            redirect("estudar/" . codifica($historico[0]->id_historico));
        }
    }

    public function corrigir_avaliacao() {

        if ($this->session->userdata('avaliacao')) {
            $id_curso = $this->session->userdata('avaliacao');
            $this->load->model("questoes_model", "qm");
            $this->load->model("historico_model", "hm");

            $questoes = $this->qm->get_all_by_curso($id_curso, $this->session->userdata('id_avaliacao'));
            $historico = $this->hm->get_historico_by_id($this->session->userdata('id_historico'))->result();
            
            $numAcertos = 0;
            $numErros = 0;
            $numQuestBr = 0;
            $numQuestResp = 0;
            $numQuest = count($questoes);
            $i = 0;
            $cr = 1;
            $resultado = null;
            $correcao = null;

            foreach($questoes as $q){
                
                foreach ($_POST as $k => $v) {
                    if($k == $q->id_questao && $v == $q->resposta){
                        $numAcertos++;
                    }
                    $numQuestResp++;
                    $resultado[$i] = array('descricao' => $questoes[$i]->descricao, 'status' => 1, 'alternativaCorreta' => $questoes[$i]->resposta, 'resposta' => $v, 'justificativa' => $questoes[$i]->justificativa);
                    $i++;
                }
                
            }
           
            $gabarito = null;
            $g = 1;
            $this->load->model("gabarito_model", "gabm");

            //gera gabarito
            foreach ($questoes as $qg) {
                foreach ($_POST as $id_questao => $resposta) {
                    if ($id_questao == $qg->id_questao) {
                        $gabarito[$g] = array("id_questao" => $id_questao, "resposta" => $resposta);
                        $correcao[$cr] = array('descricao' => $qg->descricao, 'status' => 1, 'alternativaCorreta' => $qg->resposta, 'resposta' => $resposta, 'justificativa' => $qg->justificativa);
                        $g++;
                    }
                }
                if (isset($correcao[$cr]) == false) {
                    $correcao[$cr] = array('descricao' => $qg->descricao, 'status' => 1, 'alternativaCorreta' => $qg->resposta, 'justificativa' => $qg->justificativa);
                }
                $cr++;
            }

            $gabarito = array("id_historico" => $historico[0]->id, "conteudo" => addslashes(json_encode($gabarito)), "id_tipo_prova" => $historico[0]->id_avaliacao, "data_registro" => date("Y-m-d h:i:s"));
            $this->gabm->add($gabarito);

            //calcula nota
            $numQuestBr = $numQuest - $numQuestResp;
            $nota = (($numAcertos * 100) / $numQuest) / 10;

            $avaliacao = $historico[0]->id_avaliacao;
            $dados = array('nota' => $nota,
                'data_aprovacao' => date('Y-m-d'),
                'hora_aprovacao' => date('h:i:s'),
                'ip_aprovacao' => getenv("REMOTE_ADDR"));

            $check_situacao = [2,3];
            $is_avaliacao = in_array($historico[0]->situacao, $check_situacao);
            
            
            //se aluno reprovar
            if ($nota == 0) {
                $dados['situacao'] = 7;
                $hash = date('Y') . '-' . strtoupper(substr(md5($this->session->userdata('imazon_id_fic') . ':' . microtime() . '-' . microtime()), 0, 4) . '-' . substr(md5($this->session->userdata('imazon_id_fic') . ':' . microtime()), 0, 4));
                $autenticacao = strtoupper(substr(md5($hash), 0, 8));
                $autenticacao = date('Y') . '-' . substr($autenticacao, 0, 4) . '-' . substr($autenticacao, 4, 9);
                $reteste = array('id_historico' => $historico[0]->id_historico,
                    'id_aluno' => $historico[0]->id_aluno,
                    'email' => $historico[0]->email,
                    'id_curso' => $historico[0]->id_curso,
                    'primeiro_dia' => $historico[0]->primeiro_dia,
                    'primeira_hora' => $historico[0]->primeira_hora,
                    'carga_horaria' => $historico[0]->carga_horaria,
                    'id_baselegal' => $historico[0]->id_baselegal,
                    'codigo_autenticacao' => $autenticacao,
                    'nota' => null,
                    'id_avaliacao' => 3,
                    'situacao' => 1
                );
                
            } else if ($nota < 5 && $nota >= 1) {
                //gera segunda-chance
                $dados['situacao'] = 3;
                
				$data_inicio= strtotime('2017-06-12');// data de quando começou a promoção certificado eletrônico.
				$data_primeiro_dia= strtotime($historico[0]->primeiro_dia);
					
                                
                                if($data_primeiro_dia>=$data_inicio){
					$certificado = array('id_historico' =>$historico[0]->id, 'codigo_programa' => '006', 'data_registro' => date('Y-m-d H:i:s'), 'situacao' => 0, 'transacao' => '', 'id_tipo' => '3', 'endereco' => '');
					
                                        if( !$is_avaliacao )
                                        {
                                            $this->certfm->add($certificado);
                                        }
                                        
					}	
            } else if ($nota >= 5) {
                $dados['situacao'] = 2;
				$data_inicio= strtotime('2017-06-12');// data de quando começou a promoção certificado eletrônico.
				$data_primeiro_dia= strtotime($historico[0]->primeiro_dia);
					if($data_primeiro_dia>=$data_inicio){
					$certificado = array('id_historico' =>$historico[0]->id, 'codigo_programa' => '006', 'data_registro' => date('Y-m-d H:i:s'), 'situacao' => 0, 'transacao' => '', 'id_tipo' => '3', 'endereco' => '');
					
                                        if( !$is_avaliacao )
                                        {
                                            $this->certfm->add($certificado);
                                        }
                                        
					}
            }
            $dadosHist = $this->hm->atualiza_historico_by_id($this->session->userdata('id_historico'), $dados);
            
            if ( isset($reteste) ) {
                //adiciona novo histórico
                $this->hm->add($reteste);
            }

            if (!$dadosHist) {
                $this->session->set_flashdata('warning', 'Você não pode fazer isso');
                redirect('cursos_matriculados');
            }
            $data = array('numQuestoes' => $numQuest, 'numErros' => $numErros, 'numAcertos' => $numAcertos, 'numQuestoesBranco' => $numQuestBr, 'questoes' => $questoes, 'questoesForm' => $_POST);
            $data['resultado'] = $correcao;
            $data['titulo'] = "Resultado da sua avaliação";
            $data['palavra_chave'] = "Prova";

            $this->parser->parse('aluno/include/head', $data);
            $this->load->view('aluno/tela/avaliacao_corrigida', $dados);
            $this->load->view('aluno/include/foot');
        } else {
            $this->session->set_flashdata('danger', 'Erro ao efetuar avaliação. Você já fez esta avaliação antes.');
            redirect("cursos_matriculados");
        }
    }

    public function gerar_declaracao() {
        if ($this->uri->segment(2)) {
            $h = decodifica($this->uri->segment(2));
            
            $this->load->model("historico_model", "hm");
            $historico = $this->hm->get_declaracao($h)->result();
            
            if ($historico[0]->data_aprovacao == "0000-00-00" && $historico[0]->situacao == 1 || $historico[0]->data_aprovacao == "0000-00-00" && $historico[0]->situacao == 0) {
            $params = array('orientation' => 'P', 'unit' => 'mm', 'size' => 'A4');
            require_once(APPPATH.'libraries/fpdf/Fpdf.php');
            $this->fpdf = new FPDF($params);
                $this->fpdf->AliasNbPages();
                $this->fpdf->AddPage();
                $this->fpdf->SetMargins(19, 18);
                $this->fpdf->Image('imgs/logo.png', 19, 21); //logo
                $this->fpdf->SetFont('Arial', 'B', 9);
                $this->fpdf->Ln(1); //quebra de linha
                $this->fpdf->setX(220);
                $this->fpdf->setY("20");
                $this->fpdf->setX("10");
                $this->fpdf->SetFont('Arial', 'B', 14);
                $this->fpdf->Cell(0, 13, 'CIDADE APRENDIZAGEM', 0, 1, 'R');
                $this->fpdf->setY("28");
                $this->fpdf->setX("10");
                $this->fpdf->SetFont('Arial', '', 8);
                $this->fpdf->Cell(0, 13, utf8_decode('Travessa Quatorze de Março, 221'), 0, 1, 'R');
                $this->fpdf->setY("31");
                $this->fpdf->setX("10");
                $this->fpdf->SetFont('Arial', '', 8);
                $this->fpdf->Cell(0, 13, utf8_decode('Umarizal - Belém/PA - CEP: 66055-490'), 0, 1, 'R');
                $this->fpdf->setY("34");
                $this->fpdf->setX("10");
                $this->fpdf->SetFont('Arial', '', 8);
                $this->fpdf->Cell(0, 13, 'CNPJ: 10.910.194/0001-16', 0, 1, 'R');
                $this->fpdf->setY("70");
                $this->fpdf->setX("10");
                $this->fpdf->setX(220);
                $this->fpdf->setY("80");
                $this->fpdf->setX("10");
                $this->fpdf->SetFont('Arial', 'B', 11);
                $this->fpdf->Cell(0, 13, utf8_decode('Declaração de Matrícula'), 0, 1, 'C');
                $this->fpdf->SetFont('Arial', '', 11);
                $this->fpdf->MultiCell(0, 6, 'Declaro,  para os devidos fins, que ' . utf8_decode($historico[0]->nomeAluno . ' é aluno do Programa de Formação  Imazon Cursos,  mantido pela Instituição de Ensino Cidade Aprendizagem e encontra-se matriculado desde o dia ') . date('d/m/Y', strtotime($historico[0]->primeiro_dia)) . ', no Curso ' . utf8_decode($historico[0]->nomeCurso . ', com Carga Horária de ' . $historico[0]->carga_horaria . ' Horas, do Tipo Capacitação / Atualização, com número de matrícula ' . $historico[0]->id_historico) . '.', 0, 'J');
                $this->fpdf->SetFont('Arial', 'B', 11);
                $this->fpdf->SetXY(19, 132);
                $this->fpdf->Cell(0, 10, utf8_decode('Código de Autenticação: ' . $historico[0]->codigo_autenticacao), 0, 1, 'R');
                $this->fpdf->SetXY(19, 127);
                $this->fpdf->Cell(0, 10, utf8_decode('Autenticar em: www.imazoncursos.com.br/autenticacao'), 0, 1, 'R');
                $this->fpdf->SetFont('Arial', 'B', 11);
                $this->fpdf->SetXY(19, 260);
                $this->fpdf->Cell(0, 10, utf8_decode('Obs: Declaração Válida por 90 Dias a partir da Data da Matrícula.'), 0, 1, 'L');
                $this->fpdf->Image('imgs/rubrica.jpg', 80, 150, 40);
                $this->fpdf->SetXY(75, 165);
                $this->fpdf->Cell(0, 1, 'Prof. Me Ezelildo G Dornelas', 0, 0);
                $this->fpdf->SetXY(95, 170);
                $this->fpdf->Cell(0, 1, 'Diretor', 0, 0);
                $this->fpdf->Output();
            } else {
                redirect('cursos_matriculados');
            }
        }
    }

    public function certificado_participacao() {
        $h = decodifica($this->uri->segment(2));
        $this->load->model("historico_model", "hm");
        $historico = $this->hm->get_declaracao($h)->result();
        if (count($historico) === 1) {
            $texto = utf8_decode(str_replace("<br />", "", nl2br(strip_tags($historico[0]->descricao))));
//            $params = array('orientation' => 'L', 'unit' => 'mm', 'size' => 'A4');
//            $this->load->library('fpdf', $params);
            $this->fpdf->AliasNbPages();

            $this->fpdf->AddPage();
            $this->fpdf->Image('imgs/fundo_certificado_imazon.jpg', 1, 1, 270); //logo
            $this->fpdf->SetFont('Arial', 'B', 9);
            $this->fpdf->setY("40");
            $this->fpdf->setX("10");
            $this->fpdf->SetFont('Arial', 'B', 40);
            $this->fpdf->Cell(0, 13, utf8_decode('Certificado de Participação'), 0, 1, 'C');
            $this->fpdf->SetFont('Arial', '', 16);
            $this->fpdf->Cell(0, 10, 'conferido a', 0, 1, 'C');
            $this->fpdf->SetFont('Arial', 'B', 16);
            $this->fpdf->Cell(0, 10, utf8_decode($historico[0]->nomeAluno), 0, 1, 'C');
            $this->fpdf->SetFont('Arial', 'B', 16);
            $this->fpdf->Cell(0, 10, 'RG: ' . utf8_decode($historico[0]->rg), 0, 1, 'C');

            $this->fpdf->SetFont('Arial', '', 16);

            $this->fpdf->Cell(0, 5, 'Por participar do Curso Livre ', 0, 1, 'C');
            $this->fpdf->SetFont('Arial', 'B', 16);
            $this->fpdf->MultiCell(0, 10, utf8_decode($historico[0]->nomeCurso), 0, 'C');
            $this->fpdf->SetFont('Arial', '', 16);
            $this->fpdf->Cell(0, 10, utf8_decode('Tipo: Capacitação/Atualização'), 0, 1, 'C');
            $this->fpdf->Cell(0, 10, utf8_decode('Carga Horária: ') . $historico[0]->carga_horaria . ' Horas', 0, 1, 'C');
            $this->fpdf->Cell(0, 10, utf8_decode('Data de início: ') . date('d/m/Y', strtotime($historico[0]->primeiro_dia)), 0, 1, 'C');
            $this->fpdf->Cell(0, 10, utf8_decode('Matrícula: ') . $historico[0]->id_historico, 0, 1, 'C');
            $this->fpdf->SetFont('Arial', '', 16);
            $this->fpdf->SetXY(10, 125);
            $this->fpdf->SetFont('Arial', 'B', 16);
            $this->fpdf->SetXY(10, 138);
            $this->fpdf->Cell(430, 18, utf8_decode('Belém, ') . date("d/m/Y"), 0, 1, 'C');
            $this->fpdf->SetXY(10, 125);
            $this->fpdf->SetFont('Arial', '', 16);
            $this->fpdf->SetLineWidth(0.2);
            $this->fpdf->Line(30, 170, 125, 170);
            $this->fpdf->SetXY(50, 175);
            $this->fpdf->Cell(0, 1, 'Assinatura do/a Aluno/a', 0, 0);
            $this->fpdf->SetXY(20, 187);

            $this->fpdf->Image('imgs/rubrica.jpg', 200, 150, 60);
            $this->fpdf->Line(180, 170, 265, 170);
            $this->fpdf->SetXY(200, 175);
            $this->fpdf->Cell(0, 1, 'Ezelildo G Dornelas', 0, 0);
            $this->fpdf->SetXY(210, 180);
            $this->fpdf->Cell(0, 1, 'Diretor', 0, 0);

            // Página de Verso	

            $this->fpdf->AddPage();
            $this->fpdf->Image('imgs/fundo_certificado_imazon.jpg', 1, 1, 270, 'JPG'); //logo
            $this->fpdf->SetFont('Arial', 'B', 8);
            $this->fpdf->SetXY(20, 40);
            $this->fpdf->Cell(0, 20, utf8_decode('Conteúdo'), 0, 1, 'L');
            $this->fpdf->SetXY(20, 60);
            $this->fpdf->MultiCell(0, 5, strip_tags($texto));
            $this->fpdf->SetXY(20, 90);
            $this->fpdf->Image('imgs/abed.jpg', 235, 45, 30);
            $this->fpdf->SetXY(200, 183);
            $this->fpdf->SetFont('Arial', 'B', 10);
            $this->fpdf->Cell(0, 2, 'Autenticar em: www.imazoncursos.com.br/autenticacao/', 0, 0);
            $this->fpdf->SetXY(200, 188);
            $this->fpdf->Cell(0, 1, utf8_decode('Código de Autenticação: ') . $historico[0]->codigo_autenticacao, 0, 0);

            $this->fpdf->Output();
        }
    }

    public function solicitar_segunda_chance() {
        if ($this->uri->segment(2)) {
            $h = decodifica($this->uri->segment(2));
            $this->load->model("historico_model", "hm");
            $historico = $this->hm->get_historico_by_id_historico($h)->result();
            if (count($historico) == 1) {
                if ($historico[0]->id_aluno == $this->session->userdata("imazon_id_fic")) {
                    //adiciona fatura p segunda-chance
                    print_r($historico);
                }
            }
        } else {
            redirect("cursos__matriculados");
        }
    }

    public function ultimo_curso_matriculado() {
        $historico = $this->cm->ultimo_curso_matriculado()->result();
        if (count($historico) == 1) {
            redirect(base_url("add_item_matricula") . "/" . codifica($historico[0]->id_curso) . "/" . codifica($historico[0]->carga_horaria) . "/" . codifica($historico[0]->id));
            //redirect(base_url("estudar") . "/" . codifica($historico[0]->id_historico));
        }
    }

    public function atualizaDataMatricula(){
       
        if($this->input->post("dataNova", true) && $this->input->post("tdi", true) ){
            $dataNova = $this->input->post("dataNova");
            $tdi      = decodifica($this->input->post("tdi"));
            
            $this->load->library("form_validation");
           
            $this->form_validation->set_rules("dataNova", "dataNova", "callback_valida_data");
            $this->form_validation->set_rules("tdi", "curso escolhido", "required");
            
            if(!$this->form_validation->run()) redirect("cursos_matriculados");
            
            $this->load->model("historico_model", "hm");
            
            $historico = $this->hm->get_dados_historico_cabecalho_curso_by_id_historico($tdi)->result();
            
            if(!$historico) redirect("cursos_matriculados");
            
            if((int)$historico[0]->situacao!==0 && (int)$historico[0]->situacao!==9) redirect("cursos_matriculados");
            
            $dataNova = explode("-", $dataNova);
            $dataNova = $dataNova[2].'-'.$dataNova[1].'-'.$dataNova[0];
            $dados = array("primeiro_dia"=>$dataNova);
            
            $dadosHist = $this->hm->atualiza_historico_by_id($tdi, $dados);
            
            if($dadosHist){
                $this->session->set_flashdata('success', 'Data alterada com sucesso');
            }else{
                $this->session->set_flashdata('danger', 'Erro ao alterar a data.');
            }
            
            redirect("cursos_matriculados#".$this->input->post("tdi"));
        }
    }
    
    public function valida_data($data=null){
        if($data){
            if(strtotime($data) < strtotime(date("d-m-Y"))){
                $this->form_validation->set_message('valida_data', 'Você digitou uma data incorreta!');
            }else{
                return true;
            }
        }
        return false;
        
    }
    
}
