<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Videos extends CI_Controller{
    
    public function __construct() {
        parent::__construct();
        //Youtube API
        require_once ('./Google/Client.php');
        require_once ('./Google/Service/YouTube.php');  
    }
    
    public function index(){
        
        $DEVELOPER_KEY = 'AIzaSyCZ-KzK4j2utqcWFd9o-GD7zwj5jeryXiI';

        $client = new Google_Client();
        $client->setDeveloperKey($DEVELOPER_KEY);
        // Define an object that will be used to make all API requests.
        $youtube = new Google_Service_YouTube($client);
        
        $palavra = $this->input->post('titulo');
        $palavra = html_entity_decode($palavra);
        
        $data['searchResponse'] = $youtube->search->listSearch('id,snippet',array(

            'q' => $palavra,
            'maxResults' => 8,


        ));

        $this->load->view('aluno/tela/mostra-videos',$data);
    
    }
    
}
