<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Livros extends CI_Controller{
    public function __construct() {
        parent::__construct();
        //Youtube API
        require_once ('Google/Client.php');
        require_once ('Google/Service/Books.php');  
    }
    
    public function index(){
        
        if($this->input->post('titulo'))
        
        $url = "https://www.googleapis.com/books/v1/volumes?q=".  urlencode($this->input->post('titulo'))."&key=AIzaSyDpK9duRMYIIdch0W6fZj8ZvPvyExLcqqU&country=BR";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $r = json_decode(curl_exec($ch));
        curl_close($ch);
        
        if($r){
            $data['results'] = $r;
        }
        
        $this->load->view('aluno/tela/mostra-livros',$data);
    }
    
    
}
