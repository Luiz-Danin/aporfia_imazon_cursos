<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Financeiro extends CI_Controller{

    function __construct() {
         parent::__construct();

        if (!$this->seguranca_model->valida_login_aluno()) {
            $this->load->model("usuarios_model", "um");
            $this->session->sess_destroy();
            redirect('home#login');
        }
        
        //Instancia a classe fpdf
        $params = array('orientation' => 'L', 'unit' => 'mm', 'size' => 'A4');
        require_once(APPPATH.'libraries/fpdf/Fpdf.php');
        $this->fpdf = new FPDF($params);
        
        $this->load->library('cart');
    }

    public function index(){
        redirect('faturas_pendentes');
    }

    public function faturas(){
        $this->load->model('pedido_model', 'pedidom');
        $this->load->model("usuarios_model", "um");
        $data = array('titulo' => 'Seus Pagamentos', 'palavra_chave' => 'Cursos Imazon Cursos');
        $data['pedidos'] = $this->pedidom->get_by_aluno()->result();
        $this->parser->parse('aluno/include/head', $data);
        $this->load->view('aluno/include/sidebar');
        $this->load->view('aluno/tela/faturas');
        $this->load->view('aluno/include/foot');
    }

    public function emitir_nfp()
    {

        if($this->uri->segment(4)){
            $this->load->model('pedido_model', 'pm');
            $this->load->model('produtos_model', 'prodm');
            $this->load->model("usuarios_model", "um");
            $pedido = decodifica($this->uri->segment(4));
            $p = $this->pm->get_by_transacao_aluno($pedido)->result();
            if(count($p)===1){
                $ped = $this->pm->get_item_pedido_by_transacao($pedido)->result();
                $a   = $this->um->get_aluno()->result();
//                $params = array('orientation' => 'P', 'unit' => 'mm', 'size' => 'A4');
//                $this->load->library('fpdf', $params);
                $this->fpdf->AliasNbPages();
                $this->fpdf->AddPage();
                $this->fpdf->SetMargins(19, 18);
                $this->fpdf->Image('imgs/logo.png', 19, 21); //logo
                $this->fpdf->SetFont('Arial', 'B', 9);
                $this->fpdf->Ln(1); //quebra de linha
                $this->fpdf->setX(220);
                $this->fpdf->setY("20");
                $this->fpdf->setX("10");
                $this->fpdf->SetFont('Arial', 'B', 14);
                $this->fpdf->Cell(0, 13, 'CIDADE APRENDIZAGEM', 0, 1, 'R');
                //$this->fpdf->setY("25");
                //$this->fpdf->setX("10");
                //$this->fpdf->SetFont('Arial', '', 8);
                //$this->fpdf->Cell(0, 13, utf8_decode('Razão social: Cidade Aprendizagem'), 0, 1, 'R');
                $this->fpdf->setY("28");
                $this->fpdf->setX("10");
                $this->fpdf->SetFont('Arial', '', 8);
                $this->fpdf->Cell(0, 13, utf8_decode('Travessa Quatorze de Março, 221'), 0, 1, 'R');
                $this->fpdf->setY("31");
                $this->fpdf->setX("10");
                $this->fpdf->SetFont('Arial', '', 8);
                $this->fpdf->Cell(0, 13, utf8_decode('Umarizal - Belém/PA - CEP: 66055-490'), 0, 1, 'R');
                $this->fpdf->setY("34");
                $this->fpdf->setX("10");
                $this->fpdf->SetFont('Arial', '', 8);
                $this->fpdf->Cell(0, 13, 'CNPJ: 10.910.194/0001-16', 0, 1, 'R');
                $this->fpdf->setY("70");
                $this->fpdf->setX("19");
                $this->fpdf->SetFont('Arial', 'B', 12);
                $this->fpdf->Cell(0, 13, 'Fatura IMAZON CURSOS - '.$p[0]->transacao, 0, 1, 'L');
                $this->fpdf->setY("75");
                $this->fpdf->setX("19");
                $this->fpdf->SetFont('Arial', '', 8);
                $this->fpdf->Cell(0, 13, 'Data da fatura: '.date('d/m/Y', strtotime($p[0]->data_compra)), 0, 1, 'L');
                $this->fpdf->setY("90");
                $this->fpdf->setX("19");
                $this->fpdf->SetFont('Arial', 'B', 8);
                $this->fpdf->Cell(0, 13, 'Faturado para: ', 0, 1, 'L');
                $this->fpdf->setY("94");
                $this->fpdf->setX("19");
                $this->fpdf->SetFont('Arial', '', 8);
                $this->fpdf->Cell(0, 13, utf8_decode($a[0]->nome), 0, 1, 'L');
                $this->fpdf->setY("97");
                $this->fpdf->setX("19");
                $this->fpdf->SetFont('Arial', '', 8);
                $this->fpdf->Cell(0, 13, utf8_decode($a[0]->endereco. ', Nº '.$a[0]->numero), 0, 1, 'L');
                $this->fpdf->setY("100");
                $this->fpdf->setX("19");
                $this->fpdf->SetFont('Arial', '', 8);
                $this->fpdf->Cell(0, 13, utf8_decode($a[0]->bairro), 0, 1, 'L');
                $this->fpdf->setY("103");
                $this->fpdf->setX("19");
                $this->fpdf->SetFont('Arial', '', 8);
                $this->fpdf->Cell(0, 13, utf8_decode($a[0]->cidade). ', '.$a[0]->estado. ', '.$a[0]->cep, 0, 1, 'L');
                $this->fpdf->setY("120");
                $this->fpdf->setX("19");
                $this->fpdf->SetFont('Arial', 'B', 12);
                $this->fpdf->Cell(0, 13, utf8_decode('Descrição'), 0, 1, 'C');
                $this->fpdf->setY('127');
                $this->fpdf->setX("19");
                $this->fpdf->SetFont('Arial', 'B', 8);
                $this->fpdf->Cell(0, 13, 'Item', 0, 1, 'L');
                $this->fpdf->setY('127');
                $this->fpdf->setX("100");
                $this->fpdf->SetFont('Arial', 'B', 8);
                $this->fpdf->Cell(0, 13, 'Valor', 0, 1, 'R');
                $linha = 134;
                $valor = 0;
                foreach($ped as $pe){
                    $produto = $this->prodm->get_by_id($pe->id_produto);
                    if(count($produto)!=1){
                        redirect("aluno/financeiro");
                    }
                  
                    switch ($produto[0]->tipoprod) {
                        case "mat":
                            if($produto[0]->id_produto != 4){
                                $nomeproduto = "Matrícula no CURSO ";
                            }else{
                                $nomeproduto = "Segunda chance do CURSO ";
                            }
                            
                            break;
                        case "doc":
                            $nomeproduto = $produto[0]->nome;
                            break;
                        default:
                            $nomeproduto = "";
                            break;
                    }
                    
                    $valor2 = $pe->valor;
                    $primeiro = substr($valor2, 0, strlen($valor2)-2);
                    $segundo  = substr($valor2, strlen($valor2)-2, strlen($valor2)+2);
                    $total = $primeiro.','.$segundo;
                    $this->fpdf->setY($linha);
                    $this->fpdf->setX("19");
                    $this->fpdf->SetFont('Arial', '', 8);
                    $this->fpdf->Cell(0, 13, utf8_decode($nomeproduto.' '.$pe->nome), 0, 1, 'L');
                    $this->fpdf->setY($linha);
                    $this->fpdf->setX("100");
                    $this->fpdf->SetFont('Arial', '', 8);
                    $this->fpdf->Cell(0, 13, 'R$ '. format_number($total), 0, 1, 'R');
                    $linha +=4;
                    $valor += $valor2;
                }
                $this->fpdf->setX("100");
                $this->fpdf->SetFont('Arial', '', 8);
                $this->fpdf->Cell(0, 13, 'R$ '. format_number($valor), 0, 1, 'R');
                $this->fpdf->setY('260');
                $this->fpdf->setX("100");
                $this->fpdf->SetFont('Arial', 'B', 8);
                $this->fpdf->Cell(0, 13, 'Gerado em '.date('d/m/Y').' - '.date('h:i'), 0, 1, 'R');
                $this->fpdf->Output();
            }else{
                redirect("aluno/financeiro");
            }
        }else{
            redirect("aluno/financeiro");
        }

    }
	
	public function emitir_declaracaodepagamentos(){
	

        if($this->uri->segment(4)){
            $this->load->model('pedido_model', 'pm');
            $this->load->model('produtos_model', 'prodm');
            $this->load->model("usuarios_model", "um");
           // $pedido = decodifica($this->uri->segment(4));
           $p = $this->pm->get_by_aluno_declaracao()->result();	
		   $pimazon = $this->pm->get_by_aluno_declaracao_imazon()->result();
		   $pimazon10 = $this->pm->get_by_aluno_declaracao_imazon10()->result();     
		   $a   = $this->um->get_aluno()->result();
			
			/*
			print_r($pimazon10);
			echo "<br/><br/>";
			print_r($pimazon);
			echo "<br/><br/>";
		    print_r($p);
			echo "<br/><br/>";
			
			 echo "aqui";
			   exit();*/
			 header("Content-Type: text/html; charset=ISO-8859-1",true); 
			 
			 $ano=date("Y")-1;
                         
			 if(count($p)<1 && count($pimazon)<1  && count($pimazon10)<1){
			 print utf8_decode("<script>alert('Você não realizou pagamentos no ano anterior!')</script>");
			 print "<script>window.location.href = '".base_url()."';</script>";
			 exit();
			 }   
		
            if(count($p)>0 || count($pimazon)>0  || count($pimazon10)>0){
              
//                $params = array('orientation' => 'P', 'unit' => 'mm', 'size' => 'A4');
//                $this->load->library('fpdf', $params);
                $this->fpdf->AliasNbPages();
                $this->fpdf->AddPage("P");
                $this->fpdf->SetMargins(19, 18);
                $this->fpdf->Image('imgs/logocidade.jpg', 20, 20); //logo
                $this->fpdf->SetFont('Arial', 'B', 9);
                $this->fpdf->Ln(1); //quebra de linha
                $this->fpdf->setX(220);
                $this->fpdf->setY("20");
                $this->fpdf->setX("10");
                $this->fpdf->SetFont('Arial', 'B', 14);
                $this->fpdf->Cell(0, 13, 'CIDADE APRENDIZAGEM', 0, 1, 'R');
                //$this->fpdf->setY("25");
                //$this->fpdf->setX("10");
                //$this->fpdf->SetFont('Arial', '', 8);
                //$this->fpdf->Cell(0, 13, utf8_decode('Razão social: Cidade Aprendizagem'), 0, 1, 'R');
                $this->fpdf->setY("28");
                $this->fpdf->setX("10");
                $this->fpdf->SetFont('Arial', '', 8);
                $this->fpdf->Cell(0, 13, utf8_decode('Travessa Quatorze de Março, 221'), 0, 1, 'R');
                $this->fpdf->setY("31");
                $this->fpdf->setX("10");
                $this->fpdf->SetFont('Arial', '', 8);
                $this->fpdf->Cell(0, 13, utf8_decode('Umarizal - Belém/PA - CEP: 66055-490'), 0, 1, 'R');
                $this->fpdf->setY("34");
                $this->fpdf->setX("10");
                $this->fpdf->SetFont('Arial', '', 8);
                $this->fpdf->Cell(0, 13, 'CNPJ: 10.910.194/0001-16', 0, 1, 'R');
                $this->fpdf->setY("50");
                $this->fpdf->setX("19");
                $this->fpdf->SetFont('Arial', 'B', 12);
                $this->fpdf->Cell(0, 7, utf8_decode('Declaração dos Valores Pagos a Cidade Aprendizagem'), 0, 1, 'C');
				$this->fpdf->Cell(0,7, utf8_decode('Referente ao Ano de '.$ano), 0, 1, 'C');
				$this->fpdf->Cell(0, 7, utf8_decode('Aluno: ').utf8_decode($a[0]->nome), 0, 1, 'C');
				
				       

                $this->fpdf->setY('80');
                $this->fpdf->setX("19");
                $this->fpdf->SetFont('Arial', 'B', 8);
                $this->fpdf->Cell(0, 13, 'Item', 0, 1, 'L');
                
			    $this->fpdf->setY('80');
                $this->fpdf->setX("135");
                $this->fpdf->SetFont('Arial', 'B', 8);
                $this->fpdf->Cell(0, 13, 'Data', 0, 1, 'L');
			   
			    $this->fpdf->setY('80');
                $this->fpdf->SetFont('Arial', 'B', 8);
                $this->fpdf->Cell(0, 13, 'Valor', 0, 1, 'R');
                
				$linha = 90;
                $valor = 0;
				foreach($pimazon10 as $peimazon10){
				 	$valor2 = $peimazon10->valor;
                    $primeiro = substr($valor2, 0, strlen($valor2)-2);
                    $segundo  = substr($valor2, strlen($valor2)-2, strlen($valor2)+2);
                    $total = $primeiro.','.$segundo;
                    $this->fpdf->setY($linha);
                    $this->fpdf->setX("19");
                    $this->fpdf->SetFont('Arial', '', 8);
                    //$this->fpdf->Cell(0, 13, utf8_decode($nomeproduto.' '.$pe->nome), 0, 1, 'L');
					if($peimazon10->id_tipo==1){
					$tp=utf8_decode("Matrícula no CURSO");					
					}
					
					else{
						$tp=utf8_decode("Segunda chance do CURSO");	
					}
					
                    $this->fpdf->setY($linha);
					$this->fpdf->MultiCell(100,4,$tp." ".utf8_decode($peimazon10->nome),0,'L',0,0);
					
                    $this->fpdf->setY($linha);
					
					$this->fpdf->setX("130");
                    $this->fpdf->SetFont('Arial', '', 8);
                    //$this->fpdf->Cell(0, 13, utf8_decode($nomeproduto.' '.$pe->nome), 0, 1, 'L');
					$this->fpdf->MultiCell(100,4,date('d/m/Y', strtotime($peimazon10->data_compra)),0,'L',0,0);
                    $this->fpdf->setY($linha);
										
					$this->fpdf->setY($linha);					
                    $this->fpdf->setX("100");
                    $this->fpdf->SetFont('Arial', '', 8);
                    $this->fpdf->Cell(0, 5, 'R$ '. format_number($total), 0, 1, 'R');
                    $linha +=10;
                    $valor += $valor2;
				}
				
				
				foreach($pimazon as $peimazon){
				 	$valor2 = $peimazon->valor;
                    $primeiro = substr($valor2, 0, strlen($valor2)-2);
                    $segundo  = substr($valor2, strlen($valor2)-2, strlen($valor2)+2);
                    $total = $primeiro.','.$segundo;
                    $this->fpdf->setY($linha);
                    $this->fpdf->setX("19");
                    $this->fpdf->SetFont('Arial', '', 8);
                    //$this->fpdf->Cell(0, 13, utf8_decode($nomeproduto.' '.$pe->nome), 0, 1, 'L');
					if($peimazon->id_tipo==1){
					$tp=utf8_decode("Matrícula no CURSO");					
					}
					
					else{
						$tp=utf8_decode("Segunda chance do CURSO");	
					}
					
                    $this->fpdf->setY($linha);
					$this->fpdf->MultiCell(100,4,$tp." ".utf8_decode($peimazon->nome),0,'L',0,0);
					
                    $this->fpdf->setY($linha);
					
					$this->fpdf->setX("130");
                    $this->fpdf->SetFont('Arial', '', 8);
                    //$this->fpdf->Cell(0, 13, utf8_decode($nomeproduto.' '.$pe->nome), 0, 1, 'L');
					$this->fpdf->MultiCell(100,4,date('d/m/Y', strtotime($peimazon->data_compra)),0,'L',0,0);
                    $this->fpdf->setY($linha);
										
					$this->fpdf->setY($linha);					
                    $this->fpdf->setX("100");
                    $this->fpdf->SetFont('Arial', '', 8);
                    $this->fpdf->Cell(0, 5, 'R$ '. format_number($total), 0, 1, 'R');
                    $linha +=10;
                    $valor += $valor2;
				}
                foreach($p as $pe){
                    $produto = $this->prodm->get_by_id($pe->tipo_item);
                    if(count($produto)!=1){
                        redirect("aluno/financeiro");
                    }
                  
                    switch ($produto[0]->tipoprod) {
                        case "mat":
                            if($produto[0]->id_produto != 4){
                                $nomeproduto = "Matrícula no CURSO ";
                            }else{
                                $nomeproduto = "Segunda chance do CURSO ";
                            }
                            
                            break;
                        case "doc":
                            $nomeproduto = $produto[0]->nome;
                            break;
                        default:
                            $nomeproduto = "";
                            break;
                    }
                    
                    $valor2 = $pe->valor;
                    $primeiro = substr($valor2, 0, strlen($valor2)-2);
                    $segundo  = substr($valor2, strlen($valor2)-2, strlen($valor2)+2);
                    $total = $primeiro.','.$segundo;
                    $this->fpdf->setY($linha);
                    $this->fpdf->setX("19");
                    $this->fpdf->SetFont('Arial', '', 8);
                    //$this->fpdf->Cell(0, 13, utf8_decode($nomeproduto.' '.$pe->nome), 0, 1, 'L');
					$this->fpdf->MultiCell(100,4,utf8_decode($nomeproduto.' '.$pe->nome),0,'L',0,0);
                    $this->fpdf->setY($linha);
					
					 $this->fpdf->setX("130");
                    $this->fpdf->SetFont('Arial', '', 8);
                    //$this->fpdf->Cell(0, 13, utf8_decode($nomeproduto.' '.$pe->nome), 0, 1, 'L');
					$this->fpdf->MultiCell(100,4,date('d/m/Y', strtotime($pe->data_compra)),0,'L',0,0);
                    $this->fpdf->setY($linha);
					
                    $this->fpdf->setX("100");
                    $this->fpdf->SetFont('Arial', '', 8);
                    $this->fpdf->Cell(0, 5, 'R$ '. format_number($total), 0, 1, 'R');
                    $linha +=10;
                    $valor += $valor2;
					
					if($linha>=265){
					 $this->fpdf->AliasNbPages();
                	 $this->fpdf->AddPage();
					 $linha=10;
					
					}
                }
			
				
                $this->fpdf->setX("100");
                $this->fpdf->SetFont('Arial', 'B', 8);				
                $this->fpdf->Cell(0, 13, 'Valor Total: R$ '. format_number($valor), 0, 1, 'R');               
                $this->fpdf->SetFont('Arial', 'B', 8);              
			    $this->fpdf->SetFont('Arial', '', 11);
				$this->fpdf->Cell(0,7, utf8_decode('Os gastos declarados, realizados durante o ano de '.$ano.', foram para cursos de formação continuada.'), 0, 1, 'L');
				$this->fpdf->Cell(0,7, utf8_decode('Verifique com seu contador se podem ser abatidos em sua Declaração de Rendimentos.'), 0, 1, 'L');				
                $this->fpdf->Output();
            }else{
                redirect("aluno/financeiro");
            }
        }else{
            redirect("aluno/financeiro");
        }

    }

}
