<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class idiomas extends CI_Controller {

    public function __construct() {
        parent::__construct();
        
         if ($this->seguranca_model->valida_login_aluno()) {
           
            redirect('aluno/curso_idiomas');
        }
        
        $this->load->model("cursos_model", "cm");
        $this->load->model("usuarios_model", "um");
        $this->load->model("termos_model", "tem");

        if ($this->cart->total_items() > 1) {
            $this->session->set_flashdata('alerta-carrinho', 'Você possui ' . $this->cart->total_items() . ' itens no seu carrinho. <a href="' . base_url('finalizarPedido') . '"><b>Clique aqui</b></a> para efetuar o pagamento!');
        } else if ($this->cart->total_items() == 1) {
            $this->session->set_flashdata('alerta-carrinho', 'Você possui 1 item no seu carrinho. <a href="' . base_url('finalizarPedido') . '"><b>Clique aqui</b></a> para efetuar o pagamento!');
        }
    }

    public function index($inicio = null) {
        
        $url = 'https://www.cidadeaprendizagem.com.br/interfaces/idiomas/idiomas/proxima_turma_ingles';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FILETIME, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, array('id_aluno' => $id_alunos));
        $turma= json_decode(curl_exec($ch));
               
        $data = array('titulo' => 'Cursos de Idiomas', 'palavra_chave' => 'Imazon Cursos','turma'=>$turma);       
      
        $this->parser->parse('site/include/head', $data);

        $this->load->view('site/include/sidebar_categorias_cursos');
        
        $this->load->view('site/tela/cursos_idiomas');
       
        $this->load->view('site/include/foot');
   
    }

  
    
  
    
}
