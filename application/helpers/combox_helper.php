<?php

function geraEstados($uf = null) {

    $estado = array("AC" => "Acre", "AL" => "Alagoas", "AM" => "Amazonas",
        "AP" => "Amap&aacute;", "BA" => "Bahia", "CE" => "Cear&aacute;",
        "DF" => "Distrito Federal", "ES" => "Espirito Santo", "GO" => "Goi&aacute;s",
        "MA" => "Maranh&atilde;o", "MG" => "Minas Gerais", "MS" => "Mato Grosso do Sul",
        "MT" => "Mato Grosso", "PA" => "Par&aacute;", "PB" => "Para&iacute;ba", "PE" => "Pernambuco",
        "PI" => "Piau&iacute;", "PR" => "Paran&aacute;", "RJ" => "Rio de Janeiro", "RN" => "Rio Grande do Norte",
        "RO" => "Rond&ocirc;nia",
        "RR" => "Roraima",
        "RS" => "Rio Grande do Sul",
        "SC" => "Santa Catarina",
        "SE" => "Sergipe",
        "SP" => "S&atilde;o Paulo",
        "TO" => "Tocantins");
    if($uf==null) echo '<option class="estado" value="" selected="">Selecione</option>';
    foreach ($estado as $chave => $valor) {
        $selecionado = '';
        if($chave==$uf){ 
            echo $chave .' => '.$uf; 
            $selecionado = 'selected=""';
        }    
        
        echo '<option class="estado" value="'.$chave.'" '.$selecionado.'>'.$valor.'</option>';
    }
    
}
