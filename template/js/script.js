
pathArray = location.href.split('/');
protocol = pathArray[0];
host = pathArray[2];

if (host == "aporfia.com.br") {
    var fonte = pathArray[4];
    URL_BASE = protocol + '//' + host + '/' + pathArray[3];
    
    console.log(URL_BASE);
    
} else {
    var fonte = pathArray[3];
    URL_BASE = protocol + '//' + host;
}

console.log(URL_BASE);

$(function () {
    $('.notifica-dados-aluno').popover({trigger: "hover"});
});

$('.notifica-dados-aluno').click(function () {
    $("#modal-alt-aluno").modal('show');
});

$(function () {
    $('.btn-topo-busca').on('click', function () {
        $(this).css('display', 'none');
        $('.busca').css('display', 'block');
    });
});
$(function () {
    $('.busca').on('mouseleave', function () {
        $(this).css('display', 'none').val('');
        $('.btn-topo-busca').css('display', 'block');
    });
});
$(function () {
    $('.busca').on('blur', function () {
        $(this).css('display', 'block');
    })
});

$(function () {
    $(".icone-chamada p > i").mouseover(function () {
        var button = $(this);
        button.addClass("animated bounce");
        setTimeout(function () {
            button.removeClass("animated bounce");
        }, 1000);
    });
});

$(function () {
    $('.matricular-curso').click(function () {
        var idCurso = $(this).attr("data-id");
        $('#modal-carga-horaria').modal('show');
        $("#curso-id").val(idCurso);
    });
});



$(function () {
    $('#modal-cadastro').on('show.bs.modal', function () {
        var url = $("#btn-modal-cadastrar").attr("local");
        $("#email_login_usuario").val('');
    });
});


function IsEmail(email) {
    var exclude = /[^@\-\.\w]|^[_@\.\-]|[\._\-]{2}|[@\.]{2}|(@)[^@]*\1/;
    var check = /@[\w\-]+\./;
    var checkend = /\.[a-zA-Z]{2,3}$/;
    if (((email.search(exclude) != -1) || (email.search(check)) == -1) || (email.search(checkend) == -1)) {
        return false;
    } else {
        return true;
    }
}

  var cap = Math.floor(Math.random() * 9999); 
    var valida_captcha = function(){
                                if($("#captcha").val()==cap){
                                    $("form.frm-cadastro").submit();
                                }else{
                                    alert("Preencha o captcha corretamente!");
                                    return false;
                                }
                           } 

function recuperar_senha() {

    var e1 = $(".email-aluno").val(),
            e2 = $('#email-login').val(),
            e3 = $('[name="email"]').val(),
            email;

    console.log(e1);
    console.log(e2);
    console.log(e3);

    if (e1 != undefined && e1.length > 6) {
        console.log("e1");
        var email = e1;
    }

    if (e2 != undefined && e2.length > 6) {
        console.log("e2");
        var email = e2;
    }

    if (e3 != undefined && e3.length > 6) {
        console.log("e3");
        var email = e3;
    }


    if (email.length >= 6) {
        $(".frm-login").css('display', 'none');
        $('.alert-success').html('Verificando email. Aguarde...');
        
        $.post(URL_BASE + '/recuperar_senha', $(".frm-login").serialize(), function (e) {
            if (e == "ok") {
                alert("Senha enviada com sucesso para: " + email);
                window.location.reload();
            } else {
                alert("Email inválido!");
                window.location.reload();
            }
        });
    } else {
        alert("Digite um email válido!");
    }
}

function logar(recarrega_pagina, params) {
	
    $.post(URL_BASE + '/logar', $(".frm-login").serialize(), function (e) {
        
        if (e == "ok") {
            $('.alert').html('Dados corretos! Aguarde.').removeClass('alert-success').removeClass('alert-danger').addClass('alert-success');
            setTimeout(function () {
                if (recarrega_pagina !== "undefined") {
                    switch (recarrega_pagina) {
                        case "matricula_curso":
                            $.post('matricular_curso', {curso: params.curso, carga_horaria: params.carga_horaria}, function (e) {
                                if (e) {
                                    window.location.href = "ultimo_curso_matriculado";
                                } else {
                                    alert("Não foi possível atender a sua requisição. Tente novamente mais tarde.")
                                }
                            });
                            break;
                        default:
                            window.location.href = "cursos_matriculados";
                            break;
                    }
                } else {
                    if ($("#fonte").val() != "undefined") {
                        window.location.href = fonte;
                    } else {
                        window.location.href = "./cursos_matriculados/";
                    }
                }
            }, 500);

        } else if (e == "Erro login bloqueado") {
            $('#senha-login').val('').focus();
            $('.alert').html('Sua conta foi bloqueada. Entre em contato com o nosso atendimento.').removeClass('alert-success').addClass('alert-danger');
        } else {
            $('#senha-login').val('').focus();
            $('.alert').html('Dados incorretos. Digite novamente sua senha.').removeClass('alert-success').addClass('alert-danger');
        }

    });
}

//validação de formulário de cadastro(home)
function validaFormCadastro() {

    var senha = $('#senha').val(),
            senha2 = $('#senha2').val(),
            nome = $('#nome').val(),
            email = $('#email').val(),
            ddd_celular = $('#ddd_celular').val(),
            celular = $('#celular').val();

    senha = senha.trim();
    senha2 = senha2.trim();
    nome = nome.trim();
    email = email.trim();
    ddd_celular = ddd_celular.trim();
    celular = celular.trim();

    if (senha.length < 6) {
        $(".form-group-senha1").append('<label class="control-label" for="senha">A senha deve conter no mínimo 6 digitos.</label>').addClass('has-warning');
        return false;
    }

    if (senha2.length < 6 || senha !== senha2) {
        $(".form-group-senha2").append('<label class="control-label" for="senha2">Digite corretamente a sua senha</label>').addClass('has-warning');
        return false;
    }

    if (nome.length < 6) {
        $(".form-group-nome").append('<label class="control-label" for="nome">Digite corretamente o seu nome</label>').addClass('has-warning').remove('label');
        return false;
    }

    if (email.length < 6) {
        $(".form-group-email").append('<label class="control-label" for="email">Digite corretamente seu email</label>').addClass('has-warning');
        return false;
    }

    if (ddd_celular.length != 2) {
        $(".form-group-ddd").append('<label class="control-label" for="email">Digite corretamente seu email</label>').addClass('has-warning');
        return false;
    }

    if (celular.length < 8 || celular.length > 9) {
        $(".form-group-celular").append('<label class="control-label" for="email">Digite corretamente seu email</label>').addClass('has-warning');
        return false;
    }

    $(".frm-cadastro").submit();
}

function validaFormPesquisa() {
    var termo = $('[name="busca"]').val(),
            termo = termo.trim();
    if (termo.length >= 2) {
        $('form#form-busca').submit();
    } else {
        alert("Digite ao menos uma palavra");
    }
}
