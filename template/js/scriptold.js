var fonte    = $("#fonte").val(),
    //URL_BASE = "https://imazoncursos.com.br/05042018/";
    URL_BASE = "https://aporfia.com.br/imazoncursos//";

$(function () {
    $('.notifica-dados-aluno').popover({trigger: "hover"});
});

$('.notifica-dados-aluno').click(function () {
    $("#modal-alt-aluno").modal('show');
});

$(function () {
    $('.btn-topo-busca').on('click', function () {
        $(this).css('display', 'none');
        $('.busca').css('display', 'block');
    });
});
$(function () {
    $('.busca').on('mouseleave', function () {
        $(this).css('display', 'none').val('');
        $('.btn-topo-busca').css('display', 'block');
    });
});
$(function () {
    $('.busca').on('blur', function () {
        $(this).css('display', 'block');
    })
});

$(function () {
    $(".icone-chamada p > i").mouseover(function () {
        var button = $(this);
        button.addClass("animated bounce");
        setTimeout(function () {
            button.removeClass("animated bounce");
        }, 1000);
    });
});

$(function () {
    $('.matricular-curso').click(function () {
        var idCurso = $(this).attr("data-id");
        $('#modal-carga-horaria').modal('show');
        $("#curso-id").val(idCurso);
    });
});



$(function () {

    $('#modal-cadastro').on('show.bs.modal', function () {
        var url = $("#btn-modal-cadastrar").attr("local");
        $('.modal-body').html('');
        $('.modal-body').append('<p>Primeiro digite seu email<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></p>' +
                '<form class="frm-modal-cadastro" method="post" onsubmit="return false;">' +
                '<div class="form-group">' +
                '<input type="email" class="form-control" id="email" min="3" required  name="email" placeholder="Digite seu email">' +
                '<br />'+
                '<input type="submit" class="btn btn-success" onclick="logarExterno()" value="Continuar">' +
                '</div>' +
                '<input type="hidden" id="fonte" value="' + fonte + '"/>' +
                '</form>');
    });
});


$(function () {
    $(".btn-logar").click(function () {
        var email1 = $("#email-login").val();
        if (email1.length >= 6)
            $.post(URL_BASE+'verifica_cadastro', {email: email1}, function (e) {
                if (e == "true") {
                    var email = $("#email-login").val();
                    $('.formulario-home').html('');
                    $('.formulario-home').append('<div class="alert alert-success">Por favor, digite a sua senha de acesso</div>' +
                            '<form class="frm-inline frm-login" onsubmit="return false">' +
                            '<div class="form-group">' +
                            '<input type="email" class="form-control" id="email-login" name="email" value="' + email + '">' +
                            '</div>' +
                            '<input type="hidden" id="fonte" value="' + fonte + '"/>' +
                            '<div class="form-group"><input type="password" class="form-control" id="senha-login" name="senha" placeholder="Digite a sua Senha"></div><button type="submit" class="btn btn-logar btn-logar-home btn-success" onclick="logar()">Entrar</button>' +
                            ' <button type="button" class="btn btn-link" onclick="recuperar_senha()">Esqueci minha senha</button>' +
                            '</form>');
                } else {
                    var email = $("#email-login").val();
                    $('.frm-inline').removeClass('form-login-chamada').addClass('form-cadastro-chamada');
                    $('.formulario-home').html('');
                    $('.formulario-home').append('<div class="alert alert-info">Cadastro não encontrado. Digite os seus dados.</div>' +
                            '<form action="cadastrar" class="frm-inline frm-cadastro" method="post" onsubmit="validaFormCadastro(); return false;">' +
                            '<div class="form-group form-group-nome">' +
                            '<input type="text" class="form-control" id="nome" required name="nome" placeholder="Digite o seu nome completo">' +
                            '</div>' +
                            '<div class="form-group form-group-email">' +
                            '<input type="email" class="form-control" id="email" min="3" required  name="email" value="' + email + '">' +
                            '</div>' +
                            '<div class="form-group form-group-senha1">' +
                            '<input type="password" class="form-control" required min="6" max="12" id="senha" name="senha" placeholder="Crie uma Senha">' +
                            '</div>' +
                            '<div class="form-group form-group-senha2">' +
                            '<input type="password" class="form-control" required min="6" max="12" id="senha2" name="senha2" placeholder="Repita a sua Senha">' +
                            '</div>' +
                            '<input type="hidden" id="fonte" value="' + fonte + '"/>' +
                            '<button type="submit" class="btn btn-primary">Cadastrar</button>' +
                            '</form>' +
                            '<p style="font-size:12px; font-weight:bold;">*Ao clicar em cadastrar você estará concordando com nosso <a href="../termo" target="_blank">termo de uso</a></p>'
                            );
                }
            });

        return false;
    });
});

function IsEmail(email){
    var exclude=/[^@\-\.\w]|^[_@\.\-]|[\._\-]{2}|[@\.]{2}|(@)[^@]*\1/;
    var check=/@[\w\-]+\./;
    var checkend=/\.[a-zA-Z]{2,3}$/;
    if(((email.search(exclude) != -1)||(email.search(check)) == -1)||(email.search(checkend) == -1)){return false;}
    else {return true;}
}

function recuperar_senha() {
    
    var e1 = $(".email-aluno").val(),
        e2 = $('#email-login').val(),
        e3 = $('[name="email"]').val(),
        email;
    
    console.log(e1);
    console.log(e2);
    console.log(e3);
    
    if(e1!=undefined && e1.length>6){
        console.log("e1");
        var email = e1;
    }
    
    if(e2!=undefined && e2.length>6){
        console.log("e2");
        var email = e2;
    }
    
    if(e3!=undefined && e3.length>6){
        console.log("e3");
        var email = e3;
    }
    

    if (email.length >= 6) {
        $(".frm-login").css('display', 'none');
        $('.alert-success').html('Verificando email. Aguarde...');
        $.post(URL_BASE+'recuperar_senha', $(".frm-login").serialize(), function (e) {
            if (e == "ok") {
                alert("Senha enviada com sucesso para: " + email);
                window.location.reload();
            } else {
                alert("Email inválido!");
                window.location.reload();
            }
        });
    } else {
        alert("Digite um email válido!");
    }
}

function logar(recarrega_pagina, params) {
    $.post(URL_BASE+'logar', $(".frm-login").serialize(), function (e) {
        if (e == "ok") {
            $('.alert').html('Dados corretos! Aguarde.').removeClass('alert-success').removeClass('alert-danger').addClass('alert-success');
            setTimeout(function () {
                if(recarrega_pagina!== "undefined"){
                    switch (recarrega_pagina){
                        case "matricula_curso":
                            $.post('matricular_curso', {curso:params.curso, carga_horaria:params.carga_horaria}, function (e) {
                                if(e){
                                    window.location.href = "ultimo_curso_matriculado";
                                }else{
                                    alert("Não foi possível atender a sua requisição. Tente novamente mais tarde.")
                                }
                            });
                            break;
                        default:
                            window.location.href = "cursos_matriculados";
                            break;
                    }
                }else{
                    if ($("#fonte").val() != "undefined") {
                        window.location.href = fonte;
                    } else {
                        window.location.href = "./cursos_matriculados/";
                    }
                }
            }, 1000);

        }else if(e == "Erro login bloqueado"){
            $('#senha-login').val('').focus();
            $('.alert').html('Sua conta foi bloqueada. Entre em contato com o nosso atendimento.').removeClass('alert-success').addClass('alert-danger');
        }else{
            $('#senha-login').val('').focus();
            $('.alert').html('Dados incorretos. Digite novamente sua senha.').removeClass('alert-success').addClass('alert-danger');
        }

    });
}

//validação de formulário de cadastro(home)
function validaFormCadastro() {

    var senha = $('#senha').val(),
            senha2 = $('#senha2').val(),
            nome = $('#nome').val(),
            email = $('#email').val();

    senha = senha.trim();
    senha2 = senha2.trim();
    nome = nome.trim();
    email = email.trim();

    if (senha.length < 6) {
        $(".form-group-senha1").append('<label class="control-label" for="senha">A senha deve conter no mínimo 6 digitos.</label>').addClass('has-warning');
        return false;
    }

    if (senha2.length < 6 || senha !== senha2) {
        $(".form-group-senha2").append('<label class="control-label" for="senha2">Digite corretamente a sua senha</label>').addClass('has-warning');
        return false;
    }

    if (nome.length < 6) {
        $(".form-group-nome").append('<label class="control-label" for="nome">Digite corretamente o seu nome</label>').addClass('has-warning').remove('label');
        return false;
    }

    if (email.length < 6) {
        $(".form-group-email").append('<label class="control-label" for="email">Digite corretamente seu email</label>').addClass('has-warning');
        return false;
    }

    $(".frm-cadastro").submit();
}

