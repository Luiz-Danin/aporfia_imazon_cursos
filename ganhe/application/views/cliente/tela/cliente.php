<style>
   .nav-tabs { border-bottom: 2px solid #DDD; }
    .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover { border-width: 0; }
    .nav-tabs > li > a { border: none; color: #ffffff;background: #74C200; }
        .nav-tabs > li.active > a, .nav-tabs > li > a:hover { border: none;  color: #74C200 !important; background: #fff; }
        .nav-tabs > li > a::after { content: ""; background: #74C200; height: 2px; position: absolute; width: 100%; left: 0px; bottom: -1px; transition: all 250ms ease 0s; transform: scale(0); }
    .nav-tabs > li.active > a::after, .nav-tabs > li:hover > a::after { transform: scale(1); }
.tab-nav > li > a::after { background: #00b09c none repeat scroll 0% 0%; color: #fff; }
.tab-pane { padding: 15px 0; }
.tab-content{padding:20px}
.nav-tabs > li  {width:33.33%; text-align:center;}
.card {background: #FFF none repeat scroll 0% 0%; box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.3); margin-bottom: 30px; }
body{ 
    background: #EDECEC; 
/*    padding:50px*/
}

@media all and (max-width:724px){
.nav-tabs > li > a > span {display:none;}	
.nav-tabs > li > a {padding: 5px 5px;}
} 
</style>

<style>
.contact-form{ margin-top:15px;}
.contact-form .textarea{ min-height:220px; resize:none;}
.form-control{ box-shadow:none; border-color:#eee; height:49px;}
.form-control:focus{ box-shadow:none; border-color:#00b09c;}
.form-control-feedback{ line-height:50px;}
.main-btn{ background:#74C200; border-color:#74C200; color:#fff;}
.main-btn:hover{ background:#74C200;color:#fff;}
.form-control-feedback {
line-height: 50px;
top: 0px;
}    
</style>
<style>
.feedback {
    background: #fcfae6;
    color: #857a11;
    margin: 1em;
    padding: .5em .5em .5em 2em;
    border: solid 1px khaki;
}

.panel {
    background-color: white;
    color: darkslategray;
    -webkit-border-radius: .3rem;
    -moz-border-radius: .3rem;
    -ms-border-radius: .3rem;
    border-radius: .3rem;
    margin: 1%;
}
h2 {
    color: #74C200;
}
</style>
<style>
li.tab-indicacao
{
    width: 50%;
    text-align: center;     
}
    /* TOOGLE SWICH */
.switch {
  float: left;
}
.onoffswitch {
  position: relative;
  width: 64px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
}
.onoffswitch-checkbox {
  display: none;
}
.onoffswitch-label {
  display: block;
  overflow: hidden;
  cursor: pointer;
  border: 2px solid #74C200;
  border-radius: 2px;
}
.onoffswitch-inner {
  width: 200%;
  margin-left: -100%;
  -moz-transition: margin 0.3s ease-in 0s;
  -webkit-transition: margin 0.3s ease-in 0s;
  -o-transition: margin 0.3s ease-in 0s;
  transition: margin 0.3s ease-in 0s;
}
.onoffswitch-inner:before,
.onoffswitch-inner:after {
  float: left;
  width: 50%;
  height: 20px;
  padding: 0;
  line-height: 20px;
  font-size: 12px;
  color: white;
  font-family: Trebuchet, Arial, sans-serif;
  font-weight: bold;
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
}

.onoffswitch-switch {
  width: 20px;
  margin: 0;
  background: #FFFFFF;
  border: 2px solid #1ab394;
  border-radius: 2px;
  position: absolute;
  top: 0;
  bottom: 0;
  right: 44px;
  -moz-transition: all 0.3s ease-in 0s;
  -webkit-transition: all 0.3s ease-in 0s;
  -o-transition: all 0.3s ease-in 0s;
  transition: all 0.3s ease-in 0s;
}
.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-inner {
  margin-left: 0;
}
.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-switch {
  right: 0;
}
.onoffswitch-checkbox:disabled + .onoffswitch-label .onoffswitch-inner:before {
  background-color: #919191;
}
.onoffswitch-checkbox:disabled + .onoffswitch-label,
.onoffswitch-checkbox:disabled + .onoffswitch-label .onoffswitch-switch {
  border-color: #919191;
}
/*}*/

/*}*/

.onoffswitch-inner {
  display: block;
  width: 200%;
  margin-left: -100%;
  -moz-transition: margin 0.3s ease-in 0s;
  -webkit-transition: margin 0.3s ease-in 0s;
  -o-transition: margin 0.3s ease-in 0s;
  transition: margin 0.3s ease-in 0s;
}

.onoffswitch-inner:before,
.onoffswitch-inner:after {
  display: block;
  float: left;
  width: 50%;
  height: 16px;
  padding: 0;
  line-height: 16px;
  font-size: 10px;
  color: white;
  font-family: Trebuchet, Arial, sans-serif;
  font-weight: bold;
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
}

.onoffswitch-inner {
  display: block;
  width: 200%;
  margin-left: -100%;
  -moz-transition: margin 0.3s ease-in 0s;
  -webkit-transition: margin 0.3s ease-in 0s;
  -o-transition: margin 0.3s ease-in 0s;
  transition: margin 0.3s ease-in 0s;
}
.onoffswitch-inner:before,
.onoffswitch-inner:after {
  display: block;
  float: left;
  width: 50%;
  height: 16px;
  padding: 0;
  line-height: 16px;
  font-size: 10px;
  color: white;
  font-family: Trebuchet, Arial, sans-serif;
  font-weight: bold;
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
}
.onoffswitch-inner:before {
  content: "SIM";
  padding-left: 7px;
  background-color: #74c200;
  color: #FFFFFF;
}
.onoffswitch-inner:after {
  content: "NÃO";
  padding-right: 7px;
  background-color: #C92434;
  color: #FFFFFF;
  text-align: right;
}
</style>
<style>
    footer {
    clear: both;
    padding-bottom: 21px;
    max-height: 100%;
    background: #5B5B5B;
    color: #000;
}

.conteudo-pagina{max-width: none !important; margin: auto;padding: 20px 0px 0px 0px !important;z-index: 3; background-color: #fff;}
</style>

<h2 class="text-center">Convide os seus amigos e Ganhe Saldo!</h2>
<!--<h3 class="text-center">Participano desta promoção, ganhe saldo e faça a compra de cursos a partir desse benefício.</h2>-->
<h3 class="text-center">Envie um convite a seus amigos por e-mail</h3>
<br>
<div class="container-fluid">
    
    <div class="alert alert-success alert-autocloseable-success">
        <strong>Parabéns!</strong> Você enviou seu convite, aguarde seu amigo aceitar sua indicação para receber saldo a partir de suas compras efetivas.
    </div>
    
    <div class="alert alert-danger alert-autocloseable-danger">
  				    <strong>Ops!</strong> O formato do e-mail está incorreto. Tente novamente.
    </div>
    
    <div class="alert alert-warning alert-autocloseable-warning">
  	<strong>Ops!</strong> O e-mail já foi indicado. Realize um novo Convite.
    </div>
    
    <div class="alert alert-info alert-autocloseable-info">
        <strong>Ops!</strong> Este email pertece a um aluno cadastrado no imazon. Não é possível indicar um aluno.
    </div>
    
    <div class="col-md-4 col-md-offset-4">
        <div class="form-inline">

            <div class="form-group">

              <input type="email" class="form-control" id="emailConvite" autocomplete="off" placeholder="Insira um e-mail">
            </div>
            <input type="hidden" id="id_cliente" name="id_cliente" value="<?php echo codifica( $this->session->userdata('id_cliente') ); ?>">
            <button  type="submit" class="btn btn-default main-btn enableOnInput" disabled="disabled">Enviar</button>
        </div>
    </div>
</div>

<br>
<br>

<link rel="stylesheet" href="https://opensource.keycdn.com/fontawesome/4.7.0/font-awesome.min.css" />


<div class="container-fluid">
  <div class="row">
    <div class="col-md-12"> 
      <!-- Nav tabs -->
      <div class="card">
        <ul class="nav nav-tabs" role="tablist">
          <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab"><i class="fa fa-home"></i>  <span>Home</span></a></li>
          <!--<li role="presentation" ><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab"><i class="fa fa-user"></i>  <span>Perfil de Cliente</span></a></li>-->
          <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab"><i class="fa fa-handshake-o"></i>  <span>Indicações</span></a></li>
          <!--<li class="email_child" role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab"><i class="fa fa fa-child"></i>  <span>Indicação Recebida</span></a></li>-->
          <li role="presentation"><a href="#extra" aria-controls="settings" role="tab" data-toggle="tab"><i class="fa fa-usd"></i>  <span>Saldo</span></a></li>
        </ul>
        
        <!-- Tab panes -->
        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="home">
            <section class="panel important">
                <h2>Bem-Vindo ao Indique e Ganhe</h2>
                <ul>
                  <li>Important saber que: A bonificação que você poderá ganhar é de 10% a partir da compra de uma indicação aceita.</li>
                  <li>Important saber que: Para que não fique em dúvida sobre o indique ganhe consulte o termo de uso.</li>
                  <li>Important saber que: E quanto mais indicações você realizar maiores são as possibilidades de ganha ainda mais saldo.</li>
                </ul>
                <div class="feedback">Aviso: Sem avisos.</div>
          </section>
          </div>
            <!--<div role="tabpanel" class="tab-pane" id="profile">
                <div class="feedback">This is neutral feedback Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias, praesentium. Libero perspiciatis quis aliquid iste quam dignissimos, accusamus temporibus ullam voluptatum, tempora pariatur, similique molestias blanditiis at sunt earum neque.</div>
            </div>-->
          <div role="tabpanel" class="tab-pane" id="messages">
              <div class="container-fluid">
                <h2>Consulte suas indicações</h2>


                <ul class="nav nav-tabs">
                  <li class="tab-indicacao"><a data-toggle="tab" href="#home-second">Enviadas</a></li>
                  <li class="tab-indicacao"><a data-toggle="tab" href="#menu11">Recebidas</a></li>

                </ul>

                <div class="tab-content">
                  <div id="home-second" class="tab-pane fade">
                      
                      <?php if(!empty($nome_indicacao_enviada[0])):?>
                      
                          
                     
                    <h2>Enviadas</h2>
                                <div class="container-fluid">
                                    <table class="table table-hover">
                                      <thead>
                                        <tr>
                                          <th>E-mail</th>
                                          <th>Situação</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                            <?php
                                            for($i=0; $i<count($nome_indicacao_enviada[0]); $i++)
                                            {
                                                echo '<tr class="active">';
                                                echo '<td>'.$nome_indicacao_enviada[0][$i].'</td>';
                                                echo ($nome_indicacao_enviada[1][$i]==0) ? '<td class="warning">Aguardando Aprovação</td>' : '<td class="success">Sua Indicação foi Aceita</td>';
                                                echo '</tr>';
                                            }
                                            ?>
                                      </tbody>
                                    </table>
                                  </div>
                    <?php
                    else:
                        echo '<div class="feedback">Você ainda não realizou indicações</div>';
                    endif; ?>
                  </div>
                    <div id="menu11" class="tab-pane fade">
                      <?php if(!is_null($nome_indicacao_recebida) ):?>
                      <h2>Recebida</h2>
                                <div class="container-fluid">
                                    <table class="table table-hover">
                                      <thead>
                                        <tr>
                                          <th>Nome</th>
                                          <th>Aceita Convite de Indicação?</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        <tr>
                                            <?php
                                            
                                                 //var_dump($this->session->userdata('imazon_email_aluno'));
                                            $n_i_recebida = codifica( $nome_indicacao_recebida[0]['id'] );
                                            $n_i_e = codifica( $this->session->userdata('imazon_email_aluno') );
                                            
                                            
                                          echo '<td>'.$nome_indicacao_recebida[0]['nome'].'</td>';
                                          
                                          echo '<td><div class="switch">
                                                    <div class="onoffswitch">
                                                        <input type="checkbox" class="onoffswitch-checkbox" id="example1" value="'.$n_i_recebida.'">
                                                        <label class="onoffswitch-label" for="example1">
                                                            <span class="onoffswitch-inner"></span>
                                                            <span class="onoffswitch-switch"></span>
                                                        </label>
                                                    </div>
                                                        <input id="n_i_fic" type="hidden" value="'.$n_i_e.'" >
                                                </div></td>';
                                          echo '</tr>';
                                                  ?>
                                      </tbody>
                                    </table>
                                  </div>
                      <?php 
                        else:
                        echo '<div class="feedback">Você não recebeu indicaçao</div>';
                      endif;?>
                  </div>

                </div>
              </div>
          </div>
          <div role="tabpanel" class="tab-pane" id="extra"><h2>Saldo Disponível: R$ <?php echo number_format($saldo, 2, ',', ' ');?></h2>
              <?php 
                if(!empty($bonificacao_cliente) )
                {
                    
                
              ?>
              <table class="table table-hover">
                  <thead>
                    <tr>
                      <th>Amigo Indicado</th>
                      <th>Bônus</th>
                      <th>Liberar Saldo?</th>
                      <th>Informação</th>
                    </tr>
                  </thead>
                  <tbody>
                      <?php 
                      foreach ($bonificacao_cliente as $bonificacao)
                      {
                          $data_resgate = str_replace(" 00:00:00","",$bonificacao['previsao_resgate']);
                          $data_atual = new DateTime();
                          $intervalo = strtotime($data_resgate) - strtotime( $data_atual->format('Y-m-d') );
                          $dias = floor($intervalo / (60 * 60 * 24));
                          
                          if($dias >='1' && ($bonificacao['tipo_log_bonificacao']=='0') )
                          {
                            $liberar_saldo='<td><button class="btn btn-warning"><i class="glyphicon glyphicon-dashboard"></i> Saldo em Análise</button></td>';
                            $informacao = '<td class="warning">Saldo Disponível para Resgate em '.$dias.' dias</td>';
                          }
                          else if($dias <='0' && ($bonificacao['tipo_log_bonificacao']=='0') )
                          {
                            $id_bonificacao = codifica($bonificacao['id']);
                            $liberar_saldo = '<td><button value='.$id_bonificacao.' class="btn btn-success active"><i class="glyphicon glyphicon-plus-sign"></i> Resgatar Saldo</button></td>';
                            $informacao = '<td class="success">Saldo Disponível para Resgate, clique em Resgatar Saldo</td>';
                          }
                          elseif ($dias<='0' && ($bonificacao['tipo_log_bonificacao']=='1') )
                          {
                            $liberar_saldo = '<td><button class="btn btn-info "><i class="glyphicon glyphicon-ok"></i> Saldo Resgatado</button></td>';
                            $informacao = '<td class="info">Este Saldo foi Resgatado</td>';
                          }
                          elseif($bonificacao['tipo_log_bonificacao']=='2')
                          {
                            $liberar_saldo = '<td><button class="btn btn-danger"><i class="glyphicon glyphicon-exclamation-sign"></i> Saldo Indisponível</button></td>';
                            $informacao = '<td class="danger">Compra Cancelada</td>';
                          }
                          
                          echo '<tr>';
                          echo '<td>'.$bonificacao['transacao'].'</td>';
                          echo '<td>R$ '. number_format($bonificacao['valor'], 2, ',', ' ').'</td>';
                          //echo '<td>R$ '. str_replace('.', ',', $bonificacao['valor']).'</td>';
                          //echo '<td>R$ '. $bonificacao['valor'].'</td>';
                          echo $liberar_saldo;
                          echo $informacao;
                          echo ' </tr>';
                      }
                      }
                      else
                      {
                        echo '<div class="feedback">Você não recebeu Bonificação</div>';
                      }
                      ?>
                  </tbody>
                </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid well">
	<h2><center>Funciona simples assim</center></h2>

<div id="footer">
    <div class="container-fluid">
        <div class="row">
            <br>
              <div class="col-md-4">
                <center>
                  <img src="https://cdn2.iconfinder.com/data/icons/square-logo-buttons/512/mail-128.png" class="img-circle" alt="the-brains">
                  <br>
                  <h4 class="footertext">Indique um Amigo!</h4>
                  <p class="footertext">Realize seu primeiro convite por meio do formulário de indicação.<br>
                </center>
              </div>
              <div class="col-md-4">
                <center>
                  <img src="https://cdn2.iconfinder.com/data/icons/circle-icons-1/64/money-128.png" class="img-circle" alt="...">
                  <br>
                  <h4 class="footertext">Passe a Ganhar Saldo!</h4>
                  <p class="footertext">Ganhe Saldo a partir de qualquer compra efetiva do amigo indicado que tenha aceitado sua indicação.<br>
                </center>
              </div>
              <div class="col-md-4">
                <center>
                  <img src="https://cdn2.iconfinder.com/data/icons/business-round-icons/500/success-128.png" class="img-circle" alt="...">
                  <br>
                  <h4 class="footertext">Sonhe Alto!</h4>
                  <p class="footertext">Convide mais amigos, ganhe mais saldo para comprar ainda mais cursos e se qualificar ainda mais.<br>
                </center>
              </div>
            </div>
    </div>
</div>
</div>

<!--<div class="container">
  <div>
    <a href="#model-id" role="button" class="btn btn-primary" data-toggle="modal">Consultar Termo de Uso.</a>
  </div>
  
  <div id="model-id" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3>This is the modal header.</h3>
      </div>
      <div class="modal-body">This is the modal content.Before we move on, I would first like to clarify when you should not (as well as cannot) use the pthreads extension.

In pthreads v2, the recommendation was that pthreads should not be used in a web server environment (i.e. in an FCGI process). As of pthreads v3, this recommendation has been enforced, so now you simply cannot use it in a web server environment. The two prominent reasons for this are:
</div>
      <div class="modal-footer">
        <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Close</button>
      </div>
    </div>
  </div>
  </div>
</div>-->
<br>
<script>
    $(document).ready(function() {
$('a[href="#messages"').click(function() {
        //alert('yes, the click actually happened');
        $('.nav-tabs a[href="#home-second"]').tab('show');
    });
});
</script>
<script>
$(document).ready(function(){
     $('#openBtn').click(function () {
        $('#myModal').modal({
            show: true
        })
    });
    
    $("button.active").click(function() {
         var id_bonus = $(this).val();
         var id_cliente = $("#id_cliente").val();

					  var info = ['Deseja resgatar o Saldo?'];

					  $('#myModal').html("");
					  $('body').append($('<footer/>'));
					  $('footer').append($('<div/>',{'class':'modal fade','id':'myModal','role':'dialog'}));
					  $('#myModal').append($('<div/>',{'class':'modal-dialog'}));
					  $('.modal-dialog').append($('<div/>',{'class':'modal-content'}));
					  $('.modal-content').append($('<div/>',{'class':'modal-header'}));
					  $('.modal-header').append($('<button/>',{'class':'close', 'data-dismiss':'modal'}));
					  $('.modal-header').append($('<h3/>',{'class':'modal-title',text:'Verificação de Resgate de Saldo'}));
					  $('.modal-content').append($('<div/>',{'class':'modal-body'}));
					  $('.modal-body').append($('<p/>',{text:info}));
					  $('.modal-body').append($('<div/>',{'class':'modal-footer'}));

					  $('.modal-footer').append($('<button/>',{'class':'btn btn-success','data-dismiss':'modal',text:'Sim','id':'saldo-yes'}));
					  $('.modal-footer').append($('<button/>',{'class':'btn btn-danger','data-dismiss':'modal',text:'Não','id':'saldo-no'}));

                            $('.modal-footer button').click(function(){
                                
                                  var escola_saldo = $(this).attr('id');
                                  if(escola_saldo==="saldo-yes")
                                  {
                                      var BASE_URL = "<?php echo base_url();?>";
                                      
                                      $.ajax({
                                            url: BASE_URL+'ganhe/add_saldo_cliente',
                                            dataType: 'json',
                                            type: 'POST',
                                            contentType: 'application/x-www-form-urlencoded',
                                            data: {id_bonus:id_bonus, id_cliente:id_cliente},
                                            success: function( response ){
                                                if(response===1)
                                                {
                                                 location.reload();
                                                }
                                                //$("#notification").fadeOut(300, function() { $(this).remove(); });
                                                //$("button.active").parents("tr").remove();
                                            },
                                            error: function( jqXhr, textStatus, errorThrown ){
                                                   console.log( errorThrown );
                                            }
                                    });
                                  }
                                  else if(escola_saldo==="saldo-no")
                                  {
                                      
                                  }
			});
                        //end $('.modal-footer button').click(function()

					/**
					 * Toggles modal window
					 */
					    $('#myModal').modal("toggle");
				  //}//end age verification()
				  
				  //age_verification();
        });
});
</script>
<script>
$(document).ready(function() {
    var BASE_URL = "<?php echo base_url();?>";
    var id_cliente_indicacao = $("#example1").val();
    
    $.ajax({
                url: BASE_URL+'ganhe/cliente/get_situacao_indicacao',
                dataType: 'json',
                type: 'POST',
                contentType: 'application/x-www-form-urlencoded',
                data: {id_cliente_indicacao:id_cliente_indicacao},
                success: function( response ){
                    
                        if(response[0]['situacao']==='0')
                        {
                            $("#example1").prop('checked', false);
                        }
                        else if(response[0]['situacao']==='1')
                        {
                            $("#example1").prop('checked', true);
                        }
                },
                error: function( jqXhr, textStatus, errorThrown ){
                       console.log( errorThrown );
                }
        });
});
</script>
<script type='text/javascript'>
//$(function(){
  $('input[type="checkbox"]').change(function(){
    
    var BASE_URL = "<?php echo base_url();?>";
    var value=$(this).val();
    var n_i_e = $("#n_i_fic").val();
    
    if ($(this).prop('checked'))
    {
        var valueOn = 1;
        $.ajax({
                url: BASE_URL+'ganhe/cliente/update_situacao_indicacao',
                dataType: 'json',
                type: 'POST',
                contentType: 'application/x-www-form-urlencoded',
                data: {value:value, valueOn:valueOn, n_i_e:n_i_e},
                success: function( response ){
                        //console.log(response);
                },
                error: function( jqXhr, textStatus, errorThrown ){
                       //console.log( errorThrown );
                }
        });
    }
    else
    {
        var valueOn=0;
        $.ajax({
                url: BASE_URL+'ganhe/cliente/update_situacao_indicacao',
                dataType: 'json',
                type: 'POST',
                contentType: 'application/x-www-form-urlencoded',
                data: {value:value, valueOff:valueOn, n_i_e:n_i_e},
                success: function( response ){
                        //console.log(response);
                },
                error: function( jqXhr, textStatus, errorThrown ){
                       //console.log( errorThrown );
                }
        });
    }
  });
  
//});
</script> 
 
<script type='text/javascript'>
    function validateEmail(email)
    {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
    
    $('.alert-autocloseable-success').hide();
    $('.alert-autocloseable-warning').hide();
    $('.alert-autocloseable-danger').hide();
    $('.alert-autocloseable-info').hide();
    
    	$('.enableOnInput').click(function() {
            
        var email = $("#emailConvite").val();
        var id_cliente = $("#id_cliente").val();
        var BASE_URL = "<?php echo base_url();?>";
        
            if(validateEmail(email)== false)
            {
			$('#autoclosable-btn-danger').prop("disabled", true);
			$('.alert-autocloseable-danger').show();

			$('.alert-autocloseable-danger').delay(2000).fadeOut( "slow", function() {
				// Animation complete.
				$('#autoclosable-btn-danger').prop("disabled", false);
			});
            }
            else
            {
                         $.post(BASE_URL+'ganhe/cliente/getMailUser',{email: email,id_cliente:id_cliente},function(data){
                             
                             if(data==='true')
                             {
				$('#autoclosable-btn-success').prop("disabled", true);
                                $('.alert-autocloseable-success').show();

                                $('.alert-autocloseable-success').delay(10000).fadeOut( "slow", function() {
                                        // Animation complete.
                                        $('#autoclosable-btn-success').prop("disabled", false);
                                });
                                
                                $.ajax({
                                        url: BASE_URL+'ganhe/cliente/send_convite',
                                        dataType: 'json',
                                        type: 'POST',
                                        contentType: 'application/x-www-form-urlencoded',
                                        data: {email:email, id_cliente:id_cliente},
                                        success: function( response ){
                                             //console.log(response);
                                        },
                                        error: function( jqXhr, textStatus, errorThrown ){
                                            console.log( errorThrown );
                                        }
                                    });
                             }
                             else if(data ==='aluno')
                             {
                                $('#autoclosable-btn-info').prop("disabled", true);
                                $('.alert-autocloseable-info').show();

                                $('.alert-autocloseable-info').delay(4000).fadeOut( "slow", function() {
                                        // Animation complete.
                                        $('#autoclosable-btn-info').prop("disabled", false);
                                });
                             }
                             else if(data ==='indicacao')
                             {
                                 $('#autoclosable-btn-warning').prop("disabled", true);
                                $('.alert-autocloseable-warning').show();

                                $('.alert-autocloseable-warning').delay(4000).fadeOut( "slow", function() {
                                        // Animation complete.
                                    $('#autoclosable-btn-warning').prop("disabled", false);
                                });   
                             }
                             
                         })
            }
		});

            $(function () {
                $('#emailConvite').keyup(function () {
                    
                    if ($(this).val()=='') {
                        $('.enableOnInput').prop('disabled', true);
                    } else {
                        $('.enableOnInput').prop('disabled', false);
                    }
                });
            });
</script>