<footer>
    <div class="rodape-involucro">
        <div class="row">
            <div class="col-md-2 col-lg-3 col-xs-3 rodape-esquerdo">
                <ul>
                    <li><a href="<?= base_url("termo") ?>">Termo de uso</a></li>
                    <li><a href="<?= base_url("autenticacao") ?>">Autenticador</a></li>
                    <li><a id="btn-modal-cadastrar" data-toggle="modal" data-target="#modal-cadastro">Cadastro</a></li>
                </ul>
            </div>
            <div class="col-md-8 col-lg-6 col-xs-6 rodape-centro">
                <ul>
                    <li><a href="<?= base_url("site") ?>">Copyright &COPY; Imazon Cursos 2007/<?= date("Y") ?></a></li>
                    <li><a href="<?= base_url("suporte") ?>">E-mail: atendimento@imazoncursos.com.br</a></li>
                    <!--<li><a href="<?= base_url("suporte") ?>">Telefone: 4004-0435 Ramal:8229</a></li>-->
                </ul>
            </div>
            <div class="col-md-2  col-lg-3 col-xs-2 rodape-direito">
                <ul>
                    <li><a href="<?= base_url("suporte") ?>">Suporte</a></li>
                    <li><a href="<?= base_url("blog") ?>">Blog</a></li>
                    <li><a href="<?= base_url("cursos") ?>">Cursos</a></li>
                </ul>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12" style="padding: 14px 0px 9px 14px;">
                <a href="https://www.tray.com.br/" target="_blank">
                    <img src="<?=base_url("imgs/TrayCheckout.png")?>" style="width:65px; margin-right: 1%;">
                </a>    
                <a href="http://lattes.cnpq.br/" target="_blank">
                    <img src="<?= base_url("imgs/cnpq-lattes.jpeg") ?>" style="width:89px; margin-right:1%;"/>
                </a>
                <a href="http://di.cnpq.br/di/cadi/infoInstituicao.do?acao=buscaDadosInst&nroIdInst=744645" target="_blank">
                    <img src="<?= base_url("imgs/cadi.jpg") ?>" style="width:226px; margin-right:1%;"/>
                </a>
                <a href="http://www.abed.org.br/site/pt/associados/consulta_associados_abed/?busca_rapida=cidade+aprendizagem" target="_blank">
                    <img src="<?= base_url("imgs/abed.png") ?>" style="width:102px; margin-right:1%;"/>
                </a>
                <a href="https://cidadeaprendizagem.com.br" target="_blank">
                    <img src="<?= base_url("imgs/logo-cidade.jpg") ?>" style="width:158px; margin-right:1%;"/>
                </a>  
                <a href="https://www.google.com/transparencyreport/safebrowsing/diagnostic/?hl=pt-BR#url=imazoncursos.com.br" target="_blank">
                    <img src="<?= base_url("imgs/icone-google.png") ?>" style="width:140px; margin-right:1%;"/>
                </a>
                <span id="ss_img_wrapper_115-55_image_en"><a href="http://www.alphassl.com/ssl-certificates/wildcard-ssl.html" target="_blank" title="SSL Certificates"><img alt="Wildcard SSL Certificates" border=0 id="ss_img" src="//seal.alphassl.com/SiteSeal/images/alpha_noscript_115-55_en.gif" title="SSL Certificate"></a></span><script type="text/javascript" src="//seal.alphassl.com/SiteSeal/alpha_image_115-55_en.js"></script>
            </div>    
        </div>
    </div>
</div>    
</footer>
<script src="<?= base_url('template/js/script-min.js') ?>"></script>
<script src="<?= base_url('template/js/script.js') ?>"></script>
<section>
    <div class="modal fade" id="modal-cadastro" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-body modal-body-cadastro">
                    <p>Primeiro digite seu email<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></p>
                    <form class="frm-modal-cadastro" method="post" onsubmit="return false;">
                        <div class="form-group">
                            <input type="email" class="form-control" id="email_login_usuario" min="3" required value="" name="email" placeholder="Digite seu email">
                            <br />
                            <input type="submit" class="btn btn-success" value="Continuar" onclick="logarExterno()">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
</body>

<script>
    
    var cap = Math.floor(Math.random() * 9999); 
    var valida_captcha = function(){
                                if($("#captcha").val()==cap){
                                    var loader_html = '<div class="progress">'+
                                                        '<div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">'+
                                                          '<b>Aguarde...</b>'+
                                                        '</div>'+
                                                      '</div>';   
                                    $("form.frm-cadastro").submit();
                                    $(".modal-body-cadastro").html('').html(loader_html);
                                    $(".formulario-home").html('').html(loader_html);
                                }else{
                                    alert("Preencha o captcha corretamente!");
                                    return false;
                                }
                           } 
    
    $(function () {
        $(".btn-logar").click(function () {
            var email1 = $("#email-login").val();
            
            if (email1.length >= 6)
                $.post(URL_BASE + '/verifica_cadastro', {email: email1}, function (e) {
                    
                    console.log(email1);
                    
                    if (e == "true") {
                        var email = $("#email-login").val();
                        $('.formulario-home').html('');
                        $('.formulario-home').append('<div class="alert alert-success">Por favor, digite a sua senha de acesso</div>' +
                                '<form class="frm-inline frm-login" onsubmit="return false">' +
                                '<div class="form-group">' +
                                '<input type="email" class="form-control" id="email-login" name="email" value="' + email + '">' +
                                '</div>' +
                                '<input type="hidden" id="fonte" value="' + fonte + '"/>' +
                                '<div class="form-group"><input type="password" class="form-control" id="senha-login" name="senha" placeholder="Digite a sua Senha"></div><button type="submit" class="btn btn-logar btn-logar-home btn-success" onclick="logar()">Entrar</button>' +
                                ' <button type="button" class="btn btn-link" onclick="recuperar_senha()">Esqueci minha senha</button>' +
                                '</form><br />');
                    } else { 
                        var email = $("#email-login").val(),
                            formulario_cadastro = '<div class="alert alert-info">Preencha o campo abaixo com o valor informado</div>' +
                                                        '<form action="cadastrar" class="frm-inline frm-cadastro" method="post">' +
                                                            '<div class="form-group form-group-email">' +
                                                                '<input type="email" class="form-control"  id="email" minlength="6" required  name="email" value="' + email + '">' +
                                                            '</div>' +
                                                            '<pre>Captcha: <b>' + cap + '</b></pre>' + 
                                                            '<div class="form-group">' +
                                                                '<input type="text" class="form-control" id="captcha" minlength="6" required>' +
                                                            '</div>' +
                                                            '<input type="hidden" id="fonte" value="' + fonte + '"/>' +
                                                            '<button type="button" class="btn btn-primary" onclick="valida_captcha();">Cadastrar</button>' +
                                                        '</form>' +
                                                        '<br />' +
                                                        '<p style="font-size:12px; font-weight:bold;"><a href="../termo" target="_blank">*Ao clicar em cadastrar você estará concordando com nosso termo de uso</a></p>';
                        $('.frm-inline').removeClass('form-login-chamada').addClass('form-cadastro-chamada');
                        $('.formulario-home').html('');
                        $('.formulario-home').append(formulario_cadastro);
                        $('#captcha').focus();
                    }
                });

            return false;
        });
    });

    $(function () {
        $("#busca-topo").popover({content: '<form class="form-inline" action="<?= base_url('buscar_curso') ?>" method="post">' +
                    '<div class="form-group">' +
                    '<div class="input-group">' +
                    '<input type="text" class="form-control" name="busca" required placeholder="Digite uma Palavra?">' +
                    '</div>' +
                    '</div>' +
                    '<button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-search"></i></button>' +
                    '</form>',
            html: true});
    });

    $(function () {
        $('.btn-matricular-aluno-curso').click(function () {
            var curso = $("#curso-id").attr("value"), ch;

            $('.cursos-mat').each(function () {
                if ($(this).prop("checked") == true) {
                    ch = $(this).val();
                }
            });

            $("#form-matricular-curso").ajaxForm({
                success: function (e) {
                    if (e == "ok") {
                        window.location.href = "<?= base_url('ultimo_curso_matriculado') ?>";
                    } else {
                        $('.modal-body-cadastrar-curso').html('');
                        $('.modal-body-cadastrar-curso').append('<div class="alert alert-info">' + e + '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>' +
                                '<form class="frm-inline frm-modal-cadastro-curso-externo" method="post" onsubmit="return false;">' +
                                '<div class="form-group">' +
                                '<div class="input-group">' +
                                '<input type="email" class="form-control" id="email" min="3" required  name="email" placeholder="Digite o seu email">' +
                                '</div>' +
                                '</div>' +
                                '<div class="form-group"><div class="input-group"><button onclick="logarMatricularCurso(\'' + curso + '\', \'' + ch + '\')" class="btn btn-success">Continuar</button></div></div>' +
                                '<input type="hidden" id="fonte" value="' + fonte + '"/>' +
                                '</form>');
                    }
                    return false;
                }

            }).submit();
        });
    });

    function logarMatricularCurso(cursoSelecionado, ch) {
        var email1 = $("#email").val();
        if (email1.length >= 6) {
            $.post('<?= base_url("verifica_cadastro") ?>', {email: email1}, function (e) {
                if (e == "true") {
                    $('.modal-body-cadastrar-curso').html('');
                    $('.modal-body-cadastrar-curso').append('<div class="alert alert-success">Por favor, digite a sua senha de acesso</div>' +
                            '<form class="frm-inline frm-login" onsubmit="return false">' +
                            '<div class="form-group">' +
                            '<input type="email" class="form-control" id="email-login" name="email" value="' + email1 + '">' +
                            '</div>' +
                            '<input type="hidden" id="fonte" value="' + fonte + '"/>' +
                            '<div class="form-group"><input type="password" class="form-control" id="senha-login" name="senha" placeholder="Digite a sua Senha"></div><button type="submit" class="btn btn-success" onclick="logarMatCurso(\'matricula_curso\', \'' + cursoSelecionado + '\', \'' + ch + '\')">Entrar</button>' +
                            ' <button type="button" class="btn btn-link" onclick="recuperar_senha()">Esqueci minha senha</button>' +
                            '</form>');
                } else {
                    $('.modal-body-cadastrar-curso').html('');
                    $('.modal-body-cadastrar-curso').append('<div class="alert alert-info">Email válido! Agora digite os seus dados.</div>' +
                            '<form action="<?= base_url("cadastrar") ?>" class="frm-inline frm-cadastro" method="post">' +
                            '<div class="form-group form-group-email">' +
                            '<input type="email" class="form-control" id="email" min="3" required  name="email" value="' + email1 + '">' +
                            '</div>' +
                            '<pre>Captcha: <b>' + cap + '</b></pre>' + 
                            '<div class="form-group">' +
                                '<input type="text" class="form-control" id="captcha" minlength="6" required>' +
                            '</div>' +
                            '<input type="hidden" id="fonte" value="' + fonte + '"/>' +
                            '<button type="button" class="btn btn-primary" onclick="valida_captcha();">Cadastrar</button>' +
                            '<input type="hidden" name="matricular_curso" value="' + cursoSelecionado + ':' + ch + '"/>' +
                            '</form>' +
                            '<p style="font-size:12px; font-weight:bold;">*Ao clicar em cadastrar você estará concordando com nosso <a href="../termo" target="_blank">termo de uso</a></p>'
                            );
                }
            });
        }

    }

    function logarMatCurso(recarrega_pagina, curso, ch) {
        $.post('<?= base_url('logar') ?>', $(".frm-login").serialize(), function (e) {
            if (e == "ok") {
                setTimeout(function () {
                    if (recarrega_pagina !== "undefined") {
                        switch (recarrega_pagina) {
                            case "matricula_curso":
                                $.post('<?= base_url('matricular_curso') ?>', {curso: curso, carga_horaria: ch}, function (e) {
                                    if (e == "ok") {
                                        window.location.href = "<?= base_url('ultimo_curso_matriculado') ?>";
                                    } else {
                                        alert("Não foi possível atender a sua requisição. Tente novamente mais tarde.")
                                    }
                                });
                                break;
                            default:
                                window.location.reload();
                                break;
                        }
                    } else {
                        if ($("#fonte").val() != "undefined") {
                            window.location.href = fonte;
                        } else {
                            window.location.href = "./cursos_matriculados/";
                        }
                    }
                }, 1000);

            } else {
                $('#senha-login').val('').focus();
                $('.alert').html('Dados incorretos. Digite novamente sua senha.').removeClass('alert-success').addClass('alert-danger');
            }
        });
    }


    function logarExterno(local=null, obj=null) {
        
        var e1 = $(".email-aluno").val(),
            e2 = $('#email-login').val(),
            e3 = $('[name="email"]').val(),
            e4 = $('#email_login_usuario').val(),
            email1;

        if(e1!=undefined && e1.length>6){
            var email1 = e1;
        }

        if(e2!=undefined && e2.length>6){
            var email1 = e2;
        }

        if(e3!=undefined && e3.length>6){
            var email1 = e3;
        }
        
        if(e4!=undefined && e4.length>6){
            var email1 = e4;
        }
        
        if (email1.length >= 6 && IsEmail(email1)) {
            $.post('<?= base_url('verifica_cadastro') ?>', {email: email1}, function (e) {
                if (e == "true") {
                    $('.modal-body-cadastro').html('');
                    $('.modal-body-cadastro').append('<div class="alert alert-success">Por favor, digite a sua senha de acesso</div>' +
                            '<form class="frm-inline frm-login" onsubmit="return false">' +
                            '<div class="form-group">' +
                            '<input type="email" class="form-control email-aluno" id="email-login" name="email" value="' + email1 + '">' +
                            '</div>' +
                            '<input type="hidden" id="fonte" value="' + fonte + '"/>' +
                            '<div class="form-group"><input type="password" class="form-control" id="senha-login" name="senha" placeholder="Digite a sua Senha"></div><button type="submit" class="btn btn-success" onclick="logar()">Entrar</button>' +
                            ' <button type="button" class="btn btn-link" onclick="recuperar_senha()">Esqueci minha senha</button>' +
                            '</form>');
                } else if(e == "Erro login bloqueado"){
                    alert('Sua conta foi bloqueada. Entre em contato com o nosso atendimento.');
                } else {
                    $('.frm-inline').removeClass('form-login-chamada').addClass('form-cadastro-chamada');
                    $('.modal-body-cadastro').html('');
                    $('.modal-body-cadastro').append('<div class="alert alert-info">Email válido! Agora digite os seus dados.</div>' +
                            '<form action="<?= base_url('cadastrar') ?>" class="frm-inline frm-cadastro" method="post">' +
                            '<div class="form-group form-group-email">' +
                            '<input type="email" class="form-control" id="email" min="3" required  name="email" value="' + email1 + '">' +
                            '</div>' +
                            '<pre>Captcha: <b>' + cap + '</b></pre>' + 
                            '<div class="form-group">' +
                                '<input type="text" class="form-control" id="captcha" minlength="6" required>' +
                            '</div>' +
                            '<input type="hidden" id="fonte" value="' + fonte + '"/>' +
                            '<button type="button" class="btn btn-primary" onclick="valida_captcha();">Cadastrar</button>' +
                            '</form>' +
                            '<p style="font-size:12px; font-weight:bold;">*Ao clicar em cadastrar você estará concordando com nosso <a href="../termo" target="_blank">termo de uso</a></p>'
                            );
                }
            });
        }

    }
</script>
</html>