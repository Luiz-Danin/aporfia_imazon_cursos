<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Cliente_model extends CI_Model
{
    public function __construct() {
        parent::__construct();
        $this->imazon = $this->load->database('imazon', TRUE);
        $this->imazon10 = $this->load->database('imazon10', TRUE);
        $this->vip = $this->load->database('vip', TRUE);
    }
    
    public function checkClienteTermo($id_fic, $id_termo_ativo)
    {
        $id_cliente = $this->get_id_fic($id_fic)->result_array();
        
        if( count($id_cliente)>0 )
        {
            $this->db->select();
            $this->db->where('id_cliente', $id_cliente[0]['id']);
            $this->db->where('id_termo', $id_termo_ativo);

            return $this->db->get('idq_cliente_termo')->result_array();
        }
    }
    
    public function get_termo_ativo()
    {
        $this->db->select('id,nome,descricao');
        $this->db->where('situacao', 1);
        
        return $this->db->get('idq_termo_de_uso');
    }
    
    public function get_id_fic($id_fic)
    {
        $this->db->select('id, id_fic');
        $this->db->where('id_fic', $id_fic);
        
        return $this->db->get('idq_cliente');
    }
    
    public function cadastra($cliente)
    {
        $this->db->insert('idq_cliente',$cliente);
        return $insertId = $this->db->insert_id();
    }
    
    public function cadastra_cliente_termo($cliente_termo)
    {
        $this->db->insert('idq_cliente_termo',$cliente_termo);
    }
    
    public function verifica_indicacao_by_email($email)
    {
        $this->db->select('email_indicado ');
        $this->db->where('email_indicado', $email);
        $cliente =  $this->db->get('idq_indicacao')->result_array();
        
        if (count($cliente) == 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    public function get_id_cliente_by_id_fic($id_fic)
    {
        $this->db->select();
        $this->db->where('id_fic', $id_fic);
        
        $cliente = $this->db->get('idq_cliente')->result_array();
        return $cliente;
    }

    public function get_cliente_email_aluno($email)
    {
        $this->db->select('id_fic ');
        $this->db->where('email', $email);
        $id_fic =  $this->db->get('aluno')->result_array();
        $cliente = $this->get_id_cliente_by_id_fic($id_fic);
    }
    
    public function get_data_cliente_by_id($id)
    {
        $this->db->select();
        $this->db->where('id', $id);
        return $cliente =  $this->db->get('idq_cliente')->result_array();
    }
    
    public function cadastra_indicacao($email, $id_cliente)
    {
        $indicacao = array('id_cliente'=>$id_cliente, 'email_indicado'=>$email, 'data_indicacao'=>date("Y-m-d H:i:s"), 'situacao'=>0 );
        $this->db->insert('idq_indicacao', $indicacao);
        return $insertId = $this->db->insert_id();
    }
    
    public function get_id_cliente($email)
    {
        $this->db->select('id_cliente');
        $this->db->where('email_indicado', $email);
        
        return $this->db->get('idq_indicacao')->result_array();
    }
    
    public function get_data_indicacao($id_cliente)
    {
        $this->db->select();
        $this->db->where('id', $id_cliente);
        
        return $this->db->get('idq_cliente')->result_array();
    }
    
    public function get_indicacao_enviada($id_cliente)
    {
        $this->db->select();
        $this->db->where('id_cliente', $id_cliente);
        
        return $this->db->get('idq_indicacao')->result_array();
    }
    
    public function update_situacao_indicacao($situacao, $valueOn, $email)
    {
        $this->db->set('situacao', $valueOn);
        $this->db->where('id_cliente', $situacao);
        $this->db->where('email_indicado', $email);
        $this->db->update('idq_indicacao'); 
    }
    
    public function get_situacao_indicacao($id_cliente_indicacao)
    {
        $this->db->select('situacao');
        $this->db->where('id_cliente', $id_cliente_indicacao);
        
        return $this->db->get('idq_indicacao')->result_array();
    }
    
    public function get_bonificacao_cliente($id_cliente)
    {
        $this->db->select();
        $this->db->where('id_cliente', $id_cliente);
        $this->db->order_by('previsao_resgate', 'DESC');
        //$this->db->where('tipo_log_bonificacao', 0);
        
        return $this->db->get('idq_log_bonificacao')->result_array();
    }
    
    private function update_log_bonificacao($id_bonus)
    {
        $this->db->set('data', date("Y-m-d H:i:s") );
        $this->db->set('tipo_log_bonificacao', 1);
        $this->db->where('id', $id_bonus);
        $this->db->update('idq_log_bonificacao');
    }
    
    private function get_valor_log_bonificacao($id_bonus)
    {
        $this->db->select('valor');
        $this->db->where('id', $id_bonus);
        
        return $this->db->get('idq_log_bonificacao')->result_array();
    }


    private function add_saldo_resgate($id_bonus)
    {
        $valor = $this->get_valor_log_bonificacao($id_bonus);
        
        $indicacao = array('id_log_bonificacao'=>$id_bonus, 'data'=>date("Y-m-d H:i:s"), 'valor'=>$valor[0]['valor'] );
        $this->db->insert('idq_log_saldo_resgate', $indicacao);
        return $valor[0]['valor'];
    }
    
    private function get_saldo_cliente($id_cliente)
    {
        $this->db->select('saldo');
        $this->db->where('id', $id_cliente);
        
        return $this->db->get('idq_cliente')->result_array();
    }

    private function update_valor_saldo_cliente($valor_saldo, $id_cliente)
    {
        $saldo_atual = $this->get_saldo_cliente($id_cliente);
        $saldo = $saldo_atual[0]['saldo'];
        
        $saldo_atualizado = $saldo + (float)$valor_saldo;

        $this->db->set('saldo', $saldo_atualizado);
        $this->db->where('id', $id_cliente);
        $this->db->update('idq_cliente');
        return $this->db->affected_rows();
    }

    public function add_saldo_cliente($id_bonus, $id_cliente)
    {
        if($id_bonus && $id_cliente)
        {
            $this->update_log_bonificacao($id_bonus);
            $valor_saldo = $this->add_saldo_resgate($id_bonus);
            
            if ($valor_saldo)
            {
                return $this->update_valor_saldo_cliente($valor_saldo, $id_cliente);
            }
        }
    }
    
    public function get_nome_cliente_by_pedido($transacoes)
    {
        $this->db->select('id_aluno');
        $this->db->where_in('transacao', $transacoes);
        
        return $this->db->get('pedido')->result_array();
    }
    
    public function get_nome_aluno_by_idfic($list_idfic)
    {
        foreach ($list_idfic as $fic)
        {
            $this->db->select('email');
            $this->db->where('id_fic', $fic);
        
            $nomes[] = $this->db->get('aluno')->result_array();
        }
        
        foreach ($nomes as $nome)
        {
            $email_aluno[] = $nome[0]['email'];
        }
        
        return $email_aluno;
    }
    
    private function get_situacao_pedido($transacao)
    {
        $this->db->select('transacao, situacao');
        $this->db->where_in('transacao', $transacao);
        
        return $this->db->get('pedido')->result_array();
    }
    
    private function update_saldo_by_log_bonificacao($transacao, $id_cliente)
    {
            $this->db->select('transacao, tipo_log_bonificacao, valor');
            $this->db->where('transacao', $transacao);
            $log_bonificacao = $this->db->get('idq_log_bonificacao')->result_array();
            
            
            $saldo_atual = $this->get_saldo_cliente($id_cliente);
            $saldo = $saldo_atual[0]['saldo'];
            
            foreach ($log_bonificacao as $log)
            {
                if($log['tipo_log_bonificacao']==1)
                {
                    $saldo_cliente = $saldo - $log['valor'];
                    
                    $this->db->set('saldo', $saldo_cliente );
                    $this->db->where('id', $id_cliente);
                    $this->db->update('idq_cliente');
                }
            }
        
    }

    public function cancela_bonificacao($transacao, $id_cliente)
    {
       $transacao_pedido =  $this->get_situacao_pedido($transacao);
        
        foreach ($transacao_pedido as $pedido)
        {
            if($pedido['situacao']=='7')
            {
                $this->update_saldo_by_log_bonificacao($pedido['transacao'], $id_cliente);
                
                $this->db->set('tipo_log_bonificacao', 2 );
                $this->db->where('transacao', $pedido['transacao']);
                $this->db->update('idq_log_bonificacao');
            }
        }
    }
    
    public function get_bonificacao_padrao()
    {
         $this->db->select('bonificacao_padrao');
        
        return $this->db->get('idq_configuracao')->result_array();
    }
    
}