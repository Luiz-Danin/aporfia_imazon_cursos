<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cliente extends CI_Controller
{
    public function __construct() {
        parent::__construct();
        
        $this->load->model("Usuarios_model", "um");
        $this->load->model("Cliente_model", "cl");
        $this->load->model('notificacao_model');
    }
    
    private function get_data_session($id_aluno)
    {
        $aluno = $this->um->get_by_id($id_aluno)->result();
                
                $session_id = md5(rand(date('m'), date('s')));
                    $session = array(
                        'imazon_id_aluno' => $aluno[0]->id_aluno,
                        'imazon_id_fic' => $aluno[0]->id_fic,
                        'imazon_session_id ' => $session_id,
                        'imazon_email_aluno' => $aluno[0]->email,
                        'imazon_logado' => TRUE,
                        'imazon_nome' => $aluno[0]->nome
                    );

                    return $session;
    }

    public function index($id_aluno=null)
    {
        $id_aluno = decodifica($id_aluno);
        
        if($id_aluno)
        {
            $session = $this->get_data_session($id_aluno);
            $this->session->set_userdata($session);
            
            $termo_ativo = $this->getTermoAtivo();
                
            $imazon_id_fic = $this->session->userdata('imazon_id_fic');
            
            $isClienteTermo = $this->checkClienteTermo($imazon_id_fic, $termo_ativo[0]['id']);
            
            
            if($isClienteTermo===true)
            {
                $id_cliente = $this->cl->get_id_cliente_by_id_fic($imazon_id_fic);
                
                $this->session->set_userdata('id_cliente', $id_cliente[0]['id']);
                
                $nome_indicacao_recebida = $this->getIndicacaoRecebida();
            
                
                $indicacao_envida = $this->getIndicacaoEnviada($id_cliente[0]['id']);
                
                $bonificacao_cliente = $this->get_bonificacao_cliente($id_cliente[0]['id']);
                
                $saldo = (float)$id_cliente[0]['saldo'];
                
                $indicacao = array('nome_indicacao_recebida'=>$nome_indicacao_recebida, 'nome_indicacao_enviada'=>$indicacao_envida, 'bonificacao_cliente'=>$bonificacao_cliente, 'saldo'=>$saldo);
                
                $data = array(
                'titulo' => 'Indique e Ganhe',
                'palavra_chave' => 'Imazon Cursos'
                );
                
                $this->load->helper('html');

                $this->parser->parse('cliente/include/head', $data);

                $this->load->view('cliente/tela/cliente', $indicacao);

                $this->load->view('cliente/include/foot');
            }
            else
            {
                $termo = $this->getTermoAtivo();
                $count_termo = count($termo);
                
                if($count_termo===1)
                {
                    $data = array(
                    'titulo' => 'Indique e Ganhe',
                    'palavra_chave' => 'Imazon Cursos'
                    );
                    
                    $data_termo = array('id_aluno'=>$id_aluno,
                                        'id_termo'=>$termo[0]['id'],
                                        'titulo'=>$termo[0]['nome'],
                                        'descricao_termo'=> $termo[0]['descricao'],
                                        );

                    $this->load->helper('html');

                    $this->parser->parse('cliente/include/head', $data);

                    $this->load->view('cliente/tela/termo',$data_termo);

                    $this->load->view('cliente/include/foot');
                }
                
            }
        }
        else
        {
            
        }
    }
        
        private function checkClienteTermo($id_fic, $id_termo_ativo)
        {
            $cliente = $this->cl->checkClienteTermo($id_fic, $id_termo_ativo);
            
            if( count($cliente)>0 )
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        
        private function getTermoAtivo()
        {
            return $this->cl->get_termo_ativo()->result_array();
        }
        
        private function get_bonificacao_padrao()
        {
            return $this->cl->get_bonificacao_padrao();
        }
        
        private function exchange_key_array($session)
        {
            $percentual_bonificacao = $this->get_bonificacao_padrao();
            $bonificacao = (int)$percentual_bonificacao[0]['bonificacao_padrao'];
            
            $session = array('id_fic'=>$session['imazon_id_fic'], 'codigo_cliente'=>$session['codigo_cliente'], 
                              'nome'=>$session['imazon_nome'], 'data_cadastro'=> date("Y-m-d H:i:s"), 'percentual_bonificacao'=>$bonificacao,
                                'saldo'=>0, 'situacao'=>1);
            return $session;
        }
        
        private function generate_cod_cliente()
        {
            $this->load->helper('string');
            $codigo_cliente = random_string('alnum',10);
            $codigo_cliente = 'IDQ'.$codigo_cliente;
            $codigo_cliente = array('codigo_cliente'=>$codigo_cliente);
            
            return $codigo_cliente;
        }
        
        private function get_id_fic_cliente($id_fic)
        {
            $id_fic_cliente = $this->cl->get_id_fic($id_fic)->result_array();
            
            
            if( count($id_fic_cliente)===0 )
                $id_fic_cliente = count($id_fic_cliente);
            else
                $id_fic_cliente = $id_fic_cliente[0]['id_fic'];
            
            return $id_fic_cliente;
        }

        public function cadastraCliente()
        {
            $termo_ativo_post = $this->input->post('id_termo', true);
            $termo_ativo_post = decodifica($termo_ativo_post);
            
            $id_aluno = $this->input->post('id_aluno', true);
            $id_aluno = decodifica($id_aluno);
            
            $termo_ativo = $this->getTermoAtivo();
            
            if($termo_ativo_post===$termo_ativo[0]['id'])
            {
            
               $session = $this->get_data_session($id_aluno);
               
               $codigo_cliente = $this->generate_cod_cliente();
               $data_cliente = array_merge($session, $codigo_cliente);
               
               $cliente = $this->exchange_key_array($data_cliente);
               $id_fic = $this->get_id_fic_cliente($cliente['id_fic']);
            
               
               if($id_fic ==! $cliente['id_fic'])
               {
                 $id_cliente = $this->cl->cadastra($cliente);
                 
                 if($id_cliente)
                 {
                    $this->session->set_userdata('id_cliente',$id_cliente);
                
                    
                    $cliente_termo = array('id_cliente'=>$id_cliente,'id_termo'=>$termo_ativo[0]['id'], 'data_cadastro'=>date("Y-m-d H:i:s"));
                    $this->cl->cadastra_cliente_termo($cliente_termo);
                    
                    $id_aluno = codifica($id_aluno);
                    
                    redirect('ganhe/cliente/index/'.$id_aluno);
                 }
                 
               }
               else
               {
                   echo 'Cliente existente';
                   die;
               }
               
            }
            
        }
        
        private function template_convite($nome)
        {
            
            return '<html>
                    <head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
                    <title>IMAZON Cursos</title>
                    </head>
            <body>
            <table cellpadding="0" cellspacing="0" border="0" width="100%" style="width:100%">
                    <tr>
                            <td style="background-image: linear-gradient(to bottom, #74C302, #74C302 130%); box-shadow: inset 0 2px 3px 0 rgba(255,255,255,.3), inset 0 -3px 6px 0 rgba(0,0,0,.2), 0 3px 2px 0 rgba(0,0,0,.2);" bgcolor="#74C302" >
                                    <table cellpadding="10" cellspacing="0" border="0" width="650px" align="center" style="width:650px;font-family:Verdana, Helvetica, sans-serif;color:#FFFFFF;font-size:13px;line-height:180%" >
                                            <tr>
                                                    <td align="center">
                                                            <div style="font-size:40px;font-weight:bold;text-shadow: 0.1em 0.1em #333;">IMAZON CURSOS</div>
                                                            <div style="padding: 20px 1px 1px 1px;">WWW.IMAZONCURSOS.COM.BR</div>
                                                    </td>
                                                    <td align="right">
                                                    </td>

                                            </tr>
                                    </table>
                            </td>
                    </tr>

                    <tr>
                            <!--<td height="20px" style="background:#cf2b4f; border-bottom: #000 2px solid;box-shadow: 5px 5px 20px #000000;-webkit-box-shadow: 5px 5px 20px #00000;
            -moz-box-shadow: 5px 5px 20px #00000;height:5px" bgcolor="#5F872F" align="center"></td>-->

                    </tr>
                    <tr>
                            <td style="background-color:#EBEDE2;" bgcolor="#EBEDE2">
                                    <table cellpadding="10" cellspacing="0" border="0" width="650px" style="width:650px;font-family:Verdana, Helvetica, sans-serif;color:#000000;font-size:13px;line-height:180%" align="center">
                                            <tr>

                                                    <td style="border-bottom:1px #FFFFFF solid" align="left">

                                                    <div style="font-weight:bold;text-align:center;font-weight:bold;text-align:center;">SOLICITAÇÃO DE CONVITE</div></div>

                                                    <div style="background:; border-bottom: #000 2px solid;box-shadow: 5px 5px 20px #000000;-webkit-box-shadow: 5px 5px 20px #00000;
            -moz-box-shadow: 5px 5px 20px #00000;" bgcolor="#5F872F"><p>Olá, ' . $nome . ' enviou para você um convite de participação no Indique e ganhe do Imazon Cursos, assim que você aceitar indicação realizada poderá conhecer e usufruir imediatamente dos benefícios do Indique e ganhe.<p>

                                                    <p>Por ter sido convidado, merece estas boas-vindas especiais! Encontre o curso que está buscando.</p>

                                                    <b>Para participar do indique e ganhe é necessário realizar seu cadastro no Imazon Cursos, saiba mais em:</b><br />
                                                    <a href="'.base_url().'" target="_blank">www.imazoncursos.com.br</a><br /><br /></div>

                                                    <div style="background:; border-bottom: #000 2px solid;box-shadow: 5px 5px 20px #000000;-webkit-box-shadow: 5px 5px 20px #00000;
            -moz-box-shadow: 5px 5px 20px #00000;" bgcolor="#5F872F"><p>Horario de Atendimento de Segunda e Sexta de 09:00 as 17:00 horas</p>

                                                    Cordialmente,<br />
                                                    <b>Equipe Imazon</b><br />
                                                    <a href="mailto:atendimento@imazon.com.br">atendimento@imazon.com.br</a>
                                                    <br />
                                                    <br /></div>
                                                    </td>
                                            </tr>

                                    </table>
                            </td>
                    </tr>
                    <tr>
                            <td style="background-color:#1F2327;border-bottom:2px #FFFFFF solid" bgcolor="#1F2327">
                                    <table cellpadding="10" cellspacing="0" border="0" width="650px" style="width:650px;color:#9D9A93;font-family:Verdana, Helvetica, sans-serif;font-size:11px" align="center">
                                            <tr>
                                                    <td align="left">
                                                            Para retirar seu e-mail da lista clique <a href="https://docs.google.com/spreadsheet/viewform?formkey=dDRuU2ZLV0gzSnJOc1Y5LUg4QzN4ZWc6MQ#gid=0" style="color: #FFFFFF">aqui</a>
                                                    </td>
                                                    <td align="right">
                                                    </td>
                                            </tr>
                                    </table>
                            </td>
                    </tr>
            </table>
            </body>
            </html>';
        }
        
        public function send_convite()
        {
             $email_post = $this->input->post('email', true);
             $email = filter_var($email_post, FILTER_SANITIZE_EMAIL);
             
             $id_cliente = decodifica( $this->input->post('id_cliente', true) );
             $cliente = $this->cl->get_data_cliente_by_id($id_cliente);
             
             if($email)
             {
                $is_aluno = $this->um->verifica_email_cadastrado($email);
                
                if($is_aluno)
                {
                    echo 'false';
                }
                else
                {
                    if( $cliente[0]['nome']=="" )
                    {
                        $nome_cliente = 'um Aluno do Imazon';
                    }
                    else
                    {
                        $nome_cliente = $cliente[0]['nome'];
                    }
                    
                    $this->cl->cadastra_indicacao($email, $id_cliente);
                    $msg = $this->template_convite($nome_cliente);
                    
                    $assunto = $nome_cliente." está te convidando para ingressar no Imazon Cursos!";
                    $data = array('name'=>'Imazon Cursos','email'=>$email,'subject'=>$assunto,'body'=>$msg);

                    $ch = curl_init('http://curso.me/mail/LibMail.php');
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                    $enviado = curl_exec($ch);
                    curl_close($ch);
                }
             }
        }
        
        public function getMailUser()
        {
            $id_cliente = decodifica( $this->input->post('id_cliente', true) );
            
            $email = $this->input->post('email', true);
            $email_filter = filter_var($email, FILTER_SANITIZE_EMAIL);
            
            if($email_filter)
            {
                $is_aluno = $this->um->verifica_email_cadastrado($email);
                $is_indicacao = $this->cl->verifica_indicacao_by_email($email);
                
                if($is_aluno)
                {
                    echo 'aluno';
                }
                else if ($is_indicacao)
                {
                    echo 'indicacao';
                }
                else 
                {
                    echo 'true';
                }
            }
            else
            {
                echo 'false';
            }
        }
        public function get_situacao_indicacao()
        {
            $id_cliente_indicacao = decodifica( $this->input->post("id_cliente_indicacao", true) );
            $situacao = $this->cl->get_situacao_indicacao($id_cliente_indicacao);
            
            echo json_encode($situacao);
        }
        

        public function update_situacao_indicacao()
        {
            $email = decodifica( $this->input->post('n_i_e') );
            
            $situacao = decodifica( $this->input->post("value", true) );
            $valueOn = $this->input->post("valueOn", true);
            
            if( !empty($situacao) )
            {
                $this->cl->update_situacao_indicacao($situacao, $valueOn, $email);
            }
        }


        public function getIndicacaoEnviada($id_cliente)
        {
            $indicacao_enviada = $this->cl->get_indicacao_enviada($id_cliente);
            
            if(isset($indicacao_enviada))
            {
                $ind_env = array_column($indicacao_enviada, 'email_indicado');
                $ind_env_situacao = array_column($indicacao_enviada, 'situacao');
                
                $indicacao = array($ind_env, $ind_env_situacao);
                
                return $indicacao;
            }
        }
        
        private function get_idCliente()
        {
           $email = $this->session->userdata('imazon_email_aluno'); 
           //$id_cliente = $this->session->userdata('id_cliente');
           $indicacao_recebida = $this->cl->get_id_cliente($email);
           
           if (isset($indicacao_recebida[0]['id_cliente']) )
           {
               return $indicacao_recebida[0]['id_cliente'];
           }
           
        }
        
        public function getIndicacaoRecebida()
        {
            $id_cliente = $this->get_idCliente();
            
            $data_indicacao = $this->cl->get_data_indicacao($id_cliente);
            
            if(count($data_indicacao)==0 )
            {
                $data_indicacao=null;
            }
            return $data_indicacao;
            
        }
        
        private function get_nome_cliente_by_pedido($bonificacao)
        {
            foreach ($bonificacao as $bonifica)
            {
                $transacoes[] = $bonifica['transacao'];
            }
            
            $nomes = $this->cl->get_nome_cliente_by_pedido($transacoes);
            
            foreach ($nomes as $nome)
            {
                $list_idfic[]  = $nome['id_aluno'];
            }
            
            $emails = $this->cl->get_nome_aluno_by_idfic($list_idfic);
            
            //echo '<pre>';var_dump($bonificacao);
            //die;
            for ($i=0 ; $i<count($bonificacao); $i++)
            {
                $bonificacao[$i]['transacao'] = $emails[$i];
            }
            
            
            return $bonificacao;
        }
        
        private function set_situacao_bonificacao($bonificacao, $id_cliente)
        {
            if ( !empty($bonificacao) )
            {
                foreach ($bonificacao as $bonifica)
                {
                    $transacao[] = $bonifica['transacao'];
                }
                
                $this->cl->cancela_bonificacao($transacao, $id_cliente);
            }
            
        }

        private function get_bonificacao_cliente($id_cliente)
        {
           if($id_cliente)
           {
               $bonificacao = $this->cl->get_bonificacao_cliente($id_cliente);
               
               if( !empty($bonificacao) )
               {
                    $this->set_situacao_bonificacao($bonificacao, $id_cliente);
                    
                    return $this->get_nome_cliente_by_pedido($bonificacao);
               }
               else
               {
                   return array();
               }
           }
           else
            {
               return array();
            }
        }
        
        public function add_saldo_cliente()
        {
            $id_bonus = decodifica( $this->input->post("id_bonus", true) );
            $id_cliente = decodifica( $this->input->post("id_cliente", true) );
            
            //var_dump($id_bonus);
            
            $is_saldo = $this->cl->add_saldo_cliente($id_bonus, $id_cliente);
            
            echo json_encode($is_saldo);
        }
}