<?php

function format_number($n = '') {
    if ($n == '') {
        return '';
    }

    // Remove anything that isn't a number or decimal point.
    $n = trim(preg_replace('/([^0-9\.])/i', '', $n));

    return number_format($n, 2, ',', '.');
}

function formata_data_dia($data){
    return date('d/m/Y', strtotime($data));
}