
var fonte = $("#fonte").val();
    
    $(function () {
        $('.btn-topo-busca').on('click', function () {
            $(this).css('display', 'none');
            $('.busca').css('display', 'block');
        });
    });
    $(function () {
        $('.busca').on('mouseleave', function () {
            $(this).css('display', 'none').val('');
            $('.btn-topo-busca').css('display', 'block');
        });
    });
    $(function () {
        $('.busca').on('blur', function () {
            $(this).css('display', 'block');
        })
    });


    $(function () {
        $(".icone-chamada p > i").mouseover(function () {
            var button = $(this);
            button.addClass("animated bounce");
            setTimeout(function () {
                button.removeClass("animated bounce");
            }, 1000);
        });
    });

    $(function () {
        $('.matricular-curso').click(function () {
            var idCurso = $(this).attr("data-id");
            $('#modal-carga-horaria').modal('show');
            $("#curso-id").val(idCurso);
        });
    });

    $(function () {
        $('.matricular-curso').click(function () {
            $('#modal-carga-horaria').on('hidden.bs.modal', function () {
                $("#curso-id").val(null);
            });
        });
    });

    $(function () {
        $('#modal-cadastro').on('show.bs.modal', function () {
            $('.modal-body').html('');    
            $('.modal-body').append('<form class="frm-inline frm-cadastro" action="cadastrar" method="post">' +
                    '<div class="form-group">' +
                    '<input type="text" class="form-control" id="nome" required name="nome" placeholder="Digite o seu nome">' +
                    '</div>' +
                    '<div class="form-group">' +
                    '<input type="email" class="form-control" id="email" min="3" required  name="email" placeholder="Digite o seu email">' +
                    '</div>' +
                    '<div class="form-group">' +
                    '<input type="password" class="form-control" required min="6" max="12" id="senha" name="senha" placeholder="Digite a sua Senha">' +
                    '</div>' +
                    '<div class="form-group">' +
                    '<input type="password" class="form-control" required min="6" max="12" id="senha2" name="senha2" placeholder="Repita a sua Senha">' +
                    '</div>' +
                    '<input type="hidden" id="fonte" value="'+fonte+'"/>'+
                    '<button type="submit" class="btn btn-primary">Cadastrar</button>' +
                    '</form>');
        });
    });



    $(function () {
        $("#email-login").blur(function () {
            var email1 = $("#email-login").val();
            if (email1.length >= 6)
                $.post('verifica_cadastro', {email: $("#email-login").val()}, function (e) {
                    if (e == "true") {
                        var email = $("#email-login").val();
                        $('.formulario-home').html('');
                        $('.formulario-home').append('<div class="alert alert-success">Por favor, digite a sua senha de acesso</div>' +
                                '<form class="frm-inline frm-login" onsubmit="return false">' +
                                '<div class="form-group">' +
                                '<input type="email" class="form-control" id="email-login" name="email" value="' + email + '">' +
                                '</div>' +
                                '<input type="hidden" id="fonte" value="'+fonte+'"/>'+
                                '<div class="form-group"><input type="password" class="form-control" id="senha-login" name="senha" placeholder="Digite a sua Senha"></div><button type="submit" class="btn btn-logar btn-logar-home" onclick="logar()">Entrar</button>' +
                                '</form>');
                    } else {
                        var email = $("#email-login").val();
                        $('.frm-inline').removeClass('form-login-chamada').addClass('form-cadastro-chamada');
                        $('.formulario-home').html('');
                        $('.formulario-home').append('<div class="alert alert-info">Voc&ecirc; ainda n&atilde;o possui um cadastro em nosso site. Por favor, digite os seus dados</div>' +
                                '<form action="cadastrar" class="frm-inline frm-cadastro" method="post" onsubmit="validaFormCadastro(); return false;">' +
                                '<div class="form-group form-group-nome">' +
                                '<input type="text" class="form-control" id="nome" required name="nome" placeholder="Digite o seu nome">' +
                                '</div>' +
                                '<div class="form-group form-group-email">' +
                                '<input type="email" class="form-control" id="email" min="3" required  name="email" value="' + email + '">' +
                                '</div>' +
                                '<div class="form-group form-group-senha1">' +
                                '<input type="password" class="form-control" required min="6" max="12" id="senha" name="senha" placeholder="Digite a sua Senha">' +
                                '</div>' +
                                '<div class="form-group form-group-senha2">' +
                                '<input type="password" class="form-control" required min="6" max="12" id="senha2" name="senha2" placeholder="Repita a sua Senha">' +
                                '</div>' +
                                '<input type="hidden" id="fonte" value="'+fonte+'"/>'+
                                '<button type="submit" class="btn btn-primary">Cadastrar</button>' +
                                '</form>');
                    }
                });

            return false;
        });
    });

    function logar() {
        $.post('logar', $(".frm-login").serialize(), function (e) {
            if (e == "ok") {
                $('.alert').html('Dados corretos! Aguarde, pois você será redirecionado.').removeClass('alert-success').removeClass('alert-danger').addClass('alert-success');
                setTimeout(function () {
                    if($("#fonte").val()!="undefined"){
                        window.location.href = fonte;
                    }else{
                        window.location.href = "aluno";
                    }   
                }, 2000);

            } else {
                $('#senha-login').val('').focus();
                $('.alert').html('Dados incorretos. Digite novamente sua senha.').removeClass('alert-success').addClass('alert-danger');
            }
        });
    }

    //validação de formulário de cadastro(home)
    function validaFormCadastro() {

        var senha = $('#senha').val(),
                senha2 = $('#senha2').val(),
                nome = $('#nome').val(),
                email = $('#email').val();
                    
        senha = senha.trim();
        senha2 = senha2.trim();
        nome = nome.trim();
        email = email.trim();

        if (senha.length < 6) {
            $(".form-group-senha1").append('<label class="control-label" for="senha">Digite corretamente a sua senha</label>').addClass('has-warning');
            return false;
        }

        if (senha2.length < 6 && senha !== senha2) {
            $(".form-group-senha2").append('<label class="control-label" for="senha2">Digite corretamente a sua senha</label>').addClass('has-warning');
            return false;
        }

        if (nome.length < 6) {
            $(".form-group-nome").append('<label class="control-label" for="nome">Digite corretamente o seu nome</label>').addClass('has-warning').remove('label');
            return false;
        }

        if (email.length < 6) {
            $(".form-group-email").append('<label class="control-label" for="email">Digite corretamente o seu email</label>').addClass('has-warning');
            return false;
        }

        $(".frm-cadastro").submit();
    }

